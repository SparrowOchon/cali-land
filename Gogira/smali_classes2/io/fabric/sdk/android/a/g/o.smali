.class public final Lio/fabric/sdk/android/a/g/o;
.super Ljava/lang/Object;
.source "PromptSettingsData.java"


# instance fields
.field public final bja:Ljava/lang/String;

.field public final bjb:Z

.field public final bjc:Ljava/lang/String;

.field public final bjd:Z

.field public final bje:Ljava/lang/String;

.field public final message:Ljava/lang/String;

.field public final title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lio/fabric/sdk/android/a/g/o;->title:Ljava/lang/String;

    .line 38
    iput-object p2, p0, Lio/fabric/sdk/android/a/g/o;->message:Ljava/lang/String;

    .line 39
    iput-object p3, p0, Lio/fabric/sdk/android/a/g/o;->bja:Ljava/lang/String;

    .line 40
    iput-boolean p4, p0, Lio/fabric/sdk/android/a/g/o;->bjb:Z

    .line 41
    iput-object p5, p0, Lio/fabric/sdk/android/a/g/o;->bjc:Ljava/lang/String;

    .line 42
    iput-boolean p6, p0, Lio/fabric/sdk/android/a/g/o;->bjd:Z

    .line 43
    iput-object p7, p0, Lio/fabric/sdk/android/a/g/o;->bje:Ljava/lang/String;

    return-void
.end method
