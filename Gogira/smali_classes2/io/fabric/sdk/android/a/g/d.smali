.class public final Lio/fabric/sdk/android/a/g/d;
.super Ljava/lang/Object;
.source "AppRequestData.java"


# instance fields
.field public final biA:Ljava/lang/String;

.field public final biB:Ljava/lang/String;

.field public final biC:Ljava/lang/String;

.field public final biD:Ljava/lang/String;

.field public final biE:I

.field public final biF:Ljava/lang/String;

.field public final biG:Ljava/lang/String;

.field public final biH:Lio/fabric/sdk/android/a/g/n;

.field public final biI:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection<",
            "Lio/fabric/sdk/android/k;",
            ">;"
        }
    .end annotation
.end field

.field public final name:Ljava/lang/String;

.field public final pP:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lio/fabric/sdk/android/a/g/n;Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lio/fabric/sdk/android/a/g/n;",
            "Ljava/util/Collection<",
            "Lio/fabric/sdk/android/k;",
            ">;)V"
        }
    .end annotation

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lio/fabric/sdk/android/a/g/d;->pP:Ljava/lang/String;

    .line 49
    iput-object p2, p0, Lio/fabric/sdk/android/a/g/d;->biA:Ljava/lang/String;

    .line 50
    iput-object p3, p0, Lio/fabric/sdk/android/a/g/d;->biB:Ljava/lang/String;

    .line 51
    iput-object p4, p0, Lio/fabric/sdk/android/a/g/d;->biC:Ljava/lang/String;

    .line 53
    iput-object p5, p0, Lio/fabric/sdk/android/a/g/d;->biD:Ljava/lang/String;

    .line 54
    iput-object p6, p0, Lio/fabric/sdk/android/a/g/d;->name:Ljava/lang/String;

    .line 55
    iput p7, p0, Lio/fabric/sdk/android/a/g/d;->biE:I

    .line 56
    iput-object p8, p0, Lio/fabric/sdk/android/a/g/d;->biF:Ljava/lang/String;

    .line 57
    iput-object p9, p0, Lio/fabric/sdk/android/a/g/d;->biG:Ljava/lang/String;

    .line 58
    iput-object p10, p0, Lio/fabric/sdk/android/a/g/d;->biH:Lio/fabric/sdk/android/a/g/n;

    .line 59
    iput-object p11, p0, Lio/fabric/sdk/android/a/g/d;->biI:Ljava/util/Collection;

    return-void
.end method
