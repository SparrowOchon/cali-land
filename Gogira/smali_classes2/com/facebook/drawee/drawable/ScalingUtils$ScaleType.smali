.class public interface abstract Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;
.super Ljava/lang/Object;
.source "ScalingUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/drawee/drawable/ScalingUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ScaleType"
.end annotation


# static fields
.field public static final MQ:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

.field public static final MR:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

.field public static final MS:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

.field public static final MT:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

.field public static final MU:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

.field public static final MV:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

.field public static final MW:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

.field public static final MX:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

.field public static final MY:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 35
    sget-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$i;->MZ:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sput-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->MQ:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    .line 42
    sget-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$h;->MZ:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sput-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->MR:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    .line 49
    sget-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$f;->MZ:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sput-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->MS:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    .line 56
    sget-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$g;->MZ:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sput-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->MT:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    .line 59
    sget-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$b;->MZ:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sput-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->MU:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    .line 66
    sget-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$d;->MZ:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sput-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->MV:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    .line 73
    sget-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$c;->MZ:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sput-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->MW:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    .line 83
    sget-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$j;->MZ:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sput-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->MX:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    .line 90
    sget-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$e;->MZ:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sput-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->MY:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    return-void
.end method


# virtual methods
.method public abstract a(Landroid/graphics/Matrix;Landroid/graphics/Rect;IIFF)Landroid/graphics/Matrix;
.end method
