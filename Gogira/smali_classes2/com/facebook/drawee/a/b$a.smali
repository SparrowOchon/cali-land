.class public final enum Lcom/facebook/drawee/a/b$a;
.super Ljava/lang/Enum;
.source "DraweeEventTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/drawee/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/facebook/drawee/a/b$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum JQ:Lcom/facebook/drawee/a/b$a;

.field public static final enum JR:Lcom/facebook/drawee/a/b$a;

.field public static final enum JS:Lcom/facebook/drawee/a/b$a;

.field public static final enum JT:Lcom/facebook/drawee/a/b$a;

.field public static final enum JU:Lcom/facebook/drawee/a/b$a;

.field public static final enum JV:Lcom/facebook/drawee/a/b$a;

.field public static final enum JW:Lcom/facebook/drawee/a/b$a;

.field public static final enum JX:Lcom/facebook/drawee/a/b$a;

.field public static final enum JY:Lcom/facebook/drawee/a/b$a;

.field public static final enum JZ:Lcom/facebook/drawee/a/b$a;

.field public static final enum Ka:Lcom/facebook/drawee/a/b$a;

.field public static final enum Kb:Lcom/facebook/drawee/a/b$a;

.field public static final enum Kc:Lcom/facebook/drawee/a/b$a;

.field public static final enum Kd:Lcom/facebook/drawee/a/b$a;

.field public static final enum Ke:Lcom/facebook/drawee/a/b$a;

.field public static final enum Kf:Lcom/facebook/drawee/a/b$a;

.field public static final enum Kg:Lcom/facebook/drawee/a/b$a;

.field public static final enum Kh:Lcom/facebook/drawee/a/b$a;

.field public static final enum Ki:Lcom/facebook/drawee/a/b$a;

.field public static final enum Kj:Lcom/facebook/drawee/a/b$a;

.field public static final enum Kk:Lcom/facebook/drawee/a/b$a;

.field public static final enum Kl:Lcom/facebook/drawee/a/b$a;

.field public static final enum Km:Lcom/facebook/drawee/a/b$a;

.field public static final enum Kn:Lcom/facebook/drawee/a/b$a;

.field private static final synthetic Ko:[Lcom/facebook/drawee/a/b$a;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 28
    new-instance v0, Lcom/facebook/drawee/a/b$a;

    const/4 v1, 0x0

    const-string v2, "ON_SET_HIERARCHY"

    invoke-direct {v0, v2, v1}, Lcom/facebook/drawee/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/drawee/a/b$a;->JQ:Lcom/facebook/drawee/a/b$a;

    .line 29
    new-instance v0, Lcom/facebook/drawee/a/b$a;

    const/4 v2, 0x1

    const-string v3, "ON_CLEAR_HIERARCHY"

    invoke-direct {v0, v3, v2}, Lcom/facebook/drawee/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/drawee/a/b$a;->JR:Lcom/facebook/drawee/a/b$a;

    .line 30
    new-instance v0, Lcom/facebook/drawee/a/b$a;

    const/4 v3, 0x2

    const-string v4, "ON_SET_CONTROLLER"

    invoke-direct {v0, v4, v3}, Lcom/facebook/drawee/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/drawee/a/b$a;->JS:Lcom/facebook/drawee/a/b$a;

    .line 31
    new-instance v0, Lcom/facebook/drawee/a/b$a;

    const/4 v4, 0x3

    const-string v5, "ON_CLEAR_OLD_CONTROLLER"

    invoke-direct {v0, v5, v4}, Lcom/facebook/drawee/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/drawee/a/b$a;->JT:Lcom/facebook/drawee/a/b$a;

    .line 32
    new-instance v0, Lcom/facebook/drawee/a/b$a;

    const/4 v5, 0x4

    const-string v6, "ON_CLEAR_CONTROLLER"

    invoke-direct {v0, v6, v5}, Lcom/facebook/drawee/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/drawee/a/b$a;->JU:Lcom/facebook/drawee/a/b$a;

    .line 33
    new-instance v0, Lcom/facebook/drawee/a/b$a;

    const/4 v6, 0x5

    const-string v7, "ON_INIT_CONTROLLER"

    invoke-direct {v0, v7, v6}, Lcom/facebook/drawee/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/drawee/a/b$a;->JV:Lcom/facebook/drawee/a/b$a;

    .line 34
    new-instance v0, Lcom/facebook/drawee/a/b$a;

    const/4 v7, 0x6

    const-string v8, "ON_ATTACH_CONTROLLER"

    invoke-direct {v0, v8, v7}, Lcom/facebook/drawee/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/drawee/a/b$a;->JW:Lcom/facebook/drawee/a/b$a;

    .line 35
    new-instance v0, Lcom/facebook/drawee/a/b$a;

    const/4 v8, 0x7

    const-string v9, "ON_DETACH_CONTROLLER"

    invoke-direct {v0, v9, v8}, Lcom/facebook/drawee/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/drawee/a/b$a;->JX:Lcom/facebook/drawee/a/b$a;

    .line 36
    new-instance v0, Lcom/facebook/drawee/a/b$a;

    const/16 v9, 0x8

    const-string v10, "ON_RELEASE_CONTROLLER"

    invoke-direct {v0, v10, v9}, Lcom/facebook/drawee/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/drawee/a/b$a;->JY:Lcom/facebook/drawee/a/b$a;

    .line 37
    new-instance v0, Lcom/facebook/drawee/a/b$a;

    const/16 v10, 0x9

    const-string v11, "ON_DATASOURCE_SUBMIT"

    invoke-direct {v0, v11, v10}, Lcom/facebook/drawee/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/drawee/a/b$a;->JZ:Lcom/facebook/drawee/a/b$a;

    .line 38
    new-instance v0, Lcom/facebook/drawee/a/b$a;

    const/16 v11, 0xa

    const-string v12, "ON_DATASOURCE_RESULT"

    invoke-direct {v0, v12, v11}, Lcom/facebook/drawee/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/drawee/a/b$a;->Ka:Lcom/facebook/drawee/a/b$a;

    .line 39
    new-instance v0, Lcom/facebook/drawee/a/b$a;

    const/16 v12, 0xb

    const-string v13, "ON_DATASOURCE_RESULT_INT"

    invoke-direct {v0, v13, v12}, Lcom/facebook/drawee/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/drawee/a/b$a;->Kb:Lcom/facebook/drawee/a/b$a;

    .line 40
    new-instance v0, Lcom/facebook/drawee/a/b$a;

    const/16 v13, 0xc

    const-string v14, "ON_DATASOURCE_FAILURE"

    invoke-direct {v0, v14, v13}, Lcom/facebook/drawee/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/drawee/a/b$a;->Kc:Lcom/facebook/drawee/a/b$a;

    .line 41
    new-instance v0, Lcom/facebook/drawee/a/b$a;

    const/16 v14, 0xd

    const-string v15, "ON_DATASOURCE_FAILURE_INT"

    invoke-direct {v0, v15, v14}, Lcom/facebook/drawee/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/drawee/a/b$a;->Kd:Lcom/facebook/drawee/a/b$a;

    .line 42
    new-instance v0, Lcom/facebook/drawee/a/b$a;

    const/16 v15, 0xe

    const-string v14, "ON_HOLDER_ATTACH"

    invoke-direct {v0, v14, v15}, Lcom/facebook/drawee/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/drawee/a/b$a;->Ke:Lcom/facebook/drawee/a/b$a;

    .line 43
    new-instance v0, Lcom/facebook/drawee/a/b$a;

    const-string v14, "ON_HOLDER_DETACH"

    const/16 v15, 0xf

    invoke-direct {v0, v14, v15}, Lcom/facebook/drawee/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/drawee/a/b$a;->Kf:Lcom/facebook/drawee/a/b$a;

    .line 44
    new-instance v0, Lcom/facebook/drawee/a/b$a;

    const-string v14, "ON_DRAWABLE_SHOW"

    const/16 v15, 0x10

    invoke-direct {v0, v14, v15}, Lcom/facebook/drawee/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/drawee/a/b$a;->Kg:Lcom/facebook/drawee/a/b$a;

    .line 45
    new-instance v0, Lcom/facebook/drawee/a/b$a;

    const-string v14, "ON_DRAWABLE_HIDE"

    const/16 v15, 0x11

    invoke-direct {v0, v14, v15}, Lcom/facebook/drawee/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/drawee/a/b$a;->Kh:Lcom/facebook/drawee/a/b$a;

    .line 46
    new-instance v0, Lcom/facebook/drawee/a/b$a;

    const-string v14, "ON_ACTIVITY_START"

    const/16 v15, 0x12

    invoke-direct {v0, v14, v15}, Lcom/facebook/drawee/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/drawee/a/b$a;->Ki:Lcom/facebook/drawee/a/b$a;

    .line 47
    new-instance v0, Lcom/facebook/drawee/a/b$a;

    const-string v14, "ON_ACTIVITY_STOP"

    const/16 v15, 0x13

    invoke-direct {v0, v14, v15}, Lcom/facebook/drawee/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/drawee/a/b$a;->Kj:Lcom/facebook/drawee/a/b$a;

    .line 48
    new-instance v0, Lcom/facebook/drawee/a/b$a;

    const-string v14, "ON_RUN_CLEAR_CONTROLLER"

    const/16 v15, 0x14

    invoke-direct {v0, v14, v15}, Lcom/facebook/drawee/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/drawee/a/b$a;->Kk:Lcom/facebook/drawee/a/b$a;

    .line 49
    new-instance v0, Lcom/facebook/drawee/a/b$a;

    const-string v14, "ON_SCHEDULE_CLEAR_CONTROLLER"

    const/16 v15, 0x15

    invoke-direct {v0, v14, v15}, Lcom/facebook/drawee/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/drawee/a/b$a;->Kl:Lcom/facebook/drawee/a/b$a;

    .line 50
    new-instance v0, Lcom/facebook/drawee/a/b$a;

    const-string v14, "ON_SAME_CONTROLLER_SKIPPED"

    const/16 v15, 0x16

    invoke-direct {v0, v14, v15}, Lcom/facebook/drawee/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/drawee/a/b$a;->Km:Lcom/facebook/drawee/a/b$a;

    .line 51
    new-instance v0, Lcom/facebook/drawee/a/b$a;

    const-string v14, "ON_SUBMIT_CACHE_HIT"

    const/16 v15, 0x17

    invoke-direct {v0, v14, v15}, Lcom/facebook/drawee/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/drawee/a/b$a;->Kn:Lcom/facebook/drawee/a/b$a;

    const/16 v0, 0x18

    new-array v0, v0, [Lcom/facebook/drawee/a/b$a;

    .line 27
    sget-object v14, Lcom/facebook/drawee/a/b$a;->JQ:Lcom/facebook/drawee/a/b$a;

    aput-object v14, v0, v1

    sget-object v1, Lcom/facebook/drawee/a/b$a;->JR:Lcom/facebook/drawee/a/b$a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/drawee/a/b$a;->JS:Lcom/facebook/drawee/a/b$a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/drawee/a/b$a;->JT:Lcom/facebook/drawee/a/b$a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/drawee/a/b$a;->JU:Lcom/facebook/drawee/a/b$a;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/drawee/a/b$a;->JV:Lcom/facebook/drawee/a/b$a;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/drawee/a/b$a;->JW:Lcom/facebook/drawee/a/b$a;

    aput-object v1, v0, v7

    sget-object v1, Lcom/facebook/drawee/a/b$a;->JX:Lcom/facebook/drawee/a/b$a;

    aput-object v1, v0, v8

    sget-object v1, Lcom/facebook/drawee/a/b$a;->JY:Lcom/facebook/drawee/a/b$a;

    aput-object v1, v0, v9

    sget-object v1, Lcom/facebook/drawee/a/b$a;->JZ:Lcom/facebook/drawee/a/b$a;

    aput-object v1, v0, v10

    sget-object v1, Lcom/facebook/drawee/a/b$a;->Ka:Lcom/facebook/drawee/a/b$a;

    aput-object v1, v0, v11

    sget-object v1, Lcom/facebook/drawee/a/b$a;->Kb:Lcom/facebook/drawee/a/b$a;

    aput-object v1, v0, v12

    sget-object v1, Lcom/facebook/drawee/a/b$a;->Kc:Lcom/facebook/drawee/a/b$a;

    aput-object v1, v0, v13

    sget-object v1, Lcom/facebook/drawee/a/b$a;->Kd:Lcom/facebook/drawee/a/b$a;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/drawee/a/b$a;->Ke:Lcom/facebook/drawee/a/b$a;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/drawee/a/b$a;->Kf:Lcom/facebook/drawee/a/b$a;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/drawee/a/b$a;->Kg:Lcom/facebook/drawee/a/b$a;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/drawee/a/b$a;->Kh:Lcom/facebook/drawee/a/b$a;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/drawee/a/b$a;->Ki:Lcom/facebook/drawee/a/b$a;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/drawee/a/b$a;->Kj:Lcom/facebook/drawee/a/b$a;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/drawee/a/b$a;->Kk:Lcom/facebook/drawee/a/b$a;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/drawee/a/b$a;->Kl:Lcom/facebook/drawee/a/b$a;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/drawee/a/b$a;->Km:Lcom/facebook/drawee/a/b$a;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/drawee/a/b$a;->Kn:Lcom/facebook/drawee/a/b$a;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sput-object v0, Lcom/facebook/drawee/a/b$a;->Ko:[Lcom/facebook/drawee/a/b$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/drawee/a/b$a;
    .locals 1

    .line 27
    const-class v0, Lcom/facebook/drawee/a/b$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/facebook/drawee/a/b$a;

    return-object p0
.end method

.method public static values()[Lcom/facebook/drawee/a/b$a;
    .locals 1

    .line 27
    sget-object v0, Lcom/facebook/drawee/a/b$a;->Ko:[Lcom/facebook/drawee/a/b$a;

    invoke-virtual {v0}, [Lcom/facebook/drawee/a/b$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/drawee/a/b$a;

    return-object v0
.end method
