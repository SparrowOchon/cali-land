.class public final Lcom/facebook/d/b;
.super Ljava/lang/Object;
.source "DefaultImageFormats.java"


# static fields
.field public static final PD:Lcom/facebook/d/c;

.field public static final PE:Lcom/facebook/d/c;

.field public static final PF:Lcom/facebook/d/c;

.field public static final PG:Lcom/facebook/d/c;

.field public static final PH:Lcom/facebook/d/c;

.field public static final PI:Lcom/facebook/d/c;

.field public static final PJ:Lcom/facebook/d/c;

.field public static final PK:Lcom/facebook/d/c;

.field public static final PL:Lcom/facebook/d/c;

.field public static final PM:Lcom/facebook/d/c;

.field public static final PN:Lcom/facebook/d/c;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 18
    new-instance v0, Lcom/facebook/d/c;

    const-string v1, "JPEG"

    const-string v2, "jpeg"

    invoke-direct {v0, v1, v2}, Lcom/facebook/d/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/d/b;->PD:Lcom/facebook/d/c;

    .line 19
    new-instance v0, Lcom/facebook/d/c;

    const-string v1, "PNG"

    const-string v2, "png"

    invoke-direct {v0, v1, v2}, Lcom/facebook/d/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/d/b;->PE:Lcom/facebook/d/c;

    .line 20
    new-instance v0, Lcom/facebook/d/c;

    const-string v1, "GIF"

    const-string v2, "gif"

    invoke-direct {v0, v1, v2}, Lcom/facebook/d/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/d/b;->PF:Lcom/facebook/d/c;

    .line 21
    new-instance v0, Lcom/facebook/d/c;

    const-string v1, "BMP"

    const-string v2, "bmp"

    invoke-direct {v0, v1, v2}, Lcom/facebook/d/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/d/b;->PG:Lcom/facebook/d/c;

    .line 22
    new-instance v0, Lcom/facebook/d/c;

    const-string v1, "ICO"

    const-string v2, "ico"

    invoke-direct {v0, v1, v2}, Lcom/facebook/d/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/d/b;->PH:Lcom/facebook/d/c;

    .line 23
    new-instance v0, Lcom/facebook/d/c;

    const-string v1, "webp"

    const-string v2, "WEBP_SIMPLE"

    invoke-direct {v0, v2, v1}, Lcom/facebook/d/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/d/b;->PI:Lcom/facebook/d/c;

    .line 24
    new-instance v0, Lcom/facebook/d/c;

    const-string v2, "WEBP_LOSSLESS"

    invoke-direct {v0, v2, v1}, Lcom/facebook/d/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/d/b;->PJ:Lcom/facebook/d/c;

    .line 25
    new-instance v0, Lcom/facebook/d/c;

    const-string v2, "WEBP_EXTENDED"

    invoke-direct {v0, v2, v1}, Lcom/facebook/d/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/d/b;->PK:Lcom/facebook/d/c;

    .line 26
    new-instance v0, Lcom/facebook/d/c;

    const-string v2, "WEBP_EXTENDED_WITH_ALPHA"

    invoke-direct {v0, v2, v1}, Lcom/facebook/d/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/d/b;->PL:Lcom/facebook/d/c;

    .line 28
    new-instance v0, Lcom/facebook/d/c;

    const-string v2, "WEBP_ANIMATED"

    invoke-direct {v0, v2, v1}, Lcom/facebook/d/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/d/b;->PM:Lcom/facebook/d/c;

    .line 29
    new-instance v0, Lcom/facebook/d/c;

    const-string v1, "HEIF"

    const-string v2, "heif"

    invoke-direct {v0, v1, v2}, Lcom/facebook/d/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/d/b;->PN:Lcom/facebook/d/c;

    return-void
.end method

.method public static a(Lcom/facebook/d/c;)Z
    .locals 1

    .line 40
    invoke-static {p0}, Lcom/facebook/d/b;->b(Lcom/facebook/d/c;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/facebook/d/b;->PM:Lcom/facebook/d/c;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    return p0

    :cond_1
    :goto_0
    const/4 p0, 0x1

    return p0
.end method

.method public static b(Lcom/facebook/d/c;)Z
    .locals 1

    .line 51
    sget-object v0, Lcom/facebook/d/b;->PI:Lcom/facebook/d/c;

    if-eq p0, v0, :cond_1

    sget-object v0, Lcom/facebook/d/b;->PJ:Lcom/facebook/d/c;

    if-eq p0, v0, :cond_1

    sget-object v0, Lcom/facebook/d/b;->PK:Lcom/facebook/d/c;

    if-eq p0, v0, :cond_1

    sget-object v0, Lcom/facebook/d/b;->PL:Lcom/facebook/d/c;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    return p0

    :cond_1
    :goto_0
    const/4 p0, 0x1

    return p0
.end method
