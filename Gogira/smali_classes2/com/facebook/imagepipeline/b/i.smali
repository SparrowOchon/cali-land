.class public final Lcom/facebook/imagepipeline/b/i;
.super Ljava/lang/Object;
.source "ImagePipelineExperiments.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/facebook/imagepipeline/b/i$b;,
        Lcom/facebook/imagepipeline/b/i$c;,
        Lcom/facebook/imagepipeline/b/i$a;
    }
.end annotation


# instance fields
.field final Pf:Z

.field final SM:Lcom/facebook/common/d/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/d/k<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field final TA:Z

.field final TB:Lcom/facebook/imagepipeline/b/i$c;

.field final TC:Z

.field final Tp:Z

.field final Tq:Lcom/facebook/common/k/b$a;

.field final Tr:Z

.field final Ts:Lcom/facebook/common/k/b;

.field final Tt:Z

.field final Tu:Z

.field final Tv:I

.field final Tw:I

.field Tx:Z

.field final Ty:I

.field final Tz:Z


# direct methods
.method private constructor <init>(Lcom/facebook/imagepipeline/b/i$a;)V
    .locals 1

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1144
    iget-boolean v0, p1, Lcom/facebook/imagepipeline/b/i$a;->Tp:Z

    .line 53
    iput-boolean v0, p0, Lcom/facebook/imagepipeline/b/i;->Tp:Z

    .line 2144
    iget-object v0, p1, Lcom/facebook/imagepipeline/b/i$a;->Tq:Lcom/facebook/common/k/b$a;

    .line 54
    iput-object v0, p0, Lcom/facebook/imagepipeline/b/i;->Tq:Lcom/facebook/common/k/b$a;

    .line 3144
    iget-boolean v0, p1, Lcom/facebook/imagepipeline/b/i$a;->Tr:Z

    .line 55
    iput-boolean v0, p0, Lcom/facebook/imagepipeline/b/i;->Tr:Z

    .line 4144
    iget-object v0, p1, Lcom/facebook/imagepipeline/b/i$a;->Ts:Lcom/facebook/common/k/b;

    .line 56
    iput-object v0, p0, Lcom/facebook/imagepipeline/b/i;->Ts:Lcom/facebook/common/k/b;

    .line 5144
    iget-boolean v0, p1, Lcom/facebook/imagepipeline/b/i$a;->Tt:Z

    .line 57
    iput-boolean v0, p0, Lcom/facebook/imagepipeline/b/i;->Tt:Z

    .line 6144
    iget-boolean v0, p1, Lcom/facebook/imagepipeline/b/i$a;->Tu:Z

    .line 58
    iput-boolean v0, p0, Lcom/facebook/imagepipeline/b/i;->Tu:Z

    .line 7144
    iget v0, p1, Lcom/facebook/imagepipeline/b/i$a;->Tv:I

    .line 59
    iput v0, p0, Lcom/facebook/imagepipeline/b/i;->Tv:I

    .line 8144
    iget v0, p1, Lcom/facebook/imagepipeline/b/i$a;->Tw:I

    .line 60
    iput v0, p0, Lcom/facebook/imagepipeline/b/i;->Tw:I

    .line 61
    iget-boolean v0, p1, Lcom/facebook/imagepipeline/b/i$a;->Tx:Z

    iput-boolean v0, p0, Lcom/facebook/imagepipeline/b/i;->Tx:Z

    .line 9144
    iget v0, p1, Lcom/facebook/imagepipeline/b/i$a;->Ty:I

    .line 62
    iput v0, p0, Lcom/facebook/imagepipeline/b/i;->Ty:I

    .line 10144
    iget-boolean v0, p1, Lcom/facebook/imagepipeline/b/i$a;->Tz:Z

    .line 63
    iput-boolean v0, p0, Lcom/facebook/imagepipeline/b/i;->Tz:Z

    .line 11144
    iget-boolean v0, p1, Lcom/facebook/imagepipeline/b/i$a;->TA:Z

    .line 64
    iput-boolean v0, p0, Lcom/facebook/imagepipeline/b/i;->TA:Z

    .line 12144
    iget-object v0, p1, Lcom/facebook/imagepipeline/b/i$a;->TB:Lcom/facebook/imagepipeline/b/i$c;

    if-nez v0, :cond_0

    .line 66
    new-instance v0, Lcom/facebook/imagepipeline/b/i$b;

    invoke-direct {v0}, Lcom/facebook/imagepipeline/b/i$b;-><init>()V

    iput-object v0, p0, Lcom/facebook/imagepipeline/b/i;->TB:Lcom/facebook/imagepipeline/b/i$c;

    goto :goto_0

    .line 13144
    :cond_0
    iget-object v0, p1, Lcom/facebook/imagepipeline/b/i$a;->TB:Lcom/facebook/imagepipeline/b/i$c;

    .line 68
    iput-object v0, p0, Lcom/facebook/imagepipeline/b/i;->TB:Lcom/facebook/imagepipeline/b/i$c;

    .line 70
    :goto_0
    iget-object v0, p1, Lcom/facebook/imagepipeline/b/i$a;->SM:Lcom/facebook/common/d/k;

    iput-object v0, p0, Lcom/facebook/imagepipeline/b/i;->SM:Lcom/facebook/common/d/k;

    .line 71
    iget-boolean v0, p1, Lcom/facebook/imagepipeline/b/i$a;->TC:Z

    iput-boolean v0, p0, Lcom/facebook/imagepipeline/b/i;->TC:Z

    .line 72
    iget-boolean p1, p1, Lcom/facebook/imagepipeline/b/i$a;->Pf:Z

    iput-boolean p1, p0, Lcom/facebook/imagepipeline/b/i;->Pf:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/facebook/imagepipeline/b/i$a;B)V
    .locals 0

    .line 33
    invoke-direct {p0, p1}, Lcom/facebook/imagepipeline/b/i;-><init>(Lcom/facebook/imagepipeline/b/i$a;)V

    return-void
.end method
