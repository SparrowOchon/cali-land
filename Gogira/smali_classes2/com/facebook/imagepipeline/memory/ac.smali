.class public final Lcom/facebook/imagepipeline/memory/ac;
.super Ljava/lang/Object;
.source "PoolFactory.java"


# instance fields
.field private QN:Lcom/facebook/imagepipeline/memory/d;

.field private QQ:Lcom/facebook/common/g/g;

.field private QS:Lcom/facebook/imagepipeline/memory/n;

.field private Rg:Lcom/facebook/common/g/j;

.field private final VP:Lcom/facebook/imagepipeline/memory/ab;

.field private VQ:Lcom/facebook/imagepipeline/memory/j;

.field private VR:Lcom/facebook/imagepipeline/memory/x;

.field private VS:Lcom/facebook/common/g/a;


# direct methods
.method public constructor <init>(Lcom/facebook/imagepipeline/memory/ab;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {p1}, Lcom/facebook/common/d/i;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/imagepipeline/memory/ab;

    iput-object p1, p0, Lcom/facebook/imagepipeline/memory/ac;->VP:Lcom/facebook/imagepipeline/memory/ab;

    return-void
.end method

.method private aC(I)Lcom/facebook/imagepipeline/memory/s;
    .locals 1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 156
    invoke-direct {p0}, Lcom/facebook/imagepipeline/memory/ac;->iY()Lcom/facebook/imagepipeline/memory/j;

    move-result-object p1

    return-object p1

    .line 158
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Invalid MemoryChunkType"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 154
    :cond_1
    invoke-direct {p0}, Lcom/facebook/imagepipeline/memory/ac;->jb()Lcom/facebook/imagepipeline/memory/x;

    move-result-object p1

    return-object p1
.end method

.method private iY()Lcom/facebook/imagepipeline/memory/j;
    .locals 4

    .line 78
    iget-object v0, p0, Lcom/facebook/imagepipeline/memory/ac;->VQ:Lcom/facebook/imagepipeline/memory/j;

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Lcom/facebook/imagepipeline/memory/j;

    iget-object v1, p0, Lcom/facebook/imagepipeline/memory/ac;->VP:Lcom/facebook/imagepipeline/memory/ab;

    .line 5100
    iget-object v1, v1, Lcom/facebook/imagepipeline/memory/ab;->SY:Lcom/facebook/common/g/b;

    .line 81
    iget-object v2, p0, Lcom/facebook/imagepipeline/memory/ac;->VP:Lcom/facebook/imagepipeline/memory/ab;

    .line 5104
    iget-object v2, v2, Lcom/facebook/imagepipeline/memory/ab;->VH:Lcom/facebook/imagepipeline/memory/ad;

    .line 82
    iget-object v3, p0, Lcom/facebook/imagepipeline/memory/ac;->VP:Lcom/facebook/imagepipeline/memory/ab;

    .line 5108
    iget-object v3, v3, Lcom/facebook/imagepipeline/memory/ab;->VI:Lcom/facebook/imagepipeline/memory/ae;

    .line 83
    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/imagepipeline/memory/j;-><init>(Lcom/facebook/common/g/b;Lcom/facebook/imagepipeline/memory/ad;Lcom/facebook/imagepipeline/memory/ae;)V

    iput-object v0, p0, Lcom/facebook/imagepipeline/memory/ac;->VQ:Lcom/facebook/imagepipeline/memory/j;

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/facebook/imagepipeline/memory/ac;->VQ:Lcom/facebook/imagepipeline/memory/j;

    return-object v0
.end method

.method private jb()Lcom/facebook/imagepipeline/memory/x;
    .locals 4

    .line 102
    iget-object v0, p0, Lcom/facebook/imagepipeline/memory/ac;->VR:Lcom/facebook/imagepipeline/memory/x;

    if-nez v0, :cond_0

    .line 103
    new-instance v0, Lcom/facebook/imagepipeline/memory/x;

    iget-object v1, p0, Lcom/facebook/imagepipeline/memory/ac;->VP:Lcom/facebook/imagepipeline/memory/ab;

    .line 8100
    iget-object v1, v1, Lcom/facebook/imagepipeline/memory/ab;->SY:Lcom/facebook/common/g/b;

    .line 105
    iget-object v2, p0, Lcom/facebook/imagepipeline/memory/ac;->VP:Lcom/facebook/imagepipeline/memory/ab;

    .line 8104
    iget-object v2, v2, Lcom/facebook/imagepipeline/memory/ab;->VH:Lcom/facebook/imagepipeline/memory/ad;

    .line 106
    iget-object v3, p0, Lcom/facebook/imagepipeline/memory/ac;->VP:Lcom/facebook/imagepipeline/memory/ab;

    .line 8108
    iget-object v3, v3, Lcom/facebook/imagepipeline/memory/ab;->VI:Lcom/facebook/imagepipeline/memory/ae;

    .line 107
    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/imagepipeline/memory/x;-><init>(Lcom/facebook/common/g/b;Lcom/facebook/imagepipeline/memory/ad;Lcom/facebook/imagepipeline/memory/ae;)V

    iput-object v0, p0, Lcom/facebook/imagepipeline/memory/ac;->VR:Lcom/facebook/imagepipeline/memory/x;

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/facebook/imagepipeline/memory/ac;->VR:Lcom/facebook/imagepipeline/memory/x;

    return-object v0
.end method


# virtual methods
.method public final aB(I)Lcom/facebook/common/g/g;
    .locals 2

    .line 117
    iget-object v0, p0, Lcom/facebook/imagepipeline/memory/ac;->QQ:Lcom/facebook/common/g/g;

    if-nez v0, :cond_0

    .line 118
    new-instance v0, Lcom/facebook/imagepipeline/memory/v;

    .line 120
    invoke-direct {p0, p1}, Lcom/facebook/imagepipeline/memory/ac;->aC(I)Lcom/facebook/imagepipeline/memory/s;

    move-result-object p1

    invoke-virtual {p0}, Lcom/facebook/imagepipeline/memory/ac;->jc()Lcom/facebook/common/g/j;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/facebook/imagepipeline/memory/v;-><init>(Lcom/facebook/imagepipeline/memory/s;Lcom/facebook/common/g/j;)V

    iput-object v0, p0, Lcom/facebook/imagepipeline/memory/ac;->QQ:Lcom/facebook/common/g/g;

    .line 122
    :cond_0
    iget-object p1, p0, Lcom/facebook/imagepipeline/memory/ac;->QQ:Lcom/facebook/common/g/g;

    return-object p1
.end method

.method public final iX()Lcom/facebook/imagepipeline/memory/d;
    .locals 5

    .line 42
    iget-object v0, p0, Lcom/facebook/imagepipeline/memory/ac;->QN:Lcom/facebook/imagepipeline/memory/d;

    if-nez v0, :cond_4

    .line 43
    iget-object v0, p0, Lcom/facebook/imagepipeline/memory/ac;->VP:Lcom/facebook/imagepipeline/memory/ab;

    .line 1124
    iget-object v0, v0, Lcom/facebook/imagepipeline/memory/ab;->VL:Ljava/lang/String;

    const/4 v1, -0x1

    .line 44
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    const/4 v3, 0x2

    const/4 v4, 0x1

    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v2, "dummy"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_1
    const-string v2, "experimental"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "legacy"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_3
    const-string v2, "legacy_default_params"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x2

    :cond_0
    :goto_0
    if-eqz v1, :cond_3

    if-eq v1, v4, :cond_2

    if-eq v1, v3, :cond_1

    .line 68
    new-instance v0, Lcom/facebook/imagepipeline/memory/h;

    iget-object v1, p0, Lcom/facebook/imagepipeline/memory/ac;->VP:Lcom/facebook/imagepipeline/memory/ab;

    .line 4100
    iget-object v1, v1, Lcom/facebook/imagepipeline/memory/ab;->SY:Lcom/facebook/common/g/b;

    .line 69
    iget-object v2, p0, Lcom/facebook/imagepipeline/memory/ac;->VP:Lcom/facebook/imagepipeline/memory/ab;

    .line 5092
    iget-object v2, v2, Lcom/facebook/imagepipeline/memory/ab;->VE:Lcom/facebook/imagepipeline/memory/ad;

    .line 70
    iget-object v3, p0, Lcom/facebook/imagepipeline/memory/ac;->VP:Lcom/facebook/imagepipeline/memory/ab;

    .line 5096
    iget-object v3, v3, Lcom/facebook/imagepipeline/memory/ab;->VF:Lcom/facebook/imagepipeline/memory/ae;

    .line 71
    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/imagepipeline/memory/h;-><init>(Lcom/facebook/common/g/b;Lcom/facebook/imagepipeline/memory/ad;Lcom/facebook/imagepipeline/memory/ae;)V

    iput-object v0, p0, Lcom/facebook/imagepipeline/memory/ac;->QN:Lcom/facebook/imagepipeline/memory/d;

    goto :goto_1

    .line 59
    :cond_1
    new-instance v0, Lcom/facebook/imagepipeline/memory/h;

    iget-object v1, p0, Lcom/facebook/imagepipeline/memory/ac;->VP:Lcom/facebook/imagepipeline/memory/ab;

    .line 3100
    iget-object v1, v1, Lcom/facebook/imagepipeline/memory/ab;->SY:Lcom/facebook/common/g/b;

    .line 62
    invoke-static {}, Lcom/facebook/imagepipeline/memory/k;->iU()Lcom/facebook/imagepipeline/memory/ad;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/imagepipeline/memory/ac;->VP:Lcom/facebook/imagepipeline/memory/ab;

    .line 4096
    iget-object v3, v3, Lcom/facebook/imagepipeline/memory/ab;->VF:Lcom/facebook/imagepipeline/memory/ae;

    .line 63
    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/imagepipeline/memory/h;-><init>(Lcom/facebook/common/g/b;Lcom/facebook/imagepipeline/memory/ad;Lcom/facebook/imagepipeline/memory/ae;)V

    iput-object v0, p0, Lcom/facebook/imagepipeline/memory/ac;->QN:Lcom/facebook/imagepipeline/memory/d;

    goto :goto_1

    .line 49
    :cond_2
    new-instance v0, Lcom/facebook/imagepipeline/memory/p;

    iget-object v1, p0, Lcom/facebook/imagepipeline/memory/ac;->VP:Lcom/facebook/imagepipeline/memory/ab;

    .line 1128
    iget v1, v1, Lcom/facebook/imagepipeline/memory/ab;->VM:I

    .line 51
    iget-object v2, p0, Lcom/facebook/imagepipeline/memory/ac;->VP:Lcom/facebook/imagepipeline/memory/ab;

    .line 1132
    iget v2, v2, Lcom/facebook/imagepipeline/memory/ab;->VN:I

    .line 53
    invoke-static {}, Lcom/facebook/imagepipeline/memory/y;->iW()Lcom/facebook/imagepipeline/memory/y;

    move-result-object v3

    .line 55
    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/imagepipeline/memory/p;-><init>(IILcom/facebook/imagepipeline/memory/ae;)V

    iput-object v0, p0, Lcom/facebook/imagepipeline/memory/ac;->QN:Lcom/facebook/imagepipeline/memory/d;

    goto :goto_1

    .line 46
    :cond_3
    new-instance v0, Lcom/facebook/imagepipeline/memory/m;

    invoke-direct {v0}, Lcom/facebook/imagepipeline/memory/m;-><init>()V

    iput-object v0, p0, Lcom/facebook/imagepipeline/memory/ac;->QN:Lcom/facebook/imagepipeline/memory/d;

    .line 74
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/facebook/imagepipeline/memory/ac;->QN:Lcom/facebook/imagepipeline/memory/d;

    return-object v0

    :sswitch_data_0
    .sparse-switch
        -0x6f64eb86 -> :sswitch_3
        -0x41f50c37 -> :sswitch_2
        -0x181d2318 -> :sswitch_1
        0x5b804a8 -> :sswitch_0
    .end sparse-switch
.end method

.method public final iZ()Lcom/facebook/imagepipeline/memory/n;
    .locals 3

    .line 89
    iget-object v0, p0, Lcom/facebook/imagepipeline/memory/ac;->QS:Lcom/facebook/imagepipeline/memory/n;

    if-nez v0, :cond_0

    .line 90
    new-instance v0, Lcom/facebook/imagepipeline/memory/n;

    iget-object v1, p0, Lcom/facebook/imagepipeline/memory/ac;->VP:Lcom/facebook/imagepipeline/memory/ab;

    .line 6100
    iget-object v1, v1, Lcom/facebook/imagepipeline/memory/ab;->SY:Lcom/facebook/common/g/b;

    .line 91
    iget-object v2, p0, Lcom/facebook/imagepipeline/memory/ac;->VP:Lcom/facebook/imagepipeline/memory/ab;

    .line 6112
    iget-object v2, v2, Lcom/facebook/imagepipeline/memory/ab;->VG:Lcom/facebook/imagepipeline/memory/ad;

    .line 92
    invoke-direct {v0, v1, v2}, Lcom/facebook/imagepipeline/memory/n;-><init>(Lcom/facebook/common/g/b;Lcom/facebook/imagepipeline/memory/ad;)V

    iput-object v0, p0, Lcom/facebook/imagepipeline/memory/ac;->QS:Lcom/facebook/imagepipeline/memory/n;

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/facebook/imagepipeline/memory/ac;->QS:Lcom/facebook/imagepipeline/memory/n;

    return-object v0
.end method

.method public final ja()I
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/facebook/imagepipeline/memory/ac;->VP:Lcom/facebook/imagepipeline/memory/ab;

    .line 7112
    iget-object v0, v0, Lcom/facebook/imagepipeline/memory/ab;->VG:Lcom/facebook/imagepipeline/memory/ad;

    .line 98
    iget v0, v0, Lcom/facebook/imagepipeline/memory/ad;->VZ:I

    return v0
.end method

.method public final jc()Lcom/facebook/common/g/j;
    .locals 2

    .line 126
    iget-object v0, p0, Lcom/facebook/imagepipeline/memory/ac;->Rg:Lcom/facebook/common/g/j;

    if-nez v0, :cond_0

    .line 127
    new-instance v0, Lcom/facebook/common/g/j;

    invoke-virtual {p0}, Lcom/facebook/imagepipeline/memory/ac;->jd()Lcom/facebook/common/g/a;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/common/g/j;-><init>(Lcom/facebook/common/g/a;)V

    iput-object v0, p0, Lcom/facebook/imagepipeline/memory/ac;->Rg:Lcom/facebook/common/g/j;

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/facebook/imagepipeline/memory/ac;->Rg:Lcom/facebook/common/g/j;

    return-object v0
.end method

.method public final jd()Lcom/facebook/common/g/a;
    .locals 4

    .line 142
    iget-object v0, p0, Lcom/facebook/imagepipeline/memory/ac;->VS:Lcom/facebook/common/g/a;

    if-nez v0, :cond_0

    .line 143
    new-instance v0, Lcom/facebook/imagepipeline/memory/o;

    iget-object v1, p0, Lcom/facebook/imagepipeline/memory/ac;->VP:Lcom/facebook/imagepipeline/memory/ab;

    .line 9100
    iget-object v1, v1, Lcom/facebook/imagepipeline/memory/ab;->SY:Lcom/facebook/common/g/b;

    .line 144
    iget-object v2, p0, Lcom/facebook/imagepipeline/memory/ac;->VP:Lcom/facebook/imagepipeline/memory/ab;

    .line 9116
    iget-object v2, v2, Lcom/facebook/imagepipeline/memory/ab;->VJ:Lcom/facebook/imagepipeline/memory/ad;

    .line 145
    iget-object v3, p0, Lcom/facebook/imagepipeline/memory/ac;->VP:Lcom/facebook/imagepipeline/memory/ab;

    .line 9120
    iget-object v3, v3, Lcom/facebook/imagepipeline/memory/ab;->VK:Lcom/facebook/imagepipeline/memory/ae;

    .line 146
    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/imagepipeline/memory/o;-><init>(Lcom/facebook/common/g/b;Lcom/facebook/imagepipeline/memory/ad;Lcom/facebook/imagepipeline/memory/ae;)V

    iput-object v0, p0, Lcom/facebook/imagepipeline/memory/ac;->VS:Lcom/facebook/common/g/a;

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/facebook/imagepipeline/memory/ac;->VS:Lcom/facebook/common/g/a;

    return-object v0
.end method
