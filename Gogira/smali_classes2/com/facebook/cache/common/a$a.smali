.class public final Lcom/facebook/cache/common/a$a;
.super Ljava/lang/Enum;
.source "CacheErrorLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/cache/common/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/facebook/cache/common/a$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final EW:I = 0x1

.field public static final EX:I = 0x2

.field public static final EY:I = 0x3

.field public static final EZ:I = 0x4

.field public static final Fa:I = 0x5

.field public static final Fb:I = 0x6

.field public static final Fc:I = 0x7

.field public static final Fd:I = 0x8

.field public static final Fe:I = 0x9

.field public static final Ff:I = 0xa

.field public static final Fg:I = 0xb

.field public static final Fh:I = 0xc

.field public static final Fi:I = 0xd

.field public static final Fj:I = 0xe

.field public static final Fk:I = 0xf

.field public static final Fl:I = 0x10

.field public static final Fm:I = 0x11

.field private static final synthetic Fn:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x11

    new-array v0, v0, [I

    .line 20
    sget v1, Lcom/facebook/cache/common/a$a;->EW:I

    const/4 v2, 0x0

    aput v1, v0, v2

    sget v1, Lcom/facebook/cache/common/a$a;->EX:I

    const/4 v2, 0x1

    aput v1, v0, v2

    sget v1, Lcom/facebook/cache/common/a$a;->EY:I

    const/4 v2, 0x2

    aput v1, v0, v2

    sget v1, Lcom/facebook/cache/common/a$a;->EZ:I

    const/4 v2, 0x3

    aput v1, v0, v2

    sget v1, Lcom/facebook/cache/common/a$a;->Fa:I

    const/4 v2, 0x4

    aput v1, v0, v2

    sget v1, Lcom/facebook/cache/common/a$a;->Fb:I

    const/4 v2, 0x5

    aput v1, v0, v2

    sget v1, Lcom/facebook/cache/common/a$a;->Fc:I

    const/4 v2, 0x6

    aput v1, v0, v2

    sget v1, Lcom/facebook/cache/common/a$a;->Fd:I

    const/4 v2, 0x7

    aput v1, v0, v2

    sget v1, Lcom/facebook/cache/common/a$a;->Fe:I

    const/16 v2, 0x8

    aput v1, v0, v2

    sget v1, Lcom/facebook/cache/common/a$a;->Ff:I

    const/16 v2, 0x9

    aput v1, v0, v2

    sget v1, Lcom/facebook/cache/common/a$a;->Fg:I

    const/16 v2, 0xa

    aput v1, v0, v2

    sget v1, Lcom/facebook/cache/common/a$a;->Fh:I

    const/16 v2, 0xb

    aput v1, v0, v2

    sget v1, Lcom/facebook/cache/common/a$a;->Fi:I

    const/16 v2, 0xc

    aput v1, v0, v2

    sget v1, Lcom/facebook/cache/common/a$a;->Fj:I

    const/16 v2, 0xd

    aput v1, v0, v2

    sget v1, Lcom/facebook/cache/common/a$a;->Fk:I

    const/16 v2, 0xe

    aput v1, v0, v2

    sget v1, Lcom/facebook/cache/common/a$a;->Fl:I

    const/16 v2, 0xf

    aput v1, v0, v2

    sget v1, Lcom/facebook/cache/common/a$a;->Fm:I

    const/16 v2, 0x10

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/cache/common/a$a;->Fn:[I

    return-void
.end method
