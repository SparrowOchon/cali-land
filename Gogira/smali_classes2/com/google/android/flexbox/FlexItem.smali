.class interface abstract Lcom/google/android/flexbox/FlexItem;
.super Ljava/lang/Object;
.source "FlexItem.java"

# interfaces
.implements Landroid/os/Parcelable;


# virtual methods
.method public abstract getHeight()I
.end method

.method public abstract getMaxHeight()I
.end method

.method public abstract getMaxWidth()I
.end method

.method public abstract getMinHeight()I
.end method

.method public abstract getMinWidth()I
.end method

.method public abstract getOrder()I
.end method

.method public abstract getWidth()I
.end method

.method public abstract kq()F
.end method

.method public abstract kr()F
.end method

.method public abstract ks()I
.end method

.method public abstract kt()Z
.end method

.method public abstract ku()F
.end method

.method public abstract kv()I
.end method

.method public abstract kw()I
.end method

.method public abstract kx()I
.end method

.method public abstract ky()I
.end method
