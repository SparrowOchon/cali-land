.class public final Lcom/google/android/gms/signin/a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/api/a$d$e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/signin/a$a;
    }
.end annotation


# static fields
.field public static final aOH:Lcom/google/android/gms/signin/a;


# instance fields
.field public final aOI:Z

.field public final aOJ:Z

.field public final aOK:Ljava/lang/String;

.field public final aOL:Z

.field public final aOM:Ljava/lang/String;

.field public final aON:Z

.field public final aOO:Ljava/lang/Long;

.field public final aOP:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 19
    new-instance v0, Lcom/google/android/gms/signin/a$a;

    invoke-direct {v0}, Lcom/google/android/gms/signin/a$a;-><init>()V

    .line 20
    new-instance v0, Lcom/google/android/gms/signin/a;

    .line 21
    invoke-direct {v0}, Lcom/google/android/gms/signin/a;-><init>()V

    .line 22
    sput-object v0, Lcom/google/android/gms/signin/a;->aOH:Lcom/google/android/gms/signin/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/google/android/gms/signin/a;->aOI:Z

    .line 3
    iput-boolean v0, p0, Lcom/google/android/gms/signin/a;->aOJ:Z

    const/4 v1, 0x0

    .line 4
    iput-object v1, p0, Lcom/google/android/gms/signin/a;->aOK:Ljava/lang/String;

    .line 5
    iput-boolean v0, p0, Lcom/google/android/gms/signin/a;->aOL:Z

    .line 6
    iput-boolean v0, p0, Lcom/google/android/gms/signin/a;->aON:Z

    .line 7
    iput-object v1, p0, Lcom/google/android/gms/signin/a;->aOM:Ljava/lang/String;

    .line 8
    iput-object v1, p0, Lcom/google/android/gms/signin/a;->aOO:Ljava/lang/Long;

    .line 9
    iput-object v1, p0, Lcom/google/android/gms/signin/a;->aOP:Ljava/lang/Long;

    return-void
.end method
