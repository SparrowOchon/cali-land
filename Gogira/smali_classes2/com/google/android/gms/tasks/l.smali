.class final Lcom/google/android/gms/tasks/l;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final synthetic aPw:Lcom/google/android/gms/tasks/Task;

.field private final synthetic aPx:Lcom/google/android/gms/tasks/k;


# direct methods
.method constructor <init>(Lcom/google/android/gms/tasks/k;Lcom/google/android/gms/tasks/Task;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/tasks/l;->aPx:Lcom/google/android/gms/tasks/k;

    iput-object p2, p0, Lcom/google/android/gms/tasks/l;->aPw:Lcom/google/android/gms/tasks/Task;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/google/android/gms/tasks/l;->aPw:Lcom/google/android/gms/tasks/Task;

    invoke-virtual {v0}, Lcom/google/android/gms/tasks/Task;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/google/android/gms/tasks/l;->aPx:Lcom/google/android/gms/tasks/k;

    .line 1009
    iget-object v0, v0, Lcom/google/android/gms/tasks/k;->aPv:Lcom/google/android/gms/tasks/ac;

    .line 3
    invoke-virtual {v0}, Lcom/google/android/gms/tasks/ac;->zQ()Z

    return-void

    .line 5
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/tasks/l;->aPx:Lcom/google/android/gms/tasks/k;

    .line 1010
    iget-object v0, v0, Lcom/google/android/gms/tasks/k;->aPu:Lcom/google/android/gms/tasks/a;

    .line 5
    iget-object v1, p0, Lcom/google/android/gms/tasks/l;->aPw:Lcom/google/android/gms/tasks/Task;

    invoke-interface {v0, v1}, Lcom/google/android/gms/tasks/a;->b(Lcom/google/android/gms/tasks/Task;)Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catch Lcom/google/android/gms/tasks/f; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 15
    iget-object v1, p0, Lcom/google/android/gms/tasks/l;->aPx:Lcom/google/android/gms/tasks/k;

    .line 5009
    iget-object v1, v1, Lcom/google/android/gms/tasks/k;->aPv:Lcom/google/android/gms/tasks/ac;

    .line 15
    invoke-virtual {v1, v0}, Lcom/google/android/gms/tasks/ac;->k(Ljava/lang/Object;)V

    return-void

    :catch_0
    move-exception v0

    .line 13
    iget-object v1, p0, Lcom/google/android/gms/tasks/l;->aPx:Lcom/google/android/gms/tasks/k;

    .line 4009
    iget-object v1, v1, Lcom/google/android/gms/tasks/k;->aPv:Lcom/google/android/gms/tasks/ac;

    .line 13
    invoke-virtual {v1, v0}, Lcom/google/android/gms/tasks/ac;->f(Ljava/lang/Exception;)V

    return-void

    :catch_1
    move-exception v0

    .line 8
    invoke-virtual {v0}, Lcom/google/android/gms/tasks/f;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/Exception;

    if-eqz v1, :cond_1

    .line 9
    iget-object v1, p0, Lcom/google/android/gms/tasks/l;->aPx:Lcom/google/android/gms/tasks/k;

    .line 2009
    iget-object v1, v1, Lcom/google/android/gms/tasks/k;->aPv:Lcom/google/android/gms/tasks/ac;

    .line 9
    invoke-virtual {v0}, Lcom/google/android/gms/tasks/f;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/lang/Exception;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/tasks/ac;->f(Ljava/lang/Exception;)V

    return-void

    .line 10
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/tasks/l;->aPx:Lcom/google/android/gms/tasks/k;

    .line 3009
    iget-object v1, v1, Lcom/google/android/gms/tasks/k;->aPv:Lcom/google/android/gms/tasks/ac;

    .line 10
    invoke-virtual {v1, v0}, Lcom/google/android/gms/tasks/ac;->f(Ljava/lang/Exception;)V

    return-void
.end method
