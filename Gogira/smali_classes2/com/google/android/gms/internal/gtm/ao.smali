.class abstract Lcom/google/android/gms/internal/gtm/ao;
.super Ljava/lang/Object;


# static fields
.field private static volatile handler:Landroid/os/Handler;


# instance fields
.field private final amA:Lcom/google/android/gms/internal/gtm/m;

.field private final anQ:Ljava/lang/Runnable;

.field private volatile anR:J


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/gtm/m;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p1}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3
    iput-object p1, p0, Lcom/google/android/gms/internal/gtm/ao;->amA:Lcom/google/android/gms/internal/gtm/m;

    .line 4
    new-instance p1, Lcom/google/android/gms/internal/gtm/ap;

    invoke-direct {p1, p0}, Lcom/google/android/gms/internal/gtm/ap;-><init>(Lcom/google/android/gms/internal/gtm/ao;)V

    iput-object p1, p0, Lcom/google/android/gms/internal/gtm/ao;->anQ:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/gtm/ao;)Lcom/google/android/gms/internal/gtm/m;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/google/android/gms/internal/gtm/ao;->amA:Lcom/google/android/gms/internal/gtm/m;

    return-object p0
.end method

.method static synthetic b(Lcom/google/android/gms/internal/gtm/ao;)J
    .locals 2

    const-wide/16 v0, 0x0

    .line 42
    iput-wide v0, p0, Lcom/google/android/gms/internal/gtm/ao;->anR:J

    return-wide v0
.end method

.method private final getHandler()Landroid/os/Handler;
    .locals 3

    .line 34
    sget-object v0, Lcom/google/android/gms/internal/gtm/ao;->handler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 35
    sget-object v0, Lcom/google/android/gms/internal/gtm/ao;->handler:Landroid/os/Handler;

    return-object v0

    .line 36
    :cond_0
    const-class v0, Lcom/google/android/gms/internal/gtm/ao;

    monitor-enter v0

    .line 37
    :try_start_0
    sget-object v1, Lcom/google/android/gms/internal/gtm/ao;->handler:Landroid/os/Handler;

    if-nez v1, :cond_1

    .line 38
    new-instance v1, Lcom/google/android/gms/internal/gtm/bv;

    iget-object v2, p0, Lcom/google/android/gms/internal/gtm/ao;->amA:Lcom/google/android/gms/internal/gtm/m;

    .line 4091
    iget-object v2, v2, Lcom/google/android/gms/internal/gtm/m;->acS:Landroid/content/Context;

    .line 38
    invoke-virtual {v2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/internal/gtm/bv;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lcom/google/android/gms/internal/gtm/ao;->handler:Landroid/os/Handler;

    .line 39
    :cond_1
    sget-object v1, Lcom/google/android/gms/internal/gtm/ao;->handler:Landroid/os/Handler;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 40
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method public final cancel()V
    .locals 2

    const-wide/16 v0, 0x0

    .line 31
    iput-wide v0, p0, Lcom/google/android/gms/internal/gtm/ao;->anR:J

    .line 32
    invoke-direct {p0}, Lcom/google/android/gms/internal/gtm/ao;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/gtm/ao;->anQ:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final nP()J
    .locals 5

    .line 27
    iget-wide v0, p0, Lcom/google/android/gms/internal/gtm/ao;->anR:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-wide v2

    .line 29
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/gtm/ao;->amA:Lcom/google/android/gms/internal/gtm/m;

    .line 3093
    iget-object v0, v0, Lcom/google/android/gms/internal/gtm/m;->acE:Lcom/google/android/gms/common/util/e;

    .line 29
    invoke-interface {v0}, Lcom/google/android/gms/common/util/e;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/gms/internal/gtm/ao;->anR:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final nQ()Z
    .locals 5

    .line 30
    iget-wide v0, p0, Lcom/google/android/gms/internal/gtm/ao;->anR:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public abstract run()V
.end method

.method public final w(J)V
    .locals 3

    .line 6
    invoke-virtual {p0}, Lcom/google/android/gms/internal/gtm/ao;->cancel()V

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-ltz v2, :cond_0

    .line 8
    iget-object v0, p0, Lcom/google/android/gms/internal/gtm/ao;->amA:Lcom/google/android/gms/internal/gtm/m;

    .line 1093
    iget-object v0, v0, Lcom/google/android/gms/internal/gtm/m;->acE:Lcom/google/android/gms/common/util/e;

    .line 8
    invoke-interface {v0}, Lcom/google/android/gms/common/util/e;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/internal/gtm/ao;->anR:J

    .line 9
    invoke-direct {p0}, Lcom/google/android/gms/internal/gtm/ao;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/gtm/ao;->anQ:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 11
    iget-object v0, p0, Lcom/google/android/gms/internal/gtm/ao;->amA:Lcom/google/android/gms/internal/gtm/m;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gtm/m;->na()Lcom/google/android/gms/internal/gtm/bd;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    const-string p2, "Failed to schedule delayed post. time"

    invoke-virtual {v0, p2, p1}, Lcom/google/android/gms/internal/gtm/j;->m(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final x(J)V
    .locals 6

    .line 13
    invoke-virtual {p0}, Lcom/google/android/gms/internal/gtm/ao;->nQ()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-gez v2, :cond_1

    .line 16
    invoke-virtual {p0}, Lcom/google/android/gms/internal/gtm/ao;->cancel()V

    return-void

    .line 18
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/internal/gtm/ao;->amA:Lcom/google/android/gms/internal/gtm/m;

    .line 2093
    iget-object v2, v2, Lcom/google/android/gms/internal/gtm/m;->acE:Lcom/google/android/gms/common/util/e;

    .line 18
    invoke-interface {v2}, Lcom/google/android/gms/common/util/e;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/gms/internal/gtm/ao;->anR:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    sub-long/2addr p1, v2

    cmp-long v2, p1, v0

    if-gez v2, :cond_2

    move-wide p1, v0

    .line 22
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/internal/gtm/ao;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/gtm/ao;->anQ:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 23
    invoke-direct {p0}, Lcom/google/android/gms/internal/gtm/ao;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/gtm/ao;->anQ:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move-result v0

    if-nez v0, :cond_3

    .line 25
    iget-object v0, p0, Lcom/google/android/gms/internal/gtm/ao;->amA:Lcom/google/android/gms/internal/gtm/m;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gtm/m;->na()Lcom/google/android/gms/internal/gtm/bd;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    const-string p2, "Failed to adjust delayed post. time"

    invoke-virtual {v0, p2, p1}, Lcom/google/android/gms/internal/gtm/j;->m(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_3
    return-void
.end method
