.class public final Lcom/google/android/gms/internal/gtm/au;
.super Ljava/lang/Object;


# static fields
.field private static anX:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static anY:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static anZ:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static aoA:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static aoB:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static aoC:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static aoD:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static aoE:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static aoF:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static aoG:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static aoH:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static aoI:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static aoJ:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static aoK:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static aoL:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static aoM:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static aoN:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static aoO:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static aoa:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static aob:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static aoc:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static aod:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static aoe:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static aof:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static aog:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static aoh:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static aoi:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static aoj:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static aok:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static aol:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static aom:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static aon:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static aoo:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static aop:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static aoq:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static aor:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static aos:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static aot:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static aou:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static aov:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static aow:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static aox:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static aoy:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static aoz:Lcom/google/android/gms/internal/gtm/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gtm/av<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 12

    const/4 v0, 0x0

    const-string v1, "analytics.service_enabled"

    .line 2
    invoke-static {v1, v0, v0}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;ZZ)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v1

    .line 3
    sput-object v1, Lcom/google/android/gms/internal/gtm/au;->anX:Lcom/google/android/gms/internal/gtm/av;

    const/4 v1, 0x1

    const-string v2, "analytics.service_client_enabled"

    .line 5
    invoke-static {v2, v1, v1}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;ZZ)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v1

    .line 6
    sput-object v1, Lcom/google/android/gms/internal/gtm/au;->anY:Lcom/google/android/gms/internal/gtm/av;

    const-string v1, "analytics.log_tag"

    const-string v2, "GAv4"

    const-string v3, "GAv4-SVC"

    .line 7
    invoke-static {v1, v2, v3}, Lcom/google/android/gms/internal/gtm/av;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v1

    sput-object v1, Lcom/google/android/gms/internal/gtm/au;->anZ:Lcom/google/android/gms/internal/gtm/av;

    const-wide/16 v1, 0x3c

    const-string v3, "analytics.max_tokens"

    .line 9
    invoke-static {v3, v1, v2, v1, v2}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v1

    .line 10
    sput-object v1, Lcom/google/android/gms/internal/gtm/au;->aoa:Lcom/google/android/gms/internal/gtm/av;

    .line 1010
    new-instance v1, Lcom/google/android/gms/internal/gtm/av;

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    const-string v3, "analytics.tokens_per_sec"

    invoke-static {v3, v2}, Lcom/google/android/gms/common/a/a;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/gms/common/a/a;

    move-result-object v3

    invoke-direct {v1, v3, v2}, Lcom/google/android/gms/internal/gtm/av;-><init>(Lcom/google/android/gms/common/a/a;Ljava/lang/Object;)V

    .line 13
    sput-object v1, Lcom/google/android/gms/internal/gtm/au;->aob:Lcom/google/android/gms/internal/gtm/av;

    const/16 v1, 0x7d0

    const-string v2, "analytics.max_stored_hits"

    const/16 v3, 0x4e20

    .line 15
    invoke-static {v2, v1, v3}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;II)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v2

    sput-object v2, Lcom/google/android/gms/internal/gtm/au;->aoc:Lcom/google/android/gms/internal/gtm/av;

    const-string v2, "analytics.max_stored_hits_per_app"

    .line 17
    invoke-static {v2, v1, v1}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;II)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v1

    .line 18
    sput-object v1, Lcom/google/android/gms/internal/gtm/au;->aod:Lcom/google/android/gms/internal/gtm/av;

    const/16 v1, 0x64

    const-string v2, "analytics.max_stored_properties_per_app"

    .line 20
    invoke-static {v2, v1, v1}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;II)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v1

    .line 21
    sput-object v1, Lcom/google/android/gms/internal/gtm/au;->aoe:Lcom/google/android/gms/internal/gtm/av;

    const-wide/32 v1, 0x1d4c0

    const-wide/32 v3, 0x1b7740

    const-string v5, "analytics.local_dispatch_millis"

    .line 23
    invoke-static {v5, v3, v4, v1, v2}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v5

    sput-object v5, Lcom/google/android/gms/internal/gtm/au;->aof:Lcom/google/android/gms/internal/gtm/av;

    const-wide/16 v5, 0x1388

    const-string v7, "analytics.initial_local_dispatch_millis"

    .line 25
    invoke-static {v7, v5, v6, v5, v6}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v7

    sput-object v7, Lcom/google/android/gms/internal/gtm/au;->aog:Lcom/google/android/gms/internal/gtm/av;

    const-string v7, "analytics.min_local_dispatch_millis"

    .line 27
    invoke-static {v7, v1, v2, v1, v2}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v1

    .line 28
    sput-object v1, Lcom/google/android/gms/internal/gtm/au;->aoh:Lcom/google/android/gms/internal/gtm/av;

    const-wide/32 v1, 0x6ddd00

    const-string v7, "analytics.max_local_dispatch_millis"

    .line 30
    invoke-static {v7, v1, v2, v1, v2}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v7

    .line 31
    sput-object v7, Lcom/google/android/gms/internal/gtm/au;->aoi:Lcom/google/android/gms/internal/gtm/av;

    const-string v7, "analytics.dispatch_alarm_millis"

    .line 33
    invoke-static {v7, v1, v2, v1, v2}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v1

    .line 34
    sput-object v1, Lcom/google/android/gms/internal/gtm/au;->aoj:Lcom/google/android/gms/internal/gtm/av;

    const-wide/32 v1, 0x1ee6280

    const-string v7, "analytics.max_dispatch_alarm_millis"

    .line 36
    invoke-static {v7, v1, v2, v1, v2}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v1

    .line 37
    sput-object v1, Lcom/google/android/gms/internal/gtm/au;->aok:Lcom/google/android/gms/internal/gtm/av;

    const/16 v1, 0x14

    const-string v2, "analytics.max_hits_per_dispatch"

    .line 39
    invoke-static {v2, v1, v1}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;II)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v2

    .line 40
    sput-object v2, Lcom/google/android/gms/internal/gtm/au;->aol:Lcom/google/android/gms/internal/gtm/av;

    const-string v2, "analytics.max_hits_per_batch"

    .line 42
    invoke-static {v2, v1, v1}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;II)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v2

    .line 43
    sput-object v2, Lcom/google/android/gms/internal/gtm/au;->aom:Lcom/google/android/gms/internal/gtm/av;

    const-string v2, "analytics.insecure_host"

    const-string v7, "http://www.google-analytics.com"

    .line 46
    invoke-static {v2, v7, v7}, Lcom/google/android/gms/internal/gtm/av;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v2

    .line 47
    sput-object v2, Lcom/google/android/gms/internal/gtm/au;->aon:Lcom/google/android/gms/internal/gtm/av;

    const-string v2, "analytics.secure_host"

    const-string v7, "https://ssl.google-analytics.com"

    .line 50
    invoke-static {v2, v7, v7}, Lcom/google/android/gms/internal/gtm/av;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v2

    .line 51
    sput-object v2, Lcom/google/android/gms/internal/gtm/au;->aoo:Lcom/google/android/gms/internal/gtm/av;

    const-string v2, "analytics.simple_endpoint"

    const-string v7, "/collect"

    .line 53
    invoke-static {v2, v7, v7}, Lcom/google/android/gms/internal/gtm/av;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v2

    .line 54
    sput-object v2, Lcom/google/android/gms/internal/gtm/au;->aop:Lcom/google/android/gms/internal/gtm/av;

    const-string v2, "analytics.batching_endpoint"

    const-string v7, "/batch"

    .line 56
    invoke-static {v2, v7, v7}, Lcom/google/android/gms/internal/gtm/av;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v2

    .line 57
    sput-object v2, Lcom/google/android/gms/internal/gtm/au;->aoq:Lcom/google/android/gms/internal/gtm/av;

    const/16 v2, 0x7f4

    const-string v7, "analytics.max_get_length"

    .line 59
    invoke-static {v7, v2, v2}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;II)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v2

    .line 60
    sput-object v2, Lcom/google/android/gms/internal/gtm/au;->aor:Lcom/google/android/gms/internal/gtm/av;

    .line 61
    sget-object v2, Lcom/google/android/gms/internal/gtm/ad;->anC:Lcom/google/android/gms/internal/gtm/ad;

    .line 62
    invoke-virtual {v2}, Lcom/google/android/gms/internal/gtm/ad;->name()Ljava/lang/String;

    move-result-object v2

    sget-object v7, Lcom/google/android/gms/internal/gtm/ad;->anC:Lcom/google/android/gms/internal/gtm/ad;

    .line 63
    invoke-virtual {v7}, Lcom/google/android/gms/internal/gtm/ad;->name()Ljava/lang/String;

    move-result-object v7

    const-string v8, "analytics.batching_strategy.k"

    .line 64
    invoke-static {v8, v2, v7}, Lcom/google/android/gms/internal/gtm/av;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v2

    sput-object v2, Lcom/google/android/gms/internal/gtm/au;->aos:Lcom/google/android/gms/internal/gtm/av;

    .line 65
    sget-object v2, Lcom/google/android/gms/internal/gtm/ai;->anJ:Lcom/google/android/gms/internal/gtm/ai;

    .line 66
    invoke-virtual {v2}, Lcom/google/android/gms/internal/gtm/ai;->name()Ljava/lang/String;

    move-result-object v2

    const-string v7, "analytics.compression_strategy.k"

    .line 68
    invoke-static {v7, v2, v2}, Lcom/google/android/gms/internal/gtm/av;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v2

    .line 69
    sput-object v2, Lcom/google/android/gms/internal/gtm/au;->aot:Lcom/google/android/gms/internal/gtm/av;

    const-string v2, "analytics.max_hits_per_request.k"

    .line 71
    invoke-static {v2, v1, v1}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;II)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v1

    .line 72
    sput-object v1, Lcom/google/android/gms/internal/gtm/au;->aou:Lcom/google/android/gms/internal/gtm/av;

    const/16 v1, 0x2000

    const-string v2, "analytics.max_hit_length.k"

    .line 74
    invoke-static {v2, v1, v1}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;II)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v2

    .line 75
    sput-object v2, Lcom/google/android/gms/internal/gtm/au;->aov:Lcom/google/android/gms/internal/gtm/av;

    const-string v2, "analytics.max_post_length.k"

    .line 77
    invoke-static {v2, v1, v1}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;II)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v2

    .line 78
    sput-object v2, Lcom/google/android/gms/internal/gtm/au;->aow:Lcom/google/android/gms/internal/gtm/av;

    const-string v2, "analytics.max_batch_post_length"

    .line 80
    invoke-static {v2, v1, v1}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;II)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v1

    .line 81
    sput-object v1, Lcom/google/android/gms/internal/gtm/au;->aox:Lcom/google/android/gms/internal/gtm/av;

    const-string v1, "analytics.fallback_responses.k"

    const-string v2, "404,502"

    .line 84
    invoke-static {v1, v2, v2}, Lcom/google/android/gms/internal/gtm/av;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v1

    .line 85
    sput-object v1, Lcom/google/android/gms/internal/gtm/au;->aoy:Lcom/google/android/gms/internal/gtm/av;

    const-string v1, "analytics.batch_retry_interval.seconds.k"

    const/16 v2, 0xe10

    const/16 v7, 0xe10

    .line 87
    invoke-static {v1, v2, v7}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;II)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v1

    .line 88
    sput-object v1, Lcom/google/android/gms/internal/gtm/au;->aoz:Lcom/google/android/gms/internal/gtm/av;

    const-wide/32 v1, 0x5265c00

    const-string v7, "analytics.service_monitor_interval"

    .line 90
    invoke-static {v7, v1, v2, v1, v2}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v7

    .line 91
    sput-object v7, Lcom/google/android/gms/internal/gtm/au;->aoA:Lcom/google/android/gms/internal/gtm/av;

    const-string v7, "analytics.http_connection.connect_timeout_millis"

    const v8, 0xea60

    const v9, 0xea60

    .line 93
    invoke-static {v7, v8, v9}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;II)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v7

    .line 94
    sput-object v7, Lcom/google/android/gms/internal/gtm/au;->aoB:Lcom/google/android/gms/internal/gtm/av;

    const-string v7, "analytics.http_connection.read_timeout_millis"

    const v8, 0xee48

    const v9, 0xee48

    .line 96
    invoke-static {v7, v8, v9}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;II)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v7

    .line 97
    sput-object v7, Lcom/google/android/gms/internal/gtm/au;->aoC:Lcom/google/android/gms/internal/gtm/av;

    const-string v7, "analytics.campaigns.time_limit"

    .line 99
    invoke-static {v7, v1, v2, v1, v2}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v7

    .line 100
    sput-object v7, Lcom/google/android/gms/internal/gtm/au;->aoD:Lcom/google/android/gms/internal/gtm/av;

    const-string v7, "analytics.first_party_experiment_id"

    const-string v8, ""

    .line 103
    invoke-static {v7, v8, v8}, Lcom/google/android/gms/internal/gtm/av;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v7

    .line 104
    sput-object v7, Lcom/google/android/gms/internal/gtm/au;->aoE:Lcom/google/android/gms/internal/gtm/av;

    const-string v7, "analytics.first_party_experiment_variant"

    .line 106
    invoke-static {v7, v0, v0}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;II)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v7

    .line 107
    sput-object v7, Lcom/google/android/gms/internal/gtm/au;->aoF:Lcom/google/android/gms/internal/gtm/av;

    const-string v7, "analytics.test.disable_receiver"

    .line 109
    invoke-static {v7, v0, v0}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;ZZ)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v7

    .line 110
    sput-object v7, Lcom/google/android/gms/internal/gtm/au;->aoG:Lcom/google/android/gms/internal/gtm/av;

    const-string v7, "analytics.service_client.idle_disconnect_millis"

    const-wide/16 v8, 0x2710

    const-wide/16 v10, 0x2710

    .line 112
    invoke-static {v7, v8, v9, v10, v11}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v7

    sput-object v7, Lcom/google/android/gms/internal/gtm/au;->aoH:Lcom/google/android/gms/internal/gtm/av;

    const-string v7, "analytics.service_client.connect_timeout_millis"

    .line 114
    invoke-static {v7, v5, v6, v5, v6}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v7

    .line 115
    sput-object v7, Lcom/google/android/gms/internal/gtm/au;->aoI:Lcom/google/android/gms/internal/gtm/av;

    const-string v7, "analytics.service_client.second_connect_delay_millis"

    .line 117
    invoke-static {v7, v5, v6, v5, v6}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v7

    .line 118
    sput-object v7, Lcom/google/android/gms/internal/gtm/au;->aoJ:Lcom/google/android/gms/internal/gtm/av;

    const-string v7, "analytics.service_client.unexpected_reconnect_millis"

    const-wide/32 v8, 0xea60

    const-wide/32 v10, 0xea60

    .line 120
    invoke-static {v7, v8, v9, v10, v11}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v7

    .line 121
    sput-object v7, Lcom/google/android/gms/internal/gtm/au;->aoK:Lcom/google/android/gms/internal/gtm/av;

    const-string v7, "analytics.service_client.reconnect_throttle_millis"

    .line 123
    invoke-static {v7, v3, v4, v3, v4}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v3

    .line 124
    sput-object v3, Lcom/google/android/gms/internal/gtm/au;->aoL:Lcom/google/android/gms/internal/gtm/av;

    const-string v3, "analytics.monitoring.sample_period_millis"

    .line 126
    invoke-static {v3, v1, v2, v1, v2}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v1

    .line 127
    sput-object v1, Lcom/google/android/gms/internal/gtm/au;->aoM:Lcom/google/android/gms/internal/gtm/av;

    const-string v1, "analytics.initialization_warning_threshold"

    .line 129
    invoke-static {v1, v5, v6, v5, v6}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v1

    .line 130
    sput-object v1, Lcom/google/android/gms/internal/gtm/au;->aoN:Lcom/google/android/gms/internal/gtm/av;

    const-string v1, "analytics.gcm_task_service"

    .line 132
    invoke-static {v1, v0, v0}, Lcom/google/android/gms/internal/gtm/av;->a(Ljava/lang/String;ZZ)Lcom/google/android/gms/internal/gtm/av;

    move-result-object v0

    .line 133
    sput-object v0, Lcom/google/android/gms/internal/gtm/au;->aoO:Lcom/google/android/gms/internal/gtm/av;

    return-void
.end method
