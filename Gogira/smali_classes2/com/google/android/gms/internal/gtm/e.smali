.class public final Lcom/google/android/gms/internal/gtm/e;
.super Lcom/google/android/gms/internal/gtm/k;


# instance fields
.field final amu:Lcom/google/android/gms/internal/gtm/y;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/gtm/m;Lcom/google/android/gms/internal/gtm/o;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/gtm/k;-><init>(Lcom/google/android/gms/internal/gtm/m;)V

    .line 2
    invoke-static {p2}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    new-instance v0, Lcom/google/android/gms/internal/gtm/y;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/internal/gtm/y;-><init>(Lcom/google/android/gms/internal/gtm/m;Lcom/google/android/gms/internal/gtm/o;)V

    .line 5
    iput-object v0, p0, Lcom/google/android/gms/internal/gtm/e;->amu:Lcom/google/android/gms/internal/gtm/y;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/internal/gtm/p;)J
    .locals 5

    .line 16
    invoke-virtual {p0}, Lcom/google/android/gms/internal/gtm/k;->mY()V

    .line 17
    invoke-static {p1}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    invoke-static {}, Lcom/google/android/gms/analytics/m;->kP()V

    .line 19
    iget-object v0, p0, Lcom/google/android/gms/internal/gtm/e;->amu:Lcom/google/android/gms/internal/gtm/y;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/gtm/y;->c(Lcom/google/android/gms/internal/gtm/p;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    .line 21
    iget-object v2, p0, Lcom/google/android/gms/internal/gtm/e;->amu:Lcom/google/android/gms/internal/gtm/y;

    invoke-virtual {v2, p1}, Lcom/google/android/gms/internal/gtm/y;->b(Lcom/google/android/gms/internal/gtm/p;)V

    :cond_0
    return-wide v0
.end method

.method public final a(Lcom/google/android/gms/internal/gtm/as;)V
    .locals 2

    .line 33
    invoke-virtual {p0}, Lcom/google/android/gms/internal/gtm/k;->mY()V

    .line 2010
    iget-object v0, p0, Lcom/google/android/gms/internal/gtm/j;->amA:Lcom/google/android/gms/internal/gtm/m;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gtm/m;->nb()Lcom/google/android/gms/analytics/m;

    move-result-object v0

    .line 34
    new-instance v1, Lcom/google/android/gms/internal/gtm/i;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/internal/gtm/i;-><init>(Lcom/google/android/gms/internal/gtm/e;Lcom/google/android/gms/internal/gtm/as;)V

    .line 35
    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/m;->f(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/internal/gtm/az;)V
    .locals 2

    .line 23
    invoke-static {p1}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    invoke-virtual {p0}, Lcom/google/android/gms/internal/gtm/k;->mY()V

    const-string v0, "Hit delivery requested"

    .line 25
    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/internal/gtm/j;->k(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1010
    iget-object v0, p0, Lcom/google/android/gms/internal/gtm/j;->amA:Lcom/google/android/gms/internal/gtm/m;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gtm/m;->nb()Lcom/google/android/gms/analytics/m;

    move-result-object v0

    .line 26
    new-instance v1, Lcom/google/android/gms/internal/gtm/h;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/internal/gtm/h;-><init>(Lcom/google/android/gms/internal/gtm/e;Lcom/google/android/gms/internal/gtm/az;)V

    .line 27
    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/m;->f(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 2

    const-string v0, "campaign param can\'t be empty"

    .line 74
    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/r;->i(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 4010
    iget-object v0, p0, Lcom/google/android/gms/internal/gtm/j;->amA:Lcom/google/android/gms/internal/gtm/m;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gtm/m;->nb()Lcom/google/android/gms/analytics/m;

    move-result-object v0

    .line 75
    new-instance v1, Lcom/google/android/gms/internal/gtm/g;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/gms/internal/gtm/g;-><init>(Lcom/google/android/gms/internal/gtm/e;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 76
    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/m;->f(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected final mK()V
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/google/android/gms/internal/gtm/e;->amu:Lcom/google/android/gms/internal/gtm/y;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gtm/k;->mZ()V

    return-void
.end method

.method public final mQ()V
    .locals 4

    .line 37
    invoke-virtual {p0}, Lcom/google/android/gms/internal/gtm/k;->mY()V

    .line 3007
    iget-object v0, p0, Lcom/google/android/gms/internal/gtm/j;->amA:Lcom/google/android/gms/internal/gtm/m;

    .line 3091
    iget-object v0, v0, Lcom/google/android/gms/internal/gtm/m;->acS:Landroid/content/Context;

    .line 39
    invoke-static {v0}, Lcom/google/android/gms/internal/gtm/bj;->C(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/google/android/gms/internal/gtm/bk;->ad(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 40
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.analytics.ANALYTICS_DISPATCH"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 41
    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.google.android.gms.analytics.AnalyticsService"

    invoke-direct {v2, v0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 42
    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 44
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/gtm/e;->a(Lcom/google/android/gms/internal/gtm/as;)V

    return-void
.end method

.method public final mR()V
    .locals 2

    .line 61
    invoke-virtual {p0}, Lcom/google/android/gms/internal/gtm/k;->mY()V

    .line 62
    invoke-static {}, Lcom/google/android/gms/analytics/m;->kP()V

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/internal/gtm/e;->amu:Lcom/google/android/gms/internal/gtm/y;

    .line 64
    invoke-static {}, Lcom/google/android/gms/analytics/m;->kP()V

    .line 65
    invoke-virtual {v0}, Lcom/google/android/gms/internal/gtm/k;->mY()V

    const-string v1, "Service disconnected"

    .line 66
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/gtm/j;->bd(Ljava/lang/String;)V

    return-void
.end method

.method final mS()V
    .locals 1

    .line 71
    invoke-static {}, Lcom/google/android/gms/analytics/m;->kP()V

    .line 72
    iget-object v0, p0, Lcom/google/android/gms/internal/gtm/e;->amu:Lcom/google/android/gms/internal/gtm/y;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gtm/y;->mS()V

    return-void
.end method

.method final onServiceConnected()V
    .locals 1

    .line 68
    invoke-static {}, Lcom/google/android/gms/analytics/m;->kP()V

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/internal/gtm/e;->amu:Lcom/google/android/gms/internal/gtm/y;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gtm/y;->onServiceConnected()V

    return-void
.end method
