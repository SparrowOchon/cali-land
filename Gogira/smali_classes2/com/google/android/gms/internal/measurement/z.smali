.class final Lcom/google/android/gms/internal/measurement/z;
.super Lcom/google/android/gms/internal/measurement/lw$a;


# instance fields
.field private final synthetic aqA:Lcom/google/android/gms/internal/measurement/jv;

.field private final synthetic aqW:Lcom/google/android/gms/internal/measurement/lw$b;

.field private final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/measurement/lw$b;Landroid/app/Activity;Lcom/google/android/gms/internal/measurement/jv;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/z;->aqW:Lcom/google/android/gms/internal/measurement/lw$b;

    iput-object p2, p0, Lcom/google/android/gms/internal/measurement/z;->val$activity:Landroid/app/Activity;

    iput-object p3, p0, Lcom/google/android/gms/internal/measurement/z;->aqA:Lcom/google/android/gms/internal/measurement/jv;

    iget-object p1, p1, Lcom/google/android/gms/internal/measurement/lw$b;->aqB:Lcom/google/android/gms/internal/measurement/lw;

    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/measurement/lw$a;-><init>(Lcom/google/android/gms/internal/measurement/lw;)V

    return-void
.end method


# virtual methods
.method final ov()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/z;->aqW:Lcom/google/android/gms/internal/measurement/lw$b;

    iget-object v0, v0, Lcom/google/android/gms/internal/measurement/lw$b;->aqB:Lcom/google/android/gms/internal/measurement/lw;

    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/lw;->c(Lcom/google/android/gms/internal/measurement/lw;)Lcom/google/android/gms/internal/measurement/iu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/z;->val$activity:Landroid/app/Activity;

    .line 3
    invoke-static {v1}, Lcom/google/android/gms/a/b;->ab(Ljava/lang/Object;)Lcom/google/android/gms/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/z;->aqA:Lcom/google/android/gms/internal/measurement/jv;

    iget-wide v3, p0, Lcom/google/android/gms/internal/measurement/z;->aBr:J

    .line 4
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/gms/internal/measurement/iu;->onActivitySaveInstanceState(Lcom/google/android/gms/a/a;Lcom/google/android/gms/internal/measurement/ln;J)V

    return-void
.end method
