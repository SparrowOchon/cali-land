.class public final Lcom/google/android/gms/internal/measurement/al$d;
.super Lcom/google/android/gms/internal/measurement/dr;

# interfaces
.implements Lcom/google/android/gms/internal/measurement/fd;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/measurement/al;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/measurement/al$d$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/measurement/dr<",
        "Lcom/google/android/gms/internal/measurement/al$d;",
        "Lcom/google/android/gms/internal/measurement/al$d$a;",
        ">;",
        "Lcom/google/android/gms/internal/measurement/fd;"
    }
.end annotation


# static fields
.field private static volatile zzuo:Lcom/google/android/gms/internal/measurement/fk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/fk<",
            "Lcom/google/android/gms/internal/measurement/al$d;",
            ">;"
        }
    .end annotation
.end field

.field private static final zzwq:Lcom/google/android/gms/internal/measurement/al$d;


# instance fields
.field private zzue:I

.field private zzwk:Ljava/lang/String;

.field private zzwp:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 35
    new-instance v0, Lcom/google/android/gms/internal/measurement/al$d;

    invoke-direct {v0}, Lcom/google/android/gms/internal/measurement/al$d;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/measurement/al$d;->zzwq:Lcom/google/android/gms/internal/measurement/al$d;

    .line 36
    const-class v0, Lcom/google/android/gms/internal/measurement/al$d;

    sget-object v1, Lcom/google/android/gms/internal/measurement/al$d;->zzwq:Lcom/google/android/gms/internal/measurement/al$d;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/measurement/dr;->a(Ljava/lang/Class;Lcom/google/android/gms/internal/measurement/dr;)V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/dr;-><init>()V

    const-string v0, ""

    .line 2
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/al$d;->zzwk:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/measurement/al$d;J)V
    .locals 1

    .line 1009
    iget v0, p0, Lcom/google/android/gms/internal/measurement/al$d;->zzue:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/gms/internal/measurement/al$d;->zzue:I

    .line 1010
    iput-wide p1, p0, Lcom/google/android/gms/internal/measurement/al$d;->zzwp:J

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/measurement/al$d;Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 1006
    iget v0, p0, Lcom/google/android/gms/internal/measurement/al$d;->zzue:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/measurement/al$d;->zzue:I

    .line 1007
    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/al$d;->zzwk:Ljava/lang/String;

    return-void

    .line 1005
    :cond_0
    new-instance p0, Ljava/lang/NullPointerException;

    invoke-direct {p0}, Ljava/lang/NullPointerException;-><init>()V

    throw p0
.end method

.method public static pq()Lcom/google/android/gms/internal/measurement/al$d$a;
    .locals 1

    .line 12
    sget-object v0, Lcom/google/android/gms/internal/measurement/al$d;->zzwq:Lcom/google/android/gms/internal/measurement/al$d;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/dr;->rY()Lcom/google/android/gms/internal/measurement/dr$a;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$d$a;

    return-object v0
.end method

.method static synthetic pr()Lcom/google/android/gms/internal/measurement/al$d;
    .locals 1

    .line 32
    sget-object v0, Lcom/google/android/gms/internal/measurement/al$d;->zzwq:Lcom/google/android/gms/internal/measurement/al$d;

    return-object v0
.end method


# virtual methods
.method protected final bm(I)Ljava/lang/Object;
    .locals 3

    .line 13
    sget-object v0, Lcom/google/android/gms/internal/measurement/ak;->aqY:[I

    const/4 v1, 0x1

    sub-int/2addr p1, v1

    aget p1, v0, p1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    .line 31
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    const/4 p1, 0x0

    return-object p1

    .line 29
    :pswitch_1
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    .line 20
    :pswitch_2
    sget-object p1, Lcom/google/android/gms/internal/measurement/al$d;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    if-nez p1, :cond_1

    .line 22
    const-class v0, Lcom/google/android/gms/internal/measurement/al$d;

    monitor-enter v0

    .line 23
    :try_start_0
    sget-object p1, Lcom/google/android/gms/internal/measurement/al$d;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    if-nez p1, :cond_0

    .line 25
    new-instance p1, Lcom/google/android/gms/internal/measurement/dr$c;

    sget-object v1, Lcom/google/android/gms/internal/measurement/al$d;->zzwq:Lcom/google/android/gms/internal/measurement/al$d;

    invoke-direct {p1, v1}, Lcom/google/android/gms/internal/measurement/dr$c;-><init>(Lcom/google/android/gms/internal/measurement/dr;)V

    .line 26
    sput-object p1, Lcom/google/android/gms/internal/measurement/al$d;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    .line 27
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    .line 19
    :pswitch_3
    sget-object p1, Lcom/google/android/gms/internal/measurement/al$d;->zzwq:Lcom/google/android/gms/internal/measurement/al$d;

    return-object p1

    :pswitch_4
    const/4 p1, 0x3

    new-array p1, p1, [Ljava/lang/Object;

    const-string v2, "zzue"

    aput-object v2, p1, v0

    const-string v0, "zzwk"

    aput-object v0, p1, v1

    const/4 v0, 0x2

    const-string v1, "zzwp"

    aput-object v1, p1, v0

    .line 18
    sget-object v0, Lcom/google/android/gms/internal/measurement/al$d;->zzwq:Lcom/google/android/gms/internal/measurement/al$d;

    const-string v1, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0008\u0000\u0002\u0002\u0001"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/internal/measurement/al$d;->a(Lcom/google/android/gms/internal/measurement/fb;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 15
    :pswitch_5
    new-instance p1, Lcom/google/android/gms/internal/measurement/al$d$a;

    invoke-direct {p1, v0}, Lcom/google/android/gms/internal/measurement/al$d$a;-><init>(B)V

    return-object p1

    .line 14
    :pswitch_6
    new-instance p1, Lcom/google/android/gms/internal/measurement/al$d;

    invoke-direct {p1}, Lcom/google/android/gms/internal/measurement/al$d;-><init>()V

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
