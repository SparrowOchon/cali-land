.class public final Lcom/google/android/gms/internal/measurement/al$d$a;
.super Lcom/google/android/gms/internal/measurement/dr$a;

# interfaces
.implements Lcom/google/android/gms/internal/measurement/fd;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/measurement/al$d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/measurement/dr$a<",
        "Lcom/google/android/gms/internal/measurement/al$d;",
        "Lcom/google/android/gms/internal/measurement/al$d$a;",
        ">;",
        "Lcom/google/android/gms/internal/measurement/fd;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-static {}, Lcom/google/android/gms/internal/measurement/al$d;->pr()Lcom/google/android/gms/internal/measurement/al$d;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/measurement/dr$a;-><init>(Lcom/google/android/gms/internal/measurement/dr;)V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/al$d$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final C(J)Lcom/google/android/gms/internal/measurement/al$d$a;
    .locals 1

    .line 6
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/dr$a;->se()V

    .line 7
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$d$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$d;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/internal/measurement/al$d;->a(Lcom/google/android/gms/internal/measurement/al$d;J)V

    return-object p0
.end method

.method public final bx(Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/al$d$a;
    .locals 1

    .line 3
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/dr$a;->se()V

    .line 4
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$d$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$d;

    invoke-static {v0, p1}, Lcom/google/android/gms/internal/measurement/al$d;->a(Lcom/google/android/gms/internal/measurement/al$d;Ljava/lang/String;)V

    return-object p0
.end method
