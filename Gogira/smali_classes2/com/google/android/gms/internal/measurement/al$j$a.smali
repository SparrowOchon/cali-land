.class public final Lcom/google/android/gms/internal/measurement/al$j$a;
.super Lcom/google/android/gms/internal/measurement/dr$a;

# interfaces
.implements Lcom/google/android/gms/internal/measurement/fd;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/measurement/al$j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/measurement/dr$a<",
        "Lcom/google/android/gms/internal/measurement/al$j;",
        "Lcom/google/android/gms/internal/measurement/al$j$a;",
        ">;",
        "Lcom/google/android/gms/internal/measurement/fd;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-static {}, Lcom/google/android/gms/internal/measurement/al$j;->qn()Lcom/google/android/gms/internal/measurement/al$j;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/measurement/dr$a;-><init>(Lcom/google/android/gms/internal/measurement/dr;)V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/al$j$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final Q(J)Lcom/google/android/gms/internal/measurement/al$j$a;
    .locals 1

    .line 6
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/dr$a;->se()V

    .line 7
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$j$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$j;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/internal/measurement/al$j;->a(Lcom/google/android/gms/internal/measurement/al$j;J)V

    return-object p0
.end method

.method public final bG(I)Lcom/google/android/gms/internal/measurement/al$j$a;
    .locals 1

    .line 3
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/dr$a;->se()V

    .line 4
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$j$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$j;

    invoke-static {v0, p1}, Lcom/google/android/gms/internal/measurement/al$j;->a(Lcom/google/android/gms/internal/measurement/al$j;I)V

    return-object p0
.end method

.method public final j(Ljava/lang/Iterable;)Lcom/google/android/gms/internal/measurement/al$j$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "+",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/google/android/gms/internal/measurement/al$j$a;"
        }
    .end annotation

    .line 9
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/dr$a;->se()V

    .line 10
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$j$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$j;

    invoke-static {v0, p1}, Lcom/google/android/gms/internal/measurement/al$j;->a(Lcom/google/android/gms/internal/measurement/al$j;Ljava/lang/Iterable;)V

    return-object p0
.end method

.method public final qo()Lcom/google/android/gms/internal/measurement/al$j$a;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/dr$a;->se()V

    .line 13
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$j$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$j;

    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/al$j;->a(Lcom/google/android/gms/internal/measurement/al$j;)V

    return-object p0
.end method
