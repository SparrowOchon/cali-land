.class public abstract Lcom/google/android/gms/internal/measurement/dr$b;
.super Lcom/google/android/gms/internal/measurement/dr;

# interfaces
.implements Lcom/google/android/gms/internal/measurement/fd;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/measurement/dr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MessageType:",
        "Lcom/google/android/gms/internal/measurement/dr$b<",
        "TMessageType;TBuilderType;>;BuilderType:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/gms/internal/measurement/dr<",
        "TMessageType;TBuilderType;>;",
        "Lcom/google/android/gms/internal/measurement/fd;"
    }
.end annotation


# instance fields
.field protected zzaic:Lcom/google/android/gms/internal/measurement/dh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/dh<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/dr;-><init>()V

    .line 2
    invoke-static {}, Lcom/google/android/gms/internal/measurement/dh;->rM()Lcom/google/android/gms/internal/measurement/dh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/dr$b;->zzaic:Lcom/google/android/gms/internal/measurement/dh;

    return-void
.end method


# virtual methods
.method final sj()Lcom/google/android/gms/internal/measurement/dh;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/internal/measurement/dh<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 3
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/dr$b;->zzaic:Lcom/google/android/gms/internal/measurement/dh;

    .line 1016
    iget-boolean v0, v0, Lcom/google/android/gms/internal/measurement/dh;->atE:Z

    if-eqz v0, :cond_0

    .line 4
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/dr$b;->zzaic:Lcom/google/android/gms/internal/measurement/dh;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/dh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/dh;

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/dr$b;->zzaic:Lcom/google/android/gms/internal/measurement/dh;

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/dr$b;->zzaic:Lcom/google/android/gms/internal/measurement/dh;

    return-object v0
.end method
