.class public final Lcom/google/android/gms/internal/measurement/hq;
.super Ljava/lang/Object;


# static fields
.field private static final awa:[I

.field private static final ayA:[[B

.field public static final ayB:[B

.field private static final ayr:I = 0xb

.field private static final ays:I = 0xc

.field private static final ayt:I = 0x10

.field private static final ayu:I = 0x1a

.field private static final ayv:[J

.field private static final ayw:[F

.field private static final ayx:[D

.field private static final ayy:[Z

.field private static final ayz:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    new-array v1, v0, [I

    .line 17
    sput-object v1, Lcom/google/android/gms/internal/measurement/hq;->awa:[I

    new-array v1, v0, [J

    .line 18
    sput-object v1, Lcom/google/android/gms/internal/measurement/hq;->ayv:[J

    new-array v1, v0, [F

    .line 19
    sput-object v1, Lcom/google/android/gms/internal/measurement/hq;->ayw:[F

    new-array v1, v0, [D

    .line 20
    sput-object v1, Lcom/google/android/gms/internal/measurement/hq;->ayx:[D

    new-array v1, v0, [Z

    .line 21
    sput-object v1, Lcom/google/android/gms/internal/measurement/hq;->ayy:[Z

    new-array v1, v0, [Ljava/lang/String;

    .line 22
    sput-object v1, Lcom/google/android/gms/internal/measurement/hq;->ayz:[Ljava/lang/String;

    new-array v1, v0, [[B

    .line 23
    sput-object v1, Lcom/google/android/gms/internal/measurement/hq;->ayA:[[B

    new-array v0, v0, [B

    .line 24
    sput-object v0, Lcom/google/android/gms/internal/measurement/hq;->ayB:[B

    return-void
.end method

.method public static final b(Lcom/google/android/gms/internal/measurement/he;I)I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/he;->getPosition()I

    move-result v0

    .line 3
    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/measurement/he;->bQ(I)Z

    const/4 v1, 0x1

    .line 4
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/he;->qT()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 5
    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/measurement/he;->bQ(I)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1155
    :cond_0
    iget v2, p0, Lcom/google/android/gms/internal/measurement/he;->ayd:I

    iget v3, p0, Lcom/google/android/gms/internal/measurement/he;->aya:I

    sub-int/2addr v2, v3

    if-gt v0, v2, :cond_2

    if-ltz v0, :cond_1

    .line 1159
    iget v2, p0, Lcom/google/android/gms/internal/measurement/he;->aya:I

    add-int/2addr v2, v0

    iput v2, p0, Lcom/google/android/gms/internal/measurement/he;->ayd:I

    .line 1160
    iput p1, p0, Lcom/google/android/gms/internal/measurement/he;->atg:I

    return v1

    .line 1158
    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const/16 p1, 0x18

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, p1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string p1, "Bad position "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 1156
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    iget v1, p0, Lcom/google/android/gms/internal/measurement/he;->ayd:I

    iget p0, p0, Lcom/google/android/gms/internal/measurement/he;->aya:I

    sub-int/2addr v1, p0

    const/16 p0, 0x32

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, p0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string p0, "Position "

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, " is beyond current "

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :goto_1
    throw p1

    :goto_2
    goto :goto_1
.end method
