.class public final Lcom/google/android/gms/internal/measurement/dr$d;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/measurement/dr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final enum ava:I = 0x1

.field public static final enum avb:I = 0x2

.field public static final enum avc:I = 0x3

.field public static final enum avd:I = 0x4

.field public static final enum ave:I = 0x5

.field public static final enum avf:I = 0x6

.field public static final enum avg:I = 0x7

.field private static final synthetic avh:[I

.field public static final enum avi:I = 0x1

.field public static final enum avj:I = 0x2

.field private static final synthetic avk:[I

.field public static final enum avl:I = 0x1

.field public static final enum avm:I = 0x2

.field private static final synthetic avn:[I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x7

    new-array v0, v0, [I

    sget v1, Lcom/google/android/gms/internal/measurement/dr$d;->ava:I

    const/4 v2, 0x0

    aput v1, v0, v2

    sget v1, Lcom/google/android/gms/internal/measurement/dr$d;->avb:I

    const/4 v3, 0x1

    aput v1, v0, v3

    sget v1, Lcom/google/android/gms/internal/measurement/dr$d;->avc:I

    const/4 v4, 0x2

    aput v1, v0, v4

    sget v1, Lcom/google/android/gms/internal/measurement/dr$d;->avd:I

    const/4 v5, 0x3

    aput v1, v0, v5

    sget v1, Lcom/google/android/gms/internal/measurement/dr$d;->ave:I

    const/4 v5, 0x4

    aput v1, v0, v5

    sget v1, Lcom/google/android/gms/internal/measurement/dr$d;->avf:I

    const/4 v5, 0x5

    aput v1, v0, v5

    sget v1, Lcom/google/android/gms/internal/measurement/dr$d;->avg:I

    const/4 v5, 0x6

    aput v1, v0, v5

    sput-object v0, Lcom/google/android/gms/internal/measurement/dr$d;->avh:[I

    new-array v0, v4, [I

    sget v1, Lcom/google/android/gms/internal/measurement/dr$d;->avi:I

    aput v1, v0, v2

    sget v1, Lcom/google/android/gms/internal/measurement/dr$d;->avj:I

    aput v1, v0, v3

    sput-object v0, Lcom/google/android/gms/internal/measurement/dr$d;->avk:[I

    new-array v0, v4, [I

    sget v1, Lcom/google/android/gms/internal/measurement/dr$d;->avl:I

    aput v1, v0, v2

    sget v1, Lcom/google/android/gms/internal/measurement/dr$d;->avm:I

    aput v1, v0, v3

    sput-object v0, Lcom/google/android/gms/internal/measurement/dr$d;->avn:[I

    return-void
.end method

.method public static sk()[I
    .locals 1

    .line 1
    sget-object v0, Lcom/google/android/gms/internal/measurement/dr$d;->avh:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    return-object v0
.end method
