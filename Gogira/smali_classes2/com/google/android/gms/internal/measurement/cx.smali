.class final Lcom/google/android/gms/internal/measurement/cx;
.super Lcom/google/android/gms/internal/measurement/cv;


# instance fields
.field private final atd:Z

.field private ate:I

.field private atf:I

.field private atg:I

.field private ath:I

.field private final buffer:[B

.field private limit:I

.field private pos:I


# direct methods
.method synthetic constructor <init>([BII)V
    .locals 1

    const/4 v0, 0x0

    .line 216
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/internal/measurement/cx;-><init>([BIIZ)V

    return-void
.end method

.method private constructor <init>([BIIZ)V
    .locals 1

    const/4 p4, 0x0

    .line 1
    invoke-direct {p0, p4}, Lcom/google/android/gms/internal/measurement/cv;-><init>(B)V

    const v0, 0x7fffffff

    .line 2
    iput v0, p0, Lcom/google/android/gms/internal/measurement/cx;->ath:I

    .line 3
    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/cx;->buffer:[B

    add-int/2addr p3, p2

    .line 4
    iput p3, p0, Lcom/google/android/gms/internal/measurement/cx;->limit:I

    .line 5
    iput p2, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    .line 6
    iget p1, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    iput p1, p0, Lcom/google/android/gms/internal/measurement/cx;->atf:I

    .line 7
    iput-boolean p4, p0, Lcom/google/android/gms/internal/measurement/cx;->atd:Z

    return-void
.end method

.method private final rn()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 118
    iget v0, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    .line 119
    iget v1, p0, Lcom/google/android/gms/internal/measurement/cx;->limit:I

    if-eq v1, v0, :cond_6

    .line 120
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/cx;->buffer:[B

    add-int/lit8 v3, v0, 0x1

    .line 121
    aget-byte v0, v2, v0

    if-ltz v0, :cond_0

    .line 122
    iput v3, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    return v0

    :cond_0
    sub-int/2addr v1, v3

    const/16 v4, 0x9

    if-lt v1, v4, :cond_6

    add-int/lit8 v1, v3, 0x1

    .line 125
    aget-byte v3, v2, v3

    shl-int/lit8 v3, v3, 0x7

    xor-int/2addr v0, v3

    if-gez v0, :cond_1

    xor-int/lit8 v0, v0, -0x80

    goto :goto_0

    :cond_1
    add-int/lit8 v3, v1, 0x1

    .line 127
    aget-byte v1, v2, v1

    shl-int/lit8 v1, v1, 0xe

    xor-int/2addr v0, v1

    if-ltz v0, :cond_3

    xor-int/lit16 v0, v0, 0x3f80

    :cond_2
    move v1, v3

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v3, 0x1

    .line 129
    aget-byte v3, v2, v3

    shl-int/lit8 v3, v3, 0x15

    xor-int/2addr v0, v3

    if-gez v0, :cond_4

    const v2, -0x1fc080

    xor-int/2addr v0, v2

    goto :goto_0

    :cond_4
    add-int/lit8 v3, v1, 0x1

    .line 131
    aget-byte v1, v2, v1

    shl-int/lit8 v4, v1, 0x1c

    xor-int/2addr v0, v4

    const v4, 0xfe03f80

    xor-int/2addr v0, v4

    if-gez v1, :cond_2

    add-int/lit8 v1, v3, 0x1

    .line 134
    aget-byte v3, v2, v3

    if-gez v3, :cond_5

    add-int/lit8 v3, v1, 0x1

    aget-byte v1, v2, v1

    if-gez v1, :cond_2

    add-int/lit8 v1, v3, 0x1

    aget-byte v3, v2, v3

    if-gez v3, :cond_5

    add-int/lit8 v3, v1, 0x1

    aget-byte v1, v2, v1

    if-gez v1, :cond_2

    add-int/lit8 v1, v3, 0x1

    aget-byte v2, v2, v3

    if-ltz v2, :cond_6

    .line 135
    :cond_5
    :goto_0
    iput v1, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    return v0

    .line 137
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/cv;->ri()J

    move-result-wide v0

    long-to-int v1, v0

    return v1
.end method

.method private final ro()J
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 138
    iget v0, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    .line 139
    iget v1, p0, Lcom/google/android/gms/internal/measurement/cx;->limit:I

    if-eq v1, v0, :cond_9

    .line 140
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/cx;->buffer:[B

    add-int/lit8 v3, v0, 0x1

    .line 141
    aget-byte v0, v2, v0

    if-ltz v0, :cond_0

    .line 142
    iput v3, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    int-to-long v0, v0

    return-wide v0

    :cond_0
    sub-int/2addr v1, v3

    const/16 v4, 0x9

    if-lt v1, v4, :cond_9

    add-int/lit8 v1, v3, 0x1

    .line 145
    aget-byte v3, v2, v3

    shl-int/lit8 v3, v3, 0x7

    xor-int/2addr v0, v3

    if-gez v0, :cond_1

    xor-int/lit8 v0, v0, -0x80

    :goto_0
    int-to-long v2, v0

    move-wide v3, v2

    goto/16 :goto_3

    :cond_1
    add-int/lit8 v3, v1, 0x1

    .line 147
    aget-byte v1, v2, v1

    shl-int/lit8 v1, v1, 0xe

    xor-int/2addr v0, v1

    if-ltz v0, :cond_2

    xor-int/lit16 v0, v0, 0x3f80

    int-to-long v0, v0

    move-wide v9, v0

    move v1, v3

    move-wide v3, v9

    goto/16 :goto_3

    :cond_2
    add-int/lit8 v1, v3, 0x1

    .line 149
    aget-byte v3, v2, v3

    shl-int/lit8 v3, v3, 0x15

    xor-int/2addr v0, v3

    if-gez v0, :cond_3

    const v2, -0x1fc080

    xor-int/2addr v0, v2

    goto :goto_0

    :cond_3
    int-to-long v3, v0

    add-int/lit8 v0, v1, 0x1

    .line 151
    aget-byte v1, v2, v1

    int-to-long v5, v1

    const/16 v1, 0x1c

    shl-long/2addr v5, v1

    xor-long/2addr v3, v5

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-ltz v1, :cond_5

    const-wide/32 v1, 0xfe03f80

    :goto_1
    xor-long/2addr v1, v3

    move-wide v3, v1

    :cond_4
    move v1, v0

    goto :goto_3

    :cond_5
    add-int/lit8 v1, v0, 0x1

    .line 153
    aget-byte v0, v2, v0

    int-to-long v7, v0

    const/16 v0, 0x23

    shl-long/2addr v7, v0

    xor-long/2addr v3, v7

    cmp-long v0, v3, v5

    if-gez v0, :cond_6

    const-wide v5, -0x7f01fc080L

    :goto_2
    xor-long/2addr v3, v5

    goto :goto_3

    :cond_6
    add-int/lit8 v0, v1, 0x1

    .line 155
    aget-byte v1, v2, v1

    int-to-long v7, v1

    const/16 v1, 0x2a

    shl-long/2addr v7, v1

    xor-long/2addr v3, v7

    cmp-long v1, v3, v5

    if-ltz v1, :cond_7

    const-wide v1, 0x3f80fe03f80L

    goto :goto_1

    :cond_7
    add-int/lit8 v1, v0, 0x1

    .line 157
    aget-byte v0, v2, v0

    int-to-long v7, v0

    const/16 v0, 0x31

    shl-long/2addr v7, v0

    xor-long/2addr v3, v7

    cmp-long v0, v3, v5

    if-gez v0, :cond_8

    const-wide v5, -0x1fc07f01fc080L

    goto :goto_2

    :cond_8
    add-int/lit8 v0, v1, 0x1

    .line 159
    aget-byte v1, v2, v1

    int-to-long v7, v1

    const/16 v1, 0x38

    shl-long/2addr v7, v1

    xor-long/2addr v3, v7

    const-wide v7, 0xfe03f80fe03f80L

    xor-long/2addr v3, v7

    cmp-long v1, v3, v5

    if-gez v1, :cond_4

    add-int/lit8 v1, v0, 0x1

    .line 162
    aget-byte v0, v2, v0

    int-to-long v7, v0

    cmp-long v0, v7, v5

    if-ltz v0, :cond_9

    .line 163
    :goto_3
    iput v1, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    return-wide v3

    .line 165
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/cv;->ri()J

    move-result-wide v0

    return-wide v0
.end method

.method private final rp()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 174
    iget v0, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    .line 175
    iget v1, p0, Lcom/google/android/gms/internal/measurement/cx;->limit:I

    sub-int/2addr v1, v0

    const/4 v2, 0x4

    if-lt v1, v2, :cond_0

    .line 177
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/cx;->buffer:[B

    add-int/lit8 v2, v0, 0x4

    .line 178
    iput v2, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    .line 179
    aget-byte v2, v1, v0

    and-int/lit16 v2, v2, 0xff

    add-int/lit8 v3, v0, 0x1

    aget-byte v3, v1, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    add-int/lit8 v3, v0, 0x2

    aget-byte v3, v1, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x3

    aget-byte v0, v1, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    or-int/2addr v0, v2

    return v0

    .line 176
    :cond_0
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ec;->sn()Lcom/google/android/gms/internal/measurement/ec;

    move-result-object v0

    throw v0
.end method

.method private final rq()J
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 180
    iget v0, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    .line 181
    iget v1, p0, Lcom/google/android/gms/internal/measurement/cx;->limit:I

    sub-int/2addr v1, v0

    const/16 v2, 0x8

    if-lt v1, v2, :cond_0

    .line 183
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/cx;->buffer:[B

    add-int/lit8 v3, v0, 0x8

    .line 184
    iput v3, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    .line 185
    aget-byte v3, v1, v0

    int-to-long v3, v3

    const-wide/16 v5, 0xff

    and-long/2addr v3, v5

    add-int/lit8 v7, v0, 0x1

    aget-byte v7, v1, v7

    int-to-long v7, v7

    and-long/2addr v7, v5

    shl-long/2addr v7, v2

    or-long/2addr v3, v7

    add-int/lit8 v2, v0, 0x2

    aget-byte v2, v1, v2

    int-to-long v7, v2

    and-long/2addr v7, v5

    const/16 v2, 0x10

    shl-long/2addr v7, v2

    or-long/2addr v3, v7

    add-int/lit8 v2, v0, 0x3

    aget-byte v2, v1, v2

    int-to-long v7, v2

    and-long/2addr v7, v5

    const/16 v2, 0x18

    shl-long/2addr v7, v2

    or-long/2addr v3, v7

    add-int/lit8 v2, v0, 0x4

    aget-byte v2, v1, v2

    int-to-long v7, v2

    and-long/2addr v7, v5

    const/16 v2, 0x20

    shl-long/2addr v7, v2

    or-long/2addr v3, v7

    add-int/lit8 v2, v0, 0x5

    aget-byte v2, v1, v2

    int-to-long v7, v2

    and-long/2addr v7, v5

    const/16 v2, 0x28

    shl-long/2addr v7, v2

    or-long/2addr v3, v7

    add-int/lit8 v2, v0, 0x6

    aget-byte v2, v1, v2

    int-to-long v7, v2

    and-long/2addr v7, v5

    const/16 v2, 0x30

    shl-long/2addr v7, v2

    or-long/2addr v3, v7

    add-int/lit8 v0, v0, 0x7

    aget-byte v0, v1, v0

    int-to-long v0, v0

    and-long/2addr v0, v5

    const/16 v2, 0x38

    shl-long/2addr v0, v2

    or-long/2addr v0, v3

    return-wide v0

    .line 182
    :cond_0
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ec;->sn()Lcom/google/android/gms/internal/measurement/ec;

    move-result-object v0

    throw v0
.end method

.method private final rr()V
    .locals 3

    .line 195
    iget v0, p0, Lcom/google/android/gms/internal/measurement/cx;->limit:I

    iget v1, p0, Lcom/google/android/gms/internal/measurement/cx;->ate:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/internal/measurement/cx;->limit:I

    .line 196
    iget v0, p0, Lcom/google/android/gms/internal/measurement/cx;->limit:I

    iget v1, p0, Lcom/google/android/gms/internal/measurement/cx;->atf:I

    sub-int v1, v0, v1

    .line 197
    iget v2, p0, Lcom/google/android/gms/internal/measurement/cx;->ath:I

    if-le v1, v2, :cond_0

    sub-int/2addr v1, v2

    .line 198
    iput v1, p0, Lcom/google/android/gms/internal/measurement/cx;->ate:I

    .line 199
    iget v1, p0, Lcom/google/android/gms/internal/measurement/cx;->ate:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/internal/measurement/cx;->limit:I

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 200
    iput v0, p0, Lcom/google/android/gms/internal/measurement/cx;->ate:I

    return-void
.end method

.method private final rs()B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 207
    iget v0, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    iget v1, p0, Lcom/google/android/gms/internal/measurement/cx;->limit:I

    if-eq v0, v1, :cond_0

    .line 209
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/cx;->buffer:[B

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    aget-byte v0, v1, v0

    return v0

    .line 208
    :cond_0
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ec;->sn()Lcom/google/android/gms/internal/measurement/ec;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/internal/measurement/fk;Lcom/google/android/gms/internal/measurement/de;)Lcom/google/android/gms/internal/measurement/fb;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/gms/internal/measurement/fb;",
            ">(",
            "Lcom/google/android/gms/internal/measurement/fk<",
            "TT;>;",
            "Lcom/google/android/gms/internal/measurement/de;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 84
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/cx;->rn()I

    move-result v0

    .line 85
    iget v1, p0, Lcom/google/android/gms/internal/measurement/cx;->asV:I

    iget v2, p0, Lcom/google/android/gms/internal/measurement/cx;->asW:I

    if-ge v1, v2, :cond_0

    .line 87
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/measurement/cv;->bR(I)I

    move-result v0

    .line 88
    iget v1, p0, Lcom/google/android/gms/internal/measurement/cx;->asV:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/gms/internal/measurement/cx;->asV:I

    .line 89
    invoke-interface {p1, p0, p2}, Lcom/google/android/gms/internal/measurement/fk;->c(Lcom/google/android/gms/internal/measurement/cv;Lcom/google/android/gms/internal/measurement/de;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/internal/measurement/fb;

    const/4 p2, 0x0

    .line 90
    invoke-virtual {p0, p2}, Lcom/google/android/gms/internal/measurement/cv;->bP(I)V

    .line 91
    iget p2, p0, Lcom/google/android/gms/internal/measurement/cx;->asV:I

    add-int/lit8 p2, p2, -0x1

    iput p2, p0, Lcom/google/android/gms/internal/measurement/cx;->asV:I

    .line 92
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/measurement/cv;->bS(I)V

    return-object p1

    .line 86
    :cond_0
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ec;->st()Lcom/google/android/gms/internal/measurement/ec;

    move-result-object p1

    throw p1
.end method

.method public final bP(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/measurement/ec;
        }
    .end annotation

    .line 18
    iget v0, p0, Lcom/google/android/gms/internal/measurement/cx;->atg:I

    if-ne v0, p1, :cond_0

    return-void

    .line 19
    :cond_0
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ec;->sr()Lcom/google/android/gms/internal/measurement/ec;

    move-result-object p1

    throw p1
.end method

.method public final bQ(I)Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    and-int/lit8 v0, p1, 0x7

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_6

    if-eq v0, v2, :cond_5

    const/4 v3, 0x2

    if-eq v0, v3, :cond_4

    const/4 v3, 0x4

    const/4 v4, 0x3

    if-eq v0, v4, :cond_2

    if-eq v0, v3, :cond_1

    const/4 p1, 0x5

    if-ne v0, p1, :cond_0

    .line 53
    invoke-virtual {p0, v3}, Lcom/google/android/gms/internal/measurement/cv;->bT(I)V

    return v2

    .line 55
    :cond_0
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ec;->ss()Lcom/google/android/gms/internal/measurement/eb;

    move-result-object p1

    throw p1

    :cond_1
    return v1

    .line 43
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/cv;->qT()I

    move-result v0

    if-eqz v0, :cond_3

    .line 44
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/measurement/cv;->bQ(I)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_3
    ushr-int/2addr p1, v4

    shl-int/2addr p1, v4

    or-int/2addr p1, v3

    .line 50
    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/measurement/cv;->bP(I)V

    return v2

    .line 40
    :cond_4
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/cx;->rn()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/measurement/cv;->bT(I)V

    return v2

    :cond_5
    const/16 p1, 0x8

    .line 38
    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/measurement/cv;->bT(I)V

    return v2

    .line 25
    :cond_6
    iget p1, p0, Lcom/google/android/gms/internal/measurement/cx;->limit:I

    iget v0, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    sub-int/2addr p1, v0

    const/16 v0, 0xa

    if-lt p1, v0, :cond_8

    :goto_0
    if-ge v1, v0, :cond_7

    .line 28
    iget-object p1, p0, Lcom/google/android/gms/internal/measurement/cx;->buffer:[B

    iget v3, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    aget-byte p1, p1, v3

    if-gez p1, :cond_9

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 30
    :cond_7
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ec;->sp()Lcom/google/android/gms/internal/measurement/ec;

    move-result-object p1

    throw p1

    :cond_8
    :goto_1
    if-ge v1, v0, :cond_a

    .line 34
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/cx;->rs()B

    move-result p1

    if-gez p1, :cond_9

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_9
    return v2

    .line 36
    :cond_a
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ec;->sp()Lcom/google/android/gms/internal/measurement/ec;

    move-result-object p1

    goto :goto_3

    :goto_2
    throw p1

    :goto_3
    goto :goto_2
.end method

.method public final bR(I)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/measurement/ec;
        }
    .end annotation

    if-ltz p1, :cond_1

    .line 188
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/cv;->rk()I

    move-result v0

    add-int/2addr p1, v0

    .line 189
    iget v0, p0, Lcom/google/android/gms/internal/measurement/cx;->ath:I

    if-gt p1, v0, :cond_0

    .line 192
    iput p1, p0, Lcom/google/android/gms/internal/measurement/cx;->ath:I

    .line 193
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/cx;->rr()V

    return v0

    .line 191
    :cond_0
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ec;->sn()Lcom/google/android/gms/internal/measurement/ec;

    move-result-object p1

    throw p1

    .line 187
    :cond_1
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ec;->so()Lcom/google/android/gms/internal/measurement/ec;

    move-result-object p1

    throw p1
.end method

.method public final bS(I)V
    .locals 0

    .line 202
    iput p1, p0, Lcom/google/android/gms/internal/measurement/cx;->ath:I

    .line 203
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/cx;->rr()V

    return-void
.end method

.method public final bT(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-ltz p1, :cond_0

    .line 210
    iget v0, p0, Lcom/google/android/gms/internal/measurement/cx;->limit:I

    iget v1, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    sub-int/2addr v0, v1

    if-gt p1, v0, :cond_0

    add-int/2addr v1, p1

    .line 211
    iput v1, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    return-void

    :cond_0
    if-gez p1, :cond_1

    .line 214
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ec;->so()Lcom/google/android/gms/internal/measurement/ec;

    move-result-object p1

    throw p1

    .line 215
    :cond_1
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ec;->sn()Lcom/google/android/gms/internal/measurement/ec;

    move-result-object p1

    throw p1
.end method

.method public final qT()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 9
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/cv;->rj()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 10
    iput v0, p0, Lcom/google/android/gms/internal/measurement/cx;->atg:I

    return v0

    .line 12
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/cx;->rn()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/measurement/cx;->atg:I

    .line 13
    iget v0, p0, Lcom/google/android/gms/internal/measurement/cx;->atg:I

    ushr-int/lit8 v1, v0, 0x3

    if-eqz v1, :cond_1

    return v0

    .line 16
    :cond_1
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ec;->sq()Lcom/google/android/gms/internal/measurement/ec;

    move-result-object v0

    throw v0
.end method

.method public final qU()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 58
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/cx;->ro()J

    move-result-wide v0

    return-wide v0
.end method

.method public final qV()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 59
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/cx;->ro()J

    move-result-wide v0

    return-wide v0
.end method

.method public final qW()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 60
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/cx;->rn()I

    move-result v0

    return v0
.end method

.method public final qX()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 61
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/cx;->rq()J

    move-result-wide v0

    return-wide v0
.end method

.method public final qY()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 62
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/cx;->rp()I

    move-result v0

    return v0
.end method

.method public final qZ()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 63
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/cx;->ro()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final ra()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 74
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/cx;->rn()I

    move-result v0

    if-lez v0, :cond_0

    .line 75
    iget v1, p0, Lcom/google/android/gms/internal/measurement/cx;->limit:I

    iget v2, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    sub-int/2addr v1, v2

    if-gt v0, v1, :cond_0

    .line 76
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/cx;->buffer:[B

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/internal/measurement/gr;->g([BII)Ljava/lang/String;

    move-result-object v1

    .line 77
    iget v2, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    add-int/2addr v2, v0

    iput v2, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    return-object v1

    :cond_0
    if-nez v0, :cond_1

    const-string v0, ""

    return-object v0

    :cond_1
    if-gtz v0, :cond_2

    .line 82
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ec;->so()Lcom/google/android/gms/internal/measurement/ec;

    move-result-object v0

    throw v0

    .line 83
    :cond_2
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ec;->sn()Lcom/google/android/gms/internal/measurement/ec;

    move-result-object v0

    throw v0
.end method

.method public final rb()Lcom/google/android/gms/internal/measurement/cj;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 94
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/cx;->rn()I

    move-result v0

    if-lez v0, :cond_0

    .line 95
    iget v1, p0, Lcom/google/android/gms/internal/measurement/cx;->limit:I

    iget v2, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    sub-int/2addr v1, v2

    if-gt v0, v1, :cond_0

    .line 96
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/cx;->buffer:[B

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/internal/measurement/cj;->c([BII)Lcom/google/android/gms/internal/measurement/cj;

    move-result-object v1

    .line 97
    iget v2, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    add-int/2addr v2, v0

    iput v2, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    return-object v1

    :cond_0
    if-nez v0, :cond_1

    .line 100
    sget-object v0, Lcom/google/android/gms/internal/measurement/cj;->asR:Lcom/google/android/gms/internal/measurement/cj;

    return-object v0

    :cond_1
    if-lez v0, :cond_2

    .line 102
    iget v1, p0, Lcom/google/android/gms/internal/measurement/cx;->limit:I

    iget v2, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    sub-int/2addr v1, v2

    if-gt v0, v1, :cond_2

    add-int/2addr v0, v2

    .line 104
    iput v0, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/cx;->buffer:[B

    iget v1, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    invoke-static {v0, v2, v1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    goto :goto_0

    :cond_2
    if-gtz v0, :cond_4

    if-nez v0, :cond_3

    .line 108
    sget-object v0, Lcom/google/android/gms/internal/measurement/ds;->avo:[B

    .line 111
    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/cj;->l([B)Lcom/google/android/gms/internal/measurement/cj;

    move-result-object v0

    return-object v0

    .line 109
    :cond_3
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ec;->so()Lcom/google/android/gms/internal/measurement/ec;

    move-result-object v0

    throw v0

    .line 110
    :cond_4
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ec;->sn()Lcom/google/android/gms/internal/measurement/ec;

    move-result-object v0

    throw v0
.end method

.method public final rc()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 112
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/cx;->rn()I

    move-result v0

    return v0
.end method

.method public final rd()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 113
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/cx;->rn()I

    move-result v0

    return v0
.end method

.method public final re()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 114
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/cx;->rp()I

    move-result v0

    return v0
.end method

.method public final readDouble()D
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 56
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/cx;->rq()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    return-wide v0
.end method

.method public final readFloat()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 57
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/cx;->rp()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    return v0
.end method

.method public final readString()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 64
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/cx;->rn()I

    move-result v0

    if-lez v0, :cond_0

    .line 65
    iget v1, p0, Lcom/google/android/gms/internal/measurement/cx;->limit:I

    iget v2, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    sub-int/2addr v1, v2

    if-gt v0, v1, :cond_0

    .line 66
    new-instance v1, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/cx;->buffer:[B

    sget-object v4, Lcom/google/android/gms/internal/measurement/ds;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v1, v3, v2, v0, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    .line 67
    iget v2, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    add-int/2addr v2, v0

    iput v2, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    return-object v1

    :cond_0
    if-nez v0, :cond_1

    const-string v0, ""

    return-object v0

    :cond_1
    if-gez v0, :cond_2

    .line 72
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ec;->so()Lcom/google/android/gms/internal/measurement/ec;

    move-result-object v0

    throw v0

    .line 73
    :cond_2
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ec;->sn()Lcom/google/android/gms/internal/measurement/ec;

    move-result-object v0

    throw v0
.end method

.method public final rf()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 115
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/cx;->rq()J

    move-result-wide v0

    return-wide v0
.end method

.method public final rg()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 116
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/cx;->rn()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/cx;->bU(I)I

    move-result v0

    return v0
.end method

.method public final rh()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 117
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/cx;->ro()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/measurement/cx;->T(J)J

    move-result-wide v0

    return-wide v0
.end method

.method final ri()J
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    :goto_0
    const/16 v3, 0x40

    if-ge v2, v3, :cond_1

    .line 168
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/cx;->rs()B

    move-result v3

    and-int/lit8 v4, v3, 0x7f

    int-to-long v4, v4

    shl-long/2addr v4, v2

    or-long/2addr v0, v4

    and-int/lit16 v3, v3, 0x80

    if-nez v3, :cond_0

    return-wide v0

    :cond_0
    add-int/lit8 v2, v2, 0x7

    goto :goto_0

    .line 173
    :cond_1
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ec;->sp()Lcom/google/android/gms/internal/measurement/ec;

    move-result-object v0

    goto :goto_2

    :goto_1
    throw v0

    :goto_2
    goto :goto_1
.end method

.method public final rj()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 205
    iget v0, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    iget v1, p0, Lcom/google/android/gms/internal/measurement/cx;->limit:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final rk()I
    .locals 2

    .line 206
    iget v0, p0, Lcom/google/android/gms/internal/measurement/cx;->pos:I

    iget v1, p0, Lcom/google/android/gms/internal/measurement/cx;->atf:I

    sub-int/2addr v0, v1

    return v0
.end method
