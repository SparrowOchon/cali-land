.class public final Lcom/google/android/gms/internal/measurement/id;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/measurement/ie;


# static fields
.field private static final ayK:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static final ayL:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static final ayM:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 5
    new-instance v0, Lcom/google/android/gms/internal/measurement/bm;

    const-string v1, "com.google.android.gms.measurement"

    .line 6
    invoke-static {v1}, Lcom/google/android/gms/internal/measurement/bg;->bT(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/measurement/bm;-><init>(Landroid/net/Uri;)V

    const/4 v1, 0x0

    const-string v2, "measurement.log_installs_enabled"

    .line 1015
    invoke-static {v0, v2, v1}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;Z)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v2

    .line 7
    sput-object v2, Lcom/google/android/gms/internal/measurement/id;->ayK:Lcom/google/android/gms/internal/measurement/bf;

    const-string v2, "measurement.log_third_party_store_events_enabled"

    .line 2015
    invoke-static {v0, v2, v1}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;Z)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v2

    .line 8
    sput-object v2, Lcom/google/android/gms/internal/measurement/id;->ayL:Lcom/google/android/gms/internal/measurement/bf;

    const-string v2, "measurement.log_upgrades_enabled"

    .line 3015
    invoke-static {v0, v2, v1}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;Z)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v0

    .line 9
    sput-object v0, Lcom/google/android/gms/internal/measurement/id;->ayM:Lcom/google/android/gms/internal/measurement/bf;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final tB()Z
    .locals 1

    .line 2
    sget-object v0, Lcom/google/android/gms/internal/measurement/id;->ayK:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final tC()Z
    .locals 1

    .line 3
    sget-object v0, Lcom/google/android/gms/internal/measurement/id;->ayL:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final tD()Z
    .locals 1

    .line 4
    sget-object v0, Lcom/google/android/gms/internal/measurement/id;->ayM:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
