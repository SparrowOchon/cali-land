.class public final Lcom/google/android/gms/internal/measurement/q;
.super Lcom/google/android/gms/internal/measurement/lw$a;


# instance fields
.field private final synthetic aqB:Lcom/google/android/gms/internal/measurement/lw;

.field private final synthetic aqP:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/measurement/lw;Z)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/q;->aqB:Lcom/google/android/gms/internal/measurement/lw;

    iput-boolean p2, p0, Lcom/google/android/gms/internal/measurement/q;->aqP:Z

    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/measurement/lw$a;-><init>(Lcom/google/android/gms/internal/measurement/lw;)V

    return-void
.end method


# virtual methods
.method final ov()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/q;->aqB:Lcom/google/android/gms/internal/measurement/lw;

    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/lw;->c(Lcom/google/android/gms/internal/measurement/lw;)Lcom/google/android/gms/internal/measurement/iu;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/internal/measurement/q;->aqP:Z

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/measurement/iu;->setDataCollectionEnabled(Z)V

    return-void
.end method
