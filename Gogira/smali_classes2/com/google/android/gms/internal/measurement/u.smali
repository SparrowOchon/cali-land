.class public final Lcom/google/android/gms/internal/measurement/u;
.super Lcom/google/android/gms/internal/measurement/lw$a;


# instance fields
.field private final synthetic agZ:Ljava/lang/Object;

.field private final synthetic aqB:Lcom/google/android/gms/internal/measurement/lw;

.field private final synthetic aqR:Ljava/lang/String;

.field private final synthetic aqT:Z

.field private final synthetic aqy:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/measurement/lw;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/u;->aqB:Lcom/google/android/gms/internal/measurement/lw;

    iput-object p2, p0, Lcom/google/android/gms/internal/measurement/u;->aqy:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/internal/measurement/u;->aqR:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/internal/measurement/u;->agZ:Ljava/lang/Object;

    const/4 p2, 0x1

    iput-boolean p2, p0, Lcom/google/android/gms/internal/measurement/u;->aqT:Z

    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/measurement/lw$a;-><init>(Lcom/google/android/gms/internal/measurement/lw;)V

    return-void
.end method


# virtual methods
.method final ov()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/u;->aqB:Lcom/google/android/gms/internal/measurement/lw;

    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/lw;->c(Lcom/google/android/gms/internal/measurement/lw;)Lcom/google/android/gms/internal/measurement/iu;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/u;->aqy:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/u;->aqR:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/u;->agZ:Ljava/lang/Object;

    .line 3
    invoke-static {v0}, Lcom/google/android/gms/a/b;->ab(Ljava/lang/Object;)Lcom/google/android/gms/a/a;

    move-result-object v4

    iget-boolean v5, p0, Lcom/google/android/gms/internal/measurement/u;->aqT:Z

    iget-wide v6, p0, Lcom/google/android/gms/internal/measurement/u;->timestamp:J

    .line 4
    invoke-interface/range {v1 .. v7}, Lcom/google/android/gms/internal/measurement/iu;->setUserProperty(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/a/a;ZJ)V

    return-void
.end method
