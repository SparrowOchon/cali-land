.class public final Lcom/google/android/gms/internal/measurement/al$a$a;
.super Lcom/google/android/gms/internal/measurement/dr$a;

# interfaces
.implements Lcom/google/android/gms/internal/measurement/fd;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/measurement/al$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/measurement/dr$a<",
        "Lcom/google/android/gms/internal/measurement/al$a;",
        "Lcom/google/android/gms/internal/measurement/al$a$a;",
        ">;",
        "Lcom/google/android/gms/internal/measurement/fd;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-static {}, Lcom/google/android/gms/internal/measurement/al$a;->pd()Lcom/google/android/gms/internal/measurement/al$a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/measurement/dr$a;-><init>(Lcom/google/android/gms/internal/measurement/dr;)V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/al$a$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final H(Z)Lcom/google/android/gms/internal/measurement/al$a$a;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/dr$a;->se()V

    .line 16
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$a$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$a;

    invoke-static {v0, p1}, Lcom/google/android/gms/internal/measurement/al$a;->a(Lcom/google/android/gms/internal/measurement/al$a;Z)V

    return-object p0
.end method

.method public final a(Lcom/google/android/gms/internal/measurement/al$i$a;)Lcom/google/android/gms/internal/measurement/al$a$a;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/dr$a;->se()V

    .line 8
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$a$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$a;

    invoke-static {v0, p1}, Lcom/google/android/gms/internal/measurement/al$a;->a(Lcom/google/android/gms/internal/measurement/al$a;Lcom/google/android/gms/internal/measurement/al$i$a;)V

    return-object p0
.end method

.method public final a(Lcom/google/android/gms/internal/measurement/al$i;)Lcom/google/android/gms/internal/measurement/al$a$a;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/dr$a;->se()V

    .line 13
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$a$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$a;

    invoke-static {v0, p1}, Lcom/google/android/gms/internal/measurement/al$a;->a(Lcom/google/android/gms/internal/measurement/al$a;Lcom/google/android/gms/internal/measurement/al$i;)V

    return-object p0
.end method

.method public final br(I)Lcom/google/android/gms/internal/measurement/al$a$a;
    .locals 1

    .line 3
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/dr$a;->se()V

    .line 4
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$a$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$a;

    invoke-static {v0, p1}, Lcom/google/android/gms/internal/measurement/al$a;->a(Lcom/google/android/gms/internal/measurement/al$a;I)V

    return-object p0
.end method

.method public final pa()Lcom/google/android/gms/internal/measurement/al$i;
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$a$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$a;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/al$a;->pa()Lcom/google/android/gms/internal/measurement/al$i;

    move-result-object v0

    return-object v0
.end method

.method public final pb()Lcom/google/android/gms/internal/measurement/al$i;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$a$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$a;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/al$a;->pb()Lcom/google/android/gms/internal/measurement/al$i;

    move-result-object v0

    return-object v0
.end method

.method public final pe()Z
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$a$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$a;

    .line 1012
    iget v0, v0, Lcom/google/android/gms/internal/measurement/al$a;->zzue:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
