.class final Lcom/google/android/gms/internal/measurement/x;
.super Lcom/google/android/gms/internal/measurement/lw$a;


# instance fields
.field private final synthetic aqW:Lcom/google/android/gms/internal/measurement/lw$b;

.field private final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/measurement/lw$b;Landroid/app/Activity;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/x;->aqW:Lcom/google/android/gms/internal/measurement/lw$b;

    iput-object p2, p0, Lcom/google/android/gms/internal/measurement/x;->val$activity:Landroid/app/Activity;

    iget-object p1, p1, Lcom/google/android/gms/internal/measurement/lw$b;->aqB:Lcom/google/android/gms/internal/measurement/lw;

    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/measurement/lw$a;-><init>(Lcom/google/android/gms/internal/measurement/lw;)V

    return-void
.end method


# virtual methods
.method final ov()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/x;->aqW:Lcom/google/android/gms/internal/measurement/lw$b;

    iget-object v0, v0, Lcom/google/android/gms/internal/measurement/lw$b;->aqB:Lcom/google/android/gms/internal/measurement/lw;

    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/lw;->c(Lcom/google/android/gms/internal/measurement/lw;)Lcom/google/android/gms/internal/measurement/iu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/x;->val$activity:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/gms/a/b;->ab(Ljava/lang/Object;)Lcom/google/android/gms/a/a;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/gms/internal/measurement/x;->aBr:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/internal/measurement/iu;->onActivityPaused(Lcom/google/android/gms/a/a;J)V

    return-void
.end method
