.class public Lcom/google/android/gms/internal/measurement/dr$a;
.super Lcom/google/android/gms/internal/measurement/cb;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/measurement/dr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MessageType:",
        "Lcom/google/android/gms/internal/measurement/dr<",
        "TMessageType;TBuilderType;>;BuilderType:",
        "Lcom/google/android/gms/internal/measurement/dr$a<",
        "TMessageType;TBuilderType;>;>",
        "Lcom/google/android/gms/internal/measurement/cb<",
        "TMessageType;TBuilderType;>;"
    }
.end annotation


# instance fields
.field private final auX:Lcom/google/android/gms/internal/measurement/dr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TMessageType;"
        }
    .end annotation
.end field

.field protected auY:Lcom/google/android/gms/internal/measurement/dr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TMessageType;"
        }
    .end annotation
.end field

.field private auZ:Z


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/internal/measurement/dr;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMessageType;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/cb;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/dr$a;->auX:Lcom/google/android/gms/internal/measurement/dr;

    .line 3
    sget v0, Lcom/google/android/gms/internal/measurement/dr$d;->avd:I

    .line 5
    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/measurement/dr;->bm(I)Ljava/lang/Object;

    move-result-object p1

    .line 6
    check-cast p1, Lcom/google/android/gms/internal/measurement/dr;

    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/dr$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    const/4 p1, 0x0

    .line 7
    iput-boolean p1, p0, Lcom/google/android/gms/internal/measurement/dr$a;->auZ:Z

    return-void
.end method

.method private static a(Lcom/google/android/gms/internal/measurement/dr;Lcom/google/android/gms/internal/measurement/dr;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMessageType;TMessageType;)V"
        }
    .end annotation

    .line 33
    invoke-static {}, Lcom/google/android/gms/internal/measurement/fm;->sS()Lcom/google/android/gms/internal/measurement/fm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/internal/measurement/fm;->az(Ljava/lang/Object;)Lcom/google/android/gms/internal/measurement/fq;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/google/android/gms/internal/measurement/fq;->g(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method private final b(Lcom/google/android/gms/internal/measurement/cv;Lcom/google/android/gms/internal/measurement/de;)Lcom/google/android/gms/internal/measurement/dr$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/measurement/cv;",
            "Lcom/google/android/gms/internal/measurement/de;",
            ")TBuilderType;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 44
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/dr$a;->se()V

    .line 45
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/internal/measurement/fm;->sS()Lcom/google/android/gms/internal/measurement/fm;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/dr$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/measurement/fm;->az(Ljava/lang/Object;)Lcom/google/android/gms/internal/measurement/fq;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/dr$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    .line 46
    invoke-static {p1}, Lcom/google/android/gms/internal/measurement/cw;->a(Lcom/google/android/gms/internal/measurement/cv;)Lcom/google/android/gms/internal/measurement/cw;

    move-result-object p1

    .line 47
    invoke-interface {v0, v1, p1, p2}, Lcom/google/android/gms/internal/measurement/fq;->a(Ljava/lang/Object;Lcom/google/android/gms/internal/measurement/fr;Lcom/google/android/gms/internal/measurement/de;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p1

    .line 50
    invoke-virtual {p1}, Ljava/lang/RuntimeException;->getCause()Ljava/lang/Throwable;

    move-result-object p2

    instance-of p2, p2, Ljava/io/IOException;

    if-eqz p2, :cond_0

    .line 51
    invoke-virtual {p1}, Ljava/lang/RuntimeException;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    check-cast p1, Ljava/io/IOException;

    throw p1

    .line 52
    :cond_0
    throw p1
.end method

.method private final b([BILcom/google/android/gms/internal/measurement/de;)Lcom/google/android/gms/internal/measurement/dr$a;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BI",
            "Lcom/google/android/gms/internal/measurement/de;",
            ")TBuilderType;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/measurement/ec;
        }
    .end annotation

    .line 35
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/dr$a;->se()V

    .line 36
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/internal/measurement/fm;->sS()Lcom/google/android/gms/internal/measurement/fm;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/dr$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/measurement/fm;->az(Ljava/lang/Object;)Lcom/google/android/gms/internal/measurement/fq;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/dr$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    const/4 v5, 0x0

    add-int/lit8 v6, p2, 0x0

    new-instance v7, Lcom/google/android/gms/internal/measurement/ce;

    invoke-direct {v7, p3}, Lcom/google/android/gms/internal/measurement/ce;-><init>(Lcom/google/android/gms/internal/measurement/de;)V

    move-object v4, p1

    invoke-interface/range {v2 .. v7}, Lcom/google/android/gms/internal/measurement/fq;->a(Ljava/lang/Object;[BIILcom/google/android/gms/internal/measurement/ce;)V
    :try_end_0
    .catch Lcom/google/android/gms/internal/measurement/ec; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p1

    .line 42
    new-instance p2, Ljava/lang/RuntimeException;

    const-string p3, "Reading from byte array should not throw IOException."

    invoke-direct {p2, p3, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2

    .line 40
    :catch_1
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ec;->sn()Lcom/google/android/gms/internal/measurement/ec;

    move-result-object p1

    throw p1

    :catch_2
    move-exception p1

    .line 38
    throw p1
.end method

.method private sf()Lcom/google/android/gms/internal/measurement/dr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TMessageType;"
        }
    .end annotation

    .line 19
    iget-boolean v0, p0, Lcom/google/android/gms/internal/measurement/dr$a;->auZ:Z

    if-eqz v0, :cond_0

    .line 20
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/dr$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    return-object v0

    .line 21
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/dr$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/dr;->qN()V

    const/4 v0, 0x1

    .line 22
    iput-boolean v0, p0, Lcom/google/android/gms/internal/measurement/dr$a;->auZ:Z

    .line 23
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/dr$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    return-object v0
.end method


# virtual methods
.method protected final synthetic a(Lcom/google/android/gms/internal/measurement/bz;)Lcom/google/android/gms/internal/measurement/cb;
    .locals 0

    .line 54
    check-cast p1, Lcom/google/android/gms/internal/measurement/dr;

    .line 55
    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/measurement/dr$a;->a(Lcom/google/android/gms/internal/measurement/dr;)Lcom/google/android/gms/internal/measurement/dr$a;

    move-result-object p1

    return-object p1
.end method

.method public final synthetic a(Lcom/google/android/gms/internal/measurement/cv;Lcom/google/android/gms/internal/measurement/de;)Lcom/google/android/gms/internal/measurement/cb;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 58
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/internal/measurement/dr$a;->b(Lcom/google/android/gms/internal/measurement/cv;Lcom/google/android/gms/internal/measurement/de;)Lcom/google/android/gms/internal/measurement/dr$a;

    move-result-object p1

    return-object p1
.end method

.method public final synthetic a([BILcom/google/android/gms/internal/measurement/de;)Lcom/google/android/gms/internal/measurement/cb;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/measurement/ec;
        }
    .end annotation

    .line 57
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/internal/measurement/dr$a;->b([BILcom/google/android/gms/internal/measurement/de;)Lcom/google/android/gms/internal/measurement/dr$a;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/google/android/gms/internal/measurement/dr;)Lcom/google/android/gms/internal/measurement/dr$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMessageType;)TBuilderType;"
        }
    .end annotation

    .line 30
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/dr$a;->se()V

    .line 31
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/dr$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    invoke-static {v0, p1}, Lcom/google/android/gms/internal/measurement/dr$a;->a(Lcom/google/android/gms/internal/measurement/dr;Lcom/google/android/gms/internal/measurement/dr;)V

    return-object p0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/dr$a;->auX:Lcom/google/android/gms/internal/measurement/dr;

    .line 68
    check-cast v0, Lcom/google/android/gms/internal/measurement/dr;

    .line 69
    sget v1, Lcom/google/android/gms/internal/measurement/dr$d;->ave:I

    .line 70
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/measurement/dr;->bm(I)Ljava/lang/Object;

    move-result-object v0

    .line 71
    check-cast v0, Lcom/google/android/gms/internal/measurement/dr$a;

    .line 2060
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/dr$a;->sf()Lcom/google/android/gms/internal/measurement/dr;

    move-result-object v1

    .line 73
    check-cast v1, Lcom/google/android/gms/internal/measurement/dr;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/measurement/dr$a;->a(Lcom/google/android/gms/internal/measurement/dr;)Lcom/google/android/gms/internal/measurement/dr$a;

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 2

    .line 18
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/dr$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/measurement/dr;->a(Lcom/google/android/gms/internal/measurement/dr;Z)Z

    move-result v0

    return v0
.end method

.method public final synthetic qJ()Lcom/google/android/gms/internal/measurement/cb;
    .locals 1

    .line 59
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/cb;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/dr$a;

    return-object v0
.end method

.method public final synthetic sd()Lcom/google/android/gms/internal/measurement/fb;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/dr$a;->auX:Lcom/google/android/gms/internal/measurement/dr;

    return-object v0
.end method

.method protected final se()V
    .locals 2

    .line 9
    iget-boolean v0, p0, Lcom/google/android/gms/internal/measurement/dr$a;->auZ:Z

    if-eqz v0, :cond_0

    .line 10
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/dr$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    sget v1, Lcom/google/android/gms/internal/measurement/dr$d;->avd:I

    .line 12
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/measurement/dr;->bm(I)Ljava/lang/Object;

    move-result-object v0

    .line 13
    check-cast v0, Lcom/google/android/gms/internal/measurement/dr;

    .line 14
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/dr$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/measurement/dr$a;->a(Lcom/google/android/gms/internal/measurement/dr;Lcom/google/android/gms/internal/measurement/dr;)V

    .line 15
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/dr$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    const/4 v0, 0x0

    .line 16
    iput-boolean v0, p0, Lcom/google/android/gms/internal/measurement/dr$a;->auZ:Z

    :cond_0
    return-void
.end method

.method public final sg()Lcom/google/android/gms/internal/measurement/dr;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TMessageType;"
        }
    .end annotation

    .line 1060
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/dr$a;->sf()Lcom/google/android/gms/internal/measurement/dr;

    move-result-object v0

    .line 24
    check-cast v0, Lcom/google/android/gms/internal/measurement/dr;

    .line 25
    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/dr;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    .line 27
    :cond_0
    new-instance v0, Lcom/google/android/gms/internal/measurement/gj;

    invoke-direct {v0}, Lcom/google/android/gms/internal/measurement/gj;-><init>()V

    .line 28
    throw v0
.end method

.method public final synthetic sh()Lcom/google/android/gms/internal/measurement/fb;
    .locals 1

    .line 60
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/dr$a;->sf()Lcom/google/android/gms/internal/measurement/dr;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic si()Lcom/google/android/gms/internal/measurement/fb;
    .locals 1

    .line 61
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/dr$a;->sg()Lcom/google/android/gms/internal/measurement/dr;

    move-result-object v0

    return-object v0
.end method
