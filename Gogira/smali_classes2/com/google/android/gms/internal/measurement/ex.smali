.class final Lcom/google/android/gms/internal/measurement/ex;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/measurement/eu;


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final aq(Ljava/lang/Object;)Ljava/util/Map;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/Map<",
            "**>;"
        }
    .end annotation

    .line 2
    check-cast p1, Lcom/google/android/gms/internal/measurement/ev;

    return-object p1
.end method

.method public final ar(Ljava/lang/Object;)Ljava/util/Map;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/Map<",
            "**>;"
        }
    .end annotation

    .line 4
    check-cast p1, Lcom/google/android/gms/internal/measurement/ev;

    return-object p1
.end method

.method public final as(Ljava/lang/Object;)Z
    .locals 0

    .line 5
    check-cast p1, Lcom/google/android/gms/internal/measurement/ev;

    .line 1062
    iget-boolean p1, p1, Lcom/google/android/gms/internal/measurement/ev;->zzacz:Z

    if-nez p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public final at(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .line 6
    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/internal/measurement/ev;

    const/4 v1, 0x0

    .line 2060
    iput-boolean v1, v0, Lcom/google/android/gms/internal/measurement/ev;->zzacz:Z

    return-object p1
.end method

.method public final au(Ljava/lang/Object;)I
    .locals 2

    .line 19
    check-cast p1, Lcom/google/android/gms/internal/measurement/ev;

    .line 20
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/ev;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 23
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/ev;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    return v1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map$Entry;

    .line 24
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 25
    new-instance p1, Ljava/lang/NoSuchMethodError;

    invoke-direct {p1}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw p1
.end method

.method public final f(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .line 10
    check-cast p1, Lcom/google/android/gms/internal/measurement/ev;

    .line 11
    check-cast p2, Lcom/google/android/gms/internal/measurement/ev;

    .line 12
    invoke-virtual {p2}, Lcom/google/android/gms/internal/measurement/ev;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2062
    iget-boolean v0, p1, Lcom/google/android/gms/internal/measurement/ev;->zzacz:Z

    if-nez v0, :cond_0

    .line 14
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/ev;->sG()Lcom/google/android/gms/internal/measurement/ev;

    move-result-object p1

    .line 3008
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/ev;->sH()V

    .line 3009
    invoke-virtual {p2}, Lcom/google/android/gms/internal/measurement/ev;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3010
    invoke-virtual {p1, p2}, Lcom/google/android/gms/internal/measurement/ev;->putAll(Ljava/util/Map;)V

    :cond_1
    return-object p1
.end method

.method public final sD()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ev;->sF()Lcom/google/android/gms/internal/measurement/ev;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/ev;->sG()Lcom/google/android/gms/internal/measurement/ev;

    move-result-object v0

    return-object v0
.end method

.method public final sE()Lcom/google/android/gms/internal/measurement/er;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/internal/measurement/er<",
            "**>;"
        }
    .end annotation

    .line 3
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0
.end method
