.class public abstract Lcom/google/android/gms/internal/measurement/hj;
.super Lcom/google/android/gms/internal/measurement/hp;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M:",
        "Lcom/google/android/gms/internal/measurement/hj<",
        "TM;>;>",
        "Lcom/google/android/gms/internal/measurement/hp;"
    }
.end annotation


# instance fields
.field protected ayi:Lcom/google/android/gms/internal/measurement/hl;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/hp;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/internal/measurement/hh;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 9
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/hj;->ayi:Lcom/google/android/gms/internal/measurement/hl;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/hj;->ayi:Lcom/google/android/gms/internal/measurement/hl;

    .line 2037
    iget v1, v1, Lcom/google/android/gms/internal/measurement/hl;->mSize:I

    if-ge v0, v1, :cond_1

    .line 12
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/hj;->ayi:Lcom/google/android/gms/internal/measurement/hl;

    .line 2041
    iget-object v1, v1, Lcom/google/android/gms/internal/measurement/hl;->ayo:[Lcom/google/android/gms/internal/measurement/hk;

    aget-object v1, v1, v0

    .line 13
    invoke-virtual {v1, p1}, Lcom/google/android/gms/internal/measurement/hk;->a(Lcom/google/android/gms/internal/measurement/hh;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected final a(Lcom/google/android/gms/internal/measurement/he;I)Z
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 16
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/he;->getPosition()I

    move-result v0

    .line 17
    invoke-virtual {p1, p2}, Lcom/google/android/gms/internal/measurement/he;->bQ(I)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return v2

    :cond_0
    ushr-int/lit8 v1, p2, 0x3

    .line 22
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/he;->getPosition()I

    move-result v3

    sub-int/2addr v3, v0

    if-nez v3, :cond_1

    .line 2150
    sget-object p1, Lcom/google/android/gms/internal/measurement/hq;->ayB:[B

    goto :goto_0

    .line 2151
    :cond_1
    new-array v4, v3, [B

    .line 2152
    iget v5, p1, Lcom/google/android/gms/internal/measurement/he;->aya:I

    add-int/2addr v5, v0

    .line 2153
    iget-object p1, p1, Lcom/google/android/gms/internal/measurement/he;->buffer:[B

    invoke-static {p1, v5, v4, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object p1, v4

    .line 24
    :goto_0
    new-instance v0, Lcom/google/android/gms/internal/measurement/hr;

    invoke-direct {v0, p2, p1}, Lcom/google/android/gms/internal/measurement/hr;-><init>(I[B)V

    .line 26
    iget-object p1, p0, Lcom/google/android/gms/internal/measurement/hj;->ayi:Lcom/google/android/gms/internal/measurement/hl;

    const/4 p2, 0x0

    if-nez p1, :cond_3

    .line 27
    new-instance p1, Lcom/google/android/gms/internal/measurement/hl;

    invoke-direct {p1}, Lcom/google/android/gms/internal/measurement/hl;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/hj;->ayi:Lcom/google/android/gms/internal/measurement/hl;

    :cond_2
    :goto_1
    move-object p1, p2

    goto :goto_2

    .line 3010
    :cond_3
    invoke-virtual {p1, v1}, Lcom/google/android/gms/internal/measurement/hl;->cK(I)I

    move-result v3

    if-ltz v3, :cond_2

    .line 3011
    iget-object v4, p1, Lcom/google/android/gms/internal/measurement/hl;->ayo:[Lcom/google/android/gms/internal/measurement/hk;

    aget-object v4, v4, v3

    sget-object v5, Lcom/google/android/gms/internal/measurement/hl;->ayl:Lcom/google/android/gms/internal/measurement/hk;

    if-ne v4, v5, :cond_4

    goto :goto_1

    .line 3013
    :cond_4
    iget-object p1, p1, Lcom/google/android/gms/internal/measurement/hl;->ayo:[Lcom/google/android/gms/internal/measurement/hk;

    aget-object p1, p1, v3

    :goto_2
    const/4 v3, 0x1

    if-nez p1, :cond_9

    .line 30
    new-instance p1, Lcom/google/android/gms/internal/measurement/hk;

    invoke-direct {p1}, Lcom/google/android/gms/internal/measurement/hk;-><init>()V

    .line 31
    iget-object v4, p0, Lcom/google/android/gms/internal/measurement/hj;->ayi:Lcom/google/android/gms/internal/measurement/hl;

    .line 3014
    invoke-virtual {v4, v1}, Lcom/google/android/gms/internal/measurement/hl;->cK(I)I

    move-result v5

    if-ltz v5, :cond_5

    .line 3016
    iget-object v1, v4, Lcom/google/android/gms/internal/measurement/hl;->ayo:[Lcom/google/android/gms/internal/measurement/hk;

    aput-object p1, v1, v5

    goto :goto_3

    :cond_5
    xor-int/lit8 v5, v5, -0x1

    .line 3018
    iget v6, v4, Lcom/google/android/gms/internal/measurement/hl;->mSize:I

    if-ge v5, v6, :cond_6

    iget-object v6, v4, Lcom/google/android/gms/internal/measurement/hl;->ayo:[Lcom/google/android/gms/internal/measurement/hk;

    aget-object v6, v6, v5

    sget-object v7, Lcom/google/android/gms/internal/measurement/hl;->ayl:Lcom/google/android/gms/internal/measurement/hk;

    if-ne v6, v7, :cond_6

    .line 3019
    iget-object v2, v4, Lcom/google/android/gms/internal/measurement/hl;->ayn:[I

    aput v1, v2, v5

    .line 3020
    iget-object v1, v4, Lcom/google/android/gms/internal/measurement/hl;->ayo:[Lcom/google/android/gms/internal/measurement/hk;

    aput-object p1, v1, v5

    goto :goto_3

    .line 3022
    :cond_6
    iget v6, v4, Lcom/google/android/gms/internal/measurement/hl;->mSize:I

    iget-object v7, v4, Lcom/google/android/gms/internal/measurement/hl;->ayn:[I

    array-length v7, v7

    if-lt v6, v7, :cond_7

    .line 3023
    iget v6, v4, Lcom/google/android/gms/internal/measurement/hl;->mSize:I

    add-int/2addr v6, v3

    invoke-static {v6}, Lcom/google/android/gms/internal/measurement/hl;->idealIntArraySize(I)I

    move-result v6

    .line 3024
    new-array v7, v6, [I

    .line 3025
    new-array v6, v6, [Lcom/google/android/gms/internal/measurement/hk;

    .line 3026
    iget-object v8, v4, Lcom/google/android/gms/internal/measurement/hl;->ayn:[I

    iget-object v9, v4, Lcom/google/android/gms/internal/measurement/hl;->ayn:[I

    array-length v9, v9

    invoke-static {v8, v2, v7, v2, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3027
    iget-object v8, v4, Lcom/google/android/gms/internal/measurement/hl;->ayo:[Lcom/google/android/gms/internal/measurement/hk;

    iget-object v9, v4, Lcom/google/android/gms/internal/measurement/hl;->ayo:[Lcom/google/android/gms/internal/measurement/hk;

    array-length v9, v9

    invoke-static {v8, v2, v6, v2, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3028
    iput-object v7, v4, Lcom/google/android/gms/internal/measurement/hl;->ayn:[I

    .line 3029
    iput-object v6, v4, Lcom/google/android/gms/internal/measurement/hl;->ayo:[Lcom/google/android/gms/internal/measurement/hk;

    .line 3030
    :cond_7
    iget v2, v4, Lcom/google/android/gms/internal/measurement/hl;->mSize:I

    sub-int/2addr v2, v5

    if-eqz v2, :cond_8

    .line 3031
    iget-object v2, v4, Lcom/google/android/gms/internal/measurement/hl;->ayn:[I

    add-int/lit8 v6, v5, 0x1

    iget v7, v4, Lcom/google/android/gms/internal/measurement/hl;->mSize:I

    sub-int/2addr v7, v5

    invoke-static {v2, v5, v2, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3032
    iget-object v2, v4, Lcom/google/android/gms/internal/measurement/hl;->ayo:[Lcom/google/android/gms/internal/measurement/hk;

    iget v7, v4, Lcom/google/android/gms/internal/measurement/hl;->mSize:I

    sub-int/2addr v7, v5

    invoke-static {v2, v5, v2, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3033
    :cond_8
    iget-object v2, v4, Lcom/google/android/gms/internal/measurement/hl;->ayn:[I

    aput v1, v2, v5

    .line 3034
    iget-object v1, v4, Lcom/google/android/gms/internal/measurement/hl;->ayo:[Lcom/google/android/gms/internal/measurement/hk;

    aput-object p1, v1, v5

    .line 3035
    iget v1, v4, Lcom/google/android/gms/internal/measurement/hl;->mSize:I

    add-int/2addr v1, v3

    iput v1, v4, Lcom/google/android/gms/internal/measurement/hl;->mSize:I

    .line 4004
    :cond_9
    :goto_3
    iget-object v1, p1, Lcom/google/android/gms/internal/measurement/hk;->ayk:Ljava/util/List;

    if-eqz v1, :cond_a

    .line 4005
    iget-object p1, p1, Lcom/google/android/gms/internal/measurement/hk;->ayk:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 4006
    :cond_a
    iget-object v1, p1, Lcom/google/android/gms/internal/measurement/hk;->value:Ljava/lang/Object;

    instance-of v1, v1, Lcom/google/android/gms/internal/measurement/hp;

    if-eqz v1, :cond_c

    .line 4007
    iget-object v0, v0, Lcom/google/android/gms/internal/measurement/hr;->zzado:[B

    .line 4008
    array-length v1, v0

    .line 4009
    invoke-static {v0, v1}, Lcom/google/android/gms/internal/measurement/he;->i([BI)Lcom/google/android/gms/internal/measurement/he;

    move-result-object v1

    .line 4011
    invoke-virtual {v1}, Lcom/google/android/gms/internal/measurement/he;->rn()I

    move-result v2

    .line 4013
    array-length v0, v0

    invoke-static {v2}, Lcom/google/android/gms/internal/measurement/hh;->cl(I)I

    move-result v4

    sub-int/2addr v0, v4

    if-ne v2, v0, :cond_b

    .line 4015
    iget-object v0, p1, Lcom/google/android/gms/internal/measurement/hk;->value:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/internal/measurement/hp;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/measurement/hp;->a(Lcom/google/android/gms/internal/measurement/he;)Lcom/google/android/gms/internal/measurement/hp;

    move-result-object v0

    .line 4030
    iget-object v1, p1, Lcom/google/android/gms/internal/measurement/hk;->ayj:Lcom/google/android/gms/internal/measurement/hi;

    .line 4031
    iput-object v1, p1, Lcom/google/android/gms/internal/measurement/hk;->ayj:Lcom/google/android/gms/internal/measurement/hi;

    .line 4032
    iput-object v0, p1, Lcom/google/android/gms/internal/measurement/hk;->value:Ljava/lang/Object;

    .line 4033
    iput-object p2, p1, Lcom/google/android/gms/internal/measurement/hk;->ayk:Ljava/util/List;

    :goto_4
    return v3

    .line 4014
    :cond_b
    invoke-static {}, Lcom/google/android/gms/internal/measurement/hm;->tv()Lcom/google/android/gms/internal/measurement/hm;

    move-result-object p1

    throw p1

    .line 4016
    :cond_c
    iget-object p2, p1, Lcom/google/android/gms/internal/measurement/hk;->value:Ljava/lang/Object;

    instance-of p2, p2, [Lcom/google/android/gms/internal/measurement/hp;

    if-nez p2, :cond_f

    .line 4020
    iget-object p2, p1, Lcom/google/android/gms/internal/measurement/hk;->value:Ljava/lang/Object;

    instance-of p2, p2, Lcom/google/android/gms/internal/measurement/fb;

    if-nez p2, :cond_e

    .line 4024
    iget-object p1, p1, Lcom/google/android/gms/internal/measurement/hk;->value:Ljava/lang/Object;

    instance-of p1, p1, [Lcom/google/android/gms/internal/measurement/fb;

    if-eqz p1, :cond_d

    .line 4026
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    .line 4027
    new-instance p1, Ljava/lang/NoSuchMethodError;

    invoke-direct {p1}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw p1

    .line 4028
    :cond_d
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    .line 4029
    new-instance p1, Ljava/lang/NoSuchMethodError;

    invoke-direct {p1}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw p1

    .line 4022
    :cond_e
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    .line 4023
    new-instance p1, Ljava/lang/NoSuchMethodError;

    invoke-direct {p1}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw p1

    .line 4018
    :cond_f
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    .line 4019
    new-instance p1, Ljava/lang/NoSuchMethodError;

    invoke-direct {p1}, Ljava/lang/NoSuchMethodError;-><init>()V

    goto :goto_6

    :goto_5
    throw p1

    :goto_6
    goto :goto_5
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 36
    invoke-super {p0}, Lcom/google/android/gms/internal/measurement/hp;->tt()Lcom/google/android/gms/internal/measurement/hp;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/hj;

    .line 37
    invoke-static {p0, v0}, Lcom/google/android/gms/internal/measurement/hn;->a(Lcom/google/android/gms/internal/measurement/hj;Lcom/google/android/gms/internal/measurement/hj;)V

    return-object v0
.end method

.method protected qw()I
    .locals 3

    .line 3
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/hj;->ayi:Lcom/google/android/gms/internal/measurement/hl;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 4
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/hj;->ayi:Lcom/google/android/gms/internal/measurement/hl;

    .line 1037
    iget v2, v2, Lcom/google/android/gms/internal/measurement/hl;->mSize:I

    if-ge v1, v2, :cond_1

    .line 5
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/hj;->ayi:Lcom/google/android/gms/internal/measurement/hl;

    .line 1041
    iget-object v2, v2, Lcom/google/android/gms/internal/measurement/hl;->ayo:[Lcom/google/android/gms/internal/measurement/hk;

    aget-object v2, v2, v1

    .line 6
    invoke-virtual {v2}, Lcom/google/android/gms/internal/measurement/hk;->qw()I

    move-result v2

    add-int/2addr v0, v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    return v0
.end method

.method public final synthetic tt()Lcom/google/android/gms/internal/measurement/hp;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 34
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/hp;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/hj;

    return-object v0
.end method
