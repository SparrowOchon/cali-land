.class public final Lcom/google/android/gms/internal/measurement/la;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/measurement/lb;


# static fields
.field private static final aAK:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static final aAL:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static final aAM:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static final aAN:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static final aAO:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static final ayQ:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 6
    new-instance v0, Lcom/google/android/gms/internal/measurement/bm;

    const-string v1, "com.google.android.gms.measurement"

    .line 7
    invoke-static {v1}, Lcom/google/android/gms/internal/measurement/bg;->bT(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/measurement/bm;-><init>(Landroid/net/Uri;)V

    const/4 v1, 0x1

    const-string v2, "measurement.client.sessions.background_sessions_enabled"

    .line 1015
    invoke-static {v0, v2, v1}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;Z)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v2

    .line 8
    sput-object v2, Lcom/google/android/gms/internal/measurement/la;->aAK:Lcom/google/android/gms/internal/measurement/bf;

    const/4 v2, 0x0

    const-string v3, "measurement.client.sessions.immediate_start_enabled_foreground"

    .line 2015
    invoke-static {v0, v3, v2}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;Z)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v3

    .line 9
    sput-object v3, Lcom/google/android/gms/internal/measurement/la;->aAL:Lcom/google/android/gms/internal/measurement/bf;

    const-string v3, "measurement.client.sessions.immediate_start_enabled"

    .line 3015
    invoke-static {v0, v3, v2}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;Z)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v2

    .line 10
    sput-object v2, Lcom/google/android/gms/internal/measurement/la;->aAM:Lcom/google/android/gms/internal/measurement/bf;

    const-string v2, "measurement.client.sessions.remove_expired_session_properties_enabled"

    .line 4015
    invoke-static {v0, v2, v1}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;Z)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v2

    .line 11
    sput-object v2, Lcom/google/android/gms/internal/measurement/la;->aAN:Lcom/google/android/gms/internal/measurement/bf;

    const-string v2, "measurement.client.sessions.session_id_enabled"

    .line 5015
    invoke-static {v0, v2, v1}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;Z)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v1

    .line 12
    sput-object v1, Lcom/google/android/gms/internal/measurement/la;->aAO:Lcom/google/android/gms/internal/measurement/bf;

    const-string v1, "measurement.id.sessionization_client"

    const-wide/16 v2, 0x0

    .line 6014
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v0

    .line 13
    sput-object v0, Lcom/google/android/gms/internal/measurement/la;->ayQ:Lcom/google/android/gms/internal/measurement/bf;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final uY()Z
    .locals 1

    .line 2
    sget-object v0, Lcom/google/android/gms/internal/measurement/la;->aAK:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final uZ()Z
    .locals 1

    .line 3
    sget-object v0, Lcom/google/android/gms/internal/measurement/la;->aAL:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final va()Z
    .locals 1

    .line 4
    sget-object v0, Lcom/google/android/gms/internal/measurement/la;->aAN:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final vb()Z
    .locals 1

    .line 5
    sget-object v0, Lcom/google/android/gms/internal/measurement/la;->aAO:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
