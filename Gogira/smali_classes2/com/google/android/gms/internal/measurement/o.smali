.class public final Lcom/google/android/gms/internal/measurement/o;
.super Lcom/google/android/gms/internal/measurement/lw$a;


# instance fields
.field private final synthetic aqA:Lcom/google/android/gms/internal/measurement/jv;

.field private final synthetic aqB:Lcom/google/android/gms/internal/measurement/lw;

.field private final synthetic aqy:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/measurement/lw;Ljava/lang/String;Lcom/google/android/gms/internal/measurement/jv;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/o;->aqB:Lcom/google/android/gms/internal/measurement/lw;

    iput-object p2, p0, Lcom/google/android/gms/internal/measurement/o;->aqy:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/internal/measurement/o;->aqA:Lcom/google/android/gms/internal/measurement/jv;

    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/measurement/lw$a;-><init>(Lcom/google/android/gms/internal/measurement/lw;)V

    return-void
.end method


# virtual methods
.method final ov()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/o;->aqB:Lcom/google/android/gms/internal/measurement/lw;

    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/lw;->c(Lcom/google/android/gms/internal/measurement/lw;)Lcom/google/android/gms/internal/measurement/iu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/o;->aqy:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/o;->aqA:Lcom/google/android/gms/internal/measurement/jv;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/internal/measurement/iu;->getMaxUserProperties(Ljava/lang/String;Lcom/google/android/gms/internal/measurement/ln;)V

    return-void
.end method

.method protected final ow()V
    .locals 2

    .line 4
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/o;->aqA:Lcom/google/android/gms/internal/measurement/jv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/measurement/jv;->d(Landroid/os/Bundle;)V

    return-void
.end method
