.class public final Lcom/google/android/gms/internal/measurement/ad$e;
.super Lcom/google/android/gms/internal/measurement/dr;

# interfaces
.implements Lcom/google/android/gms/internal/measurement/fd;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/measurement/ad;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/measurement/ad$e$b;,
        Lcom/google/android/gms/internal/measurement/ad$e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/measurement/dr<",
        "Lcom/google/android/gms/internal/measurement/ad$e;",
        "Lcom/google/android/gms/internal/measurement/ad$e$b;",
        ">;",
        "Lcom/google/android/gms/internal/measurement/fd;"
    }
.end annotation


# static fields
.field private static volatile zzuo:Lcom/google/android/gms/internal/measurement/fk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/fk<",
            "Lcom/google/android/gms/internal/measurement/ad$e;",
            ">;"
        }
    .end annotation
.end field

.field private static final zzvp:Lcom/google/android/gms/internal/measurement/ad$e;


# instance fields
.field public zzue:I

.field private zzvl:I

.field public zzvm:Ljava/lang/String;

.field public zzvn:Z

.field public zzvo:Lcom/google/android/gms/internal/measurement/dz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/dz<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 36
    new-instance v0, Lcom/google/android/gms/internal/measurement/ad$e;

    invoke-direct {v0}, Lcom/google/android/gms/internal/measurement/ad$e;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/measurement/ad$e;->zzvp:Lcom/google/android/gms/internal/measurement/ad$e;

    .line 37
    const-class v0, Lcom/google/android/gms/internal/measurement/ad$e;

    sget-object v1, Lcom/google/android/gms/internal/measurement/ad$e;->zzvp:Lcom/google/android/gms/internal/measurement/ad$e;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/measurement/dr;->a(Ljava/lang/Class;Lcom/google/android/gms/internal/measurement/dr;)V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/dr;-><init>()V

    const-string v0, ""

    .line 2
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/ad$e;->zzvm:Ljava/lang/String;

    .line 1093
    invoke-static {}, Lcom/google/android/gms/internal/measurement/fp;->sT()Lcom/google/android/gms/internal/measurement/fp;

    move-result-object v0

    .line 3
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/ad$e;->zzvo:Lcom/google/android/gms/internal/measurement/dz;

    return-void
.end method

.method public static oX()Lcom/google/android/gms/internal/measurement/ad$e;
    .locals 1

    .line 34
    sget-object v0, Lcom/google/android/gms/internal/measurement/ad$e;->zzvp:Lcom/google/android/gms/internal/measurement/ad$e;

    return-object v0
.end method

.method static synthetic oY()Lcom/google/android/gms/internal/measurement/ad$e;
    .locals 1

    .line 35
    sget-object v0, Lcom/google/android/gms/internal/measurement/ad$e;->zzvp:Lcom/google/android/gms/internal/measurement/ad$e;

    return-object v0
.end method


# virtual methods
.method protected final bm(I)Ljava/lang/Object;
    .locals 3

    .line 14
    sget-object v0, Lcom/google/android/gms/internal/measurement/ac;->aqY:[I

    const/4 v1, 0x1

    sub-int/2addr p1, v1

    aget p1, v0, p1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    .line 33
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    const/4 p1, 0x0

    return-object p1

    .line 31
    :pswitch_1
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    .line 22
    :pswitch_2
    sget-object p1, Lcom/google/android/gms/internal/measurement/ad$e;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    if-nez p1, :cond_1

    .line 24
    const-class v0, Lcom/google/android/gms/internal/measurement/ad$e;

    monitor-enter v0

    .line 25
    :try_start_0
    sget-object p1, Lcom/google/android/gms/internal/measurement/ad$e;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    if-nez p1, :cond_0

    .line 27
    new-instance p1, Lcom/google/android/gms/internal/measurement/dr$c;

    sget-object v1, Lcom/google/android/gms/internal/measurement/ad$e;->zzvp:Lcom/google/android/gms/internal/measurement/ad$e;

    invoke-direct {p1, v1}, Lcom/google/android/gms/internal/measurement/dr$c;-><init>(Lcom/google/android/gms/internal/measurement/dr;)V

    .line 28
    sput-object p1, Lcom/google/android/gms/internal/measurement/ad$e;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    .line 29
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    .line 21
    :pswitch_3
    sget-object p1, Lcom/google/android/gms/internal/measurement/ad$e;->zzvp:Lcom/google/android/gms/internal/measurement/ad$e;

    return-object p1

    :pswitch_4
    const/4 p1, 0x6

    new-array p1, p1, [Ljava/lang/Object;

    const-string v2, "zzue"

    aput-object v2, p1, v0

    const-string v0, "zzvl"

    aput-object v0, p1, v1

    const/4 v0, 0x2

    .line 18
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ad$e$a;->oQ()Lcom/google/android/gms/internal/measurement/dy;

    move-result-object v1

    aput-object v1, p1, v0

    const/4 v0, 0x3

    const-string v1, "zzvm"

    aput-object v1, p1, v0

    const/4 v0, 0x4

    const-string v1, "zzvn"

    aput-object v1, p1, v0

    const/4 v0, 0x5

    const-string v1, "zzvo"

    aput-object v1, p1, v0

    .line 20
    sget-object v0, Lcom/google/android/gms/internal/measurement/ad$e;->zzvp:Lcom/google/android/gms/internal/measurement/ad$e;

    const-string v1, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0001\u0000\u0001\u000c\u0000\u0002\u0008\u0001\u0003\u0007\u0002\u0004\u001a"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/internal/measurement/ad$e;->a(Lcom/google/android/gms/internal/measurement/fb;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 16
    :pswitch_5
    new-instance p1, Lcom/google/android/gms/internal/measurement/ad$e$b;

    invoke-direct {p1, v0}, Lcom/google/android/gms/internal/measurement/ad$e$b;-><init>(B)V

    return-object p1

    .line 15
    :pswitch_6
    new-instance p1, Lcom/google/android/gms/internal/measurement/ad$e;

    invoke-direct {p1}, Lcom/google/android/gms/internal/measurement/ad$e;-><init>()V

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final oT()Z
    .locals 2

    .line 5
    iget v0, p0, Lcom/google/android/gms/internal/measurement/ad$e;->zzue:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final oU()Lcom/google/android/gms/internal/measurement/ad$e$a;
    .locals 1

    .line 6
    iget v0, p0, Lcom/google/android/gms/internal/measurement/ad$e;->zzvl:I

    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/ad$e$a;->bp(I)Lcom/google/android/gms/internal/measurement/ad$e$a;

    move-result-object v0

    if-nez v0, :cond_0

    .line 7
    sget-object v0, Lcom/google/android/gms/internal/measurement/ad$e$a;->arg:Lcom/google/android/gms/internal/measurement/ad$e$a;

    :cond_0
    return-object v0
.end method

.method public final oV()Z
    .locals 1

    .line 8
    iget v0, p0, Lcom/google/android/gms/internal/measurement/ad$e;->zzue:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final oW()I
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/ad$e;->zzvo:Lcom/google/android/gms/internal/measurement/dz;

    invoke-interface {v0}, Lcom/google/android/gms/internal/measurement/dz;->size()I

    move-result v0

    return v0
.end method
