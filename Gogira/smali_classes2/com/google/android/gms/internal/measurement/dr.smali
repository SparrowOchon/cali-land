.class public abstract Lcom/google/android/gms/internal/measurement/dr;
.super Lcom/google/android/gms/internal/measurement/bz;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/measurement/dr$c;,
        Lcom/google/android/gms/internal/measurement/dr$e;,
        Lcom/google/android/gms/internal/measurement/dr$b;,
        Lcom/google/android/gms/internal/measurement/dr$a;,
        Lcom/google/android/gms/internal/measurement/dr$d;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MessageType:",
        "Lcom/google/android/gms/internal/measurement/dr<",
        "TMessageType;TBuilderType;>;BuilderType:",
        "Lcom/google/android/gms/internal/measurement/dr$a<",
        "TMessageType;TBuilderType;>;>",
        "Lcom/google/android/gms/internal/measurement/bz<",
        "TMessageType;TBuilderType;>;"
    }
.end annotation


# static fields
.field private static zzaib:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Lcom/google/android/gms/internal/measurement/dr<",
            "**>;>;"
        }
    .end annotation
.end field


# instance fields
.field protected zzahz:Lcom/google/android/gms/internal/measurement/gl;

.field private zzaia:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 160
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/measurement/dr;->zzaib:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/bz;-><init>()V

    .line 2
    invoke-static {}, Lcom/google/android/gms/internal/measurement/gl;->ti()Lcom/google/android/gms/internal/measurement/gl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/dr;->zzahz:Lcom/google/android/gms/internal/measurement/gl;

    const/4 v0, -0x1

    .line 3
    iput v0, p0, Lcom/google/android/gms/internal/measurement/dr;->zzaia:I

    return-void
.end method

.method static a(Lcom/google/android/gms/internal/measurement/dr;Lcom/google/android/gms/internal/measurement/cv;Lcom/google/android/gms/internal/measurement/de;)Lcom/google/android/gms/internal/measurement/dr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/internal/measurement/dr<",
            "TT;*>;>(TT;",
            "Lcom/google/android/gms/internal/measurement/cv;",
            "Lcom/google/android/gms/internal/measurement/de;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/measurement/ec;
        }
    .end annotation

    .line 98
    sget v0, Lcom/google/android/gms/internal/measurement/dr$d;->avd:I

    .line 99
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/measurement/dr;->bm(I)Ljava/lang/Object;

    move-result-object p0

    .line 100
    check-cast p0, Lcom/google/android/gms/internal/measurement/dr;

    .line 101
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/internal/measurement/fm;->sS()Lcom/google/android/gms/internal/measurement/fm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/internal/measurement/fm;->az(Ljava/lang/Object;)Lcom/google/android/gms/internal/measurement/fq;

    move-result-object v0

    .line 102
    invoke-static {p1}, Lcom/google/android/gms/internal/measurement/cw;->a(Lcom/google/android/gms/internal/measurement/cv;)Lcom/google/android/gms/internal/measurement/cw;

    move-result-object p1

    .line 103
    invoke-interface {v0, p0, p1, p2}, Lcom/google/android/gms/internal/measurement/fq;->a(Ljava/lang/Object;Lcom/google/android/gms/internal/measurement/fr;Lcom/google/android/gms/internal/measurement/de;)V

    .line 104
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/dr;->qN()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 111
    invoke-virtual {p0}, Ljava/lang/RuntimeException;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    instance-of p1, p1, Lcom/google/android/gms/internal/measurement/ec;

    if-eqz p1, :cond_0

    .line 112
    invoke-virtual {p0}, Ljava/lang/RuntimeException;->getCause()Ljava/lang/Throwable;

    move-result-object p0

    check-cast p0, Lcom/google/android/gms/internal/measurement/ec;

    throw p0

    .line 113
    :cond_0
    throw p0

    :catch_1
    move-exception p1

    .line 107
    invoke-virtual {p1}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object p2

    instance-of p2, p2, Lcom/google/android/gms/internal/measurement/ec;

    if-eqz p2, :cond_1

    .line 108
    invoke-virtual {p1}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object p0

    check-cast p0, Lcom/google/android/gms/internal/measurement/ec;

    throw p0

    .line 109
    :cond_1
    new-instance p2, Lcom/google/android/gms/internal/measurement/ec;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/google/android/gms/internal/measurement/ec;-><init>(Ljava/lang/String;)V

    .line 2004
    iput-object p0, p2, Lcom/google/android/gms/internal/measurement/ec;->zzaiw:Lcom/google/android/gms/internal/measurement/fb;

    .line 109
    throw p2
.end method

.method private static a(Lcom/google/android/gms/internal/measurement/dr;[BILcom/google/android/gms/internal/measurement/de;)Lcom/google/android/gms/internal/measurement/dr;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/internal/measurement/dr<",
            "TT;*>;>(TT;[BI",
            "Lcom/google/android/gms/internal/measurement/de;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/measurement/ec;
        }
    .end annotation

    .line 115
    sget v0, Lcom/google/android/gms/internal/measurement/dr$d;->avd:I

    .line 116
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/measurement/dr;->bm(I)Ljava/lang/Object;

    move-result-object p0

    .line 117
    check-cast p0, Lcom/google/android/gms/internal/measurement/dr;

    .line 118
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/internal/measurement/fm;->sS()Lcom/google/android/gms/internal/measurement/fm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/internal/measurement/fm;->az(Ljava/lang/Object;)Lcom/google/android/gms/internal/measurement/fq;

    move-result-object v0

    const/4 v3, 0x0

    new-instance v5, Lcom/google/android/gms/internal/measurement/ce;

    invoke-direct {v5, p3}, Lcom/google/android/gms/internal/measurement/ce;-><init>(Lcom/google/android/gms/internal/measurement/de;)V

    move-object v1, p0

    move-object v2, p1

    move v4, p2

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/internal/measurement/fq;->a(Ljava/lang/Object;[BIILcom/google/android/gms/internal/measurement/ce;)V

    .line 119
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/dr;->qN()V

    .line 120
    iget p1, p0, Lcom/google/android/gms/internal/measurement/dr;->zzact:I

    if-nez p1, :cond_0

    return-object p0

    .line 121
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    invoke-direct {p1}, Ljava/lang/RuntimeException;-><init>()V

    throw p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    :catch_0
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ec;->sn()Lcom/google/android/gms/internal/measurement/ec;

    move-result-object p1

    .line 4004
    iput-object p0, p1, Lcom/google/android/gms/internal/measurement/ec;->zzaiw:Lcom/google/android/gms/internal/measurement/fb;

    .line 128
    throw p1

    :catch_1
    move-exception p1

    .line 124
    invoke-virtual {p1}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object p2

    instance-of p2, p2, Lcom/google/android/gms/internal/measurement/ec;

    if-eqz p2, :cond_1

    .line 125
    invoke-virtual {p1}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object p0

    check-cast p0, Lcom/google/android/gms/internal/measurement/ec;

    throw p0

    .line 126
    :cond_1
    new-instance p2, Lcom/google/android/gms/internal/measurement/ec;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/google/android/gms/internal/measurement/ec;-><init>(Ljava/lang/String;)V

    .line 3004
    iput-object p0, p2, Lcom/google/android/gms/internal/measurement/ec;->zzaiw:Lcom/google/android/gms/internal/measurement/fb;

    .line 126
    throw p2
.end method

.method protected static a(Lcom/google/android/gms/internal/measurement/dr;[BLcom/google/android/gms/internal/measurement/de;)Lcom/google/android/gms/internal/measurement/dr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/internal/measurement/dr<",
            "TT;*>;>(TT;[B",
            "Lcom/google/android/gms/internal/measurement/de;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/measurement/ec;
        }
    .end annotation

    .line 130
    array-length v0, p1

    .line 131
    invoke-static {p0, p1, v0, p2}, Lcom/google/android/gms/internal/measurement/dr;->a(Lcom/google/android/gms/internal/measurement/dr;[BILcom/google/android/gms/internal/measurement/de;)Lcom/google/android/gms/internal/measurement/dr;

    move-result-object p0

    if-eqz p0, :cond_1

    const/4 p1, 0x1

    .line 4023
    invoke-static {p0, p1}, Lcom/google/android/gms/internal/measurement/dr;->a(Lcom/google/android/gms/internal/measurement/dr;Z)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 136
    :cond_0
    new-instance p1, Lcom/google/android/gms/internal/measurement/gj;

    invoke-direct {p1}, Lcom/google/android/gms/internal/measurement/gj;-><init>()V

    .line 138
    new-instance p2, Lcom/google/android/gms/internal/measurement/ec;

    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/gj;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/google/android/gms/internal/measurement/ec;-><init>(Ljava/lang/String;)V

    .line 5004
    iput-object p0, p2, Lcom/google/android/gms/internal/measurement/ec;->zzaiw:Lcom/google/android/gms/internal/measurement/fb;

    .line 140
    throw p2

    :cond_1
    :goto_0
    return-object p0
.end method

.method protected static a(Lcom/google/android/gms/internal/measurement/dz;)Lcom/google/android/gms/internal/measurement/dz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/gms/internal/measurement/dz<",
            "TE;>;)",
            "Lcom/google/android/gms/internal/measurement/dz<",
            "TE;>;"
        }
    .end annotation

    .line 94
    invoke-interface {p0}, Lcom/google/android/gms/internal/measurement/dz;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0xa

    goto :goto_0

    :cond_0
    shl-int/lit8 v0, v0, 0x1

    .line 97
    :goto_0
    invoke-interface {p0, v0}, Lcom/google/android/gms/internal/measurement/dz;->bK(I)Lcom/google/android/gms/internal/measurement/dz;

    move-result-object p0

    return-object p0
.end method

.method protected static a(Lcom/google/android/gms/internal/measurement/ea;)Lcom/google/android/gms/internal/measurement/ea;
    .locals 1

    .line 89
    invoke-interface {p0}, Lcom/google/android/gms/internal/measurement/ea;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0xa

    goto :goto_0

    :cond_0
    shl-int/lit8 v0, v0, 0x1

    .line 92
    :goto_0
    invoke-interface {p0, v0}, Lcom/google/android/gms/internal/measurement/ea;->cv(I)Lcom/google/android/gms/internal/measurement/ea;

    move-result-object p0

    return-object p0
.end method

.method protected static a(Lcom/google/android/gms/internal/measurement/fb;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .line 62
    new-instance v0, Lcom/google/android/gms/internal/measurement/fo;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/internal/measurement/fo;-><init>(Lcom/google/android/gms/internal/measurement/fb;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0
.end method

.method static varargs a(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 63
    :try_start_0
    invoke-virtual {p0, p1, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 66
    invoke-virtual {p0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object p0

    .line 67
    instance-of p1, p0, Ljava/lang/RuntimeException;

    if-nez p1, :cond_1

    .line 69
    instance-of p1, p0, Ljava/lang/Error;

    if-eqz p1, :cond_0

    .line 70
    check-cast p0, Ljava/lang/Error;

    throw p0

    .line 71
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "Unexpected exception thrown by generated accessor method."

    invoke-direct {p1, p2, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1

    .line 68
    :cond_1
    check-cast p0, Ljava/lang/RuntimeException;

    throw p0

    :catch_1
    move-exception p0

    .line 65
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "Couldn\'t use Java reflection to implement protocol message reflection."

    invoke-direct {p1, p2, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1
.end method

.method protected static a(Ljava/lang/Class;Lcom/google/android/gms/internal/measurement/dr;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/internal/measurement/dr<",
            "**>;>(",
            "Ljava/lang/Class<",
            "TT;>;TT;)V"
        }
    .end annotation

    .line 60
    sget-object v0, Lcom/google/android/gms/internal/measurement/dr;->zzaib:Ljava/util/Map;

    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method protected static final a(Lcom/google/android/gms/internal/measurement/dr;Z)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/internal/measurement/dr<",
            "TT;*>;>(TT;Z)Z"
        }
    .end annotation

    .line 72
    sget v0, Lcom/google/android/gms/internal/measurement/dr$d;->ava:I

    .line 74
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/measurement/dr;->bm(I)Ljava/lang/Object;

    move-result-object v0

    .line 75
    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    return v1

    :cond_0
    if-nez v0, :cond_1

    const/4 p0, 0x0

    return p0

    .line 80
    :cond_1
    invoke-static {}, Lcom/google/android/gms/internal/measurement/fm;->sS()Lcom/google/android/gms/internal/measurement/fm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/internal/measurement/fm;->az(Ljava/lang/Object;)Lcom/google/android/gms/internal/measurement/fq;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/internal/measurement/fq;->ay(Ljava/lang/Object;)Z

    move-result v0

    if-eqz p1, :cond_2

    .line 82
    sget p1, Lcom/google/android/gms/internal/measurement/dr$d;->avb:I

    .line 85
    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/measurement/dr;->bm(I)Ljava/lang/Object;

    :cond_2
    return v0
.end method

.method static r(Ljava/lang/Class;)Lcom/google/android/gms/internal/measurement/dr;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/internal/measurement/dr<",
            "**>;>(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    .line 43
    sget-object v0, Lcom/google/android/gms/internal/measurement/dr;->zzaib:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/dr;

    if-nez v0, :cond_0

    .line 45
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-static {v0, v1, v2}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    sget-object v0, Lcom/google/android/gms/internal/measurement/dr;->zzaib:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/dr;

    goto :goto_0

    :catch_0
    move-exception p0

    .line 48
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Class initialization cannot fail."

    invoke-direct {v0, v1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 51
    invoke-static {p0}, Lcom/google/android/gms/internal/measurement/go;->v(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/dr;

    .line 52
    sget v1, Lcom/google/android/gms/internal/measurement/dr$d;->avf:I

    .line 53
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/measurement/dr;->bm(I)Ljava/lang/Object;

    move-result-object v0

    .line 54
    check-cast v0, Lcom/google/android/gms/internal/measurement/dr;

    if-eqz v0, :cond_1

    .line 58
    sget-object v1, Lcom/google/android/gms/internal/measurement/dr;->zzaib:Ljava/util/Map;

    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 57
    :cond_1
    new-instance p0, Ljava/lang/IllegalStateException;

    invoke-direct {p0}, Ljava/lang/IllegalStateException;-><init>()V

    throw p0

    :cond_2
    :goto_1
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/internal/measurement/cy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 33
    invoke-static {}, Lcom/google/android/gms/internal/measurement/fm;->sS()Lcom/google/android/gms/internal/measurement/fm;

    move-result-object v0

    .line 34
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 35
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/measurement/fm;->t(Ljava/lang/Class;)Lcom/google/android/gms/internal/measurement/fq;

    move-result-object v0

    .line 2001
    iget-object v1, p1, Lcom/google/android/gms/internal/measurement/cy;->atj:Lcom/google/android/gms/internal/measurement/db;

    if-eqz v1, :cond_0

    .line 2002
    iget-object p1, p1, Lcom/google/android/gms/internal/measurement/cy;->atj:Lcom/google/android/gms/internal/measurement/db;

    goto :goto_0

    .line 2003
    :cond_0
    new-instance v1, Lcom/google/android/gms/internal/measurement/db;

    invoke-direct {v1, p1}, Lcom/google/android/gms/internal/measurement/db;-><init>(Lcom/google/android/gms/internal/measurement/cy;)V

    move-object p1, v1

    .line 36
    :goto_0
    invoke-interface {v0, p0, p1}, Lcom/google/android/gms/internal/measurement/fq;->a(Ljava/lang/Object;Lcom/google/android/gms/internal/measurement/hf;)V

    return-void
.end method

.method final bH(I)V
    .locals 0

    .line 30
    iput p1, p0, Lcom/google/android/gms/internal/measurement/dr;->zzaia:I

    return-void
.end method

.method protected abstract bm(I)Ljava/lang/Object;
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    .line 12
    :cond_0
    sget v0, Lcom/google/android/gms/internal/measurement/dr$d;->avf:I

    .line 13
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/measurement/dr;->bm(I)Ljava/lang/Object;

    move-result-object v0

    .line 14
    check-cast v0, Lcom/google/android/gms/internal/measurement/dr;

    .line 15
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    .line 17
    :cond_1
    invoke-static {}, Lcom/google/android/gms/internal/measurement/fm;->sS()Lcom/google/android/gms/internal/measurement/fm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/internal/measurement/fm;->az(Ljava/lang/Object;)Lcom/google/android/gms/internal/measurement/fq;

    move-result-object v0

    check-cast p1, Lcom/google/android/gms/internal/measurement/dr;

    invoke-interface {v0, p0, p1}, Lcom/google/android/gms/internal/measurement/fq;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 5
    iget v0, p0, Lcom/google/android/gms/internal/measurement/dr;->zzact:I

    if-eqz v0, :cond_0

    .line 6
    iget v0, p0, Lcom/google/android/gms/internal/measurement/dr;->zzact:I

    return v0

    .line 7
    :cond_0
    invoke-static {}, Lcom/google/android/gms/internal/measurement/fm;->sS()Lcom/google/android/gms/internal/measurement/fm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/internal/measurement/fm;->az(Ljava/lang/Object;)Lcom/google/android/gms/internal/measurement/fq;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/internal/measurement/fq;->hashCode(Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/measurement/dr;->zzact:I

    .line 8
    iget v0, p0, Lcom/google/android/gms/internal/measurement/dr;->zzact:I

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    .line 23
    invoke-static {p0, v0}, Lcom/google/android/gms/internal/measurement/dr;->a(Lcom/google/android/gms/internal/measurement/dr;Z)Z

    move-result v0

    return v0
.end method

.method final qI()I
    .locals 1

    .line 29
    iget v0, p0, Lcom/google/android/gms/internal/measurement/dr;->zzaia:I

    return v0
.end method

.method protected final qN()V
    .locals 1

    .line 18
    invoke-static {}, Lcom/google/android/gms/internal/measurement/fm;->sS()Lcom/google/android/gms/internal/measurement/fm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/internal/measurement/fm;->az(Ljava/lang/Object;)Lcom/google/android/gms/internal/measurement/fq;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/internal/measurement/fq;->an(Ljava/lang/Object;)V

    return-void
.end method

.method protected final rY()Lcom/google/android/gms/internal/measurement/dr$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<MessageType:",
            "Lcom/google/android/gms/internal/measurement/dr<",
            "TMessageType;TBuilderType;>;BuilderType:",
            "Lcom/google/android/gms/internal/measurement/dr$a<",
            "TMessageType;TBuilderType;>;>()TBuilderType;"
        }
    .end annotation

    .line 20
    sget v0, Lcom/google/android/gms/internal/measurement/dr$d;->ave:I

    .line 21
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/measurement/dr;->bm(I)Ljava/lang/Object;

    move-result-object v0

    .line 22
    check-cast v0, Lcom/google/android/gms/internal/measurement/dr$a;

    return-object v0
.end method

.method public final rZ()Lcom/google/android/gms/internal/measurement/dr$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TBuilderType;"
        }
    .end annotation

    .line 24
    sget v0, Lcom/google/android/gms/internal/measurement/dr$d;->ave:I

    .line 25
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/measurement/dr;->bm(I)Ljava/lang/Object;

    move-result-object v0

    .line 26
    check-cast v0, Lcom/google/android/gms/internal/measurement/dr$a;

    .line 27
    invoke-virtual {v0, p0}, Lcom/google/android/gms/internal/measurement/dr$a;->a(Lcom/google/android/gms/internal/measurement/dr;)Lcom/google/android/gms/internal/measurement/dr$a;

    return-object v0
.end method

.method public final sa()I
    .locals 2

    .line 38
    iget v0, p0, Lcom/google/android/gms/internal/measurement/dr;->zzaia:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 40
    invoke-static {}, Lcom/google/android/gms/internal/measurement/fm;->sS()Lcom/google/android/gms/internal/measurement/fm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/internal/measurement/fm;->az(Ljava/lang/Object;)Lcom/google/android/gms/internal/measurement/fq;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/internal/measurement/fq;->aw(Ljava/lang/Object;)I

    move-result v0

    .line 41
    iput v0, p0, Lcom/google/android/gms/internal/measurement/dr;->zzaia:I

    .line 42
    :cond_0
    iget v0, p0, Lcom/google/android/gms/internal/measurement/dr;->zzaia:I

    return v0
.end method

.method public final synthetic sb()Lcom/google/android/gms/internal/measurement/fa;
    .locals 1

    .line 144
    sget v0, Lcom/google/android/gms/internal/measurement/dr$d;->ave:I

    .line 145
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/measurement/dr;->bm(I)Ljava/lang/Object;

    move-result-object v0

    .line 146
    check-cast v0, Lcom/google/android/gms/internal/measurement/dr$a;

    .line 147
    invoke-virtual {v0, p0}, Lcom/google/android/gms/internal/measurement/dr$a;->a(Lcom/google/android/gms/internal/measurement/dr;)Lcom/google/android/gms/internal/measurement/dr$a;

    return-object v0
.end method

.method public final synthetic sc()Lcom/google/android/gms/internal/measurement/fa;
    .locals 1

    .line 151
    sget v0, Lcom/google/android/gms/internal/measurement/dr$d;->ave:I

    .line 152
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/measurement/dr;->bm(I)Ljava/lang/Object;

    move-result-object v0

    .line 153
    check-cast v0, Lcom/google/android/gms/internal/measurement/dr$a;

    return-object v0
.end method

.method public final synthetic sd()Lcom/google/android/gms/internal/measurement/fb;
    .locals 1

    .line 156
    sget v0, Lcom/google/android/gms/internal/measurement/dr$d;->avf:I

    .line 157
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/measurement/dr;->bm(I)Ljava/lang/Object;

    move-result-object v0

    .line 158
    check-cast v0, Lcom/google/android/gms/internal/measurement/dr;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 4
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1001
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "# "

    .line 1002
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    .line 1003
    invoke-static {p0, v1, v0}, Lcom/google/android/gms/internal/measurement/fc;->a(Lcom/google/android/gms/internal/measurement/fb;Ljava/lang/StringBuilder;I)V

    .line 1004
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
