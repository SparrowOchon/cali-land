.class abstract Lcom/google/android/gms/internal/measurement/lw$a;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/measurement/lw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x400
    name = "a"
.end annotation


# instance fields
.field final aBr:J

.field private final aBs:Z

.field private final synthetic aqB:Lcom/google/android/gms/internal/measurement/lw;

.field final timestamp:J


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/measurement/lw;)V
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/internal/measurement/lw$a;-><init>(Lcom/google/android/gms/internal/measurement/lw;Z)V

    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/internal/measurement/lw;Z)V
    .locals 2

    .line 3
    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/lw$a;->aqB:Lcom/google/android/gms/internal/measurement/lw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iget-object v0, p1, Lcom/google/android/gms/internal/measurement/lw;->aBe:Lcom/google/android/gms/common/util/e;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/e;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/internal/measurement/lw$a;->timestamp:J

    .line 5
    iget-object p1, p1, Lcom/google/android/gms/internal/measurement/lw;->aBe:Lcom/google/android/gms/common/util/e;

    invoke-interface {p1}, Lcom/google/android/gms/common/util/e;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/internal/measurement/lw$a;->aBr:J

    .line 6
    iput-boolean p2, p0, Lcom/google/android/gms/internal/measurement/lw$a;->aBs:Z

    return-void
.end method


# virtual methods
.method abstract ov()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method protected ow()V
    .locals 0

    return-void
.end method

.method public run()V
    .locals 4

    .line 8
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/lw$a;->aqB:Lcom/google/android/gms/internal/measurement/lw;

    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/lw;->a(Lcom/google/android/gms/internal/measurement/lw;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/lw$a;->ow()V

    return-void

    .line 11
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/lw$a;->ov()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    .line 14
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/lw$a;->aqB:Lcom/google/android/gms/internal/measurement/lw;

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/google/android/gms/internal/measurement/lw$a;->aBs:Z

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/internal/measurement/lw;->a(Lcom/google/android/gms/internal/measurement/lw;Ljava/lang/Exception;ZZ)V

    .line 15
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/lw$a;->ow()V

    return-void
.end method
