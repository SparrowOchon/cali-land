.class public final Lcom/google/android/gms/internal/measurement/ad$a;
.super Lcom/google/android/gms/internal/measurement/dr;

# interfaces
.implements Lcom/google/android/gms/internal/measurement/fd;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/measurement/ad;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/measurement/ad$a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/measurement/dr<",
        "Lcom/google/android/gms/internal/measurement/ad$a;",
        "Lcom/google/android/gms/internal/measurement/ad$a$a;",
        ">;",
        "Lcom/google/android/gms/internal/measurement/fd;"
    }
.end annotation


# static fields
.field private static final zzun:Lcom/google/android/gms/internal/measurement/ad$a;

.field private static volatile zzuo:Lcom/google/android/gms/internal/measurement/fk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/fk<",
            "Lcom/google/android/gms/internal/measurement/ad$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public zzue:I

.field public zzuf:I

.field public zzug:Ljava/lang/String;

.field public zzuh:Lcom/google/android/gms/internal/measurement/dz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/dz<",
            "Lcom/google/android/gms/internal/measurement/ad$b;",
            ">;"
        }
    .end annotation
.end field

.field private zzui:Z

.field private zzuj:Lcom/google/android/gms/internal/measurement/ad$c;

.field public zzuk:Z

.field public zzul:Z

.field public zzum:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 58
    new-instance v0, Lcom/google/android/gms/internal/measurement/ad$a;

    invoke-direct {v0}, Lcom/google/android/gms/internal/measurement/ad$a;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/measurement/ad$a;->zzun:Lcom/google/android/gms/internal/measurement/ad$a;

    .line 59
    const-class v0, Lcom/google/android/gms/internal/measurement/ad$a;

    sget-object v1, Lcom/google/android/gms/internal/measurement/ad$a;->zzun:Lcom/google/android/gms/internal/measurement/ad$a;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/measurement/dr;->a(Ljava/lang/Class;Lcom/google/android/gms/internal/measurement/dr;)V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/dr;-><init>()V

    const-string v0, ""

    .line 2
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/ad$a;->zzug:Ljava/lang/String;

    .line 1093
    invoke-static {}, Lcom/google/android/gms/internal/measurement/fp;->sT()Lcom/google/android/gms/internal/measurement/fp;

    move-result-object v0

    .line 3
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/ad$a;->zzuh:Lcom/google/android/gms/internal/measurement/dz;

    return-void
.end method

.method public static a([BLcom/google/android/gms/internal/measurement/de;)Lcom/google/android/gms/internal/measurement/ad$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/measurement/ec;
        }
    .end annotation

    .line 30
    sget-object v0, Lcom/google/android/gms/internal/measurement/ad$a;->zzun:Lcom/google/android/gms/internal/measurement/ad$a;

    invoke-static {v0, p0, p1}, Lcom/google/android/gms/internal/measurement/dr;->a(Lcom/google/android/gms/internal/measurement/dr;[BLcom/google/android/gms/internal/measurement/de;)Lcom/google/android/gms/internal/measurement/dr;

    move-result-object p0

    check-cast p0, Lcom/google/android/gms/internal/measurement/ad$a;

    return-object p0
.end method

.method static synthetic a(Lcom/google/android/gms/internal/measurement/ad$a;ILcom/google/android/gms/internal/measurement/ad$b;)V
    .locals 1

    if-eqz p2, :cond_1

    .line 2019
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/ad$a;->zzuh:Lcom/google/android/gms/internal/measurement/dz;

    invoke-interface {v0}, Lcom/google/android/gms/internal/measurement/dz;->qM()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2020
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/ad$a;->zzuh:Lcom/google/android/gms/internal/measurement/dz;

    .line 2021
    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/dr;->a(Lcom/google/android/gms/internal/measurement/dz;)Lcom/google/android/gms/internal/measurement/dz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/ad$a;->zzuh:Lcom/google/android/gms/internal/measurement/dz;

    .line 2022
    :cond_0
    iget-object p0, p0, Lcom/google/android/gms/internal/measurement/ad$a;->zzuh:Lcom/google/android/gms/internal/measurement/dz;

    invoke-interface {p0, p1, p2}, Lcom/google/android/gms/internal/measurement/dz;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-void

    .line 2017
    :cond_1
    new-instance p0, Ljava/lang/NullPointerException;

    invoke-direct {p0}, Ljava/lang/NullPointerException;-><init>()V

    throw p0
.end method

.method static synthetic a(Lcom/google/android/gms/internal/measurement/ad$a;Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 2010
    iget v0, p0, Lcom/google/android/gms/internal/measurement/ad$a;->zzue:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/gms/internal/measurement/ad$a;->zzue:I

    .line 2011
    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/ad$a;->zzug:Ljava/lang/String;

    return-void

    .line 2009
    :cond_0
    new-instance p0, Ljava/lang/NullPointerException;

    invoke-direct {p0}, Ljava/lang/NullPointerException;-><init>()V

    throw p0
.end method

.method static synthetic oA()Lcom/google/android/gms/internal/measurement/ad$a;
    .locals 1

    .line 55
    sget-object v0, Lcom/google/android/gms/internal/measurement/ad$a;->zzun:Lcom/google/android/gms/internal/measurement/ad$a;

    return-object v0
.end method

.method public static oz()Lcom/google/android/gms/internal/measurement/fk;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/internal/measurement/fk<",
            "Lcom/google/android/gms/internal/measurement/ad$a;",
            ">;"
        }
    .end annotation

    .line 50
    sget-object v0, Lcom/google/android/gms/internal/measurement/ad$a;->zzun:Lcom/google/android/gms/internal/measurement/ad$a;

    .line 51
    sget v1, Lcom/google/android/gms/internal/measurement/dr$d;->avg:I

    .line 52
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/measurement/dr;->bm(I)Ljava/lang/Object;

    move-result-object v0

    .line 53
    check-cast v0, Lcom/google/android/gms/internal/measurement/fk;

    return-object v0
.end method


# virtual methods
.method protected final bm(I)Ljava/lang/Object;
    .locals 3

    .line 31
    sget-object v0, Lcom/google/android/gms/internal/measurement/ac;->aqY:[I

    const/4 v1, 0x1

    sub-int/2addr p1, v1

    aget p1, v0, p1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    .line 49
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    const/4 p1, 0x0

    return-object p1

    .line 47
    :pswitch_1
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    .line 38
    :pswitch_2
    sget-object p1, Lcom/google/android/gms/internal/measurement/ad$a;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    if-nez p1, :cond_1

    .line 40
    const-class v0, Lcom/google/android/gms/internal/measurement/ad$a;

    monitor-enter v0

    .line 41
    :try_start_0
    sget-object p1, Lcom/google/android/gms/internal/measurement/ad$a;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    if-nez p1, :cond_0

    .line 43
    new-instance p1, Lcom/google/android/gms/internal/measurement/dr$c;

    sget-object v1, Lcom/google/android/gms/internal/measurement/ad$a;->zzun:Lcom/google/android/gms/internal/measurement/ad$a;

    invoke-direct {p1, v1}, Lcom/google/android/gms/internal/measurement/dr$c;-><init>(Lcom/google/android/gms/internal/measurement/dr;)V

    .line 44
    sput-object p1, Lcom/google/android/gms/internal/measurement/ad$a;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    .line 45
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    .line 37
    :pswitch_3
    sget-object p1, Lcom/google/android/gms/internal/measurement/ad$a;->zzun:Lcom/google/android/gms/internal/measurement/ad$a;

    return-object p1

    :pswitch_4
    const/16 p1, 0xa

    new-array p1, p1, [Ljava/lang/Object;

    const-string v2, "zzue"

    aput-object v2, p1, v0

    const-string v0, "zzuf"

    aput-object v0, p1, v1

    const/4 v0, 0x2

    const-string v1, "zzug"

    aput-object v1, p1, v0

    const/4 v0, 0x3

    const-string v1, "zzuh"

    aput-object v1, p1, v0

    const/4 v0, 0x4

    .line 34
    const-class v1, Lcom/google/android/gms/internal/measurement/ad$b;

    aput-object v1, p1, v0

    const/4 v0, 0x5

    const-string v1, "zzui"

    aput-object v1, p1, v0

    const/4 v0, 0x6

    const-string v1, "zzuj"

    aput-object v1, p1, v0

    const/4 v0, 0x7

    const-string v1, "zzuk"

    aput-object v1, p1, v0

    const/16 v0, 0x8

    const-string v1, "zzul"

    aput-object v1, p1, v0

    const/16 v0, 0x9

    const-string v1, "zzum"

    aput-object v1, p1, v0

    .line 36
    sget-object v0, Lcom/google/android/gms/internal/measurement/ad$a;->zzun:Lcom/google/android/gms/internal/measurement/ad$a;

    const-string v1, "\u0001\u0008\u0000\u0001\u0001\u0008\u0008\u0000\u0001\u0000\u0001\u0004\u0000\u0002\u0008\u0001\u0003\u001b\u0004\u0007\u0002\u0005\t\u0003\u0006\u0007\u0004\u0007\u0007\u0005\u0008\u0007\u0006"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/internal/measurement/ad$a;->a(Lcom/google/android/gms/internal/measurement/fb;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 33
    :pswitch_5
    new-instance p1, Lcom/google/android/gms/internal/measurement/ad$a$a;

    invoke-direct {p1, v0}, Lcom/google/android/gms/internal/measurement/ad$a$a;-><init>(B)V

    return-object p1

    .line 32
    :pswitch_6
    new-instance p1, Lcom/google/android/gms/internal/measurement/ad$a;

    invoke-direct {p1}, Lcom/google/android/gms/internal/measurement/ad$a;-><init>()V

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final ox()Z
    .locals 2

    .line 5
    iget v0, p0, Lcom/google/android/gms/internal/measurement/ad$a;->zzue:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final oy()Lcom/google/android/gms/internal/measurement/ad$c;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/ad$a;->zzuj:Lcom/google/android/gms/internal/measurement/ad$c;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/internal/measurement/ad$c;->oN()Lcom/google/android/gms/internal/measurement/ad$c;

    move-result-object v0

    :cond_0
    return-object v0
.end method
