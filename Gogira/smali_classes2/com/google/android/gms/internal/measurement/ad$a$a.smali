.class public final Lcom/google/android/gms/internal/measurement/ad$a$a;
.super Lcom/google/android/gms/internal/measurement/dr$a;

# interfaces
.implements Lcom/google/android/gms/internal/measurement/fd;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/measurement/ad$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/measurement/dr$a<",
        "Lcom/google/android/gms/internal/measurement/ad$a;",
        "Lcom/google/android/gms/internal/measurement/ad$a$a;",
        ">;",
        "Lcom/google/android/gms/internal/measurement/fd;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ad$a;->oA()Lcom/google/android/gms/internal/measurement/ad$a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/measurement/dr$a;-><init>(Lcom/google/android/gms/internal/measurement/dr;)V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .line 12
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/ad$a$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILcom/google/android/gms/internal/measurement/ad$b;)Lcom/google/android/gms/internal/measurement/ad$a$a;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/dr$a;->se()V

    .line 10
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/ad$a$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/ad$a;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/internal/measurement/ad$a;->a(Lcom/google/android/gms/internal/measurement/ad$a;ILcom/google/android/gms/internal/measurement/ad$b;)V

    return-object p0
.end method

.method public final bn(I)Lcom/google/android/gms/internal/measurement/ad$b;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/ad$a$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/ad$a;

    .line 1015
    iget-object v0, v0, Lcom/google/android/gms/internal/measurement/ad$a;->zzuh:Lcom/google/android/gms/internal/measurement/dz;

    invoke-interface {v0, p1}, Lcom/google/android/gms/internal/measurement/dz;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/internal/measurement/ad$b;

    return-object p1
.end method

.method public final bt(Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/ad$a$a;
    .locals 1

    .line 4
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/dr$a;->se()V

    .line 5
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/ad$a$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/ad$a;

    invoke-static {v0, p1}, Lcom/google/android/gms/internal/measurement/ad$a;->a(Lcom/google/android/gms/internal/measurement/ad$a;Ljava/lang/String;)V

    return-object p0
.end method

.method public final oB()Ljava/lang/String;
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/ad$a$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/ad$a;

    .line 1007
    iget-object v0, v0, Lcom/google/android/gms/internal/measurement/ad$a;->zzug:Ljava/lang/String;

    return-object v0
.end method

.method public final oC()I
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/ad$a$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/ad$a;

    .line 1014
    iget-object v0, v0, Lcom/google/android/gms/internal/measurement/ad$a;->zzuh:Lcom/google/android/gms/internal/measurement/dz;

    invoke-interface {v0}, Lcom/google/android/gms/internal/measurement/dz;->size()I

    move-result v0

    return v0
.end method
