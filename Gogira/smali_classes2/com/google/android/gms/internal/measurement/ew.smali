.class final Lcom/google/android/gms/internal/measurement/ew;
.super Ljava/lang/Object;


# static fields
.field private static final avY:Lcom/google/android/gms/internal/measurement/eu;

.field private static final avZ:Lcom/google/android/gms/internal/measurement/eu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ew;->sK()Lcom/google/android/gms/internal/measurement/eu;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/measurement/ew;->avY:Lcom/google/android/gms/internal/measurement/eu;

    .line 8
    new-instance v0, Lcom/google/android/gms/internal/measurement/ex;

    invoke-direct {v0}, Lcom/google/android/gms/internal/measurement/ex;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/measurement/ew;->avZ:Lcom/google/android/gms/internal/measurement/eu;

    return-void
.end method

.method static sI()Lcom/google/android/gms/internal/measurement/eu;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/android/gms/internal/measurement/ew;->avY:Lcom/google/android/gms/internal/measurement/eu;

    return-object v0
.end method

.method static sJ()Lcom/google/android/gms/internal/measurement/eu;
    .locals 1

    .line 2
    sget-object v0, Lcom/google/android/gms/internal/measurement/ew;->avZ:Lcom/google/android/gms/internal/measurement/eu;

    return-object v0
.end method

.method private static sK()Lcom/google/android/gms/internal/measurement/eu;
    .locals 3

    :try_start_0
    const-string v0, "com.google.protobuf.MapFieldSchemaFull"

    .line 3
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Class;

    .line 4
    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/eu;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method
