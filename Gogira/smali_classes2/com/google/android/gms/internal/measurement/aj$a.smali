.class public final Lcom/google/android/gms/internal/measurement/aj$a;
.super Lcom/google/android/gms/internal/measurement/dr;

# interfaces
.implements Lcom/google/android/gms/internal/measurement/fd;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/measurement/aj;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/measurement/aj$a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/measurement/dr<",
        "Lcom/google/android/gms/internal/measurement/aj$a;",
        "Lcom/google/android/gms/internal/measurement/aj$a$a;",
        ">;",
        "Lcom/google/android/gms/internal/measurement/fd;"
    }
.end annotation


# static fields
.field private static volatile zzuo:Lcom/google/android/gms/internal/measurement/fk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/fk<",
            "Lcom/google/android/gms/internal/measurement/aj$a;",
            ">;"
        }
    .end annotation
.end field

.field private static final zzwa:Lcom/google/android/gms/internal/measurement/aj$a;


# instance fields
.field private zzue:I

.field public zzvy:Ljava/lang/String;

.field public zzvz:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 32
    new-instance v0, Lcom/google/android/gms/internal/measurement/aj$a;

    invoke-direct {v0}, Lcom/google/android/gms/internal/measurement/aj$a;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/measurement/aj$a;->zzwa:Lcom/google/android/gms/internal/measurement/aj$a;

    .line 33
    const-class v0, Lcom/google/android/gms/internal/measurement/aj$a;

    sget-object v1, Lcom/google/android/gms/internal/measurement/aj$a;->zzwa:Lcom/google/android/gms/internal/measurement/aj$a;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/measurement/dr;->a(Ljava/lang/Class;Lcom/google/android/gms/internal/measurement/dr;)V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/dr;-><init>()V

    const-string v0, ""

    .line 2
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/aj$a;->zzvy:Ljava/lang/String;

    .line 3
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/aj$a;->zzvz:Ljava/lang/String;

    return-void
.end method

.method static synthetic oZ()Lcom/google/android/gms/internal/measurement/aj$a;
    .locals 1

    .line 31
    sget-object v0, Lcom/google/android/gms/internal/measurement/aj$a;->zzwa:Lcom/google/android/gms/internal/measurement/aj$a;

    return-object v0
.end method

.method public static oz()Lcom/google/android/gms/internal/measurement/fk;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/internal/measurement/fk<",
            "Lcom/google/android/gms/internal/measurement/aj$a;",
            ">;"
        }
    .end annotation

    .line 26
    sget-object v0, Lcom/google/android/gms/internal/measurement/aj$a;->zzwa:Lcom/google/android/gms/internal/measurement/aj$a;

    .line 27
    sget v1, Lcom/google/android/gms/internal/measurement/dr$d;->avg:I

    .line 28
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/measurement/dr;->bm(I)Ljava/lang/Object;

    move-result-object v0

    .line 29
    check-cast v0, Lcom/google/android/gms/internal/measurement/fk;

    return-object v0
.end method


# virtual methods
.method protected final bm(I)Ljava/lang/Object;
    .locals 3

    .line 7
    sget-object v0, Lcom/google/android/gms/internal/measurement/ai;->aqY:[I

    const/4 v1, 0x1

    sub-int/2addr p1, v1

    aget p1, v0, p1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    .line 25
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    const/4 p1, 0x0

    return-object p1

    .line 23
    :pswitch_1
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    .line 14
    :pswitch_2
    sget-object p1, Lcom/google/android/gms/internal/measurement/aj$a;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    if-nez p1, :cond_1

    .line 16
    const-class v0, Lcom/google/android/gms/internal/measurement/aj$a;

    monitor-enter v0

    .line 17
    :try_start_0
    sget-object p1, Lcom/google/android/gms/internal/measurement/aj$a;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    if-nez p1, :cond_0

    .line 19
    new-instance p1, Lcom/google/android/gms/internal/measurement/dr$c;

    sget-object v1, Lcom/google/android/gms/internal/measurement/aj$a;->zzwa:Lcom/google/android/gms/internal/measurement/aj$a;

    invoke-direct {p1, v1}, Lcom/google/android/gms/internal/measurement/dr$c;-><init>(Lcom/google/android/gms/internal/measurement/dr;)V

    .line 20
    sput-object p1, Lcom/google/android/gms/internal/measurement/aj$a;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    .line 21
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    .line 13
    :pswitch_3
    sget-object p1, Lcom/google/android/gms/internal/measurement/aj$a;->zzwa:Lcom/google/android/gms/internal/measurement/aj$a;

    return-object p1

    :pswitch_4
    const/4 p1, 0x3

    new-array p1, p1, [Ljava/lang/Object;

    const-string v2, "zzue"

    aput-object v2, p1, v0

    const-string v0, "zzvy"

    aput-object v0, p1, v1

    const/4 v0, 0x2

    const-string v1, "zzvz"

    aput-object v1, p1, v0

    .line 12
    sget-object v0, Lcom/google/android/gms/internal/measurement/aj$a;->zzwa:Lcom/google/android/gms/internal/measurement/aj$a;

    const-string v1, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0008\u0000\u0002\u0008\u0001"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/internal/measurement/aj$a;->a(Lcom/google/android/gms/internal/measurement/fb;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 9
    :pswitch_5
    new-instance p1, Lcom/google/android/gms/internal/measurement/aj$a$a;

    invoke-direct {p1, v0}, Lcom/google/android/gms/internal/measurement/aj$a$a;-><init>(B)V

    return-object p1

    .line 8
    :pswitch_6
    new-instance p1, Lcom/google/android/gms/internal/measurement/aj$a;

    invoke-direct {p1}, Lcom/google/android/gms/internal/measurement/aj$a;-><init>()V

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
