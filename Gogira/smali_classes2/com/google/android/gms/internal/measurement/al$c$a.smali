.class public final Lcom/google/android/gms/internal/measurement/al$c$a;
.super Lcom/google/android/gms/internal/measurement/dr$a;

# interfaces
.implements Lcom/google/android/gms/internal/measurement/fd;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/measurement/al$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/measurement/dr$a<",
        "Lcom/google/android/gms/internal/measurement/al$c;",
        "Lcom/google/android/gms/internal/measurement/al$c$a;",
        ">;",
        "Lcom/google/android/gms/internal/measurement/fd;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-static {}, Lcom/google/android/gms/internal/measurement/al$c;->pn()Lcom/google/android/gms/internal/measurement/al$c;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/measurement/dr$a;-><init>(Lcom/google/android/gms/internal/measurement/dr;)V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/al$c$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final A(J)Lcom/google/android/gms/internal/measurement/al$c$a;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/dr$a;->se()V

    .line 30
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$c$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$c;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/internal/measurement/al$c;->a(Lcom/google/android/gms/internal/measurement/al$c;J)V

    return-object p0
.end method

.method public final B(J)Lcom/google/android/gms/internal/measurement/al$c$a;
    .locals 1

    .line 33
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/dr$a;->se()V

    .line 34
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$c$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$c;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/internal/measurement/al$c;->b(Lcom/google/android/gms/internal/measurement/al$c;J)V

    return-object p0
.end method

.method public final a(ILcom/google/android/gms/internal/measurement/al$e$a;)Lcom/google/android/gms/internal/measurement/al$c$a;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/dr$a;->se()V

    .line 12
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$c$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$c;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/internal/measurement/al$c;->a(Lcom/google/android/gms/internal/measurement/al$c;ILcom/google/android/gms/internal/measurement/al$e$a;)V

    return-object p0
.end method

.method public final a(ILcom/google/android/gms/internal/measurement/al$e;)Lcom/google/android/gms/internal/measurement/al$c$a;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/dr$a;->se()V

    .line 9
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$c$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$c;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/internal/measurement/al$c;->a(Lcom/google/android/gms/internal/measurement/al$c;ILcom/google/android/gms/internal/measurement/al$e;)V

    return-object p0
.end method

.method public final a(Lcom/google/android/gms/internal/measurement/al$e$a;)Lcom/google/android/gms/internal/measurement/al$c$a;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/dr$a;->se()V

    .line 18
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$c$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$c;

    invoke-static {v0, p1}, Lcom/google/android/gms/internal/measurement/al$c;->a(Lcom/google/android/gms/internal/measurement/al$c;Lcom/google/android/gms/internal/measurement/al$e$a;)V

    return-object p0
.end method

.method public final a(Lcom/google/android/gms/internal/measurement/al$e;)Lcom/google/android/gms/internal/measurement/al$c$a;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/dr$a;->se()V

    .line 15
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$c$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$c;

    invoke-static {v0, p1}, Lcom/google/android/gms/internal/measurement/al$c;->a(Lcom/google/android/gms/internal/measurement/al$c;Lcom/google/android/gms/internal/measurement/al$e;)V

    return-object p0
.end method

.method public final bt(I)Lcom/google/android/gms/internal/measurement/al$e;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$c$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$c;

    .line 1007
    iget-object v0, v0, Lcom/google/android/gms/internal/measurement/al$c;->zzwj:Lcom/google/android/gms/internal/measurement/dz;

    invoke-interface {v0, p1}, Lcom/google/android/gms/internal/measurement/dz;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/internal/measurement/al$e;

    return-object p1
.end method

.method public final bu(I)Lcom/google/android/gms/internal/measurement/al$c$a;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/dr$a;->se()V

    .line 21
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$c$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$c;

    invoke-static {v0, p1}, Lcom/google/android/gms/internal/measurement/al$c;->a(Lcom/google/android/gms/internal/measurement/al$c;I)V

    return-object p0
.end method

.method public final bw(Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/al$c$a;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/dr$a;->se()V

    .line 25
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$c$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$c;

    invoke-static {v0, p1}, Lcom/google/android/gms/internal/measurement/al$c;->a(Lcom/google/android/gms/internal/measurement/al$c;Ljava/lang/String;)V

    return-object p0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$c$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$c;

    .line 1031
    iget-object v0, v0, Lcom/google/android/gms/internal/measurement/al$c;->zzwk:Ljava/lang/String;

    return-object v0
.end method

.method public final getTimestampMillis()J
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$c$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$c;

    .line 1038
    iget-wide v0, v0, Lcom/google/android/gms/internal/measurement/al$c;->zzwl:J

    return-wide v0
.end method

.method public final pj()I
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$c$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$c;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/al$c;->pj()I

    move-result v0

    return v0
.end method

.method public final pl()Z
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$c$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$c;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/al$c;->pl()Z

    move-result v0

    return v0
.end method

.method public final po()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/google/android/gms/internal/measurement/al$e;",
            ">;"
        }
    .end annotation

    .line 3
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$c$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$c;

    .line 1005
    iget-object v0, v0, Lcom/google/android/gms/internal/measurement/al$c;->zzwj:Lcom/google/android/gms/internal/measurement/dz;

    .line 5
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final pp()J
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$c$a;->auY:Lcom/google/android/gms/internal/measurement/dr;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$c;

    .line 1043
    iget-wide v0, v0, Lcom/google/android/gms/internal/measurement/al$c;->zzwm:J

    return-wide v0
.end method
