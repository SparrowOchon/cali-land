.class public final Lcom/google/android/gms/internal/measurement/zzx;
.super Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/google/android/gms/internal/measurement/zzx;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final aAV:J

.field public final aAW:J

.field public final aAX:Z

.field public final aAY:Ljava/lang/String;

.field public final aAZ:Ljava/lang/String;

.field public final aBa:Ljava/lang/String;

.field public final aBb:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/google/android/gms/internal/measurement/lu;

    invoke-direct {v0}, Lcom/google/android/gms/internal/measurement/lu;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzx;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JJZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;-><init>()V

    .line 2
    iput-wide p1, p0, Lcom/google/android/gms/internal/measurement/zzx;->aAV:J

    .line 3
    iput-wide p3, p0, Lcom/google/android/gms/internal/measurement/zzx;->aAW:J

    .line 4
    iput-boolean p5, p0, Lcom/google/android/gms/internal/measurement/zzx;->aAX:Z

    .line 5
    iput-object p6, p0, Lcom/google/android/gms/internal/measurement/zzx;->aAY:Ljava/lang/String;

    .line 6
    iput-object p7, p0, Lcom/google/android/gms/internal/measurement/zzx;->aAZ:Ljava/lang/String;

    .line 7
    iput-object p8, p0, Lcom/google/android/gms/internal/measurement/zzx;->aBa:Ljava/lang/String;

    .line 8
    iput-object p9, p0, Lcom/google/android/gms/internal/measurement/zzx;->aBb:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    const/16 p2, 0x4f45

    .line 1017
    invoke-static {p1, p2}, Lcom/google/android/gms/common/internal/safeparcel/b;->l(Landroid/os/Parcel;I)I

    move-result p2

    .line 12
    iget-wide v0, p0, Lcom/google/android/gms/internal/measurement/zzx;->aAV:J

    const/4 v2, 0x1

    invoke-static {p1, v2, v0, v1}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;IJ)V

    .line 13
    iget-wide v0, p0, Lcom/google/android/gms/internal/measurement/zzx;->aAW:J

    const/4 v2, 0x2

    invoke-static {p1, v2, v0, v1}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;IJ)V

    .line 14
    iget-boolean v0, p0, Lcom/google/android/gms/internal/measurement/zzx;->aAX:Z

    const/4 v1, 0x3

    invoke-static {p1, v1, v0}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;IZ)V

    .line 15
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzx;->aAY:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-static {p1, v1, v0}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    .line 16
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzx;->aAZ:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-static {p1, v1, v0}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    .line 17
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzx;->aBa:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-static {p1, v1, v0}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    .line 18
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzx;->aBb:Landroid/os/Bundle;

    const/4 v1, 0x7

    invoke-static {p1, v1, v0}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;ILandroid/os/Bundle;)V

    .line 1018
    invoke-static {p1, p2}, Lcom/google/android/gms/common/internal/safeparcel/b;->m(Landroid/os/Parcel;I)V

    return-void
.end method
