.class public final Lcom/google/android/gms/internal/measurement/al$k;
.super Lcom/google/android/gms/internal/measurement/dr;

# interfaces
.implements Lcom/google/android/gms/internal/measurement/fd;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/measurement/al;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "k"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/measurement/al$k$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/measurement/dr<",
        "Lcom/google/android/gms/internal/measurement/al$k;",
        "Lcom/google/android/gms/internal/measurement/al$k$a;",
        ">;",
        "Lcom/google/android/gms/internal/measurement/fd;"
    }
.end annotation


# static fields
.field private static volatile zzuo:Lcom/google/android/gms/internal/measurement/fk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/fk<",
            "Lcom/google/android/gms/internal/measurement/al$k;",
            ">;"
        }
    .end annotation
.end field

.field private static final zzzd:Lcom/google/android/gms/internal/measurement/al$k;


# instance fields
.field private zzue:I

.field public zzwk:Ljava/lang/String;

.field public zzwp:J

.field public zzwr:Ljava/lang/String;

.field private zzws:F

.field public zzwt:D

.field public zzzc:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 74
    new-instance v0, Lcom/google/android/gms/internal/measurement/al$k;

    invoke-direct {v0}, Lcom/google/android/gms/internal/measurement/al$k;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/measurement/al$k;->zzzd:Lcom/google/android/gms/internal/measurement/al$k;

    .line 75
    const-class v0, Lcom/google/android/gms/internal/measurement/al$k;

    sget-object v1, Lcom/google/android/gms/internal/measurement/al$k;->zzzd:Lcom/google/android/gms/internal/measurement/al$k;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/measurement/dr;->a(Ljava/lang/Class;Lcom/google/android/gms/internal/measurement/dr;)V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/dr;-><init>()V

    const-string v0, ""

    .line 2
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzwk:Ljava/lang/String;

    .line 3
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzwr:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/measurement/al$k;D)V
    .locals 1

    .line 1039
    iget v0, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzue:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzue:I

    .line 1040
    iput-wide p1, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzwt:D

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/measurement/al$k;J)V
    .locals 1

    .line 1007
    iget v0, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzue:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzue:I

    .line 1008
    iput-wide p1, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzzc:J

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/measurement/al$k;Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 1013
    iget v0, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzue:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzue:I

    .line 1014
    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzwk:Ljava/lang/String;

    return-void

    .line 1012
    :cond_0
    new-instance p0, Ljava/lang/NullPointerException;

    invoke-direct {p0}, Ljava/lang/NullPointerException;-><init>()V

    throw p0
.end method

.method static synthetic b(Lcom/google/android/gms/internal/measurement/al$k;)V
    .locals 1

    .line 1023
    iget v0, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzue:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzue:I

    .line 1025
    sget-object v0, Lcom/google/android/gms/internal/measurement/al$k;->zzzd:Lcom/google/android/gms/internal/measurement/al$k;

    .line 1026
    iget-object v0, v0, Lcom/google/android/gms/internal/measurement/al$k;->zzwr:Ljava/lang/String;

    .line 1027
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzwr:Ljava/lang/String;

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/internal/measurement/al$k;J)V
    .locals 1

    .line 1031
    iget v0, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzue:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzue:I

    .line 1032
    iput-wide p1, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzwp:J

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/internal/measurement/al$k;Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 1020
    iget v0, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzue:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzue:I

    .line 1021
    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzwr:Ljava/lang/String;

    return-void

    .line 1019
    :cond_0
    new-instance p0, Ljava/lang/NullPointerException;

    invoke-direct {p0}, Ljava/lang/NullPointerException;-><init>()V

    throw p0
.end method

.method static synthetic c(Lcom/google/android/gms/internal/measurement/al$k;)V
    .locals 2

    .line 1034
    iget v0, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzue:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzue:I

    const-wide/16 v0, 0x0

    .line 1035
    iput-wide v0, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzwp:J

    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/internal/measurement/al$k;)V
    .locals 2

    .line 1042
    iget v0, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzue:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzue:I

    const-wide/16 v0, 0x0

    .line 1043
    iput-wide v0, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzwt:D

    return-void
.end method

.method public static qq()Lcom/google/android/gms/internal/measurement/al$k$a;
    .locals 1

    .line 45
    sget-object v0, Lcom/google/android/gms/internal/measurement/al$k;->zzzd:Lcom/google/android/gms/internal/measurement/al$k;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/dr;->rY()Lcom/google/android/gms/internal/measurement/dr$a;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$k$a;

    return-object v0
.end method

.method static synthetic qr()Lcom/google/android/gms/internal/measurement/al$k;
    .locals 1

    .line 65
    sget-object v0, Lcom/google/android/gms/internal/measurement/al$k;->zzzd:Lcom/google/android/gms/internal/measurement/al$k;

    return-object v0
.end method


# virtual methods
.method protected final bm(I)Ljava/lang/Object;
    .locals 3

    .line 46
    sget-object v0, Lcom/google/android/gms/internal/measurement/ak;->aqY:[I

    const/4 v1, 0x1

    sub-int/2addr p1, v1

    aget p1, v0, p1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    .line 64
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    const/4 p1, 0x0

    return-object p1

    .line 62
    :pswitch_1
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    .line 53
    :pswitch_2
    sget-object p1, Lcom/google/android/gms/internal/measurement/al$k;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    if-nez p1, :cond_1

    .line 55
    const-class v0, Lcom/google/android/gms/internal/measurement/al$k;

    monitor-enter v0

    .line 56
    :try_start_0
    sget-object p1, Lcom/google/android/gms/internal/measurement/al$k;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    if-nez p1, :cond_0

    .line 58
    new-instance p1, Lcom/google/android/gms/internal/measurement/dr$c;

    sget-object v1, Lcom/google/android/gms/internal/measurement/al$k;->zzzd:Lcom/google/android/gms/internal/measurement/al$k;

    invoke-direct {p1, v1}, Lcom/google/android/gms/internal/measurement/dr$c;-><init>(Lcom/google/android/gms/internal/measurement/dr;)V

    .line 59
    sput-object p1, Lcom/google/android/gms/internal/measurement/al$k;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    .line 60
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    .line 52
    :pswitch_3
    sget-object p1, Lcom/google/android/gms/internal/measurement/al$k;->zzzd:Lcom/google/android/gms/internal/measurement/al$k;

    return-object p1

    :pswitch_4
    const/4 p1, 0x7

    new-array p1, p1, [Ljava/lang/Object;

    const-string v2, "zzue"

    aput-object v2, p1, v0

    const-string v0, "zzzc"

    aput-object v0, p1, v1

    const/4 v0, 0x2

    const-string v1, "zzwk"

    aput-object v1, p1, v0

    const/4 v0, 0x3

    const-string v1, "zzwr"

    aput-object v1, p1, v0

    const/4 v0, 0x4

    const-string v1, "zzwp"

    aput-object v1, p1, v0

    const/4 v0, 0x5

    const-string v1, "zzws"

    aput-object v1, p1, v0

    const/4 v0, 0x6

    const-string v1, "zzwt"

    aput-object v1, p1, v0

    .line 51
    sget-object v0, Lcom/google/android/gms/internal/measurement/al$k;->zzzd:Lcom/google/android/gms/internal/measurement/al$k;

    const-string v1, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001\u0002\u0000\u0002\u0008\u0001\u0003\u0008\u0002\u0004\u0002\u0003\u0005\u0001\u0004\u0006\u0000\u0005"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/internal/measurement/al$k;->a(Lcom/google/android/gms/internal/measurement/fb;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 48
    :pswitch_5
    new-instance p1, Lcom/google/android/gms/internal/measurement/al$k$a;

    invoke-direct {p1, v0}, Lcom/google/android/gms/internal/measurement/al$k$a;-><init>(B)V

    return-object p1

    .line 47
    :pswitch_6
    new-instance p1, Lcom/google/android/gms/internal/measurement/al$k;

    invoke-direct {p1}, Lcom/google/android/gms/internal/measurement/al$k;-><init>()V

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final ps()Z
    .locals 1

    .line 16
    iget v0, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzue:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final pt()Z
    .locals 1

    .line 29
    iget v0, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzue:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final pu()Z
    .locals 1

    .line 37
    iget v0, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzue:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final qp()Z
    .locals 2

    .line 5
    iget v0, p0, Lcom/google/android/gms/internal/measurement/al$k;->zzue:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
