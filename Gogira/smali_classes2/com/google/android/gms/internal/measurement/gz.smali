.class public enum Lcom/google/android/gms/internal/measurement/gz;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/google/android/gms/internal/measurement/gz;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum axA:Lcom/google/android/gms/internal/measurement/gz;

.field public static final enum axB:Lcom/google/android/gms/internal/measurement/gz;

.field public static final enum axC:Lcom/google/android/gms/internal/measurement/gz;

.field public static final enum axD:Lcom/google/android/gms/internal/measurement/gz;

.field public static final enum axE:Lcom/google/android/gms/internal/measurement/gz;

.field public static final enum axF:Lcom/google/android/gms/internal/measurement/gz;

.field public static final enum axG:Lcom/google/android/gms/internal/measurement/gz;

.field public static final enum axH:Lcom/google/android/gms/internal/measurement/gz;

.field public static final enum axI:Lcom/google/android/gms/internal/measurement/gz;

.field public static final enum axJ:Lcom/google/android/gms/internal/measurement/gz;

.field public static final enum axK:Lcom/google/android/gms/internal/measurement/gz;

.field public static final enum axL:Lcom/google/android/gms/internal/measurement/gz;

.field public static final enum axM:Lcom/google/android/gms/internal/measurement/gz;

.field public static final enum axN:Lcom/google/android/gms/internal/measurement/gz;

.field public static final enum axO:Lcom/google/android/gms/internal/measurement/gz;

.field private static final synthetic axP:[Lcom/google/android/gms/internal/measurement/gz;

.field public static final enum axx:Lcom/google/android/gms/internal/measurement/gz;

.field public static final enum axy:Lcom/google/android/gms/internal/measurement/gz;

.field public static final enum axz:Lcom/google/android/gms/internal/measurement/gz;


# instance fields
.field final zzant:Lcom/google/android/gms/internal/measurement/hc;

.field final zzanu:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 9
    new-instance v0, Lcom/google/android/gms/internal/measurement/gz;

    sget-object v1, Lcom/google/android/gms/internal/measurement/hc;->axT:Lcom/google/android/gms/internal/measurement/hc;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string v4, "DOUBLE"

    invoke-direct {v0, v4, v3, v1, v2}, Lcom/google/android/gms/internal/measurement/gz;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/hc;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/gz;->axx:Lcom/google/android/gms/internal/measurement/gz;

    .line 10
    new-instance v0, Lcom/google/android/gms/internal/measurement/gz;

    sget-object v1, Lcom/google/android/gms/internal/measurement/hc;->axS:Lcom/google/android/gms/internal/measurement/hc;

    const/4 v4, 0x5

    const-string v5, "FLOAT"

    invoke-direct {v0, v5, v2, v1, v4}, Lcom/google/android/gms/internal/measurement/gz;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/hc;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/gz;->axy:Lcom/google/android/gms/internal/measurement/gz;

    .line 11
    new-instance v0, Lcom/google/android/gms/internal/measurement/gz;

    sget-object v1, Lcom/google/android/gms/internal/measurement/hc;->axR:Lcom/google/android/gms/internal/measurement/hc;

    const/4 v5, 0x2

    const-string v6, "INT64"

    invoke-direct {v0, v6, v5, v1, v3}, Lcom/google/android/gms/internal/measurement/gz;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/hc;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/gz;->axz:Lcom/google/android/gms/internal/measurement/gz;

    .line 12
    new-instance v0, Lcom/google/android/gms/internal/measurement/gz;

    sget-object v1, Lcom/google/android/gms/internal/measurement/hc;->axR:Lcom/google/android/gms/internal/measurement/hc;

    const/4 v6, 0x3

    const-string v7, "UINT64"

    invoke-direct {v0, v7, v6, v1, v3}, Lcom/google/android/gms/internal/measurement/gz;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/hc;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/gz;->axA:Lcom/google/android/gms/internal/measurement/gz;

    .line 13
    new-instance v0, Lcom/google/android/gms/internal/measurement/gz;

    sget-object v1, Lcom/google/android/gms/internal/measurement/hc;->axQ:Lcom/google/android/gms/internal/measurement/hc;

    const/4 v7, 0x4

    const-string v8, "INT32"

    invoke-direct {v0, v8, v7, v1, v3}, Lcom/google/android/gms/internal/measurement/gz;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/hc;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/gz;->axB:Lcom/google/android/gms/internal/measurement/gz;

    .line 14
    new-instance v0, Lcom/google/android/gms/internal/measurement/gz;

    sget-object v1, Lcom/google/android/gms/internal/measurement/hc;->axR:Lcom/google/android/gms/internal/measurement/hc;

    const-string v8, "FIXED64"

    invoke-direct {v0, v8, v4, v1, v2}, Lcom/google/android/gms/internal/measurement/gz;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/hc;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/gz;->axC:Lcom/google/android/gms/internal/measurement/gz;

    .line 15
    new-instance v0, Lcom/google/android/gms/internal/measurement/gz;

    sget-object v1, Lcom/google/android/gms/internal/measurement/hc;->axQ:Lcom/google/android/gms/internal/measurement/hc;

    const/4 v8, 0x6

    const-string v9, "FIXED32"

    invoke-direct {v0, v9, v8, v1, v4}, Lcom/google/android/gms/internal/measurement/gz;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/hc;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/gz;->axD:Lcom/google/android/gms/internal/measurement/gz;

    .line 16
    new-instance v0, Lcom/google/android/gms/internal/measurement/gz;

    sget-object v1, Lcom/google/android/gms/internal/measurement/hc;->axU:Lcom/google/android/gms/internal/measurement/hc;

    const/4 v9, 0x7

    const-string v10, "BOOL"

    invoke-direct {v0, v10, v9, v1, v3}, Lcom/google/android/gms/internal/measurement/gz;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/hc;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/gz;->axE:Lcom/google/android/gms/internal/measurement/gz;

    .line 17
    new-instance v0, Lcom/google/android/gms/internal/measurement/gy;

    sget-object v1, Lcom/google/android/gms/internal/measurement/hc;->axV:Lcom/google/android/gms/internal/measurement/hc;

    const-string v10, "STRING"

    invoke-direct {v0, v10, v1}, Lcom/google/android/gms/internal/measurement/gy;-><init>(Ljava/lang/String;Lcom/google/android/gms/internal/measurement/hc;)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/gz;->axF:Lcom/google/android/gms/internal/measurement/gz;

    .line 18
    new-instance v0, Lcom/google/android/gms/internal/measurement/hb;

    sget-object v1, Lcom/google/android/gms/internal/measurement/hc;->axY:Lcom/google/android/gms/internal/measurement/hc;

    const-string v10, "GROUP"

    invoke-direct {v0, v10, v1}, Lcom/google/android/gms/internal/measurement/hb;-><init>(Ljava/lang/String;Lcom/google/android/gms/internal/measurement/hc;)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/gz;->axG:Lcom/google/android/gms/internal/measurement/gz;

    .line 19
    new-instance v0, Lcom/google/android/gms/internal/measurement/ha;

    sget-object v1, Lcom/google/android/gms/internal/measurement/hc;->axY:Lcom/google/android/gms/internal/measurement/hc;

    const-string v10, "MESSAGE"

    invoke-direct {v0, v10, v1}, Lcom/google/android/gms/internal/measurement/ha;-><init>(Ljava/lang/String;Lcom/google/android/gms/internal/measurement/hc;)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/gz;->axH:Lcom/google/android/gms/internal/measurement/gz;

    .line 20
    new-instance v0, Lcom/google/android/gms/internal/measurement/hd;

    sget-object v1, Lcom/google/android/gms/internal/measurement/hc;->axW:Lcom/google/android/gms/internal/measurement/hc;

    const-string v10, "BYTES"

    invoke-direct {v0, v10, v1}, Lcom/google/android/gms/internal/measurement/hd;-><init>(Ljava/lang/String;Lcom/google/android/gms/internal/measurement/hc;)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/gz;->axI:Lcom/google/android/gms/internal/measurement/gz;

    .line 21
    new-instance v0, Lcom/google/android/gms/internal/measurement/gz;

    sget-object v1, Lcom/google/android/gms/internal/measurement/hc;->axQ:Lcom/google/android/gms/internal/measurement/hc;

    const/16 v10, 0xc

    const-string v11, "UINT32"

    invoke-direct {v0, v11, v10, v1, v3}, Lcom/google/android/gms/internal/measurement/gz;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/hc;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/gz;->axJ:Lcom/google/android/gms/internal/measurement/gz;

    .line 22
    new-instance v0, Lcom/google/android/gms/internal/measurement/gz;

    sget-object v1, Lcom/google/android/gms/internal/measurement/hc;->axX:Lcom/google/android/gms/internal/measurement/hc;

    const/16 v11, 0xd

    const-string v12, "ENUM"

    invoke-direct {v0, v12, v11, v1, v3}, Lcom/google/android/gms/internal/measurement/gz;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/hc;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/gz;->axK:Lcom/google/android/gms/internal/measurement/gz;

    .line 23
    new-instance v0, Lcom/google/android/gms/internal/measurement/gz;

    sget-object v1, Lcom/google/android/gms/internal/measurement/hc;->axQ:Lcom/google/android/gms/internal/measurement/hc;

    const/16 v12, 0xe

    const-string v13, "SFIXED32"

    invoke-direct {v0, v13, v12, v1, v4}, Lcom/google/android/gms/internal/measurement/gz;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/hc;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/gz;->axL:Lcom/google/android/gms/internal/measurement/gz;

    .line 24
    new-instance v0, Lcom/google/android/gms/internal/measurement/gz;

    sget-object v1, Lcom/google/android/gms/internal/measurement/hc;->axR:Lcom/google/android/gms/internal/measurement/hc;

    const/16 v13, 0xf

    const-string v14, "SFIXED64"

    invoke-direct {v0, v14, v13, v1, v2}, Lcom/google/android/gms/internal/measurement/gz;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/hc;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/gz;->axM:Lcom/google/android/gms/internal/measurement/gz;

    .line 25
    new-instance v0, Lcom/google/android/gms/internal/measurement/gz;

    sget-object v1, Lcom/google/android/gms/internal/measurement/hc;->axQ:Lcom/google/android/gms/internal/measurement/hc;

    const/16 v14, 0x10

    const-string v15, "SINT32"

    invoke-direct {v0, v15, v14, v1, v3}, Lcom/google/android/gms/internal/measurement/gz;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/hc;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/gz;->axN:Lcom/google/android/gms/internal/measurement/gz;

    .line 26
    new-instance v0, Lcom/google/android/gms/internal/measurement/gz;

    sget-object v1, Lcom/google/android/gms/internal/measurement/hc;->axR:Lcom/google/android/gms/internal/measurement/hc;

    const/16 v15, 0x11

    const-string v14, "SINT64"

    invoke-direct {v0, v14, v15, v1, v3}, Lcom/google/android/gms/internal/measurement/gz;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/hc;I)V

    sput-object v0, Lcom/google/android/gms/internal/measurement/gz;->axO:Lcom/google/android/gms/internal/measurement/gz;

    const/16 v0, 0x12

    new-array v0, v0, [Lcom/google/android/gms/internal/measurement/gz;

    .line 27
    sget-object v1, Lcom/google/android/gms/internal/measurement/gz;->axx:Lcom/google/android/gms/internal/measurement/gz;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/gms/internal/measurement/gz;->axy:Lcom/google/android/gms/internal/measurement/gz;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/internal/measurement/gz;->axz:Lcom/google/android/gms/internal/measurement/gz;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/gms/internal/measurement/gz;->axA:Lcom/google/android/gms/internal/measurement/gz;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/gms/internal/measurement/gz;->axB:Lcom/google/android/gms/internal/measurement/gz;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/gms/internal/measurement/gz;->axC:Lcom/google/android/gms/internal/measurement/gz;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/gms/internal/measurement/gz;->axD:Lcom/google/android/gms/internal/measurement/gz;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/android/gms/internal/measurement/gz;->axE:Lcom/google/android/gms/internal/measurement/gz;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/android/gms/internal/measurement/gz;->axF:Lcom/google/android/gms/internal/measurement/gz;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/internal/measurement/gz;->axG:Lcom/google/android/gms/internal/measurement/gz;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/internal/measurement/gz;->axH:Lcom/google/android/gms/internal/measurement/gz;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/internal/measurement/gz;->axI:Lcom/google/android/gms/internal/measurement/gz;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/internal/measurement/gz;->axJ:Lcom/google/android/gms/internal/measurement/gz;

    aput-object v1, v0, v10

    sget-object v1, Lcom/google/android/gms/internal/measurement/gz;->axK:Lcom/google/android/gms/internal/measurement/gz;

    aput-object v1, v0, v11

    sget-object v1, Lcom/google/android/gms/internal/measurement/gz;->axL:Lcom/google/android/gms/internal/measurement/gz;

    aput-object v1, v0, v12

    sget-object v1, Lcom/google/android/gms/internal/measurement/gz;->axM:Lcom/google/android/gms/internal/measurement/gz;

    aput-object v1, v0, v13

    sget-object v1, Lcom/google/android/gms/internal/measurement/gz;->axN:Lcom/google/android/gms/internal/measurement/gz;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/internal/measurement/gz;->axO:Lcom/google/android/gms/internal/measurement/gz;

    aput-object v1, v0, v15

    sput-object v0, Lcom/google/android/gms/internal/measurement/gz;->axP:[Lcom/google/android/gms/internal/measurement/gz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/hc;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/measurement/hc;",
            "I)V"
        }
    .end annotation

    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3
    iput-object p3, p0, Lcom/google/android/gms/internal/measurement/gz;->zzant:Lcom/google/android/gms/internal/measurement/hc;

    .line 4
    iput p4, p0, Lcom/google/android/gms/internal/measurement/gz;->zzanu:I

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/hc;IB)V
    .locals 0

    .line 8
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/internal/measurement/gz;-><init>(Ljava/lang/String;ILcom/google/android/gms/internal/measurement/hc;I)V

    return-void
.end method

.method public static values()[Lcom/google/android/gms/internal/measurement/gz;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/android/gms/internal/measurement/gz;->axP:[Lcom/google/android/gms/internal/measurement/gz;

    invoke-virtual {v0}, [Lcom/google/android/gms/internal/measurement/gz;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/internal/measurement/gz;

    return-object v0
.end method
