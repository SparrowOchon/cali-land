.class public final Lcom/google/android/gms/internal/measurement/in;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/measurement/bv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/internal/measurement/bv<",
        "Lcom/google/android/gms/internal/measurement/iq;",
        ">;"
    }
.end annotation


# static fields
.field private static azC:Lcom/google/android/gms/internal/measurement/in;


# instance fields
.field private final ayD:Lcom/google/android/gms/internal/measurement/bv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bv<",
            "Lcom/google/android/gms/internal/measurement/iq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 10
    new-instance v0, Lcom/google/android/gms/internal/measurement/in;

    invoke-direct {v0}, Lcom/google/android/gms/internal/measurement/in;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/measurement/in;->azC:Lcom/google/android/gms/internal/measurement/in;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 5
    new-instance v0, Lcom/google/android/gms/internal/measurement/ip;

    invoke-direct {v0}, Lcom/google/android/gms/internal/measurement/ip;-><init>()V

    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/bu;->ak(Ljava/lang/Object;)Lcom/google/android/gms/internal/measurement/bv;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/measurement/in;-><init>(Lcom/google/android/gms/internal/measurement/bv;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/internal/measurement/bv;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/measurement/bv<",
            "Lcom/google/android/gms/internal/measurement/iq;",
            ">;)V"
        }
    .end annotation

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    invoke-static {p1}, Lcom/google/android/gms/internal/measurement/bu;->a(Lcom/google/android/gms/internal/measurement/bv;)Lcom/google/android/gms/internal/measurement/bv;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/in;->ayD:Lcom/google/android/gms/internal/measurement/bv;

    return-void
.end method

.method public static uo()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/google/android/gms/internal/measurement/in;->azC:Lcom/google/android/gms/internal/measurement/in;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/in;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/iq;

    invoke-interface {v0}, Lcom/google/android/gms/internal/measurement/iq;->uo()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/in;->ayD:Lcom/google/android/gms/internal/measurement/bv;

    invoke-interface {v0}, Lcom/google/android/gms/internal/measurement/bv;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/iq;

    return-object v0
.end method
