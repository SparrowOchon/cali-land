.class public final Lcom/google/android/gms/internal/measurement/al$a;
.super Lcom/google/android/gms/internal/measurement/dr;

# interfaces
.implements Lcom/google/android/gms/internal/measurement/fd;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/measurement/al;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/measurement/al$a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/measurement/dr<",
        "Lcom/google/android/gms/internal/measurement/al$a;",
        "Lcom/google/android/gms/internal/measurement/al$a$a;",
        ">;",
        "Lcom/google/android/gms/internal/measurement/fd;"
    }
.end annotation


# static fields
.field private static volatile zzuo:Lcom/google/android/gms/internal/measurement/fk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/fk<",
            "Lcom/google/android/gms/internal/measurement/al$a;",
            ">;"
        }
    .end annotation
.end field

.field private static final zzwf:Lcom/google/android/gms/internal/measurement/al$a;


# instance fields
.field public zzue:I

.field public zzwb:I

.field private zzwc:Lcom/google/android/gms/internal/measurement/al$i;

.field private zzwd:Lcom/google/android/gms/internal/measurement/al$i;

.field public zzwe:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 49
    new-instance v0, Lcom/google/android/gms/internal/measurement/al$a;

    invoke-direct {v0}, Lcom/google/android/gms/internal/measurement/al$a;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/measurement/al$a;->zzwf:Lcom/google/android/gms/internal/measurement/al$a;

    .line 50
    const-class v0, Lcom/google/android/gms/internal/measurement/al$a;

    sget-object v1, Lcom/google/android/gms/internal/measurement/al$a;->zzwf:Lcom/google/android/gms/internal/measurement/al$a;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/measurement/dr;->a(Ljava/lang/Class;Lcom/google/android/gms/internal/measurement/dr;)V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/dr;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/measurement/al$a;I)V
    .locals 1

    .line 1005
    iget v0, p0, Lcom/google/android/gms/internal/measurement/al$a;->zzue:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/measurement/al$a;->zzue:I

    .line 1006
    iput p1, p0, Lcom/google/android/gms/internal/measurement/al$a;->zzwb:I

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/measurement/al$a;Lcom/google/android/gms/internal/measurement/al$i$a;)V
    .locals 0

    .line 1061
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/dr$a;->sg()Lcom/google/android/gms/internal/measurement/dr;

    move-result-object p1

    .line 1009
    check-cast p1, Lcom/google/android/gms/internal/measurement/dr;

    check-cast p1, Lcom/google/android/gms/internal/measurement/al$i;

    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/al$a;->zzwc:Lcom/google/android/gms/internal/measurement/al$i;

    .line 1010
    iget p1, p0, Lcom/google/android/gms/internal/measurement/al$a;->zzue:I

    or-int/lit8 p1, p1, 0x2

    iput p1, p0, Lcom/google/android/gms/internal/measurement/al$a;->zzue:I

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/measurement/al$a;Lcom/google/android/gms/internal/measurement/al$i;)V
    .locals 0

    if-eqz p1, :cond_0

    .line 2016
    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/al$a;->zzwd:Lcom/google/android/gms/internal/measurement/al$i;

    .line 2017
    iget p1, p0, Lcom/google/android/gms/internal/measurement/al$a;->zzue:I

    or-int/lit8 p1, p1, 0x4

    iput p1, p0, Lcom/google/android/gms/internal/measurement/al$a;->zzue:I

    return-void

    .line 2015
    :cond_0
    new-instance p0, Ljava/lang/NullPointerException;

    invoke-direct {p0}, Ljava/lang/NullPointerException;-><init>()V

    throw p0
.end method

.method static synthetic a(Lcom/google/android/gms/internal/measurement/al$a;Z)V
    .locals 1

    .line 2021
    iget v0, p0, Lcom/google/android/gms/internal/measurement/al$a;->zzue:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/android/gms/internal/measurement/al$a;->zzue:I

    .line 2022
    iput-boolean p1, p0, Lcom/google/android/gms/internal/measurement/al$a;->zzwe:Z

    return-void
.end method

.method public static pc()Lcom/google/android/gms/internal/measurement/al$a$a;
    .locals 1

    .line 24
    sget-object v0, Lcom/google/android/gms/internal/measurement/al$a;->zzwf:Lcom/google/android/gms/internal/measurement/al$a;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/dr;->rY()Lcom/google/android/gms/internal/measurement/dr$a;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$a$a;

    return-object v0
.end method

.method static synthetic pd()Lcom/google/android/gms/internal/measurement/al$a;
    .locals 1

    .line 44
    sget-object v0, Lcom/google/android/gms/internal/measurement/al$a;->zzwf:Lcom/google/android/gms/internal/measurement/al$a;

    return-object v0
.end method


# virtual methods
.method protected final bm(I)Ljava/lang/Object;
    .locals 3

    .line 25
    sget-object v0, Lcom/google/android/gms/internal/measurement/ak;->aqY:[I

    const/4 v1, 0x1

    sub-int/2addr p1, v1

    aget p1, v0, p1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    .line 43
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    const/4 p1, 0x0

    return-object p1

    .line 41
    :pswitch_1
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    .line 32
    :pswitch_2
    sget-object p1, Lcom/google/android/gms/internal/measurement/al$a;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    if-nez p1, :cond_1

    .line 34
    const-class v0, Lcom/google/android/gms/internal/measurement/al$a;

    monitor-enter v0

    .line 35
    :try_start_0
    sget-object p1, Lcom/google/android/gms/internal/measurement/al$a;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    if-nez p1, :cond_0

    .line 37
    new-instance p1, Lcom/google/android/gms/internal/measurement/dr$c;

    sget-object v1, Lcom/google/android/gms/internal/measurement/al$a;->zzwf:Lcom/google/android/gms/internal/measurement/al$a;

    invoke-direct {p1, v1}, Lcom/google/android/gms/internal/measurement/dr$c;-><init>(Lcom/google/android/gms/internal/measurement/dr;)V

    .line 38
    sput-object p1, Lcom/google/android/gms/internal/measurement/al$a;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    .line 39
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    .line 31
    :pswitch_3
    sget-object p1, Lcom/google/android/gms/internal/measurement/al$a;->zzwf:Lcom/google/android/gms/internal/measurement/al$a;

    return-object p1

    :pswitch_4
    const/4 p1, 0x5

    new-array p1, p1, [Ljava/lang/Object;

    const-string v2, "zzue"

    aput-object v2, p1, v0

    const-string v0, "zzwb"

    aput-object v0, p1, v1

    const/4 v0, 0x2

    const-string v1, "zzwc"

    aput-object v1, p1, v0

    const/4 v0, 0x3

    const-string v1, "zzwd"

    aput-object v1, p1, v0

    const/4 v0, 0x4

    const-string v1, "zzwe"

    aput-object v1, p1, v0

    .line 30
    sget-object v0, Lcom/google/android/gms/internal/measurement/al$a;->zzwf:Lcom/google/android/gms/internal/measurement/al$a;

    const-string v1, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001\u0004\u0000\u0002\t\u0001\u0003\t\u0002\u0004\u0007\u0003"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/internal/measurement/al$a;->a(Lcom/google/android/gms/internal/measurement/fb;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 27
    :pswitch_5
    new-instance p1, Lcom/google/android/gms/internal/measurement/al$a$a;

    invoke-direct {p1, v0}, Lcom/google/android/gms/internal/measurement/al$a$a;-><init>(B)V

    return-object p1

    .line 26
    :pswitch_6
    new-instance p1, Lcom/google/android/gms/internal/measurement/al$a;

    invoke-direct {p1}, Lcom/google/android/gms/internal/measurement/al$a;-><init>()V

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final pa()Lcom/google/android/gms/internal/measurement/al$i;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$a;->zzwc:Lcom/google/android/gms/internal/measurement/al$i;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/internal/measurement/al$i;->qg()Lcom/google/android/gms/internal/measurement/al$i;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public final pb()Lcom/google/android/gms/internal/measurement/al$i;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$a;->zzwd:Lcom/google/android/gms/internal/measurement/al$i;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/internal/measurement/al$i;->qg()Lcom/google/android/gms/internal/measurement/al$i;

    move-result-object v0

    :cond_0
    return-object v0
.end method
