.class public final Lcom/google/android/gms/internal/measurement/al$e;
.super Lcom/google/android/gms/internal/measurement/dr;

# interfaces
.implements Lcom/google/android/gms/internal/measurement/fd;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/measurement/al;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/measurement/al$e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/measurement/dr<",
        "Lcom/google/android/gms/internal/measurement/al$e;",
        "Lcom/google/android/gms/internal/measurement/al$e$a;",
        ">;",
        "Lcom/google/android/gms/internal/measurement/fd;"
    }
.end annotation


# static fields
.field private static volatile zzuo:Lcom/google/android/gms/internal/measurement/fk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/fk<",
            "Lcom/google/android/gms/internal/measurement/al$e;",
            ">;"
        }
    .end annotation
.end field

.field private static final zzwu:Lcom/google/android/gms/internal/measurement/al$e;


# instance fields
.field private zzue:I

.field public zzwk:Ljava/lang/String;

.field public zzwp:J

.field public zzwr:Ljava/lang/String;

.field private zzws:F

.field public zzwt:D


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 68
    new-instance v0, Lcom/google/android/gms/internal/measurement/al$e;

    invoke-direct {v0}, Lcom/google/android/gms/internal/measurement/al$e;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/measurement/al$e;->zzwu:Lcom/google/android/gms/internal/measurement/al$e;

    .line 69
    const-class v0, Lcom/google/android/gms/internal/measurement/al$e;

    sget-object v1, Lcom/google/android/gms/internal/measurement/al$e;->zzwu:Lcom/google/android/gms/internal/measurement/al$e;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/measurement/dr;->a(Ljava/lang/Class;Lcom/google/android/gms/internal/measurement/dr;)V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/dr;-><init>()V

    const-string v0, ""

    .line 2
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzwk:Ljava/lang/String;

    .line 3
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzwr:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/measurement/al$e;D)V
    .locals 1

    .line 1034
    iget v0, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzue:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzue:I

    .line 1035
    iput-wide p1, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzwt:D

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/measurement/al$e;J)V
    .locals 1

    .line 1026
    iget v0, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzue:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzue:I

    .line 1027
    iput-wide p1, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzwp:J

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/measurement/al$e;Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 1008
    iget v0, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzue:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzue:I

    .line 1009
    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzwk:Ljava/lang/String;

    return-void

    .line 1007
    :cond_0
    new-instance p0, Ljava/lang/NullPointerException;

    invoke-direct {p0}, Ljava/lang/NullPointerException;-><init>()V

    throw p0
.end method

.method static synthetic b(Lcom/google/android/gms/internal/measurement/al$e;)V
    .locals 1

    .line 1018
    iget v0, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzue:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzue:I

    .line 1020
    sget-object v0, Lcom/google/android/gms/internal/measurement/al$e;->zzwu:Lcom/google/android/gms/internal/measurement/al$e;

    .line 1021
    iget-object v0, v0, Lcom/google/android/gms/internal/measurement/al$e;->zzwr:Ljava/lang/String;

    .line 1022
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzwr:Ljava/lang/String;

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/internal/measurement/al$e;Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 1015
    iget v0, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzue:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzue:I

    .line 1016
    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzwr:Ljava/lang/String;

    return-void

    .line 1014
    :cond_0
    new-instance p0, Ljava/lang/NullPointerException;

    invoke-direct {p0}, Ljava/lang/NullPointerException;-><init>()V

    throw p0
.end method

.method static synthetic c(Lcom/google/android/gms/internal/measurement/al$e;)V
    .locals 2

    .line 1029
    iget v0, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzue:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzue:I

    const-wide/16 v0, 0x0

    .line 1030
    iput-wide v0, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzwp:J

    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/internal/measurement/al$e;)V
    .locals 2

    .line 1037
    iget v0, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzue:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzue:I

    const-wide/16 v0, 0x0

    .line 1038
    iput-wide v0, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzwt:D

    return-void
.end method

.method public static pv()Lcom/google/android/gms/internal/measurement/al$e$a;
    .locals 1

    .line 40
    sget-object v0, Lcom/google/android/gms/internal/measurement/al$e;->zzwu:Lcom/google/android/gms/internal/measurement/al$e;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/dr;->rY()Lcom/google/android/gms/internal/measurement/dr$a;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$e$a;

    return-object v0
.end method

.method static synthetic pw()Lcom/google/android/gms/internal/measurement/al$e;
    .locals 1

    .line 60
    sget-object v0, Lcom/google/android/gms/internal/measurement/al$e;->zzwu:Lcom/google/android/gms/internal/measurement/al$e;

    return-object v0
.end method


# virtual methods
.method protected final bm(I)Ljava/lang/Object;
    .locals 3

    .line 41
    sget-object v0, Lcom/google/android/gms/internal/measurement/ak;->aqY:[I

    const/4 v1, 0x1

    sub-int/2addr p1, v1

    aget p1, v0, p1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    .line 59
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    const/4 p1, 0x0

    return-object p1

    .line 57
    :pswitch_1
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    .line 48
    :pswitch_2
    sget-object p1, Lcom/google/android/gms/internal/measurement/al$e;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    if-nez p1, :cond_1

    .line 50
    const-class v0, Lcom/google/android/gms/internal/measurement/al$e;

    monitor-enter v0

    .line 51
    :try_start_0
    sget-object p1, Lcom/google/android/gms/internal/measurement/al$e;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    if-nez p1, :cond_0

    .line 53
    new-instance p1, Lcom/google/android/gms/internal/measurement/dr$c;

    sget-object v1, Lcom/google/android/gms/internal/measurement/al$e;->zzwu:Lcom/google/android/gms/internal/measurement/al$e;

    invoke-direct {p1, v1}, Lcom/google/android/gms/internal/measurement/dr$c;-><init>(Lcom/google/android/gms/internal/measurement/dr;)V

    .line 54
    sput-object p1, Lcom/google/android/gms/internal/measurement/al$e;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    .line 55
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    .line 47
    :pswitch_3
    sget-object p1, Lcom/google/android/gms/internal/measurement/al$e;->zzwu:Lcom/google/android/gms/internal/measurement/al$e;

    return-object p1

    :pswitch_4
    const/4 p1, 0x6

    new-array p1, p1, [Ljava/lang/Object;

    const-string v2, "zzue"

    aput-object v2, p1, v0

    const-string v0, "zzwk"

    aput-object v0, p1, v1

    const/4 v0, 0x2

    const-string v1, "zzwr"

    aput-object v1, p1, v0

    const/4 v0, 0x3

    const-string v1, "zzwp"

    aput-object v1, p1, v0

    const/4 v0, 0x4

    const-string v1, "zzws"

    aput-object v1, p1, v0

    const/4 v0, 0x5

    const-string v1, "zzwt"

    aput-object v1, p1, v0

    .line 46
    sget-object v0, Lcom/google/android/gms/internal/measurement/al$e;->zzwu:Lcom/google/android/gms/internal/measurement/al$e;

    const-string v1, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0000\u0000\u0001\u0008\u0000\u0002\u0008\u0001\u0003\u0002\u0002\u0004\u0001\u0003\u0005\u0000\u0004"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/internal/measurement/al$e;->a(Lcom/google/android/gms/internal/measurement/fb;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 43
    :pswitch_5
    new-instance p1, Lcom/google/android/gms/internal/measurement/al$e$a;

    invoke-direct {p1, v0}, Lcom/google/android/gms/internal/measurement/al$e$a;-><init>(B)V

    return-object p1

    .line 42
    :pswitch_6
    new-instance p1, Lcom/google/android/gms/internal/measurement/al$e;

    invoke-direct {p1}, Lcom/google/android/gms/internal/measurement/al$e;-><init>()V

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final ps()Z
    .locals 1

    .line 11
    iget v0, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzue:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final pt()Z
    .locals 1

    .line 24
    iget v0, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzue:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final pu()Z
    .locals 1

    .line 32
    iget v0, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzue:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
