.class public final Lcom/google/android/gms/internal/measurement/al$i;
.super Lcom/google/android/gms/internal/measurement/dr;

# interfaces
.implements Lcom/google/android/gms/internal/measurement/fd;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/measurement/al;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "i"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/measurement/al$i$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/measurement/dr<",
        "Lcom/google/android/gms/internal/measurement/al$i;",
        "Lcom/google/android/gms/internal/measurement/al$i$a;",
        ">;",
        "Lcom/google/android/gms/internal/measurement/fd;"
    }
.end annotation


# static fields
.field private static volatile zzuo:Lcom/google/android/gms/internal/measurement/fk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/fk<",
            "Lcom/google/android/gms/internal/measurement/al$i;",
            ">;"
        }
    .end annotation
.end field

.field private static final zzyz:Lcom/google/android/gms/internal/measurement/al$i;


# instance fields
.field public zzyv:Lcom/google/android/gms/internal/measurement/ea;

.field public zzyw:Lcom/google/android/gms/internal/measurement/ea;

.field public zzyx:Lcom/google/android/gms/internal/measurement/dz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/dz<",
            "Lcom/google/android/gms/internal/measurement/al$b;",
            ">;"
        }
    .end annotation
.end field

.field public zzyy:Lcom/google/android/gms/internal/measurement/dz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/dz<",
            "Lcom/google/android/gms/internal/measurement/al$j;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 84
    new-instance v0, Lcom/google/android/gms/internal/measurement/al$i;

    invoke-direct {v0}, Lcom/google/android/gms/internal/measurement/al$i;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/measurement/al$i;->zzyz:Lcom/google/android/gms/internal/measurement/al$i;

    .line 85
    const-class v0, Lcom/google/android/gms/internal/measurement/al$i;

    sget-object v1, Lcom/google/android/gms/internal/measurement/al$i;->zzyz:Lcom/google/android/gms/internal/measurement/al$i;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/measurement/dr;->a(Ljava/lang/Class;Lcom/google/android/gms/internal/measurement/dr;)V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/dr;-><init>()V

    .line 1088
    invoke-static {}, Lcom/google/android/gms/internal/measurement/eo;->sC()Lcom/google/android/gms/internal/measurement/eo;

    move-result-object v0

    .line 2
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/al$i;->zzyv:Lcom/google/android/gms/internal/measurement/ea;

    .line 2088
    invoke-static {}, Lcom/google/android/gms/internal/measurement/eo;->sC()Lcom/google/android/gms/internal/measurement/eo;

    move-result-object v0

    .line 3
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/al$i;->zzyw:Lcom/google/android/gms/internal/measurement/ea;

    .line 2093
    invoke-static {}, Lcom/google/android/gms/internal/measurement/fp;->sT()Lcom/google/android/gms/internal/measurement/fp;

    move-result-object v0

    .line 4
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/al$i;->zzyx:Lcom/google/android/gms/internal/measurement/dz;

    .line 3093
    invoke-static {}, Lcom/google/android/gms/internal/measurement/fp;->sT()Lcom/google/android/gms/internal/measurement/fp;

    move-result-object v0

    .line 5
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/al$i;->zzyy:Lcom/google/android/gms/internal/measurement/dz;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/measurement/al$i;I)V
    .locals 0

    .line 6037
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/al$i;->qc()V

    .line 6038
    iget-object p0, p0, Lcom/google/android/gms/internal/measurement/al$i;->zzyx:Lcom/google/android/gms/internal/measurement/dz;

    invoke-interface {p0, p1}, Lcom/google/android/gms/internal/measurement/dz;->remove(I)Ljava/lang/Object;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/measurement/al$i;Ljava/lang/Iterable;)V
    .locals 1

    .line 4010
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$i;->zzyv:Lcom/google/android/gms/internal/measurement/ea;

    invoke-interface {v0}, Lcom/google/android/gms/internal/measurement/ea;->qM()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4011
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$i;->zzyv:Lcom/google/android/gms/internal/measurement/ea;

    .line 4012
    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/dr;->a(Lcom/google/android/gms/internal/measurement/ea;)Lcom/google/android/gms/internal/measurement/ea;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/al$i;->zzyv:Lcom/google/android/gms/internal/measurement/ea;

    .line 4013
    :cond_0
    iget-object p0, p0, Lcom/google/android/gms/internal/measurement/al$i;->zzyv:Lcom/google/android/gms/internal/measurement/ea;

    invoke-static {p1, p0}, Lcom/google/android/gms/internal/measurement/bz;->a(Ljava/lang/Iterable;Ljava/util/List;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/internal/measurement/al$i;)V
    .locals 1

    .line 4088
    invoke-static {}, Lcom/google/android/gms/internal/measurement/eo;->sC()Lcom/google/android/gms/internal/measurement/eo;

    move-result-object v0

    .line 4015
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/al$i;->zzyv:Lcom/google/android/gms/internal/measurement/ea;

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/internal/measurement/al$i;I)V
    .locals 0

    .line 6050
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/al$i;->qe()V

    .line 6051
    iget-object p0, p0, Lcom/google/android/gms/internal/measurement/al$i;->zzyy:Lcom/google/android/gms/internal/measurement/dz;

    invoke-interface {p0, p1}, Lcom/google/android/gms/internal/measurement/dz;->remove(I)Ljava/lang/Object;

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/internal/measurement/al$i;Ljava/lang/Iterable;)V
    .locals 1

    .line 5020
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$i;->zzyw:Lcom/google/android/gms/internal/measurement/ea;

    invoke-interface {v0}, Lcom/google/android/gms/internal/measurement/ea;->qM()Z

    move-result v0

    if-nez v0, :cond_0

    .line 5021
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$i;->zzyw:Lcom/google/android/gms/internal/measurement/ea;

    .line 5022
    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/dr;->a(Lcom/google/android/gms/internal/measurement/ea;)Lcom/google/android/gms/internal/measurement/ea;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/al$i;->zzyw:Lcom/google/android/gms/internal/measurement/ea;

    .line 5023
    :cond_0
    iget-object p0, p0, Lcom/google/android/gms/internal/measurement/al$i;->zzyw:Lcom/google/android/gms/internal/measurement/ea;

    invoke-static {p1, p0}, Lcom/google/android/gms/internal/measurement/bz;->a(Ljava/lang/Iterable;Ljava/util/List;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/internal/measurement/al$i;)V
    .locals 1

    .line 5088
    invoke-static {}, Lcom/google/android/gms/internal/measurement/eo;->sC()Lcom/google/android/gms/internal/measurement/eo;

    move-result-object v0

    .line 5025
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/al$i;->zzyw:Lcom/google/android/gms/internal/measurement/ea;

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/internal/measurement/al$i;Ljava/lang/Iterable;)V
    .locals 0

    .line 6034
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/al$i;->qc()V

    .line 6035
    iget-object p0, p0, Lcom/google/android/gms/internal/measurement/al$i;->zzyx:Lcom/google/android/gms/internal/measurement/dz;

    invoke-static {p1, p0}, Lcom/google/android/gms/internal/measurement/bz;->a(Ljava/lang/Iterable;Ljava/util/List;)V

    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/internal/measurement/al$i;Ljava/lang/Iterable;)V
    .locals 0

    .line 6047
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/al$i;->qe()V

    .line 6048
    iget-object p0, p0, Lcom/google/android/gms/internal/measurement/al$i;->zzyy:Lcom/google/android/gms/internal/measurement/dz;

    invoke-static {p1, p0}, Lcom/google/android/gms/internal/measurement/bz;->a(Ljava/lang/Iterable;Ljava/util/List;)V

    return-void
.end method

.method public static e([BLcom/google/android/gms/internal/measurement/de;)Lcom/google/android/gms/internal/measurement/al$i;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/measurement/ec;
        }
    .end annotation

    .line 53
    sget-object v0, Lcom/google/android/gms/internal/measurement/al$i;->zzyz:Lcom/google/android/gms/internal/measurement/al$i;

    invoke-static {v0, p0, p1}, Lcom/google/android/gms/internal/measurement/dr;->a(Lcom/google/android/gms/internal/measurement/dr;[BLcom/google/android/gms/internal/measurement/de;)Lcom/google/android/gms/internal/measurement/dr;

    move-result-object p0

    check-cast p0, Lcom/google/android/gms/internal/measurement/al$i;

    return-object p0
.end method

.method private final qc()V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$i;->zzyx:Lcom/google/android/gms/internal/measurement/dz;

    invoke-interface {v0}, Lcom/google/android/gms/internal/measurement/dz;->qM()Z

    move-result v0

    if-nez v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$i;->zzyx:Lcom/google/android/gms/internal/measurement/dz;

    .line 32
    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/dr;->a(Lcom/google/android/gms/internal/measurement/dz;)Lcom/google/android/gms/internal/measurement/dz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/al$i;->zzyx:Lcom/google/android/gms/internal/measurement/dz;

    :cond_0
    return-void
.end method

.method private final qe()V
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$i;->zzyy:Lcom/google/android/gms/internal/measurement/dz;

    invoke-interface {v0}, Lcom/google/android/gms/internal/measurement/dz;->qM()Z

    move-result v0

    if-nez v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$i;->zzyy:Lcom/google/android/gms/internal/measurement/dz;

    .line 45
    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/dr;->a(Lcom/google/android/gms/internal/measurement/dz;)Lcom/google/android/gms/internal/measurement/dz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/al$i;->zzyy:Lcom/google/android/gms/internal/measurement/dz;

    :cond_0
    return-void
.end method

.method public static qf()Lcom/google/android/gms/internal/measurement/al$i$a;
    .locals 1

    .line 54
    sget-object v0, Lcom/google/android/gms/internal/measurement/al$i;->zzyz:Lcom/google/android/gms/internal/measurement/al$i;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/dr;->rY()Lcom/google/android/gms/internal/measurement/dr$a;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$i$a;

    return-object v0
.end method

.method public static qg()Lcom/google/android/gms/internal/measurement/al$i;
    .locals 1

    .line 74
    sget-object v0, Lcom/google/android/gms/internal/measurement/al$i;->zzyz:Lcom/google/android/gms/internal/measurement/al$i;

    return-object v0
.end method

.method static synthetic qh()Lcom/google/android/gms/internal/measurement/al$i;
    .locals 1

    .line 75
    sget-object v0, Lcom/google/android/gms/internal/measurement/al$i;->zzyz:Lcom/google/android/gms/internal/measurement/al$i;

    return-object v0
.end method


# virtual methods
.method protected final bm(I)Ljava/lang/Object;
    .locals 3

    .line 55
    sget-object v0, Lcom/google/android/gms/internal/measurement/ak;->aqY:[I

    const/4 v1, 0x1

    sub-int/2addr p1, v1

    aget p1, v0, p1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    .line 73
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    const/4 p1, 0x0

    return-object p1

    .line 71
    :pswitch_1
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    .line 62
    :pswitch_2
    sget-object p1, Lcom/google/android/gms/internal/measurement/al$i;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    if-nez p1, :cond_1

    .line 64
    const-class v0, Lcom/google/android/gms/internal/measurement/al$i;

    monitor-enter v0

    .line 65
    :try_start_0
    sget-object p1, Lcom/google/android/gms/internal/measurement/al$i;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    if-nez p1, :cond_0

    .line 67
    new-instance p1, Lcom/google/android/gms/internal/measurement/dr$c;

    sget-object v1, Lcom/google/android/gms/internal/measurement/al$i;->zzyz:Lcom/google/android/gms/internal/measurement/al$i;

    invoke-direct {p1, v1}, Lcom/google/android/gms/internal/measurement/dr$c;-><init>(Lcom/google/android/gms/internal/measurement/dr;)V

    .line 68
    sput-object p1, Lcom/google/android/gms/internal/measurement/al$i;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    .line 69
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    .line 61
    :pswitch_3
    sget-object p1, Lcom/google/android/gms/internal/measurement/al$i;->zzyz:Lcom/google/android/gms/internal/measurement/al$i;

    return-object p1

    :pswitch_4
    const/4 p1, 0x6

    new-array p1, p1, [Ljava/lang/Object;

    const-string v2, "zzyv"

    aput-object v2, p1, v0

    const-string v0, "zzyw"

    aput-object v0, p1, v1

    const/4 v0, 0x2

    const-string v1, "zzyx"

    aput-object v1, p1, v0

    const/4 v0, 0x3

    .line 58
    const-class v1, Lcom/google/android/gms/internal/measurement/al$b;

    aput-object v1, p1, v0

    const/4 v0, 0x4

    const-string v1, "zzyy"

    aput-object v1, p1, v0

    const/4 v0, 0x5

    const-class v1, Lcom/google/android/gms/internal/measurement/al$j;

    aput-object v1, p1, v0

    .line 60
    sget-object v0, Lcom/google/android/gms/internal/measurement/al$i;->zzyz:Lcom/google/android/gms/internal/measurement/al$i;

    const-string v1, "\u0001\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0004\u0000\u0001\u0015\u0002\u0015\u0003\u001b\u0004\u001b"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/internal/measurement/al$i;->a(Lcom/google/android/gms/internal/measurement/fb;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 57
    :pswitch_5
    new-instance p1, Lcom/google/android/gms/internal/measurement/al$i$a;

    invoke-direct {p1, v0}, Lcom/google/android/gms/internal/measurement/al$i$a;-><init>(B)V

    return-object p1

    .line 56
    :pswitch_6
    new-instance p1, Lcom/google/android/gms/internal/measurement/al$i;

    invoke-direct {p1}, Lcom/google/android/gms/internal/measurement/al$i;-><init>()V

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final qa()I
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$i;->zzyv:Lcom/google/android/gms/internal/measurement/ea;

    invoke-interface {v0}, Lcom/google/android/gms/internal/measurement/ea;->size()I

    move-result v0

    return v0
.end method

.method public final qb()I
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$i;->zzyx:Lcom/google/android/gms/internal/measurement/dz;

    invoke-interface {v0}, Lcom/google/android/gms/internal/measurement/dz;->size()I

    move-result v0

    return v0
.end method

.method public final qd()I
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$i;->zzyy:Lcom/google/android/gms/internal/measurement/dz;

    invoke-interface {v0}, Lcom/google/android/gms/internal/measurement/dz;->size()I

    move-result v0

    return v0
.end method
