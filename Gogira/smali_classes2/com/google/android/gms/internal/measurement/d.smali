.class public final Lcom/google/android/gms/internal/measurement/d;
.super Lcom/google/android/gms/internal/measurement/lw$a;


# instance fields
.field private final synthetic aqB:Lcom/google/android/gms/internal/measurement/lw;

.field private final synthetic aqF:Ljava/lang/String;

.field private final synthetic aqG:Ljava/lang/String;

.field private final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/measurement/lw;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/d;->aqB:Lcom/google/android/gms/internal/measurement/lw;

    iput-object p2, p0, Lcom/google/android/gms/internal/measurement/d;->val$activity:Landroid/app/Activity;

    iput-object p3, p0, Lcom/google/android/gms/internal/measurement/d;->aqF:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/internal/measurement/d;->aqG:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/measurement/lw$a;-><init>(Lcom/google/android/gms/internal/measurement/lw;)V

    return-void
.end method


# virtual methods
.method final ov()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/d;->aqB:Lcom/google/android/gms/internal/measurement/lw;

    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/lw;->c(Lcom/google/android/gms/internal/measurement/lw;)Lcom/google/android/gms/internal/measurement/iu;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/d;->val$activity:Landroid/app/Activity;

    .line 3
    invoke-static {v0}, Lcom/google/android/gms/a/b;->ab(Ljava/lang/Object;)Lcom/google/android/gms/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/d;->aqF:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/internal/measurement/d;->aqG:Ljava/lang/String;

    iget-wide v5, p0, Lcom/google/android/gms/internal/measurement/d;->timestamp:J

    .line 4
    invoke-interface/range {v1 .. v6}, Lcom/google/android/gms/internal/measurement/iu;->setCurrentScreen(Lcom/google/android/gms/a/a;Ljava/lang/String;Ljava/lang/String;J)V

    return-void
.end method
