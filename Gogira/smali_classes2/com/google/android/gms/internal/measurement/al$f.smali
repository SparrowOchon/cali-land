.class public final Lcom/google/android/gms/internal/measurement/al$f;
.super Lcom/google/android/gms/internal/measurement/dr;

# interfaces
.implements Lcom/google/android/gms/internal/measurement/fd;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/measurement/al;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "f"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/measurement/al$f$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/measurement/dr<",
        "Lcom/google/android/gms/internal/measurement/al$f;",
        "Lcom/google/android/gms/internal/measurement/al$f$a;",
        ">;",
        "Lcom/google/android/gms/internal/measurement/fd;"
    }
.end annotation


# static fields
.field private static volatile zzuo:Lcom/google/android/gms/internal/measurement/fk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/fk<",
            "Lcom/google/android/gms/internal/measurement/al$f;",
            ">;"
        }
    .end annotation
.end field

.field private static final zzww:Lcom/google/android/gms/internal/measurement/al$f;


# instance fields
.field public zzwv:Lcom/google/android/gms/internal/measurement/dz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/dz<",
            "Lcom/google/android/gms/internal/measurement/al$g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 34
    new-instance v0, Lcom/google/android/gms/internal/measurement/al$f;

    invoke-direct {v0}, Lcom/google/android/gms/internal/measurement/al$f;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/measurement/al$f;->zzww:Lcom/google/android/gms/internal/measurement/al$f;

    .line 35
    const-class v0, Lcom/google/android/gms/internal/measurement/al$f;

    sget-object v1, Lcom/google/android/gms/internal/measurement/al$f;->zzww:Lcom/google/android/gms/internal/measurement/al$f;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/measurement/dr;->a(Ljava/lang/Class;Lcom/google/android/gms/internal/measurement/dr;)V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/dr;-><init>()V

    .line 1093
    invoke-static {}, Lcom/google/android/gms/internal/measurement/fp;->sT()Lcom/google/android/gms/internal/measurement/fp;

    move-result-object v0

    .line 2
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/al$f;->zzwv:Lcom/google/android/gms/internal/measurement/dz;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/measurement/al$f;Lcom/google/android/gms/internal/measurement/al$g$a;)V
    .locals 1

    .line 2007
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$f;->zzwv:Lcom/google/android/gms/internal/measurement/dz;

    invoke-interface {v0}, Lcom/google/android/gms/internal/measurement/dz;->qM()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2008
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/al$f;->zzwv:Lcom/google/android/gms/internal/measurement/dz;

    .line 2009
    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/dr;->a(Lcom/google/android/gms/internal/measurement/dz;)Lcom/google/android/gms/internal/measurement/dz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/al$f;->zzwv:Lcom/google/android/gms/internal/measurement/dz;

    .line 2010
    :cond_0
    iget-object p0, p0, Lcom/google/android/gms/internal/measurement/al$f;->zzwv:Lcom/google/android/gms/internal/measurement/dz;

    .line 2061
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/dr$a;->sg()Lcom/google/android/gms/internal/measurement/dr;

    move-result-object p1

    .line 2010
    check-cast p1, Lcom/google/android/gms/internal/measurement/dr;

    check-cast p1, Lcom/google/android/gms/internal/measurement/al$g;

    invoke-interface {p0, p1}, Lcom/google/android/gms/internal/measurement/dz;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public static pA()Lcom/google/android/gms/internal/measurement/al$f$a;
    .locals 1

    .line 12
    sget-object v0, Lcom/google/android/gms/internal/measurement/al$f;->zzww:Lcom/google/android/gms/internal/measurement/al$f;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/dr;->rY()Lcom/google/android/gms/internal/measurement/dr$a;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$f$a;

    return-object v0
.end method

.method static synthetic pB()Lcom/google/android/gms/internal/measurement/al$f;
    .locals 1

    .line 32
    sget-object v0, Lcom/google/android/gms/internal/measurement/al$f;->zzww:Lcom/google/android/gms/internal/measurement/al$f;

    return-object v0
.end method


# virtual methods
.method protected final bm(I)Ljava/lang/Object;
    .locals 3

    .line 13
    sget-object v0, Lcom/google/android/gms/internal/measurement/ak;->aqY:[I

    const/4 v1, 0x1

    sub-int/2addr p1, v1

    aget p1, v0, p1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    .line 31
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    const/4 p1, 0x0

    return-object p1

    .line 29
    :pswitch_1
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    .line 20
    :pswitch_2
    sget-object p1, Lcom/google/android/gms/internal/measurement/al$f;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    if-nez p1, :cond_1

    .line 22
    const-class v0, Lcom/google/android/gms/internal/measurement/al$f;

    monitor-enter v0

    .line 23
    :try_start_0
    sget-object p1, Lcom/google/android/gms/internal/measurement/al$f;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    if-nez p1, :cond_0

    .line 25
    new-instance p1, Lcom/google/android/gms/internal/measurement/dr$c;

    sget-object v1, Lcom/google/android/gms/internal/measurement/al$f;->zzww:Lcom/google/android/gms/internal/measurement/al$f;

    invoke-direct {p1, v1}, Lcom/google/android/gms/internal/measurement/dr$c;-><init>(Lcom/google/android/gms/internal/measurement/dr;)V

    .line 26
    sput-object p1, Lcom/google/android/gms/internal/measurement/al$f;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    .line 27
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    .line 19
    :pswitch_3
    sget-object p1, Lcom/google/android/gms/internal/measurement/al$f;->zzww:Lcom/google/android/gms/internal/measurement/al$f;

    return-object p1

    :pswitch_4
    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Object;

    const-string v2, "zzwv"

    aput-object v2, p1, v0

    .line 16
    const-class v0, Lcom/google/android/gms/internal/measurement/al$g;

    aput-object v0, p1, v1

    .line 18
    sget-object v0, Lcom/google/android/gms/internal/measurement/al$f;->zzww:Lcom/google/android/gms/internal/measurement/al$f;

    const-string v1, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/internal/measurement/al$f;->a(Lcom/google/android/gms/internal/measurement/fb;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 15
    :pswitch_5
    new-instance p1, Lcom/google/android/gms/internal/measurement/al$f$a;

    invoke-direct {p1, v0}, Lcom/google/android/gms/internal/measurement/al$f$a;-><init>(B)V

    return-object p1

    .line 14
    :pswitch_6
    new-instance p1, Lcom/google/android/gms/internal/measurement/al$f;

    invoke-direct {p1}, Lcom/google/android/gms/internal/measurement/al$f;-><init>()V

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
