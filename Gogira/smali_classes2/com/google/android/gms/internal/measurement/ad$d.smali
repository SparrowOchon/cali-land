.class public final Lcom/google/android/gms/internal/measurement/ad$d;
.super Lcom/google/android/gms/internal/measurement/dr;

# interfaces
.implements Lcom/google/android/gms/internal/measurement/fd;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/measurement/ad;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/measurement/ad$d$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/measurement/dr<",
        "Lcom/google/android/gms/internal/measurement/ad$d;",
        "Lcom/google/android/gms/internal/measurement/ad$d$a;",
        ">;",
        "Lcom/google/android/gms/internal/measurement/fd;"
    }
.end annotation


# static fields
.field private static volatile zzuo:Lcom/google/android/gms/internal/measurement/fk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/fk<",
            "Lcom/google/android/gms/internal/measurement/ad$d;",
            ">;"
        }
    .end annotation
.end field

.field private static final zzvj:Lcom/google/android/gms/internal/measurement/ad$d;


# instance fields
.field public zzue:I

.field public zzuf:I

.field public zzuk:Z

.field public zzul:Z

.field public zzum:Z

.field public zzvh:Ljava/lang/String;

.field private zzvi:Lcom/google/android/gms/internal/measurement/ad$b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 44
    new-instance v0, Lcom/google/android/gms/internal/measurement/ad$d;

    invoke-direct {v0}, Lcom/google/android/gms/internal/measurement/ad$d;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/measurement/ad$d;->zzvj:Lcom/google/android/gms/internal/measurement/ad$d;

    .line 45
    const-class v0, Lcom/google/android/gms/internal/measurement/ad$d;

    sget-object v1, Lcom/google/android/gms/internal/measurement/ad$d;->zzvj:Lcom/google/android/gms/internal/measurement/ad$d;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/measurement/dr;->a(Ljava/lang/Class;Lcom/google/android/gms/internal/measurement/dr;)V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/dr;-><init>()V

    const-string v0, ""

    .line 2
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/ad$d;->zzvh:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/measurement/ad$d;Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 1009
    iget v0, p0, Lcom/google/android/gms/internal/measurement/ad$d;->zzue:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/gms/internal/measurement/ad$d;->zzue:I

    .line 1010
    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/ad$d;->zzvh:Ljava/lang/String;

    return-void

    .line 1008
    :cond_0
    new-instance p0, Ljava/lang/NullPointerException;

    invoke-direct {p0}, Ljava/lang/NullPointerException;-><init>()V

    throw p0
.end method

.method public static b([BLcom/google/android/gms/internal/measurement/de;)Lcom/google/android/gms/internal/measurement/ad$d;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/measurement/ec;
        }
    .end annotation

    .line 17
    sget-object v0, Lcom/google/android/gms/internal/measurement/ad$d;->zzvj:Lcom/google/android/gms/internal/measurement/ad$d;

    invoke-static {v0, p0, p1}, Lcom/google/android/gms/internal/measurement/dr;->a(Lcom/google/android/gms/internal/measurement/dr;[BLcom/google/android/gms/internal/measurement/de;)Lcom/google/android/gms/internal/measurement/dr;

    move-result-object p0

    check-cast p0, Lcom/google/android/gms/internal/measurement/ad$d;

    return-object p0
.end method

.method static synthetic oS()Lcom/google/android/gms/internal/measurement/ad$d;
    .locals 1

    .line 42
    sget-object v0, Lcom/google/android/gms/internal/measurement/ad$d;->zzvj:Lcom/google/android/gms/internal/measurement/ad$d;

    return-object v0
.end method

.method public static oz()Lcom/google/android/gms/internal/measurement/fk;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/internal/measurement/fk<",
            "Lcom/google/android/gms/internal/measurement/ad$d;",
            ">;"
        }
    .end annotation

    .line 37
    sget-object v0, Lcom/google/android/gms/internal/measurement/ad$d;->zzvj:Lcom/google/android/gms/internal/measurement/ad$d;

    .line 38
    sget v1, Lcom/google/android/gms/internal/measurement/dr$d;->avg:I

    .line 39
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/measurement/dr;->bm(I)Ljava/lang/Object;

    move-result-object v0

    .line 40
    check-cast v0, Lcom/google/android/gms/internal/measurement/fk;

    return-object v0
.end method


# virtual methods
.method protected final bm(I)Ljava/lang/Object;
    .locals 3

    .line 18
    sget-object v0, Lcom/google/android/gms/internal/measurement/ac;->aqY:[I

    const/4 v1, 0x1

    sub-int/2addr p1, v1

    aget p1, v0, p1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    .line 36
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    const/4 p1, 0x0

    return-object p1

    .line 34
    :pswitch_1
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    .line 25
    :pswitch_2
    sget-object p1, Lcom/google/android/gms/internal/measurement/ad$d;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    if-nez p1, :cond_1

    .line 27
    const-class v0, Lcom/google/android/gms/internal/measurement/ad$d;

    monitor-enter v0

    .line 28
    :try_start_0
    sget-object p1, Lcom/google/android/gms/internal/measurement/ad$d;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    if-nez p1, :cond_0

    .line 30
    new-instance p1, Lcom/google/android/gms/internal/measurement/dr$c;

    sget-object v1, Lcom/google/android/gms/internal/measurement/ad$d;->zzvj:Lcom/google/android/gms/internal/measurement/ad$d;

    invoke-direct {p1, v1}, Lcom/google/android/gms/internal/measurement/dr$c;-><init>(Lcom/google/android/gms/internal/measurement/dr;)V

    .line 31
    sput-object p1, Lcom/google/android/gms/internal/measurement/ad$d;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    .line 32
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    .line 24
    :pswitch_3
    sget-object p1, Lcom/google/android/gms/internal/measurement/ad$d;->zzvj:Lcom/google/android/gms/internal/measurement/ad$d;

    return-object p1

    :pswitch_4
    const/4 p1, 0x7

    new-array p1, p1, [Ljava/lang/Object;

    const-string v2, "zzue"

    aput-object v2, p1, v0

    const-string v0, "zzuf"

    aput-object v0, p1, v1

    const/4 v0, 0x2

    const-string v1, "zzvh"

    aput-object v1, p1, v0

    const/4 v0, 0x3

    const-string v1, "zzvi"

    aput-object v1, p1, v0

    const/4 v0, 0x4

    const-string v1, "zzuk"

    aput-object v1, p1, v0

    const/4 v0, 0x5

    const-string v1, "zzul"

    aput-object v1, p1, v0

    const/4 v0, 0x6

    const-string v1, "zzum"

    aput-object v1, p1, v0

    .line 23
    sget-object v0, Lcom/google/android/gms/internal/measurement/ad$d;->zzvj:Lcom/google/android/gms/internal/measurement/ad$d;

    const-string v1, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001\u0004\u0000\u0002\u0008\u0001\u0003\t\u0002\u0004\u0007\u0003\u0005\u0007\u0004\u0006\u0007\u0005"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/internal/measurement/ad$d;->a(Lcom/google/android/gms/internal/measurement/fb;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 20
    :pswitch_5
    new-instance p1, Lcom/google/android/gms/internal/measurement/ad$d$a;

    invoke-direct {p1, v0}, Lcom/google/android/gms/internal/measurement/ad$d$a;-><init>(B)V

    return-object p1

    .line 19
    :pswitch_6
    new-instance p1, Lcom/google/android/gms/internal/measurement/ad$d;

    invoke-direct {p1}, Lcom/google/android/gms/internal/measurement/ad$d;-><init>()V

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final oR()Lcom/google/android/gms/internal/measurement/ad$b;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/ad$d;->zzvi:Lcom/google/android/gms/internal/measurement/ad$b;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/internal/measurement/ad$b;->oI()Lcom/google/android/gms/internal/measurement/ad$b;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public final ox()Z
    .locals 2

    .line 4
    iget v0, p0, Lcom/google/android/gms/internal/measurement/ad$d;->zzue:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
