.class public final Lcom/google/android/gms/internal/measurement/ad$b;
.super Lcom/google/android/gms/internal/measurement/dr;

# interfaces
.implements Lcom/google/android/gms/internal/measurement/fd;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/measurement/ad;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/measurement/ad$b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/measurement/dr<",
        "Lcom/google/android/gms/internal/measurement/ad$b;",
        "Lcom/google/android/gms/internal/measurement/ad$b$a;",
        ">;",
        "Lcom/google/android/gms/internal/measurement/fd;"
    }
.end annotation


# static fields
.field private static volatile zzuo:Lcom/google/android/gms/internal/measurement/fk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/fk<",
            "Lcom/google/android/gms/internal/measurement/ad$b;",
            ">;"
        }
    .end annotation
.end field

.field private static final zzut:Lcom/google/android/gms/internal/measurement/ad$b;


# instance fields
.field private zzue:I

.field private zzup:Lcom/google/android/gms/internal/measurement/ad$e;

.field private zzuq:Lcom/google/android/gms/internal/measurement/ad$c;

.field public zzur:Z

.field public zzus:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 38
    new-instance v0, Lcom/google/android/gms/internal/measurement/ad$b;

    invoke-direct {v0}, Lcom/google/android/gms/internal/measurement/ad$b;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/measurement/ad$b;->zzut:Lcom/google/android/gms/internal/measurement/ad$b;

    .line 39
    const-class v0, Lcom/google/android/gms/internal/measurement/ad$b;

    sget-object v1, Lcom/google/android/gms/internal/measurement/ad$b;->zzut:Lcom/google/android/gms/internal/measurement/ad$b;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/measurement/dr;->a(Ljava/lang/Class;Lcom/google/android/gms/internal/measurement/dr;)V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/dr;-><init>()V

    const-string v0, ""

    .line 2
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/ad$b;->zzus:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/measurement/ad$b;Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 1013
    iget v0, p0, Lcom/google/android/gms/internal/measurement/ad$b;->zzue:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/android/gms/internal/measurement/ad$b;->zzue:I

    .line 1014
    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/ad$b;->zzus:Ljava/lang/String;

    return-void

    .line 1012
    :cond_0
    new-instance p0, Ljava/lang/NullPointerException;

    invoke-direct {p0}, Ljava/lang/NullPointerException;-><init>()V

    throw p0
.end method

.method public static oI()Lcom/google/android/gms/internal/measurement/ad$b;
    .locals 1

    .line 35
    sget-object v0, Lcom/google/android/gms/internal/measurement/ad$b;->zzut:Lcom/google/android/gms/internal/measurement/ad$b;

    return-object v0
.end method

.method static synthetic oJ()Lcom/google/android/gms/internal/measurement/ad$b;
    .locals 1

    .line 36
    sget-object v0, Lcom/google/android/gms/internal/measurement/ad$b;->zzut:Lcom/google/android/gms/internal/measurement/ad$b;

    return-object v0
.end method


# virtual methods
.method protected final bm(I)Ljava/lang/Object;
    .locals 3

    .line 16
    sget-object v0, Lcom/google/android/gms/internal/measurement/ac;->aqY:[I

    const/4 v1, 0x1

    sub-int/2addr p1, v1

    aget p1, v0, p1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    .line 34
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    const/4 p1, 0x0

    return-object p1

    .line 32
    :pswitch_1
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    .line 23
    :pswitch_2
    sget-object p1, Lcom/google/android/gms/internal/measurement/ad$b;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    if-nez p1, :cond_1

    .line 25
    const-class v0, Lcom/google/android/gms/internal/measurement/ad$b;

    monitor-enter v0

    .line 26
    :try_start_0
    sget-object p1, Lcom/google/android/gms/internal/measurement/ad$b;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    if-nez p1, :cond_0

    .line 28
    new-instance p1, Lcom/google/android/gms/internal/measurement/dr$c;

    sget-object v1, Lcom/google/android/gms/internal/measurement/ad$b;->zzut:Lcom/google/android/gms/internal/measurement/ad$b;

    invoke-direct {p1, v1}, Lcom/google/android/gms/internal/measurement/dr$c;-><init>(Lcom/google/android/gms/internal/measurement/dr;)V

    .line 29
    sput-object p1, Lcom/google/android/gms/internal/measurement/ad$b;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    .line 30
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    .line 22
    :pswitch_3
    sget-object p1, Lcom/google/android/gms/internal/measurement/ad$b;->zzut:Lcom/google/android/gms/internal/measurement/ad$b;

    return-object p1

    :pswitch_4
    const/4 p1, 0x5

    new-array p1, p1, [Ljava/lang/Object;

    const-string v2, "zzue"

    aput-object v2, p1, v0

    const-string v0, "zzup"

    aput-object v0, p1, v1

    const/4 v0, 0x2

    const-string v1, "zzuq"

    aput-object v1, p1, v0

    const/4 v0, 0x3

    const-string v1, "zzur"

    aput-object v1, p1, v0

    const/4 v0, 0x4

    const-string v1, "zzus"

    aput-object v1, p1, v0

    .line 21
    sget-object v0, Lcom/google/android/gms/internal/measurement/ad$b;->zzut:Lcom/google/android/gms/internal/measurement/ad$b;

    const-string v1, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001\t\u0000\u0002\t\u0001\u0003\u0007\u0002\u0004\u0008\u0003"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/internal/measurement/ad$b;->a(Lcom/google/android/gms/internal/measurement/fb;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 18
    :pswitch_5
    new-instance p1, Lcom/google/android/gms/internal/measurement/ad$b$a;

    invoke-direct {p1, v0}, Lcom/google/android/gms/internal/measurement/ad$b$a;-><init>(B)V

    return-object p1

    .line 17
    :pswitch_6
    new-instance p1, Lcom/google/android/gms/internal/measurement/ad$b;

    invoke-direct {p1}, Lcom/google/android/gms/internal/measurement/ad$b;-><init>()V

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final oD()Z
    .locals 2

    .line 4
    iget v0, p0, Lcom/google/android/gms/internal/measurement/ad$b;->zzue:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final oE()Lcom/google/android/gms/internal/measurement/ad$e;
    .locals 1

    .line 5
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/ad$b;->zzup:Lcom/google/android/gms/internal/measurement/ad$e;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/internal/measurement/ad$e;->oX()Lcom/google/android/gms/internal/measurement/ad$e;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public final oF()Z
    .locals 1

    .line 6
    iget v0, p0, Lcom/google/android/gms/internal/measurement/ad$b;->zzue:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final oG()Lcom/google/android/gms/internal/measurement/ad$c;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/ad$b;->zzuq:Lcom/google/android/gms/internal/measurement/ad$c;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/internal/measurement/ad$c;->oN()Lcom/google/android/gms/internal/measurement/ad$c;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public final oH()Z
    .locals 1

    .line 8
    iget v0, p0, Lcom/google/android/gms/internal/measurement/ad$b;->zzue:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
