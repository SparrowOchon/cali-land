.class public final Lcom/google/android/gms/internal/measurement/ij;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/measurement/ik;


# static fields
.field private static final ayR:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final ayS:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final ayT:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final ayU:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final ayV:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final ayW:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final ayX:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final ayY:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final ayZ:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final azA:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final aza:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final azb:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final azc:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final azd:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final aze:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final azf:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final azg:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final azh:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final azi:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final azj:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final azk:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final azl:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final azm:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final azn:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final azo:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final azp:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final azq:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final azr:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final azs:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final azt:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final azu:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final azv:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final azw:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final azx:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final azy:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final azz:Lcom/google/android/gms/internal/measurement/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bf<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .line 38
    new-instance v0, Lcom/google/android/gms/internal/measurement/bm;

    const-string v1, "com.google.android.gms.measurement"

    .line 39
    invoke-static {v1}, Lcom/google/android/gms/internal/measurement/bg;->bT(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/measurement/bm;-><init>(Landroid/net/Uri;)V

    const-wide/16 v1, 0x2710

    const-string v3, "measurement.ad_id_cache_time"

    .line 1014
    invoke-static {v0, v3, v1, v2}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v3

    .line 40
    sput-object v3, Lcom/google/android/gms/internal/measurement/ij;->ayR:Lcom/google/android/gms/internal/measurement/bf;

    const-wide/32 v3, 0x36ee80

    const-string v5, "measurement.config.cache_time"

    .line 2014
    invoke-static {v0, v5, v3, v4}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v5

    .line 41
    sput-object v5, Lcom/google/android/gms/internal/measurement/ij;->ayS:Lcom/google/android/gms/internal/measurement/bf;

    const-string v5, "measurement.log_tag"

    const-string v6, "FA"

    .line 2017
    invoke-static {v0, v5, v6}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v5

    .line 42
    sput-object v5, Lcom/google/android/gms/internal/measurement/ij;->ayT:Lcom/google/android/gms/internal/measurement/bf;

    const-string v5, "measurement.config.url_authority"

    const-string v6, "app-measurement.com"

    .line 3017
    invoke-static {v0, v5, v6}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v5

    .line 43
    sput-object v5, Lcom/google/android/gms/internal/measurement/ij;->ayU:Lcom/google/android/gms/internal/measurement/bf;

    const-string v5, "measurement.config.url_scheme"

    const-string v6, "https"

    .line 4017
    invoke-static {v0, v5, v6}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v5

    .line 44
    sput-object v5, Lcom/google/android/gms/internal/measurement/ij;->ayV:Lcom/google/android/gms/internal/measurement/bf;

    const-wide/16 v5, 0x3e8

    const-string v7, "measurement.upload.debug_upload_interval"

    .line 5014
    invoke-static {v0, v7, v5, v6}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v7

    .line 45
    sput-object v7, Lcom/google/android/gms/internal/measurement/ij;->ayW:Lcom/google/android/gms/internal/measurement/bf;

    const-string v7, "measurement.lifetimevalue.max_currency_tracked"

    const-wide/16 v8, 0x4

    .line 6014
    invoke-static {v0, v7, v8, v9}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v7

    .line 46
    sput-object v7, Lcom/google/android/gms/internal/measurement/ij;->ayX:Lcom/google/android/gms/internal/measurement/bf;

    const-wide/32 v7, 0x186a0

    const-string v9, "measurement.store.max_stored_events_per_app"

    .line 7014
    invoke-static {v0, v9, v7, v8}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v9

    .line 47
    sput-object v9, Lcom/google/android/gms/internal/measurement/ij;->ayY:Lcom/google/android/gms/internal/measurement/bf;

    const-string v9, "measurement.experiment.max_ids"

    const-wide/16 v10, 0x32

    .line 8014
    invoke-static {v0, v9, v10, v11}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v9

    .line 48
    sput-object v9, Lcom/google/android/gms/internal/measurement/ij;->ayZ:Lcom/google/android/gms/internal/measurement/bf;

    const-string v9, "measurement.audience.filter_result_max_count"

    const-wide/16 v10, 0xc8

    .line 9014
    invoke-static {v0, v9, v10, v11}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v9

    .line 49
    sput-object v9, Lcom/google/android/gms/internal/measurement/ij;->aza:Lcom/google/android/gms/internal/measurement/bf;

    const-string v9, "measurement.alarm_manager.minimum_interval"

    const-wide/32 v10, 0xea60

    .line 10014
    invoke-static {v0, v9, v10, v11}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v9

    .line 50
    sput-object v9, Lcom/google/android/gms/internal/measurement/ij;->azb:Lcom/google/android/gms/internal/measurement/bf;

    const-wide/16 v9, 0x1f4

    const-string v11, "measurement.upload.minimum_delay"

    .line 11014
    invoke-static {v0, v11, v9, v10}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v11

    .line 51
    sput-object v11, Lcom/google/android/gms/internal/measurement/ij;->azc:Lcom/google/android/gms/internal/measurement/bf;

    const-wide/32 v11, 0x5265c00

    const-string v13, "measurement.monitoring.sample_period_millis"

    .line 12014
    invoke-static {v0, v13, v11, v12}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v13

    .line 52
    sput-object v13, Lcom/google/android/gms/internal/measurement/ij;->azd:Lcom/google/android/gms/internal/measurement/bf;

    const-string v13, "measurement.upload.realtime_upload_interval"

    .line 13014
    invoke-static {v0, v13, v1, v2}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v1

    .line 53
    sput-object v1, Lcom/google/android/gms/internal/measurement/ij;->aze:Lcom/google/android/gms/internal/measurement/bf;

    const-string v1, "measurement.upload.refresh_blacklisted_config_interval"

    const-wide/32 v13, 0x240c8400

    .line 14014
    invoke-static {v0, v1, v13, v14}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v1

    .line 54
    sput-object v1, Lcom/google/android/gms/internal/measurement/ij;->azf:Lcom/google/android/gms/internal/measurement/bf;

    const-string v1, "measurement.config.cache_time.service"

    .line 15014
    invoke-static {v0, v1, v11, v12}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v1

    .line 55
    sput-object v1, Lcom/google/android/gms/internal/measurement/ij;->azg:Lcom/google/android/gms/internal/measurement/bf;

    const-string v1, "measurement.service_client.idle_disconnect_millis"

    const-wide/16 v13, 0x1388

    .line 16014
    invoke-static {v0, v1, v13, v14}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v1

    .line 56
    sput-object v1, Lcom/google/android/gms/internal/measurement/ij;->azh:Lcom/google/android/gms/internal/measurement/bf;

    const-string v1, "measurement.log_tag.service"

    const-string v2, "FA-SVC"

    .line 16017
    invoke-static {v0, v1, v2}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v1

    .line 57
    sput-object v1, Lcom/google/android/gms/internal/measurement/ij;->azi:Lcom/google/android/gms/internal/measurement/bf;

    const-string v1, "measurement.upload.stale_data_deletion_interval"

    .line 17014
    invoke-static {v0, v1, v11, v12}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v1

    .line 58
    sput-object v1, Lcom/google/android/gms/internal/measurement/ij;->azj:Lcom/google/android/gms/internal/measurement/bf;

    const-string v1, "measurement.upload.backoff_period"

    const-wide/32 v11, 0x2932e00

    .line 18014
    invoke-static {v0, v1, v11, v12}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v1

    .line 59
    sput-object v1, Lcom/google/android/gms/internal/measurement/ij;->azk:Lcom/google/android/gms/internal/measurement/bf;

    const-string v1, "measurement.upload.initial_upload_delay_time"

    const-wide/16 v11, 0x3a98

    .line 19014
    invoke-static {v0, v1, v11, v12}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v1

    .line 60
    sput-object v1, Lcom/google/android/gms/internal/measurement/ij;->azl:Lcom/google/android/gms/internal/measurement/bf;

    const-string v1, "measurement.upload.interval"

    .line 20014
    invoke-static {v0, v1, v3, v4}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v1

    .line 61
    sput-object v1, Lcom/google/android/gms/internal/measurement/ij;->azm:Lcom/google/android/gms/internal/measurement/bf;

    const-wide/32 v1, 0x10000

    const-string v11, "measurement.upload.max_bundle_size"

    .line 21014
    invoke-static {v0, v11, v1, v2}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v11

    .line 62
    sput-object v11, Lcom/google/android/gms/internal/measurement/ij;->azn:Lcom/google/android/gms/internal/measurement/bf;

    const-string v11, "measurement.upload.max_bundles"

    const-wide/16 v12, 0x64

    .line 22014
    invoke-static {v0, v11, v12, v13}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v11

    .line 63
    sput-object v11, Lcom/google/android/gms/internal/measurement/ij;->azo:Lcom/google/android/gms/internal/measurement/bf;

    const-string v11, "measurement.upload.max_conversions_per_day"

    .line 23014
    invoke-static {v0, v11, v9, v10}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v9

    .line 64
    sput-object v9, Lcom/google/android/gms/internal/measurement/ij;->azp:Lcom/google/android/gms/internal/measurement/bf;

    const-string v9, "measurement.upload.max_error_events_per_day"

    .line 24014
    invoke-static {v0, v9, v5, v6}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v9

    .line 65
    sput-object v9, Lcom/google/android/gms/internal/measurement/ij;->azq:Lcom/google/android/gms/internal/measurement/bf;

    const-string v9, "measurement.upload.max_events_per_bundle"

    .line 25014
    invoke-static {v0, v9, v5, v6}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v5

    .line 66
    sput-object v5, Lcom/google/android/gms/internal/measurement/ij;->azr:Lcom/google/android/gms/internal/measurement/bf;

    const-string v5, "measurement.upload.max_events_per_day"

    .line 26014
    invoke-static {v0, v5, v7, v8}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v5

    .line 67
    sput-object v5, Lcom/google/android/gms/internal/measurement/ij;->azs:Lcom/google/android/gms/internal/measurement/bf;

    const-string v5, "measurement.upload.max_public_events_per_day"

    const-wide/32 v6, 0xc350

    .line 27014
    invoke-static {v0, v5, v6, v7}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v5

    .line 68
    sput-object v5, Lcom/google/android/gms/internal/measurement/ij;->azt:Lcom/google/android/gms/internal/measurement/bf;

    const-string v5, "measurement.upload.max_queue_time"

    const-wide v6, 0x90321000L

    .line 28014
    invoke-static {v0, v5, v6, v7}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v5

    .line 69
    sput-object v5, Lcom/google/android/gms/internal/measurement/ij;->azu:Lcom/google/android/gms/internal/measurement/bf;

    const-string v5, "measurement.upload.max_realtime_events_per_day"

    const-wide/16 v6, 0xa

    .line 29014
    invoke-static {v0, v5, v6, v7}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v5

    .line 70
    sput-object v5, Lcom/google/android/gms/internal/measurement/ij;->azv:Lcom/google/android/gms/internal/measurement/bf;

    const-string v5, "measurement.upload.max_batch_size"

    .line 30014
    invoke-static {v0, v5, v1, v2}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v1

    .line 71
    sput-object v1, Lcom/google/android/gms/internal/measurement/ij;->azw:Lcom/google/android/gms/internal/measurement/bf;

    const-string v1, "measurement.upload.retry_count"

    const-wide/16 v5, 0x6

    .line 31014
    invoke-static {v0, v1, v5, v6}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v1

    .line 72
    sput-object v1, Lcom/google/android/gms/internal/measurement/ij;->azx:Lcom/google/android/gms/internal/measurement/bf;

    const-string v1, "measurement.upload.retry_time"

    const-wide/32 v5, 0x1b7740

    .line 32014
    invoke-static {v0, v1, v5, v6}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v1

    .line 73
    sput-object v1, Lcom/google/android/gms/internal/measurement/ij;->azy:Lcom/google/android/gms/internal/measurement/bf;

    const-string v1, "measurement.upload.url"

    const-string v2, "https://app-measurement.com/a"

    .line 32017
    invoke-static {v0, v1, v2}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v1

    .line 74
    sput-object v1, Lcom/google/android/gms/internal/measurement/ij;->azz:Lcom/google/android/gms/internal/measurement/bf;

    const-string v1, "measurement.upload.window_interval"

    .line 33014
    invoke-static {v0, v1, v3, v4}, Lcom/google/android/gms/internal/measurement/bf;->a(Lcom/google/android/gms/internal/measurement/bm;Ljava/lang/String;J)Lcom/google/android/gms/internal/measurement/bf;

    move-result-object v0

    .line 75
    sput-object v0, Lcom/google/android/gms/internal/measurement/ij;->azA:Lcom/google/android/gms/internal/measurement/bf;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final tF()J
    .locals 2

    .line 2
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->ayR:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final tG()J
    .locals 2

    .line 3
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->ayS:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final tH()Ljava/lang/String;
    .locals 1

    .line 4
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->ayT:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final tI()Ljava/lang/String;
    .locals 1

    .line 5
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->ayU:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final tJ()Ljava/lang/String;
    .locals 1

    .line 6
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->ayV:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final tK()J
    .locals 2

    .line 7
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->ayW:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final tL()J
    .locals 2

    .line 8
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->ayX:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final tM()J
    .locals 2

    .line 9
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->ayY:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final tN()J
    .locals 2

    .line 10
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->ayZ:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final tO()J
    .locals 2

    .line 11
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->aza:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final tP()J
    .locals 2

    .line 12
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->azb:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final tQ()J
    .locals 2

    .line 13
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->azc:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final tR()J
    .locals 2

    .line 14
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->azd:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final tS()J
    .locals 2

    .line 15
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->aze:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final tT()J
    .locals 2

    .line 16
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->azf:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final tU()J
    .locals 2

    .line 18
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->azh:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final tV()J
    .locals 2

    .line 20
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->azj:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final tW()J
    .locals 2

    .line 21
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->azk:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final tX()J
    .locals 2

    .line 22
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->azl:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final tY()J
    .locals 2

    .line 23
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->azm:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final tZ()J
    .locals 2

    .line 24
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->azn:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final ua()J
    .locals 2

    .line 25
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->azo:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final ub()J
    .locals 2

    .line 26
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->azp:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final uc()J
    .locals 2

    .line 27
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->azq:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final ud()J
    .locals 2

    .line 28
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->azr:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final ue()J
    .locals 2

    .line 29
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->azs:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final uf()J
    .locals 2

    .line 30
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->azt:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final ug()J
    .locals 2

    .line 31
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->azu:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final uh()J
    .locals 2

    .line 32
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->azv:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final ui()J
    .locals 2

    .line 33
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->azw:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final uj()J
    .locals 2

    .line 34
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->azx:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final uk()J
    .locals 2

    .line 35
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->azy:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final ul()Ljava/lang/String;
    .locals 1

    .line 36
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->azz:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final um()J
    .locals 2

    .line 37
    sget-object v0, Lcom/google/android/gms/internal/measurement/ij;->azA:Lcom/google/android/gms/internal/measurement/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/bf;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method
