.class final Lcom/google/android/gms/internal/measurement/r;
.super Lcom/google/android/gms/internal/measurement/lw$a;


# instance fields
.field private final synthetic aqB:Lcom/google/android/gms/internal/measurement/lw;

.field private final synthetic aqQ:Ljava/lang/Long;

.field private final synthetic aqR:Ljava/lang/String;

.field private final synthetic aqS:Landroid/os/Bundle;

.field private final synthetic aqT:Z

.field private final synthetic aqU:Z

.field private final synthetic aqy:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/measurement/lw;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;ZZ)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/r;->aqB:Lcom/google/android/gms/internal/measurement/lw;

    const/4 p2, 0x0

    iput-object p2, p0, Lcom/google/android/gms/internal/measurement/r;->aqQ:Ljava/lang/Long;

    iput-object p3, p0, Lcom/google/android/gms/internal/measurement/r;->aqy:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/internal/measurement/r;->aqR:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/internal/measurement/r;->aqS:Landroid/os/Bundle;

    iput-boolean p6, p0, Lcom/google/android/gms/internal/measurement/r;->aqT:Z

    const/4 p2, 0x1

    iput-boolean p2, p0, Lcom/google/android/gms/internal/measurement/r;->aqU:Z

    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/measurement/lw$a;-><init>(Lcom/google/android/gms/internal/measurement/lw;)V

    return-void
.end method


# virtual methods
.method final ov()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/r;->aqQ:Ljava/lang/Long;

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/gms/internal/measurement/r;->timestamp:J

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_0
    move-wide v8, v0

    .line 3
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/r;->aqB:Lcom/google/android/gms/internal/measurement/lw;

    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/lw;->c(Lcom/google/android/gms/internal/measurement/lw;)Lcom/google/android/gms/internal/measurement/iu;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/r;->aqy:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/internal/measurement/r;->aqR:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/internal/measurement/r;->aqS:Landroid/os/Bundle;

    iget-boolean v6, p0, Lcom/google/android/gms/internal/measurement/r;->aqT:Z

    iget-boolean v7, p0, Lcom/google/android/gms/internal/measurement/r;->aqU:Z

    invoke-interface/range {v2 .. v9}, Lcom/google/android/gms/internal/measurement/iu;->logEvent(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;ZZJ)V

    return-void
.end method
