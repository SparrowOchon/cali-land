.class public final Lcom/google/android/gms/internal/measurement/ad$c;
.super Lcom/google/android/gms/internal/measurement/dr;

# interfaces
.implements Lcom/google/android/gms/internal/measurement/fd;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/measurement/ad;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/measurement/ad$c$a;,
        Lcom/google/android/gms/internal/measurement/ad$c$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/measurement/dr<",
        "Lcom/google/android/gms/internal/measurement/ad$c;",
        "Lcom/google/android/gms/internal/measurement/ad$c$a;",
        ">;",
        "Lcom/google/android/gms/internal/measurement/fd;"
    }
.end annotation


# static fields
.field private static volatile zzuo:Lcom/google/android/gms/internal/measurement/fk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/fk<",
            "Lcom/google/android/gms/internal/measurement/ad$c;",
            ">;"
        }
    .end annotation
.end field

.field private static final zzuz:Lcom/google/android/gms/internal/measurement/ad$c;


# instance fields
.field public zzue:I

.field private zzuu:I

.field public zzuv:Z

.field public zzuw:Ljava/lang/String;

.field public zzux:Ljava/lang/String;

.field public zzuy:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 39
    new-instance v0, Lcom/google/android/gms/internal/measurement/ad$c;

    invoke-direct {v0}, Lcom/google/android/gms/internal/measurement/ad$c;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/measurement/ad$c;->zzuz:Lcom/google/android/gms/internal/measurement/ad$c;

    .line 40
    const-class v0, Lcom/google/android/gms/internal/measurement/ad$c;

    sget-object v1, Lcom/google/android/gms/internal/measurement/ad$c;->zzuz:Lcom/google/android/gms/internal/measurement/ad$c;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/measurement/dr;->a(Ljava/lang/Class;Lcom/google/android/gms/internal/measurement/dr;)V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/dr;-><init>()V

    const-string v0, ""

    .line 2
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/ad$c;->zzuw:Ljava/lang/String;

    .line 3
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/ad$c;->zzux:Ljava/lang/String;

    .line 4
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/ad$c;->zzuy:Ljava/lang/String;

    return-void
.end method

.method public static oN()Lcom/google/android/gms/internal/measurement/ad$c;
    .locals 1

    .line 37
    sget-object v0, Lcom/google/android/gms/internal/measurement/ad$c;->zzuz:Lcom/google/android/gms/internal/measurement/ad$c;

    return-object v0
.end method

.method static synthetic oO()Lcom/google/android/gms/internal/measurement/ad$c;
    .locals 1

    .line 38
    sget-object v0, Lcom/google/android/gms/internal/measurement/ad$c;->zzuz:Lcom/google/android/gms/internal/measurement/ad$c;

    return-object v0
.end method


# virtual methods
.method protected final bm(I)Ljava/lang/Object;
    .locals 3

    .line 17
    sget-object v0, Lcom/google/android/gms/internal/measurement/ac;->aqY:[I

    const/4 v1, 0x1

    sub-int/2addr p1, v1

    aget p1, v0, p1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    .line 36
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    const/4 p1, 0x0

    return-object p1

    .line 34
    :pswitch_1
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    .line 25
    :pswitch_2
    sget-object p1, Lcom/google/android/gms/internal/measurement/ad$c;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    if-nez p1, :cond_1

    .line 27
    const-class v0, Lcom/google/android/gms/internal/measurement/ad$c;

    monitor-enter v0

    .line 28
    :try_start_0
    sget-object p1, Lcom/google/android/gms/internal/measurement/ad$c;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    if-nez p1, :cond_0

    .line 30
    new-instance p1, Lcom/google/android/gms/internal/measurement/dr$c;

    sget-object v1, Lcom/google/android/gms/internal/measurement/ad$c;->zzuz:Lcom/google/android/gms/internal/measurement/ad$c;

    invoke-direct {p1, v1}, Lcom/google/android/gms/internal/measurement/dr$c;-><init>(Lcom/google/android/gms/internal/measurement/dr;)V

    .line 31
    sput-object p1, Lcom/google/android/gms/internal/measurement/ad$c;->zzuo:Lcom/google/android/gms/internal/measurement/fk;

    .line 32
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    .line 24
    :pswitch_3
    sget-object p1, Lcom/google/android/gms/internal/measurement/ad$c;->zzuz:Lcom/google/android/gms/internal/measurement/ad$c;

    return-object p1

    :pswitch_4
    const/4 p1, 0x7

    new-array p1, p1, [Ljava/lang/Object;

    const-string v2, "zzue"

    aput-object v2, p1, v0

    const-string v0, "zzuu"

    aput-object v0, p1, v1

    const/4 v0, 0x2

    .line 21
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ad$c$b;->oQ()Lcom/google/android/gms/internal/measurement/dy;

    move-result-object v1

    aput-object v1, p1, v0

    const/4 v0, 0x3

    const-string v1, "zzuv"

    aput-object v1, p1, v0

    const/4 v0, 0x4

    const-string v1, "zzuw"

    aput-object v1, p1, v0

    const/4 v0, 0x5

    const-string v1, "zzux"

    aput-object v1, p1, v0

    const/4 v0, 0x6

    const-string v1, "zzuy"

    aput-object v1, p1, v0

    .line 23
    sget-object v0, Lcom/google/android/gms/internal/measurement/ad$c;->zzuz:Lcom/google/android/gms/internal/measurement/ad$c;

    const-string v1, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0000\u0000\u0001\u000c\u0000\u0002\u0007\u0001\u0003\u0008\u0002\u0004\u0008\u0003\u0005\u0008\u0004"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/internal/measurement/ad$c;->a(Lcom/google/android/gms/internal/measurement/fb;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 19
    :pswitch_5
    new-instance p1, Lcom/google/android/gms/internal/measurement/ad$c$a;

    invoke-direct {p1, v0}, Lcom/google/android/gms/internal/measurement/ad$c$a;-><init>(B)V

    return-object p1

    .line 18
    :pswitch_6
    new-instance p1, Lcom/google/android/gms/internal/measurement/ad$c;

    invoke-direct {p1}, Lcom/google/android/gms/internal/measurement/ad$c;-><init>()V

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final oK()Z
    .locals 2

    .line 6
    iget v0, p0, Lcom/google/android/gms/internal/measurement/ad$c;->zzue:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final oL()Lcom/google/android/gms/internal/measurement/ad$c$b;
    .locals 1

    .line 7
    iget v0, p0, Lcom/google/android/gms/internal/measurement/ad$c;->zzuu:I

    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/ad$c$b;->bo(I)Lcom/google/android/gms/internal/measurement/ad$c$b;

    move-result-object v0

    if-nez v0, :cond_0

    .line 8
    sget-object v0, Lcom/google/android/gms/internal/measurement/ad$c$b;->aqZ:Lcom/google/android/gms/internal/measurement/ad$c$b;

    :cond_0
    return-object v0
.end method

.method public final oM()Z
    .locals 1

    .line 9
    iget v0, p0, Lcom/google/android/gms/internal/measurement/ad$c;->zzue:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
