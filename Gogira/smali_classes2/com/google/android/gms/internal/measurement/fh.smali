.class final Lcom/google/android/gms/internal/measurement/fh;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/measurement/fq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/internal/measurement/fq<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final awg:Lcom/google/android/gms/internal/measurement/fb;

.field private final awh:Z

.field private final awq:Lcom/google/android/gms/internal/measurement/gi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/gi<",
            "**>;"
        }
    .end annotation
.end field

.field private final awr:Lcom/google/android/gms/internal/measurement/dg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/dg<",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/gms/internal/measurement/gi;Lcom/google/android/gms/internal/measurement/dg;Lcom/google/android/gms/internal/measurement/fb;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/measurement/gi<",
            "**>;",
            "Lcom/google/android/gms/internal/measurement/dg<",
            "*>;",
            "Lcom/google/android/gms/internal/measurement/fb;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/fh;->awq:Lcom/google/android/gms/internal/measurement/gi;

    .line 3
    invoke-virtual {p2, p3}, Lcom/google/android/gms/internal/measurement/dg;->e(Lcom/google/android/gms/internal/measurement/fb;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/google/android/gms/internal/measurement/fh;->awh:Z

    .line 4
    iput-object p2, p0, Lcom/google/android/gms/internal/measurement/fh;->awr:Lcom/google/android/gms/internal/measurement/dg;

    .line 5
    iput-object p3, p0, Lcom/google/android/gms/internal/measurement/fh;->awg:Lcom/google/android/gms/internal/measurement/fb;

    return-void
.end method

.method static a(Lcom/google/android/gms/internal/measurement/gi;Lcom/google/android/gms/internal/measurement/dg;Lcom/google/android/gms/internal/measurement/fb;)Lcom/google/android/gms/internal/measurement/fh;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/gms/internal/measurement/gi<",
            "**>;",
            "Lcom/google/android/gms/internal/measurement/dg<",
            "*>;",
            "Lcom/google/android/gms/internal/measurement/fb;",
            ")",
            "Lcom/google/android/gms/internal/measurement/fh<",
            "TT;>;"
        }
    .end annotation

    .line 7
    new-instance v0, Lcom/google/android/gms/internal/measurement/fh;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/internal/measurement/fh;-><init>(Lcom/google/android/gms/internal/measurement/gi;Lcom/google/android/gms/internal/measurement/dg;Lcom/google/android/gms/internal/measurement/fb;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Lcom/google/android/gms/internal/measurement/fr;Lcom/google/android/gms/internal/measurement/de;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/google/android/gms/internal/measurement/fr;",
            "Lcom/google/android/gms/internal/measurement/de;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/fh;->awq:Lcom/google/android/gms/internal/measurement/gi;

    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/fh;->awr:Lcom/google/android/gms/internal/measurement/dg;

    .line 105
    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/measurement/gi;->aB(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 106
    invoke-virtual {v1, p1}, Lcom/google/android/gms/internal/measurement/dg;->am(Ljava/lang/Object;)Lcom/google/android/gms/internal/measurement/dh;

    .line 107
    :cond_0
    :try_start_0
    invoke-interface {p2}, Lcom/google/android/gms/internal/measurement/fr;->rl()I

    move-result v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v4, 0x7fffffff

    if-ne v3, v4, :cond_1

    .line 109
    invoke-virtual {v0, p1, v2}, Lcom/google/android/gms/internal/measurement/gi;->j(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void

    .line 112
    :cond_1
    :try_start_1
    invoke-interface {p2}, Lcom/google/android/gms/internal/measurement/fr;->getTag()I

    move-result v3

    const/16 v5, 0xb

    if-eq v3, v5, :cond_4

    and-int/lit8 v4, v3, 0x7

    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    .line 117
    iget-object v4, p0, Lcom/google/android/gms/internal/measurement/fh;->awg:Lcom/google/android/gms/internal/measurement/fb;

    ushr-int/lit8 v3, v3, 0x3

    .line 119
    invoke-virtual {v1, p3, v4, v3}, Lcom/google/android/gms/internal/measurement/dg;->a(Lcom/google/android/gms/internal/measurement/de;Lcom/google/android/gms/internal/measurement/fb;I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 121
    invoke-virtual {v1}, Lcom/google/android/gms/internal/measurement/dg;->rK()V

    goto :goto_1

    .line 123
    :cond_2
    invoke-virtual {v0, v2, p2}, Lcom/google/android/gms/internal/measurement/gi;->a(Ljava/lang/Object;Lcom/google/android/gms/internal/measurement/fr;)Z

    move-result v3

    goto :goto_2

    .line 124
    :cond_3
    invoke-interface {p2}, Lcom/google/android/gms/internal/measurement/fr;->rm()Z

    move-result v3

    goto :goto_2

    :cond_4
    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v6, v5

    .line 128
    :cond_5
    :goto_0
    invoke-interface {p2}, Lcom/google/android/gms/internal/measurement/fr;->rl()I

    move-result v7

    if-eq v7, v4, :cond_9

    .line 130
    invoke-interface {p2}, Lcom/google/android/gms/internal/measurement/fr;->getTag()I

    move-result v7

    const/16 v8, 0x10

    if-ne v7, v8, :cond_6

    .line 132
    invoke-interface {p2}, Lcom/google/android/gms/internal/measurement/fr;->rc()I

    move-result v3

    .line 133
    iget-object v5, p0, Lcom/google/android/gms/internal/measurement/fh;->awg:Lcom/google/android/gms/internal/measurement/fb;

    .line 134
    invoke-virtual {v1, p3, v5, v3}, Lcom/google/android/gms/internal/measurement/dg;->a(Lcom/google/android/gms/internal/measurement/de;Lcom/google/android/gms/internal/measurement/fb;I)Ljava/lang/Object;

    move-result-object v5

    goto :goto_0

    :cond_6
    const/16 v8, 0x1a

    if-ne v7, v8, :cond_8

    if-eqz v5, :cond_7

    .line 138
    invoke-virtual {v1}, Lcom/google/android/gms/internal/measurement/dg;->rK()V

    goto :goto_0

    .line 140
    :cond_7
    invoke-interface {p2}, Lcom/google/android/gms/internal/measurement/fr;->rb()Lcom/google/android/gms/internal/measurement/cj;

    move-result-object v6

    goto :goto_0

    .line 142
    :cond_8
    invoke-interface {p2}, Lcom/google/android/gms/internal/measurement/fr;->rm()Z

    move-result v7

    if-nez v7, :cond_5

    .line 143
    :cond_9
    invoke-interface {p2}, Lcom/google/android/gms/internal/measurement/fr;->getTag()I

    move-result v4

    const/16 v7, 0xc

    if-ne v4, v7, :cond_c

    if-eqz v6, :cond_b

    if-eqz v5, :cond_a

    .line 147
    invoke-virtual {v1}, Lcom/google/android/gms/internal/measurement/dg;->rL()V

    goto :goto_1

    .line 148
    :cond_a
    invoke-virtual {v0, v2, v3, v6}, Lcom/google/android/gms/internal/measurement/gi;->a(Ljava/lang/Object;ILcom/google/android/gms/internal/measurement/cj;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_b
    :goto_1
    const/4 v3, 0x1

    :goto_2
    if-nez v3, :cond_0

    .line 151
    invoke-virtual {v0, p1, v2}, Lcom/google/android/gms/internal/measurement/gi;->j(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void

    .line 144
    :cond_c
    :try_start_2
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ec;->sr()Lcom/google/android/gms/internal/measurement/ec;

    move-result-object p2

    throw p2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p2

    .line 153
    invoke-virtual {v0, p1, v2}, Lcom/google/android/gms/internal/measurement/gi;->j(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_4

    :goto_3
    throw p2

    :goto_4
    goto :goto_3
.end method

.method public final a(Ljava/lang/Object;Lcom/google/android/gms/internal/measurement/hf;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/google/android/gms/internal/measurement/hf;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/fh;->awr:Lcom/google/android/gms/internal/measurement/dg;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/measurement/dg;->al(Ljava/lang/Object;)Lcom/google/android/gms/internal/measurement/dh;

    move-result-object v0

    .line 28
    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/dh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 29
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 30
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 31
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/internal/measurement/dj;

    .line 32
    invoke-interface {v2}, Lcom/google/android/gms/internal/measurement/dj;->rR()Lcom/google/android/gms/internal/measurement/hc;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/internal/measurement/hc;->axY:Lcom/google/android/gms/internal/measurement/hc;

    if-ne v3, v4, :cond_1

    invoke-interface {v2}, Lcom/google/android/gms/internal/measurement/dj;->rS()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-interface {v2}, Lcom/google/android/gms/internal/measurement/dj;->rT()Z

    move-result v3

    if-nez v3, :cond_1

    .line 34
    instance-of v3, v1, Lcom/google/android/gms/internal/measurement/ef;

    if-eqz v3, :cond_0

    .line 36
    invoke-interface {v2}, Lcom/google/android/gms/internal/measurement/dj;->oP()I

    move-result v2

    check-cast v1, Lcom/google/android/gms/internal/measurement/ef;

    .line 1009
    iget-object v1, v1, Lcom/google/android/gms/internal/measurement/ef;->avE:Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/measurement/ed;

    .line 36
    invoke-virtual {v1}, Lcom/google/android/gms/internal/measurement/eg;->qH()Lcom/google/android/gms/internal/measurement/cj;

    move-result-object v1

    .line 37
    invoke-interface {p2, v2, v1}, Lcom/google/android/gms/internal/measurement/hf;->c(ILjava/lang/Object;)V

    goto :goto_0

    .line 38
    :cond_0
    invoke-interface {v2}, Lcom/google/android/gms/internal/measurement/dj;->oP()I

    move-result v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p2, v2, v1}, Lcom/google/android/gms/internal/measurement/hf;->c(ILjava/lang/Object;)V

    goto :goto_0

    .line 33
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Found invalid MessageSet item."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 40
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/fh;->awq:Lcom/google/android/gms/internal/measurement/gi;

    .line 41
    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/measurement/gi;->aA(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/internal/measurement/gi;->c(Ljava/lang/Object;Lcom/google/android/gms/internal/measurement/hf;)V

    return-void
.end method

.method public final a(Ljava/lang/Object;[BIILcom/google/android/gms/internal/measurement/ce;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;[BII",
            "Lcom/google/android/gms/internal/measurement/ce;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 43
    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/internal/measurement/dr;

    iget-object v1, v0, Lcom/google/android/gms/internal/measurement/dr;->zzahz:Lcom/google/android/gms/internal/measurement/gl;

    .line 44
    invoke-static {}, Lcom/google/android/gms/internal/measurement/gl;->ti()Lcom/google/android/gms/internal/measurement/gl;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 45
    invoke-static {}, Lcom/google/android/gms/internal/measurement/gl;->tj()Lcom/google/android/gms/internal/measurement/gl;

    move-result-object v1

    .line 46
    iput-object v1, v0, Lcom/google/android/gms/internal/measurement/dr;->zzahz:Lcom/google/android/gms/internal/measurement/gl;

    .line 47
    :cond_0
    check-cast p1, Lcom/google/android/gms/internal/measurement/dr$b;

    .line 48
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/dr$b;->sj()Lcom/google/android/gms/internal/measurement/dh;

    const/4 p1, 0x0

    move-object v0, p1

    :goto_0
    if-ge p3, p4, :cond_a

    .line 51
    invoke-static {p2, p3, p5}, Lcom/google/android/gms/internal/measurement/cf;->a([BILcom/google/android/gms/internal/measurement/ce;)I

    move-result v4

    .line 52
    iget v2, p5, Lcom/google/android/gms/internal/measurement/ce;->asK:I

    const/16 p3, 0xb

    const/4 v3, 0x2

    if-eq v2, p3, :cond_3

    and-int/lit8 p3, v2, 0x7

    if-ne p3, v3, :cond_2

    .line 57
    iget-object p3, p0, Lcom/google/android/gms/internal/measurement/fh;->awr:Lcom/google/android/gms/internal/measurement/dg;

    iget-object v0, p5, Lcom/google/android/gms/internal/measurement/ce;->asN:Lcom/google/android/gms/internal/measurement/de;

    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/fh;->awg:Lcom/google/android/gms/internal/measurement/fb;

    ushr-int/lit8 v5, v2, 0x3

    .line 59
    invoke-virtual {p3, v0, v3, v5}, Lcom/google/android/gms/internal/measurement/dg;->a(Lcom/google/android/gms/internal/measurement/de;Lcom/google/android/gms/internal/measurement/fb;I)Ljava/lang/Object;

    move-result-object p3

    move-object v0, p3

    check-cast v0, Lcom/google/android/gms/internal/measurement/dr$e;

    if-nez v0, :cond_1

    move-object v3, p2

    move v5, p4

    move-object v6, v1

    move-object v7, p5

    .line 64
    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/internal/measurement/cf;->a(I[BIILcom/google/android/gms/internal/measurement/gl;Lcom/google/android/gms/internal/measurement/ce;)I

    move-result p3

    goto :goto_0

    .line 61
    :cond_1
    invoke-static {}, Lcom/google/android/gms/internal/measurement/fm;->sS()Lcom/google/android/gms/internal/measurement/fm;

    .line 62
    new-instance p1, Ljava/lang/NoSuchMethodError;

    invoke-direct {p1}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw p1

    .line 65
    :cond_2
    invoke-static {v2, p2, v4, p4, p5}, Lcom/google/android/gms/internal/measurement/cf;->a(I[BIILcom/google/android/gms/internal/measurement/ce;)I

    move-result p3

    goto :goto_0

    :cond_3
    const/4 p3, 0x0

    move-object v2, p1

    :goto_1
    if-ge v4, p4, :cond_8

    .line 70
    invoke-static {p2, v4, p5}, Lcom/google/android/gms/internal/measurement/cf;->a([BILcom/google/android/gms/internal/measurement/ce;)I

    move-result v4

    .line 71
    iget v5, p5, Lcom/google/android/gms/internal/measurement/ce;->asK:I

    ushr-int/lit8 v6, v5, 0x3

    and-int/lit8 v7, v5, 0x7

    if-eq v6, v3, :cond_6

    const/4 v8, 0x3

    if-eq v6, v8, :cond_4

    goto :goto_2

    :cond_4
    if-nez v0, :cond_5

    if-ne v7, v3, :cond_7

    .line 89
    invoke-static {p2, v4, p5}, Lcom/google/android/gms/internal/measurement/cf;->e([BILcom/google/android/gms/internal/measurement/ce;)I

    move-result v4

    .line 90
    iget-object v2, p5, Lcom/google/android/gms/internal/measurement/ce;->asM:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/gms/internal/measurement/cj;

    goto :goto_1

    .line 86
    :cond_5
    invoke-static {}, Lcom/google/android/gms/internal/measurement/fm;->sS()Lcom/google/android/gms/internal/measurement/fm;

    .line 87
    new-instance p1, Ljava/lang/NoSuchMethodError;

    invoke-direct {p1}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw p1

    :cond_6
    if-nez v7, :cond_7

    .line 80
    invoke-static {p2, v4, p5}, Lcom/google/android/gms/internal/measurement/cf;->a([BILcom/google/android/gms/internal/measurement/ce;)I

    move-result v4

    .line 81
    iget p3, p5, Lcom/google/android/gms/internal/measurement/ce;->asK:I

    .line 82
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/fh;->awr:Lcom/google/android/gms/internal/measurement/dg;

    iget-object v5, p5, Lcom/google/android/gms/internal/measurement/ce;->asN:Lcom/google/android/gms/internal/measurement/de;

    iget-object v6, p0, Lcom/google/android/gms/internal/measurement/fh;->awg:Lcom/google/android/gms/internal/measurement/fb;

    .line 83
    invoke-virtual {v0, v5, v6, p3}, Lcom/google/android/gms/internal/measurement/dg;->a(Lcom/google/android/gms/internal/measurement/de;Lcom/google/android/gms/internal/measurement/fb;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/dr$e;

    goto :goto_1

    :cond_7
    :goto_2
    const/16 v6, 0xc

    if-eq v5, v6, :cond_8

    .line 93
    invoke-static {v5, p2, v4, p4, p5}, Lcom/google/android/gms/internal/measurement/cf;->a(I[BIILcom/google/android/gms/internal/measurement/ce;)I

    move-result v4

    goto :goto_1

    :cond_8
    if-eqz v2, :cond_9

    shl-int/lit8 p3, p3, 0x3

    or-int/2addr p3, v3

    .line 99
    invoke-virtual {v1, p3, v2}, Lcom/google/android/gms/internal/measurement/gl;->d(ILjava/lang/Object;)V

    :cond_9
    move p3, v4

    goto/16 :goto_0

    :cond_a
    if-ne p3, p4, :cond_b

    return-void

    .line 102
    :cond_b
    invoke-static {}, Lcom/google/android/gms/internal/measurement/ec;->su()Lcom/google/android/gms/internal/measurement/ec;

    move-result-object p1

    goto :goto_4

    :goto_3
    throw p1

    :goto_4
    goto :goto_3
.end method

.method public final an(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/fh;->awq:Lcom/google/android/gms/internal/measurement/gi;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/measurement/gi;->an(Ljava/lang/Object;)V

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/fh;->awr:Lcom/google/android/gms/internal/measurement/dg;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/measurement/dg;->an(Ljava/lang/Object;)V

    return-void
.end method

.method public final aw(Ljava/lang/Object;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation

    .line 159
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/fh;->awq:Lcom/google/android/gms/internal/measurement/gi;

    .line 160
    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/measurement/gi;->aA(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 161
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/measurement/gi;->aC(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, 0x0

    add-int/2addr v0, v1

    .line 163
    iget-boolean v2, p0, Lcom/google/android/gms/internal/measurement/fh;->awh:Z

    if-eqz v2, :cond_2

    .line 164
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/fh;->awr:Lcom/google/android/gms/internal/measurement/dg;

    invoke-virtual {v2, p1}, Lcom/google/android/gms/internal/measurement/dg;->al(Ljava/lang/Object;)Lcom/google/android/gms/internal/measurement/dh;

    move-result-object p1

    const/4 v2, 0x0

    .line 1201
    :goto_0
    iget-object v3, p1, Lcom/google/android/gms/internal/measurement/dh;->atD:Lcom/google/android/gms/internal/measurement/fw;

    invoke-virtual {v3}, Lcom/google/android/gms/internal/measurement/fw;->sZ()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 1202
    iget-object v3, p1, Lcom/google/android/gms/internal/measurement/dh;->atD:Lcom/google/android/gms/internal/measurement/fw;

    invoke-virtual {v3, v1}, Lcom/google/android/gms/internal/measurement/fw;->cE(I)Ljava/util/Map$Entry;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/internal/measurement/dh;->e(Ljava/util/Map$Entry;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1204
    :cond_0
    iget-object p1, p1, Lcom/google/android/gms/internal/measurement/dh;->atD:Lcom/google/android/gms/internal/measurement/fw;

    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/fw;->ta()Ljava/lang/Iterable;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1205
    invoke-static {v1}, Lcom/google/android/gms/internal/measurement/dh;->e(Ljava/util/Map$Entry;)I

    move-result v1

    add-int/2addr v2, v1

    goto :goto_1

    :cond_1
    add-int/2addr v0, v2

    :cond_2
    return v0
.end method

.method public final ay(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/fh;->awr:Lcom/google/android/gms/internal/measurement/dg;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/measurement/dg;->al(Ljava/lang/Object;)Lcom/google/android/gms/internal/measurement/dh;

    move-result-object p1

    .line 158
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/dh;->isInitialized()Z

    move-result p1

    return p1
.end method

.method public final equals(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)Z"
        }
    .end annotation

    .line 9
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/fh;->awq:Lcom/google/android/gms/internal/measurement/gi;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/measurement/gi;->aA(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 10
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/fh;->awq:Lcom/google/android/gms/internal/measurement/gi;

    invoke-virtual {v1, p2}, Lcom/google/android/gms/internal/measurement/gi;->aA(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 11
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 13
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/internal/measurement/fh;->awh:Z

    if-eqz v0, :cond_1

    .line 14
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/fh;->awr:Lcom/google/android/gms/internal/measurement/dg;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/measurement/dg;->al(Ljava/lang/Object;)Lcom/google/android/gms/internal/measurement/dh;

    move-result-object p1

    .line 15
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/fh;->awr:Lcom/google/android/gms/internal/measurement/dg;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/internal/measurement/dg;->al(Ljava/lang/Object;)Lcom/google/android/gms/internal/measurement/dh;

    move-result-object p2

    .line 16
    invoke-virtual {p1, p2}, Lcom/google/android/gms/internal/measurement/dh;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method public final g(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)V"
        }
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/fh;->awq:Lcom/google/android/gms/internal/measurement/gi;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/internal/measurement/fs;->a(Lcom/google/android/gms/internal/measurement/gi;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 24
    iget-boolean v0, p0, Lcom/google/android/gms/internal/measurement/fh;->awh:Z

    if-eqz v0, :cond_0

    .line 25
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/fh;->awr:Lcom/google/android/gms/internal/measurement/dg;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/internal/measurement/fs;->a(Lcom/google/android/gms/internal/measurement/dg;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final hashCode(Ljava/lang/Object;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation

    .line 18
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/fh;->awq:Lcom/google/android/gms/internal/measurement/gi;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/measurement/gi;->aA(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 19
    iget-boolean v1, p0, Lcom/google/android/gms/internal/measurement/fh;->awh:Z

    if-eqz v1, :cond_0

    .line 20
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/fh;->awr:Lcom/google/android/gms/internal/measurement/dg;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/internal/measurement/dg;->al(Ljava/lang/Object;)Lcom/google/android/gms/internal/measurement/dh;

    move-result-object p1

    mul-int/lit8 v0, v0, 0x35

    .line 21
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/dh;->hashCode()I

    move-result p1

    add-int/2addr v0, p1

    :cond_0
    return v0
.end method

.method public final newInstance()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 8
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/fh;->awg:Lcom/google/android/gms/internal/measurement/fb;

    invoke-interface {v0}, Lcom/google/android/gms/internal/measurement/fb;->sc()Lcom/google/android/gms/internal/measurement/fa;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/internal/measurement/fa;->sh()Lcom/google/android/gms/internal/measurement/fb;

    move-result-object v0

    return-object v0
.end method
