.class public final Lcom/google/android/gms/common/util/h;
.super Ljava/lang/Object;


# static fields
.field private static akj:Ljava/lang/Boolean;

.field private static akk:Ljava/lang/Boolean;

.field public static akl:Ljava/lang/Boolean;


# direct methods
.method public static Q(Landroid/content/Context;)Z
    .locals 1

    .line 15
    sget-object v0, Lcom/google/android/gms/common/util/h;->akj:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    .line 16
    invoke-static {}, Lcom/google/android/gms/common/util/l;->mo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p0

    const-string v0, "android.hardware.type.watch"

    invoke-virtual {p0, v0}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    .line 18
    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    sput-object p0, Lcom/google/android/gms/common/util/h;->akj:Ljava/lang/Boolean;

    .line 19
    :cond_1
    sget-object p0, Lcom/google/android/gms/common/util/h;->akj:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method public static R(Landroid/content/Context;)Z
    .locals 1

    .line 20
    invoke-static {p0}, Lcom/google/android/gms/common/util/h;->Q(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 21
    invoke-static {}, Lcom/google/android/gms/common/util/l;->isAtLeastN()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 22
    invoke-static {p0}, Lcom/google/android/gms/common/util/h;->S(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_1

    invoke-static {}, Lcom/google/android/gms/common/util/l;->isAtLeastO()Z

    move-result p0

    if-nez p0, :cond_1

    :cond_0
    const/4 p0, 0x1

    return p0

    :cond_1
    const/4 p0, 0x0

    return p0
.end method

.method public static S(Landroid/content/Context;)Z
    .locals 1

    .line 24
    sget-object v0, Lcom/google/android/gms/common/util/h;->akk:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    .line 25
    invoke-static {}, Lcom/google/android/gms/common/util/l;->mp()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p0

    const-string v0, "cn.google"

    invoke-virtual {p0, v0}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    .line 27
    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    sput-object p0, Lcom/google/android/gms/common/util/h;->akk:Ljava/lang/Boolean;

    .line 28
    :cond_1
    sget-object p0, Lcom/google/android/gms/common/util/h;->akk:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method
