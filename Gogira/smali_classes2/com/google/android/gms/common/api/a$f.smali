.class public interface abstract Lcom/google/android/gms/common/api/a$f;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/api/a$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/api/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "f"
.end annotation


# virtual methods
.method public abstract a(Lcom/google/android/gms/common/internal/c$c;)V
.end method

.method public abstract a(Lcom/google/android/gms/common/internal/c$e;)V
.end method

.method public abstract a(Lcom/google/android/gms/common/internal/k;Ljava/util/Set;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/internal/k;",
            "Ljava/util/Set<",
            "Lcom/google/android/gms/common/api/Scope;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract disconnect()V
.end method

.method public abstract isConnected()Z
.end method

.method public abstract isConnecting()Z
.end method

.method public abstract kZ()Z
.end method

.method public abstract la()Z
.end method

.method public abstract lb()Ljava/lang/String;
.end method

.method public abstract lc()I
.end method

.method public abstract ld()[Lcom/google/android/gms/common/Feature;
.end method
