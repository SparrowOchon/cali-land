.class public final Lcom/google/android/gms/measurement/internal/fr;
.super Lcom/google/android/gms/measurement/internal/fb;


# instance fields
.field protected aJA:Lcom/google/android/gms/measurement/internal/gk;

.field private aJB:Lcom/google/android/gms/measurement/internal/fm;

.field private final aJC:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/google/android/gms/measurement/internal/fp;",
            ">;"
        }
    .end annotation
.end field

.field private aJD:Z

.field private final aJE:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected aJF:Z


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/measurement/internal/ek;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/google/android/gms/measurement/internal/fb;-><init>(Lcom/google/android/gms/measurement/internal/ek;)V

    .line 2
    new-instance p1, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/fr;->aJC:Ljava/util/Set;

    const/4 p1, 0x1

    .line 3
    iput-boolean p1, p0, Lcom/google/android/gms/measurement/internal/fr;->aJF:Z

    .line 4
    new-instance p1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {p1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/fr;->aJE:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/measurement/internal/fr;)V
    .locals 0

    .line 747
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/fr;->yD()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/measurement/internal/fr;Landroid/os/Bundle;)V
    .locals 25

    move-object/from16 v0, p1

    const-string v1, "app_id"

    .line 33573
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 33574
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fb;->vt()V

    .line 33575
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "name"

    .line 33576
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/r;->aQ(Ljava/lang/String;)Ljava/lang/String;

    const-string v3, "origin"

    .line 33577
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/common/internal/r;->aQ(Ljava/lang/String;)Ljava/lang/String;

    const-string v4, "value"

    .line 33578
    invoke-virtual {v0, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v5, p0

    .line 33579
    iget-object v6, v5, Lcom/google/android/gms/measurement/internal/fr;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v6}, Lcom/google/android/gms/measurement/internal/ek;->isEnabled()Z

    move-result v6

    if-nez v6, :cond_0

    .line 33580
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 34021
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "Conditional property not sent since collection is disabled"

    .line 33580
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    return-void

    .line 33582
    :cond_0
    new-instance v12, Lcom/google/android/gms/measurement/internal/zzjn;

    .line 33583
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v2, "triggered_timestamp"

    .line 33584
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 33585
    invoke-virtual {v0, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    .line 33586
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    move-object v6, v12

    invoke-direct/range {v6 .. v11}, Lcom/google/android/gms/measurement/internal/zzjn;-><init>(Ljava/lang/String;JLjava/lang/Object;Ljava/lang/String;)V

    .line 33588
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v13

    .line 33589
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string v2, "triggered_event_name"

    .line 33590
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const-string v2, "triggered_event_params"

    .line 33591
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v16

    .line 33592
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    const-wide/16 v18, 0x0

    .line 33593
    invoke-virtual/range {v13 .. v19}, Lcom/google/android/gms/measurement/internal/it;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;J)Lcom/google/android/gms/measurement/internal/zzai;

    move-result-object v17

    .line 33595
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v18

    .line 33596
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    const-string v2, "timed_out_event_name"

    .line 33597
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    const-string v2, "timed_out_event_params"

    .line 33598
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v21

    .line 33599
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    const-wide/16 v23, 0x0

    .line 33600
    invoke-virtual/range {v18 .. v24}, Lcom/google/android/gms/measurement/internal/it;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;J)Lcom/google/android/gms/measurement/internal/zzai;

    move-result-object v14

    .line 33602
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v18

    .line 33603
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    const-string v2, "expired_event_name"

    .line 33604
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    const-string v2, "expired_event_params"

    .line 33605
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v21

    .line 33606
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    const-wide/16 v23, 0x0

    .line 33607
    invoke-virtual/range {v18 .. v24}, Lcom/google/android/gms/measurement/internal/it;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;J)Lcom/google/android/gms/measurement/internal/zzai;

    move-result-object v20
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 33611
    new-instance v2, Lcom/google/android/gms/measurement/internal/zzq;

    .line 33612
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 33613
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v1, "creation_timestamp"

    .line 33614
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    const/4 v1, 0x0

    const-string v3, "trigger_event_name"

    .line 33615
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v3, "trigger_timeout"

    .line 33616
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v15

    const-string v3, "time_to_live"

    .line 33617
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v18

    move-object v6, v2

    move-object v9, v12

    move v12, v1

    invoke-direct/range {v6 .. v20}, Lcom/google/android/gms/measurement/internal/zzq;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/measurement/internal/zzjn;JZLjava/lang/String;Lcom/google/android/gms/measurement/internal/zzai;JLcom/google/android/gms/measurement/internal/zzai;JLcom/google/android/gms/measurement/internal/zzai;)V

    .line 33618
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/cb;->vz()Lcom/google/android/gms/measurement/internal/gw;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/measurement/internal/gw;->b(Lcom/google/android/gms/measurement/internal/zzq;)V

    :catch_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/measurement/internal/fr;Ljava/lang/String;Ljava/lang/String;JLandroid/os/Bundle;ZZZLjava/lang/String;)V
    .locals 0

    .line 748
    invoke-direct/range {p0 .. p9}, Lcom/google/android/gms/measurement/internal/fr;->a(Ljava/lang/String;Ljava/lang/String;JLandroid/os/Bundle;ZZZLjava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/measurement/internal/fr;Z)V
    .locals 3

    .line 32042
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 32044
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fb;->vt()V

    .line 32045
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 33021
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    .line 32045
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "Setting app measurement enabled (FE)"

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    .line 32046
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/measurement/internal/dp;->O(Z)V

    .line 32047
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/fr;->yD()V

    return-void
.end method

.method private final a(Ljava/lang/String;Ljava/lang/String;JLandroid/os/Bundle;ZZZ)V
    .locals 13

    .line 303
    invoke-static/range {p5 .. p5}, Lcom/google/android/gms/measurement/internal/it;->h(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v6

    .line 304
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v11

    new-instance v12, Lcom/google/android/gms/measurement/internal/ft;

    const/4 v10, 0x0

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide/from16 v4, p3

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/measurement/internal/ft;-><init>(Lcom/google/android/gms/measurement/internal/fr;Ljava/lang/String;Ljava/lang/String;JLandroid/os/Bundle;ZZZLjava/lang/String;)V

    .line 305
    invoke-virtual {v11, v12}, Lcom/google/android/gms/measurement/internal/ed;->f(Ljava/lang/Runnable;)V

    return-void
.end method

.method private final a(Ljava/lang/String;Ljava/lang/String;JLandroid/os/Bundle;ZZZLjava/lang/String;)V
    .locals 27

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v15, p2

    move-wide/from16 v13, p3

    move-object/from16 v12, p5

    move-object/from16 v11, p9

    .line 93
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/common/internal/r;->aQ(Ljava/lang/String;)Ljava/lang/String;

    .line 94
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/measurement/internal/j;->aFp:Lcom/google/android/gms/measurement/internal/cv;

    .line 3083
    invoke-virtual {v0, v11, v1}, Lcom/google/android/gms/measurement/internal/jb;->d(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 95
    invoke-static/range {p2 .. p2}, Lcom/google/android/gms/common/internal/r;->aQ(Ljava/lang/String;)Ljava/lang/String;

    .line 96
    :cond_0
    invoke-static/range {p5 .. p5}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 98
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fb;->vt()V

    .line 99
    iget-object v0, v7, Lcom/google/android/gms/measurement/internal/fr;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 100
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 4021
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "Event not sent since app measurement is disabled"

    .line 100
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    return-void

    .line 102
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/cb;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/cz;->pT()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/measurement/internal/j;->aFx:Lcom/google/android/gms/measurement/internal/cv;

    .line 4083
    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/measurement/internal/jb;->d(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 103
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/cb;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v0

    .line 4215
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/cz;->aFY:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 105
    invoke-interface {v0, v15}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 106
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 5021
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "Dropping non-safelisted event. event name, origin"

    .line 108
    invoke-virtual {v0, v1, v15, v8}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void

    .line 110
    :cond_2
    iget-boolean v0, v7, Lcom/google/android/gms/measurement/internal/fr;->aJD:Z

    const/4 v10, 0x0

    const/16 v16, 0x0

    const/4 v9, 0x1

    if-nez v0, :cond_4

    .line 111
    iput-boolean v9, v7, Lcom/google/android/gms/measurement/internal/fr;->aJD:Z

    .line 113
    :try_start_0
    iget-object v0, v7, Lcom/google/android/gms/measurement/internal/fr;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 5260
    iget-boolean v0, v0, Lcom/google/android/gms/measurement/internal/ek;->aAX:Z
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    const-string v1, "com.google.android.gms.tagmanager.TagManagerService"

    if-nez v0, :cond_3

    .line 115
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-static {v1, v9, v0}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 117
    :cond_3
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_0
    :try_start_2
    const-string v1, "initialize"

    new-array v2, v9, [Ljava/lang/Class;

    .line 119
    const-class v3, Landroid/content/Context;

    aput-object v3, v2, v16

    .line 120
    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    new-array v1, v9, [Ljava/lang/Object;

    .line 121
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->getContext()Landroid/content/Context;

    move-result-object v2

    aput-object v2, v1, v16

    invoke-virtual {v0, v10, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_0
    move-exception v0

    .line 124
    :try_start_3
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 6017
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    const-string v2, "Failed to invoke Tag Manager\'s initialize() method"

    .line 124
    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 127
    :catch_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 6020
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGw:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "Tag Manager is not found and thus will not be used"

    .line 127
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 128
    :cond_4
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/cb;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/cz;->pT()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/measurement/internal/j;->aFE:Lcom/google/android/gms/measurement/internal/cv;

    .line 6083
    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/measurement/internal/jb;->d(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "_cmp"

    .line 129
    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "gclid"

    .line 130
    invoke-virtual {v12, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 132
    invoke-virtual {v12, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 133
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/e;->currentTimeMillis()J

    move-result-wide v5

    const-string v2, "auto"

    const-string v3, "_lgclid"

    move-object/from16 v1, p0

    .line 134
    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/measurement/internal/fr;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;J)V

    :cond_5
    const/16 v0, 0x28

    const/4 v1, 0x2

    if-eqz p8, :cond_a

    const-string v2, "_iap"

    .line 137
    invoke-virtual {v2, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 138
    iget-object v2, v7, Lcom/google/android/gms/measurement/internal/fr;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v2

    const-string v3, "event"

    .line 139
    invoke-virtual {v2, v3, v15}, Lcom/google/android/gms/measurement/internal/it;->H(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_6

    :goto_2
    const/4 v2, 0x2

    goto :goto_3

    .line 141
    :cond_6
    sget-object v4, Lcom/google/android/gms/measurement/internal/fl;->aJt:[Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v15}, Lcom/google/android/gms/measurement/internal/it;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_7

    const/16 v2, 0xd

    goto :goto_3

    .line 143
    :cond_7
    invoke-virtual {v2, v3, v0, v15}, Lcom/google/android/gms/measurement/internal/it;->b(Ljava/lang/String;ILjava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_8

    goto :goto_2

    :cond_8
    const/4 v2, 0x0

    :goto_3
    if-eqz v2, :cond_a

    .line 148
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 7016
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGs:Lcom/google/android/gms/measurement/internal/dj;

    .line 150
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object v3

    invoke-virtual {v3, v15}, Lcom/google/android/gms/measurement/internal/df;->cf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "Invalid public event name. Event will not be logged (FE)"

    .line 151
    invoke-virtual {v1, v4, v3}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    .line 152
    iget-object v1, v7, Lcom/google/android/gms/measurement/internal/fr;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 153
    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    .line 154
    invoke-static {v15, v0, v9}, Lcom/google/android/gms/measurement/internal/it;->b(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v0

    if-eqz v15, :cond_9

    .line 155
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v16

    move/from16 v1, v16

    goto :goto_4

    :cond_9
    const/4 v1, 0x0

    .line 156
    :goto_4
    iget-object v3, v7, Lcom/google/android/gms/measurement/internal/fr;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 157
    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v3

    const-string v4, "_ev"

    .line 158
    invoke-virtual {v3, v2, v4, v0, v1}, Lcom/google/android/gms/measurement/internal/it;->a(ILjava/lang/String;Ljava/lang/String;I)V

    return-void

    .line 162
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/cb;->vA()Lcom/google/android/gms/measurement/internal/gr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/gr;->yH()Lcom/google/android/gms/measurement/internal/gs;

    move-result-object v2

    const-string v3, "_sc"

    if-eqz v2, :cond_b

    .line 163
    invoke-virtual {v12, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 164
    iput-boolean v9, v2, Lcom/google/android/gms/measurement/internal/gs;->aKg:Z

    :cond_b
    if-eqz p6, :cond_c

    if-eqz p8, :cond_c

    const/4 v4, 0x1

    goto :goto_5

    :cond_c
    const/4 v4, 0x0

    .line 166
    :goto_5
    invoke-static {v2, v12, v4}, Lcom/google/android/gms/measurement/internal/gr;->a(Lcom/google/android/gms/measurement/internal/gs;Landroid/os/Bundle;Z)V

    const-string v4, "am"

    .line 167
    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    .line 168
    invoke-static/range {p2 .. p2}, Lcom/google/android/gms/measurement/internal/it;->cS(Ljava/lang/String;)Z

    move-result v4

    if-eqz p6, :cond_d

    .line 169
    iget-object v5, v7, Lcom/google/android/gms/measurement/internal/fr;->aJB:Lcom/google/android/gms/measurement/internal/fm;

    if-eqz v5, :cond_d

    if-nez v4, :cond_d

    if-nez v17, :cond_d

    .line 170
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 7021
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    .line 172
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object v1

    invoke-virtual {v1, v15}, Lcom/google/android/gms/measurement/internal/df;->cf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 173
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object v2

    invoke-virtual {v2, v12}, Lcom/google/android/gms/measurement/internal/df;->e(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Passing event to registered event handler (FE)"

    .line 174
    invoke-virtual {v0, v3, v1, v2}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 175
    iget-object v1, v7, Lcom/google/android/gms/measurement/internal/fr;->aJB:Lcom/google/android/gms/measurement/internal/fm;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p5

    move-wide/from16 v5, p3

    invoke-interface/range {v1 .. v6}, Lcom/google/android/gms/measurement/internal/fm;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;J)V

    return-void

    .line 176
    :cond_d
    iget-object v4, v7, Lcom/google/android/gms/measurement/internal/fr;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v4}, Lcom/google/android/gms/measurement/internal/ek;->yA()Z

    move-result v4

    if-nez v4, :cond_e

    return-void

    .line 178
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v4

    invoke-virtual {v4, v15}, Lcom/google/android/gms/measurement/internal/it;->cN(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_10

    .line 180
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 8016
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGs:Lcom/google/android/gms/measurement/internal/dj;

    .line 182
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object v2

    invoke-virtual {v2, v15}, Lcom/google/android/gms/measurement/internal/df;->cf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Invalid event name. Event will not be logged (FE)"

    .line 183
    invoke-virtual {v1, v3, v2}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    .line 185
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    invoke-static {v15, v0, v9}, Lcom/google/android/gms/measurement/internal/it;->b(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v0

    if-eqz v15, :cond_f

    .line 186
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v1

    move/from16 v16, v1

    .line 187
    :cond_f
    iget-object v1, v7, Lcom/google/android/gms/measurement/internal/fr;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 188
    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v1

    const-string v2, "_ev"

    move-object/from16 p1, v1

    move-object/from16 p2, p9

    move/from16 p3, v4

    move-object/from16 p4, v2

    move-object/from16 p5, v0

    move/from16 p6, v16

    .line 189
    invoke-virtual/range {p1 .. p6}, Lcom/google/android/gms/measurement/internal/it;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    return-void

    :cond_10
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v5, "_o"

    aput-object v5, v0, v16

    const-string v4, "_sn"

    aput-object v4, v0, v9

    aput-object v3, v0, v1

    const/4 v1, 0x3

    const-string v6, "_si"

    aput-object v6, v0, v1

    .line 9008
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 194
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v1

    const/16 v18, 0x1

    move-object v9, v1

    move-object v1, v10

    move-object/from16 v10, p9

    move-object/from16 v11, p2

    move-object/from16 v12, p5

    move-wide v7, v13

    move-object v13, v0

    move/from16 v14, p8

    move-object/from16 p6, v5

    move-object v5, v15

    move/from16 v15, v18

    .line 195
    invoke-virtual/range {v9 .. v15}, Lcom/google/android/gms/measurement/internal/it;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/List;ZZ)Landroid/os/Bundle;

    move-result-object v15

    if-eqz v15, :cond_12

    .line 198
    invoke-virtual {v15, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_12

    .line 199
    invoke-virtual {v15, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_11

    goto :goto_6

    .line 201
    :cond_11
    invoke-virtual {v15, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 202
    invoke-virtual {v15, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 203
    invoke-virtual {v15, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 204
    new-instance v10, Lcom/google/android/gms/measurement/internal/gs;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    invoke-direct {v10, v1, v3, v11, v12}, Lcom/google/android/gms/measurement/internal/gs;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_7

    :cond_12
    :goto_6
    move-object v10, v1

    :goto_7
    if-nez v10, :cond_13

    move-object v14, v2

    goto :goto_8

    :cond_13
    move-object v14, v10

    .line 208
    :goto_8
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v1

    move-object/from16 v13, p9

    invoke-virtual {v1, v13}, Lcom/google/android/gms/measurement/internal/jb;->de(Ljava/lang/String;)Z

    move-result v1

    const-string v12, "_ae"

    const-wide/16 v9, 0x0

    if-eqz v1, :cond_14

    .line 211
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/cb;->vA()Lcom/google/android/gms/measurement/internal/gr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/gr;->yH()Lcom/google/android/gms/measurement/internal/gs;

    move-result-object v1

    if-eqz v1, :cond_14

    .line 212
    invoke-virtual {v12, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 213
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/cb;->vC()Lcom/google/android/gms/measurement/internal/hx;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/hx;->yN()J

    move-result-wide v1

    cmp-long v3, v1, v9

    if-lez v3, :cond_14

    .line 215
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v3

    invoke-virtual {v3, v15, v1, v2}, Lcom/google/android/gms/measurement/internal/it;->b(Landroid/os/Bundle;J)V

    .line 216
    :cond_14
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 217
    invoke-interface {v11, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/it;->ze()Ljava/security/SecureRandom;

    move-result-object v1

    invoke-virtual {v1}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v3

    .line 219
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/cb;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/cz;->pT()Ljava/lang/String;

    move-result-object v2

    sget-object v6, Lcom/google/android/gms/measurement/internal/j;->aFd:Lcom/google/android/gms/measurement/internal/cv;

    .line 9083
    invoke-virtual {v1, v2, v6}, Lcom/google/android/gms/measurement/internal/jb;->d(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 220
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dp;->aHi:Lcom/google/android/gms/measurement/internal/dt;

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/dt;->get()J

    move-result-wide v1

    cmp-long v6, v1, v9

    if-lez v6, :cond_16

    .line 221
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v1

    invoke-virtual {v1, v7, v8}, Lcom/google/android/gms/measurement/internal/dp;->al(J)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 222
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dp;->aHl:Lcom/google/android/gms/measurement/internal/dr;

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/dr;->get()Z

    move-result v1

    if-eqz v1, :cond_16

    .line 223
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 10022
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    const-string v2, "Current session is expired, remove the session number and Id"

    .line 225
    invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 226
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/cb;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/cz;->pT()Ljava/lang/String;

    move-result-object v2

    sget-object v6, Lcom/google/android/gms/measurement/internal/j;->aEZ:Lcom/google/android/gms/measurement/internal/cv;

    .line 10083
    invoke-virtual {v1, v2, v6}, Lcom/google/android/gms/measurement/internal/jb;->d(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v1

    if-eqz v1, :cond_15

    const/4 v6, 0x0

    .line 228
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/common/util/e;->currentTimeMillis()J

    move-result-wide v19

    const-string v2, "auto"

    const-string v18, "_sid"

    move-object/from16 v1, p0

    move-wide/from16 v21, v3

    move-object/from16 v3, v18

    move-object v4, v6

    move-object/from16 v23, p6

    move-wide/from16 v5, v19

    .line 229
    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/measurement/internal/fr;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;J)V

    goto :goto_9

    :cond_15
    move-object/from16 v23, p6

    move-wide/from16 v21, v3

    .line 230
    :goto_9
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/cb;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/cz;->pT()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/measurement/internal/j;->aFa:Lcom/google/android/gms/measurement/internal/cv;

    .line 11083
    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/measurement/internal/jb;->d(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v1

    if-eqz v1, :cond_17

    const/4 v4, 0x0

    .line 232
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/common/util/e;->currentTimeMillis()J

    move-result-wide v5

    const-string v2, "auto"

    const-string v3, "_sno"

    move-object/from16 v1, p0

    .line 233
    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/measurement/internal/fr;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;J)V

    goto :goto_a

    :cond_16
    move-object/from16 v23, p6

    move-wide/from16 v21, v3

    .line 234
    :cond_17
    :goto_a
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/cb;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/cz;->pT()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/jb;->dd(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_18

    const-string v1, "extend_session"

    .line 235
    invoke-virtual {v15, v1, v9, v10}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    const-wide/16 v3, 0x1

    cmp-long v5, v1, v3

    if-nez v5, :cond_18

    .line 237
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 12022
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    const-string v2, "EXTEND_SESSION param attached: initiate a new session or extend the current active session"

    .line 239
    invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    move-wide v5, v7

    move-object/from16 v7, p0

    .line 240
    iget-object v1, v7, Lcom/google/android/gms/measurement/internal/fr;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/ek;->vC()Lcom/google/android/gms/measurement/internal/hx;

    move-result-object v1

    const/4 v8, 0x1

    invoke-virtual {v1, v5, v6, v8}, Lcom/google/android/gms/measurement/internal/hx;->b(JZ)V

    goto :goto_b

    :cond_18
    move-wide v5, v7

    const/4 v8, 0x1

    move-object/from16 v7, p0

    .line 242
    :goto_b
    invoke-virtual {v15}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-virtual/range {p5 .. p5}, Landroid/os/Bundle;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    .line 243
    invoke-static {v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 244
    array-length v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_c
    const-string v10, "_eid"

    if-ge v3, v2, :cond_1b

    aget-object v9, v1, v3

    .line 245
    invoke-virtual {v15, v9}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v18

    .line 246
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    invoke-static/range {v18 .. v18}, Lcom/google/android/gms/measurement/internal/it;->aD(Ljava/lang/Object;)[Landroid/os/Bundle;

    move-result-object v8

    if-eqz v8, :cond_1a

    move-object/from16 p5, v1

    .line 248
    array-length v1, v8

    invoke-virtual {v15, v9, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move/from16 v18, v2

    const/4 v1, 0x0

    .line 249
    :goto_d
    array-length v2, v8

    if-ge v1, v2, :cond_19

    .line 250
    aget-object v2, v8, v1

    const/4 v5, 0x1

    .line 251
    invoke-static {v14, v2, v5}, Lcom/google/android/gms/measurement/internal/gr;->a(Lcom/google/android/gms/measurement/internal/gs;Landroid/os/Bundle;Z)V

    .line 253
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v5

    const/4 v6, 0x0

    const-string v19, "_ep"

    move-object/from16 v24, v9

    move-object v9, v5

    move-object v5, v10

    move-object/from16 v10, p9

    move-object/from16 v25, v11

    move-object/from16 v11, v19

    move-object/from16 v26, v12

    move-object v12, v2

    move-object v2, v13

    move-object v13, v0

    move-object/from16 v19, v14

    move/from16 v14, p8

    move-object/from16 p6, v0

    move-object v0, v15

    move v15, v6

    .line 254
    invoke-virtual/range {v9 .. v15}, Lcom/google/android/gms/measurement/internal/it;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/List;ZZ)Landroid/os/Bundle;

    move-result-object v6

    const-string v9, "_en"

    move-object/from16 v10, p2

    .line 255
    invoke-virtual {v6, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-wide/from16 v11, v21

    .line 256
    invoke-virtual {v6, v5, v11, v12}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v9, "_gn"

    move-object/from16 v13, v24

    .line 257
    invoke-virtual {v6, v9, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    array-length v9, v8

    const-string v14, "_ll"

    invoke-virtual {v6, v14, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v9, "_i"

    .line 259
    invoke-virtual {v6, v9, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move-object/from16 v9, v25

    .line 260
    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    move-object v15, v0

    move-object v10, v5

    move-object/from16 v14, v19

    move-object/from16 v12, v26

    move-wide/from16 v5, p3

    move-object/from16 v0, p6

    move-object v11, v9

    move-object v9, v13

    move-object v13, v2

    goto :goto_d

    :cond_19
    move-object/from16 v10, p2

    move-object/from16 p6, v0

    move-object v9, v11

    move-object/from16 v26, v12

    move-object v2, v13

    move-object/from16 v19, v14

    move-object v0, v15

    move-wide/from16 v11, v21

    .line 262
    array-length v1, v8

    add-int/2addr v4, v1

    goto :goto_e

    :cond_1a
    move-object/from16 v10, p2

    move-object/from16 p6, v0

    move-object/from16 p5, v1

    move/from16 v18, v2

    move-object v9, v11

    move-object/from16 v26, v12

    move-object v2, v13

    move-object/from16 v19, v14

    move-object v0, v15

    move-wide/from16 v11, v21

    :goto_e
    add-int/lit8 v3, v3, 0x1

    move-wide/from16 v5, p3

    move-object/from16 v1, p5

    move-object v15, v0

    move-object v13, v2

    move-wide/from16 v21, v11

    move/from16 v2, v18

    move-object/from16 v14, v19

    move-object/from16 v12, v26

    const/4 v8, 0x1

    move-object/from16 v0, p6

    move-object v11, v9

    goto/16 :goto_c

    :cond_1b
    move-object v5, v10

    move-object v9, v11

    move-object/from16 v26, v12

    move-object v2, v13

    move-object v0, v15

    move-wide/from16 v11, v21

    move-object/from16 v10, p2

    if-eqz v4, :cond_1c

    .line 265
    invoke-virtual {v0, v5, v11, v12}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v1, "_epc"

    .line 266
    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1c
    const/4 v0, 0x0

    .line 267
    :goto_f
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_21

    .line 268
    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    if-eqz v0, :cond_1d

    const/4 v3, 0x1

    goto :goto_10

    :cond_1d
    const/4 v3, 0x0

    :goto_10
    if-eqz v3, :cond_1e

    const-string v3, "_ep"

    move-object/from16 v8, p1

    goto :goto_11

    :cond_1e
    move-object/from16 v8, p1

    move-object v3, v10

    :goto_11
    move-object/from16 v11, v23

    .line 271
    invoke-virtual {v1, v11, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p7, :cond_1f

    .line 273
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/google/android/gms/measurement/internal/it;->g(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    :cond_1f
    move-object v12, v1

    .line 275
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 13021
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    .line 277
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object v4

    invoke-virtual {v4, v10}, Lcom/google/android/gms/measurement/internal/df;->cf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 278
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object v5

    invoke-virtual {v5, v12}, Lcom/google/android/gms/measurement/internal/df;->e(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "Logging event (FE)"

    .line 279
    invoke-virtual {v1, v6, v4, v5}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 280
    new-instance v13, Lcom/google/android/gms/measurement/internal/zzai;

    new-instance v4, Lcom/google/android/gms/measurement/internal/zzah;

    invoke-direct {v4, v12}, Lcom/google/android/gms/measurement/internal/zzah;-><init>(Landroid/os/Bundle;)V

    move-object v1, v13

    move-object v14, v2

    move-object v2, v3

    move-object v3, v4

    move-object/from16 v4, p1

    move-wide/from16 v5, p3

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/measurement/internal/zzai;-><init>(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/zzah;Ljava/lang/String;J)V

    .line 281
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/cb;->vz()Lcom/google/android/gms/measurement/internal/gw;

    move-result-object v1

    invoke-virtual {v1, v13, v14}, Lcom/google/android/gms/measurement/internal/gw;->c(Lcom/google/android/gms/measurement/internal/zzai;Ljava/lang/String;)V

    if-nez v17, :cond_20

    .line 283
    iget-object v1, v7, Lcom/google/android/gms/measurement/internal/fr;->aJC:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_12
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_20

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/measurement/internal/fp;

    .line 284
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4, v12}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-wide/from16 v5, p3

    .line 285
    invoke-interface/range {v1 .. v6}, Lcom/google/android/gms/measurement/internal/fp;->onEvent(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;J)V

    goto :goto_12

    :cond_20
    add-int/lit8 v0, v0, 0x1

    move-object/from16 v23, v11

    move-object v2, v14

    goto/16 :goto_f

    .line 290
    :cond_21
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/cb;->vA()Lcom/google/android/gms/measurement/internal/gr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/gr;->yH()Lcom/google/android/gms/measurement/internal/gs;

    move-result-object v0

    if-eqz v0, :cond_22

    move-object/from16 v0, v26

    .line 291
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 292
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/cb;->vC()Lcom/google/android/gms/measurement/internal/hx;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v1}, Lcom/google/android/gms/measurement/internal/hx;->c(ZZ)Z

    :cond_22
    return-void
.end method

.method private final a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/Object;)V
    .locals 9

    .line 347
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v0

    new-instance v8, Lcom/google/android/gms/measurement/internal/fs;

    move-object v1, v8

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p5

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/measurement/internal/fs;-><init>(Lcom/google/android/gms/measurement/internal/fr;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;J)V

    .line 348
    invoke-virtual {v0, v8}, Lcom/google/android/gms/measurement/internal/ed;->f(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/measurement/internal/fr;Landroid/os/Bundle;)V
    .locals 21

    move-object/from16 v0, p1

    const-string v1, "creation_timestamp"

    const-string v2, "origin"

    const-string v3, "app_id"

    .line 34620
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 34621
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fb;->vt()V

    .line 34622
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "name"

    .line 34623
    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/common/internal/r;->aQ(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v5, p0

    .line 34624
    iget-object v6, v5, Lcom/google/android/gms/measurement/internal/fr;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v6}, Lcom/google/android/gms/measurement/internal/ek;->isEnabled()Z

    move-result v6

    if-nez v6, :cond_0

    .line 34625
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 35021
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "Conditional property not cleared since collection is disabled"

    .line 34625
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    return-void

    .line 34627
    :cond_0
    new-instance v12, Lcom/google/android/gms/measurement/internal/zzjn;

    .line 34628
    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v6, v12

    invoke-direct/range {v6 .. v11}, Lcom/google/android/gms/measurement/internal/zzjn;-><init>(Ljava/lang/String;JLjava/lang/Object;Ljava/lang/String;)V

    .line 34630
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v13

    .line 34631
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string v4, "expired_event_name"

    .line 34632
    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const-string v4, "expired_event_params"

    .line 34633
    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v16

    .line 34634
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 34635
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v18

    .line 34636
    invoke-virtual/range {v13 .. v19}, Lcom/google/android/gms/measurement/internal/it;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;J)Lcom/google/android/gms/measurement/internal/zzai;

    move-result-object v20
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 34640
    new-instance v4, Lcom/google/android/gms/measurement/internal/zzq;

    .line 34641
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 34642
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 34643
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    const-string v1, "active"

    .line 34644
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "trigger_event_name"

    .line 34645
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    const-string v2, "trigger_timeout"

    .line 34646
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v15

    const/16 v17, 0x0

    const-string v2, "time_to_live"

    .line 34647
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v18

    move-object v6, v4

    move-object v9, v12

    move v12, v1

    invoke-direct/range {v6 .. v20}, Lcom/google/android/gms/measurement/internal/zzq;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/measurement/internal/zzjn;JZLjava/lang/String;Lcom/google/android/gms/measurement/internal/zzai;JLcom/google/android/gms/measurement/internal/zzai;JLcom/google/android/gms/measurement/internal/zzai;)V

    .line 34648
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/cb;->vz()Lcom/google/android/gms/measurement/internal/gw;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/gms/measurement/internal/gw;->b(Lcom/google/android/gms/measurement/internal/zzq;)V

    :catch_0
    return-void
.end method

.method private final yD()V
    .locals 8

    .line 49
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v0

    .line 50
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/cb;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/cz;->pT()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/measurement/internal/j;->aFk:Lcom/google/android/gms/measurement/internal/cv;

    .line 1083
    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/measurement/internal/jb;->d(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 52
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 53
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dp;->aHf:Lcom/google/android/gms/measurement/internal/dv;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/dv;->xZ()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v1, "unset"

    .line 55
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v5, 0x0

    .line 57
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/e;->currentTimeMillis()J

    move-result-wide v6

    const-string v3, "app"

    const-string v4, "_npa"

    move-object v2, p0

    .line 58
    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/measurement/internal/fr;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;J)V

    goto :goto_1

    :cond_0
    const-string v1, "true"

    .line 60
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-wide/16 v0, 0x1

    goto :goto_0

    :cond_1
    const-wide/16 v0, 0x0

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 61
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/e;->currentTimeMillis()J

    move-result-wide v6

    const-string v3, "app"

    const-string v4, "_npa"

    move-object v2, p0

    .line 62
    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/measurement/internal/fr;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;J)V

    .line 63
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fr;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/gms/measurement/internal/fr;->aJF:Z

    if-eqz v0, :cond_3

    .line 64
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 2021
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "Recording app launch after enabling measurement for the first time (FE)"

    .line 66
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 67
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fr;->yF()V

    return-void

    .line 69
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 3021
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "Updating Scion state (FE)"

    .line 69
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 70
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/cb;->vz()Lcom/google/android/gms/measurement/internal/gw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/gw;->yI()V

    return-void
.end method


# virtual methods
.method public final O(Z)V
    .locals 2

    .line 32
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fb;->vt()V

    .line 34
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/measurement/internal/gg;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/measurement/internal/gg;-><init>(Lcom/google/android/gms/measurement/internal/fr;Z)V

    .line 35
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/ed;->f(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Landroid/os/Bundle;J)V
    .locals 9

    .line 485
    invoke-static {p1}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 487
    const-class v0, Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "app_id"

    invoke-static {p1, v2, v0, v1}, Lcom/google/android/gms/measurement/internal/fi;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    .line 488
    const-class v0, Ljava/lang/String;

    const-string v2, "origin"

    invoke-static {p1, v2, v0, v1}, Lcom/google/android/gms/measurement/internal/fi;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    .line 489
    const-class v0, Ljava/lang/String;

    const-string v3, "name"

    invoke-static {p1, v3, v0, v1}, Lcom/google/android/gms/measurement/internal/fi;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    .line 490
    const-class v0, Ljava/lang/Object;

    const-string v4, "value"

    invoke-static {p1, v4, v0, v1}, Lcom/google/android/gms/measurement/internal/fi;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    .line 491
    const-class v0, Ljava/lang/String;

    const-string v5, "trigger_event_name"

    invoke-static {p1, v5, v0, v1}, Lcom/google/android/gms/measurement/internal/fi;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    .line 492
    const-class v0, Ljava/lang/Long;

    const-wide/16 v6, 0x0

    .line 493
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const-string v7, "trigger_timeout"

    .line 494
    invoke-static {p1, v7, v0, v6}, Lcom/google/android/gms/measurement/internal/fi;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    .line 495
    const-class v0, Ljava/lang/String;

    const-string v8, "timed_out_event_name"

    invoke-static {p1, v8, v0, v1}, Lcom/google/android/gms/measurement/internal/fi;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    .line 496
    const-class v0, Landroid/os/Bundle;

    const-string v8, "timed_out_event_params"

    invoke-static {p1, v8, v0, v1}, Lcom/google/android/gms/measurement/internal/fi;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    .line 497
    const-class v0, Ljava/lang/String;

    const-string v8, "triggered_event_name"

    invoke-static {p1, v8, v0, v1}, Lcom/google/android/gms/measurement/internal/fi;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    .line 498
    const-class v0, Landroid/os/Bundle;

    const-string v8, "triggered_event_params"

    invoke-static {p1, v8, v0, v1}, Lcom/google/android/gms/measurement/internal/fi;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    .line 499
    const-class v0, Ljava/lang/Long;

    const-string v8, "time_to_live"

    .line 501
    invoke-static {p1, v8, v0, v6}, Lcom/google/android/gms/measurement/internal/fi;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    .line 502
    const-class v0, Ljava/lang/String;

    const-string v6, "expired_event_name"

    invoke-static {p1, v6, v0, v1}, Lcom/google/android/gms/measurement/internal/fi;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    .line 503
    const-class v0, Landroid/os/Bundle;

    const-string v6, "expired_event_params"

    invoke-static {p1, v6, v0, v1}, Lcom/google/android/gms/measurement/internal/fi;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    .line 504
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/r;->aQ(Ljava/lang/String;)Ljava/lang/String;

    .line 505
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/r;->aQ(Ljava/lang/String;)Ljava/lang/String;

    .line 506
    invoke-virtual {p1, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "creation_timestamp"

    .line 507
    invoke-virtual {p1, v0, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 508
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 509
    invoke-virtual {p1, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p3

    .line 510
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/measurement/internal/it;->cO(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 512
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 19014
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    .line 514
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object p3

    invoke-virtual {p3, p2}, Lcom/google/android/gms/measurement/internal/df;->ch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    const-string p3, "Invalid conditional user property name"

    .line 515
    invoke-virtual {p1, p3, p2}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    return-void

    .line 517
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/gms/measurement/internal/it;->q(Ljava/lang/String;Ljava/lang/Object;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 519
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 20014
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    .line 521
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/measurement/internal/df;->ch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    const-string v0, "Invalid conditional user property value"

    .line 522
    invoke-virtual {p1, v0, p2, p3}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void

    .line 524
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    invoke-static {p2, p3}, Lcom/google/android/gms/measurement/internal/it;->r(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    .line 526
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 21014
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    .line 528
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/measurement/internal/df;->ch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    const-string v0, "Unable to normalize conditional user property value"

    .line 529
    invoke-virtual {p1, v0, p2, p3}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void

    .line 531
    :cond_2
    invoke-static {p1, v0}, Lcom/google/android/gms/measurement/internal/fi;->a(Landroid/os/Bundle;Ljava/lang/Object;)V

    .line 532
    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 533
    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 534
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    const-wide/16 v2, 0x1

    const-wide v4, 0x39ef8b000L

    if-nez p3, :cond_4

    cmp-long p3, v0, v4

    if-gtz p3, :cond_3

    cmp-long p3, v0, v2

    if-gez p3, :cond_4

    .line 536
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 22014
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    .line 538
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object p3

    invoke-virtual {p3, p2}, Lcom/google/android/gms/measurement/internal/df;->ch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 539
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    const-string v0, "Invalid conditional user property timeout"

    .line 540
    invoke-virtual {p1, v0, p2, p3}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void

    .line 542
    :cond_4
    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    cmp-long p3, v0, v4

    if-gtz p3, :cond_6

    cmp-long p3, v0, v2

    if-gez p3, :cond_5

    goto :goto_0

    .line 550
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object p2

    new-instance p3, Lcom/google/android/gms/measurement/internal/fx;

    invoke-direct {p3, p0, p1}, Lcom/google/android/gms/measurement/internal/fx;-><init>(Lcom/google/android/gms/measurement/internal/fr;Landroid/os/Bundle;)V

    .line 551
    invoke-virtual {p2, p3}, Lcom/google/android/gms/measurement/internal/ed;->f(Ljava/lang/Runnable;)V

    return-void

    .line 544
    :cond_6
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 23014
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    .line 546
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object p3

    invoke-virtual {p3, p2}, Lcom/google/android/gms/measurement/internal/df;->ch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 547
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    const-string v0, "Invalid conditional user property time to live"

    .line 548
    invoke-virtual {p1, v0, p2, p3}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/measurement/internal/fm;)V
    .locals 2

    .line 449
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 451
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fb;->vt()V

    if-eqz p1, :cond_1

    .line 452
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fr;->aJB:Lcom/google/android/gms/measurement/internal/fm;

    if-eq p1, v0, :cond_1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "EventInterceptor already set."

    .line 453
    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/r;->a(ZLjava/lang/Object;)V

    .line 454
    :cond_1
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/fr;->aJB:Lcom/google/android/gms/measurement/internal/fm;

    return-void
.end method

.method public final a(Lcom/google/android/gms/measurement/internal/fp;)V
    .locals 1

    .line 457
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fb;->vt()V

    .line 458
    invoke-static {p1}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 459
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fr;->aJC:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 460
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 16017
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    const-string v0, "OnEventListener already registered"

    .line 460
    invoke-virtual {p1, v0}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method final a(Ljava/lang/String;Ljava/lang/String;JLandroid/os/Bundle;)V
    .locals 11

    .line 89
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    move-object v10, p0

    .line 90
    iget-object v0, v10, Lcom/google/android/gms/measurement/internal/fr;->aJB:Lcom/google/android/gms/measurement/internal/fm;

    if-eqz v0, :cond_1

    invoke-static {p2}, Lcom/google/android/gms/measurement/internal/it;->cS(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v7, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    const/4 v7, 0x1

    :goto_1
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-object/from16 v5, p5

    .line 91
    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/measurement/internal/fr;->a(Ljava/lang/String;Ljava/lang/String;JLandroid/os/Bundle;ZZZLjava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 9

    .line 82
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/e;->currentTimeMillis()J

    move-result-wide v7

    const/4 v5, 0x1

    const/4 v6, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/gms/measurement/internal/fr;->logEvent(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;ZZJ)V

    return-void
.end method

.method final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;J)V
    .locals 8

    .line 350
    invoke-static {p1}, Lcom/google/android/gms/common/internal/r;->aQ(Ljava/lang/String;)Ljava/lang/String;

    .line 351
    invoke-static {p2}, Lcom/google/android/gms/common/internal/r;->aQ(Ljava/lang/String;)Ljava/lang/String;

    .line 352
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 354
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fb;->vt()V

    .line 355
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v0

    .line 356
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/cb;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/cz;->pT()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/measurement/internal/j;->aFk:Lcom/google/android/gms/measurement/internal/cv;

    .line 13083
    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/measurement/internal/jb;->d(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v0

    const-string v1, "_npa"

    if-eqz v0, :cond_3

    const-string v0, "allow_personalized_ads"

    .line 357
    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 358
    instance-of v0, p3, Ljava/lang/String;

    if-eqz v0, :cond_2

    move-object v0, p3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 359
    sget-object p2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, p2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p2

    const-string p3, "false"

    invoke-virtual {p3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    const-wide/16 v2, 0x1

    if-eqz p2, :cond_0

    move-wide v4, v2

    goto :goto_0

    :cond_0
    const-wide/16 v4, 0x0

    :goto_0
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    .line 361
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dp;->aHf:Lcom/google/android/gms/measurement/internal/dv;

    .line 362
    move-object v4, p2

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v6, v4, v2

    if-nez v6, :cond_1

    const-string p3, "true"

    :cond_1
    invoke-virtual {v0, p3}, Lcom/google/android/gms/measurement/internal/dv;->cq(Ljava/lang/String;)V

    move-object v6, p2

    goto :goto_1

    :cond_2
    if-nez p3, :cond_3

    .line 365
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object p2

    iget-object p2, p2, Lcom/google/android/gms/measurement/internal/dp;->aHf:Lcom/google/android/gms/measurement/internal/dv;

    const-string v0, "unset"

    invoke-virtual {p2, v0}, Lcom/google/android/gms/measurement/internal/dv;->cq(Ljava/lang/String;)V

    move-object v6, p3

    :goto_1
    move-object v3, v1

    goto :goto_2

    :cond_3
    move-object v3, p2

    move-object v6, p3

    .line 366
    :goto_2
    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/fr;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {p2}, Lcom/google/android/gms/measurement/internal/ek;->isEnabled()Z

    move-result p2

    if-nez p2, :cond_4

    .line 367
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 14021
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    const-string p2, "User property not set since app measurement is disabled"

    .line 367
    invoke-virtual {p1, p2}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    return-void

    .line 369
    :cond_4
    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/fr;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {p2}, Lcom/google/android/gms/measurement/internal/ek;->yA()Z

    move-result p2

    if-nez p2, :cond_5

    return-void

    .line 371
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p2

    .line 15021
    iget-object p2, p2, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    .line 373
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object p3

    invoke-virtual {p3, v3}, Lcom/google/android/gms/measurement/internal/df;->cf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    const-string v0, "Setting user property (FE)"

    invoke-virtual {p2, v0, p3, v6}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 374
    new-instance p2, Lcom/google/android/gms/measurement/internal/zzjn;

    move-object v2, p2

    move-wide v4, p4

    move-object v7, p1

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/measurement/internal/zzjn;-><init>(Ljava/lang/String;JLjava/lang/Object;Ljava/lang/String;)V

    .line 375
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/cb;->vz()Lcom/google/android/gms/measurement/internal/gw;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/google/android/gms/measurement/internal/gw;->a(Lcom/google/android/gms/measurement/internal/zzjn;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;ZJ)V
    .locals 6

    if-nez p1, :cond_0

    const-string p1, "app"

    :cond_0
    move-object v1, p1

    const/4 p1, 0x6

    const/4 v0, 0x0

    const/16 v2, 0x18

    if-eqz p4, :cond_1

    .line 312
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/google/android/gms/measurement/internal/it;->cO(Ljava/lang/String;)I

    move-result p1

    goto :goto_0

    .line 313
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object p4

    const-string v3, "user property"

    .line 314
    invoke-virtual {p4, v3, p2}, Lcom/google/android/gms/measurement/internal/it;->H(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    goto :goto_0

    .line 316
    :cond_2
    sget-object v4, Lcom/google/android/gms/measurement/internal/fn;->aJv:[Ljava/lang/String;

    invoke-virtual {p4, v3, v4, p2}, Lcom/google/android/gms/measurement/internal/it;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    const/16 p1, 0xf

    goto :goto_0

    .line 318
    :cond_3
    invoke-virtual {p4, v3, v2, p2}, Lcom/google/android/gms/measurement/internal/it;->b(Ljava/lang/String;ILjava/lang/String;)Z

    move-result p4

    if-nez p4, :cond_4

    goto :goto_0

    :cond_4
    const/4 p1, 0x0

    :goto_0
    const-string p4, "_ev"

    const/4 v3, 0x1

    if-eqz p1, :cond_6

    .line 324
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    invoke-static {p2, v2, v3}, Lcom/google/android/gms/measurement/internal/it;->b(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object p3

    if-eqz p2, :cond_5

    .line 325
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    .line 326
    :cond_5
    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/fr;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {p2}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object p2

    invoke-virtual {p2, p1, p4, p3, v0}, Lcom/google/android/gms/measurement/internal/it;->a(ILjava/lang/String;Ljava/lang/String;I)V

    return-void

    :cond_6
    if-eqz p3, :cond_b

    .line 329
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object p1

    invoke-virtual {p1, p2, p3}, Lcom/google/android/gms/measurement/internal/it;->q(Ljava/lang/String;Ljava/lang/Object;)I

    move-result p1

    if-eqz p1, :cond_9

    .line 332
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    invoke-static {p2, v2, v3}, Lcom/google/android/gms/measurement/internal/it;->b(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object p2

    .line 334
    instance-of p5, p3, Ljava/lang/String;

    if-nez p5, :cond_7

    instance-of p5, p3, Ljava/lang/CharSequence;

    if-eqz p5, :cond_8

    .line 335
    :cond_7
    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    .line 336
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    .line 337
    :cond_8
    iget-object p3, p0, Lcom/google/android/gms/measurement/internal/fr;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 338
    invoke-virtual {p3}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object p3

    .line 339
    invoke-virtual {p3, p1, p4, p2, v0}, Lcom/google/android/gms/measurement/internal/it;->a(ILjava/lang/String;Ljava/lang/String;I)V

    return-void

    .line 341
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    invoke-static {p2, p3}, Lcom/google/android/gms/measurement/internal/it;->r(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_a

    move-object v0, p0

    move-object v2, p2

    move-wide v3, p5

    .line 343
    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/measurement/internal/fr;->a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/Object;)V

    :cond_a
    return-void

    :cond_b
    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p2

    move-wide v3, p5

    .line 345
    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/measurement/internal/fr;->a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/Object;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 4

    .line 560
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/e;->currentTimeMillis()J

    move-result-wide v0

    .line 561
    invoke-static {p2}, Lcom/google/android/gms/common/internal/r;->aQ(Ljava/lang/String;)Ljava/lang/String;

    .line 562
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    if-eqz p1, :cond_0

    const-string v3, "app_id"

    .line 564
    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string p1, "name"

    .line 565
    invoke-virtual {v2, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "creation_timestamp"

    .line 566
    invoke-virtual {v2, p1, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    if-eqz p3, :cond_1

    const-string p1, "expired_event_name"

    .line 568
    invoke-virtual {v2, p1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "expired_event_params"

    .line 569
    invoke-virtual {v2, p1, p4}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 570
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object p1

    new-instance p2, Lcom/google/android/gms/measurement/internal/fw;

    invoke-direct {p2, p0, v2}, Lcom/google/android/gms/measurement/internal/fw;-><init>(Lcom/google/android/gms/measurement/internal/fr;Landroid/os/Bundle;)V

    .line 571
    invoke-virtual {p1, p2}, Lcom/google/android/gms/measurement/internal/ed;->f(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 685
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ed;->yu()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 686
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 27014
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string p2, "Cannot get user properties from analytics worker thread"

    .line 686
    invoke-virtual {p1, p2}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 687
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 688
    :cond_0
    invoke-static {}, Lcom/google/android/gms/measurement/internal/ja;->isMainThread()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 689
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 28014
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string p2, "Cannot get user properties from main thread"

    .line 689
    invoke-virtual {p1, p2}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 690
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 691
    :cond_1
    new-instance v7, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v7}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 692
    monitor-enter v7

    .line 693
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fr;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 694
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v8

    new-instance v9, Lcom/google/android/gms/measurement/internal/gc;

    move-object v0, v9

    move-object v1, p0

    move-object v2, v7

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/measurement/internal/gc;-><init>(Lcom/google/android/gms/measurement/internal/fr;Ljava/util/concurrent/atomic/AtomicReference;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 695
    invoke-virtual {v8, v9}, Lcom/google/android/gms/measurement/internal/ed;->f(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 p1, 0x1388

    .line 696
    :try_start_1
    invoke-virtual {v7, p1, p2}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 699
    :try_start_2
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p2

    .line 28017
    iget-object p2, p2, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    const-string p3, "Interrupted waiting for get user properties"

    .line 699
    invoke-virtual {p2, p3, p1}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    .line 700
    :goto_0
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 701
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    if-nez p1, :cond_2

    .line 703
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 29017
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    const-string p2, "Timed out waiting for get user properties"

    .line 703
    invoke-virtual {p1, p2}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 704
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 705
    :cond_2
    new-instance p2, Landroidx/collection/ArrayMap;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p3

    invoke-direct {p2, p3}, Landroidx/collection/ArrayMap;-><init>(I)V

    .line 706
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/google/android/gms/measurement/internal/zzjn;

    .line 707
    iget-object p4, p3, Lcom/google/android/gms/measurement/internal/zzjn;->name:Ljava/lang/String;

    invoke-virtual {p3}, Lcom/google/android/gms/measurement/internal/zzjn;->getValue()Ljava/lang/Object;

    move-result-object p3

    invoke-interface {p2, p4, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    return-object p2

    :catchall_0
    move-exception p1

    .line 700
    :try_start_3
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    :goto_2
    throw p1

    :goto_3
    goto :goto_2
.end method

.method public final b(Lcom/google/android/gms/measurement/internal/fp;)V
    .locals 1

    .line 463
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fb;->vt()V

    .line 464
    invoke-static {p1}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 465
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fr;->aJC:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 466
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 17017
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    const-string v0, "OnEventListener had not been registered"

    .line 466
    invoke-virtual {p1, v0}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 8

    .line 307
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/e;->currentTimeMillis()J

    move-result-wide v6

    const/4 v5, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/gms/measurement/internal/fr;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;ZJ)V

    return-void
.end method

.method final cI(Ljava/lang/String;)V
    .locals 1

    .line 415
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fr;->aJE:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public final clearConditionalUserProperty(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    const/4 v0, 0x0

    .line 554
    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/google/android/gms/measurement/internal/fr;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public final e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .line 655
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ed;->yu()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 656
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 24014
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string p2, "Cannot get conditional user properties from analytics worker thread"

    .line 658
    invoke-virtual {p1, p2}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 659
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1, v1}, Ljava/util/ArrayList;-><init>(I)V

    return-object p1

    .line 660
    :cond_0
    invoke-static {}, Lcom/google/android/gms/measurement/internal/ja;->isMainThread()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 661
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 25014
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string p2, "Cannot get conditional user properties from main thread"

    .line 661
    invoke-virtual {p1, p2}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 662
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1, v1}, Ljava/util/ArrayList;-><init>(I)V

    return-object p1

    .line 663
    :cond_1
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 664
    monitor-enter v0

    .line 665
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/fr;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 666
    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/ek;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v1

    new-instance v8, Lcom/google/android/gms/measurement/internal/fz;

    move-object v2, v8

    move-object v3, p0

    move-object v4, v0

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/measurement/internal/fz;-><init>(Lcom/google/android/gms/measurement/internal/fr;Ljava/util/concurrent/atomic/AtomicReference;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    invoke-virtual {v1, v8}, Lcom/google/android/gms/measurement/internal/ed;->f(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 p2, 0x1388

    .line 668
    :try_start_1
    invoke-virtual {v0, p2, p3}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p2

    .line 671
    :try_start_2
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p3

    .line 25017
    iget-object p3, p3, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "Interrupted waiting for get conditional user properties"

    .line 673
    invoke-virtual {p3, v1, p1, p2}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 674
    :goto_0
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 675
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/List;

    if-nez p2, :cond_2

    .line 677
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p2

    .line 26017
    iget-object p2, p2, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    const-string p3, "Timed out waiting for get conditional user properties"

    .line 677
    invoke-virtual {p2, p3, p1}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    .line 678
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    return-object p1

    .line 679
    :cond_2
    invoke-static {p2}, Lcom/google/android/gms/measurement/internal/it;->S(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1

    :catchall_0
    move-exception p1

    .line 674
    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw p1
.end method

.method public final bridge synthetic getContext()Landroid/content/Context;
    .locals 1

    .line 738
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public final getCurrentScreenClass()Ljava/lang/String;
    .locals 1

    .line 714
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fr;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vA()Lcom/google/android/gms/measurement/internal/gr;

    move-result-object v0

    .line 30050
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/gr;->aJY:Lcom/google/android/gms/measurement/internal/gs;

    if-eqz v0, :cond_0

    .line 716
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/gs;->aKe:Ljava/lang/String;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getCurrentScreenName()Ljava/lang/String;
    .locals 1

    .line 710
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fr;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vA()Lcom/google/android/gms/measurement/internal/gr;

    move-result-object v0

    .line 29050
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/gr;->aJY:Lcom/google/android/gms/measurement/internal/gs;

    if-eqz v0, :cond_0

    .line 712
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/gs;->aKd:Ljava/lang/String;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getGmpAppId()Ljava/lang/String;
    .locals 3

    .line 718
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fr;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 30257
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/ek;->aBa:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 719
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fr;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 31257
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/ek;->aBa:Ljava/lang/String;

    return-object v0

    .line 720
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/common/api/internal/e;->lD()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 723
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/fr;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/ek;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 32014
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string v2, "getGoogleAppId failed with exception"

    .line 723
    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic lX()V
    .locals 0

    .line 728
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->lX()V

    return-void
.end method

.method public final logEvent(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;ZZJ)V
    .locals 10

    if-nez p1, :cond_0

    const-string v0, "app"

    move-object v1, v0

    goto :goto_0

    :cond_0
    move-object v1, p1

    :goto_0
    if-nez p3, :cond_1

    .line 298
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    move-object v5, v0

    goto :goto_1

    :cond_1
    move-object v5, p3

    :goto_1
    const/4 v0, 0x1

    move-object v9, p0

    if-eqz p5, :cond_3

    .line 299
    iget-object v2, v9, Lcom/google/android/gms/measurement/internal/fr;->aJB:Lcom/google/android/gms/measurement/internal/fm;

    if-eqz v2, :cond_3

    .line 300
    invoke-static {p2}, Lcom/google/android/gms/measurement/internal/it;->cS(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    const/4 v7, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v7, 0x1

    :goto_3
    xor-int/lit8 v8, p4, 0x1

    move-object v0, p0

    move-object v2, p2

    move-wide/from16 v3, p6

    move v6, p5

    .line 301
    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/measurement/internal/fr;->a(Ljava/lang/String;Ljava/lang/String;JLandroid/os/Bundle;ZZZ)V

    return-void
.end method

.method public final setConditionalUserProperty(Landroid/os/Bundle;J)V
    .locals 3

    .line 470
    invoke-static {p1}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 472
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, p1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    const-string p1, "app_id"

    .line 473
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 474
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 18017
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    const-string v2, "Package name should be null when calling setConditionalUserProperty"

    .line 476
    invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 477
    :cond_0
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 478
    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/gms/measurement/internal/fr;->a(Landroid/os/Bundle;J)V

    return-void
.end method

.method public final bridge synthetic vA()Lcom/google/android/gms/measurement/internal/gr;
    .locals 1

    .line 733
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vA()Lcom/google/android/gms/measurement/internal/gr;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vB()Lcom/google/android/gms/measurement/internal/dd;
    .locals 1

    .line 734
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vB()Lcom/google/android/gms/measurement/internal/dd;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vC()Lcom/google/android/gms/measurement/internal/hx;
    .locals 1

    .line 735
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vC()Lcom/google/android/gms/measurement/internal/hx;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vD()Lcom/google/android/gms/measurement/internal/d;
    .locals 1

    .line 736
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vD()Lcom/google/android/gms/measurement/internal/d;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vE()Lcom/google/android/gms/common/util/e;
    .locals 1

    .line 737
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vF()Lcom/google/android/gms/measurement/internal/df;
    .locals 1

    .line 739
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vG()Lcom/google/android/gms/measurement/internal/it;
    .locals 1

    .line 740
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vH()Lcom/google/android/gms/measurement/internal/ed;
    .locals 1

    .line 741
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vI()Lcom/google/android/gms/measurement/internal/dh;
    .locals 1

    .line 742
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vJ()Lcom/google/android/gms/measurement/internal/dp;
    .locals 1

    .line 743
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vK()Lcom/google/android/gms/measurement/internal/jb;
    .locals 1

    .line 744
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v0

    return-object v0
.end method

.method protected final vM()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final bridge synthetic vu()V
    .locals 0

    .line 725
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vu()V

    return-void
.end method

.method public final bridge synthetic vv()V
    .locals 0

    .line 727
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vv()V

    return-void
.end method

.method public final bridge synthetic vw()Lcom/google/android/gms/measurement/internal/a;
    .locals 1

    .line 729
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vw()Lcom/google/android/gms/measurement/internal/a;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vx()Lcom/google/android/gms/measurement/internal/fr;
    .locals 1

    .line 730
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vy()Lcom/google/android/gms/measurement/internal/cz;
    .locals 1

    .line 731
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vz()Lcom/google/android/gms/measurement/internal/gw;
    .locals 1

    .line 732
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vz()Lcom/google/android/gms/measurement/internal/gw;

    move-result-object v0

    return-object v0
.end method

.method public final yC()V
    .locals 2

    .line 7
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Application;

    if-eqz v0, :cond_0

    .line 8
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    .line 9
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/fr;->aJA:Lcom/google/android/gms/measurement/internal/gk;

    .line 10
    invoke-virtual {v0, v1}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    :cond_0
    return-void
.end method

.method public final yE()Ljava/lang/String;
    .locals 1

    .line 402
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fr;->aJE:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final yF()V
    .locals 3

    .line 432
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 434
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fb;->vt()V

    .line 435
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fr;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->yA()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 437
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/cb;->vz()Lcom/google/android/gms/measurement/internal/gw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/gw;->yF()V

    const/4 v0, 0x0

    .line 438
    iput-boolean v0, p0, Lcom/google/android/gms/measurement/internal/fr;->aJF:Z

    .line 439
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/dp;->xV()Ljava/lang/String;

    move-result-object v0

    .line 440
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 441
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vD()Lcom/google/android/gms/measurement/internal/d;

    move-result-object v1

    .line 442
    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/fg;->vt()V

    .line 443
    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 444
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 445
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "_po"

    .line 446
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "auto"

    const-string v2, "_ou"

    .line 447
    invoke-virtual {p0, v0, v2, v1}, Lcom/google/android/gms/measurement/internal/fr;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_1
    return-void
.end method

.method public final zza(Z)V
    .locals 2

    .line 37
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fb;->vt()V

    .line 39
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/measurement/internal/gf;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/measurement/internal/gf;-><init>(Lcom/google/android/gms/measurement/internal/fr;Z)V

    .line 40
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/ed;->f(Ljava/lang/Runnable;)V

    return-void
.end method
