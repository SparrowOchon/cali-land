.class final Lcom/google/android/gms/measurement/internal/hb;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final synthetic aJk:Lcom/google/android/gms/measurement/internal/zzn;

.field private final synthetic aKm:Lcom/google/android/gms/measurement/internal/gw;

.field private final synthetic aKw:Z


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/gw;Lcom/google/android/gms/measurement/internal/zzn;Z)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/hb;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    iput-object p2, p0, Lcom/google/android/gms/measurement/internal/hb;->aJk:Lcom/google/android/gms/measurement/internal/zzn;

    iput-boolean p3, p0, Lcom/google/android/gms/measurement/internal/hb;->aKw:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .line 2
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/hb;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    .line 1320
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/gw;->aKo:Lcom/google/android/gms/measurement/internal/cy;

    if-nez v0, :cond_0

    .line 4
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/hb;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 2014
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "Discarding data. Failed to send app launch"

    .line 4
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    return-void

    .line 6
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/hb;->aJk:Lcom/google/android/gms/measurement/internal/zzn;

    invoke-interface {v0, v1}, Lcom/google/android/gms/measurement/internal/cy;->a(Lcom/google/android/gms/measurement/internal/zzn;)V

    .line 7
    iget-boolean v1, p0, Lcom/google/android/gms/measurement/internal/hb;->aKw:Z

    if-eqz v1, :cond_1

    .line 8
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/hb;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/cb;->vB()Lcom/google/android/gms/measurement/internal/dd;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/dd;->oo()Z

    .line 9
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/hb;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/measurement/internal/hb;->aJk:Lcom/google/android/gms/measurement/internal/zzn;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/measurement/internal/gw;->a(Lcom/google/android/gms/measurement/internal/cy;Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;Lcom/google/android/gms/measurement/internal/zzn;)V

    .line 10
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/hb;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    .line 2321
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/gw;->yJ()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    .line 13
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/hb;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 3014
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string v2, "Failed to send app launch to the service"

    .line 13
    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method
