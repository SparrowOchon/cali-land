.class final Lcom/google/android/gms/measurement/internal/if;
.super Ljava/lang/Object;


# instance fields
.field final aBe:Lcom/google/android/gms/common/util/e;

.field startTime:J


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/util/e;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p1}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/if;->aBe:Lcom/google/android/gms/common/util/e;

    return-void
.end method


# virtual methods
.method public final start()V
    .locals 2

    .line 5
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/if;->aBe:Lcom/google/android/gms/common/util/e;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/e;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/measurement/internal/if;->startTime:J

    return-void
.end method
