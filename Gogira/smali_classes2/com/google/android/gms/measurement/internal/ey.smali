.class final Lcom/google/android/gms/measurement/internal/ey;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final synthetic aJk:Lcom/google/android/gms/measurement/internal/zzn;

.field private final synthetic aJl:Lcom/google/android/gms/measurement/internal/el;

.field private final synthetic aJn:Lcom/google/android/gms/measurement/internal/zzai;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/el;Lcom/google/android/gms/measurement/internal/zzai;Lcom/google/android/gms/measurement/internal/zzn;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/ey;->aJl:Lcom/google/android/gms/measurement/internal/el;

    iput-object p2, p0, Lcom/google/android/gms/measurement/internal/ey;->aJn:Lcom/google/android/gms/measurement/internal/zzai;

    iput-object p3, p0, Lcom/google/android/gms/measurement/internal/ey;->aJk:Lcom/google/android/gms/measurement/internal/zzn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .line 2
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ey;->aJl:Lcom/google/android/gms/measurement/internal/el;

    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ey;->aJn:Lcom/google/android/gms/measurement/internal/zzai;

    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/ey;->aJk:Lcom/google/android/gms/measurement/internal/zzn;

    .line 1016
    iget-object v3, v1, Lcom/google/android/gms/measurement/internal/zzai;->name:Ljava/lang/String;

    const-string v4, "_cmp"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_2

    iget-object v3, v1, Lcom/google/android/gms/measurement/internal/zzai;->aDQ:Lcom/google/android/gms/measurement/internal/zzah;

    if-eqz v3, :cond_2

    iget-object v3, v1, Lcom/google/android/gms/measurement/internal/zzai;->aDQ:Lcom/google/android/gms/measurement/internal/zzah;

    .line 2015
    iget-object v3, v3, Lcom/google/android/gms/measurement/internal/zzah;->aDT:Landroid/os/Bundle;

    invoke-virtual {v3}, Landroid/os/Bundle;->size()I

    move-result v3

    if-nez v3, :cond_0

    goto :goto_0

    .line 1019
    :cond_0
    iget-object v3, v1, Lcom/google/android/gms/measurement/internal/zzai;->aDQ:Lcom/google/android/gms/measurement/internal/zzah;

    const-string v5, "_cis"

    invoke-virtual {v3, v5}, Lcom/google/android/gms/measurement/internal/zzah;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1020
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "referrer broadcast"

    .line 1021
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "referrer API"

    .line 1022
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1023
    :cond_1
    iget-object v3, v0, Lcom/google/android/gms/measurement/internal/el;->aGH:Lcom/google/android/gms/measurement/internal/ii;

    .line 2096
    iget-object v3, v3, Lcom/google/android/gms/measurement/internal/ii;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 2233
    iget-object v3, v3, Lcom/google/android/gms/measurement/internal/ek;->aIH:Lcom/google/android/gms/measurement/internal/jb;

    .line 1024
    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/zzn;->packageName:Ljava/lang/String;

    .line 1025
    invoke-virtual {v3, v2}, Lcom/google/android/gms/measurement/internal/jb;->da(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v4, 0x1

    :cond_2
    :goto_0
    if-eqz v4, :cond_3

    .line 1029
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/el;->aGH:Lcom/google/android/gms/measurement/internal/ii;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ii;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 3020
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGw:Lcom/google/android/gms/measurement/internal/dj;

    .line 1029
    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/zzai;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Event has been filtered "

    invoke-virtual {v0, v3, v2}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1030
    new-instance v0, Lcom/google/android/gms/measurement/internal/zzai;

    iget-object v6, v1, Lcom/google/android/gms/measurement/internal/zzai;->aDQ:Lcom/google/android/gms/measurement/internal/zzah;

    iget-object v7, v1, Lcom/google/android/gms/measurement/internal/zzai;->aAZ:Ljava/lang/String;

    iget-wide v8, v1, Lcom/google/android/gms/measurement/internal/zzai;->aDU:J

    const-string v5, "_cmpx"

    move-object v4, v0

    invoke-direct/range {v4 .. v9}, Lcom/google/android/gms/measurement/internal/zzai;-><init>(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/zzah;Ljava/lang/String;J)V

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 4
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ey;->aJl:Lcom/google/android/gms/measurement/internal/el;

    invoke-static {v1}, Lcom/google/android/gms/measurement/internal/el;->a(Lcom/google/android/gms/measurement/internal/el;)Lcom/google/android/gms/measurement/internal/ii;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/ii;->za()V

    .line 5
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ey;->aJl:Lcom/google/android/gms/measurement/internal/el;

    invoke-static {v1}, Lcom/google/android/gms/measurement/internal/el;->a(Lcom/google/android/gms/measurement/internal/el;)Lcom/google/android/gms/measurement/internal/ii;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/ey;->aJk:Lcom/google/android/gms/measurement/internal/zzn;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/measurement/internal/ii;->b(Lcom/google/android/gms/measurement/internal/zzai;Lcom/google/android/gms/measurement/internal/zzn;)V

    return-void
.end method
