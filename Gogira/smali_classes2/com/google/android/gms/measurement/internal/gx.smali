.class final Lcom/google/android/gms/measurement/internal/gx;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final synthetic aJk:Lcom/google/android/gms/measurement/internal/zzn;

.field private final synthetic aJo:Lcom/google/android/gms/measurement/internal/zzjn;

.field private final synthetic aKm:Lcom/google/android/gms/measurement/internal/gw;

.field private final synthetic aKu:Z


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/gw;ZLcom/google/android/gms/measurement/internal/zzjn;Lcom/google/android/gms/measurement/internal/zzn;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/gx;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    iput-boolean p2, p0, Lcom/google/android/gms/measurement/internal/gx;->aKu:Z

    iput-object p3, p0, Lcom/google/android/gms/measurement/internal/gx;->aJo:Lcom/google/android/gms/measurement/internal/zzjn;

    iput-object p4, p0, Lcom/google/android/gms/measurement/internal/gx;->aJk:Lcom/google/android/gms/measurement/internal/zzn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .line 2
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/gx;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    .line 1320
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/gw;->aKo:Lcom/google/android/gms/measurement/internal/cy;

    if-nez v0, :cond_0

    .line 4
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/gx;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 2014
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "Discarding data. Failed to set user attribute"

    .line 4
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    return-void

    .line 6
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/gx;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    iget-boolean v2, p0, Lcom/google/android/gms/measurement/internal/gx;->aKu:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/gx;->aJo:Lcom/google/android/gms/measurement/internal/zzjn;

    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/measurement/internal/gx;->aJk:Lcom/google/android/gms/measurement/internal/zzn;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/measurement/internal/gw;->a(Lcom/google/android/gms/measurement/internal/cy;Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;Lcom/google/android/gms/measurement/internal/zzn;)V

    .line 7
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/gx;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    .line 2321
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/gw;->yJ()V

    return-void
.end method
