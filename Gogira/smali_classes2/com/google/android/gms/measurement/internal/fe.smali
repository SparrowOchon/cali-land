.class final Lcom/google/android/gms/measurement/internal/fe;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final synthetic aJl:Lcom/google/android/gms/measurement/internal/el;

.field private final synthetic aJm:Ljava/lang/String;

.field private final synthetic aJp:Ljava/lang/String;

.field private final synthetic aJq:J

.field private final synthetic aqF:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/el;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/fe;->aJl:Lcom/google/android/gms/measurement/internal/el;

    iput-object p2, p0, Lcom/google/android/gms/measurement/internal/fe;->aJp:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/measurement/internal/fe;->aJm:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/measurement/internal/fe;->aqF:Ljava/lang/String;

    iput-wide p5, p0, Lcom/google/android/gms/measurement/internal/fe;->aJq:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .line 2
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fe;->aJp:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fe;->aJl:Lcom/google/android/gms/measurement/internal/el;

    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/el;->a(Lcom/google/android/gms/measurement/internal/el;)Lcom/google/android/gms/measurement/internal/ii;

    move-result-object v0

    .line 3464
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/ii;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 5
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vA()Lcom/google/android/gms/measurement/internal/gr;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/fe;->aJm:Ljava/lang/String;

    const/4 v2, 0x0

    .line 6
    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/measurement/internal/gr;->a(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/gs;)V

    return-void

    .line 8
    :cond_0
    new-instance v1, Lcom/google/android/gms/measurement/internal/gs;

    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/fe;->aqF:Ljava/lang/String;

    iget-wide v3, p0, Lcom/google/android/gms/measurement/internal/fe;->aJq:J

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/google/android/gms/measurement/internal/gs;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 9
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fe;->aJl:Lcom/google/android/gms/measurement/internal/el;

    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/el;->a(Lcom/google/android/gms/measurement/internal/el;)Lcom/google/android/gms/measurement/internal/ii;

    move-result-object v0

    .line 4464
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/ii;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 11
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vA()Lcom/google/android/gms/measurement/internal/gr;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/fe;->aJm:Ljava/lang/String;

    .line 12
    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/measurement/internal/gr;->a(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/gs;)V

    return-void
.end method
