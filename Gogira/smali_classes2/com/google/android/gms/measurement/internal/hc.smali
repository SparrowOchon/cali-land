.class final Lcom/google/android/gms/measurement/internal/hc;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final synthetic aJO:Lcom/google/android/gms/internal/measurement/ln;

.field private final synthetic aJk:Lcom/google/android/gms/measurement/internal/zzn;

.field private final synthetic aKm:Lcom/google/android/gms/measurement/internal/gw;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/gw;Lcom/google/android/gms/measurement/internal/zzn;Lcom/google/android/gms/internal/measurement/ln;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/hc;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    iput-object p2, p0, Lcom/google/android/gms/measurement/internal/hc;->aJk:Lcom/google/android/gms/measurement/internal/zzn;

    iput-object p3, p0, Lcom/google/android/gms/measurement/internal/hc;->aJO:Lcom/google/android/gms/internal/measurement/ln;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    const-string v0, "Failed to get app instance id"

    const/4 v1, 0x0

    .line 3
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/hc;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    .line 1320
    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/gw;->aKo:Lcom/google/android/gms/measurement/internal/cy;

    if-nez v2, :cond_0

    .line 5
    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/hc;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v2

    .line 2014
    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    .line 5
    invoke-virtual {v2, v0}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/hc;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/hc;->aJO:Lcom/google/android/gms/internal/measurement/ln;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/measurement/internal/it;->b(Lcom/google/android/gms/internal/measurement/ln;Ljava/lang/String;)V

    return-void

    .line 8
    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/google/android/gms/measurement/internal/hc;->aJk:Lcom/google/android/gms/measurement/internal/zzn;

    invoke-interface {v2, v3}, Lcom/google/android/gms/measurement/internal/cy;->c(Lcom/google/android/gms/measurement/internal/zzn;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 10
    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/hc;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/cb;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/gms/measurement/internal/fr;->cI(Ljava/lang/String;)V

    .line 11
    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/hc;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/dp;->aGY:Lcom/google/android/gms/measurement/internal/dv;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/measurement/internal/dv;->cq(Ljava/lang/String;)V

    .line 12
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/hc;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    .line 2321
    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/gw;->yJ()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 13
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/hc;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/hc;->aJO:Lcom/google/android/gms/internal/measurement/ln;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/measurement/internal/it;->b(Lcom/google/android/gms/internal/measurement/ln;Ljava/lang/String;)V

    return-void

    :catchall_0
    move-exception v0

    goto :goto_0

    :catch_0
    move-exception v2

    .line 16
    :try_start_2
    iget-object v3, p0, Lcom/google/android/gms/measurement/internal/hc;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v3

    .line 3014
    iget-object v3, v3, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    .line 16
    invoke-virtual {v3, v0, v2}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 17
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/hc;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/hc;->aJO:Lcom/google/android/gms/internal/measurement/ln;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/measurement/internal/it;->b(Lcom/google/android/gms/internal/measurement/ln;Ljava/lang/String;)V

    return-void

    .line 19
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/hc;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/measurement/internal/hc;->aJO:Lcom/google/android/gms/internal/measurement/ln;

    invoke-virtual {v2, v3, v1}, Lcom/google/android/gms/measurement/internal/it;->b(Lcom/google/android/gms/internal/measurement/ln;Ljava/lang/String;)V

    throw v0
.end method
