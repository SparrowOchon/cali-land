.class final Lcom/google/android/gms/measurement/internal/gu;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final synthetic aKi:Lcom/google/android/gms/measurement/internal/gr;

.field private final synthetic aKj:Z

.field private final synthetic aKk:Lcom/google/android/gms/measurement/internal/gs;

.field private final synthetic aKl:Lcom/google/android/gms/measurement/internal/gs;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/gr;ZLcom/google/android/gms/measurement/internal/gs;Lcom/google/android/gms/measurement/internal/gs;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/gu;->aKi:Lcom/google/android/gms/measurement/internal/gr;

    iput-boolean p2, p0, Lcom/google/android/gms/measurement/internal/gu;->aKj:Z

    iput-object p3, p0, Lcom/google/android/gms/measurement/internal/gu;->aKk:Lcom/google/android/gms/measurement/internal/gs;

    iput-object p4, p0, Lcom/google/android/gms/measurement/internal/gu;->aKl:Lcom/google/android/gms/measurement/internal/gs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .line 3
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/gu;->aKi:Lcom/google/android/gms/measurement/internal/gr;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/gu;->aKi:Lcom/google/android/gms/measurement/internal/gr;

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/cb;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/cz;->pT()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/jb;->de(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    .line 4
    iget-boolean v0, p0, Lcom/google/android/gms/measurement/internal/gu;->aKj:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/gu;->aKi:Lcom/google/android/gms/measurement/internal/gr;

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/gr;->aJX:Lcom/google/android/gms/measurement/internal/gs;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    .line 6
    iget-object v3, p0, Lcom/google/android/gms/measurement/internal/gu;->aKi:Lcom/google/android/gms/measurement/internal/gr;

    iget-object v4, v3, Lcom/google/android/gms/measurement/internal/gr;->aJX:Lcom/google/android/gms/measurement/internal/gs;

    invoke-static {v3, v4, v2}, Lcom/google/android/gms/measurement/internal/gr;->a(Lcom/google/android/gms/measurement/internal/gr;Lcom/google/android/gms/measurement/internal/gs;Z)V

    goto :goto_1

    .line 7
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/measurement/internal/gu;->aKj:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/gu;->aKi:Lcom/google/android/gms/measurement/internal/gr;

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/gr;->aJX:Lcom/google/android/gms/measurement/internal/gs;

    if-eqz v0, :cond_2

    .line 8
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/gu;->aKi:Lcom/google/android/gms/measurement/internal/gr;

    iget-object v3, v0, Lcom/google/android/gms/measurement/internal/gr;->aJX:Lcom/google/android/gms/measurement/internal/gs;

    invoke-static {v0, v3, v2}, Lcom/google/android/gms/measurement/internal/gr;->a(Lcom/google/android/gms/measurement/internal/gr;Lcom/google/android/gms/measurement/internal/gs;Z)V

    :cond_2
    const/4 v0, 0x0

    .line 9
    :cond_3
    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/measurement/internal/gu;->aKk:Lcom/google/android/gms/measurement/internal/gs;

    if-eqz v3, :cond_4

    iget-wide v3, v3, Lcom/google/android/gms/measurement/internal/gs;->aKf:J

    iget-object v5, p0, Lcom/google/android/gms/measurement/internal/gu;->aKl:Lcom/google/android/gms/measurement/internal/gs;

    iget-wide v5, v5, Lcom/google/android/gms/measurement/internal/gs;->aKf:J

    cmp-long v7, v3, v5

    if-nez v7, :cond_4

    iget-object v3, p0, Lcom/google/android/gms/measurement/internal/gu;->aKk:Lcom/google/android/gms/measurement/internal/gs;

    iget-object v3, v3, Lcom/google/android/gms/measurement/internal/gs;->aKe:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/measurement/internal/gu;->aKl:Lcom/google/android/gms/measurement/internal/gs;

    iget-object v4, v4, Lcom/google/android/gms/measurement/internal/gs;->aKe:Ljava/lang/String;

    .line 10
    invoke-static {v3, v4}, Lcom/google/android/gms/measurement/internal/it;->K(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/gms/measurement/internal/gu;->aKk:Lcom/google/android/gms/measurement/internal/gs;

    iget-object v3, v3, Lcom/google/android/gms/measurement/internal/gs;->aKd:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/measurement/internal/gu;->aKl:Lcom/google/android/gms/measurement/internal/gs;

    iget-object v4, v4, Lcom/google/android/gms/measurement/internal/gs;->aKd:Ljava/lang/String;

    .line 11
    invoke-static {v3, v4}, Lcom/google/android/gms/measurement/internal/it;->K(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    :cond_4
    const/4 v1, 0x1

    :cond_5
    if-eqz v1, :cond_9

    .line 13
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 14
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/gu;->aKl:Lcom/google/android/gms/measurement/internal/gs;

    invoke-static {v1, v8, v2}, Lcom/google/android/gms/measurement/internal/gr;->a(Lcom/google/android/gms/measurement/internal/gs;Landroid/os/Bundle;Z)V

    .line 15
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/gu;->aKk:Lcom/google/android/gms/measurement/internal/gs;

    if-eqz v1, :cond_7

    .line 16
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/gs;->aKd:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 17
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/gu;->aKk:Lcom/google/android/gms/measurement/internal/gs;

    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/gs;->aKd:Ljava/lang/String;

    const-string v2, "_pn"

    invoke-virtual {v8, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/gu;->aKk:Lcom/google/android/gms/measurement/internal/gs;

    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/gs;->aKe:Ljava/lang/String;

    const-string v2, "_pc"

    invoke-virtual {v8, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/gu;->aKk:Lcom/google/android/gms/measurement/internal/gs;

    iget-wide v1, v1, Lcom/google/android/gms/measurement/internal/gs;->aKf:J

    const-string v3, "_pi"

    invoke-virtual {v8, v3, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 20
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/gu;->aKi:Lcom/google/android/gms/measurement/internal/gr;

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/gu;->aKi:Lcom/google/android/gms/measurement/internal/gr;

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/cb;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/cz;->pT()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/jb;->de(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    if-eqz v0, :cond_8

    .line 21
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/gu;->aKi:Lcom/google/android/gms/measurement/internal/gr;

    .line 22
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cb;->vC()Lcom/google/android/gms/measurement/internal/hx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/hx;->yN()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_8

    .line 24
    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/gu;->aKi:Lcom/google/android/gms/measurement/internal/gr;

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v2

    invoke-virtual {v2, v8, v0, v1}, Lcom/google/android/gms/measurement/internal/it;->b(Landroid/os/Bundle;J)V

    .line 25
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/gu;->aKi:Lcom/google/android/gms/measurement/internal/gr;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cb;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v3

    .line 1085
    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 1086
    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/fh;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/e;->currentTimeMillis()J

    move-result-wide v6

    const-string v4, "auto"

    const-string v5, "_vs"

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/gms/measurement/internal/fr;->a(Ljava/lang/String;Ljava/lang/String;JLandroid/os/Bundle;)V

    .line 27
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/gu;->aKi:Lcom/google/android/gms/measurement/internal/gr;

    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/gu;->aKl:Lcom/google/android/gms/measurement/internal/gs;

    iput-object v1, v0, Lcom/google/android/gms/measurement/internal/gr;->aJX:Lcom/google/android/gms/measurement/internal/gs;

    .line 28
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cb;->vz()Lcom/google/android/gms/measurement/internal/gw;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/gu;->aKl:Lcom/google/android/gms/measurement/internal/gs;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/gw;->a(Lcom/google/android/gms/measurement/internal/gs;)V

    return-void
.end method
