.class final Lcom/google/android/gms/measurement/internal/iz;
.super Lcom/google/android/gms/measurement/internal/ij;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/ii;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/google/android/gms/measurement/internal/ij;-><init>(Lcom/google/android/gms/measurement/internal/ii;)V

    return-void
.end method

.method private static a(DLcom/google/android/gms/internal/measurement/ad$c;)Ljava/lang/Boolean;
    .locals 1

    .line 803
    :try_start_0
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p0, p1}, Ljava/math/BigDecimal;-><init>(D)V

    invoke-static {p0, p1}, Ljava/lang/Math;->ulp(D)D

    move-result-wide p0

    invoke-static {v0, p2, p0, p1}, Lcom/google/android/gms/measurement/internal/iz;->a(Ljava/math/BigDecimal;Lcom/google/android/gms/internal/measurement/ad$c;D)Ljava/lang/Boolean;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private static a(JLcom/google/android/gms/internal/measurement/ad$c;)Ljava/lang/Boolean;
    .locals 1

    .line 800
    :try_start_0
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p0, p1}, Ljava/math/BigDecimal;-><init>(J)V

    const-wide/16 p0, 0x0

    invoke-static {v0, p2, p0, p1}, Lcom/google/android/gms/measurement/internal/iz;->a(Ljava/math/BigDecimal;Lcom/google/android/gms/internal/measurement/ad$c;D)Ljava/lang/Boolean;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private final a(Lcom/google/android/gms/internal/measurement/ad$a;Ljava/lang/String;Ljava/util/List;J)Ljava/lang/Boolean;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/measurement/ad$a;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/google/android/gms/internal/measurement/al$e;",
            ">;J)",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    .line 50010
    iget v0, p1, Lcom/google/android/gms/internal/measurement/ad$a;->zzue:I

    and-int/lit8 v0, v0, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v3, 0x0

    if-eqz v0, :cond_2

    .line 592
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/ad$a;->oy()Lcom/google/android/gms/internal/measurement/ad$c;

    move-result-object v0

    invoke-static {p4, p5, v0}, Lcom/google/android/gms/measurement/internal/iz;->a(JLcom/google/android/gms/internal/measurement/ad$c;)Ljava/lang/Boolean;

    move-result-object p4

    if-nez p4, :cond_1

    return-object v3

    .line 595
    :cond_1
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p4

    if-nez p4, :cond_2

    .line 596
    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object p1

    .line 597
    :cond_2
    new-instance p4, Ljava/util/HashSet;

    invoke-direct {p4}, Ljava/util/HashSet;-><init>()V

    .line 50011
    iget-object p5, p1, Lcom/google/android/gms/internal/measurement/ad$a;->zzuh:Lcom/google/android/gms/internal/measurement/dz;

    .line 598
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p5

    :goto_1
    invoke-interface {p5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/ad$b;

    .line 50012
    iget-object v4, v0, Lcom/google/android/gms/internal/measurement/ad$b;->zzus:Ljava/lang/String;

    .line 599
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 600
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 50013
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    .line 602
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object p3

    invoke-virtual {p3, p2}, Lcom/google/android/gms/measurement/internal/df;->cf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    const-string p3, "null or empty param name in filter. event"

    .line 603
    invoke-virtual {p1, p3, p2}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    return-object v3

    .line 50014
    :cond_3
    iget-object v0, v0, Lcom/google/android/gms/internal/measurement/ad$b;->zzus:Ljava/lang/String;

    .line 605
    invoke-interface {p4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 607
    :cond_4
    new-instance p5, Landroidx/collection/ArrayMap;

    invoke-direct {p5}, Landroidx/collection/ArrayMap;-><init>()V

    .line 608
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_5
    :goto_2
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$e;

    .line 50015
    iget-object v4, v0, Lcom/google/android/gms/internal/measurement/al$e;->zzwk:Ljava/lang/String;

    .line 609
    invoke-interface {p4, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 610
    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/al$e;->pt()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 50016
    iget-object v4, v0, Lcom/google/android/gms/internal/measurement/al$e;->zzwk:Ljava/lang/String;

    .line 611
    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/al$e;->pt()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 50017
    iget-wide v5, v0, Lcom/google/android/gms/internal/measurement/al$e;->zzwp:J

    .line 611
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_3

    :cond_6
    move-object v0, v3

    :goto_3
    invoke-interface {p5, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 612
    :cond_7
    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/al$e;->pu()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 50018
    iget-object v4, v0, Lcom/google/android/gms/internal/measurement/al$e;->zzwk:Ljava/lang/String;

    .line 613
    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/al$e;->pu()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 50019
    iget-wide v5, v0, Lcom/google/android/gms/internal/measurement/al$e;->zzwt:D

    .line 613
    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_4

    :cond_8
    move-object v0, v3

    :goto_4
    invoke-interface {p5, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 614
    :cond_9
    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/al$e;->ps()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 50020
    iget-object v4, v0, Lcom/google/android/gms/internal/measurement/al$e;->zzwk:Ljava/lang/String;

    .line 50021
    iget-object v0, v0, Lcom/google/android/gms/internal/measurement/al$e;->zzwr:Ljava/lang/String;

    .line 615
    invoke-interface {p5, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 616
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 50022
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    .line 618
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object p3

    invoke-virtual {p3, p2}, Lcom/google/android/gms/measurement/internal/df;->cf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 619
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object p3

    .line 50023
    iget-object p4, v0, Lcom/google/android/gms/internal/measurement/al$e;->zzwk:Ljava/lang/String;

    .line 619
    invoke-virtual {p3, p4}, Lcom/google/android/gms/measurement/internal/df;->cg(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    const-string p4, "Unknown value for param. event, param"

    .line 620
    invoke-virtual {p1, p4, p2, p3}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v3

    .line 50024
    :cond_b
    iget-object p1, p1, Lcom/google/android/gms/internal/measurement/ad$a;->zzuh:Lcom/google/android/gms/internal/measurement/dz;

    .line 623
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_c
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_1b

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/google/android/gms/internal/measurement/ad$b;

    .line 624
    invoke-virtual {p3}, Lcom/google/android/gms/internal/measurement/ad$b;->oH()Z

    move-result p4

    if-eqz p4, :cond_d

    .line 50025
    iget-boolean p4, p3, Lcom/google/android/gms/internal/measurement/ad$b;->zzur:Z

    if-eqz p4, :cond_d

    const/4 p4, 0x1

    goto :goto_5

    :cond_d
    const/4 p4, 0x0

    .line 50026
    :goto_5
    iget-object v0, p3, Lcom/google/android/gms/internal/measurement/ad$b;->zzus:Ljava/lang/String;

    .line 626
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 627
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 50027
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    .line 629
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object p3

    invoke-virtual {p3, p2}, Lcom/google/android/gms/measurement/internal/df;->cf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    const-string p3, "Event has empty param name. event"

    .line 630
    invoke-virtual {p1, p3, p2}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    return-object v3

    .line 632
    :cond_e
    invoke-interface {p5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 633
    instance-of v5, v4, Ljava/lang/Long;

    if-eqz v5, :cond_11

    .line 634
    invoke-virtual {p3}, Lcom/google/android/gms/internal/measurement/ad$b;->oF()Z

    move-result v5

    if-nez v5, :cond_f

    .line 635
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 50028
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    .line 637
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object p3

    invoke-virtual {p3, p2}, Lcom/google/android/gms/measurement/internal/df;->cf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 638
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object p3

    invoke-virtual {p3, v0}, Lcom/google/android/gms/measurement/internal/df;->cg(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    const-string p4, "No number filter for long param. event, param"

    .line 639
    invoke-virtual {p1, p4, p2, p3}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v3

    .line 641
    :cond_f
    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p3}, Lcom/google/android/gms/internal/measurement/ad$b;->oG()Lcom/google/android/gms/internal/measurement/ad$c;

    move-result-object p3

    invoke-static {v4, v5, p3}, Lcom/google/android/gms/measurement/internal/iz;->a(JLcom/google/android/gms/internal/measurement/ad$c;)Ljava/lang/Boolean;

    move-result-object p3

    if-nez p3, :cond_10

    return-object v3

    .line 644
    :cond_10
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p3

    if-ne p3, p4, :cond_c

    .line 645
    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object p1

    .line 646
    :cond_11
    instance-of v5, v4, Ljava/lang/Double;

    if-eqz v5, :cond_14

    .line 647
    invoke-virtual {p3}, Lcom/google/android/gms/internal/measurement/ad$b;->oF()Z

    move-result v5

    if-nez v5, :cond_12

    .line 648
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 50029
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    .line 650
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object p3

    invoke-virtual {p3, p2}, Lcom/google/android/gms/measurement/internal/df;->cf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 651
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object p3

    invoke-virtual {p3, v0}, Lcom/google/android/gms/measurement/internal/df;->cg(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    const-string p4, "No number filter for double param. event, param"

    .line 652
    invoke-virtual {p1, p4, p2, p3}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v3

    .line 654
    :cond_12
    check-cast v4, Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-virtual {p3}, Lcom/google/android/gms/internal/measurement/ad$b;->oG()Lcom/google/android/gms/internal/measurement/ad$c;

    move-result-object p3

    invoke-static {v4, v5, p3}, Lcom/google/android/gms/measurement/internal/iz;->a(DLcom/google/android/gms/internal/measurement/ad$c;)Ljava/lang/Boolean;

    move-result-object p3

    if-nez p3, :cond_13

    return-object v3

    .line 657
    :cond_13
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p3

    if-ne p3, p4, :cond_c

    .line 658
    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object p1

    .line 659
    :cond_14
    instance-of v5, v4, Ljava/lang/String;

    if-eqz v5, :cond_19

    .line 660
    invoke-virtual {p3}, Lcom/google/android/gms/internal/measurement/ad$b;->oD()Z

    move-result v5

    if-eqz v5, :cond_15

    .line 661
    check-cast v4, Ljava/lang/String;

    invoke-virtual {p3}, Lcom/google/android/gms/internal/measurement/ad$b;->oE()Lcom/google/android/gms/internal/measurement/ad$e;

    move-result-object p3

    invoke-direct {p0, v4, p3}, Lcom/google/android/gms/measurement/internal/iz;->a(Ljava/lang/String;Lcom/google/android/gms/internal/measurement/ad$e;)Ljava/lang/Boolean;

    move-result-object p3

    goto :goto_6

    .line 662
    :cond_15
    invoke-virtual {p3}, Lcom/google/android/gms/internal/measurement/ad$b;->oF()Z

    move-result v5

    if-eqz v5, :cond_18

    .line 663
    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/gms/measurement/internal/ip;->cL(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_17

    .line 664
    invoke-virtual {p3}, Lcom/google/android/gms/internal/measurement/ad$b;->oG()Lcom/google/android/gms/internal/measurement/ad$c;

    move-result-object p3

    invoke-static {v4, p3}, Lcom/google/android/gms/measurement/internal/iz;->a(Ljava/lang/String;Lcom/google/android/gms/internal/measurement/ad$c;)Ljava/lang/Boolean;

    move-result-object p3

    :goto_6
    if-nez p3, :cond_16

    return-object v3

    .line 679
    :cond_16
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p3

    if-ne p3, p4, :cond_c

    .line 680
    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object p1

    .line 665
    :cond_17
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 50030
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    .line 667
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object p3

    invoke-virtual {p3, p2}, Lcom/google/android/gms/measurement/internal/df;->cf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 668
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object p3

    invoke-virtual {p3, v0}, Lcom/google/android/gms/measurement/internal/df;->cg(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    const-string p4, "Invalid param value for number filter. event, param"

    .line 669
    invoke-virtual {p1, p4, p2, p3}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v3

    .line 671
    :cond_18
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 50031
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    .line 673
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object p3

    invoke-virtual {p3, p2}, Lcom/google/android/gms/measurement/internal/df;->cf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 674
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object p3

    invoke-virtual {p3, v0}, Lcom/google/android/gms/measurement/internal/df;->cg(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    const-string p4, "No filter for String param. event, param"

    .line 675
    invoke-virtual {p1, p4, p2, p3}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v3

    :cond_19
    if-nez v4, :cond_1a

    .line 682
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 50032
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    .line 684
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object p3

    invoke-virtual {p3, p2}, Lcom/google/android/gms/measurement/internal/df;->cf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 685
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object p3

    invoke-virtual {p3, v0}, Lcom/google/android/gms/measurement/internal/df;->cg(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    const-string p4, "Missing param for filter. event, param"

    .line 686
    invoke-virtual {p1, p4, p2, p3}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 687
    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object p1

    .line 688
    :cond_1a
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 50033
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    .line 690
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object p3

    invoke-virtual {p3, p2}, Lcom/google/android/gms/measurement/internal/df;->cf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 691
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object p3

    invoke-virtual {p3, v0}, Lcom/google/android/gms/measurement/internal/df;->cg(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    const-string p4, "Unknown param type. event, param"

    .line 692
    invoke-virtual {p1, p4, p2, p3}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v3

    .line 694
    :cond_1b
    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object p1
.end method

.method private final a(Lcom/google/android/gms/internal/measurement/ad$d;Lcom/google/android/gms/internal/measurement/al$k;)Ljava/lang/Boolean;
    .locals 3

    .line 695
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/ad$d;->oR()Lcom/google/android/gms/internal/measurement/ad$b;

    move-result-object p1

    .line 50034
    iget-boolean v0, p1, Lcom/google/android/gms/internal/measurement/ad$b;->zzur:Z

    .line 697
    invoke-virtual {p2}, Lcom/google/android/gms/internal/measurement/al$k;->pt()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    .line 698
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/ad$b;->oF()Z

    move-result v1

    if-nez v1, :cond_0

    .line 699
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 50035
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    .line 701
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object v0

    .line 50036
    iget-object p2, p2, Lcom/google/android/gms/internal/measurement/al$k;->zzwk:Ljava/lang/String;

    .line 701
    invoke-virtual {v0, p2}, Lcom/google/android/gms/measurement/internal/df;->ch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    const-string v0, "No number filter for long property. property"

    .line 702
    invoke-virtual {p1, v0, p2}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    return-object v2

    .line 50037
    :cond_0
    iget-wide v1, p2, Lcom/google/android/gms/internal/measurement/al$k;->zzwp:J

    .line 705
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/ad$b;->oG()Lcom/google/android/gms/internal/measurement/ad$c;

    move-result-object p1

    invoke-static {v1, v2, p1}, Lcom/google/android/gms/measurement/internal/iz;->a(JLcom/google/android/gms/internal/measurement/ad$c;)Ljava/lang/Boolean;

    move-result-object p1

    .line 706
    invoke-static {p1, v0}, Lcom/google/android/gms/measurement/internal/iz;->a(Ljava/lang/Boolean;Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 707
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/internal/measurement/al$k;->pu()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 708
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/ad$b;->oF()Z

    move-result v1

    if-nez v1, :cond_2

    .line 709
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 50038
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    .line 711
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object v0

    .line 50039
    iget-object p2, p2, Lcom/google/android/gms/internal/measurement/al$k;->zzwk:Ljava/lang/String;

    .line 711
    invoke-virtual {v0, p2}, Lcom/google/android/gms/measurement/internal/df;->ch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    const-string v0, "No number filter for double property. property"

    .line 712
    invoke-virtual {p1, v0, p2}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    return-object v2

    .line 50040
    :cond_2
    iget-wide v1, p2, Lcom/google/android/gms/internal/measurement/al$k;->zzwt:D

    .line 715
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/ad$b;->oG()Lcom/google/android/gms/internal/measurement/ad$c;

    move-result-object p1

    invoke-static {v1, v2, p1}, Lcom/google/android/gms/measurement/internal/iz;->a(DLcom/google/android/gms/internal/measurement/ad$c;)Ljava/lang/Boolean;

    move-result-object p1

    .line 716
    invoke-static {p1, v0}, Lcom/google/android/gms/measurement/internal/iz;->a(Ljava/lang/Boolean;Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 717
    :cond_3
    invoke-virtual {p2}, Lcom/google/android/gms/internal/measurement/al$k;->ps()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 718
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/ad$b;->oD()Z

    move-result v1

    if-nez v1, :cond_6

    .line 719
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/ad$b;->oF()Z

    move-result v1

    if-nez v1, :cond_4

    .line 720
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 50041
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    .line 722
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object v0

    .line 50042
    iget-object p2, p2, Lcom/google/android/gms/internal/measurement/al$k;->zzwk:Ljava/lang/String;

    .line 722
    invoke-virtual {v0, p2}, Lcom/google/android/gms/measurement/internal/df;->ch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    const-string v0, "No string or number filter defined. property"

    .line 723
    invoke-virtual {p1, v0, p2}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 50043
    :cond_4
    iget-object v1, p2, Lcom/google/android/gms/internal/measurement/al$k;->zzwr:Ljava/lang/String;

    .line 724
    invoke-static {v1}, Lcom/google/android/gms/measurement/internal/ip;->cL(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 50044
    iget-object p2, p2, Lcom/google/android/gms/internal/measurement/al$k;->zzwr:Ljava/lang/String;

    .line 726
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/ad$b;->oG()Lcom/google/android/gms/internal/measurement/ad$c;

    move-result-object p1

    invoke-static {p2, p1}, Lcom/google/android/gms/measurement/internal/iz;->a(Ljava/lang/String;Lcom/google/android/gms/internal/measurement/ad$c;)Ljava/lang/Boolean;

    move-result-object p1

    .line 727
    invoke-static {p1, v0}, Lcom/google/android/gms/measurement/internal/iz;->a(Ljava/lang/Boolean;Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 728
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 50045
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    .line 730
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object v0

    .line 50046
    iget-object v1, p2, Lcom/google/android/gms/internal/measurement/al$k;->zzwk:Ljava/lang/String;

    .line 730
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/df;->ch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 50047
    iget-object p2, p2, Lcom/google/android/gms/internal/measurement/al$k;->zzwr:Ljava/lang/String;

    const-string v1, "Invalid user property value for Numeric number filter. property, value"

    .line 732
    invoke-virtual {p1, v1, v0, p2}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    return-object v2

    .line 50048
    :cond_6
    iget-object p2, p2, Lcom/google/android/gms/internal/measurement/al$k;->zzwr:Ljava/lang/String;

    .line 735
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/ad$b;->oE()Lcom/google/android/gms/internal/measurement/ad$e;

    move-result-object p1

    invoke-direct {p0, p2, p1}, Lcom/google/android/gms/measurement/internal/iz;->a(Ljava/lang/String;Lcom/google/android/gms/internal/measurement/ad$e;)Ljava/lang/Boolean;

    move-result-object p1

    .line 736
    invoke-static {p1, v0}, Lcom/google/android/gms/measurement/internal/iz;->a(Ljava/lang/Boolean;Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 737
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 50049
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    .line 739
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object v0

    .line 50050
    iget-object p2, p2, Lcom/google/android/gms/internal/measurement/al$k;->zzwk:Ljava/lang/String;

    .line 739
    invoke-virtual {v0, p2}, Lcom/google/android/gms/measurement/internal/df;->ch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    const-string v0, "User property has no value, property"

    .line 740
    invoke-virtual {p1, v0, p2}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    return-object v2
.end method

.method private static a(Ljava/lang/Boolean;Z)Ljava/lang/Boolean;
    .locals 0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 744
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eq p0, p1, :cond_1

    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method private static a(Ljava/lang/String;Lcom/google/android/gms/internal/measurement/ad$c;)Ljava/lang/Boolean;
    .locals 4

    .line 806
    invoke-static {p0}, Lcom/google/android/gms/measurement/internal/ip;->cL(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 808
    :cond_0
    :try_start_0
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, p1, v2, v3}, Lcom/google/android/gms/measurement/internal/iz;->a(Ljava/math/BigDecimal;Lcom/google/android/gms/internal/measurement/ad$c;D)Ljava/lang/Boolean;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    return-object v1
.end method

.method private final a(Ljava/lang/String;Lcom/google/android/gms/internal/measurement/ad$e$a;ZLjava/lang/String;Ljava/util/List;Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/internal/measurement/ad$e$a;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 777
    :cond_0
    sget-object v1, Lcom/google/android/gms/internal/measurement/ad$e$a;->arm:Lcom/google/android/gms/internal/measurement/ad$e$a;

    if-ne p2, v1, :cond_2

    if-eqz p5, :cond_1

    .line 778
    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_3

    :cond_1
    return-object v0

    :cond_2
    if-nez p4, :cond_3

    return-object v0

    :cond_3
    if-nez p3, :cond_5

    .line 782
    sget-object v1, Lcom/google/android/gms/internal/measurement/ad$e$a;->arh:Lcom/google/android/gms/internal/measurement/ad$e$a;

    if-ne p2, v1, :cond_4

    goto :goto_0

    .line 784
    :cond_4
    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 785
    :cond_5
    :goto_0
    sget-object v1, Lcom/google/android/gms/measurement/internal/iy;->aLL:[I

    invoke-virtual {p2}, Lcom/google/android/gms/internal/measurement/ad$e$a;->ordinal()I

    move-result p2

    aget p2, v1, p2

    packed-switch p2, :pswitch_data_0

    return-object v0

    .line 798
    :pswitch_0
    invoke-interface {p5, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 797
    :pswitch_1
    invoke-virtual {p1, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 796
    :pswitch_2
    invoke-virtual {p1, p4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 795
    :pswitch_3
    invoke-virtual {p1, p4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 794
    :pswitch_4
    invoke-virtual {p1, p4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :pswitch_5
    if-eqz p3, :cond_6

    const/4 p2, 0x0

    goto :goto_1

    :cond_6
    const/16 p2, 0x42

    .line 787
    :goto_1
    :try_start_0
    invoke-static {p6, p2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object p2

    .line 788
    invoke-virtual {p2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/regex/Matcher;->matches()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1
    :try_end_0
    .catch Ljava/util/regex/PatternSyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    .line 790
    :catch_0
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 50055
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    const-string p2, "Invalid regular expression in REGEXP audience filter. expression"

    .line 792
    invoke-virtual {p1, p2, p6}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private final a(Ljava/lang/String;Lcom/google/android/gms/internal/measurement/ad$e;)Ljava/lang/Boolean;
    .locals 9

    .line 745
    invoke-static {p2}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 748
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/internal/measurement/ad$e;->oT()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-virtual {p2}, Lcom/google/android/gms/internal/measurement/ad$e;->oU()Lcom/google/android/gms/internal/measurement/ad$e$a;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/internal/measurement/ad$e$a;->arg:Lcom/google/android/gms/internal/measurement/ad$e$a;

    if-ne v1, v2, :cond_1

    goto/16 :goto_6

    .line 750
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/internal/measurement/ad$e;->oU()Lcom/google/android/gms/internal/measurement/ad$e$a;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/internal/measurement/ad$e$a;->arm:Lcom/google/android/gms/internal/measurement/ad$e$a;

    if-ne v1, v2, :cond_2

    .line 751
    invoke-virtual {p2}, Lcom/google/android/gms/internal/measurement/ad$e;->oW()I

    move-result v1

    if-nez v1, :cond_3

    return-object v0

    .line 753
    :cond_2
    invoke-virtual {p2}, Lcom/google/android/gms/internal/measurement/ad$e;->oV()Z

    move-result v1

    if-nez v1, :cond_3

    return-object v0

    .line 755
    :cond_3
    invoke-virtual {p2}, Lcom/google/android/gms/internal/measurement/ad$e;->oU()Lcom/google/android/gms/internal/measurement/ad$e$a;

    move-result-object v4

    .line 50051
    iget-boolean v5, p2, Lcom/google/android/gms/internal/measurement/ad$e;->zzvn:Z

    if-nez v5, :cond_5

    .line 757
    sget-object v1, Lcom/google/android/gms/internal/measurement/ad$e$a;->arh:Lcom/google/android/gms/internal/measurement/ad$e$a;

    if-eq v4, v1, :cond_5

    sget-object v1, Lcom/google/android/gms/internal/measurement/ad$e$a;->arm:Lcom/google/android/gms/internal/measurement/ad$e$a;

    if-ne v4, v1, :cond_4

    goto :goto_0

    .line 50053
    :cond_4
    iget-object v1, p2, Lcom/google/android/gms/internal/measurement/ad$e;->zzvm:Ljava/lang/String;

    .line 759
    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 50052
    :cond_5
    :goto_0
    iget-object v1, p2, Lcom/google/android/gms/internal/measurement/ad$e;->zzvm:Ljava/lang/String;

    :goto_1
    move-object v6, v1

    .line 760
    invoke-virtual {p2}, Lcom/google/android/gms/internal/measurement/ad$e;->oW()I

    move-result v1

    if-nez v1, :cond_6

    move-object v7, v0

    goto :goto_4

    .line 50054
    :cond_6
    iget-object p2, p2, Lcom/google/android/gms/internal/measurement/ad$e;->zzvo:Lcom/google/android/gms/internal/measurement/dz;

    if-eqz v5, :cond_7

    :goto_2
    move-object v7, p2

    goto :goto_4

    .line 765
    :cond_7
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 766
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_3
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 767
    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 769
    :cond_8
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    goto :goto_2

    .line 772
    :goto_4
    sget-object p2, Lcom/google/android/gms/internal/measurement/ad$e$a;->arh:Lcom/google/android/gms/internal/measurement/ad$e$a;

    if-ne v4, p2, :cond_9

    move-object v8, v6

    goto :goto_5

    :cond_9
    move-object v8, v0

    :goto_5
    move-object v2, p0

    move-object v3, p1

    .line 774
    invoke-direct/range {v2 .. v8}, Lcom/google/android/gms/measurement/internal/iz;->a(Ljava/lang/String;Lcom/google/android/gms/internal/measurement/ad$e$a;ZLjava/lang/String;Ljava/util/List;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :cond_a
    :goto_6
    return-object v0
.end method

.method private static a(Ljava/math/BigDecimal;Lcom/google/android/gms/internal/measurement/ad$c;D)Ljava/lang/Boolean;
    .locals 10

    .line 811
    invoke-static {p1}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 812
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/ad$c;->oK()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_17

    .line 813
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/ad$c;->oL()Lcom/google/android/gms/internal/measurement/ad$c$b;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/internal/measurement/ad$c$b;->aqZ:Lcom/google/android/gms/internal/measurement/ad$c$b;

    if-ne v0, v2, :cond_0

    goto/16 :goto_5

    .line 815
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/ad$c;->oL()Lcom/google/android/gms/internal/measurement/ad$c$b;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/internal/measurement/ad$c$b;->ard:Lcom/google/android/gms/internal/measurement/ad$c$b;

    const/4 v3, 0x4

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-ne v0, v2, :cond_4

    .line 50056
    iget v0, p1, Lcom/google/android/gms/internal/measurement/ad$c;->zzue:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    .line 50057
    iget v0, p1, Lcom/google/android/gms/internal/measurement/ad$c;->zzue:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    if-nez v0, :cond_6

    :cond_3
    return-object v1

    .line 50058
    :cond_4
    iget v0, p1, Lcom/google/android/gms/internal/measurement/ad$c;->zzue:I

    and-int/2addr v0, v3

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    :goto_2
    if-nez v0, :cond_6

    return-object v1

    .line 820
    :cond_6
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/ad$c;->oL()Lcom/google/android/gms/internal/measurement/ad$c$b;

    move-result-object v0

    .line 824
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/ad$c;->oL()Lcom/google/android/gms/internal/measurement/ad$c$b;

    move-result-object v2

    sget-object v6, Lcom/google/android/gms/internal/measurement/ad$c$b;->ard:Lcom/google/android/gms/internal/measurement/ad$c$b;

    if-ne v2, v6, :cond_9

    .line 50059
    iget-object v2, p1, Lcom/google/android/gms/internal/measurement/ad$c;->zzux:Ljava/lang/String;

    .line 825
    invoke-static {v2}, Lcom/google/android/gms/measurement/internal/ip;->cL(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 50060
    iget-object v2, p1, Lcom/google/android/gms/internal/measurement/ad$c;->zzuy:Ljava/lang/String;

    .line 826
    invoke-static {v2}, Lcom/google/android/gms/measurement/internal/ip;->cL(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    goto :goto_3

    .line 828
    :cond_7
    :try_start_0
    new-instance v2, Ljava/math/BigDecimal;

    .line 50061
    iget-object v6, p1, Lcom/google/android/gms/internal/measurement/ad$c;->zzux:Ljava/lang/String;

    .line 828
    invoke-direct {v2, v6}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 829
    new-instance v6, Ljava/math/BigDecimal;

    .line 50062
    iget-object p1, p1, Lcom/google/android/gms/internal/measurement/ad$c;->zzuy:Ljava/lang/String;

    .line 829
    invoke-direct {v6, p1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-object p1, v2

    move-object v2, v1

    goto :goto_4

    :catch_0
    :cond_8
    :goto_3
    return-object v1

    .line 50063
    :cond_9
    iget-object v2, p1, Lcom/google/android/gms/internal/measurement/ad$c;->zzuw:Ljava/lang/String;

    .line 833
    invoke-static {v2}, Lcom/google/android/gms/measurement/internal/ip;->cL(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_a

    return-object v1

    .line 835
    :cond_a
    :try_start_1
    new-instance v2, Ljava/math/BigDecimal;

    .line 50064
    iget-object p1, p1, Lcom/google/android/gms/internal/measurement/ad$c;->zzuw:Ljava/lang/String;

    .line 835
    invoke-direct {v2, p1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-object p1, v1

    move-object v6, p1

    .line 841
    :goto_4
    sget-object v7, Lcom/google/android/gms/internal/measurement/ad$c$b;->ard:Lcom/google/android/gms/internal/measurement/ad$c$b;

    if-ne v0, v7, :cond_b

    if-nez p1, :cond_c

    return-object v1

    :cond_b
    if-eqz v2, :cond_17

    .line 845
    :cond_c
    sget-object v7, Lcom/google/android/gms/measurement/internal/iy;->aLM:[I

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/ad$c$b;->ordinal()I

    move-result v0

    aget v0, v7, v0

    const/4 v7, -0x1

    if-eq v0, v5, :cond_15

    const/4 v8, 0x2

    if-eq v0, v8, :cond_13

    const/4 v9, 0x3

    if-eq v0, v9, :cond_f

    if-eq v0, v3, :cond_d

    goto :goto_5

    .line 856
    :cond_d
    invoke-virtual {p0, p1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result p1

    if-eq p1, v7, :cond_e

    invoke-virtual {p0, v6}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result p0

    if-eq p0, v5, :cond_e

    const/4 v4, 0x1

    :cond_e
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0

    :cond_f
    const-wide/16 v0, 0x0

    cmpl-double p1, p2, v0

    if-eqz p1, :cond_11

    .line 849
    new-instance p1, Ljava/math/BigDecimal;

    invoke-direct {p1, p2, p3}, Ljava/math/BigDecimal;-><init>(D)V

    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, v8}, Ljava/math/BigDecimal;-><init>(I)V

    .line 850
    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    .line 851
    invoke-virtual {p0, p1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result p1

    if-ne p1, v5, :cond_10

    new-instance p1, Ljava/math/BigDecimal;

    invoke-direct {p1, p2, p3}, Ljava/math/BigDecimal;-><init>(D)V

    new-instance p2, Ljava/math/BigDecimal;

    invoke-direct {p2, v8}, Ljava/math/BigDecimal;-><init>(I)V

    .line 852
    invoke-virtual {p1, p2}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    .line 853
    invoke-virtual {p0, p1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result p0

    if-ne p0, v7, :cond_10

    const/4 v4, 0x1

    .line 854
    :cond_10
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0

    .line 855
    :cond_11
    invoke-virtual {p0, v2}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result p0

    if-nez p0, :cond_12

    const/4 v4, 0x1

    :cond_12
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0

    .line 847
    :cond_13
    invoke-virtual {p0, v2}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result p0

    if-ne p0, v5, :cond_14

    const/4 v4, 0x1

    :cond_14
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0

    .line 846
    :cond_15
    invoke-virtual {p0, v2}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result p0

    if-ne p0, v7, :cond_16

    const/4 v4, 0x1

    :cond_16
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0

    :catch_1
    :cond_17
    :goto_5
    return-object v1
.end method

.method private static a(Ljava/util/Map;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;IJ)V"
        }
    .end annotation

    .line 872
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    const-wide/16 v1, 0x3e8

    .line 873
    div-long/2addr p2, v1

    if-eqz v0, :cond_0

    .line 875
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v2, p2, v0

    if-lez v2, :cond_1

    .line 876
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-interface {p0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method private static b(Ljava/util/Map;IJ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;>;IJ)V"
        }
    .end annotation

    .line 878
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    .line 880
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 881
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {p0, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    const-wide/16 p0, 0x3e8

    .line 882
    div-long/2addr p2, p0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private static o(Ljava/util/Map;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/google/android/gms/internal/measurement/al$b;",
            ">;"
        }
    .end annotation

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 861
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    .line 862
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 863
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 865
    invoke-static {}, Lcom/google/android/gms/internal/measurement/al$b;->ph()Lcom/google/android/gms/internal/measurement/al$b$a;

    move-result-object v3

    .line 866
    invoke-virtual {v3, v2}, Lcom/google/android/gms/internal/measurement/al$b$a;->bs(I)Lcom/google/android/gms/internal/measurement/al$b$a;

    move-result-object v3

    .line 867
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/internal/measurement/al$b$a;->z(J)Lcom/google/android/gms/internal/measurement/al$b$a;

    move-result-object v2

    .line 868
    invoke-virtual {v2}, Lcom/google/android/gms/internal/measurement/dr$a;->si()Lcom/google/android/gms/internal/measurement/fb;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/internal/measurement/dr;

    check-cast v2, Lcom/google/android/gms/internal/measurement/al$b;

    .line 869
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method


# virtual methods
.method final a(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 69
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/google/android/gms/internal/measurement/al$c;",
            ">;",
            "Ljava/util/List<",
            "Lcom/google/android/gms/internal/measurement/al$k;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/google/android/gms/internal/measurement/al$a;",
            ">;"
        }
    .end annotation

    move-object/from16 v7, p0

    move-object/from16 v9, p1

    .line 4
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/common/internal/r;->aQ(Ljava/lang/String;)Ljava/lang/String;

    .line 5
    invoke-static/range {p2 .. p2}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 6
    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    new-instance v15, Ljava/util/HashSet;

    invoke-direct {v15}, Ljava/util/HashSet;-><init>()V

    .line 8
    new-instance v13, Landroidx/collection/ArrayMap;

    invoke-direct {v13}, Landroidx/collection/ArrayMap;-><init>()V

    .line 9
    new-instance v14, Landroidx/collection/ArrayMap;

    invoke-direct {v14}, Landroidx/collection/ArrayMap;-><init>()V

    .line 10
    new-instance v11, Landroidx/collection/ArrayMap;

    invoke-direct {v11}, Landroidx/collection/ArrayMap;-><init>()V

    .line 11
    new-instance v12, Landroidx/collection/ArrayMap;

    invoke-direct {v12}, Landroidx/collection/ArrayMap;-><init>()V

    .line 12
    new-instance v10, Landroidx/collection/ArrayMap;

    invoke-direct {v10}, Landroidx/collection/ArrayMap;-><init>()V

    .line 13
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/google/android/gms/measurement/internal/jb;->cY(Ljava/lang/String;)Z

    move-result v25

    .line 14
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v0

    .line 15
    sget-object v1, Lcom/google/android/gms/measurement/internal/j;->aFq:Lcom/google/android/gms/measurement/internal/cv;

    invoke-virtual {v0, v9, v1}, Lcom/google/android/gms/measurement/internal/jb;->d(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v26

    .line 18
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/measurement/internal/j;->aFy:Lcom/google/android/gms/measurement/internal/cv;

    .line 19
    invoke-virtual {v0, v9, v1}, Lcom/google/android/gms/measurement/internal/jb;->d(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v27

    .line 22
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/measurement/internal/j;->aFz:Lcom/google/android/gms/measurement/internal/cv;

    .line 23
    invoke-virtual {v0, v9, v1}, Lcom/google/android/gms/measurement/internal/jb;->d(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v28

    if-nez v27, :cond_0

    if-eqz v28, :cond_2

    .line 27
    :cond_0
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/measurement/al$c;

    .line 1031
    iget-object v2, v1, Lcom/google/android/gms/internal/measurement/al$c;->zzwk:Ljava/lang/String;

    const-string v3, "_s"

    .line 28
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1038
    iget-wide v0, v1, Lcom/google/android/gms/internal/measurement/al$c;->zzwl:J

    .line 29
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    move-object/from16 v29, v0

    goto :goto_0

    :cond_2
    const/16 v29, 0x0

    :goto_0
    const/4 v6, 0x1

    const/4 v4, 0x0

    if-eqz v29, :cond_3

    if-eqz v28, :cond_3

    .line 33
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v1

    .line 34
    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/ij;->vt()V

    .line 35
    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 36
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/common/internal/r;->aQ(Ljava/lang/String;)Ljava/lang/String;

    .line 37
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 38
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "current_session_count"

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 39
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/jg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "events"

    const-string v5, "app_id = ?"

    new-array v8, v6, [Ljava/lang/String;

    aput-object v9, v8, v4

    .line 40
    invoke-virtual {v2, v3, v0, v5, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 43
    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 2014
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    .line 45
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/measurement/internal/dh;->ci(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    const-string v3, "Error resetting session-scoped event counts. appId"

    .line 46
    invoke-virtual {v1, v3, v2, v0}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 47
    :cond_3
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/google/android/gms/measurement/internal/jg;->dk(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 48
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1a

    .line 49
    new-instance v1, Ljava/util/HashSet;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    if-eqz v27, :cond_b

    if-eqz v29, :cond_b

    .line 53
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xL()Lcom/google/android/gms/measurement/internal/iz;

    move-result-object v2

    .line 55
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/common/internal/r;->aQ(Ljava/lang/String;)Ljava/lang/String;

    .line 56
    invoke-static {v0}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    new-instance v3, Landroidx/collection/ArrayMap;

    invoke-direct {v3}, Landroidx/collection/ArrayMap;-><init>()V

    .line 58
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_c

    .line 60
    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/ig;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v5

    invoke-virtual {v5, v9}, Lcom/google/android/gms/measurement/internal/jg;->dj(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v5

    .line 61
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_c

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    .line 62
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/internal/measurement/al$i;

    .line 63
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    if-eqz v4, :cond_a

    .line 64
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v20

    if-eqz v20, :cond_4

    goto/16 :goto_5

    :cond_4
    move-object/from16 v20, v5

    .line 68
    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/ig;->xK()Lcom/google/android/gms/measurement/internal/ip;

    move-result-object v5

    move-object/from16 v21, v8

    .line 2017
    iget-object v8, v6, Lcom/google/android/gms/internal/measurement/al$i;->zzyw:Lcom/google/android/gms/internal/measurement/ea;

    .line 68
    invoke-virtual {v5, v8, v4}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    .line 69
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_9

    .line 71
    invoke-virtual {v6}, Lcom/google/android/gms/internal/measurement/dr;->rZ()Lcom/google/android/gms/internal/measurement/dr$a;

    move-result-object v8

    .line 72
    check-cast v8, Lcom/google/android/gms/internal/measurement/dr$a;

    check-cast v8, Lcom/google/android/gms/internal/measurement/al$i$a;

    invoke-virtual {v8}, Lcom/google/android/gms/internal/measurement/al$i$a;->qj()Lcom/google/android/gms/internal/measurement/al$i$a;

    move-result-object v8

    invoke-virtual {v8, v5}, Lcom/google/android/gms/internal/measurement/al$i$a;->g(Ljava/lang/Iterable;)Lcom/google/android/gms/internal/measurement/al$i$a;

    move-result-object v5

    .line 74
    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/ig;->xK()Lcom/google/android/gms/measurement/internal/ip;

    move-result-object v8

    move-object/from16 v22, v2

    .line 3007
    iget-object v2, v6, Lcom/google/android/gms/internal/measurement/al$i;->zzyv:Lcom/google/android/gms/internal/measurement/ea;

    .line 74
    invoke-virtual {v8, v2, v4}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 75
    invoke-virtual {v5}, Lcom/google/android/gms/internal/measurement/al$i$a;->qi()Lcom/google/android/gms/internal/measurement/al$i$a;

    move-result-object v8

    invoke-virtual {v8, v2}, Lcom/google/android/gms/internal/measurement/al$i$a;->f(Ljava/lang/Iterable;)Lcom/google/android/gms/internal/measurement/al$i$a;

    const/4 v2, 0x0

    .line 76
    :goto_3
    invoke-virtual {v6}, Lcom/google/android/gms/internal/measurement/al$i;->qb()I

    move-result v8

    if-ge v2, v8, :cond_6

    .line 3029
    iget-object v8, v6, Lcom/google/android/gms/internal/measurement/al$i;->zzyx:Lcom/google/android/gms/internal/measurement/dz;

    invoke-interface {v8, v2}, Lcom/google/android/gms/internal/measurement/dz;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/gms/internal/measurement/al$b;

    .line 4004
    iget v8, v8, Lcom/google/android/gms/internal/measurement/al$b;->zzwg:I

    .line 78
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 79
    invoke-interface {v4, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 80
    invoke-virtual {v5, v2}, Lcom/google/android/gms/internal/measurement/al$i$a;->bD(I)Lcom/google/android/gms/internal/measurement/al$i$a;

    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_6
    const/4 v2, 0x0

    .line 82
    :goto_4
    invoke-virtual {v6}, Lcom/google/android/gms/internal/measurement/al$i;->qd()I

    move-result v8

    if-ge v2, v8, :cond_8

    .line 4042
    iget-object v8, v6, Lcom/google/android/gms/internal/measurement/al$i;->zzyy:Lcom/google/android/gms/internal/measurement/dz;

    invoke-interface {v8, v2}, Lcom/google/android/gms/internal/measurement/dz;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/gms/internal/measurement/al$j;

    .line 5005
    iget v8, v8, Lcom/google/android/gms/internal/measurement/al$j;->zzwg:I

    .line 84
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 85
    invoke-interface {v4, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 86
    invoke-virtual {v5, v2}, Lcom/google/android/gms/internal/measurement/al$i$a;->bE(I)Lcom/google/android/gms/internal/measurement/al$i$a;

    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 88
    :cond_8
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5}, Lcom/google/android/gms/internal/measurement/dr$a;->si()Lcom/google/android/gms/internal/measurement/fb;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/internal/measurement/dr;

    check-cast v4, Lcom/google/android/gms/internal/measurement/al$i;

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_6

    :cond_9
    move-object/from16 v5, v20

    move-object/from16 v8, v21

    goto :goto_7

    :cond_a
    :goto_5
    move-object/from16 v22, v2

    move-object/from16 v20, v5

    move-object/from16 v21, v8

    .line 65
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_6
    move-object/from16 v5, v20

    move-object/from16 v8, v21

    move-object/from16 v2, v22

    :goto_7
    const/4 v4, 0x0

    const/4 v6, 0x1

    goto/16 :goto_2

    :cond_b
    move-object v3, v0

    .line 92
    :cond_c
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 93
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/internal/measurement/al$i;

    .line 94
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v14, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/BitSet;

    .line 95
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v11, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/BitSet;

    if-eqz v25, :cond_12

    .line 100
    new-instance v8, Landroidx/collection/ArrayMap;

    invoke-direct {v8}, Landroidx/collection/ArrayMap;-><init>()V

    if-eqz v4, :cond_11

    .line 101
    invoke-virtual {v4}, Lcom/google/android/gms/internal/measurement/al$i;->qb()I

    move-result v17

    if-nez v17, :cond_d

    goto :goto_d

    :cond_d
    move-object/from16 v17, v1

    .line 5027
    iget-object v1, v4, Lcom/google/android/gms/internal/measurement/al$i;->zzyx:Lcom/google/android/gms/internal/measurement/dz;

    .line 103
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_10

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    move-object/from16 v21, v1

    move-object/from16 v1, v20

    check-cast v1, Lcom/google/android/gms/internal/measurement/al$b;

    .line 104
    invoke-virtual {v1}, Lcom/google/android/gms/internal/measurement/al$b;->pf()Z

    move-result v20

    if-eqz v20, :cond_f

    move-object/from16 v20, v3

    .line 6004
    iget v3, v1, Lcom/google/android/gms/internal/measurement/al$b;->zzwg:I

    .line 106
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 107
    invoke-virtual {v1}, Lcom/google/android/gms/internal/measurement/al$b;->pg()Z

    move-result v22

    if-eqz v22, :cond_e

    move-object/from16 v22, v6

    .line 6009
    iget-wide v6, v1, Lcom/google/android/gms/internal/measurement/al$b;->zzwh:J

    .line 108
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_a

    :cond_e
    move-object/from16 v22, v6

    const/4 v1, 0x0

    .line 110
    :goto_a
    invoke-interface {v8, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_b

    :cond_f
    move-object/from16 v20, v3

    move-object/from16 v22, v6

    :goto_b
    move-object/from16 v7, p0

    move-object/from16 v3, v20

    move-object/from16 v1, v21

    move-object/from16 v6, v22

    goto :goto_9

    :cond_10
    :goto_c
    move-object/from16 v20, v3

    move-object/from16 v22, v6

    goto :goto_e

    :cond_11
    :goto_d
    move-object/from16 v17, v1

    goto :goto_c

    .line 115
    :goto_e
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 116
    invoke-interface {v12, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_f

    :cond_12
    move-object/from16 v17, v1

    move-object/from16 v20, v3

    move-object/from16 v22, v6

    const/4 v8, 0x0

    :goto_f
    if-nez v5, :cond_13

    .line 118
    new-instance v5, Ljava/util/BitSet;

    invoke-direct {v5}, Ljava/util/BitSet;-><init>()V

    .line 119
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v14, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    new-instance v6, Ljava/util/BitSet;

    invoke-direct {v6}, Ljava/util/BitSet;-><init>()V

    .line 121
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v11, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_10

    :cond_13
    move-object/from16 v6, v22

    :goto_10
    if-eqz v4, :cond_17

    const/4 v1, 0x0

    .line 123
    :goto_11
    invoke-virtual {v4}, Lcom/google/android/gms/internal/measurement/al$i;->qa()I

    move-result v3

    shl-int/lit8 v3, v3, 0x6

    if-ge v1, v3, :cond_17

    .line 7007
    iget-object v3, v4, Lcom/google/android/gms/internal/measurement/al$i;->zzyv:Lcom/google/android/gms/internal/measurement/ea;

    .line 125
    invoke-static {v3, v1}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/util/List;I)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 126
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v3

    .line 7022
    iget-object v3, v3, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    .line 128
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v21, v11

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    move-object/from16 v22, v12

    const-string v12, "Filter already evaluated. audience ID, filter ID"

    invoke-virtual {v3, v12, v7, v11}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 129
    invoke-virtual {v6, v1}, Ljava/util/BitSet;->set(I)V

    .line 8017
    iget-object v3, v4, Lcom/google/android/gms/internal/measurement/al$i;->zzyw:Lcom/google/android/gms/internal/measurement/ea;

    .line 130
    invoke-static {v3, v1}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/util/List;I)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 131
    invoke-virtual {v5, v1}, Ljava/util/BitSet;->set(I)V

    const/4 v3, 0x1

    goto :goto_12

    :cond_14
    move-object/from16 v21, v11

    move-object/from16 v22, v12

    :cond_15
    const/4 v3, 0x0

    :goto_12
    if-eqz v8, :cond_16

    if-nez v3, :cond_16

    .line 134
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v8, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_16
    add-int/lit8 v1, v1, 0x1

    move-object/from16 v11, v21

    move-object/from16 v12, v22

    goto :goto_11

    :cond_17
    move-object/from16 v21, v11

    move-object/from16 v22, v12

    .line 136
    invoke-static {}, Lcom/google/android/gms/internal/measurement/al$a;->pc()Lcom/google/android/gms/internal/measurement/al$a$a;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/google/android/gms/internal/measurement/al$a$a;->H(Z)Lcom/google/android/gms/internal/measurement/al$a$a;

    move-result-object v1

    if-eqz v27, :cond_18

    .line 139
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/internal/measurement/al$i;

    .line 140
    invoke-virtual {v1, v3}, Lcom/google/android/gms/internal/measurement/al$a$a;->a(Lcom/google/android/gms/internal/measurement/al$i;)Lcom/google/android/gms/internal/measurement/al$a$a;

    goto :goto_13

    .line 141
    :cond_18
    invoke-virtual {v1, v4}, Lcom/google/android/gms/internal/measurement/al$a$a;->a(Lcom/google/android/gms/internal/measurement/al$i;)Lcom/google/android/gms/internal/measurement/al$a$a;

    .line 142
    :goto_13
    invoke-static {}, Lcom/google/android/gms/internal/measurement/al$i;->qf()Lcom/google/android/gms/internal/measurement/al$i$a;

    move-result-object v3

    .line 143
    invoke-static {v5}, Lcom/google/android/gms/measurement/internal/ip;->a(Ljava/util/BitSet;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/internal/measurement/al$i$a;->g(Ljava/lang/Iterable;)Lcom/google/android/gms/internal/measurement/al$i$a;

    move-result-object v3

    .line 144
    invoke-static {v6}, Lcom/google/android/gms/measurement/internal/ip;->a(Ljava/util/BitSet;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/internal/measurement/al$i$a;->f(Ljava/lang/Iterable;)Lcom/google/android/gms/internal/measurement/al$i$a;

    move-result-object v3

    if-eqz v25, :cond_19

    .line 147
    invoke-static {v8}, Lcom/google/android/gms/measurement/internal/iz;->o(Ljava/util/Map;)Ljava/util/List;

    move-result-object v4

    .line 148
    invoke-virtual {v3, v4}, Lcom/google/android/gms/internal/measurement/al$i$a;->h(Ljava/lang/Iterable;)Lcom/google/android/gms/internal/measurement/al$i$a;

    .line 149
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    new-instance v5, Landroidx/collection/ArrayMap;

    invoke-direct {v5}, Landroidx/collection/ArrayMap;-><init>()V

    invoke-interface {v10, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    :cond_19
    invoke-virtual {v1, v3}, Lcom/google/android/gms/internal/measurement/al$a$a;->a(Lcom/google/android/gms/internal/measurement/al$i$a;)Lcom/google/android/gms/internal/measurement/al$a$a;

    .line 151
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/gms/internal/measurement/dr$a;->si()Lcom/google/android/gms/internal/measurement/fb;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/measurement/dr;

    check-cast v1, Lcom/google/android/gms/internal/measurement/al$a;

    invoke-interface {v13, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v7, p0

    move-object/from16 v1, v17

    move-object/from16 v3, v20

    move-object/from16 v11, v21

    move-object/from16 v12, v22

    goto/16 :goto_8

    :cond_1a
    move-object/from16 v21, v11

    move-object/from16 v22, v12

    .line 153
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const-string v7, "Filter definition"

    const-string v8, "Skipping failed audience ID"

    const-string v30, "null"

    if-nez v0, :cond_4c

    .line 157
    new-instance v6, Landroidx/collection/ArrayMap;

    invoke-direct {v6}, Landroidx/collection/ArrayMap;-><init>()V

    .line 158
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v31

    const-wide/16 v32, 0x0

    move-wide/from16 v2, v32

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_14
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4c

    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/internal/measurement/al$c;

    .line 8031
    iget-object v5, v4, Lcom/google/android/gms/internal/measurement/al$c;->zzwk:Ljava/lang/String;

    .line 9005
    iget-object v11, v4, Lcom/google/android/gms/internal/measurement/al$c;->zzwj:Lcom/google/android/gms/internal/measurement/dz;

    .line 161
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xK()Lcom/google/android/gms/measurement/internal/ip;

    const-string v12, "_eid"

    invoke-static {v4, v12}, Lcom/google/android/gms/measurement/internal/ip;->c(Lcom/google/android/gms/internal/measurement/al$c;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v23

    move-object/from16 v24, v8

    move-object/from16 v8, v23

    check-cast v8, Ljava/lang/Long;

    if-eqz v8, :cond_1b

    const/16 v23, 0x1

    goto :goto_15

    :cond_1b
    const/16 v23, 0x0

    :goto_15
    move-wide/from16 v34, v2

    if-eqz v23, :cond_1c

    const-string v2, "_ep"

    .line 164
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1c

    const/4 v2, 0x1

    goto :goto_16

    :cond_1c
    const/4 v2, 0x0

    :goto_16
    const-wide/16 v36, 0x1

    if-eqz v2, :cond_27

    .line 166
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xK()Lcom/google/android/gms/measurement/internal/ip;

    const-string v2, "_en"

    invoke-static {v4, v2}, Lcom/google/android/gms/measurement/internal/ip;->c(Lcom/google/android/gms/internal/measurement/al$c;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    move-object v5, v2

    check-cast v5, Ljava/lang/String;

    .line 167
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 168
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v2

    .line 9014
    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string v3, "Extra parameter without an event name. eventId"

    .line 168
    invoke-virtual {v2, v3, v8}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    move-object/from16 v40, v6

    move-object/from16 v18, v10

    goto/16 :goto_21

    :cond_1d
    if-eqz v0, :cond_1f

    if-eqz v1, :cond_1f

    .line 170
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v38

    cmp-long v23, v2, v38

    if-eqz v23, :cond_1e

    goto :goto_17

    :cond_1e
    move-object v12, v0

    move-object/from16 v23, v1

    goto :goto_18

    .line 172
    :cond_1f
    :goto_17
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v2

    invoke-virtual {v2, v9, v8}, Lcom/google/android/gms/measurement/internal/jg;->b(Ljava/lang/String;Ljava/lang/Long;)Landroid/util/Pair;

    move-result-object v2

    if-eqz v2, :cond_26

    .line 173
    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-nez v3, :cond_20

    goto/16 :goto_20

    .line 178
    :cond_20
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$c;

    .line 179
    iget-object v1, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 181
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xK()Lcom/google/android/gms/measurement/internal/ip;

    invoke-static {v0, v12}, Lcom/google/android/gms/measurement/internal/ip;->c(Lcom/google/android/gms/internal/measurement/al$c;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    move-object v12, v0

    move-object/from16 v23, v1

    move-wide/from16 v34, v2

    :goto_18
    sub-long v34, v34, v36

    cmp-long v0, v34, v32

    if-gtz v0, :cond_21

    .line 184
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v1

    .line 185
    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 186
    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 10022
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    const-string v2, "Clearing complex main event info. appId"

    .line 186
    invoke-virtual {v0, v2, v9}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    .line 187
    :try_start_1
    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/jg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v2, "delete from main_event_params where app_id=?"
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_3

    const/4 v3, 0x1

    :try_start_2
    new-array v8, v3, [Ljava/lang/String;
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_2

    const/16 v18, 0x0

    :try_start_3
    aput-object v9, v8, v18

    .line 188
    invoke-virtual {v0, v2, v8}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1b

    :catch_1
    move-exception v0

    goto :goto_1a

    :catch_2
    move-exception v0

    goto :goto_19

    :catch_3
    move-exception v0

    const/4 v3, 0x1

    :goto_19
    const/16 v18, 0x0

    .line 191
    :goto_1a
    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 11014
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string v2, "Error clearing complex main event"

    .line 191
    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    :goto_1b
    move-object v8, v4

    move-object/from16 v40, v6

    move-object/from16 v18, v10

    move-object v10, v5

    goto :goto_1c

    :cond_21
    const/4 v3, 0x1

    const/16 v18, 0x0

    .line 193
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v1

    move-object/from16 v2, p1

    const/16 v19, 0x1

    move-object v3, v8

    move-object v8, v4

    move-object/from16 v18, v10

    move-object v10, v5

    move-wide/from16 v4, v34

    move-object/from16 v40, v6

    move-object v6, v12

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/measurement/internal/jg;->a(Ljava/lang/String;Ljava/lang/Long;JLcom/google/android/gms/internal/measurement/al$c;)Z

    .line 194
    :goto_1c
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 12005
    iget-object v1, v12, Lcom/google/android/gms/internal/measurement/al$c;->zzwj:Lcom/google/android/gms/internal/measurement/dz;

    .line 195
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_22
    :goto_1d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_23

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/internal/measurement/al$e;

    .line 197
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xK()Lcom/google/android/gms/measurement/internal/ip;

    .line 13005
    iget-object v3, v2, Lcom/google/android/gms/internal/measurement/al$e;->zzwk:Ljava/lang/String;

    .line 197
    invoke-static {v8, v3}, Lcom/google/android/gms/measurement/internal/ip;->b(Lcom/google/android/gms/internal/measurement/al$c;Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/al$e;

    move-result-object v3

    if-nez v3, :cond_22

    .line 199
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1d

    .line 201
    :cond_23
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_25

    .line 202
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_24

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/internal/measurement/al$e;

    .line 203
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1e

    :cond_24
    move-object/from16 v41, v0

    move-object v0, v10

    goto :goto_1f

    .line 206
    :cond_25
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 13017
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "No unique parameters in main event. eventName"

    .line 206
    invoke-virtual {v0, v1, v10}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    move-object v0, v10

    move-object/from16 v41, v11

    :goto_1f
    move-object/from16 v38, v12

    move-object/from16 v39, v23

    move-object v12, v8

    goto/16 :goto_24

    :cond_26
    :goto_20
    move-object/from16 v40, v6

    move-object/from16 v18, v10

    move-object v10, v5

    .line 174
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v2

    .line 10014
    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string v3, "Extra parameter without existing main event. eventName, eventId"

    .line 176
    invoke-virtual {v2, v3, v10, v8}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_21
    move-object/from16 v10, v18

    move-object/from16 v8, v24

    move-wide/from16 v2, v34

    move-object/from16 v6, v40

    goto/16 :goto_14

    :cond_27
    move-object v12, v4

    move-object/from16 v40, v6

    move-object/from16 v18, v10

    if-eqz v23, :cond_2a

    .line 211
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xK()Lcom/google/android/gms/measurement/internal/ip;

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const-string v1, "_epc"

    .line 212
    invoke-static {v12, v1}, Lcom/google/android/gms/measurement/internal/ip;->c(Lcom/google/android/gms/internal/measurement/al$c;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_28

    goto :goto_22

    :cond_28
    move-object v0, v1

    .line 214
    :goto_22
    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v34

    cmp-long v0, v34, v32

    if-gtz v0, :cond_29

    .line 216
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 14017
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "Complex event with zero extra param count. eventName"

    .line 218
    invoke-virtual {v0, v1, v5}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    move-object v10, v5

    goto :goto_23

    .line 219
    :cond_29
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v1

    move-object/from16 v2, p1

    move-object v3, v8

    move-object v10, v5

    move-wide/from16 v4, v34

    move-object v6, v12

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/measurement/internal/jg;->a(Ljava/lang/String;Ljava/lang/Long;JLcom/google/android/gms/internal/measurement/al$c;)Z

    :goto_23
    move-object/from16 v39, v8

    move-object v0, v10

    move-object/from16 v41, v11

    move-object/from16 v38, v12

    goto :goto_24

    :cond_2a
    move-object v10, v5

    move-object/from16 v38, v0

    move-object/from16 v39, v1

    move-object v0, v10

    move-object/from16 v41, v11

    .line 220
    :goto_24
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v1

    .line 14031
    iget-object v2, v12, Lcom/google/android/gms/internal/measurement/al$c;->zzwk:Ljava/lang/String;

    .line 220
    invoke-virtual {v1, v9, v2}, Lcom/google/android/gms/measurement/internal/jg;->L(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/f;

    move-result-object v1

    if-nez v1, :cond_2c

    .line 222
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 15017
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    .line 224
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/measurement/internal/dh;->ci(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 225
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/gms/measurement/internal/df;->cf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "Event aggregate wasn\'t created during raw event logging. appId, event"

    .line 226
    invoke-virtual {v1, v4, v2, v3}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    if-eqz v28, :cond_2b

    .line 228
    new-instance v1, Lcom/google/android/gms/measurement/internal/f;

    move-object/from16 v5, v24

    const/4 v6, 0x0

    move-object v8, v1

    .line 15031
    iget-object v10, v12, Lcom/google/android/gms/internal/measurement/al$c;->zzwk:Ljava/lang/String;

    move-object/from16 v4, v18

    const-wide/16 v2, 0x1

    move-object/from16 v44, v12

    move-object/from16 v42, v21

    move-object/from16 v43, v22

    move-wide v11, v2

    move-object/from16 v45, v13

    move-object/from16 v46, v14

    move-wide v13, v2

    move-object/from16 v47, v15

    move-wide v15, v2

    move-object/from16 v3, v44

    move-object/from16 v44, v7

    .line 15038
    iget-wide v6, v3, Lcom/google/android/gms/internal/measurement/al$c;->zzwl:J

    move-wide/from16 v17, v6

    const-wide/16 v19, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    move-object v7, v9

    move-object/from16 v9, p1

    .line 230
    invoke-direct/range {v8 .. v24}, Lcom/google/android/gms/measurement/internal/f;-><init>(Ljava/lang/String;Ljava/lang/String;JJJJJLjava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;)V

    goto/16 :goto_25

    :cond_2b
    move-object/from16 v44, v7

    move-object v7, v9

    move-object v3, v12

    move-object/from16 v45, v13

    move-object/from16 v46, v14

    move-object/from16 v47, v15

    move-object/from16 v4, v18

    move-object/from16 v42, v21

    move-object/from16 v43, v22

    move-object/from16 v5, v24

    .line 231
    new-instance v1, Lcom/google/android/gms/measurement/internal/f;

    .line 16031
    iget-object v10, v3, Lcom/google/android/gms/internal/measurement/al$c;->zzwk:Ljava/lang/String;

    const-wide/16 v11, 0x1

    const-wide/16 v13, 0x1

    .line 16038
    iget-wide v8, v3, Lcom/google/android/gms/internal/measurement/al$c;->zzwl:J

    move-wide v15, v8

    move-object v8, v1

    move-object/from16 v9, p1

    .line 233
    invoke-direct/range {v8 .. v16}, Lcom/google/android/gms/measurement/internal/f;-><init>(Ljava/lang/String;Ljava/lang/String;JJJ)V

    goto :goto_25

    :cond_2c
    move-object/from16 v44, v7

    move-object v7, v9

    move-object v3, v12

    move-object/from16 v45, v13

    move-object/from16 v46, v14

    move-object/from16 v47, v15

    move-object/from16 v4, v18

    move-object/from16 v42, v21

    move-object/from16 v43, v22

    move-object/from16 v5, v24

    if-eqz v28, :cond_2d

    .line 236
    new-instance v2, Lcom/google/android/gms/measurement/internal/f;

    move-object/from16 v48, v2

    iget-object v6, v1, Lcom/google/android/gms/measurement/internal/f;->aDF:Ljava/lang/String;

    move-object/from16 v49, v6

    iget-object v6, v1, Lcom/google/android/gms/measurement/internal/f;->name:Ljava/lang/String;

    move-object/from16 v50, v6

    iget-wide v8, v1, Lcom/google/android/gms/measurement/internal/f;->aDG:J

    add-long v51, v8, v36

    iget-wide v8, v1, Lcom/google/android/gms/measurement/internal/f;->aDH:J

    add-long v53, v8, v36

    iget-wide v8, v1, Lcom/google/android/gms/measurement/internal/f;->aDI:J

    add-long v55, v8, v36

    iget-wide v8, v1, Lcom/google/android/gms/measurement/internal/f;->aDJ:J

    move-wide/from16 v57, v8

    iget-wide v8, v1, Lcom/google/android/gms/measurement/internal/f;->aDK:J

    move-wide/from16 v59, v8

    iget-object v6, v1, Lcom/google/android/gms/measurement/internal/f;->aDL:Ljava/lang/Long;

    move-object/from16 v61, v6

    iget-object v6, v1, Lcom/google/android/gms/measurement/internal/f;->aDM:Ljava/lang/Long;

    move-object/from16 v62, v6

    iget-object v6, v1, Lcom/google/android/gms/measurement/internal/f;->aDN:Ljava/lang/Long;

    move-object/from16 v63, v6

    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/f;->aDO:Ljava/lang/Boolean;

    move-object/from16 v64, v1

    invoke-direct/range {v48 .. v64}, Lcom/google/android/gms/measurement/internal/f;-><init>(Ljava/lang/String;Ljava/lang/String;JJJJJLjava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;)V

    move-object v8, v2

    :goto_25
    move-object v6, v3

    goto :goto_26

    .line 239
    :cond_2d
    new-instance v2, Lcom/google/android/gms/measurement/internal/f;

    move-object v8, v2

    iget-object v9, v1, Lcom/google/android/gms/measurement/internal/f;->aDF:Ljava/lang/String;

    iget-object v10, v1, Lcom/google/android/gms/measurement/internal/f;->name:Ljava/lang/String;

    iget-wide v11, v1, Lcom/google/android/gms/measurement/internal/f;->aDG:J

    add-long v11, v11, v36

    iget-wide v13, v1, Lcom/google/android/gms/measurement/internal/f;->aDH:J

    add-long v13, v13, v36

    move-object/from16 v36, v2

    move-object v6, v3

    iget-wide v2, v1, Lcom/google/android/gms/measurement/internal/f;->aDI:J

    move-wide v15, v2

    iget-wide v2, v1, Lcom/google/android/gms/measurement/internal/f;->aDJ:J

    move-wide/from16 v17, v2

    iget-wide v2, v1, Lcom/google/android/gms/measurement/internal/f;->aDK:J

    move-wide/from16 v19, v2

    iget-object v2, v1, Lcom/google/android/gms/measurement/internal/f;->aDL:Ljava/lang/Long;

    move-object/from16 v21, v2

    iget-object v2, v1, Lcom/google/android/gms/measurement/internal/f;->aDM:Ljava/lang/Long;

    move-object/from16 v22, v2

    iget-object v2, v1, Lcom/google/android/gms/measurement/internal/f;->aDN:Ljava/lang/Long;

    move-object/from16 v23, v2

    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/f;->aDO:Ljava/lang/Boolean;

    move-object/from16 v24, v1

    invoke-direct/range {v8 .. v24}, Lcom/google/android/gms/measurement/internal/f;-><init>(Ljava/lang/String;Ljava/lang/String;JJJJJLjava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;)V

    move-object/from16 v8, v36

    .line 241
    :goto_26
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/google/android/gms/measurement/internal/jg;->a(Lcom/google/android/gms/measurement/internal/f;)V

    .line 242
    iget-wide v9, v8, Lcom/google/android/gms/measurement/internal/f;->aDG:J

    move-object/from16 v11, v40

    .line 244
    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    if-nez v1, :cond_2f

    .line 246
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v1

    invoke-virtual {v1, v7, v0}, Lcom/google/android/gms/measurement/internal/jg;->Q(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    if-nez v1, :cond_2e

    .line 248
    new-instance v1, Landroidx/collection/ArrayMap;

    invoke-direct {v1}, Landroidx/collection/ArrayMap;-><init>()V

    .line 249
    :cond_2e
    invoke-interface {v11, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2f
    move-object v12, v1

    .line 250
    invoke-interface {v12}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_27
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4b

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v14

    .line 251
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v15, v47

    invoke-interface {v15, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_30

    .line 252
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 17022
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    .line 252
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v5, v2}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    move-object/from16 v47, v15

    goto :goto_27

    .line 254
    :cond_30
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v3, v46

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/BitSet;

    .line 255
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-wide/from16 v16, v9

    move-object/from16 v9, v42

    invoke-interface {v9, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/BitSet;

    if-eqz v25, :cond_31

    .line 260
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    move-object/from16 v40, v11

    move-object/from16 v11, v43

    invoke-interface {v11, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/Map;

    move-object/from16 v18, v1

    .line 262
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    move-object/from16 v19, v1

    goto :goto_28

    :cond_31
    move-object/from16 v18, v1

    move-object/from16 v40, v11

    move-object/from16 v11, v43

    const/4 v10, 0x0

    const/16 v19, 0x0

    .line 263
    :goto_28
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v20, v10

    move-object/from16 v10, v45

    invoke-interface {v10, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/measurement/al$a;

    if-nez v1, :cond_33

    .line 266
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/internal/measurement/al$a;->pc()Lcom/google/android/gms/internal/measurement/al$a$a;

    move-result-object v2

    move-object/from16 v21, v13

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Lcom/google/android/gms/internal/measurement/al$a$a;->H(Z)Lcom/google/android/gms/internal/measurement/al$a$a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/internal/measurement/dr$a;->si()Lcom/google/android/gms/internal/measurement/fb;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/internal/measurement/dr;

    check-cast v2, Lcom/google/android/gms/internal/measurement/al$a;

    .line 267
    invoke-interface {v10, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    new-instance v1, Ljava/util/BitSet;

    invoke-direct {v1}, Ljava/util/BitSet;-><init>()V

    .line 269
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    new-instance v2, Ljava/util/BitSet;

    invoke-direct {v2}, Ljava/util/BitSet;-><init>()V

    .line 271
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-interface {v9, v13, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v25, :cond_32

    .line 273
    new-instance v13, Landroidx/collection/ArrayMap;

    invoke-direct {v13}, Landroidx/collection/ArrayMap;-><init>()V

    move-object/from16 v18, v1

    .line 275
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 276
    invoke-interface {v11, v1, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    new-instance v1, Landroidx/collection/ArrayMap;

    invoke-direct {v1}, Landroidx/collection/ArrayMap;-><init>()V

    move-object/from16 v22, v2

    .line 279
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 280
    invoke-interface {v4, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v45, v10

    move-object/from16 v2, v22

    move-object v10, v1

    move-object/from16 v68, v18

    move-object/from16 v18, v13

    move-object/from16 v13, v68

    goto :goto_2a

    :cond_32
    move-object/from16 v18, v1

    move-object/from16 v22, v2

    goto :goto_29

    :cond_33
    move-object/from16 v21, v13

    :goto_29
    move-object/from16 v45, v10

    move-object/from16 v13, v18

    move-object/from16 v10, v19

    move-object/from16 v18, v20

    .line 281
    :goto_2a
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v12, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 282
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_2b
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4a

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/measurement/ad$a;

    if-eqz v28, :cond_34

    if-eqz v27, :cond_34

    move-object/from16 v20, v2

    .line 17029
    iget-boolean v2, v1, Lcom/google/android/gms/internal/measurement/ad$a;->zzum:Z

    move-object/from16 v46, v3

    if-eqz v2, :cond_35

    .line 286
    iget-wide v2, v8, Lcom/google/android/gms/measurement/internal/f;->aDI:J

    move-wide/from16 v22, v2

    goto :goto_2c

    :cond_34
    move-object/from16 v20, v2

    move-object/from16 v46, v3

    :cond_35
    move-wide/from16 v22, v16

    .line 287
    :goto_2c
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/google/android/gms/measurement/internal/dh;->I(I)Z

    move-result v2

    if-eqz v2, :cond_37

    .line 288
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v2

    .line 18022
    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    .line 290
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 291
    invoke-virtual {v1}, Lcom/google/android/gms/internal/measurement/ad$a;->ox()Z

    move-result v24

    if-eqz v24, :cond_36

    move-object/from16 v24, v4

    .line 19006
    iget v4, v1, Lcom/google/android/gms/internal/measurement/ad$a;->zzuf:I

    .line 291
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v36, v5

    goto :goto_2d

    :cond_36
    move-object/from16 v24, v4

    move-object/from16 v36, v5

    const/4 v4, 0x0

    .line 292
    :goto_2d
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object v5

    move-object/from16 v37, v6

    .line 19007
    iget-object v6, v1, Lcom/google/android/gms/internal/measurement/ad$a;->zzug:Ljava/lang/String;

    .line 292
    invoke-virtual {v5, v6}, Lcom/google/android/gms/measurement/internal/df;->cf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "Evaluating filter. audience, filter, event"

    .line 293
    invoke-virtual {v2, v6, v3, v4, v5}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 294
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v2

    .line 19022
    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    .line 296
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xK()Lcom/google/android/gms/measurement/internal/ip;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/android/gms/measurement/internal/ip;->a(Lcom/google/android/gms/internal/measurement/ad$a;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v5, v44

    invoke-virtual {v2, v5, v3}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_2e

    :cond_37
    move-object/from16 v24, v4

    move-object/from16 v36, v5

    move-object/from16 v37, v6

    move-object/from16 v5, v44

    .line 297
    :goto_2e
    invoke-virtual {v1}, Lcom/google/android/gms/internal/measurement/ad$a;->ox()Z

    move-result v2

    if-eqz v2, :cond_47

    .line 20006
    iget v2, v1, Lcom/google/android/gms/internal/measurement/ad$a;->zzuf:I

    const/16 v6, 0x100

    if-le v2, v6, :cond_38

    goto/16 :goto_38

    :cond_38
    const-string v4, "Event filter result"

    if-eqz v25, :cond_42

    .line 21026
    iget-boolean v2, v1, Lcom/google/android/gms/internal/measurement/ad$a;->zzuk:Z

    .line 21027
    iget-boolean v3, v1, Lcom/google/android/gms/internal/measurement/ad$a;->zzul:Z

    if-eqz v27, :cond_39

    .line 21029
    iget-boolean v6, v1, Lcom/google/android/gms/internal/measurement/ad$a;->zzum:Z

    if-eqz v6, :cond_39

    const/4 v6, 0x1

    goto :goto_2f

    :cond_39
    const/4 v6, 0x0

    :goto_2f
    if-nez v2, :cond_3b

    if-nez v3, :cond_3b

    if-eqz v6, :cond_3a

    goto :goto_30

    :cond_3a
    const/16 v42, 0x0

    goto :goto_31

    :cond_3b
    :goto_30
    const/16 v42, 0x1

    .line 22006
    :goto_31
    iget v2, v1, Lcom/google/android/gms/internal/measurement/ad$a;->zzuf:I

    .line 310
    invoke-virtual {v13, v2}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-eqz v2, :cond_3d

    if-nez v42, :cond_3d

    .line 311
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v2

    .line 22022
    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    .line 313
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 314
    invoke-virtual {v1}, Lcom/google/android/gms/internal/measurement/ad$a;->ox()Z

    move-result v4

    if-eqz v4, :cond_3c

    .line 23006
    iget v1, v1, Lcom/google/android/gms/internal/measurement/ad$a;->zzuf:I

    .line 314
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_32

    :cond_3c
    const/4 v1, 0x0

    :goto_32
    const-string v4, "Event filter already evaluated true and it is not associated with an enhanced audience. audience ID, filter ID"

    .line 315
    invoke-virtual {v2, v4, v3, v1}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    move-object/from16 v44, v5

    move-object/from16 v2, v20

    move-object/from16 v4, v24

    move-object/from16 v5, v36

    move-object/from16 v6, v37

    move-object/from16 v3, v46

    goto/16 :goto_2b

    :cond_3d
    move-object v6, v1

    move-object/from16 v2, v18

    move-object/from16 v1, p0

    move-object/from16 v18, v8

    move-object/from16 v8, v20

    move-object/from16 v20, v12

    move-object v12, v2

    move-object v2, v6

    move-object/from16 v43, v11

    move-object/from16 v44, v37

    move-object/from16 v11, v46

    move/from16 v37, v3

    move-object v3, v0

    move-object/from16 v65, v24

    move-object/from16 v24, v9

    move-object v9, v4

    move-object/from16 v4, v41

    move-object/from16 v66, v5

    move-object/from16 v67, v36

    move-object v11, v6

    move-wide/from16 v5, v22

    .line 317
    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/measurement/internal/iz;->a(Lcom/google/android/gms/internal/measurement/ad$a;Ljava/lang/String;Ljava/util/List;J)Ljava/lang/Boolean;

    move-result-object v1

    .line 318
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v2

    .line 23022
    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    if-nez v1, :cond_3e

    move-object/from16 v3, v30

    goto :goto_33

    :cond_3e
    move-object v3, v1

    .line 320
    :goto_33
    invoke-virtual {v2, v9, v3}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    if-nez v1, :cond_3f

    .line 322
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v15, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_34

    .line 24006
    :cond_3f
    iget v2, v11, Lcom/google/android/gms/internal/measurement/ad$a;->zzuf:I

    .line 324
    invoke-virtual {v8, v2}, Ljava/util/BitSet;->set(I)V

    .line 325
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_41

    .line 25006
    iget v1, v11, Lcom/google/android/gms/internal/measurement/ad$a;->zzuf:I

    .line 326
    invoke-virtual {v13, v1}, Ljava/util/BitSet;->set(I)V

    if-eqz v42, :cond_41

    .line 327
    invoke-virtual/range {v44 .. v44}, Lcom/google/android/gms/internal/measurement/al$c;->pl()Z

    move-result v1

    if-eqz v1, :cond_41

    if-eqz v37, :cond_40

    .line 26006
    iget v1, v11, Lcom/google/android/gms/internal/measurement/ad$a;->zzuf:I

    move-object/from16 v5, v44

    .line 26038
    iget-wide v2, v5, Lcom/google/android/gms/internal/measurement/al$c;->zzwl:J

    .line 332
    invoke-static {v10, v1, v2, v3}, Lcom/google/android/gms/measurement/internal/iz;->b(Ljava/util/Map;IJ)V

    goto :goto_36

    :cond_40
    move-object/from16 v5, v44

    .line 27006
    iget v1, v11, Lcom/google/android/gms/internal/measurement/ad$a;->zzuf:I

    .line 27038
    iget-wide v2, v5, Lcom/google/android/gms/internal/measurement/al$c;->zzwl:J

    .line 336
    invoke-static {v12, v1, v2, v3}, Lcom/google/android/gms/measurement/internal/iz;->a(Ljava/util/Map;IJ)V

    goto :goto_36

    :cond_41
    :goto_34
    move-object v2, v8

    move-object/from16 v8, v18

    move-object/from16 v9, v24

    move-object/from16 v11, v43

    move-object/from16 v6, v44

    goto/16 :goto_3c

    :cond_42
    move-object/from16 v66, v5

    move-object/from16 v43, v11

    move-object/from16 v65, v24

    move-object/from16 v67, v36

    move-object/from16 v5, v37

    move-object v11, v1

    move-object/from16 v24, v9

    move-object v9, v4

    move-object/from16 v68, v18

    move-object/from16 v18, v8

    move-object/from16 v8, v20

    move-object/from16 v20, v12

    move-object/from16 v12, v68

    .line 28006
    iget v1, v11, Lcom/google/android/gms/internal/measurement/ad$a;->zzuf:I

    .line 338
    invoke-virtual {v13, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v1

    if-eqz v1, :cond_44

    .line 339
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 28022
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    .line 341
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 342
    invoke-virtual {v11}, Lcom/google/android/gms/internal/measurement/ad$a;->ox()Z

    move-result v3

    if-eqz v3, :cond_43

    .line 29006
    iget v3, v11, Lcom/google/android/gms/internal/measurement/ad$a;->zzuf:I

    .line 342
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_35

    :cond_43
    const/4 v3, 0x0

    :goto_35
    const-string v4, "Event filter already evaluated true. audience ID, filter ID"

    .line 343
    invoke-virtual {v1, v4, v2, v3}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_36
    move-object v6, v5

    move-object v2, v8

    move-object/from16 v8, v18

    move-object/from16 v9, v24

    goto/16 :goto_3b

    :cond_44
    move-object/from16 v1, p0

    move-object v2, v11

    move-object v3, v0

    move-object/from16 v4, v41

    move-object/from16 v36, v5

    move-wide/from16 v5, v22

    .line 345
    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/measurement/internal/iz;->a(Lcom/google/android/gms/internal/measurement/ad$a;Ljava/lang/String;Ljava/util/List;J)Ljava/lang/Boolean;

    move-result-object v1

    .line 346
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v2

    .line 29022
    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    if-nez v1, :cond_45

    move-object/from16 v3, v30

    goto :goto_37

    :cond_45
    move-object v3, v1

    .line 348
    :goto_37
    invoke-virtual {v2, v9, v3}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    if-nez v1, :cond_46

    .line 350
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v15, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3a

    .line 30006
    :cond_46
    iget v2, v11, Lcom/google/android/gms/internal/measurement/ad$a;->zzuf:I

    .line 352
    invoke-virtual {v8, v2}, Ljava/util/BitSet;->set(I)V

    .line 353
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_49

    .line 31006
    iget v1, v11, Lcom/google/android/gms/internal/measurement/ad$a;->zzuf:I

    .line 354
    invoke-virtual {v13, v1}, Ljava/util/BitSet;->set(I)V

    goto :goto_3a

    :cond_47
    :goto_38
    move-object/from16 v66, v5

    move-object/from16 v43, v11

    move-object/from16 v65, v24

    move-object/from16 v67, v36

    move-object/from16 v36, v37

    move-object v11, v1

    move-object/from16 v24, v9

    move-object/from16 v68, v18

    move-object/from16 v18, v8

    move-object/from16 v8, v20

    move-object/from16 v20, v12

    move-object/from16 v12, v68

    .line 298
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 20017
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    .line 300
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/measurement/internal/dh;->ci(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 301
    invoke-virtual {v11}, Lcom/google/android/gms/internal/measurement/ad$a;->ox()Z

    move-result v3

    if-eqz v3, :cond_48

    .line 21006
    iget v3, v11, Lcom/google/android/gms/internal/measurement/ad$a;->zzuf:I

    .line 301
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_39

    :cond_48
    const/4 v3, 0x0

    :goto_39
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "Invalid event filter ID. appId, id"

    .line 302
    invoke-virtual {v1, v4, v2, v3}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_49
    :goto_3a
    move-object v2, v8

    move-object/from16 v8, v18

    move-object/from16 v9, v24

    move-object/from16 v6, v36

    :goto_3b
    move-object/from16 v11, v43

    :goto_3c
    move-object/from16 v3, v46

    move-object/from16 v4, v65

    move-object/from16 v44, v66

    move-object/from16 v5, v67

    move-object/from16 v18, v12

    move-object/from16 v12, v20

    goto/16 :goto_2b

    :cond_4a
    move-object/from16 v46, v3

    move-object/from16 v42, v9

    move-object/from16 v43, v11

    move-object/from16 v47, v15

    move-wide/from16 v9, v16

    move-object/from16 v13, v21

    move-object/from16 v11, v40

    goto/16 :goto_27

    :cond_4b
    move-object v10, v4

    move-object v8, v5

    move-object v9, v7

    move-object v6, v11

    move-wide/from16 v2, v34

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    move-object/from16 v21, v42

    move-object/from16 v22, v43

    move-object/from16 v7, v44

    move-object/from16 v13, v45

    move-object/from16 v14, v46

    move-object/from16 v15, v47

    goto/16 :goto_14

    :cond_4c
    move-object/from16 v66, v7

    move-object/from16 v67, v8

    move-object v7, v9

    move-object/from16 v65, v10

    move-object/from16 v45, v13

    move-object/from16 v46, v14

    move-object/from16 v24, v21

    move-object/from16 v43, v22

    .line 358
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6d

    .line 359
    new-instance v0, Landroidx/collection/ArrayMap;

    invoke-direct {v0}, Landroidx/collection/ArrayMap;-><init>()V

    .line 360
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/internal/measurement/al$k;

    .line 31010
    iget-object v3, v2, Lcom/google/android/gms/internal/measurement/al$k;->zzwk:Ljava/lang/String;

    .line 362
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    if-nez v3, :cond_4e

    .line 364
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v3

    .line 32010
    iget-object v4, v2, Lcom/google/android/gms/internal/measurement/al$k;->zzwk:Ljava/lang/String;

    .line 364
    invoke-virtual {v3, v7, v4}, Lcom/google/android/gms/measurement/internal/jg;->R(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    if-nez v3, :cond_4d

    .line 366
    new-instance v3, Landroidx/collection/ArrayMap;

    invoke-direct {v3}, Landroidx/collection/ArrayMap;-><init>()V

    .line 33010
    :cond_4d
    iget-object v4, v2, Lcom/google/android/gms/internal/measurement/al$k;->zzwk:Ljava/lang/String;

    .line 367
    invoke-interface {v0, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 368
    :cond_4e
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3e
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6c

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 369
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v15, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4f

    .line 370
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v6

    .line 33022
    iget-object v6, v6, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    .line 370
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v8, v67

    invoke-virtual {v6, v8, v5}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_3e

    :cond_4f
    move-object/from16 v8, v67

    .line 372
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v9, v46

    invoke-interface {v9, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/BitSet;

    .line 373
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    move-object/from16 v11, v24

    invoke-interface {v11, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/BitSet;

    if-eqz v25, :cond_50

    .line 378
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    move-object/from16 v13, v43

    invoke-interface {v13, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/Map;

    .line 380
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    move-object/from16 p2, v1

    move-object/from16 v1, v65

    invoke-interface {v1, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/Map;

    move-object/from16 v16, v0

    goto :goto_3f

    :cond_50
    move-object/from16 p2, v1

    move-object/from16 v13, v43

    move-object/from16 v1, v65

    move-object/from16 v16, v0

    const/4 v12, 0x0

    const/4 v14, 0x0

    .line 381
    :goto_3f
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    move-object/from16 p3, v4

    move-object/from16 v4, v45

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$a;

    if-nez v0, :cond_51

    .line 384
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/internal/measurement/al$a;->pc()Lcom/google/android/gms/internal/measurement/al$a$a;

    move-result-object v6

    const/4 v10, 0x1

    invoke-virtual {v6, v10}, Lcom/google/android/gms/internal/measurement/al$a$a;->H(Z)Lcom/google/android/gms/internal/measurement/al$a$a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/internal/measurement/dr$a;->si()Lcom/google/android/gms/internal/measurement/fb;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/internal/measurement/dr;

    check-cast v6, Lcom/google/android/gms/internal/measurement/al$a;

    .line 385
    invoke-interface {v4, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 386
    new-instance v6, Ljava/util/BitSet;

    invoke-direct {v6}, Ljava/util/BitSet;-><init>()V

    .line 387
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v9, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 388
    new-instance v10, Ljava/util/BitSet;

    invoke-direct {v10}, Ljava/util/BitSet;-><init>()V

    .line 389
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v11, v0, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v25, :cond_51

    .line 391
    new-instance v12, Landroidx/collection/ArrayMap;

    invoke-direct {v12}, Landroidx/collection/ArrayMap;-><init>()V

    .line 393
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 394
    invoke-interface {v13, v0, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 395
    new-instance v14, Landroidx/collection/ArrayMap;

    invoke-direct {v14}, Landroidx/collection/ArrayMap;-><init>()V

    .line 397
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 398
    invoke-interface {v1, v0, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 399
    :cond_51
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 400
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_40
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_6b

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    check-cast v0, Lcom/google/android/gms/internal/measurement/ad$d;

    move-object/from16 v17, v3

    .line 401
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v3

    move-object/from16 v24, v8

    const/4 v8, 0x2

    invoke-virtual {v3, v8}, Lcom/google/android/gms/measurement/internal/dh;->I(I)Z

    move-result v3

    if-eqz v3, :cond_53

    .line 402
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v3

    .line 34022
    iget-object v3, v3, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    .line 404
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 405
    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/ad$d;->ox()Z

    move-result v19

    if-eqz v19, :cond_52

    .line 35005
    iget v7, v0, Lcom/google/android/gms/internal/measurement/ad$d;->zzuf:I

    .line 405
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v65, v1

    goto :goto_41

    :cond_52
    move-object/from16 v65, v1

    const/4 v7, 0x0

    .line 406
    :goto_41
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object v1

    move-object/from16 v43, v13

    .line 35006
    iget-object v13, v0, Lcom/google/android/gms/internal/measurement/ad$d;->zzvh:Ljava/lang/String;

    .line 406
    invoke-virtual {v1, v13}, Lcom/google/android/gms/measurement/internal/df;->ch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v13, "Evaluating filter. audience, filter, property"

    .line 407
    invoke-virtual {v3, v13, v8, v7, v1}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 408
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 35022
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    .line 410
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xK()Lcom/google/android/gms/measurement/internal/ip;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/gms/measurement/internal/ip;->a(Lcom/google/android/gms/internal/measurement/ad$d;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v7, v66

    invoke-virtual {v1, v7, v3}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_42

    :cond_53
    move-object/from16 v65, v1

    move-object/from16 v43, v13

    move-object/from16 v7, v66

    .line 411
    :goto_42
    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/ad$d;->ox()Z

    move-result v1

    if-eqz v1, :cond_69

    .line 36005
    iget v1, v0, Lcom/google/android/gms/internal/measurement/ad$d;->zzuf:I

    const/16 v3, 0x100

    if-le v1, v3, :cond_54

    goto/16 :goto_4d

    :cond_54
    const-string v1, "Property filter result"

    if-eqz v25, :cond_63

    .line 37013
    iget-boolean v8, v0, Lcom/google/android/gms/internal/measurement/ad$d;->zzuk:Z

    .line 37014
    iget-boolean v13, v0, Lcom/google/android/gms/internal/measurement/ad$d;->zzul:Z

    if-eqz v27, :cond_55

    .line 37016
    iget-boolean v3, v0, Lcom/google/android/gms/internal/measurement/ad$d;->zzum:Z

    if-eqz v3, :cond_55

    const/4 v3, 0x1

    goto :goto_43

    :cond_55
    const/4 v3, 0x0

    :goto_43
    if-nez v8, :cond_57

    if-nez v13, :cond_57

    if-eqz v3, :cond_56

    goto :goto_44

    :cond_56
    move-object/from16 v44, v7

    const/4 v8, 0x0

    goto :goto_45

    :cond_57
    :goto_44
    move-object/from16 v44, v7

    const/4 v8, 0x1

    .line 38005
    :goto_45
    iget v7, v0, Lcom/google/android/gms/internal/measurement/ad$d;->zzuf:I

    .line 425
    invoke-virtual {v6, v7}, Ljava/util/BitSet;->get(I)Z

    move-result v7

    if-eqz v7, :cond_59

    if-nez v8, :cond_59

    .line 426
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 38022
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    .line 428
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 429
    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/ad$d;->ox()Z

    move-result v7

    if-eqz v7, :cond_58

    .line 39005
    iget v0, v0, Lcom/google/android/gms/internal/measurement/ad$d;->zzuf:I

    .line 429
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    goto :goto_46

    :cond_58
    const/4 v8, 0x0

    :goto_46
    const-string v0, "Property filter already evaluated true and it is not associated with an enhanced audience. audience ID, filter ID"

    .line 430
    invoke-virtual {v1, v0, v3, v8}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    move-object/from16 v7, p1

    move-object/from16 v3, v17

    move-object/from16 v0, v18

    move-object/from16 v8, v24

    move-object/from16 v13, v43

    move-object/from16 v66, v44

    goto/16 :goto_4b

    :cond_59
    move-object/from16 v7, p0

    .line 432
    invoke-direct {v7, v0, v2}, Lcom/google/android/gms/measurement/internal/iz;->a(Lcom/google/android/gms/internal/measurement/ad$d;Lcom/google/android/gms/internal/measurement/al$k;)Ljava/lang/Boolean;

    move-result-object v19

    move-object/from16 v42, v11

    .line 433
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v11

    .line 39022
    iget-object v11, v11, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    move-object/from16 v45, v4

    if-nez v19, :cond_5a

    move-object/from16 v4, v30

    goto :goto_47

    :cond_5a
    move-object/from16 v4, v19

    .line 435
    :goto_47
    invoke-virtual {v11, v1, v4}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    if-nez v19, :cond_5c

    .line 437
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v15, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_5b
    move-object/from16 v7, p1

    move-object/from16 v3, v17

    move-object/from16 v0, v18

    move-object/from16 v8, v24

    move-object/from16 v11, v42

    move-object/from16 v13, v43

    move-object/from16 v66, v44

    move-object/from16 v4, v45

    goto/16 :goto_4b

    .line 40005
    :cond_5c
    iget v1, v0, Lcom/google/android/gms/internal/measurement/ad$d;->zzuf:I

    .line 439
    invoke-virtual {v10, v1}, Ljava/util/BitSet;->set(I)V

    if-eqz v27, :cond_5d

    if-eqz v3, :cond_5d

    .line 440
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_5b

    :cond_5d
    if-eqz v26, :cond_5f

    .line 41005
    iget v1, v0, Lcom/google/android/gms/internal/measurement/ad$d;->zzuf:I

    .line 442
    invoke-virtual {v6, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v1

    if-eqz v1, :cond_5e

    .line 41013
    iget-boolean v1, v0, Lcom/google/android/gms/internal/measurement/ad$d;->zzuk:Z

    if-eqz v1, :cond_60

    .line 42005
    :cond_5e
    iget v1, v0, Lcom/google/android/gms/internal/measurement/ad$d;->zzuf:I

    .line 443
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v6, v1, v4}, Ljava/util/BitSet;->set(IZ)V

    goto :goto_48

    .line 43005
    :cond_5f
    iget v1, v0, Lcom/google/android/gms/internal/measurement/ad$d;->zzuf:I

    .line 444
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v6, v1, v4}, Ljava/util/BitSet;->set(IZ)V

    .line 445
    :cond_60
    :goto_48
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_5b

    if-eqz v8, :cond_5b

    .line 446
    invoke-virtual {v2}, Lcom/google/android/gms/internal/measurement/al$k;->qp()Z

    move-result v1

    if-eqz v1, :cond_5b

    move-object/from16 v46, v9

    .line 43006
    iget-wide v8, v2, Lcom/google/android/gms/internal/measurement/al$k;->zzzc:J

    if-eqz v27, :cond_61

    if-eqz v3, :cond_61

    if-eqz v29, :cond_61

    .line 449
    invoke-virtual/range {v29 .. v29}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    :cond_61
    if-eqz v13, :cond_62

    .line 44005
    iget v0, v0, Lcom/google/android/gms/internal/measurement/ad$d;->zzuf:I

    .line 453
    invoke-static {v14, v0, v8, v9}, Lcom/google/android/gms/measurement/internal/iz;->b(Ljava/util/Map;IJ)V

    goto :goto_4a

    .line 45005
    :cond_62
    iget v0, v0, Lcom/google/android/gms/internal/measurement/ad$d;->zzuf:I

    .line 456
    invoke-static {v12, v0, v8, v9}, Lcom/google/android/gms/measurement/internal/iz;->a(Ljava/util/Map;IJ)V

    goto :goto_4a

    :cond_63
    move-object/from16 v45, v4

    move-object/from16 v44, v7

    move-object/from16 v46, v9

    move-object/from16 v42, v11

    move-object/from16 v7, p0

    .line 46005
    iget v3, v0, Lcom/google/android/gms/internal/measurement/ad$d;->zzuf:I

    .line 458
    invoke-virtual {v6, v3}, Ljava/util/BitSet;->get(I)Z

    move-result v3

    if-eqz v3, :cond_66

    .line 459
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 46022
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    .line 461
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 462
    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/ad$d;->ox()Z

    move-result v4

    if-eqz v4, :cond_64

    .line 47005
    iget v0, v0, Lcom/google/android/gms/internal/measurement/ad$d;->zzuf:I

    .line 462
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    goto :goto_49

    :cond_64
    const/4 v8, 0x0

    :goto_49
    const-string v0, "Property filter already evaluated true. audience ID, filter ID"

    .line 463
    invoke-virtual {v1, v0, v3, v8}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_65
    :goto_4a
    move-object/from16 v7, p1

    move-object/from16 v3, v17

    move-object/from16 v0, v18

    move-object/from16 v8, v24

    move-object/from16 v11, v42

    move-object/from16 v13, v43

    move-object/from16 v66, v44

    move-object/from16 v4, v45

    move-object/from16 v9, v46

    :goto_4b
    move-object/from16 v1, v65

    goto/16 :goto_40

    .line 465
    :cond_66
    invoke-direct {v7, v0, v2}, Lcom/google/android/gms/measurement/internal/iz;->a(Lcom/google/android/gms/internal/measurement/ad$d;Lcom/google/android/gms/internal/measurement/al$k;)Ljava/lang/Boolean;

    move-result-object v3

    .line 466
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v4

    .line 47022
    iget-object v4, v4, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    if-nez v3, :cond_67

    move-object/from16 v8, v30

    goto :goto_4c

    :cond_67
    move-object v8, v3

    .line 468
    :goto_4c
    invoke-virtual {v4, v1, v8}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    if-nez v3, :cond_68

    .line 470
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v15, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_4a

    .line 48005
    :cond_68
    iget v1, v0, Lcom/google/android/gms/internal/measurement/ad$d;->zzuf:I

    .line 472
    invoke-virtual {v10, v1}, Ljava/util/BitSet;->set(I)V

    .line 473
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_65

    .line 49005
    iget v0, v0, Lcom/google/android/gms/internal/measurement/ad$d;->zzuf:I

    .line 474
    invoke-virtual {v6, v0}, Ljava/util/BitSet;->set(I)V

    goto :goto_4a

    :cond_69
    :goto_4d
    move-object/from16 v45, v4

    move-object/from16 v44, v7

    move-object/from16 v46, v9

    move-object/from16 v42, v11

    move-object/from16 v7, p0

    .line 412
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 36017
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    .line 414
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/measurement/internal/dh;->ci(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 415
    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/ad$d;->ox()Z

    move-result v4

    if-eqz v4, :cond_6a

    .line 37005
    iget v0, v0, Lcom/google/android/gms/internal/measurement/ad$d;->zzuf:I

    .line 415
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    goto :goto_4e

    :cond_6a
    const/4 v8, 0x0

    :goto_4e
    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "Invalid property filter ID. appId, id"

    .line 416
    invoke-virtual {v1, v4, v3, v0}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 417
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v15, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v7, p1

    move-object/from16 v1, p2

    move-object/from16 v4, p3

    move-object/from16 v0, v16

    move-object/from16 v3, v17

    move-object/from16 v67, v24

    move-object/from16 v24, v42

    move-object/from16 v66, v44

    goto/16 :goto_3e

    :cond_6b
    move-object/from16 v7, p0

    move-object/from16 v7, p1

    move-object/from16 v65, v1

    move-object/from16 v45, v4

    move-object/from16 v67, v8

    move-object/from16 v46, v9

    move-object/from16 v24, v11

    move-object/from16 v43, v13

    move-object/from16 v0, v16

    move-object/from16 v1, p2

    move-object/from16 v4, p3

    goto/16 :goto_3e

    :cond_6c
    move-object/from16 v7, p0

    move-object/from16 v7, p1

    goto/16 :goto_3d

    :cond_6d
    move-object/from16 v7, p0

    move-object/from16 v42, v24

    .line 478
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 479
    invoke-interface/range {v46 .. v46}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 480
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v15, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7d

    .line 481
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v4, v45

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/internal/measurement/al$a;

    if-nez v3, :cond_6e

    .line 483
    invoke-static {}, Lcom/google/android/gms/internal/measurement/al$a;->pc()Lcom/google/android/gms/internal/measurement/al$a$a;

    move-result-object v3

    goto :goto_50

    .line 485
    :cond_6e
    invoke-virtual {v3}, Lcom/google/android/gms/internal/measurement/dr;->rZ()Lcom/google/android/gms/internal/measurement/dr$a;

    move-result-object v3

    .line 486
    check-cast v3, Lcom/google/android/gms/internal/measurement/dr$a;

    check-cast v3, Lcom/google/android/gms/internal/measurement/al$a$a;

    .line 487
    :goto_50
    invoke-virtual {v3, v0}, Lcom/google/android/gms/internal/measurement/al$a$a;->br(I)Lcom/google/android/gms/internal/measurement/al$a$a;

    .line 488
    invoke-static {}, Lcom/google/android/gms/internal/measurement/al$i;->qf()Lcom/google/android/gms/internal/measurement/al$i$a;

    move-result-object v5

    .line 489
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v8, v46

    invoke-interface {v8, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/BitSet;

    invoke-static {v6}, Lcom/google/android/gms/measurement/internal/ip;->a(Ljava/util/BitSet;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/gms/internal/measurement/al$i$a;->g(Ljava/lang/Iterable;)Lcom/google/android/gms/internal/measurement/al$i$a;

    move-result-object v5

    .line 490
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v9, v42

    invoke-interface {v9, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/BitSet;

    invoke-static {v6}, Lcom/google/android/gms/measurement/internal/ip;->a(Ljava/util/BitSet;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/gms/internal/measurement/al$i$a;->f(Ljava/lang/Iterable;)Lcom/google/android/gms/internal/measurement/al$i$a;

    move-result-object v5

    if-eqz v25, :cond_7b

    .line 493
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v10, v43

    invoke-interface {v10, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map;

    .line 494
    invoke-static {v6}, Lcom/google/android/gms/measurement/internal/iz;->o(Ljava/util/Map;)Ljava/util/List;

    move-result-object v6

    .line 495
    invoke-virtual {v5, v6}, Lcom/google/android/gms/internal/measurement/al$i$a;->h(Ljava/lang/Iterable;)Lcom/google/android/gms/internal/measurement/al$i$a;

    .line 497
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v11, v65

    invoke-interface {v11, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map;

    if-nez v6, :cond_6f

    .line 500
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    move-object/from16 p2, v2

    move-object v12, v6

    goto/16 :goto_53

    .line 501
    :cond_6f
    new-instance v12, Ljava/util/ArrayList;

    .line 502
    invoke-interface {v6}, Ljava/util/Map;->size()I

    move-result v13

    invoke-direct {v12, v13}, Ljava/util/ArrayList;-><init>(I)V

    .line 503
    invoke-interface {v6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_51
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_71

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Integer;

    move-object/from16 p2, v2

    .line 504
    invoke-static {}, Lcom/google/android/gms/internal/measurement/al$j;->qm()Lcom/google/android/gms/internal/measurement/al$j$a;

    move-result-object v2

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v2, v7}, Lcom/google/android/gms/internal/measurement/al$j$a;->bG(I)Lcom/google/android/gms/internal/measurement/al$j$a;

    move-result-object v2

    .line 505
    invoke-interface {v6, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/List;

    if-eqz v7, :cond_70

    .line 507
    invoke-static {v7}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 508
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_52
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_70

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Long;

    move-object/from16 p3, v6

    move-object/from16 v16, v7

    .line 509
    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Lcom/google/android/gms/internal/measurement/al$j$a;->Q(J)Lcom/google/android/gms/internal/measurement/al$j$a;

    move-object/from16 v6, p3

    move-object/from16 v7, v16

    goto :goto_52

    :cond_70
    move-object/from16 p3, v6

    .line 511
    invoke-virtual {v2}, Lcom/google/android/gms/internal/measurement/dr$a;->si()Lcom/google/android/gms/internal/measurement/fb;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/internal/measurement/dr;

    check-cast v2, Lcom/google/android/gms/internal/measurement/al$j;

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v7, p0

    move-object/from16 v2, p2

    move-object/from16 v6, p3

    goto :goto_51

    :cond_71
    move-object/from16 p2, v2

    :goto_53
    if-eqz v26, :cond_72

    .line 515
    invoke-virtual {v3}, Lcom/google/android/gms/internal/measurement/al$a$a;->pe()Z

    move-result v2

    if-eqz v2, :cond_72

    .line 517
    invoke-virtual {v3}, Lcom/google/android/gms/internal/measurement/al$a$a;->pb()Lcom/google/android/gms/internal/measurement/al$i;

    move-result-object v2

    .line 49040
    iget-object v2, v2, Lcom/google/android/gms/internal/measurement/al$i;->zzyy:Lcom/google/android/gms/internal/measurement/dz;

    .line 519
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_73

    :cond_72
    move-object/from16 v46, v8

    move-object/from16 v42, v9

    const/16 v16, 0x1

    goto/16 :goto_59

    .line 521
    :cond_73
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v12}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 522
    new-instance v7, Landroidx/collection/ArrayMap;

    invoke-direct {v7}, Landroidx/collection/ArrayMap;-><init>()V

    .line 523
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_54
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_75

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/gms/internal/measurement/al$j;

    .line 524
    invoke-virtual {v12}, Lcom/google/android/gms/internal/measurement/al$j;->pf()Z

    move-result v13

    if-eqz v13, :cond_74

    .line 525
    invoke-virtual {v12}, Lcom/google/android/gms/internal/measurement/al$j;->qk()I

    move-result v13

    if-lez v13, :cond_74

    .line 50005
    iget v13, v12, Lcom/google/android/gms/internal/measurement/al$j;->zzwg:I

    .line 527
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    .line 528
    invoke-virtual {v12}, Lcom/google/android/gms/internal/measurement/al$j;->qk()I

    move-result v14

    const/16 v16, 0x1

    add-int/lit8 v14, v14, -0x1

    .line 529
    invoke-virtual {v12, v14}, Lcom/google/android/gms/internal/measurement/al$j;->bF(I)J

    move-result-wide v17

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    .line 530
    invoke-interface {v7, v13, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_54

    :cond_74
    const/16 v16, 0x1

    goto :goto_54

    :cond_75
    const/16 v16, 0x1

    const/4 v2, 0x0

    .line 532
    :goto_55
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v12

    if-ge v2, v12, :cond_79

    .line 533
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/gms/internal/measurement/al$j;

    .line 535
    invoke-virtual {v12}, Lcom/google/android/gms/internal/measurement/al$j;->pf()Z

    move-result v13

    if-eqz v13, :cond_76

    .line 50006
    iget v13, v12, Lcom/google/android/gms/internal/measurement/al$j;->zzwg:I

    .line 536
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    goto :goto_56

    :cond_76
    const/4 v13, 0x0

    .line 538
    :goto_56
    invoke-interface {v7, v13}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Long;

    if-eqz v13, :cond_78

    .line 540
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 541
    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v17

    move-object/from16 v46, v8

    const/4 v8, 0x0

    invoke-virtual {v12, v8}, Lcom/google/android/gms/internal/measurement/al$j;->bF(I)J

    move-result-wide v19

    cmp-long v21, v17, v19

    if-gez v21, :cond_77

    .line 542
    invoke-interface {v14, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50007
    :cond_77
    iget-object v13, v12, Lcom/google/android/gms/internal/measurement/al$j;->zzza:Lcom/google/android/gms/internal/measurement/ea;

    .line 543
    invoke-interface {v14, v13}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 545
    invoke-virtual {v12}, Lcom/google/android/gms/internal/measurement/dr;->rZ()Lcom/google/android/gms/internal/measurement/dr$a;

    move-result-object v12

    .line 546
    check-cast v12, Lcom/google/android/gms/internal/measurement/dr$a;

    check-cast v12, Lcom/google/android/gms/internal/measurement/al$j$a;

    .line 547
    invoke-virtual {v12}, Lcom/google/android/gms/internal/measurement/al$j$a;->qo()Lcom/google/android/gms/internal/measurement/al$j$a;

    move-result-object v12

    .line 548
    invoke-virtual {v12, v14}, Lcom/google/android/gms/internal/measurement/al$j$a;->j(Ljava/lang/Iterable;)Lcom/google/android/gms/internal/measurement/al$j$a;

    move-result-object v12

    .line 549
    invoke-virtual {v12}, Lcom/google/android/gms/internal/measurement/dr$a;->si()Lcom/google/android/gms/internal/measurement/fb;

    move-result-object v12

    check-cast v12, Lcom/google/android/gms/internal/measurement/dr;

    check-cast v12, Lcom/google/android/gms/internal/measurement/al$j;

    .line 550
    invoke-interface {v6, v2, v12}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_57

    :cond_78
    move-object/from16 v46, v8

    const/4 v8, 0x0

    :goto_57
    add-int/lit8 v2, v2, 0x1

    move-object/from16 v8, v46

    goto :goto_55

    :cond_79
    move-object/from16 v46, v8

    const/4 v8, 0x0

    .line 552
    invoke-interface {v7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_58
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_7a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    .line 554
    invoke-static {}, Lcom/google/android/gms/internal/measurement/al$j;->qm()Lcom/google/android/gms/internal/measurement/al$j$a;

    move-result-object v13

    .line 555
    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v14

    invoke-virtual {v13, v14}, Lcom/google/android/gms/internal/measurement/al$j$a;->bG(I)Lcom/google/android/gms/internal/measurement/al$j$a;

    move-result-object v13

    .line 556
    invoke-interface {v7, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Long;

    move-object/from16 v42, v9

    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v13, v8, v9}, Lcom/google/android/gms/internal/measurement/al$j$a;->Q(J)Lcom/google/android/gms/internal/measurement/al$j$a;

    move-result-object v8

    .line 557
    invoke-virtual {v8}, Lcom/google/android/gms/internal/measurement/dr$a;->si()Lcom/google/android/gms/internal/measurement/fb;

    move-result-object v8

    check-cast v8, Lcom/google/android/gms/internal/measurement/dr;

    check-cast v8, Lcom/google/android/gms/internal/measurement/al$j;

    .line 558
    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v9, v42

    const/4 v8, 0x0

    goto :goto_58

    :cond_7a
    move-object/from16 v42, v9

    move-object v12, v6

    .line 562
    :goto_59
    invoke-virtual {v5, v12}, Lcom/google/android/gms/internal/measurement/al$i$a;->i(Ljava/lang/Iterable;)Lcom/google/android/gms/internal/measurement/al$i$a;

    goto :goto_5a

    :cond_7b
    move-object/from16 p2, v2

    move-object/from16 v46, v8

    move-object/from16 v42, v9

    move-object/from16 v10, v43

    move-object/from16 v11, v65

    const/16 v16, 0x1

    .line 563
    :goto_5a
    invoke-virtual {v3, v5}, Lcom/google/android/gms/internal/measurement/al$a$a;->a(Lcom/google/android/gms/internal/measurement/al$i$a;)Lcom/google/android/gms/internal/measurement/al$a$a;

    .line 564
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3}, Lcom/google/android/gms/internal/measurement/dr$a;->si()Lcom/google/android/gms/internal/measurement/fb;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/internal/measurement/dr;

    check-cast v5, Lcom/google/android/gms/internal/measurement/al$a;

    invoke-interface {v4, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 565
    invoke-virtual {v3}, Lcom/google/android/gms/internal/measurement/dr$a;->si()Lcom/google/android/gms/internal/measurement/fb;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/internal/measurement/dr;

    check-cast v2, Lcom/google/android/gms/internal/measurement/al$a;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 566
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v2

    .line 567
    invoke-virtual {v3}, Lcom/google/android/gms/internal/measurement/al$a$a;->pa()Lcom/google/android/gms/internal/measurement/al$i;

    move-result-object v3

    .line 568
    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/ij;->vt()V

    .line 569
    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 570
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/common/internal/r;->aQ(Ljava/lang/String;)Ljava/lang/String;

    .line 571
    invoke-static {v3}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 572
    invoke-virtual {v3}, Lcom/google/android/gms/internal/measurement/bz;->toByteArray()[B

    move-result-object v3

    .line 573
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "app_id"

    move-object/from16 v7, p1

    .line 574
    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 575
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v6, "audience_id"

    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "current_results"

    .line 576
    invoke-virtual {v5, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 577
    :try_start_4
    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/jg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v3, "audience_filter_values"
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_5

    const/4 v6, 0x5

    const/4 v8, 0x0

    .line 579
    :try_start_5
    invoke-virtual {v0, v3, v8, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v5

    const-wide/16 v12, -0x1

    cmp-long v0, v5, v12

    if-nez v0, :cond_7c

    .line 581
    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 50008
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string v3, "Failed to insert filter results (got -1). appId"

    .line 583
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/measurement/internal/dh;->ci(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v0, v3, v5}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_5c

    :catch_4
    move-exception v0

    goto :goto_5b

    :catch_5
    move-exception v0

    const/4 v8, 0x0

    .line 586
    :goto_5b
    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v2

    .line 50009
    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    .line 588
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/measurement/internal/dh;->ci(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    const-string v5, "Error storing filter results. appId"

    invoke-virtual {v2, v5, v3, v0}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_7c
    :goto_5c
    move-object/from16 v7, p0

    move-object/from16 v2, p2

    move-object/from16 v45, v4

    move-object/from16 v43, v10

    move-object/from16 v65, v11

    goto/16 :goto_4f

    :cond_7d
    move-object/from16 v7, p1

    move-object/from16 v7, p0

    goto/16 :goto_4f

    :cond_7e
    return-object v1
.end method

.method protected final vM()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
