.class final Lcom/google/android/gms/measurement/internal/fy;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final synthetic aJy:Ljava/util/concurrent/atomic/AtomicReference;

.field private final synthetic aJz:Lcom/google/android/gms/measurement/internal/fr;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/fr;Ljava/util/concurrent/atomic/AtomicReference;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/fy;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    iput-object p2, p0, Lcom/google/android/gms/measurement/internal/fy;->aJy:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .line 2
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fy;->aJy:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v0

    .line 3
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/fy;->aJy:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/fy;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/measurement/internal/fy;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/cb;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/cz;->pT()Ljava/lang/String;

    move-result-object v3

    .line 1168
    sget-object v4, Lcom/google/android/gms/measurement/internal/j;->aEN:Lcom/google/android/gms/measurement/internal/cv;

    if-nez v3, :cond_0

    const/4 v2, 0x0

    .line 1171
    invoke-virtual {v4, v2}, Lcom/google/android/gms/measurement/internal/cv;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 1172
    check-cast v2, Ljava/lang/String;

    goto :goto_0

    .line 1173
    :cond_0
    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/jb;->aLW:Lcom/google/android/gms/measurement/internal/jd;

    .line 2010
    iget-object v5, v4, Lcom/google/android/gms/measurement/internal/cv;->aFL:Ljava/lang/String;

    .line 1173
    invoke-interface {v2, v3, v5}, Lcom/google/android/gms/measurement/internal/jd;->D(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/google/android/gms/measurement/internal/cv;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 3
    :goto_0
    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/fy;->aJy:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    .line 7
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    .line 6
    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/fy;->aJy:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    throw v1

    :catchall_1
    move-exception v1

    .line 7
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v1
.end method
