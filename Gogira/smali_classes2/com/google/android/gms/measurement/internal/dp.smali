.class final Lcom/google/android/gms/measurement/internal/dp;
.super Lcom/google/android/gms/measurement/internal/fg;


# static fields
.field static final aGO:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field aGP:Landroid/content/SharedPreferences;

.field public aGQ:Lcom/google/android/gms/measurement/internal/ds;

.field public final aGR:Lcom/google/android/gms/measurement/internal/dt;

.field public final aGS:Lcom/google/android/gms/measurement/internal/dt;

.field public final aGT:Lcom/google/android/gms/measurement/internal/dt;

.field public final aGU:Lcom/google/android/gms/measurement/internal/dt;

.field public final aGV:Lcom/google/android/gms/measurement/internal/dt;

.field public final aGW:Lcom/google/android/gms/measurement/internal/dt;

.field public final aGX:Lcom/google/android/gms/measurement/internal/dt;

.field public final aGY:Lcom/google/android/gms/measurement/internal/dv;

.field private aGZ:Ljava/lang/String;

.field private aHa:Z

.field private aHb:J

.field public final aHc:Lcom/google/android/gms/measurement/internal/dt;

.field public final aHd:Lcom/google/android/gms/measurement/internal/dt;

.field public final aHe:Lcom/google/android/gms/measurement/internal/dr;

.field public final aHf:Lcom/google/android/gms/measurement/internal/dv;

.field public final aHg:Lcom/google/android/gms/measurement/internal/dr;

.field public final aHh:Lcom/google/android/gms/measurement/internal/dr;

.field public final aHi:Lcom/google/android/gms/measurement/internal/dt;

.field public final aHj:Lcom/google/android/gms/measurement/internal/dt;

.field public aHk:Z

.field public aHl:Lcom/google/android/gms/measurement/internal/dr;

.field public aHm:Lcom/google/android/gms/measurement/internal/dt;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 145
    new-instance v0, Landroid/util/Pair;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, ""

    invoke-direct {v0, v2, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    sput-object v0, Lcom/google/android/gms/measurement/internal/dp;->aGO:Landroid/util/Pair;

    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/measurement/internal/ek;)V
    .locals 5

    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/gms/measurement/internal/fg;-><init>(Lcom/google/android/gms/measurement/internal/ek;)V

    .line 28
    new-instance p1, Lcom/google/android/gms/measurement/internal/dt;

    const-wide/16 v0, 0x0

    const-string v2, "last_upload"

    invoke-direct {p1, p0, v2, v0, v1}, Lcom/google/android/gms/measurement/internal/dt;-><init>(Lcom/google/android/gms/measurement/internal/dp;Ljava/lang/String;J)V

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/dp;->aGR:Lcom/google/android/gms/measurement/internal/dt;

    .line 29
    new-instance p1, Lcom/google/android/gms/measurement/internal/dt;

    const-string v2, "last_upload_attempt"

    invoke-direct {p1, p0, v2, v0, v1}, Lcom/google/android/gms/measurement/internal/dt;-><init>(Lcom/google/android/gms/measurement/internal/dp;Ljava/lang/String;J)V

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/dp;->aGS:Lcom/google/android/gms/measurement/internal/dt;

    .line 30
    new-instance p1, Lcom/google/android/gms/measurement/internal/dt;

    const-string v2, "backoff"

    invoke-direct {p1, p0, v2, v0, v1}, Lcom/google/android/gms/measurement/internal/dt;-><init>(Lcom/google/android/gms/measurement/internal/dp;Ljava/lang/String;J)V

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/dp;->aGT:Lcom/google/android/gms/measurement/internal/dt;

    .line 31
    new-instance p1, Lcom/google/android/gms/measurement/internal/dt;

    const-string v2, "last_delete_stale"

    invoke-direct {p1, p0, v2, v0, v1}, Lcom/google/android/gms/measurement/internal/dt;-><init>(Lcom/google/android/gms/measurement/internal/dp;Ljava/lang/String;J)V

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/dp;->aGU:Lcom/google/android/gms/measurement/internal/dt;

    .line 32
    new-instance p1, Lcom/google/android/gms/measurement/internal/dt;

    const-string v2, "time_before_start"

    const-wide/16 v3, 0x2710

    invoke-direct {p1, p0, v2, v3, v4}, Lcom/google/android/gms/measurement/internal/dt;-><init>(Lcom/google/android/gms/measurement/internal/dp;Ljava/lang/String;J)V

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/dp;->aHc:Lcom/google/android/gms/measurement/internal/dt;

    .line 33
    new-instance p1, Lcom/google/android/gms/measurement/internal/dt;

    const-string v2, "session_timeout"

    const-wide/32 v3, 0x1b7740

    invoke-direct {p1, p0, v2, v3, v4}, Lcom/google/android/gms/measurement/internal/dt;-><init>(Lcom/google/android/gms/measurement/internal/dp;Ljava/lang/String;J)V

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/dp;->aHd:Lcom/google/android/gms/measurement/internal/dt;

    .line 34
    new-instance p1, Lcom/google/android/gms/measurement/internal/dr;

    const-string v2, "start_new_session"

    const/4 v3, 0x1

    invoke-direct {p1, p0, v2, v3}, Lcom/google/android/gms/measurement/internal/dr;-><init>(Lcom/google/android/gms/measurement/internal/dp;Ljava/lang/String;Z)V

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/dp;->aHe:Lcom/google/android/gms/measurement/internal/dr;

    .line 35
    new-instance p1, Lcom/google/android/gms/measurement/internal/dt;

    const-string v2, "last_pause_time"

    invoke-direct {p1, p0, v2, v0, v1}, Lcom/google/android/gms/measurement/internal/dt;-><init>(Lcom/google/android/gms/measurement/internal/dp;Ljava/lang/String;J)V

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/dp;->aHi:Lcom/google/android/gms/measurement/internal/dt;

    .line 36
    new-instance p1, Lcom/google/android/gms/measurement/internal/dt;

    const-string v2, "time_active"

    invoke-direct {p1, p0, v2, v0, v1}, Lcom/google/android/gms/measurement/internal/dt;-><init>(Lcom/google/android/gms/measurement/internal/dp;Ljava/lang/String;J)V

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/dp;->aHj:Lcom/google/android/gms/measurement/internal/dt;

    .line 37
    new-instance p1, Lcom/google/android/gms/measurement/internal/dv;

    const-string v2, "non_personalized_ads"

    invoke-direct {p1, p0, v2}, Lcom/google/android/gms/measurement/internal/dv;-><init>(Lcom/google/android/gms/measurement/internal/dp;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/dp;->aHf:Lcom/google/android/gms/measurement/internal/dv;

    .line 38
    new-instance p1, Lcom/google/android/gms/measurement/internal/dr;

    const/4 v2, 0x0

    const-string v3, "use_dynamite_api"

    invoke-direct {p1, p0, v3, v2}, Lcom/google/android/gms/measurement/internal/dr;-><init>(Lcom/google/android/gms/measurement/internal/dp;Ljava/lang/String;Z)V

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/dp;->aHg:Lcom/google/android/gms/measurement/internal/dr;

    .line 39
    new-instance p1, Lcom/google/android/gms/measurement/internal/dr;

    const-string v3, "allow_remote_dynamite"

    invoke-direct {p1, p0, v3, v2}, Lcom/google/android/gms/measurement/internal/dr;-><init>(Lcom/google/android/gms/measurement/internal/dp;Ljava/lang/String;Z)V

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/dp;->aHh:Lcom/google/android/gms/measurement/internal/dr;

    .line 40
    new-instance p1, Lcom/google/android/gms/measurement/internal/dt;

    const-string v3, "midnight_offset"

    invoke-direct {p1, p0, v3, v0, v1}, Lcom/google/android/gms/measurement/internal/dt;-><init>(Lcom/google/android/gms/measurement/internal/dp;Ljava/lang/String;J)V

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/dp;->aGV:Lcom/google/android/gms/measurement/internal/dt;

    .line 41
    new-instance p1, Lcom/google/android/gms/measurement/internal/dt;

    const-string v3, "first_open_time"

    invoke-direct {p1, p0, v3, v0, v1}, Lcom/google/android/gms/measurement/internal/dt;-><init>(Lcom/google/android/gms/measurement/internal/dp;Ljava/lang/String;J)V

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/dp;->aGW:Lcom/google/android/gms/measurement/internal/dt;

    .line 42
    new-instance p1, Lcom/google/android/gms/measurement/internal/dt;

    const-string v3, "app_install_time"

    invoke-direct {p1, p0, v3, v0, v1}, Lcom/google/android/gms/measurement/internal/dt;-><init>(Lcom/google/android/gms/measurement/internal/dp;Ljava/lang/String;J)V

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/dp;->aGX:Lcom/google/android/gms/measurement/internal/dt;

    .line 43
    new-instance p1, Lcom/google/android/gms/measurement/internal/dv;

    const-string v0, "app_instance_id"

    invoke-direct {p1, p0, v0}, Lcom/google/android/gms/measurement/internal/dv;-><init>(Lcom/google/android/gms/measurement/internal/dp;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/dp;->aGY:Lcom/google/android/gms/measurement/internal/dv;

    .line 44
    new-instance p1, Lcom/google/android/gms/measurement/internal/dr;

    const-string v0, "app_backgrounded"

    invoke-direct {p1, p0, v0, v2}, Lcom/google/android/gms/measurement/internal/dr;-><init>(Lcom/google/android/gms/measurement/internal/dp;Ljava/lang/String;Z)V

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/dp;->aHl:Lcom/google/android/gms/measurement/internal/dr;

    .line 45
    new-instance p1, Lcom/google/android/gms/measurement/internal/dt;

    const-string v0, "deep_link_last_retrieved"

    const-wide/16 v1, -0x1

    invoke-direct {p1, p0, v0, v1, v2}, Lcom/google/android/gms/measurement/internal/dt;-><init>(Lcom/google/android/gms/measurement/internal/dp;Ljava/lang/String;J)V

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/dp;->aHm:Lcom/google/android/gms/measurement/internal/dt;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/measurement/internal/dp;)Landroid/content/SharedPreferences;
    .locals 0

    .line 144
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/dp;->xP()Landroid/content/SharedPreferences;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method final N(Z)V
    .locals 3

    .line 81
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 82
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 1022
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    .line 82
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "Setting useService"

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    .line 83
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/dp;->xP()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "use_service"

    .line 84
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 85
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method final O(Z)V
    .locals 3

    .line 107
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 108
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 3022
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    .line 108
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "Setting measurementEnabled"

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    .line 109
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/dp;->xP()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "measurement_enabled"

    .line 110
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 111
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method final P(Z)Z
    .locals 2

    .line 113
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 114
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/dp;->xP()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "measurement_enabled"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    return p1
.end method

.method final Q(Z)V
    .locals 3

    .line 130
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 131
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 4022
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    .line 133
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "Updating deferred analytics collection"

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    .line 134
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/dp;->xP()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "deferred_analytics_collection"

    .line 135
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 136
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method final al(J)Z
    .locals 3

    .line 141
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/dp;->aHd:Lcom/google/android/gms/measurement/internal/dt;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/dt;->get()J

    move-result-wide v0

    sub-long/2addr p1, v0

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/dp;->aHi:Lcom/google/android/gms/measurement/internal/dt;

    .line 142
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/dt;->get()J

    move-result-wide v0

    cmp-long v2, p1, v0

    if-lez v2, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method final cl(Ljava/lang/String;)Landroid/util/Pair;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, ""

    .line 1
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 2
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/common/util/e;->elapsedRealtime()J

    move-result-wide v1

    .line 3
    iget-object v3, p0, Lcom/google/android/gms/measurement/internal/dp;->aGZ:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-wide v4, p0, Lcom/google/android/gms/measurement/internal/dp;->aHb:J

    cmp-long v6, v1, v4

    if-gez v6, :cond_0

    .line 4
    new-instance p1, Landroid/util/Pair;

    iget-boolean v0, p0, Lcom/google/android/gms/measurement/internal/dp;->aHa:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p1, v3, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1

    .line 5
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v3

    .line 6
    sget-object v4, Lcom/google/android/gms/measurement/internal/j;->aEg:Lcom/google/android/gms/measurement/internal/cv;

    invoke-virtual {v3, p1, v4}, Lcom/google/android/gms/measurement/internal/jb;->a(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/cv;)J

    move-result-wide v3

    add-long/2addr v1, v3

    .line 7
    iput-wide v1, p0, Lcom/google/android/gms/measurement/internal/dp;->aHb:J

    const/4 p1, 0x1

    .line 8
    invoke-static {p1}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;->setShouldSkipGmsCoreVersionCheck(Z)V

    .line 9
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;->getAdvertisingIdInfo(Landroid/content/Context;)Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 11
    invoke-virtual {p1}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->getId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/measurement/internal/dp;->aGZ:Ljava/lang/String;

    .line 12
    invoke-virtual {p1}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->isLimitAdTrackingEnabled()Z

    move-result p1

    iput-boolean p1, p0, Lcom/google/android/gms/measurement/internal/dp;->aHa:Z

    .line 13
    :cond_1
    iget-object p1, p0, Lcom/google/android/gms/measurement/internal/dp;->aGZ:Ljava/lang/String;

    if-nez p1, :cond_2

    .line 14
    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/dp;->aGZ:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 17
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 1021
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    const-string v2, "Unable to get advertising id"

    .line 17
    invoke-virtual {v1, v2, p1}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    .line 18
    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/dp;->aGZ:Ljava/lang/String;

    :cond_2
    :goto_0
    const/4 p1, 0x0

    .line 19
    invoke-static {p1}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;->setShouldSkipGmsCoreVersionCheck(Z)V

    .line 20
    new-instance p1, Landroid/util/Pair;

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/dp;->aGZ:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/gms/measurement/internal/dp;->aHa:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1
.end method

.method final cm(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .line 21
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 22
    invoke-virtual {p0, p1}, Lcom/google/android/gms/measurement/internal/dp;->cl(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object p1

    iget-object p1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    .line 23
    invoke-static {}, Lcom/google/android/gms/measurement/internal/it;->getMessageDigest()Ljava/security/MessageDigest;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 26
    :cond_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    new-instance v5, Ljava/math/BigInteger;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object p1

    invoke-direct {v5, v2, p1}, Ljava/math/BigInteger;-><init>(I[B)V

    aput-object v5, v3, v4

    const-string p1, "%032X"

    invoke-static {v1, p1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method final cn(Ljava/lang/String;)V
    .locals 2

    .line 63
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 64
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/dp;->xP()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "gmp_app_id"

    .line 65
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 66
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method final co(Ljava/lang/String;)V
    .locals 2

    .line 70
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 71
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/dp;->xP()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "admob_app_id"

    .line 72
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 73
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method protected final vM()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final xA()V
    .locals 9

    .line 48
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "com.google.android.gms.measurement.prefs"

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/dp;->aGP:Landroid/content/SharedPreferences;

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/dp;->aGP:Landroid/content/SharedPreferences;

    const-string v2, "has_been_opened"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/measurement/internal/dp;->aHk:Z

    .line 50
    iget-boolean v0, p0, Lcom/google/android/gms/measurement/internal/dp;->aHk:Z

    if-nez v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/dp;->aGP:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const/4 v1, 0x1

    .line 52
    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 53
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 54
    :cond_0
    new-instance v0, Lcom/google/android/gms/measurement/internal/ds;

    const-wide/16 v1, 0x0

    .line 55
    sget-object v3, Lcom/google/android/gms/measurement/internal/j;->aEh:Lcom/google/android/gms/measurement/internal/cv;

    const/4 v4, 0x0

    .line 56
    invoke-virtual {v3, v4}, Lcom/google/android/gms/measurement/internal/cv;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 57
    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    const/4 v8, 0x0

    const-string v5, "health_monitor"

    move-object v3, v0

    move-object v4, p0

    .line 58
    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/measurement/internal/ds;-><init>(Lcom/google/android/gms/measurement/internal/dp;Ljava/lang/String;JB)V

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/dp;->aGQ:Lcom/google/android/gms/measurement/internal/ds;

    return-void
.end method

.method final xP()Landroid/content/SharedPreferences;
    .locals 1

    .line 60
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 61
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fg;->vt()V

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/dp;->aGP:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method final xQ()Ljava/lang/String;
    .locals 3

    .line 68
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 69
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/dp;->xP()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "gmp_app_id"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final xR()Ljava/lang/String;
    .locals 3

    .line 75
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 76
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/dp;->xP()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "admob_app_id"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final xS()Ljava/lang/Boolean;
    .locals 3

    .line 77
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 78
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/dp;->xP()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "use_service"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 80
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/dp;->xP()Landroid/content/SharedPreferences;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method final xT()V
    .locals 3

    .line 87
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 88
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 2022
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "Clearing collection preferences."

    .line 88
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 89
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/measurement/internal/j;->aFl:Lcom/google/android/gms/measurement/internal/cv;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/jb;->a(Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 90
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/dp;->xU()Ljava/lang/Boolean;

    move-result-object v0

    .line 91
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/dp;->xP()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 92
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 93
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    if-eqz v0, :cond_0

    .line 95
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/measurement/internal/dp;->O(Z)V

    :cond_0
    return-void

    .line 97
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/dp;->xP()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "measurement_enabled"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    .line 100
    invoke-virtual {p0, v1}, Lcom/google/android/gms/measurement/internal/dp;->P(Z)Z

    move-result v1

    .line 101
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/dp;->xP()Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 102
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 103
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    if-eqz v0, :cond_3

    .line 105
    invoke-virtual {p0, v1}, Lcom/google/android/gms/measurement/internal/dp;->O(Z)V

    :cond_3
    return-void
.end method

.method final xU()Ljava/lang/Boolean;
    .locals 3

    .line 115
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 116
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/dp;->xP()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "measurement_enabled"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/dp;->xP()Landroid/content/SharedPreferences;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final xV()Ljava/lang/String;
    .locals 4

    .line 119
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 120
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/dp;->xP()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "previous_os_version"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 121
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vD()Lcom/google/android/gms/measurement/internal/d;

    move-result-object v2

    .line 122
    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/fg;->vt()V

    .line 123
    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 125
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 126
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/dp;->xP()Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 127
    invoke-interface {v3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 128
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-object v0
.end method
