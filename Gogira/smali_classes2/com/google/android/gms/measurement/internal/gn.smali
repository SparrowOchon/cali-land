.class final synthetic Lcom/google/android/gms/measurement/internal/gn;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final aJQ:Lcom/google/android/gms/measurement/internal/go;

.field private final aJR:I

.field private final aJS:Ljava/lang/Exception;

.field private final aJT:[B

.field private final aJU:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/go;ILjava/lang/Exception;[BLjava/util/Map;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/gn;->aJQ:Lcom/google/android/gms/measurement/internal/go;

    iput p2, p0, Lcom/google/android/gms/measurement/internal/gn;->aJR:I

    iput-object p3, p0, Lcom/google/android/gms/measurement/internal/gn;->aJS:Ljava/lang/Exception;

    iput-object p4, p0, Lcom/google/android/gms/measurement/internal/gn;->aJT:[B

    iput-object p5, p0, Lcom/google/android/gms/measurement/internal/gn;->aJU:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/gn;->aJQ:Lcom/google/android/gms/measurement/internal/go;

    iget v1, p0, Lcom/google/android/gms/measurement/internal/gn;->aJR:I

    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/gn;->aJS:Ljava/lang/Exception;

    iget-object v3, p0, Lcom/google/android/gms/measurement/internal/gn;->aJT:[B

    .line 1039
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/go;->aJV:Lcom/google/android/gms/measurement/internal/gl;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/measurement/internal/gl;->a(ILjava/lang/Throwable;[B)V

    return-void
.end method
