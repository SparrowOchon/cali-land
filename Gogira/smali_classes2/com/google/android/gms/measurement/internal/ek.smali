.class public Lcom/google/android/gms/measurement/internal/ek;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/measurement/internal/fj;


# static fields
.field private static volatile aIE:Lcom/google/android/gms/measurement/internal/ek;


# instance fields
.field final aAX:Z

.field final aBa:Ljava/lang/String;

.field private final aBe:Lcom/google/android/gms/common/util/e;

.field private final aDV:Lcom/google/android/gms/measurement/internal/ja;

.field final aIF:Ljava/lang/String;

.field final aIG:Ljava/lang/String;

.field final aIH:Lcom/google/android/gms/measurement/internal/jb;

.field private final aII:Lcom/google/android/gms/measurement/internal/dp;

.field final aIJ:Lcom/google/android/gms/measurement/internal/dh;

.field final aIK:Lcom/google/android/gms/measurement/internal/ed;

.field private final aIL:Lcom/google/android/gms/measurement/internal/hx;

.field private final aIM:Lcom/google/android/gms/measurement/internal/it;

.field private final aIN:Lcom/google/android/gms/measurement/internal/df;

.field private final aIO:Lcom/google/android/gms/measurement/internal/gr;

.field final aIP:Lcom/google/android/gms/measurement/internal/fr;

.field private final aIQ:Lcom/google/android/gms/measurement/internal/a;

.field private final aIR:Lcom/google/android/gms/measurement/internal/gm;

.field private aIS:Lcom/google/android/gms/measurement/internal/dd;

.field private aIT:Lcom/google/android/gms/measurement/internal/gw;

.field private aIU:Lcom/google/android/gms/measurement/internal/d;

.field private aIV:Lcom/google/android/gms/measurement/internal/cz;

.field aIW:Lcom/google/android/gms/measurement/internal/du;

.field private aIX:Ljava/lang/Boolean;

.field private aIY:J

.field private volatile aIZ:Ljava/lang/Boolean;

.field private aJa:Ljava/lang/Boolean;

.field private aJb:Ljava/lang/Boolean;

.field aJc:I

.field private aJd:Ljava/util/concurrent/atomic/AtomicInteger;

.field final aJe:J

.field private asG:Z

.field private final ask:Landroid/content/Context;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/measurement/internal/fo;)V
    .locals 7

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/google/android/gms/measurement/internal/ek;->asG:Z

    .line 3
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aJd:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 4
    invoke-static {p1}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 6
    new-instance v1, Lcom/google/android/gms/measurement/internal/ja;

    invoke-direct {v1}, Lcom/google/android/gms/measurement/internal/ja;-><init>()V

    .line 7
    iput-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aDV:Lcom/google/android/gms/measurement/internal/ja;

    .line 8
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aDV:Lcom/google/android/gms/measurement/internal/ja;

    invoke-static {v1}, Lcom/google/android/gms/measurement/internal/j;->a(Lcom/google/android/gms/measurement/internal/ja;)V

    .line 9
    iget-object v1, p1, Lcom/google/android/gms/measurement/internal/fo;->ask:Landroid/content/Context;

    iput-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->ask:Landroid/content/Context;

    .line 10
    iget-object v1, p1, Lcom/google/android/gms/measurement/internal/fo;->aBa:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aBa:Ljava/lang/String;

    .line 11
    iget-object v1, p1, Lcom/google/android/gms/measurement/internal/fo;->aIF:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aIF:Ljava/lang/String;

    .line 12
    iget-object v1, p1, Lcom/google/android/gms/measurement/internal/fo;->aIG:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aIG:Ljava/lang/String;

    .line 13
    iget-boolean v1, p1, Lcom/google/android/gms/measurement/internal/fo;->aAX:Z

    iput-boolean v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aAX:Z

    .line 14
    iget-object v1, p1, Lcom/google/android/gms/measurement/internal/fo;->aIZ:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aIZ:Ljava/lang/Boolean;

    .line 15
    iget-object v1, p1, Lcom/google/android/gms/measurement/internal/fo;->aJx:Lcom/google/android/gms/internal/measurement/zzx;

    if-eqz v1, :cond_1

    .line 16
    iget-object v2, v1, Lcom/google/android/gms/internal/measurement/zzx;->aBb:Landroid/os/Bundle;

    if-eqz v2, :cond_1

    .line 17
    iget-object v2, v1, Lcom/google/android/gms/internal/measurement/zzx;->aBb:Landroid/os/Bundle;

    const-string v3, "measurementEnabled"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 18
    instance-of v3, v2, Ljava/lang/Boolean;

    if-eqz v3, :cond_0

    .line 19
    check-cast v2, Ljava/lang/Boolean;

    iput-object v2, p0, Lcom/google/android/gms/measurement/internal/ek;->aJa:Ljava/lang/Boolean;

    .line 20
    :cond_0
    iget-object v1, v1, Lcom/google/android/gms/internal/measurement/zzx;->aBb:Landroid/os/Bundle;

    const-string v2, "measurementDeactivated"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 21
    instance-of v2, v1, Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    .line 22
    check-cast v1, Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aJb:Ljava/lang/Boolean;

    .line 23
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->ask:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/internal/measurement/bf;->ai(Landroid/content/Context;)V

    .line 25
    invoke-static {}, Lcom/google/android/gms/common/util/g;->mm()Lcom/google/android/gms/common/util/e;

    move-result-object v1

    .line 26
    iput-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aBe:Lcom/google/android/gms/common/util/e;

    .line 27
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aBe:Lcom/google/android/gms/common/util/e;

    invoke-interface {v1}, Lcom/google/android/gms/common/util/e;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aJe:J

    .line 29
    new-instance v1, Lcom/google/android/gms/measurement/internal/jb;

    invoke-direct {v1, p0}, Lcom/google/android/gms/measurement/internal/jb;-><init>(Lcom/google/android/gms/measurement/internal/ek;)V

    .line 30
    iput-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aIH:Lcom/google/android/gms/measurement/internal/jb;

    .line 32
    new-instance v1, Lcom/google/android/gms/measurement/internal/dp;

    invoke-direct {v1, p0}, Lcom/google/android/gms/measurement/internal/dp;-><init>(Lcom/google/android/gms/measurement/internal/ek;)V

    .line 34
    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/fg;->initialize()V

    .line 35
    iput-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aII:Lcom/google/android/gms/measurement/internal/dp;

    .line 37
    new-instance v1, Lcom/google/android/gms/measurement/internal/dh;

    invoke-direct {v1, p0}, Lcom/google/android/gms/measurement/internal/dh;-><init>(Lcom/google/android/gms/measurement/internal/ek;)V

    .line 39
    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/fg;->initialize()V

    .line 40
    iput-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aIJ:Lcom/google/android/gms/measurement/internal/dh;

    .line 42
    new-instance v1, Lcom/google/android/gms/measurement/internal/it;

    invoke-direct {v1, p0}, Lcom/google/android/gms/measurement/internal/it;-><init>(Lcom/google/android/gms/measurement/internal/ek;)V

    .line 44
    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/fg;->initialize()V

    .line 45
    iput-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aIM:Lcom/google/android/gms/measurement/internal/it;

    .line 47
    new-instance v1, Lcom/google/android/gms/measurement/internal/df;

    invoke-direct {v1, p0}, Lcom/google/android/gms/measurement/internal/df;-><init>(Lcom/google/android/gms/measurement/internal/ek;)V

    .line 49
    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/fg;->initialize()V

    .line 50
    iput-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aIN:Lcom/google/android/gms/measurement/internal/df;

    .line 52
    new-instance v1, Lcom/google/android/gms/measurement/internal/a;

    invoke-direct {v1, p0}, Lcom/google/android/gms/measurement/internal/a;-><init>(Lcom/google/android/gms/measurement/internal/ek;)V

    .line 53
    iput-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aIQ:Lcom/google/android/gms/measurement/internal/a;

    .line 55
    new-instance v1, Lcom/google/android/gms/measurement/internal/gr;

    invoke-direct {v1, p0}, Lcom/google/android/gms/measurement/internal/gr;-><init>(Lcom/google/android/gms/measurement/internal/ek;)V

    .line 57
    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/fb;->initialize()V

    .line 58
    iput-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aIO:Lcom/google/android/gms/measurement/internal/gr;

    .line 60
    new-instance v1, Lcom/google/android/gms/measurement/internal/fr;

    invoke-direct {v1, p0}, Lcom/google/android/gms/measurement/internal/fr;-><init>(Lcom/google/android/gms/measurement/internal/ek;)V

    .line 62
    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/fb;->initialize()V

    .line 63
    iput-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aIP:Lcom/google/android/gms/measurement/internal/fr;

    .line 65
    new-instance v1, Lcom/google/android/gms/measurement/internal/hx;

    invoke-direct {v1, p0}, Lcom/google/android/gms/measurement/internal/hx;-><init>(Lcom/google/android/gms/measurement/internal/ek;)V

    .line 67
    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/fb;->initialize()V

    .line 68
    iput-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aIL:Lcom/google/android/gms/measurement/internal/hx;

    .line 70
    new-instance v1, Lcom/google/android/gms/measurement/internal/gm;

    invoke-direct {v1, p0}, Lcom/google/android/gms/measurement/internal/gm;-><init>(Lcom/google/android/gms/measurement/internal/ek;)V

    .line 72
    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/fg;->initialize()V

    .line 73
    iput-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aIR:Lcom/google/android/gms/measurement/internal/gm;

    .line 75
    new-instance v1, Lcom/google/android/gms/measurement/internal/ed;

    invoke-direct {v1, p0}, Lcom/google/android/gms/measurement/internal/ed;-><init>(Lcom/google/android/gms/measurement/internal/ek;)V

    .line 77
    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/fg;->initialize()V

    .line 78
    iput-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aIK:Lcom/google/android/gms/measurement/internal/ed;

    .line 79
    iget-object v1, p1, Lcom/google/android/gms/measurement/internal/fo;->aJx:Lcom/google/android/gms/internal/measurement/zzx;

    const/4 v2, 0x1

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/google/android/gms/measurement/internal/fo;->aJx:Lcom/google/android/gms/internal/measurement/zzx;

    iget-wide v3, v1, Lcom/google/android/gms/internal/measurement/zzx;->aAW:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    xor-int/2addr v1, v2

    .line 84
    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/ek;->ask:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    instance-of v2, v2, Landroid/app/Application;

    if-eqz v2, :cond_4

    .line 85
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v2

    .line 86
    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/fh;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    instance-of v3, v3, Landroid/app/Application;

    if-eqz v3, :cond_5

    .line 87
    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/fh;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, Landroid/app/Application;

    .line 88
    iget-object v4, v2, Lcom/google/android/gms/measurement/internal/fr;->aJA:Lcom/google/android/gms/measurement/internal/gk;

    if-nez v4, :cond_3

    .line 89
    new-instance v4, Lcom/google/android/gms/measurement/internal/gk;

    invoke-direct {v4, v2, v0}, Lcom/google/android/gms/measurement/internal/gk;-><init>(Lcom/google/android/gms/measurement/internal/fr;B)V

    iput-object v4, v2, Lcom/google/android/gms/measurement/internal/fr;->aJA:Lcom/google/android/gms/measurement/internal/gk;

    :cond_3
    if-eqz v1, :cond_5

    .line 91
    iget-object v0, v2, Lcom/google/android/gms/measurement/internal/fr;->aJA:Lcom/google/android/gms/measurement/internal/gk;

    .line 92
    invoke-virtual {v3, v0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 93
    iget-object v0, v2, Lcom/google/android/gms/measurement/internal/fr;->aJA:Lcom/google/android/gms/measurement/internal/gk;

    .line 94
    invoke-virtual {v3, v0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 95
    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 1022
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "Registered activity lifecycle callback"

    .line 95
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    goto :goto_1

    .line 97
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 2017
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "Application context is not an Application"

    .line 97
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 98
    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIK:Lcom/google/android/gms/measurement/internal/ed;

    new-instance v1, Lcom/google/android/gms/measurement/internal/em;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/measurement/internal/em;-><init>(Lcom/google/android/gms/measurement/internal/ek;Lcom/google/android/gms/measurement/internal/fo;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/ed;->f(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/internal/measurement/zzx;)Lcom/google/android/gms/measurement/internal/ek;
    .locals 11

    if-eqz p1, :cond_1

    .line 275
    iget-object v0, p1, Lcom/google/android/gms/internal/measurement/zzx;->aAZ:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/internal/measurement/zzx;->aBa:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 276
    :cond_0
    new-instance v0, Lcom/google/android/gms/internal/measurement/zzx;

    iget-wide v2, p1, Lcom/google/android/gms/internal/measurement/zzx;->aAV:J

    iget-wide v4, p1, Lcom/google/android/gms/internal/measurement/zzx;->aAW:J

    iget-boolean v6, p1, Lcom/google/android/gms/internal/measurement/zzx;->aAX:Z

    iget-object v7, p1, Lcom/google/android/gms/internal/measurement/zzx;->aAY:Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    iget-object v10, p1, Lcom/google/android/gms/internal/measurement/zzx;->aBb:Landroid/os/Bundle;

    move-object v1, v0

    invoke-direct/range {v1 .. v10}, Lcom/google/android/gms/internal/measurement/zzx;-><init>(JJZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    move-object p1, v0

    .line 277
    :cond_1
    invoke-static {p0}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    sget-object v0, Lcom/google/android/gms/measurement/internal/ek;->aIE:Lcom/google/android/gms/measurement/internal/ek;

    if-nez v0, :cond_3

    .line 280
    const-class v0, Lcom/google/android/gms/measurement/internal/ek;

    monitor-enter v0

    .line 281
    :try_start_0
    sget-object v1, Lcom/google/android/gms/measurement/internal/ek;->aIE:Lcom/google/android/gms/measurement/internal/ek;

    if-nez v1, :cond_2

    .line 283
    new-instance v1, Lcom/google/android/gms/measurement/internal/fo;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/measurement/internal/fo;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/measurement/zzx;)V

    .line 285
    new-instance p0, Lcom/google/android/gms/measurement/internal/ek;

    invoke-direct {p0, v1}, Lcom/google/android/gms/measurement/internal/ek;-><init>(Lcom/google/android/gms/measurement/internal/fo;)V

    .line 286
    sput-object p0, Lcom/google/android/gms/measurement/internal/ek;->aIE:Lcom/google/android/gms/measurement/internal/ek;

    .line 287
    :cond_2
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    :cond_3
    if-eqz p1, :cond_4

    .line 288
    iget-object p0, p1, Lcom/google/android/gms/internal/measurement/zzx;->aBb:Landroid/os/Bundle;

    if-eqz p0, :cond_4

    iget-object p0, p1, Lcom/google/android/gms/internal/measurement/zzx;->aBb:Landroid/os/Bundle;

    const-string v0, "dataCollectionDefaultEnabled"

    .line 289
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 290
    sget-object p0, Lcom/google/android/gms/measurement/internal/ek;->aIE:Lcom/google/android/gms/measurement/internal/ek;

    iget-object p1, p1, Lcom/google/android/gms/internal/measurement/zzx;->aBb:Landroid/os/Bundle;

    const-string v0, "dataCollectionDefaultEnabled"

    .line 291
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    .line 292
    invoke-virtual {p0, p1}, Lcom/google/android/gms/measurement/internal/ek;->zza(Z)V

    .line 293
    :cond_4
    :goto_0
    sget-object p0, Lcom/google/android/gms/measurement/internal/ek;->aIE:Lcom/google/android/gms/measurement/internal/ek;

    return-object p0
.end method

.method static synthetic a(Lcom/google/android/gms/measurement/internal/ek;Lcom/google/android/gms/measurement/internal/fo;)V
    .locals 4

    .line 11101
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 11102
    invoke-static {}, Lcom/google/android/gms/measurement/internal/jb;->zi()Ljava/lang/String;

    .line 11104
    new-instance v0, Lcom/google/android/gms/measurement/internal/d;

    invoke-direct {v0, p0}, Lcom/google/android/gms/measurement/internal/d;-><init>(Lcom/google/android/gms/measurement/internal/ek;)V

    .line 11106
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fg;->initialize()V

    .line 11107
    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIU:Lcom/google/android/gms/measurement/internal/d;

    .line 11109
    new-instance v0, Lcom/google/android/gms/measurement/internal/cz;

    iget-wide v1, p1, Lcom/google/android/gms/measurement/internal/fo;->aAW:J

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/measurement/internal/cz;-><init>(Lcom/google/android/gms/measurement/internal/ek;J)V

    .line 11111
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fb;->initialize()V

    .line 11112
    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIV:Lcom/google/android/gms/measurement/internal/cz;

    .line 11114
    new-instance p1, Lcom/google/android/gms/measurement/internal/dd;

    invoke-direct {p1, p0}, Lcom/google/android/gms/measurement/internal/dd;-><init>(Lcom/google/android/gms/measurement/internal/ek;)V

    .line 11116
    invoke-virtual {p1}, Lcom/google/android/gms/measurement/internal/fb;->initialize()V

    .line 11117
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/ek;->aIS:Lcom/google/android/gms/measurement/internal/dd;

    .line 11119
    new-instance p1, Lcom/google/android/gms/measurement/internal/gw;

    invoke-direct {p1, p0}, Lcom/google/android/gms/measurement/internal/gw;-><init>(Lcom/google/android/gms/measurement/internal/ek;)V

    .line 11121
    invoke-virtual {p1}, Lcom/google/android/gms/measurement/internal/fb;->initialize()V

    .line 11122
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/ek;->aIT:Lcom/google/android/gms/measurement/internal/gw;

    .line 11123
    iget-object p1, p0, Lcom/google/android/gms/measurement/internal/ek;->aIM:Lcom/google/android/gms/measurement/internal/it;

    invoke-virtual {p1}, Lcom/google/android/gms/measurement/internal/fg;->yB()V

    .line 11124
    iget-object p1, p0, Lcom/google/android/gms/measurement/internal/ek;->aII:Lcom/google/android/gms/measurement/internal/dp;

    invoke-virtual {p1}, Lcom/google/android/gms/measurement/internal/fg;->yB()V

    .line 11126
    new-instance p1, Lcom/google/android/gms/measurement/internal/du;

    invoke-direct {p1, p0}, Lcom/google/android/gms/measurement/internal/du;-><init>(Lcom/google/android/gms/measurement/internal/ek;)V

    .line 11127
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/ek;->aIW:Lcom/google/android/gms/measurement/internal/du;

    .line 11128
    iget-object p1, p0, Lcom/google/android/gms/measurement/internal/ek;->aIV:Lcom/google/android/gms/measurement/internal/cz;

    invoke-virtual {p1}, Lcom/google/android/gms/measurement/internal/fb;->yB()V

    .line 11129
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 12020
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGw:Lcom/google/android/gms/measurement/internal/dj;

    const-wide/16 v1, 0x3f7a

    .line 11131
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "App measurement is starting up, version"

    invoke-virtual {p1, v2, v1}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    .line 11135
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 13020
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGw:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "To enable debug logging run: adb shell setprop log.tag.FA VERBOSE"

    .line 11135
    invoke-virtual {p1, v1}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 11139
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cz;->pT()Ljava/lang/String;

    move-result-object p1

    .line 11141
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aBa:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 11143
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/measurement/internal/it;->cT(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 11144
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 14020
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGw:Lcom/google/android/gms/measurement/internal/dj;

    const-string v0, "Faster debug mode event logging enabled. To disable, run:\n  adb shell setprop debug.firebase.analytics.app .none."

    move-object v3, v0

    move-object v0, p1

    move-object p1, v3

    goto :goto_0

    .line 11147
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 15020
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGw:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "To enable faster debug mode event logging run:\n  adb shell setprop debug.firebase.analytics.app "

    .line 11148
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/lang/String;

    invoke-direct {p1, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 11149
    :goto_0
    invoke-virtual {v0, p1}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 11150
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 15021
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    const-string v0, "Debug-level message logging enabled"

    .line 11150
    invoke-virtual {p1, v0}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 11151
    iget p1, p0, Lcom/google/android/gms/measurement/internal/ek;->aJc:I

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aJd:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-eq p1, v0, :cond_3

    .line 11152
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 16014
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    .line 11153
    iget v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aJc:I

    .line 11154
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aJd:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "Not all components initialized"

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_3
    const/4 p1, 0x1

    .line 11155
    iput-boolean p1, p0, Lcom/google/android/gms/measurement/internal/ek;->asG:Z

    return-void
.end method

.method private static a(Lcom/google/android/gms/measurement/internal/fb;)V
    .locals 3

    if-eqz p0, :cond_1

    .line 304
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fb;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 305
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Component not initialized: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 303
    :cond_1
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Component not created"

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static a(Lcom/google/android/gms/measurement/internal/fg;)V
    .locals 3

    if-eqz p0, :cond_1

    .line 299
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fg;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 300
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Component not initialized: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 298
    :cond_1
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Component not created"

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static a(Lcom/google/android/gms/measurement/internal/fh;)V
    .locals 1

    if-eqz p0, :cond_0

    return-void

    .line 308
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Component not created"

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static d(Landroid/content/Context;Landroid/os/Bundle;)Lcom/google/android/gms/measurement/internal/ek;
    .locals 11

    .line 274
    new-instance v10, Lcom/google/android/gms/internal/measurement/zzx;

    const-wide/16 v1, 0x0

    const-wide/16 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, v10

    move-object v9, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/internal/measurement/zzx;-><init>(JJZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-static {p0, v10}, Lcom/google/android/gms/measurement/internal/ek;->a(Landroid/content/Context;Lcom/google/android/gms/internal/measurement/zzx;)Lcom/google/android/gms/measurement/internal/ek;

    move-result-object p0

    return-object p0
.end method

.method private final vt()V
    .locals 2

    .line 294
    iget-boolean v0, p0, Lcom/google/android/gms/measurement/internal/ek;->asG:Z

    if-eqz v0, :cond_0

    return-void

    .line 295
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "AppMeasurement is not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static vu()V
    .locals 2

    .line 374
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected call on client side"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private final yx()Lcom/google/android/gms/measurement/internal/gm;
    .locals 1

    .line 253
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIR:Lcom/google/android/gms/measurement/internal/gm;

    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/ek;->a(Lcom/google/android/gms/measurement/internal/fg;)V

    .line 254
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIR:Lcom/google/android/gms/measurement/internal/gm;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/internal/measurement/ln;)V
    .locals 5

    .line 414
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 415
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/ek;->yx()Lcom/google/android/gms/measurement/internal/gm;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/ek;->a(Lcom/google/android/gms/measurement/internal/fg;)V

    .line 416
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cz;->pT()Ljava/lang/String;

    move-result-object v0

    .line 418
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/measurement/internal/dp;->cl(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    .line 420
    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/ek;->aIH:Lcom/google/android/gms/measurement/internal/jb;

    .line 421
    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/jb;->zm()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    const-string v3, ""

    if-eqz v2, :cond_2

    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    .line 425
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/ek;->yx()Lcom/google/android/gms/measurement/internal/gm;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/gm;->xJ()Z

    move-result v2

    if-nez v2, :cond_1

    .line 426
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 11017
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "Network is not available for Deferred Deep Link request. Skipping"

    .line 426
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 427
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v0

    invoke-virtual {v0, p1, v3}, Lcom/google/android/gms/measurement/internal/it;->b(Lcom/google/android/gms/internal/measurement/ln;Ljava/lang/String;)V

    return-void

    .line 430
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v2

    .line 431
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vy()Lcom/google/android/gms/measurement/internal/cz;

    const-wide/16 v3, 0x3f7a

    .line 433
    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v0, v1}, Lcom/google/android/gms/measurement/internal/it;->b(JLjava/lang/String;Ljava/lang/String;)Ljava/net/URL;

    move-result-object v1

    .line 434
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/ek;->yx()Lcom/google/android/gms/measurement/internal/gm;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/measurement/internal/ej;

    invoke-direct {v3, p0, p1}, Lcom/google/android/gms/measurement/internal/ej;-><init>(Lcom/google/android/gms/measurement/internal/ek;Lcom/google/android/gms/internal/measurement/ln;)V

    .line 436
    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 437
    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/fg;->vt()V

    .line 438
    invoke-static {v1}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 439
    invoke-static {v3}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 440
    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/fh;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object p1

    new-instance v4, Lcom/google/android/gms/measurement/internal/go;

    invoke-direct {v4, v2, v0, v1, v3}, Lcom/google/android/gms/measurement/internal/go;-><init>(Lcom/google/android/gms/measurement/internal/gm;Ljava/lang/String;Ljava/net/URL;Lcom/google/android/gms/measurement/internal/gl;)V

    .line 441
    invoke-virtual {p1, v4}, Lcom/google/android/gms/measurement/internal/ed;->g(Ljava/lang/Runnable;)V

    return-void

    .line 422
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 10021
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "ADID unavailable to retrieve Deferred Deep Link. Skipping"

    .line 422
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 423
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v0

    invoke-virtual {v0, p1, v3}, Lcom/google/android/gms/measurement/internal/it;->b(Lcom/google/android/gms/internal/measurement/ln;Ljava/lang/String;)V

    return-void
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .line 255
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->ask:Landroid/content/Context;

    return-object v0
.end method

.method public final isEnabled()Z
    .locals 4

    .line 314
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 315
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/ek;->vt()V

    .line 317
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIH:Lcom/google/android/gms/measurement/internal/jb;

    .line 318
    sget-object v1, Lcom/google/android/gms/measurement/internal/j;->aFl:Lcom/google/android/gms/measurement/internal/cv;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/jb;->a(Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v0

    const/4 v1, 0x1

    const-string v2, "firebase_analytics_collection_enabled"

    const/4 v3, 0x0

    if-eqz v0, :cond_7

    .line 320
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIH:Lcom/google/android/gms/measurement/internal/jb;

    .line 321
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/jb;->zl()Z

    move-result v0

    if-eqz v0, :cond_0

    return v3

    .line 323
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aJb:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 324
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    return v3

    .line 326
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/dp;->xU()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 328
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    .line 330
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIH:Lcom/google/android/gms/measurement/internal/jb;

    .line 8134
    invoke-virtual {v0, v2}, Lcom/google/android/gms/measurement/internal/jb;->cV(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 333
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    .line 334
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aJa:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 335
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    .line 336
    :cond_4
    invoke-static {}, Lcom/google/android/gms/common/api/internal/e;->lE()Z

    move-result v0

    if-eqz v0, :cond_5

    return v3

    .line 340
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIH:Lcom/google/android/gms/measurement/internal/jb;

    .line 341
    sget-object v2, Lcom/google/android/gms/measurement/internal/j;->aFg:Lcom/google/android/gms/measurement/internal/cv;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/measurement/internal/jb;->a(Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIZ:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 342
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIZ:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :cond_6
    return v1

    .line 345
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIH:Lcom/google/android/gms/measurement/internal/jb;

    .line 346
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/jb;->zl()Z

    move-result v0

    if-eqz v0, :cond_8

    return v3

    .line 349
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIH:Lcom/google/android/gms/measurement/internal/jb;

    .line 9134
    invoke-virtual {v0, v2}, Lcom/google/android/gms/measurement/internal/jb;->cV(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 352
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    .line 353
    :cond_9
    invoke-static {}, Lcom/google/android/gms/common/api/internal/e;->lE()Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_a

    .line 355
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aIZ:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    .line 356
    sget-object v1, Lcom/google/android/gms/measurement/internal/j;->aFg:Lcom/google/android/gms/measurement/internal/cv;

    const/4 v2, 0x0

    .line 357
    invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/cv;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 358
    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 360
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIZ:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 361
    :cond_a
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/measurement/internal/dp;->P(Z)Z

    move-result v0

    return v0
.end method

.method protected final start()V
    .locals 6

    .line 158
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 159
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dp;->aGR:Lcom/google/android/gms/measurement/internal/dt;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/dt;->get()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    .line 160
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dp;->aGR:Lcom/google/android/gms/measurement/internal/dt;

    .line 161
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aBe:Lcom/google/android/gms/common/util/e;

    .line 162
    invoke-interface {v1}, Lcom/google/android/gms/common/util/e;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/measurement/internal/dt;->set(J)V

    .line 163
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dp;->aGW:Lcom/google/android/gms/measurement/internal/dt;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/dt;->get()J

    move-result-wide v0

    cmp-long v4, v0, v2

    if-nez v4, :cond_1

    .line 165
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 2022
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    .line 165
    iget-wide v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aJe:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "Persisting first open"

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    .line 166
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dp;->aGW:Lcom/google/android/gms/measurement/internal/dt;

    iget-wide v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aJe:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/measurement/internal/dt;->set(J)V

    .line 167
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->yA()Z

    move-result v0

    if-nez v0, :cond_6

    .line 168
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 169
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v0

    const-string v1, "android.permission.INTERNET"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/it;->cR(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 170
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 3014
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "App is missing INTERNET permission"

    .line 170
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 171
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v0

    const-string v1, "android.permission.ACCESS_NETWORK_STATE"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/it;->cR(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 172
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 4014
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "App is missing ACCESS_NETWORK_STATE permission"

    .line 172
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 177
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->ask:Landroid/content/Context;

    .line 178
    invoke-static {v0}, Lcom/google/android/gms/common/c/c;->Y(Landroid/content/Context;)Lcom/google/android/gms/common/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/c/b;->my()Z

    move-result v0

    if-nez v0, :cond_5

    .line 179
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIH:Lcom/google/android/gms/measurement/internal/jb;

    .line 180
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/jb;->mL()Z

    move-result v0

    if-nez v0, :cond_5

    .line 182
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->ask:Landroid/content/Context;

    .line 183
    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/dz;->at(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 184
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 5014
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "AppMeasurementReceiver not registered/enabled"

    .line 184
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 186
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->ask:Landroid/content/Context;

    .line 187
    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/it;->av(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 188
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 6014
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "AppMeasurementService not registered/enabled"

    .line 188
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 189
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 7014
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "Uploading is not possible. App measurement disabled"

    .line 189
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 194
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cz;->getGmpAppId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 195
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cz;->xB()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 196
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    .line 197
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cz;->getGmpAppId()Ljava/lang/String;

    move-result-object v0

    .line 198
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/dp;->xQ()Ljava/lang/String;

    move-result-object v1

    .line 199
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/cz;->xB()Ljava/lang/String;

    move-result-object v2

    .line 200
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/dp;->xR()Ljava/lang/String;

    move-result-object v3

    .line 201
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/measurement/internal/it;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 202
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 7020
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGw:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "Rechecking which service to use due to a GMP App Id change"

    .line 202
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 203
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/dp;->xT()V

    .line 204
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vB()Lcom/google/android/gms/measurement/internal/dd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/dd;->xE()V

    .line 205
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIT:Lcom/google/android/gms/measurement/internal/gw;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/gw;->disconnect()V

    .line 206
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIT:Lcom/google/android/gms/measurement/internal/gw;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/gw;->yK()V

    .line 207
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dp;->aGW:Lcom/google/android/gms/measurement/internal/dt;

    iget-wide v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aJe:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/measurement/internal/dt;->set(J)V

    .line 208
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dp;->aGY:Lcom/google/android/gms/measurement/internal/dv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dv;->cq(Ljava/lang/String;)V

    .line 209
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/cz;->getGmpAppId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dp;->cn(Ljava/lang/String;)V

    .line 210
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/cz;->xB()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dp;->co(Ljava/lang/String;)V

    .line 211
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dp;->aGY:Lcom/google/android/gms/measurement/internal/dv;

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/dv;->xZ()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/fr;->cI(Ljava/lang/String;)V

    .line 215
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cz;->getGmpAppId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 216
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cz;->xB()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 217
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->isEnabled()Z

    move-result v0

    .line 218
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v1

    .line 7140
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dp;->aGP:Landroid/content/SharedPreferences;

    const-string v2, "deferred_analytics_collection"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 219
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aIH:Lcom/google/android/gms/measurement/internal/jb;

    .line 220
    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/jb;->zl()Z

    move-result v1

    if-nez v1, :cond_b

    .line 221
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v1

    xor-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/dp;->Q(Z)V

    :cond_b
    if-eqz v0, :cond_c

    .line 223
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fr;->yF()V

    .line 224
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vz()Lcom/google/android/gms/measurement/internal/gw;

    move-result-object v0

    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/gw;->a(Ljava/util/concurrent/atomic/AtomicReference;)V

    .line 225
    :cond_d
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dp;->aHg:Lcom/google/android/gms/measurement/internal/dr;

    .line 226
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aIH:Lcom/google/android/gms/measurement/internal/jb;

    .line 227
    sget-object v2, Lcom/google/android/gms/measurement/internal/j;->aFu:Lcom/google/android/gms/measurement/internal/cv;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/jb;->a(Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dr;->set(Z)V

    .line 228
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dp;->aHh:Lcom/google/android/gms/measurement/internal/dr;

    .line 229
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aIH:Lcom/google/android/gms/measurement/internal/jb;

    .line 230
    sget-object v2, Lcom/google/android/gms/measurement/internal/j;->aFv:Lcom/google/android/gms/measurement/internal/cv;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/jb;->a(Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dr;->set(Z)V

    return-void
.end method

.method public final vA()Lcom/google/android/gms/measurement/internal/gr;
    .locals 1

    .line 262
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIO:Lcom/google/android/gms/measurement/internal/gr;

    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/ek;->a(Lcom/google/android/gms/measurement/internal/fb;)V

    .line 263
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIO:Lcom/google/android/gms/measurement/internal/gr;

    return-object v0
.end method

.method public final vB()Lcom/google/android/gms/measurement/internal/dd;
    .locals 1

    .line 251
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIS:Lcom/google/android/gms/measurement/internal/dd;

    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/ek;->a(Lcom/google/android/gms/measurement/internal/fb;)V

    .line 252
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIS:Lcom/google/android/gms/measurement/internal/dd;

    return-object v0
.end method

.method public final vC()Lcom/google/android/gms/measurement/internal/hx;
    .locals 1

    .line 241
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIL:Lcom/google/android/gms/measurement/internal/hx;

    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/ek;->a(Lcom/google/android/gms/measurement/internal/fb;)V

    .line 242
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIL:Lcom/google/android/gms/measurement/internal/hx;

    return-object v0
.end method

.method public final vD()Lcom/google/android/gms/measurement/internal/d;
    .locals 1

    .line 266
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIU:Lcom/google/android/gms/measurement/internal/d;

    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/ek;->a(Lcom/google/android/gms/measurement/internal/fg;)V

    .line 267
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIU:Lcom/google/android/gms/measurement/internal/d;

    return-object v0
.end method

.method public final vE()Lcom/google/android/gms/common/util/e;
    .locals 1

    .line 261
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aBe:Lcom/google/android/gms/common/util/e;

    return-object v0
.end method

.method public final vF()Lcom/google/android/gms/measurement/internal/df;
    .locals 1

    .line 249
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIN:Lcom/google/android/gms/measurement/internal/df;

    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/ek;->a(Lcom/google/android/gms/measurement/internal/fh;)V

    .line 250
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIN:Lcom/google/android/gms/measurement/internal/df;

    return-object v0
.end method

.method public final vG()Lcom/google/android/gms/measurement/internal/it;
    .locals 1

    .line 247
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIM:Lcom/google/android/gms/measurement/internal/it;

    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/ek;->a(Lcom/google/android/gms/measurement/internal/fh;)V

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIM:Lcom/google/android/gms/measurement/internal/it;

    return-object v0
.end method

.method public final vH()Lcom/google/android/gms/measurement/internal/ed;
    .locals 1

    .line 239
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIK:Lcom/google/android/gms/measurement/internal/ed;

    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/ek;->a(Lcom/google/android/gms/measurement/internal/fg;)V

    .line 240
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIK:Lcom/google/android/gms/measurement/internal/ed;

    return-object v0
.end method

.method public final vI()Lcom/google/android/gms/measurement/internal/dh;
    .locals 1

    .line 236
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIJ:Lcom/google/android/gms/measurement/internal/dh;

    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/ek;->a(Lcom/google/android/gms/measurement/internal/fg;)V

    .line 237
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIJ:Lcom/google/android/gms/measurement/internal/dh;

    return-object v0
.end method

.method public final vJ()Lcom/google/android/gms/measurement/internal/dp;
    .locals 1

    .line 234
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aII:Lcom/google/android/gms/measurement/internal/dp;

    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/ek;->a(Lcom/google/android/gms/measurement/internal/fh;)V

    .line 235
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aII:Lcom/google/android/gms/measurement/internal/dp;

    return-object v0
.end method

.method public final vw()Lcom/google/android/gms/measurement/internal/a;
    .locals 2

    .line 270
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIQ:Lcom/google/android/gms/measurement/internal/a;

    if-eqz v0, :cond_0

    return-object v0

    .line 272
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Component not created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final vx()Lcom/google/android/gms/measurement/internal/fr;
    .locals 1

    .line 245
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIP:Lcom/google/android/gms/measurement/internal/fr;

    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/ek;->a(Lcom/google/android/gms/measurement/internal/fb;)V

    .line 246
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIP:Lcom/google/android/gms/measurement/internal/fr;

    return-object v0
.end method

.method public final vy()Lcom/google/android/gms/measurement/internal/cz;
    .locals 1

    .line 268
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIV:Lcom/google/android/gms/measurement/internal/cz;

    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/ek;->a(Lcom/google/android/gms/measurement/internal/fb;)V

    .line 269
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIV:Lcom/google/android/gms/measurement/internal/cz;

    return-object v0
.end method

.method public final vz()Lcom/google/android/gms/measurement/internal/gw;
    .locals 1

    .line 264
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIT:Lcom/google/android/gms/measurement/internal/gw;

    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/ek;->a(Lcom/google/android/gms/measurement/internal/fb;)V

    .line 265
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIT:Lcom/google/android/gms/measurement/internal/gw;

    return-object v0
.end method

.method protected final yA()Z
    .locals 6

    .line 381
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/ek;->vt()V

    .line 383
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 384
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIX:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-wide v1, p0, Lcom/google/android/gms/measurement/internal/ek;->aIY:J

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-eqz v5, :cond_0

    if-eqz v0, :cond_5

    .line 385
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_5

    .line 386
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aBe:Lcom/google/android/gms/common/util/e;

    .line 387
    invoke-interface {v0}, Lcom/google/android/gms/common/util/e;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/gms/measurement/internal/ek;->aIY:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    cmp-long v4, v0, v2

    if-lez v4, :cond_5

    .line 389
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aBe:Lcom/google/android/gms/common/util/e;

    .line 390
    invoke-interface {v0}, Lcom/google/android/gms/common/util/e;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIY:J

    .line 396
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v0

    const-string v1, "android.permission.INTERNET"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/it;->cR(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    .line 397
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v0

    const-string v3, "android.permission.ACCESS_NETWORK_STATE"

    invoke-virtual {v0, v3}, Lcom/google/android/gms/measurement/internal/it;->cR(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 398
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->ask:Landroid/content/Context;

    .line 399
    invoke-static {v0}, Lcom/google/android/gms/common/c/c;->Y(Landroid/content/Context;)Lcom/google/android/gms/common/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/c/b;->my()Z

    move-result v0

    if-nez v0, :cond_1

    .line 400
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIH:Lcom/google/android/gms/measurement/internal/jb;

    .line 401
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/jb;->mL()Z

    move-result v0

    if-nez v0, :cond_1

    .line 402
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->ask:Landroid/content/Context;

    .line 403
    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/dz;->at(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 404
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->ask:Landroid/content/Context;

    .line 405
    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/it;->av(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    .line 406
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIX:Ljava/lang/Boolean;

    .line 407
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIX:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 409
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/cz;->getGmpAppId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/measurement/internal/cz;->xB()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/measurement/internal/it;->J(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 410
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ek;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cz;->xB()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    .line 411
    :cond_4
    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIX:Ljava/lang/Boolean;

    .line 412
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIX:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final yy()Z
    .locals 1

    .line 312
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIZ:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aIZ:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method final yz()V
    .locals 1

    .line 379
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ek;->aJd:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    return-void
.end method

.method final zza(Z)V
    .locals 0

    .line 310
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/ek;->aIZ:Ljava/lang/Boolean;

    return-void
.end method
