.class final Lcom/google/android/gms/measurement/internal/hj;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final synthetic aJO:Lcom/google/android/gms/internal/measurement/ln;

.field private final synthetic aJk:Lcom/google/android/gms/measurement/internal/zzn;

.field private final synthetic aKm:Lcom/google/android/gms/measurement/internal/gw;

.field private final synthetic aqy:Ljava/lang/String;

.field private final synthetic aqz:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/gw;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/measurement/internal/zzn;Lcom/google/android/gms/internal/measurement/ln;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/hj;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    iput-object p2, p0, Lcom/google/android/gms/measurement/internal/hj;->aqy:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/measurement/internal/hj;->aqz:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/measurement/internal/hj;->aJk:Lcom/google/android/gms/measurement/internal/zzn;

    iput-object p5, p0, Lcom/google/android/gms/measurement/internal/hj;->aJO:Lcom/google/android/gms/internal/measurement/ln;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    const-string v0, "Failed to get conditional properties"

    .line 2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/hj;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    .line 1320
    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/gw;->aKo:Lcom/google/android/gms/measurement/internal/cy;

    if-nez v2, :cond_0

    .line 5
    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/hj;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v2

    .line 2014
    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    .line 6
    iget-object v3, p0, Lcom/google/android/gms/measurement/internal/hj;->aqy:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/measurement/internal/hj;->aqz:Ljava/lang/String;

    .line 7
    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/hj;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/hj;->aJO:Lcom/google/android/gms/internal/measurement/ln;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/measurement/internal/it;->a(Lcom/google/android/gms/internal/measurement/ln;Ljava/util/ArrayList;)V

    return-void

    .line 10
    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/google/android/gms/measurement/internal/hj;->aqy:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/measurement/internal/hj;->aqz:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/measurement/internal/hj;->aJk:Lcom/google/android/gms/measurement/internal/zzn;

    .line 11
    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/gms/measurement/internal/cy;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/measurement/internal/zzn;)Ljava/util/List;

    move-result-object v2

    .line 12
    invoke-static {v2}, Lcom/google/android/gms/measurement/internal/it;->S(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v1

    .line 13
    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/hj;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    .line 2321
    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/gw;->yJ()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 14
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/hj;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/hj;->aJO:Lcom/google/android/gms/internal/measurement/ln;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/measurement/internal/it;->a(Lcom/google/android/gms/internal/measurement/ln;Ljava/util/ArrayList;)V

    return-void

    :catchall_0
    move-exception v0

    goto :goto_0

    :catch_0
    move-exception v2

    .line 17
    :try_start_2
    iget-object v3, p0, Lcom/google/android/gms/measurement/internal/hj;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v3

    .line 3014
    iget-object v3, v3, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    .line 18
    iget-object v4, p0, Lcom/google/android/gms/measurement/internal/hj;->aqy:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/measurement/internal/hj;->aqz:Ljava/lang/String;

    .line 19
    invoke-virtual {v3, v0, v4, v5, v2}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 20
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/hj;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/hj;->aJO:Lcom/google/android/gms/internal/measurement/ln;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/measurement/internal/it;->a(Lcom/google/android/gms/internal/measurement/ln;Ljava/util/ArrayList;)V

    return-void

    .line 22
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/hj;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/measurement/internal/hj;->aJO:Lcom/google/android/gms/internal/measurement/ln;

    invoke-virtual {v2, v3, v1}, Lcom/google/android/gms/measurement/internal/it;->a(Lcom/google/android/gms/internal/measurement/ln;Ljava/util/ArrayList;)V

    throw v0
.end method
