.class final Lcom/google/android/gms/measurement/internal/hf;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final synthetic aJk:Lcom/google/android/gms/measurement/internal/zzn;

.field private final synthetic aKm:Lcom/google/android/gms/measurement/internal/gw;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/gw;Lcom/google/android/gms/measurement/internal/zzn;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/hf;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    iput-object p2, p0, Lcom/google/android/gms/measurement/internal/hf;->aJk:Lcom/google/android/gms/measurement/internal/zzn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/hf;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    .line 1320
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/gw;->aKo:Lcom/google/android/gms/measurement/internal/cy;

    if-nez v0, :cond_0

    .line 4
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/hf;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 2014
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "Failed to send measurementEnabled to service"

    .line 4
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    return-void

    .line 6
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/hf;->aJk:Lcom/google/android/gms/measurement/internal/zzn;

    invoke-interface {v0, v1}, Lcom/google/android/gms/measurement/internal/cy;->b(Lcom/google/android/gms/measurement/internal/zzn;)V

    .line 7
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/hf;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    .line 2321
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/gw;->yJ()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    .line 10
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/hf;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 3014
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string v2, "Failed to send measurementEnabled to the service"

    .line 10
    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method
