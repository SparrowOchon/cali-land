.class final Lcom/google/android/gms/measurement/internal/gv;
.super Lcom/google/android/gms/measurement/internal/b;


# instance fields
.field private final synthetic aKm:Lcom/google/android/gms/measurement/internal/gw;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/gw;Lcom/google/android/gms/measurement/internal/fj;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/gv;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    invoke-direct {p0, p2}, Lcom/google/android/gms/measurement/internal/b;-><init>(Lcom/google/android/gms/measurement/internal/fj;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/gv;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    .line 2253
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 2254
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/gw;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2256
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 3022
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    const-string v2, "Inactivity, disconnecting from the service"

    .line 2256
    invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 2257
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/gw;->disconnect()V

    :cond_0
    return-void
.end method
