.class public Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;
.super Lcom/google/android/gms/internal/measurement/ll;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService$b;,
        Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService$a;
    }
.end annotation


# instance fields
.field aDi:Lcom/google/android/gms/measurement/internal/ek;

.field private aDo:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/gms/measurement/internal/fp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/ll;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 3
    new-instance v0, Landroidx/collection/ArrayMap;

    invoke-direct {v0}, Landroidx/collection/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDo:Ljava/util/Map;

    return-void
.end method

.method private final a(Lcom/google/android/gms/internal/measurement/ln;Ljava/lang/String;)V
    .locals 1

    .line 245
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/measurement/internal/it;->b(Lcom/google/android/gms/internal/measurement/ln;Ljava/lang/String;)V

    return-void
.end method

.method private final vt()V
    .locals 2

    .line 4
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    if-eqz v0, :cond_0

    return-void

    .line 5
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempting to perform action before initialize."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public beginAdUnitExposure(Ljava/lang/String;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 83
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vw()Lcom/google/android/gms/measurement/internal/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/measurement/internal/a;->beginAdUnitExposure(Ljava/lang/String;J)V

    return-void
.end method

.method public clearConditionalUserProperty(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 211
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 212
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 213
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v0

    .line 214
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/measurement/internal/fr;->clearConditionalUserProperty(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public endAdUnitExposure(Ljava/lang/String;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 86
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 87
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vw()Lcom/google/android/gms/measurement/internal/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/measurement/internal/a;->endAdUnitExposure(Ljava/lang/String;J)V

    return-void
.end method

.method public generateEventId(Lcom/google/android/gms/internal/measurement/ln;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 79
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/it;->zd()J

    move-result-wide v0

    .line 81
    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v2

    invoke-virtual {v2, p1, v0, v1}, Lcom/google/android/gms/measurement/internal/it;->a(Lcom/google/android/gms/internal/measurement/ln;J)V

    return-void
.end method

.method public getAppInstanceId(Lcom/google/android/gms/internal/measurement/ln;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 56
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 57
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 58
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/measurement/internal/ga;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/measurement/internal/ga;-><init>(Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;Lcom/google/android/gms/internal/measurement/ln;)V

    .line 59
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/ed;->f(Ljava/lang/Runnable;)V

    return-void
.end method

.method public getCachedAppInstanceId(Lcom/google/android/gms/internal/measurement/ln;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 53
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fr;->yE()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a(Lcom/google/android/gms/internal/measurement/ln;Ljava/lang/String;)V

    return-void
.end method

.method public getConditionalUserProperties(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/measurement/ln;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 216
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 217
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 218
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/measurement/internal/iw;

    invoke-direct {v1, p0, p3, p1, p2}, Lcom/google/android/gms/measurement/internal/iw;-><init>(Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;Lcom/google/android/gms/internal/measurement/ln;Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/ed;->f(Ljava/lang/Runnable;)V

    return-void
.end method

.method public getCurrentScreenClass(Lcom/google/android/gms/internal/measurement/ln;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 50
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fr;->getCurrentScreenClass()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a(Lcom/google/android/gms/internal/measurement/ln;Ljava/lang/String;)V

    return-void
.end method

.method public getCurrentScreenName(Lcom/google/android/gms/internal/measurement/ln;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 47
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 48
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fr;->getCurrentScreenName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a(Lcom/google/android/gms/internal/measurement/ln;Ljava/lang/String;)V

    return-void
.end method

.method public getDeepLink(Lcom/google/android/gms/internal/measurement/ln;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 61
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 64
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/measurement/internal/j;->aFC:Lcom/google/android/gms/measurement/internal/cv;

    const/4 v3, 0x0

    .line 66
    invoke-virtual {v1, v3, v2}, Lcom/google/android/gms/measurement/internal/jb;->d(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v1

    const-string v2, ""

    if-nez v1, :cond_0

    .line 68
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v0

    invoke-virtual {v0, p1, v2}, Lcom/google/android/gms/measurement/internal/it;->b(Lcom/google/android/gms/internal/measurement/ln;Ljava/lang/String;)V

    return-void

    .line 70
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dp;->aHm:Lcom/google/android/gms/measurement/internal/dt;

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/dt;->get()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-lez v1, :cond_1

    .line 71
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v0

    invoke-virtual {v0, p1, v2}, Lcom/google/android/gms/measurement/internal/it;->b(Lcom/google/android/gms/internal/measurement/ln;Ljava/lang/String;)V

    return-void

    .line 73
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dp;->aHm:Lcom/google/android/gms/measurement/internal/dt;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/util/e;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/measurement/internal/dt;->set(J)V

    .line 74
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/fr;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/measurement/internal/ek;->a(Lcom/google/android/gms/internal/measurement/ln;)V

    return-void
.end method

.method public getGmpAppId(Lcom/google/android/gms/internal/measurement/ln;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 76
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fr;->getGmpAppId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a(Lcom/google/android/gms/internal/measurement/ln;Ljava/lang/String;)V

    return-void
.end method

.method public getMaxUserProperties(Ljava/lang/String;Lcom/google/android/gms/internal/measurement/ln;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 41
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 42
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    .line 43
    invoke-static {p1}, Lcom/google/android/gms/common/internal/r;->aQ(Ljava/lang/String;)Ljava/lang/String;

    .line 45
    iget-object p1, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {p1}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object p1

    const/16 v0, 0x19

    invoke-virtual {p1, p2, v0}, Lcom/google/android/gms/measurement/internal/it;->a(Lcom/google/android/gms/internal/measurement/ln;I)V

    return-void
.end method

.method public getTestFlag(Lcom/google/android/gms/internal/measurement/ln;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 221
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    if-eqz p2, :cond_4

    const/4 v0, 0x1

    if-eq p2, v0, :cond_3

    const/4 v0, 0x2

    if-eq p2, v0, :cond_2

    const/4 v0, 0x3

    if-eq p2, v0, :cond_1

    const/4 v0, 0x4

    if-eq p2, v0, :cond_0

    goto :goto_0

    .line 241
    :cond_0
    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 242
    invoke-virtual {p2}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object p2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 243
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v0

    .line 6012
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 6013
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/measurement/internal/fq;

    invoke-direct {v3, v0, v1}, Lcom/google/android/gms/measurement/internal/fq;-><init>(Lcom/google/android/gms/measurement/internal/fr;Ljava/util/concurrent/atomic/AtomicReference;)V

    const-string v0, "boolean test flag value"

    .line 6014
    invoke-virtual {v2, v1, v0, v3}, Lcom/google/android/gms/measurement/internal/ed;->a(Ljava/util/concurrent/atomic/AtomicReference;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 243
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p2, p1, v0}, Lcom/google/android/gms/measurement/internal/it;->a(Lcom/google/android/gms/internal/measurement/ln;Z)V

    :goto_0
    return-void

    .line 239
    :cond_1
    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {p2}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object p2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v0

    .line 5024
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 5025
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/measurement/internal/ge;

    invoke-direct {v3, v0, v1}, Lcom/google/android/gms/measurement/internal/ge;-><init>(Lcom/google/android/gms/measurement/internal/fr;Ljava/util/concurrent/atomic/AtomicReference;)V

    const-string v0, "int test flag value"

    .line 5026
    invoke-virtual {v2, v1, v0, v3}, Lcom/google/android/gms/measurement/internal/ed;->a(Ljava/util/concurrent/atomic/AtomicReference;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 239
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p2, p1, v0}, Lcom/google/android/gms/measurement/internal/it;->a(Lcom/google/android/gms/internal/measurement/ln;I)V

    return-void

    .line 229
    :cond_2
    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 230
    invoke-virtual {p2}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object p2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 231
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v0

    .line 4028
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 4029
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/measurement/internal/gd;

    invoke-direct {v3, v0, v1}, Lcom/google/android/gms/measurement/internal/gd;-><init>(Lcom/google/android/gms/measurement/internal/fr;Ljava/util/concurrent/atomic/AtomicReference;)V

    const-string v0, "double test flag value"

    .line 4030
    invoke-virtual {v2, v1, v0, v3}, Lcom/google/android/gms/measurement/internal/ed;->a(Ljava/util/concurrent/atomic/AtomicReference;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    .line 231
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 232
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "r"

    .line 233
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 234
    :try_start_0
    invoke-interface {p1, v2}, Lcom/google/android/gms/internal/measurement/ln;->d(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 237
    iget-object p2, p2, Lcom/google/android/gms/measurement/internal/it;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {p2}, Lcom/google/android/gms/measurement/internal/ek;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p2

    .line 5017
    iget-object p2, p2, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    const-string v0, "Error returning double value to wrapper"

    .line 237
    invoke-virtual {p2, v0, p1}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    return-void

    .line 227
    :cond_3
    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {p2}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object p2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v0

    .line 4020
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 4021
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/measurement/internal/gb;

    invoke-direct {v3, v0, v1}, Lcom/google/android/gms/measurement/internal/gb;-><init>(Lcom/google/android/gms/measurement/internal/fr;Ljava/util/concurrent/atomic/AtomicReference;)V

    const-string v0, "long test flag value"

    .line 4022
    invoke-virtual {v2, v1, v0, v3}, Lcom/google/android/gms/measurement/internal/ed;->a(Ljava/util/concurrent/atomic/AtomicReference;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 227
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p2, p1, v0, v1}, Lcom/google/android/gms/measurement/internal/it;->a(Lcom/google/android/gms/internal/measurement/ln;J)V

    return-void

    .line 223
    :cond_4
    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 224
    invoke-virtual {p2}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object p2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 225
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v0

    .line 4016
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 4017
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/measurement/internal/fy;

    invoke-direct {v3, v0, v1}, Lcom/google/android/gms/measurement/internal/fy;-><init>(Lcom/google/android/gms/measurement/internal/fr;Ljava/util/concurrent/atomic/AtomicReference;)V

    const-string v0, "String test flag value"

    .line 4018
    invoke-virtual {v2, v1, v0, v3}, Lcom/google/android/gms/measurement/internal/ed;->a(Ljava/util/concurrent/atomic/AtomicReference;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 225
    invoke-virtual {p2, p1, v0}, Lcom/google/android/gms/measurement/internal/it;->b(Lcom/google/android/gms/internal/measurement/ln;Ljava/lang/String;)V

    return-void
.end method

.method public getUserProperties(Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/gms/internal/measurement/ln;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 172
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 174
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v0

    new-instance v7, Lcom/google/android/gms/measurement/internal/ha;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p4

    move-object v4, p1

    move-object v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/measurement/internal/ha;-><init>(Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;Lcom/google/android/gms/internal/measurement/ln;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 175
    invoke-virtual {v0, v7}, Lcom/google/android/gms/measurement/internal/ed;->f(Ljava/lang/Runnable;)V

    return-void
.end method

.method public initForTests(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 89
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    return-void
.end method

.method public initialize(Lcom/google/android/gms/a/a;Lcom/google/android/gms/internal/measurement/zzx;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 7
    invoke-static {p1}, Lcom/google/android/gms/a/b;->a(Lcom/google/android/gms/a/a;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    .line 8
    iget-object p3, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    if-nez p3, :cond_0

    .line 9
    invoke-static {p1, p2}, Lcom/google/android/gms/measurement/internal/ek;->a(Landroid/content/Context;Lcom/google/android/gms/internal/measurement/zzx;)Lcom/google/android/gms/measurement/internal/ek;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    return-void

    .line 10
    :cond_0
    invoke-virtual {p3}, Lcom/google/android/gms/measurement/internal/ek;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 1017
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    const-string p2, "Attempting to initialize multiple times"

    .line 10
    invoke-virtual {p1, p2}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    return-void
.end method

.method public isDataCollectionEnabled(Lcom/google/android/gms/internal/measurement/ln;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 250
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 251
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 252
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/measurement/internal/iv;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/measurement/internal/iv;-><init>(Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;Lcom/google/android/gms/internal/measurement/ln;)V

    .line 253
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/ed;->f(Ljava/lang/Runnable;)V

    return-void
.end method

.method public logEvent(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;ZZJ)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 12
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    move-object v0, p0

    .line 13
    iget-object v1, v0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v2

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move v7, p5

    move-wide/from16 v8, p6

    invoke-virtual/range {v2 .. v9}, Lcom/google/android/gms/measurement/internal/fr;->logEvent(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;ZZJ)V

    return-void
.end method

.method public logEventAndBundle(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lcom/google/android/gms/internal/measurement/ln;J)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 91
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 92
    invoke-static {p2}, Lcom/google/android/gms/common/internal/r;->aQ(Ljava/lang/String;)Ljava/lang/String;

    .line 94
    new-instance v0, Landroid/os/Bundle;

    if-eqz p3, :cond_0

    invoke-direct {v0, p3}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    goto :goto_0

    :cond_0
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    :goto_0
    const-string v4, "app"

    const-string v1, "_o"

    .line 95
    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    new-instance v0, Lcom/google/android/gms/measurement/internal/zzai;

    new-instance v3, Lcom/google/android/gms/measurement/internal/zzah;

    invoke-direct {v3, p3}, Lcom/google/android/gms/measurement/internal/zzah;-><init>(Landroid/os/Bundle;)V

    move-object v1, v0

    move-object v2, p2

    move-wide v5, p5

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/measurement/internal/zzai;-><init>(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/zzah;Ljava/lang/String;J)V

    .line 97
    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 98
    invoke-virtual {p2}, Lcom/google/android/gms/measurement/internal/ek;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object p2

    new-instance p3, Lcom/google/android/gms/measurement/internal/ib;

    invoke-direct {p3, p0, p4, v0, p1}, Lcom/google/android/gms/measurement/internal/ib;-><init>(Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;Lcom/google/android/gms/internal/measurement/ln;Lcom/google/android/gms/measurement/internal/zzai;Ljava/lang/String;)V

    .line 99
    invoke-virtual {p2, p3}, Lcom/google/android/gms/measurement/internal/ed;->f(Ljava/lang/Runnable;)V

    return-void
.end method

.method public logHealthData(ILjava/lang/String;Lcom/google/android/gms/a/a;Lcom/google/android/gms/a/a;Lcom/google/android/gms/a/a;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 177
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    const/4 v0, 0x0

    if-nez p3, :cond_0

    move-object v6, v0

    goto :goto_0

    .line 178
    :cond_0
    invoke-static {p3}, Lcom/google/android/gms/a/b;->a(Lcom/google/android/gms/a/a;)Ljava/lang/Object;

    move-result-object p3

    move-object v6, p3

    :goto_0
    if-nez p4, :cond_1

    move-object v7, v0

    goto :goto_1

    .line 179
    :cond_1
    invoke-static {p4}, Lcom/google/android/gms/a/b;->a(Lcom/google/android/gms/a/a;)Ljava/lang/Object;

    move-result-object p3

    move-object v7, p3

    :goto_1
    if-nez p5, :cond_2

    goto :goto_2

    .line 180
    :cond_2
    invoke-static {p5}, Lcom/google/android/gms/a/b;->a(Lcom/google/android/gms/a/a;)Ljava/lang/Object;

    move-result-object v0

    :goto_2
    move-object v8, v0

    .line 181
    iget-object p3, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {p3}, Lcom/google/android/gms/measurement/internal/ek;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    const/4 v3, 0x1

    const/4 v4, 0x0

    move v2, p1

    move-object v5, p2

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/gms/measurement/internal/dh;->a(IZZLjava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public onActivityCreated(Lcom/google/android/gms/a/a;Landroid/os/Bundle;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 119
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 120
    iget-object p3, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 121
    invoke-virtual {p3}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object p3

    .line 122
    iget-object p3, p3, Lcom/google/android/gms/measurement/internal/fr;->aJA:Lcom/google/android/gms/measurement/internal/gk;

    if-eqz p3, :cond_0

    .line 125
    iget-object p4, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {p4}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object p4

    invoke-virtual {p4}, Lcom/google/android/gms/measurement/internal/fr;->yC()V

    .line 126
    invoke-static {p1}, Lcom/google/android/gms/a/b;->a(Lcom/google/android/gms/a/a;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    invoke-interface {p3, p1, p2}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onActivityDestroyed(Lcom/google/android/gms/a/a;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 128
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 129
    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 130
    invoke-virtual {p2}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object p2

    .line 131
    iget-object p2, p2, Lcom/google/android/gms/measurement/internal/fr;->aJA:Lcom/google/android/gms/measurement/internal/gk;

    if-eqz p2, :cond_0

    .line 134
    iget-object p3, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {p3}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object p3

    invoke-virtual {p3}, Lcom/google/android/gms/measurement/internal/fr;->yC()V

    .line 135
    invoke-static {p1}, Lcom/google/android/gms/a/b;->a(Lcom/google/android/gms/a/a;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    invoke-interface {p2, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityDestroyed(Landroid/app/Activity;)V

    :cond_0
    return-void
.end method

.method public onActivityPaused(Lcom/google/android/gms/a/a;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 137
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 138
    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 139
    invoke-virtual {p2}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object p2

    .line 140
    iget-object p2, p2, Lcom/google/android/gms/measurement/internal/fr;->aJA:Lcom/google/android/gms/measurement/internal/gk;

    if-eqz p2, :cond_0

    .line 143
    iget-object p3, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {p3}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object p3

    invoke-virtual {p3}, Lcom/google/android/gms/measurement/internal/fr;->yC()V

    .line 144
    invoke-static {p1}, Lcom/google/android/gms/a/b;->a(Lcom/google/android/gms/a/a;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    invoke-interface {p2, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityPaused(Landroid/app/Activity;)V

    :cond_0
    return-void
.end method

.method public onActivityResumed(Lcom/google/android/gms/a/a;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 146
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 147
    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 148
    invoke-virtual {p2}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object p2

    .line 149
    iget-object p2, p2, Lcom/google/android/gms/measurement/internal/fr;->aJA:Lcom/google/android/gms/measurement/internal/gk;

    if-eqz p2, :cond_0

    .line 152
    iget-object p3, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {p3}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object p3

    invoke-virtual {p3}, Lcom/google/android/gms/measurement/internal/fr;->yC()V

    .line 153
    invoke-static {p1}, Lcom/google/android/gms/a/b;->a(Lcom/google/android/gms/a/a;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    invoke-interface {p2, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityResumed(Landroid/app/Activity;)V

    :cond_0
    return-void
.end method

.method public onActivitySaveInstanceState(Lcom/google/android/gms/a/a;Lcom/google/android/gms/internal/measurement/ln;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 155
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 156
    iget-object p3, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 157
    invoke-virtual {p3}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object p3

    .line 158
    iget-object p3, p3, Lcom/google/android/gms/measurement/internal/fr;->aJA:Lcom/google/android/gms/measurement/internal/gk;

    .line 160
    new-instance p4, Landroid/os/Bundle;

    invoke-direct {p4}, Landroid/os/Bundle;-><init>()V

    if-eqz p3, :cond_0

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fr;->yC()V

    .line 163
    invoke-static {p1}, Lcom/google/android/gms/a/b;->a(Lcom/google/android/gms/a/a;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    invoke-interface {p3, p1, p4}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 164
    :cond_0
    :try_start_0
    invoke-interface {p2, p4}, Lcom/google/android/gms/internal/measurement/ln;->d(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 167
    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {p2}, Lcom/google/android/gms/measurement/internal/ek;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p2

    .line 3017
    iget-object p2, p2, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    const-string p3, "Error returning bundle value to wrapper"

    .line 167
    invoke-virtual {p2, p3, p1}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public onActivityStarted(Lcom/google/android/gms/a/a;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 101
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 102
    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 103
    invoke-virtual {p2}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object p2

    .line 104
    iget-object p2, p2, Lcom/google/android/gms/measurement/internal/fr;->aJA:Lcom/google/android/gms/measurement/internal/gk;

    if-eqz p2, :cond_0

    .line 107
    iget-object p3, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {p3}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object p3

    invoke-virtual {p3}, Lcom/google/android/gms/measurement/internal/fr;->yC()V

    .line 108
    invoke-static {p1}, Lcom/google/android/gms/a/b;->a(Lcom/google/android/gms/a/a;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    invoke-interface {p2, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityStarted(Landroid/app/Activity;)V

    :cond_0
    return-void
.end method

.method public onActivityStopped(Lcom/google/android/gms/a/a;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 110
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 111
    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 112
    invoke-virtual {p2}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object p2

    .line 113
    iget-object p2, p2, Lcom/google/android/gms/measurement/internal/fr;->aJA:Lcom/google/android/gms/measurement/internal/gk;

    if-eqz p2, :cond_0

    .line 116
    iget-object p3, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {p3}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object p3

    invoke-virtual {p3}, Lcom/google/android/gms/measurement/internal/fr;->yC()V

    .line 117
    invoke-static {p1}, Lcom/google/android/gms/a/b;->a(Lcom/google/android/gms/a/a;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    invoke-interface {p2, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityStopped(Landroid/app/Activity;)V

    :cond_0
    return-void
.end method

.method public performAction(Landroid/os/Bundle;Lcom/google/android/gms/internal/measurement/ln;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 169
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    const/4 p1, 0x0

    .line 170
    invoke-interface {p2, p1}, Lcom/google/android/gms/internal/measurement/ln;->d(Landroid/os/Bundle;)V

    return-void
.end method

.method public registerOnMeasurementEventListener(Lcom/google/android/gms/internal/measurement/lo;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 190
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDo:Ljava/util/Map;

    invoke-interface {p1}, Lcom/google/android/gms/internal/measurement/lo;->id()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/measurement/internal/fp;

    if-nez v0, :cond_0

    .line 193
    new-instance v0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService$a;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService$a;-><init>(Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;Lcom/google/android/gms/internal/measurement/lo;)V

    .line 194
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDo:Ljava/util/Map;

    invoke-interface {p1}, Lcom/google/android/gms/internal/measurement/lo;->id()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    :cond_0
    iget-object p1, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {p1}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/google/android/gms/measurement/internal/fr;->a(Lcom/google/android/gms/measurement/internal/fp;)V

    return-void
.end method

.method public resetAnalyticsData(J)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 33
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v0

    const/4 v1, 0x0

    .line 1428
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/fr;->cI(Ljava/lang/String;)V

    .line 1429
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/measurement/internal/fv;

    invoke-direct {v2, v0, p1, p2}, Lcom/google/android/gms/measurement/internal/fv;-><init>(Lcom/google/android/gms/measurement/internal/fr;J)V

    .line 1430
    invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/ed;->f(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setConditionalUserProperty(Landroid/os/Bundle;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 205
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    if-nez p1, :cond_0

    .line 207
    iget-object p1, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {p1}, Lcom/google/android/gms/measurement/internal/ek;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 4014
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string p2, "Conditional user property must not be null"

    .line 207
    invoke-virtual {p1, p2}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    return-void

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/measurement/internal/fr;->setConditionalUserProperty(Landroid/os/Bundle;J)V

    return-void
.end method

.method public setCurrentScreen(Lcom/google/android/gms/a/a;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 25
    iget-object p4, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 26
    invoke-virtual {p4}, Lcom/google/android/gms/measurement/internal/ek;->vA()Lcom/google/android/gms/measurement/internal/gr;

    move-result-object p4

    .line 27
    invoke-static {p1}, Lcom/google/android/gms/a/b;->a(Lcom/google/android/gms/a/a;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p4, p1, p2, p3}, Lcom/google/android/gms/measurement/internal/gr;->setCurrentScreen(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setDataCollectionEnabled(Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 247
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/measurement/internal/fr;->zza(Z)V

    return-void
.end method

.method public setEventInterceptor(Lcom/google/android/gms/internal/measurement/lo;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 183
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService$b;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService$b;-><init>(Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;Lcom/google/android/gms/internal/measurement/lo;)V

    .line 186
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fb;->vt()V

    .line 187
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object p1

    new-instance v2, Lcom/google/android/gms/measurement/internal/fu;

    invoke-direct {v2, v0, v1}, Lcom/google/android/gms/measurement/internal/fu;-><init>(Lcom/google/android/gms/measurement/internal/fr;Lcom/google/android/gms/measurement/internal/fm;)V

    .line 188
    invoke-virtual {p1, v2}, Lcom/google/android/gms/measurement/internal/ed;->f(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setInstanceIdProvider(Lcom/google/android/gms/internal/measurement/lt;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 203
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    return-void
.end method

.method public setMeasurementEnabled(ZJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 30
    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {p2}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/google/android/gms/measurement/internal/fr;->O(Z)V

    return-void
.end method

.method public setMinimumSessionDuration(J)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 36
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v0

    .line 2073
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/measurement/internal/gi;

    invoke-direct {v2, v0, p1, p2}, Lcom/google/android/gms/measurement/internal/gi;-><init>(Lcom/google/android/gms/measurement/internal/fr;J)V

    .line 2074
    invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/ed;->f(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setSessionTimeoutDuration(J)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 39
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v0

    .line 2077
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/measurement/internal/gh;

    invoke-direct {v2, v0, p1, p2}, Lcom/google/android/gms/measurement/internal/gh;-><init>(Lcom/google/android/gms/measurement/internal/fr;J)V

    .line 2078
    invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/ed;->f(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setUserId(Ljava/lang/String;J)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 20
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 21
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "_id"

    const/4 v5, 0x1

    move-object v4, p1

    move-wide v6, p2

    .line 22
    invoke-virtual/range {v1 .. v7}, Lcom/google/android/gms/measurement/internal/fr;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;ZJ)V

    return-void
.end method

.method public setUserProperty(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/a/a;ZJ)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 16
    invoke-static {p3}, Lcom/google/android/gms/a/b;->a(Lcom/google/android/gms/a/a;)Ljava/lang/Object;

    move-result-object v3

    .line 17
    iget-object p3, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {p3}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move v4, p4

    move-wide v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/measurement/internal/fr;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;ZJ)V

    return-void
.end method

.method public unregisterOnMeasurementEventListener(Lcom/google/android/gms/internal/measurement/lo;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 197
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->vt()V

    .line 198
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDo:Ljava/util/Map;

    invoke-interface {p1}, Lcom/google/android/gms/internal/measurement/lo;->id()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/measurement/internal/fp;

    if-nez v0, :cond_0

    .line 200
    new-instance v0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService$a;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService$a;-><init>(Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;Lcom/google/android/gms/internal/measurement/lo;)V

    .line 201
    :cond_0
    iget-object p1, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {p1}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/google/android/gms/measurement/internal/fr;->b(Lcom/google/android/gms/measurement/internal/fp;)V

    return-void
.end method
