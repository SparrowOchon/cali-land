.class final Lcom/google/android/gms/measurement/internal/gk;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# instance fields
.field private final synthetic aJz:Lcom/google/android/gms/measurement/internal/fr;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/measurement/internal/fr;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/measurement/internal/fr;B)V
    .locals 0

    .line 98
    invoke-direct {p0, p1}, Lcom/google/android/gms/measurement/internal/gk;-><init>(Lcom/google/android/gms/measurement/internal/fr;)V

    return-void
.end method


# virtual methods
.method public final onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 19

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    const-string v0, "referrer"

    .line 4
    :try_start_0
    iget-object v4, v1, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v4}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v4

    .line 1022
    iget-object v4, v4, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    const-string v5, "onActivityCreated"

    .line 4
    invoke-virtual {v4, v5}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 5
    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v4, :cond_0

    .line 7
    iget-object v0, v1, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cb;->vA()Lcom/google/android/gms/measurement/internal/gr;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/measurement/internal/gr;->onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V

    return-void

    .line 9
    :cond_0
    :try_start_1
    invoke-virtual {v4}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    if-eqz v5, :cond_13

    .line 10
    invoke-virtual {v5}, Landroid/net/Uri;->isHierarchical()Z

    move-result v6

    if-nez v6, :cond_1

    goto/16 :goto_8

    .line 13
    :cond_1
    iget-object v6, v1, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v6}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    invoke-static {v4}, Lcom/google/android/gms/measurement/internal/it;->c(Landroid/content/Intent;)Z

    move-result v4
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v6, "auto"

    if-eqz v4, :cond_2

    :try_start_2
    const-string v4, "gs"

    goto :goto_0

    :cond_2
    move-object v4, v6

    .line 17
    :goto_0
    invoke-virtual {v5, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 18
    iget-object v8, v1, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v8}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v8

    sget-object v9, Lcom/google/android/gms/measurement/internal/j;->aFE:Lcom/google/android/gms/measurement/internal/cv;

    invoke-virtual {v8, v9}, Lcom/google/android/gms/measurement/internal/jb;->a(Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v8
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-string v9, "Activity created with data \'referrer\' without required params"

    const-string v10, "utm_medium"

    const-string v11, "_cis"

    const-string v12, "utm_source"

    const-string v13, "utm_campaign"

    const-string v15, "gclid"

    if-nez v8, :cond_4

    :try_start_3
    iget-object v8, v1, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    .line 19
    invoke-virtual {v8}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v8

    sget-object v14, Lcom/google/android/gms/measurement/internal/j;->aFG:Lcom/google/android/gms/measurement/internal/cv;

    invoke-virtual {v8, v14}, Lcom/google/android/gms/measurement/internal/jb;->a(Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v8

    if-eqz v8, :cond_3

    goto :goto_2

    :cond_3
    :goto_1
    move-object/from16 v17, v9

    const/4 v14, 0x0

    goto :goto_4

    .line 21
    :cond_4
    :goto_2
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_5

    goto :goto_1

    .line 23
    :cond_5
    invoke-virtual {v7, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_6

    .line 24
    invoke-virtual {v7, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_6

    .line 25
    invoke-virtual {v7, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_6

    .line 26
    invoke-virtual {v7, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_6

    .line 27
    iget-object v0, v1, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 2021
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    .line 27
    invoke-virtual {v0, v9}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    goto :goto_1

    .line 29
    :cond_6
    iget-object v8, v1, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    .line 30
    invoke-virtual {v8}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v8

    const-string v14, "https://google.com/search?"

    move-object/from16 v17, v9

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v18

    if-eqz v18, :cond_7

    invoke-virtual {v14, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    goto :goto_3

    :cond_7
    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v14}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 31
    :goto_3
    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/gms/measurement/internal/it;->s(Landroid/net/Uri;)Landroid/os/Bundle;

    move-result-object v8

    if-eqz v8, :cond_8

    .line 33
    invoke-virtual {v8, v11, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    move-object v14, v8

    :goto_4
    const/4 v0, 0x1

    if-nez v3, :cond_b

    .line 38
    iget-object v9, v1, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v9}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v9

    invoke-virtual {v9, v5}, Lcom/google/android/gms/measurement/internal/it;->s(Landroid/net/Uri;)Landroid/os/Bundle;

    move-result-object v5

    if-eqz v5, :cond_a

    const-string v9, "intent"

    .line 40
    invoke-virtual {v5, v11, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    iget-object v9, v1, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v9}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v9

    sget-object v11, Lcom/google/android/gms/measurement/internal/j;->aFE:Lcom/google/android/gms/measurement/internal/cv;

    invoke-virtual {v9, v11}, Lcom/google/android/gms/measurement/internal/jb;->a(Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 42
    invoke-virtual {v5, v15}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_9

    if-eqz v14, :cond_9

    .line 43
    invoke-virtual {v14, v15}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_9

    const-string v9, "_cer"

    const-string v11, "gclid=%s"

    new-array v8, v0, [Ljava/lang/Object;

    .line 45
    invoke-virtual {v14, v15}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    const/16 v18, 0x0

    aput-object v16, v8, v18

    invoke-static {v11, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 46
    invoke-virtual {v5, v9, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    :cond_9
    const/16 v18, 0x0

    .line 47
    :goto_5
    iget-object v8, v1, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    const-string v9, "_cmp"

    invoke-virtual {v8, v4, v9, v5}, Lcom/google/android/gms/measurement/internal/fr;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_6

    :cond_a
    const/16 v18, 0x0

    goto :goto_6

    :cond_b
    const/16 v18, 0x0

    const/4 v5, 0x0

    .line 48
    :goto_6
    iget-object v4, v1, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v4}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v4

    sget-object v8, Lcom/google/android/gms/measurement/internal/j;->aFG:Lcom/google/android/gms/measurement/internal/cv;

    invoke-virtual {v4, v8}, Lcom/google/android/gms/measurement/internal/jb;->a(Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v4

    if-eqz v4, :cond_d

    if-eqz v14, :cond_d

    .line 49
    invoke-virtual {v14, v15}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_d

    if-eqz v5, :cond_c

    .line 50
    invoke-virtual {v5, v15}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_d

    .line 51
    :cond_c
    iget-object v4, v1, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    const-string v5, "_lgclid"

    .line 52
    invoke-virtual {v14, v15}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 53
    invoke-virtual {v4, v6, v5, v8}, Lcom/google/android/gms/measurement/internal/fr;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 54
    :cond_d
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v4, :cond_e

    .line 55
    iget-object v0, v1, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cb;->vA()Lcom/google/android/gms/measurement/internal/gr;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/measurement/internal/gr;->onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V

    return-void

    .line 58
    :cond_e
    :try_start_4
    invoke-virtual {v7, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 59
    invoke-virtual {v7, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_10

    .line 60
    invoke-virtual {v7, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_10

    .line 61
    invoke-virtual {v7, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_10

    const-string v4, "utm_term"

    .line 62
    invoke-virtual {v7, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_10

    const-string v4, "utm_content"

    .line 63
    invoke-virtual {v7, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_f

    goto :goto_7

    :cond_f
    const/4 v0, 0x0

    :cond_10
    :goto_7
    if-nez v0, :cond_11

    .line 65
    iget-object v0, v1, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 3021
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    move-object/from16 v4, v17

    .line 65
    invoke-virtual {v0, v4}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 66
    iget-object v0, v1, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cb;->vA()Lcom/google/android/gms/measurement/internal/gr;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/measurement/internal/gr;->onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V

    return-void

    .line 68
    :cond_11
    :try_start_5
    iget-object v0, v1, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 4021
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    const-string v4, "Activity created with referrer"

    .line 68
    invoke-virtual {v0, v4, v7}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    .line 70
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 71
    iget-object v0, v1, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    const-string v4, "_ldl"

    invoke-virtual {v0, v6, v4, v7}, Lcom/google/android/gms/measurement/internal/fr;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 75
    :cond_12
    iget-object v0, v1, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cb;->vA()Lcom/google/android/gms/measurement/internal/gr;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/measurement/internal/gr;->onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V

    return-void

    .line 11
    :cond_13
    :goto_8
    iget-object v0, v1, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cb;->vA()Lcom/google/android/gms/measurement/internal/gr;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/measurement/internal/gr;->onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V

    return-void

    :catchall_0
    move-exception v0

    goto :goto_9

    :catch_0
    move-exception v0

    .line 78
    :try_start_6
    iget-object v4, v1, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v4}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v4

    .line 5014
    iget-object v4, v4, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string v5, "Throwable caught in onActivityCreated"

    .line 78
    invoke-virtual {v4, v5, v0}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 79
    iget-object v0, v1, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cb;->vA()Lcom/google/android/gms/measurement/internal/gr;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/measurement/internal/gr;->onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V

    return-void

    .line 81
    :goto_9
    iget-object v4, v1, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v4}, Lcom/google/android/gms/measurement/internal/cb;->vA()Lcom/google/android/gms/measurement/internal/gr;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Lcom/google/android/gms/measurement/internal/gr;->onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V

    goto :goto_b

    :goto_a
    throw v0

    :goto_b
    goto :goto_a
.end method

.method public final onActivityDestroyed(Landroid/app/Activity;)V
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cb;->vA()Lcom/google/android/gms/measurement/internal/gr;

    move-result-object v0

    .line 5132
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/gr;->aKa:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final onActivityPaused(Landroid/app/Activity;)V
    .locals 4

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cb;->vA()Lcom/google/android/gms/measurement/internal/gr;

    move-result-object v0

    .line 6115
    invoke-virtual {v0, p1}, Lcom/google/android/gms/measurement/internal/gr;->e(Landroid/app/Activity;)Lcom/google/android/gms/measurement/internal/gs;

    move-result-object p1

    .line 6116
    iget-object v1, v0, Lcom/google/android/gms/measurement/internal/gr;->aJY:Lcom/google/android/gms/measurement/internal/gs;

    iput-object v1, v0, Lcom/google/android/gms/measurement/internal/gr;->aJZ:Lcom/google/android/gms/measurement/internal/gs;

    const/4 v1, 0x0

    .line 6117
    iput-object v1, v0, Lcom/google/android/gms/measurement/internal/gr;->aJY:Lcom/google/android/gms/measurement/internal/gs;

    .line 6118
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/measurement/internal/gt;

    invoke-direct {v2, v0, p1}, Lcom/google/android/gms/measurement/internal/gt;-><init>(Lcom/google/android/gms/measurement/internal/gr;Lcom/google/android/gms/measurement/internal/gs;)V

    .line 6119
    invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/ed;->f(Ljava/lang/Runnable;)V

    .line 85
    iget-object p1, p0, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {p1}, Lcom/google/android/gms/measurement/internal/cb;->vC()Lcom/google/android/gms/measurement/internal/hx;

    move-result-object p1

    .line 86
    invoke-virtual {p1}, Lcom/google/android/gms/measurement/internal/fh;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/e;->elapsedRealtime()J

    move-result-wide v0

    .line 87
    invoke-virtual {p1}, Lcom/google/android/gms/measurement/internal/fh;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/measurement/internal/ic;

    invoke-direct {v3, p1, v0, v1}, Lcom/google/android/gms/measurement/internal/ic;-><init>(Lcom/google/android/gms/measurement/internal/hx;J)V

    .line 88
    invoke-virtual {v2, v3}, Lcom/google/android/gms/measurement/internal/ed;->f(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final onActivityResumed(Landroid/app/Activity;)V
    .locals 4

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cb;->vA()Lcom/google/android/gms/measurement/internal/gr;

    move-result-object v0

    .line 7108
    invoke-virtual {v0, p1}, Lcom/google/android/gms/measurement/internal/gr;->e(Landroid/app/Activity;)Lcom/google/android/gms/measurement/internal/gs;

    move-result-object v1

    const/4 v2, 0x0

    .line 7109
    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/gms/measurement/internal/gr;->a(Landroid/app/Activity;Lcom/google/android/gms/measurement/internal/gs;Z)V

    .line 7110
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cb;->vw()Lcom/google/android/gms/measurement/internal/a;

    move-result-object p1

    .line 7111
    invoke-virtual {p1}, Lcom/google/android/gms/measurement/internal/fh;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/e;->elapsedRealtime()J

    move-result-wide v0

    .line 7112
    invoke-virtual {p1}, Lcom/google/android/gms/measurement/internal/fh;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/measurement/internal/db;

    invoke-direct {v3, p1, v0, v1}, Lcom/google/android/gms/measurement/internal/db;-><init>(Lcom/google/android/gms/measurement/internal/a;J)V

    .line 7113
    invoke-virtual {v2, v3}, Lcom/google/android/gms/measurement/internal/ed;->f(Ljava/lang/Runnable;)V

    .line 91
    iget-object p1, p0, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {p1}, Lcom/google/android/gms/measurement/internal/cb;->vC()Lcom/google/android/gms/measurement/internal/hx;

    move-result-object p1

    .line 92
    invoke-virtual {p1}, Lcom/google/android/gms/measurement/internal/fh;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/e;->elapsedRealtime()J

    move-result-wide v0

    .line 93
    invoke-virtual {p1}, Lcom/google/android/gms/measurement/internal/fh;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/measurement/internal/id;

    invoke-direct {v3, p1, v0, v1}, Lcom/google/android/gms/measurement/internal/id;-><init>(Lcom/google/android/gms/measurement/internal/hx;J)V

    .line 94
    invoke-virtual {v2, v3}, Lcom/google/android/gms/measurement/internal/ed;->f(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 4

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/gk;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cb;->vA()Lcom/google/android/gms/measurement/internal/gr;

    move-result-object v0

    if-eqz p2, :cond_0

    .line 7123
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/gr;->aKa:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/measurement/internal/gs;

    if-eqz p1, :cond_0

    .line 7126
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 7127
    iget-wide v1, p1, Lcom/google/android/gms/measurement/internal/gs;->aKf:J

    const-string v3, "id"

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 7128
    iget-object v1, p1, Lcom/google/android/gms/measurement/internal/gs;->aKd:Ljava/lang/String;

    const-string v2, "name"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 7129
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/gs;->aKe:Ljava/lang/String;

    const-string v1, "referrer_name"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "com.google.app_measurement.screen_service"

    .line 7130
    invoke-virtual {p2, p1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public final onActivityStarted(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method

.method public final onActivityStopped(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method
