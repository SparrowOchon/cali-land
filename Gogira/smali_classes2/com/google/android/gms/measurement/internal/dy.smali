.class final Lcom/google/android/gms/measurement/internal/dy;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final synthetic aHC:Lcom/google/android/gms/measurement/internal/ek;

.field private final synthetic aHD:Lcom/google/android/gms/measurement/internal/dh;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/ek;Lcom/google/android/gms/measurement/internal/dh;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/dy;->aHC:Lcom/google/android/gms/measurement/internal/ek;

    iput-object p2, p0, Lcom/google/android/gms/measurement/internal/dy;->aHD:Lcom/google/android/gms/measurement/internal/dh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/dy;->aHC:Lcom/google/android/gms/measurement/internal/ek;

    .line 1243
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/ek;->aIW:Lcom/google/android/gms/measurement/internal/du;

    if-nez v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/dy;->aHD:Lcom/google/android/gms/measurement/internal/dh;

    .line 2014
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "Install Referrer Reporter is null"

    .line 3
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    return-void

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/dy;->aHC:Lcom/google/android/gms/measurement/internal/ek;

    .line 2243
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/ek;->aIW:Lcom/google/android/gms/measurement/internal/du;

    .line 7
    iget-object v1, v0, Lcom/google/android/gms/measurement/internal/du;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/ek;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/du;->cp(Ljava/lang/String;)V

    return-void
.end method
