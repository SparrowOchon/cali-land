.class final synthetic Lcom/google/android/gms/measurement/internal/ej;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/measurement/internal/gl;


# instance fields
.field private final aIC:Lcom/google/android/gms/measurement/internal/ek;

.field private final aID:Lcom/google/android/gms/internal/measurement/ln;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/ek;Lcom/google/android/gms/internal/measurement/ln;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/ej;->aIC:Lcom/google/android/gms/measurement/internal/ek;

    iput-object p2, p0, Lcom/google/android/gms/measurement/internal/ej;->aID:Lcom/google/android/gms/internal/measurement/ln;

    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/Throwable;[B)V
    .locals 10

    const-string v0, "gclid"

    const-string v1, "deeplink"

    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/ej;->aIC:Lcom/google/android/gms/measurement/internal/ek;

    iget-object v3, p0, Lcom/google/android/gms/measurement/internal/ej;->aID:Lcom/google/android/gms/internal/measurement/ln;

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/16 v6, 0xc8

    if-eq p1, v6, :cond_0

    const/16 v6, 0xcc

    if-eq p1, v6, :cond_0

    const/16 v6, 0x130

    if-ne p1, v6, :cond_1

    :cond_0
    if-nez p2, :cond_1

    const/4 v6, 0x1

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    :goto_0
    const-string v7, ""

    if-nez v6, :cond_2

    .line 1446
    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/ek;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p3

    .line 2017
    iget-object p3, p3, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    .line 1448
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string v0, "Network Request for Deferred Deep Link failed. response, exception"

    .line 1449
    invoke-virtual {p3, v0, p1, p2}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1450
    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object p1

    invoke-virtual {p1, v3, v7}, Lcom/google/android/gms/measurement/internal/it;->b(Lcom/google/android/gms/internal/measurement/ln;Ljava/lang/String;)V

    return-void

    .line 1452
    :cond_2
    array-length p1, p3

    if-nez p1, :cond_3

    .line 1453
    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object p1

    invoke-virtual {p1, v3, v7}, Lcom/google/android/gms/measurement/internal/it;->b(Lcom/google/android/gms/internal/measurement/ln;Ljava/lang/String;)V

    return-void

    .line 1455
    :cond_3
    new-instance p1, Ljava/lang/String;

    invoke-direct {p1, p3}, Ljava/lang/String;-><init>([B)V

    .line 1456
    :try_start_0
    new-instance p2, Lorg/json/JSONObject;

    invoke-direct {p2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1458
    invoke-virtual {p2, v1, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1459
    invoke-virtual {p2, v0, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 1460
    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object p3

    .line 1462
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 1464
    invoke-virtual {p3}, Lcom/google/android/gms/measurement/internal/fh;->getContext()Landroid/content/Context;

    move-result-object p3

    .line 1465
    invoke-virtual {p3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p3

    new-instance v6, Landroid/content/Intent;

    const-string v8, "android.intent.action.VIEW"

    .line 1466
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-direct {v6, v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p3, v6, v5}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object p3

    if-eqz p3, :cond_4

    .line 1467
    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result p3

    if-nez p3, :cond_4

    goto :goto_1

    :cond_4
    const/4 v4, 0x0

    :goto_1
    if-nez v4, :cond_5

    .line 1469
    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/ek;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p3

    .line 3017
    iget-object p3, p3, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    const-string v0, "Deferred Deep Link validation failed. gclid, deep link"

    .line 1471
    invoke-virtual {p3, v0, p2, p1}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1472
    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object p1

    invoke-virtual {p1, v3, v7}, Lcom/google/android/gms/measurement/internal/it;->b(Lcom/google/android/gms/internal/measurement/ln;Ljava/lang/String;)V

    return-void

    .line 1474
    :cond_5
    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    .line 1475
    invoke-virtual {p3, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1476
    invoke-virtual {p3, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1477
    iget-object p2, v2, Lcom/google/android/gms/measurement/internal/ek;->aIP:Lcom/google/android/gms/measurement/internal/fr;

    const-string v0, "auto"

    const-string v1, "_cmp"

    invoke-virtual {p2, v0, v1, p3}, Lcom/google/android/gms/measurement/internal/fr;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1478
    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object p2

    invoke-virtual {p2, v3, p1}, Lcom/google/android/gms/measurement/internal/it;->b(Lcom/google/android/gms/internal/measurement/ln;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 1481
    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/ek;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p2

    .line 4014
    iget-object p2, p2, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string p3, "Failed to parse the Deferred Deep Link response. exception"

    .line 1481
    invoke-virtual {p2, p3, p1}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1482
    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object p1

    invoke-virtual {p1, v3, v7}, Lcom/google/android/gms/measurement/internal/it;->b(Lcom/google/android/gms/internal/measurement/ln;Ljava/lang/String;)V

    return-void
.end method
