.class public final synthetic Lcom/google/android/gms/measurement/internal/hw;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final aKE:Lcom/google/android/gms/measurement/internal/hu;

.field private final aKK:Lcom/google/android/gms/measurement/internal/dh;

.field private final aKL:Landroid/app/job/JobParameters;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/measurement/internal/hu;Lcom/google/android/gms/measurement/internal/dh;Landroid/app/job/JobParameters;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/hw;->aKE:Lcom/google/android/gms/measurement/internal/hu;

    iput-object p2, p0, Lcom/google/android/gms/measurement/internal/hw;->aKK:Lcom/google/android/gms/measurement/internal/dh;

    iput-object p3, p0, Lcom/google/android/gms/measurement/internal/hw;->aKL:Landroid/app/job/JobParameters;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/hw;->aKE:Lcom/google/android/gms/measurement/internal/hu;

    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/hw;->aKK:Lcom/google/android/gms/measurement/internal/dh;

    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/hw;->aKL:Landroid/app/job/JobParameters;

    .line 2022
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    const-string v3, "AppMeasurementJobService processed last upload request."

    .line 1074
    invoke-virtual {v1, v3}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 1075
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/hu;->aKH:Landroid/content/Context;

    check-cast v0, Lcom/google/android/gms/measurement/internal/hy;

    invoke-interface {v0, v2}, Lcom/google/android/gms/measurement/internal/hy;->a(Landroid/app/job/JobParameters;)V

    return-void
.end method
