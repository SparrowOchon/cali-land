.class Lcom/google/android/gms/measurement/internal/ig;
.super Lcom/google/android/gms/measurement/internal/fh;

# interfaces
.implements Lcom/google/android/gms/measurement/internal/fj;


# instance fields
.field protected final aGH:Lcom/google/android/gms/measurement/internal/ii;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/ii;)V
    .locals 1

    .line 3464
    iget-object v0, p1, Lcom/google/android/gms/measurement/internal/ii;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 1
    invoke-direct {p0, v0}, Lcom/google/android/gms/measurement/internal/fh;-><init>(Lcom/google/android/gms/measurement/internal/ek;)V

    .line 2
    invoke-static {p1}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/ig;->aGH:Lcom/google/android/gms/measurement/internal/ii;

    return-void
.end method


# virtual methods
.method public xK()Lcom/google/android/gms/measurement/internal/ip;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ig;->aGH:Lcom/google/android/gms/measurement/internal/ii;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ii;->xK()Lcom/google/android/gms/measurement/internal/ip;

    move-result-object v0

    return-object v0
.end method

.method public xL()Lcom/google/android/gms/measurement/internal/iz;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ig;->aGH:Lcom/google/android/gms/measurement/internal/ii;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ii;->xL()Lcom/google/android/gms/measurement/internal/iz;

    move-result-object v0

    return-object v0
.end method

.method public xM()Lcom/google/android/gms/measurement/internal/jg;
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ig;->aGH:Lcom/google/android/gms/measurement/internal/ii;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ii;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v0

    return-object v0
.end method

.method public xN()Lcom/google/android/gms/measurement/internal/ee;
    .locals 1

    .line 5
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ig;->aGH:Lcom/google/android/gms/measurement/internal/ii;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ii;->xN()Lcom/google/android/gms/measurement/internal/ee;

    move-result-object v0

    return-object v0
.end method
