.class public final Lcom/google/android/gms/measurement/internal/zzq;
.super Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/google/android/gms/measurement/internal/zzq;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public aAZ:Ljava/lang/String;

.field public aLN:Lcom/google/android/gms/measurement/internal/zzjn;

.field public aLO:J

.field public aLP:Ljava/lang/String;

.field public aLQ:Lcom/google/android/gms/measurement/internal/zzai;

.field public aLR:J

.field public aLS:Lcom/google/android/gms/measurement/internal/zzai;

.field public aLT:J

.field public aLU:Lcom/google/android/gms/measurement/internal/zzai;

.field public active:Z

.field public packageName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 43
    new-instance v0, Lcom/google/android/gms/measurement/internal/jc;

    invoke-direct {v0}, Lcom/google/android/gms/measurement/internal/jc;-><init>()V

    sput-object v0, Lcom/google/android/gms/measurement/internal/zzq;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/measurement/internal/zzq;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;-><init>()V

    .line 2
    invoke-static {p1}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3
    iget-object v0, p1, Lcom/google/android/gms/measurement/internal/zzq;->packageName:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/zzq;->packageName:Ljava/lang/String;

    .line 4
    iget-object v0, p1, Lcom/google/android/gms/measurement/internal/zzq;->aAZ:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/zzq;->aAZ:Ljava/lang/String;

    .line 5
    iget-object v0, p1, Lcom/google/android/gms/measurement/internal/zzq;->aLN:Lcom/google/android/gms/measurement/internal/zzjn;

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/zzq;->aLN:Lcom/google/android/gms/measurement/internal/zzjn;

    .line 6
    iget-wide v0, p1, Lcom/google/android/gms/measurement/internal/zzq;->aLO:J

    iput-wide v0, p0, Lcom/google/android/gms/measurement/internal/zzq;->aLO:J

    .line 7
    iget-boolean v0, p1, Lcom/google/android/gms/measurement/internal/zzq;->active:Z

    iput-boolean v0, p0, Lcom/google/android/gms/measurement/internal/zzq;->active:Z

    .line 8
    iget-object v0, p1, Lcom/google/android/gms/measurement/internal/zzq;->aLP:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/zzq;->aLP:Ljava/lang/String;

    .line 9
    iget-object v0, p1, Lcom/google/android/gms/measurement/internal/zzq;->aLQ:Lcom/google/android/gms/measurement/internal/zzai;

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/zzq;->aLQ:Lcom/google/android/gms/measurement/internal/zzai;

    .line 10
    iget-wide v0, p1, Lcom/google/android/gms/measurement/internal/zzq;->aLR:J

    iput-wide v0, p0, Lcom/google/android/gms/measurement/internal/zzq;->aLR:J

    .line 11
    iget-object v0, p1, Lcom/google/android/gms/measurement/internal/zzq;->aLS:Lcom/google/android/gms/measurement/internal/zzai;

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/zzq;->aLS:Lcom/google/android/gms/measurement/internal/zzai;

    .line 12
    iget-wide v0, p1, Lcom/google/android/gms/measurement/internal/zzq;->aLT:J

    iput-wide v0, p0, Lcom/google/android/gms/measurement/internal/zzq;->aLT:J

    .line 13
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/zzq;->aLU:Lcom/google/android/gms/measurement/internal/zzai;

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/zzq;->aLU:Lcom/google/android/gms/measurement/internal/zzai;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/measurement/internal/zzjn;JZLjava/lang/String;Lcom/google/android/gms/measurement/internal/zzai;JLcom/google/android/gms/measurement/internal/zzai;JLcom/google/android/gms/measurement/internal/zzai;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/zzq;->packageName:Ljava/lang/String;

    .line 17
    iput-object p2, p0, Lcom/google/android/gms/measurement/internal/zzq;->aAZ:Ljava/lang/String;

    .line 18
    iput-object p3, p0, Lcom/google/android/gms/measurement/internal/zzq;->aLN:Lcom/google/android/gms/measurement/internal/zzjn;

    .line 19
    iput-wide p4, p0, Lcom/google/android/gms/measurement/internal/zzq;->aLO:J

    .line 20
    iput-boolean p6, p0, Lcom/google/android/gms/measurement/internal/zzq;->active:Z

    .line 21
    iput-object p7, p0, Lcom/google/android/gms/measurement/internal/zzq;->aLP:Ljava/lang/String;

    .line 22
    iput-object p8, p0, Lcom/google/android/gms/measurement/internal/zzq;->aLQ:Lcom/google/android/gms/measurement/internal/zzai;

    .line 23
    iput-wide p9, p0, Lcom/google/android/gms/measurement/internal/zzq;->aLR:J

    .line 24
    iput-object p11, p0, Lcom/google/android/gms/measurement/internal/zzq;->aLS:Lcom/google/android/gms/measurement/internal/zzai;

    .line 25
    iput-wide p12, p0, Lcom/google/android/gms/measurement/internal/zzq;->aLT:J

    .line 26
    iput-object p14, p0, Lcom/google/android/gms/measurement/internal/zzq;->aLU:Lcom/google/android/gms/measurement/internal/zzai;

    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    const/16 v0, 0x4f45

    .line 1017
    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/safeparcel/b;->l(Landroid/os/Parcel;I)I

    move-result v0

    .line 30
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/zzq;->packageName:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-static {p1, v2, v1}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    .line 31
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/zzq;->aAZ:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-static {p1, v2, v1}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    .line 32
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/zzq;->aLN:Lcom/google/android/gms/measurement/internal/zzjn;

    const/4 v2, 0x4

    invoke-static {p1, v2, v1, p2}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;I)V

    .line 33
    iget-wide v1, p0, Lcom/google/android/gms/measurement/internal/zzq;->aLO:J

    const/4 v3, 0x5

    invoke-static {p1, v3, v1, v2}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;IJ)V

    .line 34
    iget-boolean v1, p0, Lcom/google/android/gms/measurement/internal/zzq;->active:Z

    const/4 v2, 0x6

    invoke-static {p1, v2, v1}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;IZ)V

    .line 35
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/zzq;->aLP:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-static {p1, v2, v1}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    .line 36
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/zzq;->aLQ:Lcom/google/android/gms/measurement/internal/zzai;

    const/16 v2, 0x8

    invoke-static {p1, v2, v1, p2}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;I)V

    .line 37
    iget-wide v1, p0, Lcom/google/android/gms/measurement/internal/zzq;->aLR:J

    const/16 v3, 0x9

    invoke-static {p1, v3, v1, v2}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;IJ)V

    .line 38
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/zzq;->aLS:Lcom/google/android/gms/measurement/internal/zzai;

    const/16 v2, 0xa

    invoke-static {p1, v2, v1, p2}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;I)V

    .line 39
    iget-wide v1, p0, Lcom/google/android/gms/measurement/internal/zzq;->aLT:J

    const/16 v3, 0xb

    invoke-static {p1, v3, v1, v2}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;IJ)V

    .line 40
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/zzq;->aLU:Lcom/google/android/gms/measurement/internal/zzai;

    const/16 v2, 0xc

    invoke-static {p1, v2, v1, p2}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;I)V

    .line 1018
    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/safeparcel/b;->m(Landroid/os/Parcel;I)V

    return-void
.end method
