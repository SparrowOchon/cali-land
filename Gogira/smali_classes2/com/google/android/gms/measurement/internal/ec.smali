.class final Lcom/google/android/gms/measurement/internal/ec;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final synthetic aHC:Lcom/google/android/gms/measurement/internal/ek;

.field private final synthetic aHD:Lcom/google/android/gms/measurement/internal/dh;

.field private final synthetic aIb:J

.field private final synthetic aIc:Landroid/os/Bundle;

.field private final synthetic aId:Landroid/content/BroadcastReceiver$PendingResult;

.field private final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/ek;JLandroid/os/Bundle;Landroid/content/Context;Lcom/google/android/gms/measurement/internal/dh;Landroid/content/BroadcastReceiver$PendingResult;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/ec;->aHC:Lcom/google/android/gms/measurement/internal/ek;

    iput-wide p2, p0, Lcom/google/android/gms/measurement/internal/ec;->aIb:J

    iput-object p4, p0, Lcom/google/android/gms/measurement/internal/ec;->aIc:Landroid/os/Bundle;

    iput-object p5, p0, Lcom/google/android/gms/measurement/internal/ec;->val$context:Landroid/content/Context;

    iput-object p6, p0, Lcom/google/android/gms/measurement/internal/ec;->aHD:Lcom/google/android/gms/measurement/internal/dh;

    iput-object p7, p0, Lcom/google/android/gms/measurement/internal/ec;->aId:Landroid/content/BroadcastReceiver$PendingResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .line 2
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ec;->aHC:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dp;->aGW:Lcom/google/android/gms/measurement/internal/dt;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/dt;->get()J

    move-result-wide v0

    .line 3
    iget-wide v2, p0, Lcom/google/android/gms/measurement/internal/ec;->aIb:J

    const-wide/16 v4, 0x0

    cmp-long v6, v0, v4

    if-lez v6, :cond_1

    cmp-long v6, v2, v0

    if-gez v6, :cond_0

    cmp-long v6, v2, v4

    if-gtz v6, :cond_1

    :cond_0
    const-wide/16 v2, 0x1

    sub-long v2, v0, v2

    :cond_1
    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    .line 7
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ec;->aIc:Landroid/os/Bundle;

    const-string v1, "click_timestamp"

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 8
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ec;->aIc:Landroid/os/Bundle;

    const-string v1, "_cis"

    const-string v2, "referrer broadcast"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ec;->val$context:Landroid/content/Context;

    const/4 v1, 0x0

    .line 10
    invoke-static {v0, v1}, Lcom/google/android/gms/measurement/internal/ek;->a(Landroid/content/Context;Lcom/google/android/gms/internal/measurement/zzx;)Lcom/google/android/gms/measurement/internal/ek;

    move-result-object v0

    .line 11
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ec;->aIc:Landroid/os/Bundle;

    const-string v2, "auto"

    const-string v3, "_cmp"

    .line 12
    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/gms/measurement/internal/fr;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 13
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ec;->aHD:Lcom/google/android/gms/measurement/internal/dh;

    .line 1022
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "Install campaign recorded"

    .line 13
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 14
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ec;->aId:Landroid/content/BroadcastReceiver$PendingResult;

    if-eqz v0, :cond_3

    .line 15
    invoke-virtual {v0}, Landroid/content/BroadcastReceiver$PendingResult;->finish()V

    :cond_3
    return-void
.end method
