.class public final Lcom/google/android/gms/measurement/internal/fo;
.super Ljava/lang/Object;


# instance fields
.field aAW:J

.field aAX:Z

.field aBa:Ljava/lang/String;

.field aIF:Ljava/lang/String;

.field aIG:Ljava/lang/String;

.field aIZ:Ljava/lang/Boolean;

.field aJx:Lcom/google/android/gms/internal/measurement/zzx;

.field final ask:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/measurement/zzx;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/google/android/gms/measurement/internal/fo;->aAX:Z

    .line 3
    invoke-static {p1}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    .line 5
    invoke-static {p1}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 6
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/fo;->ask:Landroid/content/Context;

    if-eqz p2, :cond_0

    .line 9
    iput-object p2, p0, Lcom/google/android/gms/measurement/internal/fo;->aJx:Lcom/google/android/gms/internal/measurement/zzx;

    .line 10
    iget-object p1, p2, Lcom/google/android/gms/internal/measurement/zzx;->aBa:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/fo;->aBa:Ljava/lang/String;

    .line 11
    iget-object p1, p2, Lcom/google/android/gms/internal/measurement/zzx;->aAZ:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/fo;->aIF:Ljava/lang/String;

    .line 12
    iget-object p1, p2, Lcom/google/android/gms/internal/measurement/zzx;->aAY:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/fo;->aIG:Ljava/lang/String;

    .line 13
    iget-boolean p1, p2, Lcom/google/android/gms/internal/measurement/zzx;->aAX:Z

    iput-boolean p1, p0, Lcom/google/android/gms/measurement/internal/fo;->aAX:Z

    .line 14
    iget-wide v1, p2, Lcom/google/android/gms/internal/measurement/zzx;->aAW:J

    iput-wide v1, p0, Lcom/google/android/gms/measurement/internal/fo;->aAW:J

    .line 15
    iget-object p1, p2, Lcom/google/android/gms/internal/measurement/zzx;->aBb:Landroid/os/Bundle;

    if-eqz p1, :cond_0

    .line 16
    iget-object p1, p2, Lcom/google/android/gms/internal/measurement/zzx;->aBb:Landroid/os/Bundle;

    const-string p2, "dataCollectionDefaultEnabled"

    .line 17
    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/fo;->aIZ:Ljava/lang/Boolean;

    :cond_0
    return-void
.end method
