.class final Lcom/google/android/gms/measurement/internal/fv;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final synthetic aJM:J

.field private final synthetic aJz:Lcom/google/android/gms/measurement/internal/fr;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/fr;J)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/fv;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    iput-wide p2, p0, Lcom/google/android/gms/measurement/internal/fv;->aJM:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .line 2
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fv;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    iget-wide v1, p0, Lcom/google/android/gms/measurement/internal/fv;->aJM:J

    .line 3
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 5
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fb;->vt()V

    .line 6
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v3

    .line 1021
    iget-object v3, v3, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    const-string v4, "Resetting analytics data (FE)"

    .line 6
    invoke-virtual {v3, v4}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 7
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cb;->vC()Lcom/google/android/gms/measurement/internal/hx;

    move-result-object v3

    .line 2011
    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 2012
    iget-object v4, v3, Lcom/google/android/gms/measurement/internal/hx;->aKN:Lcom/google/android/gms/measurement/internal/b;

    invoke-virtual {v4}, Lcom/google/android/gms/measurement/internal/b;->cancel()V

    .line 2013
    iget-object v4, v3, Lcom/google/android/gms/measurement/internal/hx;->aKO:Lcom/google/android/gms/measurement/internal/b;

    invoke-virtual {v4}, Lcom/google/android/gms/measurement/internal/b;->cancel()V

    const-wide/16 v4, 0x0

    .line 2014
    iput-wide v4, v3, Lcom/google/android/gms/measurement/internal/hx;->aKM:J

    .line 2015
    iget-wide v4, v3, Lcom/google/android/gms/measurement/internal/hx;->aKM:J

    iput-wide v4, v3, Lcom/google/android/gms/measurement/internal/hx;->acG:J

    .line 8
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cb;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/measurement/internal/cz;->pT()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/measurement/internal/jb;->cZ(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 9
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/gms/measurement/internal/dp;->aGW:Lcom/google/android/gms/measurement/internal/dt;

    invoke-virtual {v3, v1, v2}, Lcom/google/android/gms/measurement/internal/dt;->set(J)V

    .line 10
    :cond_0
    iget-object v1, v0, Lcom/google/android/gms/measurement/internal/fr;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/ek;->isEnabled()Z

    move-result v1

    .line 11
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/jb;->zl()Z

    move-result v2

    if-nez v2, :cond_1

    .line 12
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v2

    xor-int/lit8 v3, v1, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/gms/measurement/internal/dp;->Q(Z)V

    .line 13
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cb;->vz()Lcom/google/android/gms/measurement/internal/gw;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/gw;->xE()V

    xor-int/lit8 v1, v1, 0x1

    .line 14
    iput-boolean v1, v0, Lcom/google/android/gms/measurement/internal/fr;->aJF:Z

    .line 15
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fv;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cb;->vz()Lcom/google/android/gms/measurement/internal/gw;

    move-result-object v0

    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/gw;->a(Ljava/util/concurrent/atomic/AtomicReference;)V

    return-void
.end method
