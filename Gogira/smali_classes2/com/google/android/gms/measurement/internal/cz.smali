.class public final Lcom/google/android/gms/measurement/internal/cz;
.super Lcom/google/android/gms/measurement/internal/fb;


# instance fields
.field private aAW:J

.field private aDF:Ljava/lang/String;

.field private aFS:Ljava/lang/String;

.field private aFT:I

.field private aFU:Ljava/lang/String;

.field private aFV:Ljava/lang/String;

.field private aFW:J

.field private aFX:J

.field aFY:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private aFZ:I

.field private aGa:Ljava/lang/String;

.field private arz:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/ek;J)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/google/android/gms/measurement/internal/fb;-><init>(Lcom/google/android/gms/measurement/internal/ek;)V

    .line 2
    iput-wide p2, p0, Lcom/google/android/gms/measurement/internal/cz;->aAW:J

    return-void
.end method

.method private final oi()Ljava/lang/String;
    .locals 7

    const/4 v0, 0x0

    .line 183
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 184
    invoke-virtual {v1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    const-string v2, "com.google.firebase.analytics.FirebaseAnalytics"

    .line 185
    invoke-virtual {v1, v2}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    if-nez v1, :cond_0

    return-object v0

    :cond_0
    :try_start_1
    const-string v2, "getInstance"

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Class;

    .line 191
    const-class v5, Landroid/content/Context;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    .line 192
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->getContext()Landroid/content/Context;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v2, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v2, :cond_1

    return-object v0

    :cond_1
    :try_start_2
    const-string v3, "getFirebaseInstanceId"

    new-array v4, v6, [Ljava/lang/Class;

    .line 200
    invoke-virtual {v1, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    new-array v3, v6, [Ljava/lang/Object;

    .line 201
    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    return-object v1

    .line 203
    :catch_0
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 17019
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGv:Lcom/google/android/gms/measurement/internal/dj;

    const-string v2, "Failed to retrieve Firebase Instance Id"

    .line 203
    invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    return-object v0

    .line 197
    :catch_1
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 17018
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGu:Lcom/google/android/gms/measurement/internal/dj;

    const-string v2, "Failed to obtain Firebase Analytics instance"

    .line 197
    invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    :catch_2
    return-object v0
.end method


# virtual methods
.method final ce(Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/zzn;
    .locals 33

    move-object/from16 v0, p0

    .line 125
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 127
    new-instance v29, Lcom/google/android/gms/measurement/internal/zzn;

    .line 128
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/cz;->pT()Ljava/lang/String;

    move-result-object v2

    .line 129
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/cz;->getGmpAppId()Ljava/lang/String;

    move-result-object v3

    .line 131
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fb;->vt()V

    .line 132
    iget-object v4, v0, Lcom/google/android/gms/measurement/internal/cz;->aFS:Ljava/lang/String;

    .line 134
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/cz;->xC()I

    move-result v1

    int-to-long v5, v1

    .line 136
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fb;->vt()V

    .line 137
    iget-object v7, v0, Lcom/google/android/gms/measurement/internal/cz;->aFU:Ljava/lang/String;

    .line 142
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fb;->vt()V

    .line 143
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 144
    iget-wide v8, v0, Lcom/google/android/gms/measurement/internal/cz;->aFW:J

    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-nez v1, :cond_0

    .line 145
    iget-object v1, v0, Lcom/google/android/gms/measurement/internal/cz;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/google/android/gms/measurement/internal/it;->o(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, v0, Lcom/google/android/gms/measurement/internal/cz;->aFW:J

    .line 146
    :cond_0
    iget-wide v12, v0, Lcom/google/android/gms/measurement/internal/cz;->aFW:J

    .line 147
    iget-object v1, v0, Lcom/google/android/gms/measurement/internal/cz;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 148
    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/ek;->isEnabled()Z

    move-result v14

    .line 149
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/gms/measurement/internal/dp;->aHk:Z

    const/4 v8, 0x1

    xor-int/lit8 v15, v1, 0x1

    .line 151
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 153
    iget-object v1, v0, Lcom/google/android/gms/measurement/internal/cz;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/ek;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    const/16 v16, 0x0

    goto :goto_0

    .line 155
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/cz;->oi()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v16, v1

    .line 158
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fb;->vt()V

    .line 159
    iget-wide v8, v0, Lcom/google/android/gms/measurement/internal/cz;->aFX:J

    .line 160
    iget-object v1, v0, Lcom/google/android/gms/measurement/internal/cz;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 13362
    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/ek;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v10

    iget-object v10, v10, Lcom/google/android/gms/measurement/internal/dp;->aGW:Lcom/google/android/gms/measurement/internal/dt;

    invoke-virtual {v10}, Lcom/google/android/gms/measurement/internal/dt;->get()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    .line 13363
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v21

    const-wide/16 v19, 0x0

    cmp-long v11, v21, v19

    if-nez v11, :cond_2

    .line 13364
    iget-wide v10, v1, Lcom/google/android/gms/measurement/internal/ek;->aJe:J

    move-wide/from16 v19, v8

    move-wide/from16 v21, v10

    goto :goto_1

    :cond_2
    move-wide/from16 v19, v8

    .line 13365
    iget-wide v8, v1, Lcom/google/android/gms/measurement/internal/ek;->aJe:J

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    move-wide/from16 v21, v8

    .line 162
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/cz;->xD()I

    move-result v23

    .line 163
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/jb;->zm()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v24

    .line 164
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v1

    const-string v8, "google_analytics_ssaid_collection_enabled"

    .line 166
    invoke-virtual {v1, v8}, Lcom/google/android/gms/measurement/internal/jb;->cV(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v8, 0x0

    if-eqz v1, :cond_4

    .line 167
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_2

    :cond_3
    const/16 v25, 0x0

    goto :goto_3

    :cond_4
    :goto_2
    const/16 v25, 0x1

    .line 169
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v1

    .line 14138
    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 14139
    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/dp;->xP()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v9, "deferred_analytics_collection"

    invoke-interface {v1, v9, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v26

    .line 170
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/cz;->xB()Ljava/lang/String;

    move-result-object v27

    .line 171
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/cz;->pT()Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/google/android/gms/measurement/internal/j;->aFj:Lcom/google/android/gms/measurement/internal/cv;

    .line 15083
    invoke-virtual {v1, v8, v9}, Lcom/google/android/gms/measurement/internal/jb;->d(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 173
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v1

    const-string v8, "google_analytics_default_allow_ad_personalization_signals"

    invoke-virtual {v1, v8}, Lcom/google/android/gms/measurement/internal/jb;->cV(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 174
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    const/4 v8, 0x1

    xor-int/2addr v1, v8

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    move-object/from16 v28, v1

    goto :goto_4

    :cond_5
    const/16 v28, 0x0

    .line 177
    :goto_4
    iget-wide v10, v0, Lcom/google/android/gms/measurement/internal/cz;->aAW:J

    .line 178
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/cz;->pT()Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/google/android/gms/measurement/internal/j;->aFx:Lcom/google/android/gms/measurement/internal/cv;

    .line 16083
    invoke-virtual {v1, v8, v9}, Lcom/google/android/gms/measurement/internal/jb;->d(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 179
    iget-object v1, v0, Lcom/google/android/gms/measurement/internal/cz;->aFY:Ljava/util/List;

    move-object/from16 v30, v1

    goto :goto_5

    :cond_6
    const/16 v30, 0x0

    :goto_5
    const-wide/16 v8, 0x3f7a

    move-wide/from16 v17, v19

    move-object/from16 v1, v29

    move-wide/from16 v31, v10

    move-wide v10, v12

    move-object/from16 v12, p1

    move v13, v14

    move v14, v15

    move-object/from16 v15, v16

    move-wide/from16 v16, v17

    move-wide/from16 v18, v21

    move/from16 v20, v23

    move/from16 v21, v24

    move/from16 v22, v25

    move/from16 v23, v26

    move-object/from16 v24, v27

    move-object/from16 v25, v28

    move-wide/from16 v26, v31

    move-object/from16 v28, v30

    .line 180
    invoke-direct/range {v1 .. v28}, Lcom/google/android/gms/measurement/internal/zzn;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;JJLjava/lang/String;ZZLjava/lang/String;JJIZZZLjava/lang/String;Ljava/lang/Boolean;JLjava/util/List;)V

    return-object v29
.end method

.method public final bridge synthetic getContext()Landroid/content/Context;
    .locals 1

    .line 229
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method final getGmpAppId()Ljava/lang/String;
    .locals 1

    .line 207
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fb;->vt()V

    .line 208
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/cz;->arz:Ljava/lang/String;

    return-object v0
.end method

.method public final bridge synthetic lX()V
    .locals 0

    .line 219
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->lX()V

    return-void
.end method

.method final pT()Ljava/lang/String;
    .locals 1

    .line 205
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fb;->vt()V

    .line 206
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/cz;->aDF:Ljava/lang/String;

    return-object v0
.end method

.method public final bridge synthetic vA()Lcom/google/android/gms/measurement/internal/gr;
    .locals 1

    .line 224
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vA()Lcom/google/android/gms/measurement/internal/gr;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vB()Lcom/google/android/gms/measurement/internal/dd;
    .locals 1

    .line 225
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vB()Lcom/google/android/gms/measurement/internal/dd;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vC()Lcom/google/android/gms/measurement/internal/hx;
    .locals 1

    .line 226
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vC()Lcom/google/android/gms/measurement/internal/hx;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vD()Lcom/google/android/gms/measurement/internal/d;
    .locals 1

    .line 227
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vD()Lcom/google/android/gms/measurement/internal/d;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vE()Lcom/google/android/gms/common/util/e;
    .locals 1

    .line 228
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vF()Lcom/google/android/gms/measurement/internal/df;
    .locals 1

    .line 230
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vG()Lcom/google/android/gms/measurement/internal/it;
    .locals 1

    .line 231
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vH()Lcom/google/android/gms/measurement/internal/ed;
    .locals 1

    .line 232
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vI()Lcom/google/android/gms/measurement/internal/dh;
    .locals 1

    .line 233
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vJ()Lcom/google/android/gms/measurement/internal/dp;
    .locals 1

    .line 234
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vK()Lcom/google/android/gms/measurement/internal/jb;
    .locals 1

    .line 235
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v0

    return-object v0
.end method

.method protected final vM()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final bridge synthetic vu()V
    .locals 0

    .line 216
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vu()V

    return-void
.end method

.method public final bridge synthetic vv()V
    .locals 0

    .line 218
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vv()V

    return-void
.end method

.method public final bridge synthetic vw()Lcom/google/android/gms/measurement/internal/a;
    .locals 1

    .line 220
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vw()Lcom/google/android/gms/measurement/internal/a;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vx()Lcom/google/android/gms/measurement/internal/fr;
    .locals 1

    .line 221
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vy()Lcom/google/android/gms/measurement/internal/cz;
    .locals 1

    .line 222
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vz()Lcom/google/android/gms/measurement/internal/gw;
    .locals 1

    .line 223
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vz()Lcom/google/android/gms/measurement/internal/gw;

    move-result-object v0

    return-object v0
.end method

.method protected final xA()V
    .locals 13

    .line 9
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 10
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "Unknown"

    const-string v3, ""

    const/4 v4, 0x0

    const-string v5, "unknown"

    const/high16 v6, -0x80000000

    if-nez v1, :cond_1

    .line 12
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v7

    .line 1014
    iget-object v7, v7, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    .line 14
    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/dh;->ci(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    const-string v9, "PackageManager is null, app identity information might be inaccurate. appId"

    .line 15
    invoke-virtual {v7, v9, v8}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    move-object v8, v2

    goto :goto_4

    .line 16
    :cond_1
    :try_start_0
    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 19
    :catch_0
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v7

    .line 2014
    iget-object v7, v7, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    .line 21
    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/dh;->ci(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    const-string v9, "Error retrieving app installer package name. appId"

    invoke-virtual {v7, v9, v8}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    :goto_0
    if-nez v5, :cond_2

    const-string v5, "manual_install"

    goto :goto_1

    :cond_2
    const-string v7, "com.android.vending"

    .line 24
    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    move-object v5, v3

    .line 26
    :cond_3
    :goto_1
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 28
    iget-object v8, v7, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 29
    invoke-virtual {v1, v8}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v8

    .line 30
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 31
    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_2

    :cond_4
    move-object v8, v2

    .line 32
    :goto_2
    :try_start_2
    iget-object v2, v7, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 33
    iget v6, v7, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_4

    :catch_1
    move-object v7, v2

    move-object v2, v8

    goto :goto_3

    :catch_2
    move-object v7, v2

    .line 36
    :goto_3
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v8

    .line 3014
    iget-object v8, v8, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    .line 38
    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/dh;->ci(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    const-string v10, "Error retrieving package info. appId, appName"

    .line 39
    invoke-virtual {v8, v10, v9, v2}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v8, v2

    move-object v2, v7

    .line 40
    :goto_4
    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/cz;->aDF:Ljava/lang/String;

    .line 41
    iput-object v5, p0, Lcom/google/android/gms/measurement/internal/cz;->aFU:Ljava/lang/String;

    .line 42
    iput-object v2, p0, Lcom/google/android/gms/measurement/internal/cz;->aFS:Ljava/lang/String;

    .line 43
    iput v6, p0, Lcom/google/android/gms/measurement/internal/cz;->aFT:I

    .line 44
    iput-object v8, p0, Lcom/google/android/gms/measurement/internal/cz;->aFV:Ljava/lang/String;

    const-wide/16 v5, 0x0

    .line 45
    iput-wide v5, p0, Lcom/google/android/gms/measurement/internal/cz;->aFW:J

    .line 48
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/common/api/internal/e;->J(Landroid/content/Context;)Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    const/4 v7, 0x1

    if-eqz v2, :cond_5

    .line 49
    invoke-virtual {v2}, Lcom/google/android/gms/common/api/Status;->kS()Z

    move-result v8

    if-eqz v8, :cond_5

    const/4 v8, 0x1

    goto :goto_5

    :cond_5
    const/4 v8, 0x0

    .line 50
    :goto_5
    iget-object v9, p0, Lcom/google/android/gms/measurement/internal/cz;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 3257
    iget-object v9, v9, Lcom/google/android/gms/measurement/internal/ek;->aBa:Ljava/lang/String;

    .line 51
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    const-string v10, "am"

    if-nez v9, :cond_6

    iget-object v9, p0, Lcom/google/android/gms/measurement/internal/cz;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 3258
    iget-object v9, v9, Lcom/google/android/gms/measurement/internal/ek;->aIF:Ljava/lang/String;

    .line 52
    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    const/4 v9, 0x1

    goto :goto_6

    :cond_6
    const/4 v9, 0x0

    :goto_6
    or-int/2addr v8, v9

    if-nez v8, :cond_8

    if-nez v2, :cond_7

    .line 56
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v2

    .line 4014
    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string v9, "GoogleService failed to initialize (no status)"

    .line 56
    invoke-virtual {v2, v9}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    goto :goto_7

    .line 57
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v9

    .line 5014
    iget-object v9, v9, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    .line 5024
    iget v11, v2, Lcom/google/android/gms/common/api/Status;->ady:I

    .line 59
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    .line 6019
    iget-object v2, v2, Lcom/google/android/gms/common/api/Status;->adA:Ljava/lang/String;

    const-string v12, "GoogleService failed to initialize, status"

    .line 61
    invoke-virtual {v9, v12, v11, v2}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_8
    :goto_7
    if-eqz v8, :cond_c

    .line 64
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v2

    const-string v8, "firebase_analytics_collection_enabled"

    .line 6134
    invoke-virtual {v2, v8}, Lcom/google/android/gms/measurement/internal/jb;->cV(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    .line 65
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/gms/measurement/internal/jb;->zl()Z

    move-result v8

    if-eqz v8, :cond_9

    .line 66
    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/cz;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 6256
    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/ek;->aBa:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 67
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v2

    .line 7020
    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/dh;->aGw:Lcom/google/android/gms/measurement/internal/dj;

    const-string v8, "Collection disabled with firebase_analytics_collection_deactivated=1"

    .line 69
    invoke-virtual {v2, v8}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    goto :goto_8

    :cond_9
    if-eqz v2, :cond_a

    .line 70
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-nez v8, :cond_a

    .line 71
    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/cz;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 7256
    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/ek;->aBa:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 72
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v2

    .line 8020
    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/dh;->aGw:Lcom/google/android/gms/measurement/internal/dj;

    const-string v8, "Collection disabled with firebase_analytics_collection_enabled=0"

    .line 74
    invoke-virtual {v2, v8}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    goto :goto_8

    :cond_a
    if-nez v2, :cond_b

    .line 76
    invoke-static {}, Lcom/google/android/gms/common/api/internal/e;->lE()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 78
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v2

    .line 9020
    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/dh;->aGw:Lcom/google/android/gms/measurement/internal/dj;

    const-string v8, "Collection disabled with google_app_measurement_enable=0"

    .line 78
    invoke-virtual {v2, v8}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    goto :goto_8

    .line 80
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v2

    .line 9022
    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    const-string v8, "Collection enabled"

    .line 80
    invoke-virtual {v2, v8}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    const/4 v2, 0x1

    goto :goto_9

    :cond_c
    :goto_8
    const/4 v2, 0x0

    .line 81
    :goto_9
    iput-object v3, p0, Lcom/google/android/gms/measurement/internal/cz;->arz:Ljava/lang/String;

    .line 82
    iput-object v3, p0, Lcom/google/android/gms/measurement/internal/cz;->aGa:Ljava/lang/String;

    .line 83
    iput-wide v5, p0, Lcom/google/android/gms/measurement/internal/cz;->aFX:J

    .line 85
    iget-object v5, p0, Lcom/google/android/gms/measurement/internal/cz;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 9257
    iget-object v5, v5, Lcom/google/android/gms/measurement/internal/ek;->aBa:Ljava/lang/String;

    .line 85
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_d

    iget-object v5, p0, Lcom/google/android/gms/measurement/internal/cz;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 9258
    iget-object v5, v5, Lcom/google/android/gms/measurement/internal/ek;->aIF:Ljava/lang/String;

    .line 86
    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 87
    iget-object v5, p0, Lcom/google/android/gms/measurement/internal/cz;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 10257
    iget-object v5, v5, Lcom/google/android/gms/measurement/internal/ek;->aBa:Ljava/lang/String;

    .line 87
    iput-object v5, p0, Lcom/google/android/gms/measurement/internal/cz;->aGa:Ljava/lang/String;

    .line 88
    :cond_d
    :try_start_3
    invoke-static {}, Lcom/google/android/gms/common/api/internal/e;->lD()Ljava/lang/String;

    move-result-object v5

    .line 89
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_e

    goto :goto_a

    :cond_e
    move-object v3, v5

    :goto_a
    iput-object v3, p0, Lcom/google/android/gms/measurement/internal/cz;->arz:Ljava/lang/String;

    .line 90
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_f

    .line 91
    new-instance v3, Lcom/google/android/gms/common/internal/t;

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v3, v5}, Lcom/google/android/gms/common/internal/t;-><init>(Landroid/content/Context;)V

    const-string v5, "admob_app_id"

    .line 92
    invoke-virtual {v3, v5}, Lcom/google/android/gms/common/internal/t;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gms/measurement/internal/cz;->aGa:Ljava/lang/String;

    :cond_f
    if-eqz v2, :cond_10

    .line 94
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v2

    .line 11022
    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    const-string v3, "App package, google app id"

    .line 94
    iget-object v5, p0, Lcom/google/android/gms/measurement/internal/cz;->aDF:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/measurement/internal/cz;->arz:Ljava/lang/String;

    invoke-virtual {v2, v3, v5, v6}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_b

    :catch_3
    move-exception v2

    .line 97
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v3

    .line 12014
    iget-object v3, v3, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    .line 99
    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/dh;->ci(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    const-string v5, "getGoogleAppId or isMeasurementEnabled failed with exception. appId"

    .line 100
    invoke-virtual {v3, v5, v0, v2}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_10
    :goto_b
    const/4 v0, 0x0

    .line 102
    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/cz;->aFY:Ljava/util/List;

    .line 103
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/cz;->aDF:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/measurement/internal/j;->aFx:Lcom/google/android/gms/measurement/internal/cv;

    .line 12083
    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/measurement/internal/jb;->d(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 106
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v0

    const-string v2, "analytics.safelisted_events"

    invoke-virtual {v0, v2}, Lcom/google/android/gms/measurement/internal/jb;->cW(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 109
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_11

    .line 110
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v2

    .line 13017
    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    const-string v3, "Safelisted event list cannot be empty. Ignoring"

    .line 110
    invoke-virtual {v2, v3}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    :goto_c
    const/4 v7, 0x0

    goto :goto_d

    .line 112
    :cond_11
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_12
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_13

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 113
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v5

    const-string v6, "safelisted event"

    invoke-virtual {v5, v6, v3}, Lcom/google/android/gms/measurement/internal/it;->I(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_12

    goto :goto_c

    :cond_13
    :goto_d
    if-eqz v7, :cond_14

    .line 118
    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/cz;->aFY:Ljava/util/List;

    .line 119
    :cond_14
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v0, v2, :cond_16

    if-eqz v1, :cond_15

    .line 121
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->W(Landroid/content/Context;)Z

    move-result v0

    iput v0, p0, Lcom/google/android/gms/measurement/internal/cz;->aFZ:I

    return-void

    .line 122
    :cond_15
    iput v4, p0, Lcom/google/android/gms/measurement/internal/cz;->aFZ:I

    return-void

    .line 123
    :cond_16
    iput v4, p0, Lcom/google/android/gms/measurement/internal/cz;->aFZ:I

    return-void
.end method

.method final xB()Ljava/lang/String;
    .locals 1

    .line 209
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fb;->vt()V

    .line 210
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/cz;->aGa:Ljava/lang/String;

    return-object v0
.end method

.method final xC()I
    .locals 1

    .line 211
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fb;->vt()V

    .line 212
    iget v0, p0, Lcom/google/android/gms/measurement/internal/cz;->aFT:I

    return v0
.end method

.method final xD()I
    .locals 1

    .line 213
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fb;->vt()V

    .line 214
    iget v0, p0, Lcom/google/android/gms/measurement/internal/cz;->aFZ:I

    return v0
.end method
