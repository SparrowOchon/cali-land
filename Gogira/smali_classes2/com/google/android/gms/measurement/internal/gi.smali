.class final Lcom/google/android/gms/measurement/internal/gi;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final synthetic aJP:J

.field private final synthetic aJz:Lcom/google/android/gms/measurement/internal/fr;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/fr;J)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/gi;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    iput-wide p2, p0, Lcom/google/android/gms/measurement/internal/gi;->aJP:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/gi;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dp;->aHc:Lcom/google/android/gms/measurement/internal/dt;

    iget-wide v1, p0, Lcom/google/android/gms/measurement/internal/gi;->aJP:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/measurement/internal/dt;->set(J)V

    .line 3
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/gi;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 1021
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    .line 3
    iget-wide v1, p0, Lcom/google/android/gms/measurement/internal/gi;->aJP:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "Minimum session duration set"

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method
