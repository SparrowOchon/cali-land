.class final Lcom/google/android/gms/measurement/internal/fq;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final synthetic aJy:Ljava/util/concurrent/atomic/AtomicReference;

.field private final synthetic aJz:Lcom/google/android/gms/measurement/internal/fr;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/fr;Ljava/util/concurrent/atomic/AtomicReference;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/fq;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    iput-object p2, p0, Lcom/google/android/gms/measurement/internal/fq;->aJy:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .line 2
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fq;->aJy:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v0

    .line 3
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/fq;->aJy:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/fq;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/measurement/internal/fq;->aJz:Lcom/google/android/gms/measurement/internal/fr;

    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/cb;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/cz;->pT()Ljava/lang/String;

    move-result-object v3

    .line 1167
    sget-object v4, Lcom/google/android/gms/measurement/internal/j;->aEM:Lcom/google/android/gms/measurement/internal/cv;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/measurement/internal/jb;->d(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v2

    .line 3
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/fq;->aJy:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    .line 7
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    .line 6
    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/fq;->aJy:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    throw v1

    :catchall_1
    move-exception v1

    .line 7
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v1
.end method
