.class final Lcom/google/android/gms/measurement/internal/ih;
.super Lcom/google/android/gms/measurement/internal/b;


# instance fields
.field private final synthetic aKI:Lcom/google/android/gms/measurement/internal/ii;

.field private final synthetic aKT:Lcom/google/android/gms/measurement/internal/ie;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/ie;Lcom/google/android/gms/measurement/internal/fj;Lcom/google/android/gms/measurement/internal/ii;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/ih;->aKT:Lcom/google/android/gms/measurement/internal/ie;

    iput-object p3, p0, Lcom/google/android/gms/measurement/internal/ih;->aKI:Lcom/google/android/gms/measurement/internal/ii;

    invoke-direct {p0, p2}, Lcom/google/android/gms/measurement/internal/b;-><init>(Lcom/google/android/gms/measurement/internal/fj;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ih;->aKT:Lcom/google/android/gms/measurement/internal/ie;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ie;->cancel()V

    .line 3
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ih;->aKT:Lcom/google/android/gms/measurement/internal/ie;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 1022
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "Starting upload from DelayedRunnable"

    .line 3
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 4
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ih;->aKI:Lcom/google/android/gms/measurement/internal/ii;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ii;->yV()V

    return-void
.end method
