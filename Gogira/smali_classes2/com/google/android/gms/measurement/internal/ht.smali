.class public final synthetic Lcom/google/android/gms/measurement/internal/ht;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final aJR:I

.field private final aKE:Lcom/google/android/gms/measurement/internal/hu;

.field private final aKF:Lcom/google/android/gms/measurement/internal/dh;

.field private final aKG:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/measurement/internal/hu;ILcom/google/android/gms/measurement/internal/dh;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/ht;->aKE:Lcom/google/android/gms/measurement/internal/hu;

    iput p2, p0, Lcom/google/android/gms/measurement/internal/ht;->aJR:I

    iput-object p3, p0, Lcom/google/android/gms/measurement/internal/ht;->aKF:Lcom/google/android/gms/measurement/internal/dh;

    iput-object p4, p0, Lcom/google/android/gms/measurement/internal/ht;->aKG:Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ht;->aKE:Lcom/google/android/gms/measurement/internal/hu;

    iget v1, p0, Lcom/google/android/gms/measurement/internal/ht;->aJR:I

    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/ht;->aKF:Lcom/google/android/gms/measurement/internal/dh;

    iget-object v3, p0, Lcom/google/android/gms/measurement/internal/ht;->aKG:Landroid/content/Intent;

    .line 1077
    iget-object v4, v0, Lcom/google/android/gms/measurement/internal/hu;->aKH:Landroid/content/Context;

    check-cast v4, Lcom/google/android/gms/measurement/internal/hy;

    invoke-interface {v4, v1}, Lcom/google/android/gms/measurement/internal/hy;->cM(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2022
    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    .line 1081
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v4, "Local AppMeasurementService processed last upload request. StartId"

    .line 1082
    invoke-virtual {v2, v4, v1}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1083
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/hu;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 3022
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    const-string v2, "Completed wakeful intent."

    .line 1083
    invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 1084
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/hu;->aKH:Landroid/content/Context;

    check-cast v0, Lcom/google/android/gms/measurement/internal/hy;

    invoke-interface {v0, v3}, Lcom/google/android/gms/measurement/internal/hy;->b(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method
