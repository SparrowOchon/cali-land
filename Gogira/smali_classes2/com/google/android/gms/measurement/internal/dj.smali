.class public final Lcom/google/android/gms/measurement/internal/dj;
.super Ljava/lang/Object;


# instance fields
.field private final aGA:Z

.field private final aGB:Z

.field private final synthetic aGn:Lcom/google/android/gms/measurement/internal/dh;

.field private final priority:I


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/dh;IZZ)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/dj;->aGn:Lcom/google/android/gms/measurement/internal/dh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p2, p0, Lcom/google/android/gms/measurement/internal/dj;->priority:I

    .line 3
    iput-boolean p3, p0, Lcom/google/android/gms/measurement/internal/dj;->aGA:Z

    .line 4
    iput-boolean p4, p0, Lcom/google/android/gms/measurement/internal/dj;->aGB:Z

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 8

    .line 10
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/dj;->aGn:Lcom/google/android/gms/measurement/internal/dh;

    iget v1, p0, Lcom/google/android/gms/measurement/internal/dj;->priority:I

    iget-boolean v2, p0, Lcom/google/android/gms/measurement/internal/dj;->aGA:Z

    iget-boolean v3, p0, Lcom/google/android/gms/measurement/internal/dj;->aGB:Z

    const/4 v7, 0x0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/measurement/internal/dh;->a(IZZLjava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 8

    .line 12
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/dj;->aGn:Lcom/google/android/gms/measurement/internal/dh;

    iget v1, p0, Lcom/google/android/gms/measurement/internal/dj;->priority:I

    iget-boolean v2, p0, Lcom/google/android/gms/measurement/internal/dj;->aGA:Z

    iget-boolean v3, p0, Lcom/google/android/gms/measurement/internal/dj;->aGB:Z

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/measurement/internal/dh;->a(IZZLjava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public final ck(Ljava/lang/String;)V
    .locals 8

    .line 6
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/dj;->aGn:Lcom/google/android/gms/measurement/internal/dh;

    iget v1, p0, Lcom/google/android/gms/measurement/internal/dj;->priority:I

    iget-boolean v2, p0, Lcom/google/android/gms/measurement/internal/dj;->aGA:Z

    iget-boolean v3, p0, Lcom/google/android/gms/measurement/internal/dj;->aGB:Z

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v4, p1

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/measurement/internal/dh;->a(IZZLjava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public final j(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 8

    .line 8
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/dj;->aGn:Lcom/google/android/gms/measurement/internal/dh;

    iget v1, p0, Lcom/google/android/gms/measurement/internal/dj;->priority:I

    iget-boolean v2, p0, Lcom/google/android/gms/measurement/internal/dj;->aGA:Z

    iget-boolean v3, p0, Lcom/google/android/gms/measurement/internal/dj;->aGB:Z

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v4, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/measurement/internal/dh;->a(IZZLjava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method
