.class public final Lcom/google/android/gms/measurement/internal/ip;
.super Lcom/google/android/gms/measurement/internal/ij;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/ii;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/google/android/gms/measurement/internal/ij;-><init>(Lcom/google/android/gms/measurement/internal/ii;)V

    return-void
.end method

.method private static a(ZZZ)Ljava/lang/String;
    .locals 1

    .line 239
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p0, :cond_0

    const-string p0, "Dynamic "

    .line 241
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    if-eqz p1, :cond_1

    const-string p0, "Sequence "

    .line 243
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    if-eqz p2, :cond_2

    const-string p0, "Session-Scoped "

    .line 245
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 246
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static a(Ljava/util/BitSet;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/BitSet;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 395
    invoke-virtual {p0}, Ljava/util/BitSet;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x3f

    const/16 v1, 0x40

    div-int/2addr v0, v1

    .line 396
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v0, :cond_2

    const-wide/16 v5, 0x0

    move-wide v6, v5

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v1, :cond_1

    shl-int/lit8 v8, v4, 0x6

    add-int/2addr v8, v5

    .line 400
    invoke-virtual {p0}, Ljava/util/BitSet;->length()I

    move-result v9

    if-ge v8, v9, :cond_1

    .line 401
    invoke-virtual {p0, v8}, Ljava/util/BitSet;->get(I)Z

    move-result v8

    if-eqz v8, :cond_0

    const-wide/16 v8, 0x1

    shl-long/2addr v8, v5

    or-long/2addr v6, v8

    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 404
    :cond_1
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    return-object v2
.end method

.method static a(Lcom/google/android/gms/internal/measurement/al$c$a;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 4

    .line 38
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/al$c$a;->po()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    .line 40
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 41
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/internal/measurement/al$e;

    .line 5005
    iget-object v2, v2, Lcom/google/android/gms/internal/measurement/al$e;->zzwk:Ljava/lang/String;

    .line 41
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    .line 45
    :goto_1
    invoke-static {}, Lcom/google/android/gms/internal/measurement/al$e;->pv()Lcom/google/android/gms/internal/measurement/al$e$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/measurement/al$e$a;->by(Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/al$e$a;

    move-result-object p1

    .line 46
    instance-of v0, p2, Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 47
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lcom/google/android/gms/internal/measurement/al$e$a;->D(J)Lcom/google/android/gms/internal/measurement/al$e$a;

    :cond_2
    if-ltz v1, :cond_3

    .line 53
    invoke-virtual {p0, v1, p1}, Lcom/google/android/gms/internal/measurement/al$c$a;->a(ILcom/google/android/gms/internal/measurement/al$e$a;)Lcom/google/android/gms/internal/measurement/al$c$a;

    return-void

    .line 54
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/measurement/al$c$a;->a(Lcom/google/android/gms/internal/measurement/al$e$a;)Lcom/google/android/gms/internal/measurement/al$c$a;

    return-void
.end method

.method private final a(Ljava/lang/StringBuilder;ILcom/google/android/gms/internal/measurement/ad$b;)V
    .locals 5

    if-nez p3, :cond_0

    return-void

    .line 323
    :cond_0
    invoke-static {p1, p2}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;I)V

    const-string v0, "filter {\n"

    .line 324
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 325
    invoke-virtual {p3}, Lcom/google/android/gms/internal/measurement/ad$b;->oH()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 32009
    iget-boolean v0, p3, Lcom/google/android/gms/internal/measurement/ad$b;->zzur:Z

    .line 326
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "complement"

    invoke-static {p1, p2, v1, v0}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 328
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object v0

    .line 32010
    iget-object v1, p3, Lcom/google/android/gms/internal/measurement/ad$b;->zzus:Ljava/lang/String;

    .line 328
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/df;->cg(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "param_name"

    .line 329
    invoke-static {p1, p2, v1, v0}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    add-int/lit8 v0, p2, 0x1

    .line 330
    invoke-virtual {p3}, Lcom/google/android/gms/internal/measurement/ad$b;->oE()Lcom/google/android/gms/internal/measurement/ad$e;

    move-result-object v1

    const-string v2, "}\n"

    if-eqz v1, :cond_7

    .line 332
    invoke-static {p1, v0}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;I)V

    const-string v3, "string_filter"

    .line 333
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " {\n"

    .line 334
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 335
    invoke-virtual {v1}, Lcom/google/android/gms/internal/measurement/ad$e;->oT()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 336
    invoke-virtual {v1}, Lcom/google/android/gms/internal/measurement/ad$e;->oU()Lcom/google/android/gms/internal/measurement/ad$e$a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/internal/measurement/ad$e$a;->name()Ljava/lang/String;

    move-result-object v3

    const-string v4, "match_type"

    invoke-static {p1, v0, v4, v3}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 33009
    :cond_2
    iget-object v3, v1, Lcom/google/android/gms/internal/measurement/ad$e;->zzvm:Ljava/lang/String;

    const-string v4, "expression"

    .line 337
    invoke-static {p1, v0, v4, v3}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 33010
    iget v3, v1, Lcom/google/android/gms/internal/measurement/ad$e;->zzue:I

    and-int/lit8 v3, v3, 0x4

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_4

    .line 33011
    iget-boolean v3, v1, Lcom/google/android/gms/internal/measurement/ad$e;->zzvn:Z

    .line 339
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const-string v4, "case_sensitive"

    invoke-static {p1, v0, v4, v3}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 340
    :cond_4
    invoke-virtual {v1}, Lcom/google/android/gms/internal/measurement/ad$e;->oW()I

    move-result v3

    if-lez v3, :cond_6

    add-int/lit8 v3, v0, 0x1

    .line 341
    invoke-static {p1, v3}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;I)V

    const-string v3, "expression_list {\n"

    .line 342
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33012
    iget-object v1, v1, Lcom/google/android/gms/internal/measurement/ad$e;->zzvo:Lcom/google/android/gms/internal/measurement/dz;

    .line 343
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    add-int/lit8 v4, v0, 0x2

    .line 344
    invoke-static {p1, v4}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;I)V

    .line 345
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\n"

    .line 346
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 348
    :cond_5
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 349
    :cond_6
    invoke-static {p1, v0}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;I)V

    .line 350
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 351
    :cond_7
    invoke-virtual {p3}, Lcom/google/android/gms/internal/measurement/ad$b;->oG()Lcom/google/android/gms/internal/measurement/ad$c;

    move-result-object p3

    const-string v1, "number_filter"

    invoke-static {p1, v0, v1, p3}, Lcom/google/android/gms/measurement/internal/ip;->a(Ljava/lang/StringBuilder;ILjava/lang/String;Lcom/google/android/gms/internal/measurement/ad$c;)V

    .line 352
    invoke-static {p1, p2}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;I)V

    .line 353
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method private static a(Ljava/lang/StringBuilder;ILjava/lang/String;Lcom/google/android/gms/internal/measurement/ad$c;)V
    .locals 1

    if-nez p3, :cond_0

    return-void

    .line 308
    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;I)V

    .line 309
    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " {\n"

    .line 310
    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    invoke-virtual {p3}, Lcom/google/android/gms/internal/measurement/ad$c;->oK()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 312
    invoke-virtual {p3}, Lcom/google/android/gms/internal/measurement/ad$c;->oL()Lcom/google/android/gms/internal/measurement/ad$c$b;

    move-result-object p2

    invoke-virtual {p2}, Lcom/google/android/gms/internal/measurement/ad$c$b;->name()Ljava/lang/String;

    move-result-object p2

    const-string v0, "comparison_type"

    invoke-static {p0, p1, v0, p2}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 313
    :cond_1
    invoke-virtual {p3}, Lcom/google/android/gms/internal/measurement/ad$c;->oM()Z

    move-result p2

    if-eqz p2, :cond_2

    .line 31010
    iget-boolean p2, p3, Lcom/google/android/gms/internal/measurement/ad$c;->zzuv:Z

    .line 314
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    const-string v0, "match_as_float"

    invoke-static {p0, p1, v0, p2}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 31012
    :cond_2
    iget-object p2, p3, Lcom/google/android/gms/internal/measurement/ad$c;->zzuw:Ljava/lang/String;

    const-string v0, "comparison_value"

    .line 315
    invoke-static {p0, p1, v0, p2}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 31014
    iget-object p2, p3, Lcom/google/android/gms/internal/measurement/ad$c;->zzux:Ljava/lang/String;

    const-string v0, "min_comparison_value"

    .line 316
    invoke-static {p0, p1, v0, p2}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 31016
    iget-object p2, p3, Lcom/google/android/gms/internal/measurement/ad$c;->zzuy:Ljava/lang/String;

    const-string p3, "max_comparison_value"

    .line 317
    invoke-static {p0, p1, p3, p2}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 318
    invoke-static {p0, p1}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;I)V

    const-string p1, "}\n"

    .line 319
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method private final a(Ljava/lang/StringBuilder;Ljava/lang/String;Lcom/google/android/gms/internal/measurement/al$i;Ljava/lang/String;)V
    .locals 9

    if-nez p3, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x3

    .line 250
    invoke-static {p1, v0}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;I)V

    .line 251
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " {\n"

    .line 252
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27018
    iget-object p2, p3, Lcom/google/android/gms/internal/measurement/al$i;->zzyw:Lcom/google/android/gms/internal/measurement/ea;

    invoke-interface {p2}, Lcom/google/android/gms/internal/measurement/ea;->size()I

    move-result p2

    const/16 v1, 0xa

    const/4 v2, 0x4

    const-string v3, ", "

    const/4 v4, 0x0

    if-eqz p2, :cond_3

    .line 254
    invoke-static {p1, v2}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;I)V

    const-string p2, "results: "

    .line 255
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28017
    iget-object p2, p3, Lcom/google/android/gms/internal/measurement/al$i;->zzyw:Lcom/google/android/gms/internal/measurement/ea;

    .line 257
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 v5, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    add-int/lit8 v7, v5, 0x1

    if-eqz v5, :cond_1

    .line 259
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    :cond_1
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move v5, v7

    goto :goto_0

    .line 262
    :cond_2
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 263
    :cond_3
    invoke-virtual {p3}, Lcom/google/android/gms/internal/measurement/al$i;->qa()I

    move-result p2

    if-eqz p2, :cond_6

    .line 264
    invoke-static {p1, v2}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;I)V

    const-string p2, "status: "

    .line 265
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29007
    iget-object p2, p3, Lcom/google/android/gms/internal/measurement/al$i;->zzyv:Lcom/google/android/gms/internal/measurement/ea;

    .line 267
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 v5, 0x0

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    add-int/lit8 v7, v5, 0x1

    if-eqz v5, :cond_4

    .line 269
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    :cond_4
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move v5, v7

    goto :goto_1

    .line 272
    :cond_5
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 273
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object p2

    invoke-virtual {p2, p4}, Lcom/google/android/gms/measurement/internal/jb;->cY(Ljava/lang/String;)Z

    move-result p2

    const-string p4, "}\n"

    if-eqz p2, :cond_11

    .line 274
    invoke-virtual {p3}, Lcom/google/android/gms/internal/measurement/al$i;->qb()I

    move-result p2

    const/4 v1, 0x0

    if-eqz p2, :cond_b

    .line 275
    invoke-static {p1, v2}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;I)V

    const-string p2, "dynamic_filter_timestamps: {"

    .line 276
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29027
    iget-object p2, p3, Lcom/google/android/gms/internal/measurement/al$i;->zzyx:Lcom/google/android/gms/internal/measurement/dz;

    .line 278
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 v5, 0x0

    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_a

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/internal/measurement/al$b;

    add-int/lit8 v7, v5, 0x1

    if-eqz v5, :cond_7

    .line 280
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    :cond_7
    invoke-virtual {v6}, Lcom/google/android/gms/internal/measurement/al$b;->pf()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 30004
    iget v5, v6, Lcom/google/android/gms/internal/measurement/al$b;->zzwg:I

    .line 281
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto :goto_3

    :cond_8
    move-object v5, v1

    :goto_3
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v5, ":"

    .line 282
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    invoke-virtual {v6}, Lcom/google/android/gms/internal/measurement/al$b;->pg()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 30009
    iget-wide v5, v6, Lcom/google/android/gms/internal/measurement/al$b;->zzwh:J

    .line 283
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto :goto_4

    :cond_9
    move-object v5, v1

    :goto_4
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move v5, v7

    goto :goto_2

    .line 285
    :cond_a
    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    :cond_b
    invoke-virtual {p3}, Lcom/google/android/gms/internal/measurement/al$i;->qd()I

    move-result p2

    if-eqz p2, :cond_11

    .line 287
    invoke-static {p1, v2}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;I)V

    const-string p2, "sequence_filter_timestamps: {"

    .line 288
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30040
    iget-object p2, p3, Lcom/google/android/gms/internal/measurement/al$i;->zzyy:Lcom/google/android/gms/internal/measurement/dz;

    .line 290
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 p3, 0x0

    :goto_5
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/internal/measurement/al$j;

    add-int/lit8 v5, p3, 0x1

    if-eqz p3, :cond_c

    .line 292
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 293
    :cond_c
    invoke-virtual {v2}, Lcom/google/android/gms/internal/measurement/al$j;->pf()Z

    move-result p3

    if-eqz p3, :cond_d

    .line 31005
    iget p3, v2, Lcom/google/android/gms/internal/measurement/al$j;->zzwg:I

    .line 293
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    goto :goto_6

    :cond_d
    move-object p3, v1

    :goto_6
    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p3, ": ["

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31009
    iget-object p3, v2, Lcom/google/android/gms/internal/measurement/al$j;->zzza:Lcom/google/android/gms/internal/measurement/ea;

    .line 295
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    const/4 v2, 0x0

    :goto_7
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_f

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    add-int/lit8 v8, v2, 0x1

    if-eqz v2, :cond_e

    .line 297
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298
    :cond_e
    invoke-virtual {p1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move v2, v8

    goto :goto_7

    :cond_f
    const-string p3, "]"

    .line 300
    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move p3, v5

    goto :goto_5

    .line 302
    :cond_10
    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    :cond_11
    invoke-static {p1, v0}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;I)V

    .line 304
    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method static b(Lcom/google/android/gms/internal/measurement/al$c;Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/al$e;
    .locals 2

    .line 3005
    iget-object p0, p0, Lcom/google/android/gms/internal/measurement/al$c;->zzwj:Lcom/google/android/gms/internal/measurement/dz;

    .line 24
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/al$e;

    .line 4005
    iget-object v1, v0, Lcom/google/android/gms/internal/measurement/al$e;->zzwk:Ljava/lang/String;

    .line 25
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method private static b(Ljava/lang/StringBuilder;I)V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    const-string v1, "  "

    .line 356
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V
    .locals 0

    if-nez p3, :cond_0

    return-void

    :cond_0
    add-int/lit8 p1, p1, 0x1

    .line 361
    invoke-static {p0, p1}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;I)V

    .line 362
    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ": "

    .line 363
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 364
    invoke-virtual {p0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 p1, 0xa

    .line 365
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    return-void
.end method

.method static b(Ljava/util/List;I)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;I)Z"
        }
    .end annotation

    .line 392
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    shl-int/lit8 v0, v0, 0x6

    if-ge p1, v0, :cond_0

    div-int/lit8 v0, p1, 0x40

    .line 393
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    rem-int/lit8 p1, p1, 0x40

    shl-long p0, v2, p1

    and-long/2addr p0, v0

    const-wide/16 v0, 0x0

    cmp-long v2, p0, v0

    if-eqz v2, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method static c(Lcom/google/android/gms/internal/measurement/al$c;Ljava/lang/String;)Ljava/lang/Object;
    .locals 0

    .line 29
    invoke-static {p0, p1}, Lcom/google/android/gms/measurement/internal/ip;->b(Lcom/google/android/gms/internal/measurement/al$c;Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/al$e;

    move-result-object p0

    if-eqz p0, :cond_2

    .line 31
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/al$e;->ps()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 4012
    iget-object p0, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzwr:Ljava/lang/String;

    return-object p0

    .line 33
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/al$e;->pt()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 4025
    iget-wide p0, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzwp:J

    .line 34
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    return-object p0

    .line 35
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/internal/measurement/al$e;->pu()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 4033
    iget-wide p0, p0, Lcom/google/android/gms/internal/measurement/al$e;->zzwt:D

    .line 36
    invoke-static {p0, p1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p0

    return-object p0

    :cond_2
    const/4 p0, 0x0

    return-object p0
.end method

.method static cL(Ljava/lang/String;)Z
    .locals 1

    if-eqz p0, :cond_0

    const-string v0, "([+-])?([0-9]+\\.?[0-9]*|[0-9]*\\.?[0-9]+)"

    .line 389
    invoke-virtual {p0, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 390
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p0

    const/16 v0, 0x136

    if-gt p0, v0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method static d(Lcom/google/android/gms/measurement/internal/zzai;Lcom/google/android/gms/measurement/internal/zzn;)Z
    .locals 0

    .line 380
    invoke-static {p0}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 381
    invoke-static {p1}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 382
    iget-object p0, p1, Lcom/google/android/gms/measurement/internal/zzn;->arz:Ljava/lang/String;

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_0

    iget-object p0, p1, Lcom/google/android/gms/measurement/internal/zzn;->aGa:Ljava/lang/String;

    .line 383
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x0

    return p0

    :cond_0
    const/4 p0, 0x1

    return p0
.end method


# virtual methods
.method final a([BLandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Landroid/os/Parcelable;",
            ">([B",
            "Landroid/os/Parcelable$Creator<",
            "TT;>;)TT;"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 369
    :cond_0
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 370
    :try_start_0
    array-length v2, p1

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v3, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 371
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 372
    invoke-interface {p2, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/os/Parcelable;
    :try_end_0
    .catch Lcom/google/android/gms/common/internal/safeparcel/a$a; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 373
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object p1

    :catchall_0
    move-exception p1

    goto :goto_0

    .line 376
    :catch_0
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 33014
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string p2, "Failed to load parcelable from buffer"

    .line 376
    invoke-virtual {p1, p2}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 377
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0

    .line 379
    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw p1
.end method

.method final a(Lcom/google/android/gms/internal/measurement/ad$a;)Ljava/lang/String;
    .locals 5

    if-nez p1, :cond_0

    const-string p1, "null"

    return-object p1

    .line 200
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\nevent_filter {\n"

    .line 201
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/ad$a;->ox()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    .line 25006
    iget v1, p1, Lcom/google/android/gms/internal/measurement/ad$a;->zzuf:I

    .line 203
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v3, "filter_id"

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 205
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object v1

    .line 25007
    iget-object v3, p1, Lcom/google/android/gms/internal/measurement/ad$a;->zzug:Ljava/lang/String;

    .line 205
    invoke-virtual {v1, v3}, Lcom/google/android/gms/measurement/internal/df;->cf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "event_name"

    .line 206
    invoke-static {v0, v2, v3, v1}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 25026
    iget-boolean v1, p1, Lcom/google/android/gms/internal/measurement/ad$a;->zzuk:Z

    .line 25027
    iget-boolean v3, p1, Lcom/google/android/gms/internal/measurement/ad$a;->zzul:Z

    .line 25029
    iget-boolean v4, p1, Lcom/google/android/gms/internal/measurement/ad$a;->zzum:Z

    .line 209
    invoke-static {v1, v3, v4}, Lcom/google/android/gms/measurement/internal/ip;->a(ZZZ)Ljava/lang/String;

    move-result-object v1

    .line 210
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "filter_type"

    .line 211
    invoke-static {v0, v2, v3, v1}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 212
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/ad$a;->oy()Lcom/google/android/gms/internal/measurement/ad$c;

    move-result-object v1

    const/4 v2, 0x1

    const-string v3, "event_count_filter"

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/gms/measurement/internal/ip;->a(Ljava/lang/StringBuilder;ILjava/lang/String;Lcom/google/android/gms/internal/measurement/ad$c;)V

    const-string v1, "  filters {\n"

    .line 213
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26013
    iget-object p1, p1, Lcom/google/android/gms/internal/measurement/ad$a;->zzuh:Lcom/google/android/gms/internal/measurement/dz;

    .line 214
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/measurement/ad$b;

    const/4 v3, 0x2

    .line 215
    invoke-direct {p0, v0, v3, v1}, Lcom/google/android/gms/measurement/internal/ip;->a(Ljava/lang/StringBuilder;ILcom/google/android/gms/internal/measurement/ad$b;)V

    goto :goto_0

    .line 217
    :cond_3
    invoke-static {v0, v2}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;I)V

    const-string p1, "}\n}\n"

    .line 218
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method final a(Lcom/google/android/gms/internal/measurement/ad$d;)Ljava/lang/String;
    .locals 5

    if-nez p1, :cond_0

    const-string p1, "null"

    return-object p1

    .line 222
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\nproperty_filter {\n"

    .line 223
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/ad$d;->ox()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    .line 27005
    iget v1, p1, Lcom/google/android/gms/internal/measurement/ad$d;->zzuf:I

    .line 225
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v3, "filter_id"

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 227
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object v1

    .line 27006
    iget-object v3, p1, Lcom/google/android/gms/internal/measurement/ad$d;->zzvh:Ljava/lang/String;

    .line 227
    invoke-virtual {v1, v3}, Lcom/google/android/gms/measurement/internal/df;->ch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "property_name"

    .line 228
    invoke-static {v0, v2, v3, v1}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 27013
    iget-boolean v1, p1, Lcom/google/android/gms/internal/measurement/ad$d;->zzuk:Z

    .line 27014
    iget-boolean v3, p1, Lcom/google/android/gms/internal/measurement/ad$d;->zzul:Z

    .line 27016
    iget-boolean v4, p1, Lcom/google/android/gms/internal/measurement/ad$d;->zzum:Z

    .line 233
    invoke-static {v1, v3, v4}, Lcom/google/android/gms/measurement/internal/ip;->a(ZZZ)Ljava/lang/String;

    move-result-object v1

    .line 234
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "filter_type"

    .line 235
    invoke-static {v0, v2, v3, v1}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    :cond_2
    const/4 v1, 0x1

    .line 236
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/ad$d;->oR()Lcom/google/android/gms/internal/measurement/ad$b;

    move-result-object p1

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/measurement/internal/ip;->a(Ljava/lang/StringBuilder;ILcom/google/android/gms/internal/measurement/ad$b;)V

    const-string p1, "}\n"

    .line 237
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 238
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method final a(Lcom/google/android/gms/internal/measurement/al$f;)Ljava/lang/String;
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    if-nez v1, :cond_0

    const-string v1, ""

    return-object v1

    .line 58
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\nbatch {\n"

    .line 59
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6004
    iget-object v1, v1, Lcom/google/android/gms/internal/measurement/al$f;->zzwv:Lcom/google/android/gms/internal/measurement/dz;

    .line 60
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const-string v4, "}\n"

    if-eqz v3, :cond_3a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/internal/measurement/al$g;

    if-eqz v3, :cond_1

    const/4 v5, 0x1

    .line 63
    invoke-static {v2, v5}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;I)V

    const-string v6, "bundle {\n"

    .line 64
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6023
    iget v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzue:I

    and-int/2addr v6, v5

    if-eqz v6, :cond_2

    const/4 v6, 0x1

    goto :goto_1

    :cond_2
    const/4 v6, 0x0

    :goto_1
    if-eqz v6, :cond_3

    .line 6024
    iget v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzwy:I

    .line 66
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const-string v8, "protocol_version"

    invoke-static {v2, v5, v8, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 6103
    :cond_3
    iget-object v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzxg:Ljava/lang/String;

    const-string v8, "platform"

    .line 67
    invoke-static {v2, v5, v8, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 6150
    iget v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzue:I

    and-int/lit16 v6, v6, 0x4000

    if-eqz v6, :cond_4

    const/4 v6, 0x1

    goto :goto_2

    :cond_4
    const/4 v6, 0x0

    :goto_2
    if-eqz v6, :cond_5

    .line 6151
    iget-wide v8, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzxo:J

    .line 69
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const-string v8, "gmp_version"

    invoke-static {v2, v5, v8, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 6155
    :cond_5
    iget v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzue:I

    const v8, 0x8000

    and-int/2addr v6, v8

    if-eqz v6, :cond_6

    const/4 v6, 0x1

    goto :goto_3

    :cond_6
    const/4 v6, 0x0

    :goto_3
    if-eqz v6, :cond_7

    .line 6156
    iget-wide v8, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzxp:J

    .line 71
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const-string v8, "uploading_gmp_version"

    invoke-static {v2, v5, v8, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 6283
    :cond_7
    iget v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzwx:I

    and-int/lit8 v6, v6, 0x10

    if-eqz v6, :cond_8

    const/4 v6, 0x1

    goto :goto_4

    :cond_8
    const/4 v6, 0x0

    :goto_4
    if-eqz v6, :cond_9

    .line 6284
    iget-wide v8, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzym:J

    .line 73
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const-string v8, "dynamite_version"

    invoke-static {v2, v5, v8, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 7236
    :cond_9
    iget v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzue:I

    const/high16 v8, 0x20000000

    and-int/2addr v6, v8

    if-eqz v6, :cond_a

    const/4 v6, 0x1

    goto :goto_5

    :cond_a
    const/4 v6, 0x0

    :goto_5
    if-eqz v6, :cond_b

    .line 7237
    iget-wide v8, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzye:J

    .line 75
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const-string v8, "config_version"

    invoke-static {v2, v5, v8, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 8199
    :cond_b
    iget-object v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzxw:Ljava/lang/String;

    const-string v8, "gmp_app_id"

    .line 76
    invoke-static {v2, v5, v8, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 8263
    iget-object v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzyj:Ljava/lang/String;

    const-string v8, "admob_app_id"

    .line 77
    invoke-static {v2, v5, v8, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 9138
    iget-object v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzxm:Ljava/lang/String;

    const-string v8, "app_id"

    .line 78
    invoke-static {v2, v5, v8, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 9144
    iget-object v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzxn:Ljava/lang/String;

    const-string v8, "app_version"

    .line 79
    invoke-static {v2, v5, v8, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 9225
    iget v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzue:I

    const/high16 v8, 0x2000000

    and-int/2addr v6, v8

    if-eqz v6, :cond_c

    const/4 v6, 0x1

    goto :goto_6

    :cond_c
    const/4 v6, 0x0

    :goto_6
    if-eqz v6, :cond_d

    .line 9226
    iget v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzya:I

    .line 81
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const-string v8, "app_version_major"

    invoke-static {v2, v5, v8, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 10219
    :cond_d
    iget-object v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzxz:Ljava/lang/String;

    const-string v8, "firebase_instance_id"

    .line 82
    invoke-static {v2, v5, v8, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 11177
    iget v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzue:I

    const/high16 v8, 0x80000

    and-int/2addr v6, v8

    if-eqz v6, :cond_e

    const/4 v6, 0x1

    goto :goto_7

    :cond_e
    const/4 v6, 0x0

    :goto_7
    if-eqz v6, :cond_f

    .line 11178
    iget-wide v8, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzxt:J

    .line 84
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const-string v8, "dev_cert_hash"

    invoke-static {v2, v5, v8, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 12132
    :cond_f
    iget-object v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzxl:Ljava/lang/String;

    const-string v8, "app_store"

    .line 85
    invoke-static {v2, v5, v8, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 13072
    iget v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzue:I

    const/4 v8, 0x2

    and-int/2addr v6, v8

    if-eqz v6, :cond_10

    const/4 v6, 0x1

    goto :goto_8

    :cond_10
    const/4 v6, 0x0

    :goto_8
    if-eqz v6, :cond_11

    .line 13073
    iget-wide v9, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzxb:J

    .line 87
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const-string v9, "upload_timestamp_millis"

    invoke-static {v2, v5, v9, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 13077
    :cond_11
    iget v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzue:I

    and-int/lit8 v6, v6, 0x4

    if-eqz v6, :cond_12

    const/4 v6, 0x1

    goto :goto_9

    :cond_12
    const/4 v6, 0x0

    :goto_9
    if-eqz v6, :cond_13

    .line 13078
    iget-wide v9, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzxc:J

    .line 89
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const-string v9, "start_timestamp_millis"

    invoke-static {v2, v5, v9, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 90
    :cond_13
    invoke-virtual {v3}, Lcom/google/android/gms/internal/measurement/al$g;->pF()Z

    move-result v6

    if-eqz v6, :cond_14

    .line 13083
    iget-wide v9, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzxd:J

    .line 91
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const-string v9, "end_timestamp_millis"

    invoke-static {v2, v5, v9, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 13087
    :cond_14
    iget v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzue:I

    and-int/lit8 v6, v6, 0x10

    if-eqz v6, :cond_15

    const/4 v6, 0x1

    goto :goto_a

    :cond_15
    const/4 v6, 0x0

    :goto_a
    if-eqz v6, :cond_16

    .line 13088
    iget-wide v9, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzxe:J

    .line 94
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const-string v9, "previous_bundle_start_timestamp_millis"

    .line 95
    invoke-static {v2, v5, v9, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 13095
    :cond_16
    iget v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzue:I

    and-int/lit8 v6, v6, 0x20

    if-eqz v6, :cond_17

    const/4 v6, 0x1

    goto :goto_b

    :cond_17
    const/4 v6, 0x0

    :goto_b
    if-eqz v6, :cond_18

    .line 13096
    iget-wide v9, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzxf:J

    .line 98
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const-string v9, "previous_bundle_end_timestamp_millis"

    .line 99
    invoke-static {v2, v5, v9, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 13171
    :cond_18
    iget-object v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzxs:Ljava/lang/String;

    const-string v9, "app_instance_id"

    .line 100
    invoke-static {v2, v5, v9, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 14160
    iget-object v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzxq:Ljava/lang/String;

    const-string v9, "resettable_device_id"

    .line 101
    invoke-static {v2, v5, v9, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 14230
    iget-object v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzyd:Ljava/lang/String;

    const-string v9, "device_id"

    .line 102
    invoke-static {v2, v5, v9, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 14246
    iget-object v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzyg:Ljava/lang/String;

    const-string v9, "ds_id"

    .line 103
    invoke-static {v2, v5, v9, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 15166
    iget v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzue:I

    const/high16 v9, 0x20000

    and-int/2addr v6, v9

    if-eqz v6, :cond_19

    const/4 v6, 0x1

    goto :goto_c

    :cond_19
    const/4 v6, 0x0

    :goto_c
    if-eqz v6, :cond_1a

    .line 15167
    iget-boolean v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzxr:Z

    .line 105
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    const-string v9, "limited_ad_tracking"

    invoke-static {v2, v5, v9, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 16109
    :cond_1a
    iget-object v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzxh:Ljava/lang/String;

    const-string v9, "os_version"

    .line 106
    invoke-static {v2, v5, v9, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 16115
    iget-object v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzxi:Ljava/lang/String;

    const-string v9, "device_model"

    .line 107
    invoke-static {v2, v5, v9, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 16121
    iget-object v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzxj:Ljava/lang/String;

    const-string v9, "user_default_language"

    .line 108
    invoke-static {v2, v5, v9, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 16127
    iget v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzue:I

    and-int/lit16 v6, v6, 0x400

    if-eqz v6, :cond_1b

    const/4 v6, 0x1

    goto :goto_d

    :cond_1b
    const/4 v6, 0x0

    :goto_d
    if-eqz v6, :cond_1c

    .line 16128
    iget v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzxk:I

    .line 110
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const-string v9, "time_zone_offset_minutes"

    invoke-static {v2, v5, v9, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 16182
    :cond_1c
    iget v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzue:I

    const/high16 v9, 0x100000

    and-int/2addr v6, v9

    if-eqz v6, :cond_1d

    const/4 v6, 0x1

    goto :goto_e

    :cond_1d
    const/4 v6, 0x0

    :goto_e
    if-eqz v6, :cond_1e

    .line 16183
    iget v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzxu:I

    .line 112
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const-string v9, "bundle_sequential_index"

    invoke-static {v2, v5, v9, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 16205
    :cond_1e
    iget v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzue:I

    const/high16 v9, 0x800000

    and-int/2addr v6, v9

    if-eqz v6, :cond_1f

    const/4 v6, 0x1

    goto :goto_f

    :cond_1f
    const/4 v6, 0x0

    :goto_f
    if-eqz v6, :cond_20

    .line 16206
    iget-boolean v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzxx:Z

    .line 114
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    const-string v9, "service_upload"

    invoke-static {v2, v5, v9, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 17187
    :cond_20
    iget-object v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzxv:Ljava/lang/String;

    const-string v9, "health_monitor"

    .line 115
    invoke-static {v2, v5, v9, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 17241
    iget v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzue:I

    const/high16 v9, 0x40000000    # 2.0f

    and-int/2addr v6, v9

    if-eqz v6, :cond_21

    const/4 v6, 0x1

    goto :goto_10

    :cond_21
    const/4 v6, 0x0

    :goto_10
    if-eqz v6, :cond_22

    .line 17242
    iget-wide v9, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzyf:J

    const-wide/16 v11, 0x0

    cmp-long v6, v9, v11

    if-eqz v6, :cond_22

    .line 18242
    iget-wide v9, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzyf:J

    .line 117
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const-string v9, "android_id"

    invoke-static {v2, v5, v9, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 118
    :cond_22
    invoke-virtual {v3}, Lcom/google/android/gms/internal/measurement/al$g;->pG()Z

    move-result v6

    if-eqz v6, :cond_23

    .line 18259
    iget v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzyi:I

    .line 119
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const-string v9, "retry_counter"

    invoke-static {v2, v5, v9, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 19049
    :cond_23
    iget-object v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzxa:Lcom/google/android/gms/internal/measurement/dz;

    const-string v9, "double_value"

    const-string v10, "int_value"

    const-string v11, "string_value"

    const-string v12, "name"

    const/4 v13, 0x0

    if-eqz v6, :cond_28

    .line 123
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_24
    :goto_11
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_28

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/gms/internal/measurement/al$k;

    if-eqz v14, :cond_24

    .line 125
    invoke-static {v2, v8}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;I)V

    const-string v15, "user_property {\n"

    .line 126
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    invoke-virtual {v14}, Lcom/google/android/gms/internal/measurement/al$k;->qp()Z

    move-result v15

    if-eqz v15, :cond_25

    move-object v15, v6

    .line 20006
    iget-wide v5, v14, Lcom/google/android/gms/internal/measurement/al$k;->zzzc:J

    .line 128
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto :goto_12

    :cond_25
    move-object v15, v6

    move-object v5, v13

    :goto_12
    const-string v6, "set_timestamp_millis"

    .line 129
    invoke-static {v2, v8, v6, v5}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 131
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object v5

    .line 20010
    iget-object v6, v14, Lcom/google/android/gms/internal/measurement/al$k;->zzwk:Ljava/lang/String;

    .line 131
    invoke-virtual {v5, v6}, Lcom/google/android/gms/measurement/internal/df;->ch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 132
    invoke-static {v2, v8, v12, v5}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 20017
    iget-object v5, v14, Lcom/google/android/gms/internal/measurement/al$k;->zzwr:Ljava/lang/String;

    .line 133
    invoke-static {v2, v8, v11, v5}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 134
    invoke-virtual {v14}, Lcom/google/android/gms/internal/measurement/al$k;->pt()Z

    move-result v5

    if-eqz v5, :cond_26

    .line 20030
    iget-wide v5, v14, Lcom/google/android/gms/internal/measurement/al$k;->zzwp:J

    .line 134
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto :goto_13

    :cond_26
    move-object v5, v13

    :goto_13
    invoke-static {v2, v8, v10, v5}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 136
    invoke-virtual {v14}, Lcom/google/android/gms/internal/measurement/al$k;->pu()Z

    move-result v5

    if-eqz v5, :cond_27

    .line 20038
    iget-wide v5, v14, Lcom/google/android/gms/internal/measurement/al$k;->zzwt:D

    .line 136
    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    goto :goto_14

    :cond_27
    move-object v5, v13

    .line 137
    :goto_14
    invoke-static {v2, v8, v9, v5}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 138
    invoke-static {v2, v8}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;I)V

    .line 139
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v6, v15

    const/4 v5, 0x1

    goto :goto_11

    .line 20210
    :cond_28
    iget-object v5, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzxy:Lcom/google/android/gms/internal/measurement/dz;

    .line 21138
    iget-object v6, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzxm:Ljava/lang/String;

    if-eqz v5, :cond_2e

    .line 146
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_29
    :goto_15
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_2e

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/gms/internal/measurement/al$a;

    if-eqz v14, :cond_29

    .line 148
    invoke-static {v2, v8}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;I)V

    const-string v15, "audience_membership {\n"

    .line 149
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22003
    iget v15, v14, Lcom/google/android/gms/internal/measurement/al$a;->zzue:I

    const/16 v16, 0x1

    and-int/lit8 v15, v15, 0x1

    if-eqz v15, :cond_2a

    const/4 v15, 0x1

    goto :goto_16

    :cond_2a
    const/4 v15, 0x0

    :goto_16
    if-eqz v15, :cond_2b

    .line 22004
    iget v15, v14, Lcom/google/android/gms/internal/measurement/al$a;->zzwb:I

    .line 151
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    const-string v7, "audience_id"

    invoke-static {v2, v8, v7, v15}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 22019
    :cond_2b
    iget v7, v14, Lcom/google/android/gms/internal/measurement/al$a;->zzue:I

    and-int/lit8 v7, v7, 0x8

    if-eqz v7, :cond_2c

    const/4 v7, 0x1

    goto :goto_17

    :cond_2c
    const/4 v7, 0x0

    :goto_17
    if-eqz v7, :cond_2d

    .line 22020
    iget-boolean v7, v14, Lcom/google/android/gms/internal/measurement/al$a;->zzwe:Z

    .line 153
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    const-string v15, "new_audience"

    invoke-static {v2, v8, v15, v7}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 154
    :cond_2d
    invoke-virtual {v14}, Lcom/google/android/gms/internal/measurement/al$a;->pa()Lcom/google/android/gms/internal/measurement/al$i;

    move-result-object v7

    const-string v15, "current_data"

    invoke-direct {v0, v2, v15, v7, v6}, Lcom/google/android/gms/measurement/internal/ip;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Lcom/google/android/gms/internal/measurement/al$i;Ljava/lang/String;)V

    .line 155
    invoke-virtual {v14}, Lcom/google/android/gms/internal/measurement/al$a;->pb()Lcom/google/android/gms/internal/measurement/al$i;

    move-result-object v7

    const-string v14, "previous_data"

    invoke-direct {v0, v2, v14, v7, v6}, Lcom/google/android/gms/measurement/internal/ip;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Lcom/google/android/gms/internal/measurement/al$i;Ljava/lang/String;)V

    .line 156
    invoke-static {v2, v8}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;I)V

    .line 157
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_15

    .line 22028
    :cond_2e
    iget-object v3, v3, Lcom/google/android/gms/internal/measurement/al$g;->zzwz:Lcom/google/android/gms/internal/measurement/dz;

    if-eqz v3, :cond_39

    .line 162
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2f
    :goto_18
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_39

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/internal/measurement/al$c;

    if-eqz v5, :cond_2f

    .line 164
    invoke-static {v2, v8}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;I)V

    const-string v6, "event {\n"

    .line 165
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object v6

    .line 22031
    iget-object v7, v5, Lcom/google/android/gms/internal/measurement/al$c;->zzwk:Ljava/lang/String;

    .line 166
    invoke-virtual {v6, v7}, Lcom/google/android/gms/measurement/internal/df;->cf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v8, v12, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 167
    invoke-virtual {v5}, Lcom/google/android/gms/internal/measurement/al$c;->pl()Z

    move-result v6

    if-eqz v6, :cond_30

    .line 22038
    iget-wide v6, v5, Lcom/google/android/gms/internal/measurement/al$c;->zzwl:J

    .line 168
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const-string v7, "timestamp_millis"

    invoke-static {v2, v8, v7, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 22042
    :cond_30
    iget v6, v5, Lcom/google/android/gms/internal/measurement/al$c;->zzue:I

    and-int/lit8 v6, v6, 0x4

    if-eqz v6, :cond_31

    const/4 v6, 0x1

    goto :goto_19

    :cond_31
    const/4 v6, 0x0

    :goto_19
    if-eqz v6, :cond_32

    .line 22043
    iget-wide v6, v5, Lcom/google/android/gms/internal/measurement/al$c;->zzwm:J

    .line 170
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const-string v7, "previous_timestamp_millis"

    invoke-static {v2, v8, v7, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 22047
    :cond_32
    iget v6, v5, Lcom/google/android/gms/internal/measurement/al$c;->zzue:I

    and-int/lit8 v6, v6, 0x8

    if-eqz v6, :cond_33

    const/4 v6, 0x1

    goto :goto_1a

    :cond_33
    const/4 v6, 0x0

    :goto_1a
    if-eqz v6, :cond_34

    .line 22048
    iget v6, v5, Lcom/google/android/gms/internal/measurement/al$c;->zzwn:I

    .line 172
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const-string v7, "count"

    invoke-static {v2, v8, v7, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 173
    :cond_34
    invoke-virtual {v5}, Lcom/google/android/gms/internal/measurement/al$c;->pj()I

    move-result v6

    if-eqz v6, :cond_38

    .line 23005
    iget-object v5, v5, Lcom/google/android/gms/internal/measurement/al$c;->zzwj:Lcom/google/android/gms/internal/measurement/dz;

    if-eqz v5, :cond_38

    .line 177
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_35
    :goto_1b
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_38

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/internal/measurement/al$e;

    if-eqz v6, :cond_35

    const/4 v7, 0x3

    .line 179
    invoke-static {v2, v7}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;I)V

    const-string v14, "param {\n"

    .line 180
    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object v14

    .line 24005
    iget-object v15, v6, Lcom/google/android/gms/internal/measurement/al$e;->zzwk:Ljava/lang/String;

    .line 181
    invoke-virtual {v14, v15}, Lcom/google/android/gms/measurement/internal/df;->cg(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v2, v7, v12, v14}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 24012
    iget-object v14, v6, Lcom/google/android/gms/internal/measurement/al$e;->zzwr:Ljava/lang/String;

    .line 182
    invoke-static {v2, v7, v11, v14}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 183
    invoke-virtual {v6}, Lcom/google/android/gms/internal/measurement/al$e;->pt()Z

    move-result v14

    if-eqz v14, :cond_36

    .line 24025
    iget-wide v14, v6, Lcom/google/android/gms/internal/measurement/al$e;->zzwp:J

    .line 183
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    goto :goto_1c

    :cond_36
    move-object v14, v13

    :goto_1c
    invoke-static {v2, v7, v10, v14}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 185
    invoke-virtual {v6}, Lcom/google/android/gms/internal/measurement/al$e;->pu()Z

    move-result v14

    if-eqz v14, :cond_37

    .line 24033
    iget-wide v14, v6, Lcom/google/android/gms/internal/measurement/al$e;->zzwt:D

    .line 185
    invoke-static {v14, v15}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    goto :goto_1d

    :cond_37
    move-object v6, v13

    .line 186
    :goto_1d
    invoke-static {v2, v7, v9, v6}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    .line 187
    invoke-static {v2, v7}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;I)V

    .line 188
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1b

    .line 190
    :cond_38
    invoke-static {v2, v8}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;I)V

    .line 191
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_18

    :cond_39
    const/4 v3, 0x1

    .line 193
    invoke-static {v2, v3}, Lcom/google/android/gms/measurement/internal/ip;->b(Ljava/lang/StringBuilder;I)V

    .line 194
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 196
    :cond_3a
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method final a(Lcom/google/android/gms/internal/measurement/al$e$a;Ljava/lang/Object;)V
    .locals 2

    .line 14
    invoke-static {p2}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 15
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/al$e$a;->px()Lcom/google/android/gms/internal/measurement/al$e$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/al$e$a;->py()Lcom/google/android/gms/internal/measurement/al$e$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/al$e$a;->pz()Lcom/google/android/gms/internal/measurement/al$e$a;

    .line 16
    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 17
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p1, p2}, Lcom/google/android/gms/internal/measurement/al$e$a;->bz(Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/al$e$a;

    return-void

    .line 18
    :cond_0
    instance-of v0, p2, Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 19
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/measurement/al$e$a;->D(J)Lcom/google/android/gms/internal/measurement/al$e$a;

    return-void

    .line 20
    :cond_1
    instance-of v0, p2, Ljava/lang/Double;

    if-eqz v0, :cond_2

    .line 21
    check-cast p2, Ljava/lang/Double;

    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/measurement/al$e$a;->b(D)Lcom/google/android/gms/internal/measurement/al$e$a;

    return-void

    .line 22
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 2014
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string v0, "Ignoring invalid (type) event param value"

    .line 22
    invoke-virtual {p1, v0, p2}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method final a(Lcom/google/android/gms/internal/measurement/al$k$a;Ljava/lang/Object;)V
    .locals 2

    .line 4
    invoke-static {p2}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 5
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/al$k$a;->qs()Lcom/google/android/gms/internal/measurement/al$k$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/al$k$a;->qt()Lcom/google/android/gms/internal/measurement/al$k$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/al$k$a;->qu()Lcom/google/android/gms/internal/measurement/al$k$a;

    .line 6
    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 7
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p1, p2}, Lcom/google/android/gms/internal/measurement/al$k$a;->bP(Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/al$k$a;

    return-void

    .line 8
    :cond_0
    instance-of v0, p2, Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 9
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/measurement/al$k$a;->S(J)Lcom/google/android/gms/internal/measurement/al$k$a;

    return-void

    .line 10
    :cond_1
    instance-of v0, p2, Ljava/lang/Double;

    if-eqz v0, :cond_2

    .line 11
    check-cast p2, Ljava/lang/Double;

    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/measurement/al$k$a;->c(D)Lcom/google/android/gms/internal/measurement/al$k$a;

    return-void

    .line 12
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 1014
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string v0, "Ignoring invalid (type) user attribute value"

    .line 12
    invoke-virtual {p1, v0, p2}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method final b(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 407
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 408
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    .line 409
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gez v1, :cond_0

    .line 410
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 33017
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    const-string v2, "Ignoring negative bit index to be cleared"

    .line 410
    invoke-virtual {v1, v2, p2}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 412
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    div-int/lit8 v1, v1, 0x40

    .line 413
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 414
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 34017
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    .line 416
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "Ignoring bit index greater than bitSet size"

    invoke-virtual {v1, v3, p2, v2}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 418
    :cond_1
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x1

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    rem-int/lit8 p2, p2, 0x40

    shl-long/2addr v4, p2

    const-wide/16 v6, -0x1

    xor-long/2addr v4, v6

    and-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {v0, v1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 420
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p1

    .line 421
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p2

    add-int/lit8 p2, p2, -0x1

    :goto_1
    move v8, p2

    move p2, p1

    move p1, v8

    if-ltz p1, :cond_3

    .line 422
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-nez v5, :cond_3

    add-int/lit8 p2, p1, -0x1

    goto :goto_1

    :cond_3
    const/4 p1, 0x0

    .line 425
    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method final d(JJ)Z
    .locals 3

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-eqz v2, :cond_1

    cmp-long v2, p3, v0

    if-lez v2, :cond_1

    .line 427
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/e;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide p1

    cmp-long v0, p1, p3

    if-lez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final bridge synthetic getContext()Landroid/content/Context;
    .locals 1

    .line 494
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ij;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic lX()V
    .locals 0

    .line 491
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ij;->lX()V

    return-void
.end method

.method final u([B)J
    .locals 2

    .line 429
    invoke-static {p1}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 430
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 431
    invoke-static {}, Lcom/google/android/gms/measurement/internal/it;->getMessageDigest()Ljava/security/MessageDigest;

    move-result-object v0

    if-nez v0, :cond_0

    .line 433
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 35014
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string v0, "Failed to get MD5"

    .line 433
    invoke-virtual {p1, v0}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    return-wide v0

    .line 435
    :cond_0
    invoke-virtual {v0, p1}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object p1

    .line 436
    invoke-static {p1}, Lcom/google/android/gms/measurement/internal/it;->x([B)J

    move-result-wide v0

    return-wide v0
.end method

.method final v([B)[B
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 437
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 438
    new-instance p1, Ljava/util/zip/GZIPInputStream;

    invoke-direct {p1, v0}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    .line 439
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v2, 0x400

    new-array v2, v2, [B

    .line 441
    :goto_0
    invoke-virtual {p1, v2}, Ljava/util/zip/GZIPInputStream;->read([B)I

    move-result v3

    if-lez v3, :cond_0

    const/4 v4, 0x0

    .line 443
    invoke-virtual {v1, v2, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 445
    :cond_0
    invoke-virtual {p1}, Ljava/util/zip/GZIPInputStream;->close()V

    .line 446
    invoke-virtual {v0}, Ljava/io/ByteArrayInputStream;->close()V

    .line 447
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 449
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 36014
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "Failed to ungzip content"

    .line 449
    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    .line 450
    goto :goto_2

    :goto_1
    throw p1

    :goto_2
    goto :goto_1
.end method

.method public final bridge synthetic vD()Lcom/google/android/gms/measurement/internal/d;
    .locals 1

    .line 492
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ij;->vD()Lcom/google/android/gms/measurement/internal/d;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vE()Lcom/google/android/gms/common/util/e;
    .locals 1

    .line 493
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ij;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vF()Lcom/google/android/gms/measurement/internal/df;
    .locals 1

    .line 495
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ij;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vG()Lcom/google/android/gms/measurement/internal/it;
    .locals 1

    .line 496
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ij;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vH()Lcom/google/android/gms/measurement/internal/ed;
    .locals 1

    .line 497
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ij;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vI()Lcom/google/android/gms/measurement/internal/dh;
    .locals 1

    .line 498
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ij;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vJ()Lcom/google/android/gms/measurement/internal/dp;
    .locals 1

    .line 499
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ij;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vK()Lcom/google/android/gms/measurement/internal/jb;
    .locals 1

    .line 500
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ij;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v0

    return-object v0
.end method

.method protected final vM()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final bridge synthetic vu()V
    .locals 0

    .line 488
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ij;->vu()V

    return-void
.end method

.method public final bridge synthetic vv()V
    .locals 0

    .line 490
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ij;->vv()V

    return-void
.end method

.method final w([B)[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 451
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 452
    new-instance v1, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v1, v0}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 453
    invoke-virtual {v1, p1}, Ljava/util/zip/GZIPOutputStream;->write([B)V

    .line 454
    invoke-virtual {v1}, Ljava/util/zip/GZIPOutputStream;->close()V

    .line 455
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 456
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 458
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 37014
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "Failed to gzip content"

    .line 458
    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    .line 459
    throw p1
.end method

.method public final bridge synthetic xK()Lcom/google/android/gms/measurement/internal/ip;
    .locals 1

    .line 484
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ij;->xK()Lcom/google/android/gms/measurement/internal/ip;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic xL()Lcom/google/android/gms/measurement/internal/iz;
    .locals 1

    .line 485
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ij;->xL()Lcom/google/android/gms/measurement/internal/iz;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic xM()Lcom/google/android/gms/measurement/internal/jg;
    .locals 1

    .line 486
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ij;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic xN()Lcom/google/android/gms/measurement/internal/ee;
    .locals 1

    .line 487
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ij;->xN()Lcom/google/android/gms/measurement/internal/ee;

    move-result-object v0

    return-object v0
.end method

.method final zc()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 460
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ip;->aGH:Lcom/google/android/gms/measurement/internal/ii;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ii;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/j;->as(Landroid/content/Context;)Ljava/util/Map;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    .line 461
    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    if-nez v2, :cond_0

    goto/16 :goto_2

    .line 463
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 464
    sget-object v3, Lcom/google/android/gms/measurement/internal/j;->aER:Lcom/google/android/gms/measurement/internal/cv;

    .line 465
    invoke-virtual {v3, v1}, Lcom/google/android/gms/measurement/internal/cv;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 466
    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 467
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 468
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string v6, "measurement.id."

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 469
    :try_start_0
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_1

    .line 471
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 472
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-lt v4, v3, :cond_1

    .line 473
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v4

    .line 37017
    iget-object v4, v4, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    const-string v5, "Too many experiment IDs. Number of IDs"

    .line 475
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v4

    .line 479
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v5

    .line 38017
    iget-object v5, v5, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    const-string v6, "Experiment ID NumberFormatException"

    .line 479
    invoke-virtual {v5, v6, v4}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 481
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    return-object v1

    :cond_3
    return-object v2

    :cond_4
    :goto_2
    return-object v1
.end method
