.class public Lcom/google/android/gms/measurement/internal/fh;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/measurement/internal/fj;


# instance fields
.field protected final aDi:Lcom/google/android/gms/measurement/internal/ek;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/ek;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p1}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/fh;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    return-void
.end method


# virtual methods
.method public getContext()Landroid/content/Context;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fh;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public lX()V
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fh;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    return-void
.end method

.method public vD()Lcom/google/android/gms/measurement/internal/d;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fh;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vD()Lcom/google/android/gms/measurement/internal/d;

    move-result-object v0

    return-object v0
.end method

.method public vE()Lcom/google/android/gms/common/util/e;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fh;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object v0

    return-object v0
.end method

.method public vF()Lcom/google/android/gms/measurement/internal/df;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fh;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object v0

    return-object v0
.end method

.method public vG()Lcom/google/android/gms/measurement/internal/it;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fh;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v0

    return-object v0
.end method

.method public vH()Lcom/google/android/gms/measurement/internal/ed;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fh;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v0

    return-object v0
.end method

.method public vI()Lcom/google/android/gms/measurement/internal/dh;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fh;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    return-object v0
.end method

.method public vJ()Lcom/google/android/gms/measurement/internal/dp;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fh;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    return-object v0
.end method

.method public vK()Lcom/google/android/gms/measurement/internal/jb;
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fh;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    .line 1233
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/ek;->aIH:Lcom/google/android/gms/measurement/internal/jb;

    return-object v0
.end method

.method public vu()V
    .locals 0

    .line 21
    invoke-static {}, Lcom/google/android/gms/measurement/internal/ek;->vu()V

    return-void
.end method

.method public vv()V
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/fh;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ek;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vv()V

    return-void
.end method
