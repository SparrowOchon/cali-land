.class final Lcom/google/android/gms/measurement/internal/im;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final synthetic aJk:Lcom/google/android/gms/measurement/internal/zzn;

.field private final synthetic aLv:Lcom/google/android/gms/measurement/internal/ii;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/ii;Lcom/google/android/gms/measurement/internal/zzn;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/im;->aLv:Lcom/google/android/gms/measurement/internal/ii;

    iput-object p2, p0, Lcom/google/android/gms/measurement/internal/im;->aJk:Lcom/google/android/gms/measurement/internal/zzn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 3
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/im;->aLv:Lcom/google/android/gms/measurement/internal/ii;

    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/im;->aJk:Lcom/google/android/gms/measurement/internal/zzn;

    invoke-static {v0, v1}, Lcom/google/android/gms/measurement/internal/ii;->a(Lcom/google/android/gms/measurement/internal/ii;Lcom/google/android/gms/measurement/internal/zzn;)Lcom/google/android/gms/measurement/internal/ea;

    move-result-object v0

    if-nez v0, :cond_0

    .line 5
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/im;->aLv:Lcom/google/android/gms/measurement/internal/ii;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ii;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 1017
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGt:Lcom/google/android/gms/measurement/internal/dj;

    const-string v1, "App info was null when attempting to get app instance id"

    .line 7
    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0

    .line 9
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ea;->getAppInstanceId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
