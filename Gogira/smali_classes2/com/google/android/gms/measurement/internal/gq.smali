.class final Lcom/google/android/gms/measurement/internal/gq;
.super Lcom/google/android/gms/measurement/internal/ij;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/measurement/internal/ii;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/google/android/gms/measurement/internal/ij;-><init>(Lcom/google/android/gms/measurement/internal/ii;)V

    return-void
.end method

.method private static yG()Ljava/lang/String;
    .locals 2

    .line 211
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "This implementation should not be used."

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final b(Lcom/google/android/gms/measurement/internal/zzai;Ljava/lang/String;)[B
    .locals 25

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    move-object/from16 v12, p2

    const-string v2, "_r"

    .line 4
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 5
    invoke-static {}, Lcom/google/android/gms/measurement/internal/ek;->vu()V

    .line 6
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/common/internal/r;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    invoke-static/range {p2 .. p2}, Lcom/google/android/gms/common/internal/r;->aQ(Ljava/lang/String;)Ljava/lang/String;

    .line 8
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/measurement/internal/j;->aFo:Lcom/google/android/gms/measurement/internal/cv;

    .line 1083
    invoke-virtual {v3, v12, v4}, Lcom/google/android/gms/measurement/internal/jb;->d(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v3

    const/4 v4, 0x0

    if-nez v3, :cond_0

    .line 9
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 2021
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    const-string v2, "Generating ScionPayload disabled. packageName"

    .line 9
    invoke-virtual {v0, v2, v12}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    new-array v0, v4, [B

    return-object v0

    .line 11
    :cond_0
    iget-object v3, v0, Lcom/google/android/gms/measurement/internal/zzai;->name:Ljava/lang/String;

    const-string v5, "_iap"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const/4 v13, 0x0

    if-nez v3, :cond_1

    iget-object v3, v0, Lcom/google/android/gms/measurement/internal/zzai;->name:Ljava/lang/String;

    const-string v5, "_iapx"

    .line 12
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 13
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v2

    .line 3021
    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    .line 14
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/zzai;->name:Ljava/lang/String;

    const-string v3, "Generating a payload for this event is not available. package_name, event_name"

    .line 15
    invoke-virtual {v2, v3, v12, v0}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v13

    .line 17
    :cond_1
    invoke-static {}, Lcom/google/android/gms/internal/measurement/al$f;->pA()Lcom/google/android/gms/internal/measurement/al$f$a;

    move-result-object v14

    .line 18
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/jg;->beginTransaction()V

    .line 19
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v3

    invoke-virtual {v3, v12}, Lcom/google/android/gms/measurement/internal/jg;->dg(Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/ea;

    move-result-object v15

    if-nez v15, :cond_2

    .line 21
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 4021
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    const-string v2, "Log and bundle not available. package_name"

    .line 21
    invoke-virtual {v0, v2, v12}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    new-array v0, v4, [B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/jg;->endTransaction()V

    return-object v0

    .line 25
    :cond_2
    :try_start_1
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->yj()Z

    move-result v3

    if-nez v3, :cond_3

    .line 26
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 5021
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    const-string v2, "Log and bundle disabled. package_name"

    .line 26
    invoke-virtual {v0, v2, v12}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    new-array v0, v4, [B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 28
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/jg;->endTransaction()V

    return-object v0

    .line 30
    :cond_3
    :try_start_2
    invoke-static {}, Lcom/google/android/gms/internal/measurement/al$g;->pH()Lcom/google/android/gms/internal/measurement/al$g$a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/internal/measurement/al$g$a;->pJ()Lcom/google/android/gms/internal/measurement/al$g$a;

    move-result-object v3

    const-string v5, "android"

    invoke-virtual {v3, v5}, Lcom/google/android/gms/internal/measurement/al$g$a;->bA(Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/al$g$a;

    move-result-object v11

    .line 31
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->pT()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 32
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->pT()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v3}, Lcom/google/android/gms/internal/measurement/al$g$a;->bF(Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/al$g$a;

    .line 33
    :cond_4
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->yf()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 34
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->yf()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v3}, Lcom/google/android/gms/internal/measurement/al$g$a;->bE(Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/al$g$a;

    .line 35
    :cond_5
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->yd()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 36
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->yd()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v3}, Lcom/google/android/gms/internal/measurement/al$g$a;->bG(Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/al$g$a;

    .line 37
    :cond_6
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->ye()J

    move-result-wide v5

    const-wide/32 v7, -0x80000000

    cmp-long v3, v5, v7

    if-eqz v3, :cond_7

    .line 38
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->ye()J

    move-result-wide v5

    long-to-int v3, v5

    invoke-virtual {v11, v3}, Lcom/google/android/gms/internal/measurement/al$g$a;->bA(I)Lcom/google/android/gms/internal/measurement/al$g$a;

    .line 40
    :cond_7
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->yg()J

    move-result-wide v5

    invoke-virtual {v11, v5, v6}, Lcom/google/android/gms/internal/measurement/al$g$a;->J(J)Lcom/google/android/gms/internal/measurement/al$g$a;

    move-result-object v3

    .line 41
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->yi()J

    move-result-wide v5

    invoke-virtual {v3, v5, v6}, Lcom/google/android/gms/internal/measurement/al$g$a;->O(J)Lcom/google/android/gms/internal/measurement/al$g$a;

    .line 42
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->getGmpAppId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 43
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->getGmpAppId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v3}, Lcom/google/android/gms/internal/measurement/al$g$a;->bK(Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/al$g$a;

    goto :goto_0

    .line 44
    :cond_8
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->xB()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 45
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->xB()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v3}, Lcom/google/android/gms/internal/measurement/al$g$a;->bN(Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/al$g$a;

    .line 46
    :cond_9
    :goto_0
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->yh()J

    move-result-wide v5

    invoke-virtual {v11, v5, v6}, Lcom/google/android/gms/internal/measurement/al$g$a;->L(J)Lcom/google/android/gms/internal/measurement/al$g$a;

    .line 47
    iget-object v3, v1, Lcom/google/android/gms/measurement/internal/gq;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/ek;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 48
    invoke-static {}, Lcom/google/android/gms/measurement/internal/jb;->zq()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 49
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v3

    invoke-virtual {v11}, Lcom/google/android/gms/internal/measurement/al$g$a;->pT()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/android/gms/measurement/internal/jb;->cX(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 51
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 52
    invoke-virtual {v11}, Lcom/google/android/gms/internal/measurement/al$g$a;->pW()Lcom/google/android/gms/internal/measurement/al$g$a;

    .line 54
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v3

    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->pT()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/android/gms/measurement/internal/dp;->cl(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v3

    .line 55
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->yq()Z

    move-result v5

    if-eqz v5, :cond_b

    iget-object v5, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Ljava/lang/CharSequence;

    .line 56
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v5, :cond_b

    .line 57
    :try_start_3
    iget-object v5, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-wide v5, v0, Lcom/google/android/gms/measurement/internal/zzai;->aDU:J

    .line 58
    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    .line 59
    invoke-static {}, Lcom/google/android/gms/measurement/internal/gq;->yG()Ljava/lang/String;

    move-result-object v5

    .line 60
    invoke-virtual {v11, v5}, Lcom/google/android/gms/internal/measurement/al$g$a;->bH(Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/al$g$a;
    :try_end_3
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 67
    :try_start_4
    iget-object v5, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v5, :cond_b

    .line 68
    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v11, v3}, Lcom/google/android/gms/internal/measurement/al$g$a;->I(Z)Lcom/google/android/gms/internal/measurement/al$g$a;

    goto :goto_1

    :catch_0
    move-exception v0

    .line 63
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v2

    .line 6021
    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    const-string v3, "Resettable device id encryption failed"

    .line 63
    invoke-virtual {v0}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    new-array v0, v4, [B
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 65
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/jg;->endTransaction()V

    return-object v0

    .line 70
    :cond_b
    :goto_1
    :try_start_5
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vD()Lcom/google/android/gms/measurement/internal/d;

    move-result-object v3

    .line 71
    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/fg;->vt()V

    .line 72
    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 73
    invoke-virtual {v11, v3}, Lcom/google/android/gms/internal/measurement/al$g$a;->bC(Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/al$g$a;

    move-result-object v3

    .line 74
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vD()Lcom/google/android/gms/measurement/internal/d;

    move-result-object v5

    .line 75
    invoke-virtual {v5}, Lcom/google/android/gms/measurement/internal/fg;->vt()V

    .line 76
    sget-object v5, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 77
    invoke-virtual {v3, v5}, Lcom/google/android/gms/internal/measurement/al$g$a;->bB(Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/al$g$a;

    move-result-object v3

    .line 78
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vD()Lcom/google/android/gms/measurement/internal/d;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/measurement/internal/d;->vN()J

    move-result-wide v5

    long-to-int v6, v5

    invoke-virtual {v3, v6}, Lcom/google/android/gms/internal/measurement/al$g$a;->by(I)Lcom/google/android/gms/internal/measurement/al$g$a;

    move-result-object v3

    .line 79
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vD()Lcom/google/android/gms/measurement/internal/d;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/measurement/internal/d;->vO()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/android/gms/internal/measurement/al$g$a;->bD(Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/al$g$a;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 81
    :try_start_6
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->getAppInstanceId()Ljava/lang/String;

    iget-wide v5, v0, Lcom/google/android/gms/measurement/internal/zzai;->aDU:J

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    .line 82
    invoke-static {}, Lcom/google/android/gms/measurement/internal/gq;->yG()Ljava/lang/String;

    move-result-object v3

    .line 83
    invoke-virtual {v11, v3}, Lcom/google/android/gms/internal/measurement/al$g$a;->bI(Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/al$g$a;
    :try_end_6
    .catch Ljava/lang/SecurityException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 90
    :try_start_7
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->getFirebaseInstanceId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 91
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->getFirebaseInstanceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v3}, Lcom/google/android/gms/internal/measurement/al$g$a;->bL(Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/al$g$a;

    .line 93
    :cond_c
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->pT()Ljava/lang/String;

    move-result-object v3

    .line 94
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/google/android/gms/measurement/internal/jg;->df(Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    .line 96
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_d
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_e

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/gms/measurement/internal/iq;

    const-string v8, "_lte"

    .line 97
    iget-object v9, v7, Lcom/google/android/gms/measurement/internal/iq;->name:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_d

    goto :goto_2

    :cond_e
    move-object v7, v13

    :goto_2
    const-wide/16 v23, 0x0

    if-eqz v7, :cond_f

    .line 101
    iget-object v6, v7, Lcom/google/android/gms/measurement/internal/iq;->value:Ljava/lang/Object;

    if-nez v6, :cond_10

    .line 102
    :cond_f
    new-instance v6, Lcom/google/android/gms/measurement/internal/iq;

    const-string v18, "auto"

    const-string v19, "_lte"

    .line 103
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/gms/common/util/e;->currentTimeMillis()J

    move-result-wide v20

    .line 104
    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    move-object/from16 v16, v6

    move-object/from16 v17, v3

    invoke-direct/range {v16 .. v22}, Lcom/google/android/gms/measurement/internal/iq;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/Object;)V

    .line 105
    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/google/android/gms/measurement/internal/jg;->a(Lcom/google/android/gms/measurement/internal/iq;)Z

    .line 107
    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v6

    sget-object v7, Lcom/google/android/gms/measurement/internal/j;->aFj:Lcom/google/android/gms/measurement/internal/cv;

    .line 7083
    invoke-virtual {v6, v3, v7}, Lcom/google/android/gms/measurement/internal/jb;->d(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v3

    const-wide/16 v6, 0x1

    if-eqz v3, :cond_13

    .line 108
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xK()Lcom/google/android/gms/measurement/internal/ip;

    move-result-object v3

    .line 109
    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v8

    .line 8022
    iget-object v8, v8, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    const-string v9, "Checking account type status for ad personalization signals"

    .line 109
    invoke-virtual {v8, v9}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 110
    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/fh;->vD()Lcom/google/android/gms/measurement/internal/d;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/gms/measurement/internal/d;->vQ()Z

    move-result v8

    if-eqz v8, :cond_13

    .line 111
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->pT()Ljava/lang/String;

    move-result-object v8

    .line 112
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->yq()Z

    move-result v9

    if-eqz v9, :cond_13

    .line 113
    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/ig;->xN()Lcom/google/android/gms/measurement/internal/ee;

    move-result-object v9

    invoke-virtual {v9, v8}, Lcom/google/android/gms/measurement/internal/ee;->cD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_13

    .line 114
    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v9

    .line 9021
    iget-object v9, v9, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    const-string v10, "Turning off ad personalization due to account type"

    .line 114
    invoke-virtual {v9, v10}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 115
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .line 116
    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_12

    .line 117
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/gms/measurement/internal/iq;

    const-string v4, "_npa"

    .line 118
    iget-object v10, v10, Lcom/google/android/gms/measurement/internal/iq;->name:Ljava/lang/String;

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 119
    invoke-interface {v9}, Ljava/util/Iterator;->remove()V

    goto :goto_4

    :cond_11
    const/4 v4, 0x0

    goto :goto_3

    .line 122
    :cond_12
    :goto_4
    new-instance v4, Lcom/google/android/gms/measurement/internal/iq;

    const-string v18, "auto"

    const-string v19, "_npa"

    .line 123
    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/fh;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/common/util/e;->currentTimeMillis()J

    move-result-wide v20

    .line 124
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    move-object/from16 v16, v4

    move-object/from16 v17, v8

    invoke-direct/range {v16 .. v22}, Lcom/google/android/gms/measurement/internal/iq;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/Object;)V

    .line 125
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    :cond_13
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Lcom/google/android/gms/internal/measurement/al$k;

    const/4 v4, 0x0

    .line 127
    :goto_5
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v8

    if-ge v4, v8, :cond_14

    .line 128
    invoke-static {}, Lcom/google/android/gms/internal/measurement/al$k;->qq()Lcom/google/android/gms/internal/measurement/al$k$a;

    move-result-object v8

    .line 129
    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/gms/measurement/internal/iq;

    iget-object v9, v9, Lcom/google/android/gms/measurement/internal/iq;->name:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/google/android/gms/internal/measurement/al$k$a;->bO(Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/al$k$a;

    move-result-object v8

    .line 130
    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/gms/measurement/internal/iq;

    iget-wide v9, v9, Lcom/google/android/gms/measurement/internal/iq;->aLy:J

    invoke-virtual {v8, v9, v10}, Lcom/google/android/gms/internal/measurement/al$k$a;->R(J)Lcom/google/android/gms/internal/measurement/al$k$a;

    move-result-object v8

    .line 131
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xK()Lcom/google/android/gms/measurement/internal/ip;

    move-result-object v9

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/gms/measurement/internal/iq;

    iget-object v10, v10, Lcom/google/android/gms/measurement/internal/iq;->value:Ljava/lang/Object;

    invoke-virtual {v9, v8, v10}, Lcom/google/android/gms/measurement/internal/ip;->a(Lcom/google/android/gms/internal/measurement/al$k$a;Ljava/lang/Object;)V

    .line 132
    invoke-virtual {v8}, Lcom/google/android/gms/internal/measurement/dr$a;->si()Lcom/google/android/gms/internal/measurement/fb;

    move-result-object v8

    check-cast v8, Lcom/google/android/gms/internal/measurement/dr;

    check-cast v8, Lcom/google/android/gms/internal/measurement/al$k;

    aput-object v8, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 135
    :cond_14
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v11, v3}, Lcom/google/android/gms/internal/measurement/al$g$a;->c(Ljava/lang/Iterable;)Lcom/google/android/gms/internal/measurement/al$g$a;

    .line 136
    iget-object v3, v0, Lcom/google/android/gms/measurement/internal/zzai;->aDQ:Lcom/google/android/gms/measurement/internal/zzah;

    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/zzah;->vR()Landroid/os/Bundle;

    move-result-object v9

    const-string v3, "_c"

    .line 137
    invoke-virtual {v9, v3, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 138
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v3

    .line 10021
    iget-object v3, v3, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    const-string v4, "Marking in-app purchase as real-time"

    .line 138
    invoke-virtual {v3, v4}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 139
    invoke-virtual {v9, v2, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v3, "_o"

    .line 140
    iget-object v4, v0, Lcom/google/android/gms/measurement/internal/zzai;->aAZ:Ljava/lang/String;

    invoke-virtual {v9, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v3

    invoke-virtual {v11}, Lcom/google/android/gms/internal/measurement/al$g$a;->pT()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/measurement/internal/it;->cT(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 142
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v3

    const-string v4, "_dbg"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v9, v4, v5}, Lcom/google/android/gms/measurement/internal/it;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 143
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v3

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v9, v2, v4}, Lcom/google/android/gms/measurement/internal/it;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 144
    :cond_15
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/gms/measurement/internal/zzai;->name:Ljava/lang/String;

    invoke-virtual {v2, v12, v3}, Lcom/google/android/gms/measurement/internal/jg;->L(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/f;

    move-result-object v2

    if-nez v2, :cond_16

    .line 147
    new-instance v16, Lcom/google/android/gms/measurement/internal/f;

    iget-object v4, v0, Lcom/google/android/gms/measurement/internal/zzai;->name:Ljava/lang/String;

    const-wide/16 v5, 0x0

    const-wide/16 v7, 0x0

    iget-wide v2, v0, Lcom/google/android/gms/measurement/internal/zzai;->aDU:J

    move-wide/from16 v17, v2

    move-object/from16 v2, v16

    move-object/from16 v3, p2

    move-object/from16 v19, v9

    move-wide/from16 v9, v17

    invoke-direct/range {v2 .. v10}, Lcom/google/android/gms/measurement/internal/f;-><init>(Ljava/lang/String;Ljava/lang/String;JJJ)V

    move-object/from16 v9, v16

    move-wide/from16 v16, v23

    goto :goto_6

    :cond_16
    move-object/from16 v19, v9

    .line 148
    iget-wide v3, v2, Lcom/google/android/gms/measurement/internal/f;->aDJ:J

    .line 149
    iget-wide v5, v0, Lcom/google/android/gms/measurement/internal/zzai;->aDU:J

    .line 150
    invoke-virtual {v2, v5, v6}, Lcom/google/android/gms/measurement/internal/f;->ak(J)Lcom/google/android/gms/measurement/internal/f;

    move-result-object v16

    move-object/from16 v9, v16

    move-wide/from16 v16, v3

    .line 151
    :goto_6
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v2

    invoke-virtual {v2, v9}, Lcom/google/android/gms/measurement/internal/jg;->a(Lcom/google/android/gms/measurement/internal/f;)V

    .line 152
    new-instance v10, Lcom/google/android/gms/measurement/internal/g;

    iget-object v3, v1, Lcom/google/android/gms/measurement/internal/gq;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    iget-object v4, v0, Lcom/google/android/gms/measurement/internal/zzai;->aAZ:Ljava/lang/String;

    iget-object v6, v0, Lcom/google/android/gms/measurement/internal/zzai;->name:Ljava/lang/String;

    iget-wide v7, v0, Lcom/google/android/gms/measurement/internal/zzai;->aDU:J

    move-object v2, v10

    move-object/from16 v5, p2

    move-object v13, v9

    move-object v1, v10

    move-wide/from16 v9, v16

    move-object v12, v11

    move-object/from16 v11, v19

    invoke-direct/range {v2 .. v11}, Lcom/google/android/gms/measurement/internal/g;-><init>(Lcom/google/android/gms/measurement/internal/ek;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLandroid/os/Bundle;)V

    .line 153
    invoke-static {}, Lcom/google/android/gms/internal/measurement/al$c;->pm()Lcom/google/android/gms/internal/measurement/al$c$a;

    move-result-object v2

    iget-wide v3, v1, Lcom/google/android/gms/measurement/internal/g;->timestamp:J

    .line 154
    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/internal/measurement/al$c$a;->A(J)Lcom/google/android/gms/internal/measurement/al$c$a;

    move-result-object v2

    iget-object v3, v1, Lcom/google/android/gms/measurement/internal/g;->name:Ljava/lang/String;

    .line 155
    invoke-virtual {v2, v3}, Lcom/google/android/gms/internal/measurement/al$c$a;->bw(Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/al$c$a;

    move-result-object v2

    iget-wide v3, v1, Lcom/google/android/gms/measurement/internal/g;->aDP:J

    .line 156
    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/internal/measurement/al$c$a;->B(J)Lcom/google/android/gms/internal/measurement/al$c$a;

    move-result-object v2

    .line 157
    iget-object v3, v1, Lcom/google/android/gms/measurement/internal/g;->aDQ:Lcom/google/android/gms/measurement/internal/zzah;

    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/zzah;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_17

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 158
    invoke-static {}, Lcom/google/android/gms/internal/measurement/al$e;->pv()Lcom/google/android/gms/internal/measurement/al$e$a;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/google/android/gms/internal/measurement/al$e$a;->by(Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/al$e$a;

    move-result-object v5

    .line 159
    iget-object v6, v1, Lcom/google/android/gms/measurement/internal/g;->aDQ:Lcom/google/android/gms/measurement/internal/zzah;

    invoke-virtual {v6, v4}, Lcom/google/android/gms/measurement/internal/zzah;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 160
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xK()Lcom/google/android/gms/measurement/internal/ip;

    move-result-object v6

    invoke-virtual {v6, v5, v4}, Lcom/google/android/gms/measurement/internal/ip;->a(Lcom/google/android/gms/internal/measurement/al$e$a;Ljava/lang/Object;)V

    .line 161
    invoke-virtual {v2, v5}, Lcom/google/android/gms/internal/measurement/al$c$a;->a(Lcom/google/android/gms/internal/measurement/al$e$a;)Lcom/google/android/gms/internal/measurement/al$c$a;

    goto :goto_7

    .line 164
    :cond_17
    invoke-virtual {v12, v2}, Lcom/google/android/gms/internal/measurement/al$g$a;->a(Lcom/google/android/gms/internal/measurement/al$c$a;)Lcom/google/android/gms/internal/measurement/al$g$a;

    move-result-object v1

    .line 165
    invoke-static {}, Lcom/google/android/gms/internal/measurement/al$h;->pY()Lcom/google/android/gms/internal/measurement/al$h$a;

    move-result-object v3

    .line 166
    invoke-static {}, Lcom/google/android/gms/internal/measurement/al$d;->pq()Lcom/google/android/gms/internal/measurement/al$d$a;

    move-result-object v4

    iget-wide v5, v13, Lcom/google/android/gms/measurement/internal/f;->aDG:J

    .line 167
    invoke-virtual {v4, v5, v6}, Lcom/google/android/gms/internal/measurement/al$d$a;->C(J)Lcom/google/android/gms/internal/measurement/al$d$a;

    move-result-object v4

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/zzai;->name:Ljava/lang/String;

    .line 168
    invoke-virtual {v4, v0}, Lcom/google/android/gms/internal/measurement/al$d$a;->bx(Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/al$d$a;

    move-result-object v0

    .line 169
    invoke-virtual {v3, v0}, Lcom/google/android/gms/internal/measurement/al$h$a;->a(Lcom/google/android/gms/internal/measurement/al$d$a;)Lcom/google/android/gms/internal/measurement/al$h$a;

    move-result-object v0

    .line 170
    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/measurement/al$g$a;->a(Lcom/google/android/gms/internal/measurement/al$h$a;)Lcom/google/android/gms/internal/measurement/al$g$a;

    .line 172
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xL()Lcom/google/android/gms/measurement/internal/iz;

    move-result-object v0

    .line 173
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->pT()Ljava/lang/String;

    move-result-object v1

    .line 174
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    .line 175
    invoke-virtual {v12}, Lcom/google/android/gms/internal/measurement/al$g$a;->pN()Ljava/util/List;

    move-result-object v4

    .line 176
    invoke-virtual {v0, v1, v3, v4}, Lcom/google/android/gms/measurement/internal/iz;->a(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 177
    invoke-virtual {v12, v0}, Lcom/google/android/gms/internal/measurement/al$g$a;->d(Ljava/lang/Iterable;)Lcom/google/android/gms/internal/measurement/al$g$a;

    .line 178
    invoke-virtual {v2}, Lcom/google/android/gms/internal/measurement/al$c$a;->pl()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 180
    invoke-virtual {v2}, Lcom/google/android/gms/internal/measurement/al$c$a;->getTimestampMillis()J

    move-result-wide v0

    invoke-virtual {v12, v0, v1}, Lcom/google/android/gms/internal/measurement/al$g$a;->F(J)Lcom/google/android/gms/internal/measurement/al$g$a;

    move-result-object v0

    .line 181
    invoke-virtual {v2}, Lcom/google/android/gms/internal/measurement/al$c$a;->getTimestampMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/measurement/al$g$a;->G(J)Lcom/google/android/gms/internal/measurement/al$g$a;

    .line 182
    :cond_18
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->yc()J

    move-result-wide v0

    cmp-long v2, v0, v23

    if-eqz v2, :cond_19

    .line 184
    invoke-virtual {v12, v0, v1}, Lcom/google/android/gms/internal/measurement/al$g$a;->I(J)Lcom/google/android/gms/internal/measurement/al$g$a;

    .line 185
    :cond_19
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->yb()J

    move-result-wide v2

    cmp-long v4, v2, v23

    if-eqz v4, :cond_1a

    .line 187
    invoke-virtual {v12, v2, v3}, Lcom/google/android/gms/internal/measurement/al$g$a;->H(J)Lcom/google/android/gms/internal/measurement/al$g$a;

    goto :goto_8

    :cond_1a
    cmp-long v2, v0, v23

    if-eqz v2, :cond_1b

    .line 189
    invoke-virtual {v12, v0, v1}, Lcom/google/android/gms/internal/measurement/al$g$a;->H(J)Lcom/google/android/gms/internal/measurement/al$g$a;

    .line 190
    :cond_1b
    :goto_8
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->yn()V

    .line 192
    invoke-virtual {v15}, Lcom/google/android/gms/measurement/internal/ea;->yk()J

    move-result-wide v0

    long-to-int v1, v0

    invoke-virtual {v12, v1}, Lcom/google/android/gms/internal/measurement/al$g$a;->bz(I)Lcom/google/android/gms/internal/measurement/al$g$a;

    move-result-object v0

    const-wide/16 v1, 0x3f7a

    .line 193
    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/measurement/al$g$a;->K(J)Lcom/google/android/gms/internal/measurement/al$g$a;

    move-result-object v0

    .line 194
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/common/util/e;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/measurement/al$g$a;->E(J)Lcom/google/android/gms/internal/measurement/al$g$a;

    move-result-object v0

    const/4 v1, 0x1

    .line 195
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/measurement/al$g$a;->J(Z)Lcom/google/android/gms/internal/measurement/al$g$a;

    .line 196
    invoke-virtual {v14, v12}, Lcom/google/android/gms/internal/measurement/al$f$a;->a(Lcom/google/android/gms/internal/measurement/al$g$a;)Lcom/google/android/gms/internal/measurement/al$f$a;

    .line 197
    invoke-virtual {v12}, Lcom/google/android/gms/internal/measurement/al$g$a;->pP()J

    move-result-wide v0

    invoke-virtual {v15, v0, v1}, Lcom/google/android/gms/measurement/internal/ea;->v(J)V

    .line 198
    invoke-virtual {v12}, Lcom/google/android/gms/internal/measurement/al$g$a;->pQ()J

    move-result-wide v0

    invoke-virtual {v15, v0, v1}, Lcom/google/android/gms/measurement/internal/ea;->am(J)V

    .line 199
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v0

    invoke-virtual {v0, v15}, Lcom/google/android/gms/measurement/internal/jg;->c(Lcom/google/android/gms/measurement/internal/ea;)V

    .line 200
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/jg;->setTransactionSuccessful()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 201
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/jg;->endTransaction()V

    .line 204
    :try_start_8
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xK()Lcom/google/android/gms/measurement/internal/ip;

    move-result-object v0

    invoke-virtual {v14}, Lcom/google/android/gms/internal/measurement/dr$a;->si()Lcom/google/android/gms/internal/measurement/fb;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/measurement/dr;

    check-cast v1, Lcom/google/android/gms/internal/measurement/al$f;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/measurement/bz;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/ip;->w([B)[B

    move-result-object v0
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1

    return-object v0

    :catch_1
    move-exception v0

    .line 206
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 11014
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGq:Lcom/google/android/gms/measurement/internal/dj;

    .line 208
    invoke-static/range {p2 .. p2}, Lcom/google/android/gms/measurement/internal/dh;->ci(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    const-string v3, "Data loss. Failed to bundle and serialize. appId"

    .line 209
    invoke-virtual {v1, v3, v2, v0}, Lcom/google/android/gms/measurement/internal/dj;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v1, 0x0

    return-object v1

    :catch_2
    move-exception v0

    .line 86
    :try_start_9
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 7021
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    const-string v2, "app instance id encryption failed"

    .line 86
    invoke-virtual {v0}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v1, 0x0

    new-array v0, v1, [B
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 88
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/jg;->endTransaction()V

    return-object v0

    :catchall_0
    move-exception v0

    .line 203
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/ig;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/jg;->endTransaction()V

    goto :goto_a

    :goto_9
    throw v0

    :goto_a
    goto :goto_9
.end method

.method protected final vM()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
