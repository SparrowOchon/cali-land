.class public final Lcom/google/android/gms/measurement/internal/zzn;
.super Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/google/android/gms/measurement/internal/zzn;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final aAV:J

.field public final aAW:J

.field public final aCa:Z

.field public final aFS:Ljava/lang/String;

.field public final aFU:Ljava/lang/String;

.field public final aFX:J

.field public final aFY:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final aFZ:I

.field public final aGa:Ljava/lang/String;

.field public final aHH:Ljava/lang/String;

.field public final aHL:J

.field public final aHM:J

.field public final aHN:Z

.field public final aHO:Z

.field public final aHP:Z

.field public final aHQ:Ljava/lang/Boolean;

.field public final aJe:J

.field public final aLJ:Ljava/lang/String;

.field public final aLK:Z

.field public final arz:Ljava/lang/String;

.field public final packageName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 73
    new-instance v0, Lcom/google/android/gms/measurement/internal/ix;

    invoke-direct {v0}, Lcom/google/android/gms/measurement/internal/ix;-><init>()V

    sput-object v0, Lcom/google/android/gms/measurement/internal/zzn;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;JJLjava/lang/String;ZZLjava/lang/String;JJIZZZLjava/lang/String;Ljava/lang/Boolean;JLjava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "JJ",
            "Ljava/lang/String;",
            "ZZ",
            "Ljava/lang/String;",
            "JJIZZZ",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "J",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;-><init>()V

    .line 2
    invoke-static {p1}, Lcom/google/android/gms/common/internal/r;->aQ(Ljava/lang/String;)Ljava/lang/String;

    move-object v1, p1

    .line 3
    iput-object v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->packageName:Ljava/lang/String;

    .line 4
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    move-object v1, p2

    :goto_0
    iput-object v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->arz:Ljava/lang/String;

    move-object v1, p3

    .line 5
    iput-object v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aFS:Ljava/lang/String;

    move-wide v1, p4

    .line 6
    iput-wide v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aHL:J

    move-object v1, p6

    .line 7
    iput-object v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aFU:Ljava/lang/String;

    move-wide v1, p7

    .line 8
    iput-wide v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aAV:J

    move-wide v1, p9

    .line 9
    iput-wide v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aHM:J

    move-object v1, p11

    .line 10
    iput-object v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aLJ:Ljava/lang/String;

    move v1, p12

    .line 11
    iput-boolean v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aHN:Z

    move/from16 v1, p13

    .line 12
    iput-boolean v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aCa:Z

    move-object/from16 v1, p14

    .line 13
    iput-object v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aHH:Ljava/lang/String;

    move-wide/from16 v1, p15

    .line 14
    iput-wide v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aFX:J

    move-wide/from16 v1, p17

    .line 15
    iput-wide v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aJe:J

    move/from16 v1, p19

    .line 16
    iput v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aFZ:I

    move/from16 v1, p20

    .line 17
    iput-boolean v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aHO:Z

    move/from16 v1, p21

    .line 18
    iput-boolean v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aHP:Z

    move/from16 v1, p22

    .line 19
    iput-boolean v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aLK:Z

    move-object/from16 v1, p23

    .line 20
    iput-object v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aGa:Ljava/lang/String;

    move-object/from16 v1, p24

    .line 21
    iput-object v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aHQ:Ljava/lang/Boolean;

    move-wide/from16 v1, p25

    .line 22
    iput-wide v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aAW:J

    move-object/from16 v1, p27

    .line 23
    iput-object v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aFY:Ljava/util/List;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;ZZJLjava/lang/String;JJIZZZLjava/lang/String;Ljava/lang/Boolean;JLjava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JJ",
            "Ljava/lang/String;",
            "ZZJ",
            "Ljava/lang/String;",
            "JJIZZZ",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "J",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 25
    invoke-direct {p0}, Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;-><init>()V

    move-object v1, p1

    .line 26
    iput-object v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->packageName:Ljava/lang/String;

    move-object v1, p2

    .line 27
    iput-object v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->arz:Ljava/lang/String;

    move-object v1, p3

    .line 28
    iput-object v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aFS:Ljava/lang/String;

    move-wide v1, p12

    .line 29
    iput-wide v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aHL:J

    move-object v1, p4

    .line 30
    iput-object v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aFU:Ljava/lang/String;

    move-wide v1, p5

    .line 31
    iput-wide v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aAV:J

    move-wide v1, p7

    .line 32
    iput-wide v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aHM:J

    move-object v1, p9

    .line 33
    iput-object v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aLJ:Ljava/lang/String;

    move v1, p10

    .line 34
    iput-boolean v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aHN:Z

    move v1, p11

    .line 35
    iput-boolean v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aCa:Z

    move-object/from16 v1, p14

    .line 36
    iput-object v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aHH:Ljava/lang/String;

    move-wide/from16 v1, p15

    .line 37
    iput-wide v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aFX:J

    move-wide/from16 v1, p17

    .line 38
    iput-wide v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aJe:J

    move/from16 v1, p19

    .line 39
    iput v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aFZ:I

    move/from16 v1, p20

    .line 40
    iput-boolean v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aHO:Z

    move/from16 v1, p21

    .line 41
    iput-boolean v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aHP:Z

    move/from16 v1, p22

    .line 42
    iput-boolean v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aLK:Z

    move-object/from16 v1, p23

    .line 43
    iput-object v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aGa:Ljava/lang/String;

    move-object/from16 v1, p24

    .line 44
    iput-object v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aHQ:Ljava/lang/Boolean;

    move-wide/from16 v1, p25

    .line 45
    iput-wide v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aAW:J

    move-object/from16 v1, p27

    .line 46
    iput-object v1, v0, Lcom/google/android/gms/measurement/internal/zzn;->aFY:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    const/16 p2, 0x4f45

    .line 1017
    invoke-static {p1, p2}, Lcom/google/android/gms/common/internal/safeparcel/b;->l(Landroid/os/Parcel;I)I

    move-result p2

    .line 50
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/zzn;->packageName:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-static {p1, v1, v0}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/zzn;->arz:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-static {p1, v1, v0}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/zzn;->aFS:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-static {p1, v1, v0}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/zzn;->aFU:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-static {p1, v2, v0}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    .line 54
    iget-wide v2, p0, Lcom/google/android/gms/measurement/internal/zzn;->aAV:J

    const/4 v0, 0x6

    invoke-static {p1, v0, v2, v3}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;IJ)V

    .line 55
    iget-wide v2, p0, Lcom/google/android/gms/measurement/internal/zzn;->aHM:J

    const/4 v0, 0x7

    invoke-static {p1, v0, v2, v3}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;IJ)V

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/zzn;->aLJ:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-static {p1, v2, v0}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    .line 57
    iget-boolean v0, p0, Lcom/google/android/gms/measurement/internal/zzn;->aHN:Z

    const/16 v2, 0x9

    invoke-static {p1, v2, v0}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;IZ)V

    .line 58
    iget-boolean v0, p0, Lcom/google/android/gms/measurement/internal/zzn;->aCa:Z

    const/16 v2, 0xa

    invoke-static {p1, v2, v0}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;IZ)V

    .line 59
    iget-wide v2, p0, Lcom/google/android/gms/measurement/internal/zzn;->aHL:J

    const/16 v0, 0xb

    invoke-static {p1, v0, v2, v3}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;IJ)V

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/zzn;->aHH:Ljava/lang/String;

    const/16 v2, 0xc

    invoke-static {p1, v2, v0}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    .line 61
    iget-wide v2, p0, Lcom/google/android/gms/measurement/internal/zzn;->aFX:J

    const/16 v0, 0xd

    invoke-static {p1, v0, v2, v3}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;IJ)V

    .line 62
    iget-wide v2, p0, Lcom/google/android/gms/measurement/internal/zzn;->aJe:J

    const/16 v0, 0xe

    invoke-static {p1, v0, v2, v3}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;IJ)V

    .line 63
    iget v0, p0, Lcom/google/android/gms/measurement/internal/zzn;->aFZ:I

    const/16 v2, 0xf

    invoke-static {p1, v2, v0}, Lcom/google/android/gms/common/internal/safeparcel/b;->d(Landroid/os/Parcel;II)V

    .line 64
    iget-boolean v0, p0, Lcom/google/android/gms/measurement/internal/zzn;->aHO:Z

    const/16 v2, 0x10

    invoke-static {p1, v2, v0}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;IZ)V

    .line 65
    iget-boolean v0, p0, Lcom/google/android/gms/measurement/internal/zzn;->aHP:Z

    const/16 v2, 0x11

    invoke-static {p1, v2, v0}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;IZ)V

    .line 66
    iget-boolean v0, p0, Lcom/google/android/gms/measurement/internal/zzn;->aLK:Z

    const/16 v2, 0x12

    invoke-static {p1, v2, v0}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;IZ)V

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/zzn;->aGa:Ljava/lang/String;

    const/16 v2, 0x13

    invoke-static {p1, v2, v0}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/zzn;->aHQ:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    const/16 v2, 0x15

    .line 1027
    invoke-static {p1, v2, v1}, Lcom/google/android/gms/common/internal/safeparcel/b;->c(Landroid/os/Parcel;II)V

    .line 1028
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    :cond_0
    const/16 v0, 0x16

    .line 69
    iget-wide v1, p0, Lcom/google/android/gms/measurement/internal/zzn;->aAW:J

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;IJ)V

    const/16 v0, 0x17

    .line 70
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/zzn;->aFY:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/common/internal/safeparcel/b;->a(Landroid/os/Parcel;ILjava/util/List;)V

    .line 2018
    invoke-static {p1, p2}, Lcom/google/android/gms/common/internal/safeparcel/b;->m(Landroid/os/Parcel;I)V

    return-void
.end method
