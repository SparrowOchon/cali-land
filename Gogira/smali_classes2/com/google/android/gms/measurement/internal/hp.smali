.class final Lcom/google/android/gms/measurement/internal/hp;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final synthetic aKB:Lcom/google/android/gms/measurement/internal/ho;

.field private final synthetic aKD:Lcom/google/android/gms/measurement/internal/cy;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/ho;Lcom/google/android/gms/measurement/internal/cy;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/hp;->aKB:Lcom/google/android/gms/measurement/internal/ho;

    iput-object p2, p0, Lcom/google/android/gms/measurement/internal/hp;->aKD:Lcom/google/android/gms/measurement/internal/cy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/hp;->aKB:Lcom/google/android/gms/measurement/internal/ho;

    monitor-enter v0

    .line 3
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/hp;->aKB:Lcom/google/android/gms/measurement/internal/ho;

    invoke-static {v1}, Lcom/google/android/gms/measurement/internal/ho;->a(Lcom/google/android/gms/measurement/internal/ho;)Z

    .line 4
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/hp;->aKB:Lcom/google/android/gms/measurement/internal/ho;

    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/ho;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/gw;->isConnected()Z

    move-result v1

    if-nez v1, :cond_0

    .line 5
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/hp;->aKB:Lcom/google/android/gms/measurement/internal/ho;

    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/ho;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v1

    .line 1021
    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dh;->aGx:Lcom/google/android/gms/measurement/internal/dj;

    const-string v2, "Connected to remote service"

    .line 5
    invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/dj;->ck(Ljava/lang/String;)V

    .line 6
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/hp;->aKB:Lcom/google/android/gms/measurement/internal/ho;

    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/ho;->aKm:Lcom/google/android/gms/measurement/internal/gw;

    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/hp;->aKD:Lcom/google/android/gms/measurement/internal/cy;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/gw;->a(Lcom/google/android/gms/measurement/internal/cy;)V

    .line 7
    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
