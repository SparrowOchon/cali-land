.class final Lcom/google/android/gms/measurement/internal/er;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/util/List<",
        "Lcom/google/android/gms/measurement/internal/iq;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final synthetic aJk:Lcom/google/android/gms/measurement/internal/zzn;

.field private final synthetic aJl:Lcom/google/android/gms/measurement/internal/el;

.field private final synthetic aqy:Ljava/lang/String;

.field private final synthetic aqz:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/el;Lcom/google/android/gms/measurement/internal/zzn;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/er;->aJl:Lcom/google/android/gms/measurement/internal/el;

    iput-object p2, p0, Lcom/google/android/gms/measurement/internal/er;->aJk:Lcom/google/android/gms/measurement/internal/zzn;

    iput-object p3, p0, Lcom/google/android/gms/measurement/internal/er;->aqy:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/measurement/internal/er;->aqz:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 3
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/er;->aJl:Lcom/google/android/gms/measurement/internal/el;

    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/el;->a(Lcom/google/android/gms/measurement/internal/el;)Lcom/google/android/gms/measurement/internal/ii;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ii;->za()V

    .line 4
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/er;->aJl:Lcom/google/android/gms/measurement/internal/el;

    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/el;->a(Lcom/google/android/gms/measurement/internal/el;)Lcom/google/android/gms/measurement/internal/ii;

    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ii;->xM()Lcom/google/android/gms/measurement/internal/jg;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/er;->aJk:Lcom/google/android/gms/measurement/internal/zzn;

    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/zzn;->packageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/er;->aqy:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/measurement/internal/er;->aqz:Ljava/lang/String;

    .line 6
    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/measurement/internal/jg;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
