.class public final Lcom/google/android/gms/measurement/internal/hx;
.super Lcom/google/android/gms/measurement/internal/fb;


# instance fields
.field aKM:J

.field final aKN:Lcom/google/android/gms/measurement/internal/b;

.field final aKO:Lcom/google/android/gms/measurement/internal/b;

.field acG:J

.field private handler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/ek;)V
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Lcom/google/android/gms/measurement/internal/fb;-><init>(Lcom/google/android/gms/measurement/internal/ek;)V

    .line 2
    new-instance p1, Lcom/google/android/gms/measurement/internal/ia;

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/hx;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-direct {p1, p0, v0}, Lcom/google/android/gms/measurement/internal/ia;-><init>(Lcom/google/android/gms/measurement/internal/hx;Lcom/google/android/gms/measurement/internal/fj;)V

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/hx;->aKN:Lcom/google/android/gms/measurement/internal/b;

    .line 3
    new-instance p1, Lcom/google/android/gms/measurement/internal/hz;

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/hx;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-direct {p1, p0, v0}, Lcom/google/android/gms/measurement/internal/hz;-><init>(Lcom/google/android/gms/measurement/internal/hx;Lcom/google/android/gms/measurement/internal/fj;)V

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/hx;->aKO:Lcom/google/android/gms/measurement/internal/b;

    .line 4
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object p1

    invoke-interface {p1}, Lcom/google/android/gms/common/util/e;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/measurement/internal/hx;->aKM:J

    .line 5
    iget-wide v0, p0, Lcom/google/android/gms/measurement/internal/hx;->aKM:J

    iput-wide v0, p0, Lcom/google/android/gms/measurement/internal/hx;->acG:J

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/measurement/internal/hx;)V
    .locals 3

    .line 4128
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    const/4 v0, 0x0

    .line 4129
    invoke-virtual {p0, v0, v0}, Lcom/google/android/gms/measurement/internal/hx;->c(ZZ)Z

    .line 4130
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/cb;->vw()Lcom/google/android/gms/measurement/internal/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object p0

    invoke-interface {p0}, Lcom/google/android/gms/common/util/e;->elapsedRealtime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/measurement/internal/a;->ah(J)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/measurement/internal/hx;J)V
    .locals 6

    .line 5017
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 5018
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/hx;->yM()V

    .line 5019
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/cb;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/cz;->pT()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/measurement/internal/j;->aFd:Lcom/google/android/gms/measurement/internal/cv;

    .line 5083
    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/measurement/internal/jb;->d(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 5020
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dp;->aHl:Lcom/google/android/gms/measurement/internal/dr;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/dr;->set(Z)V

    .line 5021
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    .line 6022
    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    .line 5021
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const-string v3, "Activity resumed, time"

    invoke-virtual {v0, v3, v2}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    .line 5022
    iput-wide p1, p0, Lcom/google/android/gms/measurement/internal/hx;->aKM:J

    .line 5023
    iget-wide p1, p0, Lcom/google/android/gms/measurement/internal/hx;->aKM:J

    iput-wide p1, p0, Lcom/google/android/gms/measurement/internal/hx;->acG:J

    .line 5024
    iget-object p1, p0, Lcom/google/android/gms/measurement/internal/hx;->aDi:Lcom/google/android/gms/measurement/internal/ek;

    invoke-virtual {p1}, Lcom/google/android/gms/measurement/internal/ek;->isEnabled()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 5026
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object p1

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/cb;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object p2

    invoke-virtual {p2}, Lcom/google/android/gms/measurement/internal/cz;->pT()Ljava/lang/String;

    move-result-object p2

    .line 6190
    sget-object v0, Lcom/google/android/gms/measurement/internal/j;->aFb:Lcom/google/android/gms/measurement/internal/cv;

    invoke-virtual {p1, p2, v0}, Lcom/google/android/gms/measurement/internal/jb;->d(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 5027
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object p1

    invoke-interface {p1}, Lcom/google/android/gms/common/util/e;->currentTimeMillis()J

    move-result-wide p1

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/android/gms/measurement/internal/hx;->b(JZ)V

    return-void

    .line 5029
    :cond_1
    iget-object p1, p0, Lcom/google/android/gms/measurement/internal/hx;->aKN:Lcom/google/android/gms/measurement/internal/b;

    invoke-virtual {p1}, Lcom/google/android/gms/measurement/internal/b;->cancel()V

    .line 5030
    iget-object p1, p0, Lcom/google/android/gms/measurement/internal/hx;->aKO:Lcom/google/android/gms/measurement/internal/b;

    invoke-virtual {p1}, Lcom/google/android/gms/measurement/internal/b;->cancel()V

    .line 5031
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object p1

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object p2

    invoke-interface {p2}, Lcom/google/android/gms/common/util/e;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/measurement/internal/dp;->al(J)Z

    move-result p1

    const-wide/16 v0, 0x0

    if-eqz p1, :cond_2

    .line 5032
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object p1

    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dp;->aHe:Lcom/google/android/gms/measurement/internal/dr;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/google/android/gms/measurement/internal/dr;->set(Z)V

    .line 5033
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object p1

    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dp;->aHj:Lcom/google/android/gms/measurement/internal/dt;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/measurement/internal/dt;->set(J)V

    .line 5034
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object p1

    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dp;->aHe:Lcom/google/android/gms/measurement/internal/dr;

    invoke-virtual {p1}, Lcom/google/android/gms/measurement/internal/dr;->get()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 5035
    iget-object p1, p0, Lcom/google/android/gms/measurement/internal/hx;->aKN:Lcom/google/android/gms/measurement/internal/b;

    .line 5036
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object p2

    iget-object p2, p2, Lcom/google/android/gms/measurement/internal/dp;->aHc:Lcom/google/android/gms/measurement/internal/dt;

    invoke-virtual {p2}, Lcom/google/android/gms/measurement/internal/dt;->get()J

    move-result-wide v2

    .line 5037
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object p0

    iget-object p0, p0, Lcom/google/android/gms/measurement/internal/dp;->aHj:Lcom/google/android/gms/measurement/internal/dt;

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/dt;->get()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 5038
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 5039
    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/measurement/internal/b;->aj(J)V

    return-void

    .line 5040
    :cond_3
    iget-object p1, p0, Lcom/google/android/gms/measurement/internal/hx;->aKO:Lcom/google/android/gms/measurement/internal/b;

    const-wide/32 v2, 0x36ee80

    .line 5041
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object p0

    iget-object p0, p0, Lcom/google/android/gms/measurement/internal/dp;->aHj:Lcom/google/android/gms/measurement/internal/dt;

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/dt;->get()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 5042
    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/measurement/internal/b;->aj(J)V

    :cond_4
    return-void
.end method


# virtual methods
.method final at(J)V
    .locals 9

    .line 72
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 73
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/e;->elapsedRealtime()J

    move-result-wide v0

    .line 74
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v2

    .line 1022
    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    .line 74
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const-string v1, "Session started, time"

    invoke-virtual {v2, v1, v0}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    .line 76
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/cb;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/cz;->pT()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/jb;->db(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x3e8

    .line 77
    div-long v0, p1, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 78
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/cb;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v1

    const-string v2, "auto"

    const-string v3, "_sid"

    move-object v4, v0

    move-wide v5, p1

    .line 79
    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/measurement/internal/fr;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;J)V

    .line 80
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/dp;->aHe:Lcom/google/android/gms/measurement/internal/dr;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/dr;->set(Z)V

    .line 81
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 82
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/cb;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/cz;->pT()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/jb;->db(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 83
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-string v2, "_sid"

    invoke-virtual {v8, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 84
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/cb;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v3

    const-string v4, "auto"

    const-string v5, "_s"

    move-wide v6, p1

    .line 85
    invoke-virtual/range {v3 .. v8}, Lcom/google/android/gms/measurement/internal/fr;->a(Ljava/lang/String;Ljava/lang/String;JLandroid/os/Bundle;)V

    .line 86
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dp;->aHi:Lcom/google/android/gms/measurement/internal/dt;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/measurement/internal/dt;->set(J)V

    return-void
.end method

.method final b(JZ)V
    .locals 5

    .line 44
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 45
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/hx;->yM()V

    .line 46
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/hx;->aKN:Lcom/google/android/gms/measurement/internal/b;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/b;->cancel()V

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/hx;->aKO:Lcom/google/android/gms/measurement/internal/b;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/b;->cancel()V

    .line 48
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/measurement/internal/dp;->al(J)Z

    move-result v0

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dp;->aHe:Lcom/google/android/gms/measurement/internal/dr;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/google/android/gms/measurement/internal/dr;->set(Z)V

    .line 50
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dp;->aHj:Lcom/google/android/gms/measurement/internal/dt;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/measurement/internal/dt;->set(J)V

    :cond_0
    if-eqz p3, :cond_1

    .line 52
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object p3

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/cb;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cz;->pT()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/google/android/gms/measurement/internal/jb;->dd(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_1

    .line 53
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object p3

    iget-object p3, p3, Lcom/google/android/gms/measurement/internal/dp;->aHi:Lcom/google/android/gms/measurement/internal/dt;

    invoke-virtual {p3, p1, p2}, Lcom/google/android/gms/measurement/internal/dt;->set(J)V

    .line 54
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object p3

    iget-object p3, p3, Lcom/google/android/gms/measurement/internal/dp;->aHe:Lcom/google/android/gms/measurement/internal/dr;

    invoke-virtual {p3}, Lcom/google/android/gms/measurement/internal/dr;->get()Z

    move-result p3

    if-eqz p3, :cond_2

    .line 55
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/measurement/internal/hx;->at(J)V

    return-void

    .line 56
    :cond_2
    iget-object p1, p0, Lcom/google/android/gms/measurement/internal/hx;->aKO:Lcom/google/android/gms/measurement/internal/b;

    const-wide/32 p2, 0x36ee80

    .line 57
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/dp;->aHj:Lcom/google/android/gms/measurement/internal/dt;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/dt;->get()J

    move-result-wide v3

    sub-long/2addr p2, v3

    invoke-static {v1, v2, p2, p3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide p2

    .line 58
    invoke-virtual {p1, p2, p3}, Lcom/google/android/gms/measurement/internal/b;->aj(J)V

    return-void
.end method

.method public final c(ZZ)Z
    .locals 8

    .line 95
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 96
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fb;->vt()V

    .line 97
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/e;->elapsedRealtime()J

    move-result-wide v0

    .line 98
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/dp;->aHi:Lcom/google/android/gms/measurement/internal/dt;

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/common/util/e;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/measurement/internal/dt;->set(J)V

    .line 99
    iget-wide v2, p0, Lcom/google/android/gms/measurement/internal/hx;->aKM:J

    sub-long v2, v0, v2

    if-nez p1, :cond_0

    const-wide/16 v4, 0x3e8

    cmp-long p1, v2, v4

    if-gez p1, :cond_0

    .line 101
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 2022
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    .line 103
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    const-string v0, "Screen exposed for less than 1000 ms. Event not sent. time"

    invoke-virtual {p1, v0, p2}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 p1, 0x0

    return p1

    .line 105
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object p1

    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dp;->aHj:Lcom/google/android/gms/measurement/internal/dt;

    invoke-virtual {p1, v2, v3}, Lcom/google/android/gms/measurement/internal/dt;->set(J)V

    .line 106
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object p1

    .line 3022
    iget-object p1, p1, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    .line 106
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const-string v5, "Recording user engagement, ms"

    invoke-virtual {p1, v5, v4}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    .line 107
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    const-string v4, "_et"

    .line 108
    invoke-virtual {p1, v4, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 110
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/cb;->vA()Lcom/google/android/gms/measurement/internal/gr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/gr;->yH()Lcom/google/android/gms/measurement/internal/gs;

    move-result-object v2

    const/4 v3, 0x1

    .line 111
    invoke-static {v2, p1, v3}, Lcom/google/android/gms/measurement/internal/gr;->a(Lcom/google/android/gms/measurement/internal/gs;Landroid/os/Bundle;Z)V

    .line 112
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/cb;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/measurement/internal/cz;->pT()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/gms/measurement/internal/jb;->de(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 113
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/cb;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/measurement/internal/cz;->pT()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/google/android/gms/measurement/internal/j;->aFi:Lcom/google/android/gms/measurement/internal/cv;

    .line 3083
    invoke-virtual {v2, v4, v5}, Lcom/google/android/gms/measurement/internal/jb;->d(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-nez p2, :cond_3

    .line 115
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/hx;->yN()J

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    const-wide/16 v4, 0x1

    const-string v2, "_fr"

    .line 117
    invoke-virtual {p1, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_0

    .line 118
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/hx;->yN()J

    .line 119
    :cond_3
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/cb;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/measurement/internal/cz;->pT()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/google/android/gms/measurement/internal/j;->aFi:Lcom/google/android/gms/measurement/internal/cv;

    .line 4083
    invoke-virtual {v2, v4, v5}, Lcom/google/android/gms/measurement/internal/jb;->d(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v2

    if-eqz v2, :cond_4

    if-nez p2, :cond_5

    .line 120
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/cb;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object p2

    const-string v2, "auto"

    const-string v4, "_e"

    .line 121
    invoke-virtual {p2, v2, v4, p1}, Lcom/google/android/gms/measurement/internal/fr;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 122
    :cond_5
    iput-wide v0, p0, Lcom/google/android/gms/measurement/internal/hx;->aKM:J

    .line 123
    iget-object p1, p0, Lcom/google/android/gms/measurement/internal/hx;->aKO:Lcom/google/android/gms/measurement/internal/b;

    invoke-virtual {p1}, Lcom/google/android/gms/measurement/internal/b;->cancel()V

    .line 124
    iget-object p1, p0, Lcom/google/android/gms/measurement/internal/hx;->aKO:Lcom/google/android/gms/measurement/internal/b;

    const-wide/16 v0, 0x0

    const-wide/32 v4, 0x36ee80

    .line 125
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object p2

    iget-object p2, p2, Lcom/google/android/gms/measurement/internal/dp;->aHj:Lcom/google/android/gms/measurement/internal/dt;

    invoke-virtual {p2}, Lcom/google/android/gms/measurement/internal/dt;->get()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 126
    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/measurement/internal/b;->aj(J)V

    return v3
.end method

.method public final bridge synthetic getContext()Landroid/content/Context;
    .locals 1

    .line 146
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic lX()V
    .locals 0

    .line 136
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->lX()V

    return-void
.end method

.method public final bridge synthetic vA()Lcom/google/android/gms/measurement/internal/gr;
    .locals 1

    .line 141
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vA()Lcom/google/android/gms/measurement/internal/gr;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vB()Lcom/google/android/gms/measurement/internal/dd;
    .locals 1

    .line 142
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vB()Lcom/google/android/gms/measurement/internal/dd;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vC()Lcom/google/android/gms/measurement/internal/hx;
    .locals 1

    .line 143
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vC()Lcom/google/android/gms/measurement/internal/hx;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vD()Lcom/google/android/gms/measurement/internal/d;
    .locals 1

    .line 144
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vD()Lcom/google/android/gms/measurement/internal/d;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vE()Lcom/google/android/gms/common/util/e;
    .locals 1

    .line 145
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vF()Lcom/google/android/gms/measurement/internal/df;
    .locals 1

    .line 147
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vF()Lcom/google/android/gms/measurement/internal/df;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vG()Lcom/google/android/gms/measurement/internal/it;
    .locals 1

    .line 148
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vG()Lcom/google/android/gms/measurement/internal/it;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vH()Lcom/google/android/gms/measurement/internal/ed;
    .locals 1

    .line 149
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vH()Lcom/google/android/gms/measurement/internal/ed;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vI()Lcom/google/android/gms/measurement/internal/dh;
    .locals 1

    .line 150
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vJ()Lcom/google/android/gms/measurement/internal/dp;
    .locals 1

    .line 151
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vK()Lcom/google/android/gms/measurement/internal/jb;
    .locals 1

    .line 152
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v0

    return-object v0
.end method

.method protected final vM()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final bridge synthetic vu()V
    .locals 0

    .line 133
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vu()V

    return-void
.end method

.method public final bridge synthetic vv()V
    .locals 0

    .line 135
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vv()V

    return-void
.end method

.method public final bridge synthetic vw()Lcom/google/android/gms/measurement/internal/a;
    .locals 1

    .line 137
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vw()Lcom/google/android/gms/measurement/internal/a;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vx()Lcom/google/android/gms/measurement/internal/fr;
    .locals 1

    .line 138
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vx()Lcom/google/android/gms/measurement/internal/fr;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vy()Lcom/google/android/gms/measurement/internal/cz;
    .locals 1

    .line 139
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic vz()Lcom/google/android/gms/measurement/internal/gw;
    .locals 1

    .line 140
    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/fb;->vz()Lcom/google/android/gms/measurement/internal/gw;

    move-result-object v0

    return-object v0
.end method

.method final yM()V
    .locals 2

    .line 7
    monitor-enter p0

    .line 8
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/hx;->handler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 9
    new-instance v0, Lcom/google/android/gms/internal/measurement/ft;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/measurement/ft;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/hx;->handler:Landroid/os/Handler;

    .line 10
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method final yN()J
    .locals 4

    .line 91
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/fh;->vE()Lcom/google/android/gms/common/util/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/e;->elapsedRealtime()J

    move-result-wide v0

    .line 92
    iget-wide v2, p0, Lcom/google/android/gms/measurement/internal/hx;->acG:J

    sub-long v2, v0, v2

    .line 93
    iput-wide v0, p0, Lcom/google/android/gms/measurement/internal/hx;->acG:J

    return-wide v2
.end method
