.class final Lcom/google/android/gms/measurement/internal/ic;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final synthetic aGb:J

.field private final synthetic aKP:Lcom/google/android/gms/measurement/internal/hx;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/hx;J)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/ic;->aKP:Lcom/google/android/gms/measurement/internal/hx;

    iput-wide p2, p0, Lcom/google/android/gms/measurement/internal/ic;->aGb:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .line 2
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ic;->aKP:Lcom/google/android/gms/measurement/internal/hx;

    iget-wide v1, p0, Lcom/google/android/gms/measurement/internal/ic;->aGb:J

    .line 2060
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->lX()V

    .line 2061
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/hx;->yM()V

    .line 2062
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vK()Lcom/google/android/gms/measurement/internal/jb;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/cb;->vy()Lcom/google/android/gms/measurement/internal/cz;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/measurement/internal/cz;->pT()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/google/android/gms/measurement/internal/j;->aFd:Lcom/google/android/gms/measurement/internal/cv;

    .line 2083
    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/measurement/internal/jb;->d(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/cv;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2063
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/gms/measurement/internal/dp;->aHl:Lcom/google/android/gms/measurement/internal/dr;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/gms/measurement/internal/dr;->set(Z)V

    .line 2064
    :cond_0
    iget-object v3, v0, Lcom/google/android/gms/measurement/internal/hx;->aKN:Lcom/google/android/gms/measurement/internal/b;

    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/b;->cancel()V

    .line 2065
    iget-object v3, v0, Lcom/google/android/gms/measurement/internal/hx;->aKO:Lcom/google/android/gms/measurement/internal/b;

    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/b;->cancel()V

    .line 2066
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vI()Lcom/google/android/gms/measurement/internal/dh;

    move-result-object v3

    .line 3022
    iget-object v3, v3, Lcom/google/android/gms/measurement/internal/dh;->aGy:Lcom/google/android/gms/measurement/internal/dj;

    .line 2066
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const-string v5, "Activity paused, time"

    invoke-virtual {v3, v5, v4}, Lcom/google/android/gms/measurement/internal/dj;->j(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2067
    iget-wide v3, v0, Lcom/google/android/gms/measurement/internal/hx;->aKM:J

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-eqz v7, :cond_1

    .line 2068
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/gms/measurement/internal/dp;->aHj:Lcom/google/android/gms/measurement/internal/dt;

    .line 2069
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/fh;->vJ()Lcom/google/android/gms/measurement/internal/dp;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/gms/measurement/internal/dp;->aHj:Lcom/google/android/gms/measurement/internal/dt;

    invoke-virtual {v4}, Lcom/google/android/gms/measurement/internal/dt;->get()J

    move-result-wide v4

    iget-wide v6, v0, Lcom/google/android/gms/measurement/internal/hx;->aKM:J

    sub-long/2addr v1, v6

    add-long/2addr v4, v1

    .line 2070
    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/measurement/internal/dt;->set(J)V

    :cond_1
    return-void
.end method
