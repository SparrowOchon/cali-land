.class final synthetic Lcom/google/firebase/iid/ag;
.super Ljava/lang/Object;
.source "com.google.firebase:firebase-iid@@19.0.1"

# interfaces
.implements Lcom/google/android/gms/tasks/a;


# instance fields
.field private final aUU:Lcom/google/firebase/iid/FirebaseInstanceId;

.field private final alC:Ljava/lang/String;

.field private final alO:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/firebase/iid/FirebaseInstanceId;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/firebase/iid/ag;->aUU:Lcom/google/firebase/iid/FirebaseInstanceId;

    iput-object p2, p0, Lcom/google/firebase/iid/ag;->alC:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/firebase/iid/ag;->alO:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final b(Lcom/google/android/gms/tasks/Task;)Ljava/lang/Object;
    .locals 5

    iget-object p1, p0, Lcom/google/firebase/iid/ag;->aUU:Lcom/google/firebase/iid/FirebaseInstanceId;

    iget-object v0, p0, Lcom/google/firebase/iid/ag;->alC:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/firebase/iid/ag;->alO:Ljava/lang/String;

    .line 1134
    invoke-static {}, Lcom/google/firebase/iid/FirebaseInstanceId;->AU()Ljava/lang/String;

    move-result-object v2

    .line 1135
    invoke-static {v0, v1}, Lcom/google/firebase/iid/FirebaseInstanceId;->W(Ljava/lang/String;Ljava/lang/String;)Lcom/google/firebase/iid/q;

    move-result-object v3

    .line 1136
    invoke-virtual {p1, v3}, Lcom/google/firebase/iid/FirebaseInstanceId;->a(Lcom/google/firebase/iid/q;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1137
    new-instance p1, Lcom/google/firebase/iid/at;

    iget-object v0, v3, Lcom/google/firebase/iid/q;->aVu:Ljava/lang/String;

    invoke-direct {p1, v2, v0}, Lcom/google/firebase/iid/at;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/gms/tasks/j;->aH(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1

    .line 1138
    :cond_0
    iget-object v3, p1, Lcom/google/firebase/iid/FirebaseInstanceId;->aUN:Lcom/google/firebase/iid/l;

    new-instance v4, Lcom/google/firebase/iid/ai;

    invoke-direct {v4, p1, v2, v0, v1}, Lcom/google/firebase/iid/ai;-><init>(Lcom/google/firebase/iid/FirebaseInstanceId;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1, v4}, Lcom/google/firebase/iid/l;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/firebase/iid/n;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method
