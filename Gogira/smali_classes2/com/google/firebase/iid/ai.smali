.class final synthetic Lcom/google/firebase/iid/ai;
.super Ljava/lang/Object;
.source "com.google.firebase:firebase-iid@@19.0.1"

# interfaces
.implements Lcom/google/firebase/iid/n;


# instance fields
.field private final aUU:Lcom/google/firebase/iid/FirebaseInstanceId;

.field private final aVQ:Ljava/lang/String;

.field private final alC:Ljava/lang/String;

.field private final alO:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/firebase/iid/FirebaseInstanceId;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/firebase/iid/ai;->aUU:Lcom/google/firebase/iid/FirebaseInstanceId;

    iput-object p2, p0, Lcom/google/firebase/iid/ai;->alC:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/firebase/iid/ai;->alO:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/firebase/iid/ai;->aVQ:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final Bf()Lcom/google/android/gms/tasks/Task;
    .locals 7

    iget-object v0, p0, Lcom/google/firebase/iid/ai;->aUU:Lcom/google/firebase/iid/FirebaseInstanceId;

    iget-object v1, p0, Lcom/google/firebase/iid/ai;->alC:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/firebase/iid/ai;->alO:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/firebase/iid/ai;->aVQ:Ljava/lang/String;

    .line 1139
    iget-object v4, v0, Lcom/google/firebase/iid/FirebaseInstanceId;->aUM:Lcom/google/firebase/iid/ak;

    .line 2012
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2013
    invoke-virtual {v4, v1, v2, v3, v5}, Lcom/google/firebase/iid/ak;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gms/tasks/Task;

    move-result-object v5

    .line 2014
    invoke-virtual {v4, v5}, Lcom/google/firebase/iid/ak;->g(Lcom/google/android/gms/tasks/Task;)Lcom/google/android/gms/tasks/Task;

    move-result-object v4

    .line 1140
    iget-object v5, v0, Lcom/google/firebase/iid/FirebaseInstanceId;->aPt:Ljava/util/concurrent/Executor;

    new-instance v6, Lcom/google/firebase/iid/ah;

    invoke-direct {v6, v0, v2, v3, v1}, Lcom/google/firebase/iid/ah;-><init>(Lcom/google/firebase/iid/FirebaseInstanceId;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Lcom/google/android/gms/tasks/Task;->a(Ljava/util/concurrent/Executor;Lcom/google/android/gms/tasks/g;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    return-object v0
.end method
