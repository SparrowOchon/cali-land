.class final synthetic Lcom/google/firebase/iid/am;
.super Ljava/lang/Object;
.source "com.google.firebase:firebase-iid@@19.0.1"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final aVW:Lcom/google/firebase/iid/ak;

.field private final aVX:Landroid/os/Bundle;

.field private final aVY:Lcom/google/android/gms/tasks/h;


# direct methods
.method constructor <init>(Lcom/google/firebase/iid/ak;Landroid/os/Bundle;Lcom/google/android/gms/tasks/h;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/firebase/iid/am;->aVW:Lcom/google/firebase/iid/ak;

    iput-object p2, p0, Lcom/google/firebase/iid/am;->aVX:Landroid/os/Bundle;

    iput-object p3, p0, Lcom/google/firebase/iid/am;->aVY:Lcom/google/android/gms/tasks/h;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lcom/google/firebase/iid/am;->aVW:Lcom/google/firebase/iid/ak;

    iget-object v1, p0, Lcom/google/firebase/iid/am;->aVX:Landroid/os/Bundle;

    iget-object v2, p0, Lcom/google/firebase/iid/am;->aVY:Lcom/google/android/gms/tasks/h;

    .line 1073
    :try_start_0
    iget-object v0, v0, Lcom/google/firebase/iid/ak;->aVU:Lcom/google/firebase/iid/m;

    invoke-virtual {v0, v1}, Lcom/google/firebase/iid/m;->c(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 1074
    invoke-virtual {v2, v0}, Lcom/google/android/gms/tasks/h;->k(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    .line 1077
    invoke-virtual {v2, v0}, Lcom/google/android/gms/tasks/h;->f(Ljava/lang/Exception;)V

    return-void
.end method
