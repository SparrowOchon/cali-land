.class final synthetic Lcom/google/firebase/iid/ah;
.super Ljava/lang/Object;
.source "com.google.firebase:firebase-iid@@19.0.1"

# interfaces
.implements Lcom/google/android/gms/tasks/g;


# instance fields
.field private final aUU:Lcom/google/firebase/iid/FirebaseInstanceId;

.field private final aVQ:Ljava/lang/String;

.field private final alC:Ljava/lang/String;

.field private final alO:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/firebase/iid/FirebaseInstanceId;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/firebase/iid/ah;->aUU:Lcom/google/firebase/iid/FirebaseInstanceId;

    iput-object p2, p0, Lcom/google/firebase/iid/ah;->alC:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/firebase/iid/ah;->alO:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/firebase/iid/ah;->aVQ:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final aG(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;
    .locals 8

    iget-object v0, p0, Lcom/google/firebase/iid/ah;->aUU:Lcom/google/firebase/iid/FirebaseInstanceId;

    iget-object v3, p0, Lcom/google/firebase/iid/ah;->alC:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/firebase/iid/ah;->alO:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/firebase/iid/ah;->aVQ:Ljava/lang/String;

    check-cast p1, Ljava/lang/String;

    .line 1141
    sget-object v1, Lcom/google/firebase/iid/FirebaseInstanceId;->aUI:Lcom/google/firebase/iid/s;

    iget-object v0, v0, Lcom/google/firebase/iid/FirebaseInstanceId;->aUL:Lcom/google/firebase/iid/g;

    .line 1142
    invoke-virtual {v0}, Lcom/google/firebase/iid/g;->Bb()Ljava/lang/String;

    move-result-object v6

    const-string v2, ""

    move-object v5, p1

    .line 1143
    invoke-virtual/range {v1 .. v6}, Lcom/google/firebase/iid/s;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1144
    new-instance v0, Lcom/google/firebase/iid/at;

    invoke-direct {v0, v7, p1}, Lcom/google/firebase/iid/at;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/tasks/j;->aH(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method
