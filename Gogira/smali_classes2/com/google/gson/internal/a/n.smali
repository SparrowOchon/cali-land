.class public final Lcom/google/gson/internal/a/n;
.super Ljava/lang/Object;
.source "TypeAdapters.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/gson/internal/a/n$a;
    }
.end annotation


# static fields
.field public static final aYI:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/Class;",
            ">;"
        }
    .end annotation
.end field

.field public static final aYJ:Lcom/google/gson/r;

.field public static final aYK:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/util/BitSet;",
            ">;"
        }
    .end annotation
.end field

.field public static final aYL:Lcom/google/gson/r;

.field public static final aYM:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final aYN:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final aYO:Lcom/google/gson/r;

.field public static final aYP:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final aYQ:Lcom/google/gson/r;

.field public static final aYR:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final aYS:Lcom/google/gson/r;

.field public static final aYT:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final aYU:Lcom/google/gson/r;

.field public static final aYV:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/util/concurrent/atomic/AtomicInteger;",
            ">;"
        }
    .end annotation
.end field

.field public static final aYW:Lcom/google/gson/r;

.field public static final aYX:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/util/concurrent/atomic/AtomicBoolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final aYY:Lcom/google/gson/r;

.field public static final aYZ:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/util/concurrent/atomic/AtomicIntegerArray;",
            ">;"
        }
    .end annotation
.end field

.field public static final aZA:Lcom/google/gson/r;

.field public static final aZB:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/util/Calendar;",
            ">;"
        }
    .end annotation
.end field

.field public static final aZC:Lcom/google/gson/r;

.field public static final aZD:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field public static final aZE:Lcom/google/gson/r;

.field public static final aZF:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Lcom/google/gson/JsonElement;",
            ">;"
        }
    .end annotation
.end field

.field public static final aZG:Lcom/google/gson/r;

.field public static final aZH:Lcom/google/gson/r;

.field public static final aZa:Lcom/google/gson/r;

.field public static final aZb:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final aZc:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final aZd:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final aZe:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final aZf:Lcom/google/gson/r;

.field public static final aZg:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field

.field public static final aZh:Lcom/google/gson/r;

.field public static final aZi:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final aZj:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field

.field public static final aZk:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/math/BigInteger;",
            ">;"
        }
    .end annotation
.end field

.field public static final aZl:Lcom/google/gson/r;

.field public static final aZm:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/StringBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public static final aZn:Lcom/google/gson/r;

.field public static final aZo:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/StringBuffer;",
            ">;"
        }
    .end annotation
.end field

.field public static final aZp:Lcom/google/gson/r;

.field public static final aZq:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/net/URL;",
            ">;"
        }
    .end annotation
.end field

.field public static final aZr:Lcom/google/gson/r;

.field public static final aZs:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/net/URI;",
            ">;"
        }
    .end annotation
.end field

.field public static final aZt:Lcom/google/gson/r;

.field public static final aZu:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation
.end field

.field public static final aZv:Lcom/google/gson/r;

.field public static final aZw:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/util/UUID;",
            ">;"
        }
    .end annotation
.end field

.field public static final aZx:Lcom/google/gson/r;

.field public static final aZy:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/util/Currency;",
            ">;"
        }
    .end annotation
.end field

.field public static final aZz:Lcom/google/gson/r;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 69
    new-instance v0, Lcom/google/gson/internal/a/n$1;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$1;-><init>()V

    .line 80
    invoke-virtual {v0}, Lcom/google/gson/internal/a/n$1;->nullSafe()Lcom/google/gson/TypeAdapter;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/n;->aYI:Lcom/google/gson/TypeAdapter;

    .line 82
    const-class v0, Ljava/lang/Class;

    sget-object v1, Lcom/google/gson/internal/a/n;->aYI:Lcom/google/gson/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/n;->a(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)Lcom/google/gson/r;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/n;->aYJ:Lcom/google/gson/r;

    .line 84
    new-instance v0, Lcom/google/gson/internal/a/n$12;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$12;-><init>()V

    .line 129
    invoke-virtual {v0}, Lcom/google/gson/internal/a/n$12;->nullSafe()Lcom/google/gson/TypeAdapter;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/n;->aYK:Lcom/google/gson/TypeAdapter;

    .line 131
    const-class v0, Ljava/util/BitSet;

    sget-object v1, Lcom/google/gson/internal/a/n;->aYK:Lcom/google/gson/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/n;->a(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)Lcom/google/gson/r;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/n;->aYL:Lcom/google/gson/r;

    .line 133
    new-instance v0, Lcom/google/gson/internal/a/n$23;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$23;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/n;->aYM:Lcom/google/gson/TypeAdapter;

    .line 155
    new-instance v0, Lcom/google/gson/internal/a/n$30;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$30;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/n;->aYN:Lcom/google/gson/TypeAdapter;

    .line 169
    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Boolean;

    sget-object v2, Lcom/google/gson/internal/a/n;->aYM:Lcom/google/gson/TypeAdapter;

    .line 170
    invoke-static {v0, v1, v2}, Lcom/google/gson/internal/a/n;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)Lcom/google/gson/r;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/n;->aYO:Lcom/google/gson/r;

    .line 172
    new-instance v0, Lcom/google/gson/internal/a/n$31;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$31;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/n;->aYP:Lcom/google/gson/TypeAdapter;

    .line 192
    sget-object v0, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Byte;

    sget-object v2, Lcom/google/gson/internal/a/n;->aYP:Lcom/google/gson/TypeAdapter;

    .line 193
    invoke-static {v0, v1, v2}, Lcom/google/gson/internal/a/n;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)Lcom/google/gson/r;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/n;->aYQ:Lcom/google/gson/r;

    .line 195
    new-instance v0, Lcom/google/gson/internal/a/n$32;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$32;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/n;->aYR:Lcom/google/gson/TypeAdapter;

    .line 214
    sget-object v0, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Short;

    sget-object v2, Lcom/google/gson/internal/a/n;->aYR:Lcom/google/gson/TypeAdapter;

    .line 215
    invoke-static {v0, v1, v2}, Lcom/google/gson/internal/a/n;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)Lcom/google/gson/r;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/n;->aYS:Lcom/google/gson/r;

    .line 217
    new-instance v0, Lcom/google/gson/internal/a/n$33;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$33;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/n;->aYT:Lcom/google/gson/TypeAdapter;

    .line 235
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Integer;

    sget-object v2, Lcom/google/gson/internal/a/n;->aYT:Lcom/google/gson/TypeAdapter;

    .line 236
    invoke-static {v0, v1, v2}, Lcom/google/gson/internal/a/n;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)Lcom/google/gson/r;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/n;->aYU:Lcom/google/gson/r;

    .line 238
    new-instance v0, Lcom/google/gson/internal/a/n$34;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$34;-><init>()V

    .line 249
    invoke-virtual {v0}, Lcom/google/gson/internal/a/n$34;->nullSafe()Lcom/google/gson/TypeAdapter;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/n;->aYV:Lcom/google/gson/TypeAdapter;

    .line 250
    const-class v0, Ljava/util/concurrent/atomic/AtomicInteger;

    sget-object v1, Lcom/google/gson/internal/a/n;->aYV:Lcom/google/gson/TypeAdapter;

    .line 251
    invoke-static {v0, v1}, Lcom/google/gson/internal/a/n;->a(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)Lcom/google/gson/r;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/n;->aYW:Lcom/google/gson/r;

    .line 253
    new-instance v0, Lcom/google/gson/internal/a/n$35;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$35;-><init>()V

    .line 260
    invoke-virtual {v0}, Lcom/google/gson/internal/a/n$35;->nullSafe()Lcom/google/gson/TypeAdapter;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/n;->aYX:Lcom/google/gson/TypeAdapter;

    .line 261
    const-class v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    sget-object v1, Lcom/google/gson/internal/a/n;->aYX:Lcom/google/gson/TypeAdapter;

    .line 262
    invoke-static {v0, v1}, Lcom/google/gson/internal/a/n;->a(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)Lcom/google/gson/r;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/n;->aYY:Lcom/google/gson/r;

    .line 264
    new-instance v0, Lcom/google/gson/internal/a/n$2;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$2;-><init>()V

    .line 291
    invoke-virtual {v0}, Lcom/google/gson/internal/a/n$2;->nullSafe()Lcom/google/gson/TypeAdapter;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/n;->aYZ:Lcom/google/gson/TypeAdapter;

    .line 292
    const-class v0, Ljava/util/concurrent/atomic/AtomicIntegerArray;

    sget-object v1, Lcom/google/gson/internal/a/n;->aYZ:Lcom/google/gson/TypeAdapter;

    .line 293
    invoke-static {v0, v1}, Lcom/google/gson/internal/a/n;->a(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)Lcom/google/gson/r;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/n;->aZa:Lcom/google/gson/r;

    .line 295
    new-instance v0, Lcom/google/gson/internal/a/n$3;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$3;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/n;->aZb:Lcom/google/gson/TypeAdapter;

    .line 314
    new-instance v0, Lcom/google/gson/internal/a/n$4;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$4;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/n;->aZc:Lcom/google/gson/TypeAdapter;

    .line 329
    new-instance v0, Lcom/google/gson/internal/a/n$5;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$5;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/n;->aZd:Lcom/google/gson/TypeAdapter;

    .line 344
    new-instance v0, Lcom/google/gson/internal/a/n$6;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$6;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/n;->aZe:Lcom/google/gson/TypeAdapter;

    .line 365
    const-class v0, Ljava/lang/Number;

    sget-object v1, Lcom/google/gson/internal/a/n;->aZe:Lcom/google/gson/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/n;->a(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)Lcom/google/gson/r;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/n;->aZf:Lcom/google/gson/r;

    .line 367
    new-instance v0, Lcom/google/gson/internal/a/n$7;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$7;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/n;->aZg:Lcom/google/gson/TypeAdapter;

    .line 386
    sget-object v0, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Character;

    sget-object v2, Lcom/google/gson/internal/a/n;->aZg:Lcom/google/gson/TypeAdapter;

    .line 387
    invoke-static {v0, v1, v2}, Lcom/google/gson/internal/a/n;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)Lcom/google/gson/r;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/n;->aZh:Lcom/google/gson/r;

    .line 389
    new-instance v0, Lcom/google/gson/internal/a/n$8;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$8;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/n;->aZi:Lcom/google/gson/TypeAdapter;

    .line 409
    new-instance v0, Lcom/google/gson/internal/a/n$9;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$9;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/n;->aZj:Lcom/google/gson/TypeAdapter;

    .line 427
    new-instance v0, Lcom/google/gson/internal/a/n$10;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$10;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/n;->aZk:Lcom/google/gson/TypeAdapter;

    .line 445
    const-class v0, Ljava/lang/String;

    sget-object v1, Lcom/google/gson/internal/a/n;->aZi:Lcom/google/gson/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/n;->a(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)Lcom/google/gson/r;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/n;->aZl:Lcom/google/gson/r;

    .line 447
    new-instance v0, Lcom/google/gson/internal/a/n$11;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$11;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/n;->aZm:Lcom/google/gson/TypeAdapter;

    .line 462
    const-class v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/google/gson/internal/a/n;->aZm:Lcom/google/gson/TypeAdapter;

    .line 463
    invoke-static {v0, v1}, Lcom/google/gson/internal/a/n;->a(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)Lcom/google/gson/r;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/n;->aZn:Lcom/google/gson/r;

    .line 465
    new-instance v0, Lcom/google/gson/internal/a/n$13;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$13;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/n;->aZo:Lcom/google/gson/TypeAdapter;

    .line 480
    const-class v0, Ljava/lang/StringBuffer;

    sget-object v1, Lcom/google/gson/internal/a/n;->aZo:Lcom/google/gson/TypeAdapter;

    .line 481
    invoke-static {v0, v1}, Lcom/google/gson/internal/a/n;->a(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)Lcom/google/gson/r;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/n;->aZp:Lcom/google/gson/r;

    .line 483
    new-instance v0, Lcom/google/gson/internal/a/n$14;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$14;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/n;->aZq:Lcom/google/gson/TypeAdapter;

    .line 499
    const-class v0, Ljava/net/URL;

    sget-object v1, Lcom/google/gson/internal/a/n;->aZq:Lcom/google/gson/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/n;->a(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)Lcom/google/gson/r;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/n;->aZr:Lcom/google/gson/r;

    .line 501
    new-instance v0, Lcom/google/gson/internal/a/n$15;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$15;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/n;->aZs:Lcom/google/gson/TypeAdapter;

    .line 521
    const-class v0, Ljava/net/URI;

    sget-object v1, Lcom/google/gson/internal/a/n;->aZs:Lcom/google/gson/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/n;->a(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)Lcom/google/gson/r;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/n;->aZt:Lcom/google/gson/r;

    .line 523
    new-instance v0, Lcom/google/gson/internal/a/n$16;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$16;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/n;->aZu:Lcom/google/gson/TypeAdapter;

    .line 539
    const-class v0, Ljava/net/InetAddress;

    sget-object v1, Lcom/google/gson/internal/a/n;->aZu:Lcom/google/gson/TypeAdapter;

    .line 540
    invoke-static {v0, v1}, Lcom/google/gson/internal/a/n;->b(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)Lcom/google/gson/r;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/n;->aZv:Lcom/google/gson/r;

    .line 542
    new-instance v0, Lcom/google/gson/internal/a/n$17;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$17;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/n;->aZw:Lcom/google/gson/TypeAdapter;

    .line 557
    const-class v0, Ljava/util/UUID;

    sget-object v1, Lcom/google/gson/internal/a/n;->aZw:Lcom/google/gson/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/n;->a(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)Lcom/google/gson/r;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/n;->aZx:Lcom/google/gson/r;

    .line 559
    new-instance v0, Lcom/google/gson/internal/a/n$18;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$18;-><init>()V

    .line 568
    invoke-virtual {v0}, Lcom/google/gson/internal/a/n$18;->nullSafe()Lcom/google/gson/TypeAdapter;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/n;->aZy:Lcom/google/gson/TypeAdapter;

    .line 569
    const-class v0, Ljava/util/Currency;

    sget-object v1, Lcom/google/gson/internal/a/n;->aZy:Lcom/google/gson/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/n;->a(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)Lcom/google/gson/r;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/n;->aZz:Lcom/google/gson/r;

    .line 571
    new-instance v0, Lcom/google/gson/internal/a/n$19;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$19;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/n;->aZA:Lcom/google/gson/r;

    .line 592
    new-instance v0, Lcom/google/gson/internal/a/n$20;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$20;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/n;->aZB:Lcom/google/gson/TypeAdapter;

    .line 657
    const-class v0, Ljava/util/Calendar;

    const-class v1, Ljava/util/GregorianCalendar;

    sget-object v2, Lcom/google/gson/internal/a/n;->aZB:Lcom/google/gson/TypeAdapter;

    .line 1861
    new-instance v3, Lcom/google/gson/internal/a/n$27;

    invoke-direct {v3, v0, v1, v2}, Lcom/google/gson/internal/a/n$27;-><init>(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    .line 658
    sput-object v3, Lcom/google/gson/internal/a/n;->aZC:Lcom/google/gson/r;

    .line 660
    new-instance v0, Lcom/google/gson/internal/a/n$21;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$21;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/n;->aZD:Lcom/google/gson/TypeAdapter;

    .line 695
    const-class v0, Ljava/util/Locale;

    sget-object v1, Lcom/google/gson/internal/a/n;->aZD:Lcom/google/gson/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/n;->a(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)Lcom/google/gson/r;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/n;->aZE:Lcom/google/gson/r;

    .line 697
    new-instance v0, Lcom/google/gson/internal/a/n$22;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$22;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/n;->aZF:Lcom/google/gson/TypeAdapter;

    .line 769
    const-class v0, Lcom/google/gson/JsonElement;

    sget-object v1, Lcom/google/gson/internal/a/n;->aZF:Lcom/google/gson/TypeAdapter;

    .line 770
    invoke-static {v0, v1}, Lcom/google/gson/internal/a/n;->b(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)Lcom/google/gson/r;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/n;->aZG:Lcom/google/gson/r;

    .line 807
    new-instance v0, Lcom/google/gson/internal/a/n$24;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n$24;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/n;->aZH:Lcom/google/gson/r;

    return-void
.end method

.method public static a(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)Lcom/google/gson/r;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TTT;>;",
            "Lcom/google/gson/TypeAdapter<",
            "TTT;>;)",
            "Lcom/google/gson/r;"
        }
    .end annotation

    .line 833
    new-instance v0, Lcom/google/gson/internal/a/n$25;

    invoke-direct {v0, p0, p1}, Lcom/google/gson/internal/a/n$25;-><init>(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)Lcom/google/gson/r;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TTT;>;",
            "Ljava/lang/Class<",
            "TTT;>;",
            "Lcom/google/gson/TypeAdapter<",
            "-TTT;>;)",
            "Lcom/google/gson/r;"
        }
    .end annotation

    .line 846
    new-instance v0, Lcom/google/gson/internal/a/n$26;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/gson/internal/a/n$26;-><init>(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    return-object v0
.end method

.method private static b(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)Lcom/google/gson/r;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT1;>;",
            "Lcom/google/gson/TypeAdapter<",
            "TT1;>;)",
            "Lcom/google/gson/r;"
        }
    .end annotation

    .line 880
    new-instance v0, Lcom/google/gson/internal/a/n$28;

    invoke-direct {v0, p0, p1}, Lcom/google/gson/internal/a/n$28;-><init>(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    return-object v0
.end method
