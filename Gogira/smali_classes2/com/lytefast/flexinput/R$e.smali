.class public final Lcom/lytefast/flexinput/R$e;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lytefast/flexinput/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "e"
.end annotation


# static fields
.field public static final action0:I = 0x7f0a002d

.field public static final action_bar:I = 0x7f0a002e

.field public static final action_bar_activity_content:I = 0x7f0a002f

.field public static final action_bar_container:I = 0x7f0a0030

.field public static final action_bar_root:I = 0x7f0a0031

.field public static final action_bar_spinner:I = 0x7f0a0032

.field public static final action_bar_subtitle:I = 0x7f0a0033

.field public static final action_bar_title:I = 0x7f0a0035

.field public static final action_btn:I = 0x7f0a0038

.field public static final action_container:I = 0x7f0a0039

.field public static final action_context_bar:I = 0x7f0a003a

.field public static final action_divider:I = 0x7f0a003b

.field public static final action_image:I = 0x7f0a003c

.field public static final action_menu_divider:I = 0x7f0a003d

.field public static final action_menu_presenter:I = 0x7f0a003e

.field public static final action_mode_bar:I = 0x7f0a003f

.field public static final action_mode_bar_stub:I = 0x7f0a0040

.field public static final action_mode_close_button:I = 0x7f0a0041

.field public static final action_text:I = 0x7f0a0042

.field public static final actions:I = 0x7f0a0043

.field public static final activity_chooser_view_content:I = 0x7f0a0051

.field public static final add:I = 0x7f0a0052

.field public static final add_btn:I = 0x7f0a0053

.field public static final add_content_keyboard_btn:I = 0x7f0a0056

.field public static final alertTitle:I = 0x7f0a0061

.field public static final async:I = 0x7f0a0080

.field public static final attachment_clear_btn:I = 0x7f0a0081

.field public static final attachment_preview_container:I = 0x7f0a0082

.field public static final attachment_preview_list:I = 0x7f0a0083

.field public static final auto:I = 0x7f0a00b8

.field public static final back:I = 0x7f0a00ba

.field public static final blocking:I = 0x7f0a00cd

.field public static final bottom:I = 0x7f0a0105

.field public static final buttonPanel:I = 0x7f0a010c

.field public static final camera_container:I = 0x7f0a0112

.field public static final camera_facing_btn:I = 0x7f0a0113

.field public static final camera_flash_btn:I = 0x7f0a0114

.field public static final camera_view:I = 0x7f0a0115

.field public static final camera_view_cropper:I = 0x7f0a0116

.field public static final cancel_action:I = 0x7f0a0117

.field public static final center:I = 0x7f0a0123

.field public static final centerCrop:I = 0x7f0a0124

.field public static final centerInside:I = 0x7f0a0125

.field public static final checkbox:I = 0x7f0a0212

.field public static final chronometer:I = 0x7f0a0216

.field public static final container:I = 0x7f0a0231

.field public static final content:I = 0x7f0a0232

.field public static final contentPanel:I = 0x7f0a0233

.field public static final content_iv:I = 0x7f0a0234

.field public static final content_pager:I = 0x7f0a0235

.field public static final content_tabs:I = 0x7f0a0236

.field public static final coordinator:I = 0x7f0a0237

.field public static final custom:I = 0x7f0a0247

.field public static final customPanel:I = 0x7f0a0248

.field public static final decor_content_parent:I = 0x7f0a024c

.field public static final default_activity_button:I = 0x7f0a024d

.field public static final design_bottom_sheet:I = 0x7f0a024e

.field public static final design_menu_item_action_area:I = 0x7f0a024f

.field public static final design_menu_item_action_area_stub:I = 0x7f0a0250

.field public static final design_menu_item_text:I = 0x7f0a0251

.field public static final design_navigation_view:I = 0x7f0a0252

.field public static final edit_query:I = 0x7f0a02a3

.field public static final emoji_btn:I = 0x7f0a02a9

.field public static final emoji_container:I = 0x7f0a02aa

.field public static final end:I = 0x7f0a02be

.field public static final end_padder:I = 0x7f0a02bf

.field public static final expand_activities_button:I = 0x7f0a02c4

.field public static final expanded_menu:I = 0x7f0a02c5

.field public static final file_name_tv:I = 0x7f0a02d7

.field public static final file_subtitle_tv:I = 0x7f0a02d8

.field public static final fill:I = 0x7f0a02d9

.field public static final filled:I = 0x7f0a02dc

.field public static final fitBottomStart:I = 0x7f0a02dd

.field public static final fitCenter:I = 0x7f0a02de

.field public static final fitEnd:I = 0x7f0a02df

.field public static final fitStart:I = 0x7f0a02e0

.field public static final fitXY:I = 0x7f0a02e1

.field public static final fixed:I = 0x7f0a02e2

.field public static final focusCrop:I = 0x7f0a02e5

.field public static final forever:I = 0x7f0a02e7

.field public static final front:I = 0x7f0a030e

.field public static final ghost_view:I = 0x7f0a030f

.field public static final gone:I = 0x7f0a0321

.field public static final group_divider:I = 0x7f0a0322

.field public static final home:I = 0x7f0a0389

.field public static final icon:I = 0x7f0a038b

.field public static final icon_group:I = 0x7f0a038c

.field public static final image:I = 0x7f0a038f

.field public static final info:I = 0x7f0a03a1

.field public static final invisible:I = 0x7f0a03af

.field public static final italic:I = 0x7f0a03ce

.field public static final item_check_indicator:I = 0x7f0a03d0

.field public static final item_touch_helper_previous_elevation:I = 0x7f0a0401

.field public static final labeled:I = 0x7f0a0408

.field public static final largeLabel:I = 0x7f0a0409

.field public static final launch_btn:I = 0x7f0a040a

.field public static final launch_camera_btn:I = 0x7f0a040b

.field public static final left:I = 0x7f0a040f

.field public static final line1:I = 0x7f0a0411

.field public static final line3:I = 0x7f0a0412

.field public static final list:I = 0x7f0a0413

.field public static final listMode:I = 0x7f0a0414

.field public static final list_item:I = 0x7f0a0415

.field public static final main_input_container:I = 0x7f0a041a

.field public static final masked:I = 0x7f0a0432

.field public static final media_actions:I = 0x7f0a0433

.field public static final message:I = 0x7f0a0477

.field public static final mini:I = 0x7f0a047c

.field public static final mtrl_child_content_container:I = 0x7f0a047d

.field public static final mtrl_internal_children_alpha_tag:I = 0x7f0a047e

.field public static final multiply:I = 0x7f0a047f

.field public static final navigation_header_container:I = 0x7f0a0481

.field public static final none:I = 0x7f0a0494

.field public static final normal:I = 0x7f0a0495

.field public static final notification_background:I = 0x7f0a049f

.field public static final notification_main_column:I = 0x7f0a04a0

.field public static final notification_main_column_container:I = 0x7f0a04a1

.field public static final off:I = 0x7f0a04ba

.field public static final on:I = 0x7f0a04bc

.field public static final outline:I = 0x7f0a04bd

.field public static final packed:I = 0x7f0a04ca

.field public static final page_tabs:I = 0x7f0a04cb

.field public static final parallax:I = 0x7f0a04cc

.field public static final parent:I = 0x7f0a04cd

.field public static final parentPanel:I = 0x7f0a04ce

.field public static final parent_matrix:I = 0x7f0a04cf

.field public static final percent:I = 0x7f0a0500

.field public static final permissions_container:I = 0x7f0a0509

.field public static final permissions_req_btn:I = 0x7f0a050a

.field public static final pin:I = 0x7f0a050b

.field public static final progress_circular:I = 0x7f0a054e

.field public static final progress_horizontal:I = 0x7f0a0550

.field public static final radio:I = 0x7f0a055b

.field public static final redEye:I = 0x7f0a0561

.field public static final right:I = 0x7f0a0575

.field public static final right_icon:I = 0x7f0a0576

.field public static final right_side:I = 0x7f0a0577

.field public static final save_image_matrix:I = 0x7f0a05a1

.field public static final save_non_transition_alpha:I = 0x7f0a05a2

.field public static final save_scale_type:I = 0x7f0a05a3

.field public static final screen:I = 0x7f0a05a5

.field public static final scrollIndicatorDown:I = 0x7f0a05a7

.field public static final scrollIndicatorUp:I = 0x7f0a05a8

.field public static final scrollView:I = 0x7f0a05a9

.field public static final scrollable:I = 0x7f0a05ab

.field public static final search_badge:I = 0x7f0a05ad

.field public static final search_bar:I = 0x7f0a05ae

.field public static final search_button:I = 0x7f0a05af

.field public static final search_close_btn:I = 0x7f0a05b1

.field public static final search_edit_frame:I = 0x7f0a05b2

.field public static final search_go_btn:I = 0x7f0a05b5

.field public static final search_mag_icon:I = 0x7f0a05b7

.field public static final search_plate:I = 0x7f0a05b8

.field public static final search_src_text:I = 0x7f0a05bb

.field public static final search_voice_btn:I = 0x7f0a05c7

.field public static final select_dialog_listview:I = 0x7f0a05c8

.field public static final selected:I = 0x7f0a05c9

.field public static final selection_indicator:I = 0x7f0a05cb

.field public static final send_btn:I = 0x7f0a05cc

.field public static final shortcut:I = 0x7f0a0702

.field public static final smallLabel:I = 0x7f0a0706

.field public static final snackbar_action:I = 0x7f0a0707

.field public static final snackbar_text:I = 0x7f0a0708

.field public static final spacer:I = 0x7f0a070e

.field public static final split_action_bar:I = 0x7f0a070f

.field public static final spread:I = 0x7f0a0710

.field public static final spread_inside:I = 0x7f0a0711

.field public static final src_atop:I = 0x7f0a0712

.field public static final src_in:I = 0x7f0a0713

.field public static final src_over:I = 0x7f0a0714

.field public static final start:I = 0x7f0a0717

.field public static final status_bar_latest_event_content:I = 0x7f0a071b

.field public static final stretch:I = 0x7f0a072e

.field public static final submenuarrow:I = 0x7f0a072f

.field public static final submit_area:I = 0x7f0a0730

.field public static final surface_view:I = 0x7f0a0739

.field public static final swipeRefreshLayout:I = 0x7f0a073a

.field public static final tabMode:I = 0x7f0a073c

.field public static final tag_transition_group:I = 0x7f0a0742

.field public static final tag_unhandled_key_event_manager:I = 0x7f0a0743

.field public static final tag_unhandled_key_listeners:I = 0x7f0a0744

.field public static final take_photo_btn:I = 0x7f0a0745

.field public static final text:I = 0x7f0a074a

.field public static final text2:I = 0x7f0a074b

.field public static final textSpacerNoButtons:I = 0x7f0a074c

.field public static final textSpacerNoTitle:I = 0x7f0a074d

.field public static final text_input:I = 0x7f0a074f

.field public static final text_input_password_toggle:I = 0x7f0a0750

.field public static final textinput_counter:I = 0x7f0a0753

.field public static final textinput_error:I = 0x7f0a0754

.field public static final textinput_helper_text:I = 0x7f0a0755

.field public static final texture_view:I = 0x7f0a0756

.field public static final thumb_iv:I = 0x7f0a0757

.field public static final time:I = 0x7f0a0759

.field public static final title:I = 0x7f0a075a

.field public static final titleDividerNoCustom:I = 0x7f0a075b

.field public static final title_template:I = 0x7f0a075c

.field public static final top:I = 0x7f0a0761

.field public static final topPanel:I = 0x7f0a0762

.field public static final torch:I = 0x7f0a0763

.field public static final touch_outside:I = 0x7f0a0764

.field public static final transition_current_scene:I = 0x7f0a0765

.field public static final transition_layout_save:I = 0x7f0a0766

.field public static final transition_position:I = 0x7f0a0767

.field public static final transition_scene_layoutid_cache:I = 0x7f0a0768

.field public static final transition_transform:I = 0x7f0a0769

.field public static final type_iv:I = 0x7f0a076d

.field public static final uniform:I = 0x7f0a0775

.field public static final unlabeled:I = 0x7f0a0776

.field public static final up:I = 0x7f0a0778

.field public static final view_offset_helper:I = 0x7f0a07e9

.field public static final view_pager:I = 0x7f0a07eb

.field public static final visible:I = 0x7f0a07ec

.field public static final wrap:I = 0x7f0a083b

.field public static final wrap_content:I = 0x7f0a083c


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
