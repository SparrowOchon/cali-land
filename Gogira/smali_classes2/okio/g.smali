.class public final Lokio/g;
.super Lokio/s;
.source "ForwardingTimeout.java"


# instance fields
.field public bBI:Lokio/s;


# direct methods
.method public constructor <init>(Lokio/s;)V
    .locals 1

    .line 25
    invoke-direct {p0}, Lokio/s;-><init>()V

    if-eqz p1, :cond_0

    .line 27
    iput-object p1, p0, Lokio/g;->bBI:Lokio/s;

    return-void

    .line 26
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "delegate == null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public final IA()Lokio/s;
    .locals 1

    .line 66
    iget-object v0, p0, Lokio/g;->bBI:Lokio/s;

    invoke-virtual {v0}, Lokio/s;->IA()Lokio/s;

    move-result-object v0

    return-object v0
.end method

.method public final IB()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 70
    iget-object v0, p0, Lokio/g;->bBI:Lokio/s;

    invoke-virtual {v0}, Lokio/s;->IB()V

    return-void
.end method

.method public final Iw()J
    .locals 2

    .line 46
    iget-object v0, p0, Lokio/g;->bBI:Lokio/s;

    invoke-virtual {v0}, Lokio/s;->Iw()J

    move-result-wide v0

    return-wide v0
.end method

.method public final Ix()Z
    .locals 1

    .line 50
    iget-object v0, p0, Lokio/g;->bBI:Lokio/s;

    invoke-virtual {v0}, Lokio/s;->Ix()Z

    move-result v0

    return v0
.end method

.method public final Iy()J
    .locals 2

    .line 54
    iget-object v0, p0, Lokio/g;->bBI:Lokio/s;

    invoke-virtual {v0}, Lokio/s;->Iy()J

    move-result-wide v0

    return-wide v0
.end method

.method public final Iz()Lokio/s;
    .locals 1

    .line 62
    iget-object v0, p0, Lokio/g;->bBI:Lokio/s;

    invoke-virtual {v0}, Lokio/s;->Iz()Lokio/s;

    move-result-object v0

    return-object v0
.end method

.method public final aT(J)Lokio/s;
    .locals 1

    .line 58
    iget-object v0, p0, Lokio/g;->bBI:Lokio/s;

    invoke-virtual {v0, p1, p2}, Lokio/s;->aT(J)Lokio/s;

    move-result-object p1

    return-object p1
.end method

.method public final d(JLjava/util/concurrent/TimeUnit;)Lokio/s;
    .locals 1

    .line 42
    iget-object v0, p0, Lokio/g;->bBI:Lokio/s;

    invoke-virtual {v0, p1, p2, p3}, Lokio/s;->d(JLjava/util/concurrent/TimeUnit;)Lokio/s;

    move-result-object p1

    return-object p1
.end method
