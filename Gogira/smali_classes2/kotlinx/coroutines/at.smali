.class public abstract Lkotlinx/coroutines/at;
.super Lkotlinx/coroutines/y;
.source "EventLoop.common.kt"


# instance fields
.field private bnk:J

.field private bnl:Z

.field private bnm:Lkotlinx/coroutines/internal/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/internal/b<",
            "Lkotlinx/coroutines/ao<",
            "*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Lkotlinx/coroutines/y;-><init>()V

    return-void
.end method

.method private static X(Z)J
    .locals 2

    if-eqz p0, :cond_0

    const-wide v0, 0x100000000L

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x1

    return-wide v0
.end method


# virtual methods
.method public EM()J
    .locals 2

    .line 50
    invoke-virtual {p0}, Lkotlinx/coroutines/at;->EO()Z

    move-result v0

    if-nez v0, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    return-wide v0

    .line 51
    :cond_0
    invoke-virtual {p0}, Lkotlinx/coroutines/at;->EN()J

    move-result-wide v0

    return-wide v0
.end method

.method protected EN()J
    .locals 3

    .line 58
    iget-object v0, p0, Lkotlinx/coroutines/at;->bnm:Lkotlinx/coroutines/internal/b;

    const-wide v1, 0x7fffffffffffffffL

    if-nez v0, :cond_0

    return-wide v1

    .line 59
    :cond_0
    invoke-virtual {v0}, Lkotlinx/coroutines/internal/b;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    return-wide v1

    :cond_1
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final EO()Z
    .locals 2

    .line 63
    iget-object v0, p0, Lkotlinx/coroutines/at;->bnm:Lkotlinx/coroutines/internal/b;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 64
    :cond_0
    invoke-virtual {v0}, Lkotlinx/coroutines/internal/b;->Fm()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlinx/coroutines/ao;

    if-nez v0, :cond_1

    return v1

    .line 65
    :cond_1
    invoke-virtual {v0}, Lkotlinx/coroutines/ao;->run()V

    const/4 v0, 0x1

    return v0
.end method

.method public final EP()Z
    .locals 6

    .line 90
    iget-wide v0, p0, Lkotlinx/coroutines/at;->bnk:J

    const/4 v2, 0x1

    invoke-static {v2}, Lkotlinx/coroutines/at;->X(Z)J

    move-result-wide v3

    cmp-long v5, v0, v3

    if-ltz v5, :cond_0

    return v2

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final EQ()Z
    .locals 1

    .line 94
    iget-object v0, p0, Lkotlinx/coroutines/at;->bnm:Lkotlinx/coroutines/internal/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lkotlinx/coroutines/internal/b;->isEmpty()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public final ER()V
    .locals 6

    .line 105
    iget-wide v0, p0, Lkotlinx/coroutines/at;->bnk:J

    const/4 v2, 0x1

    invoke-static {v2}, Lkotlinx/coroutines/at;->X(Z)J

    move-result-wide v3

    sub-long/2addr v0, v3

    iput-wide v0, p0, Lkotlinx/coroutines/at;->bnk:J

    .line 106
    iget-wide v0, p0, Lkotlinx/coroutines/at;->bnk:J

    const-wide/16 v3, 0x0

    cmp-long v5, v0, v3

    if-lez v5, :cond_0

    return-void

    :cond_0
    cmp-long v5, v0, v3

    if-nez v5, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_3

    .line 108
    iget-boolean v0, p0, Lkotlinx/coroutines/at;->bnl:Z

    if-eqz v0, :cond_2

    .line 110
    invoke-virtual {p0}, Lkotlinx/coroutines/at;->shutdown()V

    :cond_2
    return-void

    .line 107
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Extra decrementUseCount"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final Y(Z)V
    .locals 4

    .line 100
    iget-wide v0, p0, Lkotlinx/coroutines/at;->bnk:J

    invoke-static {p1}, Lkotlinx/coroutines/at;->X(Z)J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lkotlinx/coroutines/at;->bnk:J

    if-nez p1, :cond_0

    const/4 p1, 0x1

    .line 101
    iput-boolean p1, p0, Lkotlinx/coroutines/at;->bnl:Z

    :cond_0
    return-void
.end method

.method public final a(Lkotlinx/coroutines/ao;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlinx/coroutines/ao<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "task"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lkotlinx/coroutines/at;->bnm:Lkotlinx/coroutines/internal/b;

    if-nez v0, :cond_0

    .line 82
    new-instance v0, Lkotlinx/coroutines/internal/b;

    invoke-direct {v0}, Lkotlinx/coroutines/internal/b;-><init>()V

    iput-object v0, p0, Lkotlinx/coroutines/at;->bnm:Lkotlinx/coroutines/internal/b;

    .line 83
    :cond_0
    invoke-virtual {v0, p1}, Lkotlinx/coroutines/internal/b;->addLast(Ljava/lang/Object;)V

    return-void
.end method

.method protected isEmpty()Z
    .locals 1

    .line 54
    invoke-virtual {p0}, Lkotlinx/coroutines/at;->EQ()Z

    move-result v0

    return v0
.end method

.method protected shutdown()V
    .locals 0

    return-void
.end method
