.class public final Lkotlinx/coroutines/m;
.super Lkotlinx/coroutines/bf;
.source "JobSupport.kt"

# interfaces
.implements Lkotlinx/coroutines/l;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlinx/coroutines/bf<",
        "Lkotlinx/coroutines/bj;",
        ">;",
        "Lkotlinx/coroutines/l;"
    }
.end annotation


# instance fields
.field public final bmz:Lkotlinx/coroutines/n;


# direct methods
.method public constructor <init>(Lkotlinx/coroutines/bj;Lkotlinx/coroutines/n;)V
    .locals 1

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "childJob"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1372
    check-cast p1, Lkotlinx/coroutines/Job;

    invoke-direct {p0, p1}, Lkotlinx/coroutines/bf;-><init>(Lkotlinx/coroutines/Job;)V

    iput-object p2, p0, Lkotlinx/coroutines/m;->bmz:Lkotlinx/coroutines/n;

    return-void
.end method


# virtual methods
.method public final bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1369
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lkotlinx/coroutines/m;->invoke(Ljava/lang/Throwable;)V

    sget-object p1, Lkotlin/Unit;->bjS:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/Throwable;)V
    .locals 1

    .line 1373
    iget-object p1, p0, Lkotlinx/coroutines/m;->bmz:Lkotlinx/coroutines/n;

    iget-object v0, p0, Lkotlinx/coroutines/m;->job:Lkotlinx/coroutines/Job;

    check-cast v0, Lkotlinx/coroutines/bq;

    invoke-interface {p1, v0}, Lkotlinx/coroutines/n;->a(Lkotlinx/coroutines/bq;)V

    return-void
.end method

.method public final s(Ljava/lang/Throwable;)Z
    .locals 1

    const-string v0, "cause"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1374
    iget-object v0, p0, Lkotlinx/coroutines/m;->job:Lkotlinx/coroutines/Job;

    check-cast v0, Lkotlinx/coroutines/bj;

    invoke-virtual {v0, p1}, Lkotlinx/coroutines/bj;->s(Ljava/lang/Throwable;)Z

    move-result p1

    return p1
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .line 1375
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ChildHandle["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lkotlinx/coroutines/m;->bmz:Lkotlinx/coroutines/n;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
