.class final Lkotlinx/coroutines/bj$a;
.super Lkotlinx/coroutines/bi;
.source "JobSupport.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlinx/coroutines/bj;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlinx/coroutines/bi<",
        "Lkotlinx/coroutines/Job;",
        ">;"
    }
.end annotation


# instance fields
.field private final bnB:Lkotlinx/coroutines/bj;

.field private final bnC:Lkotlinx/coroutines/bj$b;

.field private final bnD:Lkotlinx/coroutines/m;

.field private final bnE:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lkotlinx/coroutines/bj;Lkotlinx/coroutines/bj$b;Lkotlinx/coroutines/m;Ljava/lang/Object;)V
    .locals 1

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "child"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1075
    iget-object v0, p3, Lkotlinx/coroutines/m;->bmz:Lkotlinx/coroutines/n;

    check-cast v0, Lkotlinx/coroutines/Job;

    invoke-direct {p0, v0}, Lkotlinx/coroutines/bi;-><init>(Lkotlinx/coroutines/Job;)V

    iput-object p1, p0, Lkotlinx/coroutines/bj$a;->bnB:Lkotlinx/coroutines/bj;

    iput-object p2, p0, Lkotlinx/coroutines/bj$a;->bnC:Lkotlinx/coroutines/bj$b;

    iput-object p3, p0, Lkotlinx/coroutines/bj$a;->bnD:Lkotlinx/coroutines/m;

    iput-object p4, p0, Lkotlinx/coroutines/bj$a;->bnE:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1070
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lkotlinx/coroutines/bj$a;->invoke(Ljava/lang/Throwable;)V

    sget-object p1, Lkotlin/Unit;->bjS:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/Throwable;)V
    .locals 3

    .line 1077
    iget-object p1, p0, Lkotlinx/coroutines/bj$a;->bnB:Lkotlinx/coroutines/bj;

    iget-object v0, p0, Lkotlinx/coroutines/bj$a;->bnC:Lkotlinx/coroutines/bj$b;

    iget-object v1, p0, Lkotlinx/coroutines/bj$a;->bnD:Lkotlinx/coroutines/m;

    iget-object v2, p0, Lkotlinx/coroutines/bj$a;->bnE:Ljava/lang/Object;

    invoke-static {p1, v0, v1, v2}, Lkotlinx/coroutines/bj;->a(Lkotlinx/coroutines/bj;Lkotlinx/coroutines/bj$b;Lkotlinx/coroutines/m;Ljava/lang/Object;)V

    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .line 1080
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ChildCompletion["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lkotlinx/coroutines/bj$a;->bnD:Lkotlinx/coroutines/m;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lkotlinx/coroutines/bj$a;->bnE:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
