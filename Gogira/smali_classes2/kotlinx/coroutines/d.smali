.class public final Lkotlinx/coroutines/d;
.super Ljava/lang/Object;


# direct methods
.method public static synthetic a(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;I)Lkotlinx/coroutines/Job;
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-static {p0, p1, v0, p2, p3}, Lkotlinx/coroutines/e;->a(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/ad;Lkotlin/jvm/functions/Function2;I)Lkotlinx/coroutines/Job;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/ad;Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/Job;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlinx/coroutines/CoroutineScope;",
            "Lkotlin/coroutines/CoroutineContext;",
            "Lkotlinx/coroutines/ad;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lkotlinx/coroutines/CoroutineScope;",
            "-",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;+",
            "Ljava/lang/Object;",
            ">;)",
            "Lkotlinx/coroutines/Job;"
        }
    .end annotation

    const-string v0, "$this$launch"

    .line 1
    invoke-static {p0, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "start"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1050
    invoke-static {p0, p1}, Lkotlinx/coroutines/x;->a(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;)Lkotlin/coroutines/CoroutineContext;

    move-result-object p0

    .line 1121
    move-object p1, p2

    check-cast p1, Lkotlinx/coroutines/ad;

    sget-object v0, Lkotlinx/coroutines/ad;->bmP:Lkotlinx/coroutines/ad;

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    .line 1052
    new-instance p1, Lkotlinx/coroutines/bl;

    invoke-direct {p1, p0, p3}, Lkotlinx/coroutines/bl;-><init>(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;)V

    check-cast p1, Lkotlinx/coroutines/bs;

    goto :goto_1

    .line 1053
    :cond_1
    new-instance p1, Lkotlinx/coroutines/bs;

    invoke-direct {p1, p0, v1}, Lkotlinx/coroutines/bs;-><init>(Lkotlin/coroutines/CoroutineContext;Z)V

    .line 1054
    :goto_1
    invoke-virtual {p1, p2, p1, p3}, Lkotlinx/coroutines/bs;->a(Lkotlinx/coroutines/ad;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)V

    .line 1055
    check-cast p1, Lkotlinx/coroutines/Job;

    return-object p1
.end method
