.class public final enum Lkotlinx/coroutines/ad;
.super Ljava/lang/Enum;
.source "CoroutineStart.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lkotlinx/coroutines/ad;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum bmO:Lkotlinx/coroutines/ad;

.field public static final enum bmP:Lkotlinx/coroutines/ad;

.field public static final enum bmQ:Lkotlinx/coroutines/ad;

.field public static final enum bmR:Lkotlinx/coroutines/ad;

.field private static final synthetic bmS:[Lkotlinx/coroutines/ad;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [Lkotlinx/coroutines/ad;

    new-instance v1, Lkotlinx/coroutines/ad;

    const/4 v2, 0x0

    const-string v3, "DEFAULT"

    invoke-direct {v1, v3, v2}, Lkotlinx/coroutines/ad;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlinx/coroutines/ad;->bmO:Lkotlinx/coroutines/ad;

    aput-object v1, v0, v2

    new-instance v1, Lkotlinx/coroutines/ad;

    const/4 v2, 0x1

    const-string v3, "LAZY"

    invoke-direct {v1, v3, v2}, Lkotlinx/coroutines/ad;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlinx/coroutines/ad;->bmP:Lkotlinx/coroutines/ad;

    aput-object v1, v0, v2

    new-instance v1, Lkotlinx/coroutines/ad;

    const/4 v2, 0x2

    const-string v3, "ATOMIC"

    invoke-direct {v1, v3, v2}, Lkotlinx/coroutines/ad;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlinx/coroutines/ad;->bmQ:Lkotlinx/coroutines/ad;

    aput-object v1, v0, v2

    new-instance v1, Lkotlinx/coroutines/ad;

    const/4 v2, 0x3

    const-string v3, "UNDISPATCHED"

    invoke-direct {v1, v3, v2}, Lkotlinx/coroutines/ad;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlinx/coroutines/ad;->bmR:Lkotlinx/coroutines/ad;

    aput-object v1, v0, v2

    sput-object v0, Lkotlinx/coroutines/ad;->bmS:[Lkotlinx/coroutines/ad;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlinx/coroutines/ad;
    .locals 1

    const-class v0, Lkotlinx/coroutines/ad;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lkotlinx/coroutines/ad;

    return-object p0
.end method

.method public static values()[Lkotlinx/coroutines/ad;
    .locals 1

    sget-object v0, Lkotlinx/coroutines/ad;->bmS:[Lkotlinx/coroutines/ad;

    invoke-virtual {v0}, [Lkotlinx/coroutines/ad;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlinx/coroutines/ad;

    return-object v0
.end method
