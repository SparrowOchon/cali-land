.class public final Lkotlinx/coroutines/bk;
.super Ljava/lang/Object;
.source "JobSupport.kt"


# static fields
.field private static final bnI:Lkotlinx/coroutines/internal/v;

.field private static final bnJ:Lkotlinx/coroutines/as;

.field private static final bnK:Lkotlinx/coroutines/as;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1221
    new-instance v0, Lkotlinx/coroutines/internal/v;

    const-string v1, "SEALED"

    invoke-direct {v0, v1}, Lkotlinx/coroutines/internal/v;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlinx/coroutines/bk;->bnI:Lkotlinx/coroutines/internal/v;

    .line 1223
    new-instance v0, Lkotlinx/coroutines/as;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlinx/coroutines/as;-><init>(Z)V

    sput-object v0, Lkotlinx/coroutines/bk;->bnJ:Lkotlinx/coroutines/as;

    .line 1225
    new-instance v0, Lkotlinx/coroutines/as;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lkotlinx/coroutines/as;-><init>(Z)V

    sput-object v0, Lkotlinx/coroutines/bk;->bnK:Lkotlinx/coroutines/as;

    return-void
.end method

.method public static final synthetic Fg()Lkotlinx/coroutines/internal/v;
    .locals 1

    .line 1
    sget-object v0, Lkotlinx/coroutines/bk;->bnI:Lkotlinx/coroutines/internal/v;

    return-object v0
.end method

.method public static final synthetic Fh()Lkotlinx/coroutines/as;
    .locals 1

    .line 1
    sget-object v0, Lkotlinx/coroutines/bk;->bnK:Lkotlinx/coroutines/as;

    return-object v0
.end method

.method public static final synthetic Fi()Lkotlinx/coroutines/as;
    .locals 1

    .line 1
    sget-object v0, Lkotlinx/coroutines/bk;->bnJ:Lkotlinx/coroutines/as;

    return-object v0
.end method

.method public static final bx(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .line 1207
    instance-of v0, p0, Lkotlinx/coroutines/ba;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    move-object v0, p0

    :goto_0
    check-cast v0, Lkotlinx/coroutines/ba;

    if-eqz v0, :cond_2

    iget-object v0, v0, Lkotlinx/coroutines/ba;->bnw:Lkotlinx/coroutines/az;

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    move-object p0, v0

    :cond_2
    :goto_1
    return-object p0
.end method

.method public static final synthetic by(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .line 2206
    instance-of v0, p0, Lkotlinx/coroutines/az;

    if-eqz v0, :cond_0

    new-instance v0, Lkotlinx/coroutines/ba;

    check-cast p0, Lkotlinx/coroutines/az;

    invoke-direct {v0, p0}, Lkotlinx/coroutines/ba;-><init>(Lkotlinx/coroutines/az;)V

    return-object v0

    :cond_0
    return-object p0
.end method
