.class public Lkotlin/a/am;
.super Lkotlin/a/al;
.source "Sets.kt"


# direct methods
.method public static final e(Ljava/util/Set;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Set<",
            "+TT;>;)",
            "Ljava/util/Set<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$optimizeReadOnlySet"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    invoke-interface {p0}, Ljava/util/Set;->size()I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    return-object p0

    .line 93
    :cond_0
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    invoke-static {p0}, Lkotlin/a/ak;->bb(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p0

    return-object p0

    .line 2034
    :cond_1
    sget-object p0, Lkotlin/a/aa;->bkj:Lkotlin/a/aa;

    check-cast p0, Ljava/util/Set;

    return-object p0
.end method

.method public static final varargs o([Ljava/lang/Object;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)",
            "Ljava/util/Set<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "elements"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    array-length v0, p0

    if-lez v0, :cond_0

    invoke-static {p0}, Lkotlin/a/g;->h([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p0

    return-object p0

    .line 1034
    :cond_0
    sget-object p0, Lkotlin/a/aa;->bkj:Lkotlin/a/aa;

    check-cast p0, Ljava/util/Set;

    return-object p0
.end method
