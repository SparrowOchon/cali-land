.class public final Lkotlin/jvm/internal/w;
.super Ljava/lang/Object;
.source "Reflection.java"


# static fields
.field private static final bkS:Lkotlin/jvm/internal/x;

.field private static final bkT:[Lkotlin/reflect/b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "kotlin.reflect.jvm.internal.ReflectionFactoryImpl"

    .line 26
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 27
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/jvm/internal/x;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    nop

    :goto_0
    if-eqz v0, :cond_0

    goto :goto_1

    .line 34
    :cond_0
    new-instance v0, Lkotlin/jvm/internal/x;

    invoke-direct {v0}, Lkotlin/jvm/internal/x;-><init>()V

    :goto_1
    sput-object v0, Lkotlin/jvm/internal/w;->bkS:Lkotlin/jvm/internal/x;

    const/4 v0, 0x0

    new-array v0, v0, [Lkotlin/reflect/b;

    .line 39
    sput-object v0, Lkotlin/jvm/internal/w;->bkT:[Lkotlin/reflect/b;

    return-void
.end method

.method public static Q(Ljava/lang/Class;)Lkotlin/reflect/b;
    .locals 1

    .line 1029
    new-instance v0, Lkotlin/jvm/internal/e;

    invoke-direct {v0, p0}, Lkotlin/jvm/internal/e;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method public static a(Lkotlin/jvm/internal/i;)Ljava/lang/String;
    .locals 0

    .line 78
    invoke-static {p0}, Lkotlin/jvm/internal/x;->a(Lkotlin/jvm/internal/i;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static a(Lkotlin/jvm/internal/l;)Ljava/lang/String;
    .locals 0

    .line 1038
    invoke-static {p0}, Lkotlin/jvm/internal/x;->a(Lkotlin/jvm/internal/i;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static a(Lkotlin/jvm/internal/j;)Lkotlin/reflect/KFunction;
    .locals 0

    return-object p0
.end method

.method public static a(Lkotlin/jvm/internal/n;)Lkotlin/reflect/KMutableProperty0;
    .locals 0

    return-object p0
.end method

.method public static a(Lkotlin/jvm/internal/s;)Lkotlin/reflect/KProperty0;
    .locals 0

    return-object p0
.end method

.method public static a(Lkotlin/jvm/internal/o;)Lkotlin/reflect/c;
    .locals 0

    return-object p0
.end method

.method public static a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;
    .locals 0

    return-object p0
.end method

.method public static f(Ljava/lang/Class;Ljava/lang/String;)Lkotlin/reflect/KDeclarationContainer;
    .locals 1

    .line 1025
    new-instance v0, Lkotlin/jvm/internal/q;

    invoke-direct {v0, p0, p1}, Lkotlin/jvm/internal/q;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    return-object v0
.end method
