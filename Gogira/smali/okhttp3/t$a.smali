.class public final Lokhttp3/t$a;
.super Ljava/lang/Object;
.source "OkHttpClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lokhttp3/t;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field bqE:Lokhttp3/internal/i/c;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field bqe:Lokhttp3/o;

.field bqf:Ljavax/net/SocketFactory;

.field bqg:Lokhttp3/b;

.field bqh:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lokhttp3/u;",
            ">;"
        }
    .end annotation
.end field

.field bqi:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lokhttp3/k;",
            ">;"
        }
    .end annotation
.end field

.field bqj:Ljava/net/Proxy;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field bqk:Lokhttp3/g;

.field bqm:Lokhttp3/internal/a/e;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field buA:Lokhttp3/n;

.field final buB:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lokhttp3/Interceptor;",
            ">;"
        }
    .end annotation
.end field

.field final buC:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lokhttp3/Interceptor;",
            ">;"
        }
    .end annotation
.end field

.field buD:Lokhttp3/p$a;

.field buE:Lokhttp3/c;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field buF:Lokhttp3/b;

.field buG:Lokhttp3/j;

.field buH:Z

.field public buI:Z

.field buJ:Z

.field buK:I

.field buL:I

.field buM:I

.field buN:I

.field buO:I

.field cookieJar:Lokhttp3/m;

.field hostnameVerifier:Ljavax/net/ssl/HostnameVerifier;

.field proxySelector:Ljava/net/ProxySelector;

.field sslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 473
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 448
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lokhttp3/t$a;->buB:Ljava/util/List;

    .line 449
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lokhttp3/t$a;->buC:Ljava/util/List;

    .line 474
    new-instance v0, Lokhttp3/n;

    invoke-direct {v0}, Lokhttp3/n;-><init>()V

    iput-object v0, p0, Lokhttp3/t$a;->buA:Lokhttp3/n;

    .line 475
    sget-object v0, Lokhttp3/t;->buy:Ljava/util/List;

    iput-object v0, p0, Lokhttp3/t$a;->bqh:Ljava/util/List;

    .line 476
    sget-object v0, Lokhttp3/t;->buz:Ljava/util/List;

    iput-object v0, p0, Lokhttp3/t$a;->bqi:Ljava/util/List;

    .line 477
    sget-object v0, Lokhttp3/p;->btH:Lokhttp3/p;

    invoke-static {v0}, Lokhttp3/p;->a(Lokhttp3/p;)Lokhttp3/p$a;

    move-result-object v0

    iput-object v0, p0, Lokhttp3/t$a;->buD:Lokhttp3/p$a;

    .line 478
    invoke-static {}, Ljava/net/ProxySelector;->getDefault()Ljava/net/ProxySelector;

    move-result-object v0

    iput-object v0, p0, Lokhttp3/t$a;->proxySelector:Ljava/net/ProxySelector;

    .line 479
    iget-object v0, p0, Lokhttp3/t$a;->proxySelector:Ljava/net/ProxySelector;

    if-nez v0, :cond_0

    .line 480
    new-instance v0, Lokhttp3/internal/h/a;

    invoke-direct {v0}, Lokhttp3/internal/h/a;-><init>()V

    iput-object v0, p0, Lokhttp3/t$a;->proxySelector:Ljava/net/ProxySelector;

    .line 482
    :cond_0
    sget-object v0, Lokhttp3/m;->btz:Lokhttp3/m;

    iput-object v0, p0, Lokhttp3/t$a;->cookieJar:Lokhttp3/m;

    .line 483
    invoke-static {}, Ljavax/net/SocketFactory;->getDefault()Ljavax/net/SocketFactory;

    move-result-object v0

    iput-object v0, p0, Lokhttp3/t$a;->bqf:Ljavax/net/SocketFactory;

    .line 484
    sget-object v0, Lokhttp3/internal/i/d;->bAr:Lokhttp3/internal/i/d;

    iput-object v0, p0, Lokhttp3/t$a;->hostnameVerifier:Ljavax/net/ssl/HostnameVerifier;

    .line 485
    sget-object v0, Lokhttp3/g;->bqC:Lokhttp3/g;

    iput-object v0, p0, Lokhttp3/t$a;->bqk:Lokhttp3/g;

    .line 486
    sget-object v0, Lokhttp3/b;->bql:Lokhttp3/b;

    iput-object v0, p0, Lokhttp3/t$a;->bqg:Lokhttp3/b;

    .line 487
    sget-object v0, Lokhttp3/b;->bql:Lokhttp3/b;

    iput-object v0, p0, Lokhttp3/t$a;->buF:Lokhttp3/b;

    .line 488
    new-instance v0, Lokhttp3/j;

    invoke-direct {v0}, Lokhttp3/j;-><init>()V

    iput-object v0, p0, Lokhttp3/t$a;->buG:Lokhttp3/j;

    .line 489
    sget-object v0, Lokhttp3/o;->btG:Lokhttp3/o;

    iput-object v0, p0, Lokhttp3/t$a;->bqe:Lokhttp3/o;

    const/4 v0, 0x1

    .line 490
    iput-boolean v0, p0, Lokhttp3/t$a;->buH:Z

    .line 491
    iput-boolean v0, p0, Lokhttp3/t$a;->buI:Z

    .line 492
    iput-boolean v0, p0, Lokhttp3/t$a;->buJ:Z

    const/4 v0, 0x0

    .line 493
    iput v0, p0, Lokhttp3/t$a;->buK:I

    const/16 v1, 0x2710

    .line 494
    iput v1, p0, Lokhttp3/t$a;->buL:I

    .line 495
    iput v1, p0, Lokhttp3/t$a;->buM:I

    .line 496
    iput v1, p0, Lokhttp3/t$a;->buN:I

    .line 497
    iput v0, p0, Lokhttp3/t$a;->buO:I

    return-void
.end method

.method constructor <init>(Lokhttp3/t;)V
    .locals 2

    .line 500
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 448
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lokhttp3/t$a;->buB:Ljava/util/List;

    .line 449
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lokhttp3/t$a;->buC:Ljava/util/List;

    .line 501
    iget-object v0, p1, Lokhttp3/t;->buA:Lokhttp3/n;

    iput-object v0, p0, Lokhttp3/t$a;->buA:Lokhttp3/n;

    .line 502
    iget-object v0, p1, Lokhttp3/t;->bqj:Ljava/net/Proxy;

    iput-object v0, p0, Lokhttp3/t$a;->bqj:Ljava/net/Proxy;

    .line 503
    iget-object v0, p1, Lokhttp3/t;->bqh:Ljava/util/List;

    iput-object v0, p0, Lokhttp3/t$a;->bqh:Ljava/util/List;

    .line 504
    iget-object v0, p1, Lokhttp3/t;->bqi:Ljava/util/List;

    iput-object v0, p0, Lokhttp3/t$a;->bqi:Ljava/util/List;

    .line 505
    iget-object v0, p0, Lokhttp3/t$a;->buB:Ljava/util/List;

    iget-object v1, p1, Lokhttp3/t;->buB:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 506
    iget-object v0, p0, Lokhttp3/t$a;->buC:Ljava/util/List;

    iget-object v1, p1, Lokhttp3/t;->buC:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 507
    iget-object v0, p1, Lokhttp3/t;->buD:Lokhttp3/p$a;

    iput-object v0, p0, Lokhttp3/t$a;->buD:Lokhttp3/p$a;

    .line 508
    iget-object v0, p1, Lokhttp3/t;->proxySelector:Ljava/net/ProxySelector;

    iput-object v0, p0, Lokhttp3/t$a;->proxySelector:Ljava/net/ProxySelector;

    .line 509
    iget-object v0, p1, Lokhttp3/t;->cookieJar:Lokhttp3/m;

    iput-object v0, p0, Lokhttp3/t$a;->cookieJar:Lokhttp3/m;

    .line 510
    iget-object v0, p1, Lokhttp3/t;->bqm:Lokhttp3/internal/a/e;

    iput-object v0, p0, Lokhttp3/t$a;->bqm:Lokhttp3/internal/a/e;

    .line 511
    iget-object v0, p1, Lokhttp3/t;->buE:Lokhttp3/c;

    iput-object v0, p0, Lokhttp3/t$a;->buE:Lokhttp3/c;

    .line 512
    iget-object v0, p1, Lokhttp3/t;->bqf:Ljavax/net/SocketFactory;

    iput-object v0, p0, Lokhttp3/t$a;->bqf:Ljavax/net/SocketFactory;

    .line 513
    iget-object v0, p1, Lokhttp3/t;->sslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

    iput-object v0, p0, Lokhttp3/t$a;->sslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

    .line 514
    iget-object v0, p1, Lokhttp3/t;->bqE:Lokhttp3/internal/i/c;

    iput-object v0, p0, Lokhttp3/t$a;->bqE:Lokhttp3/internal/i/c;

    .line 515
    iget-object v0, p1, Lokhttp3/t;->hostnameVerifier:Ljavax/net/ssl/HostnameVerifier;

    iput-object v0, p0, Lokhttp3/t$a;->hostnameVerifier:Ljavax/net/ssl/HostnameVerifier;

    .line 516
    iget-object v0, p1, Lokhttp3/t;->bqk:Lokhttp3/g;

    iput-object v0, p0, Lokhttp3/t$a;->bqk:Lokhttp3/g;

    .line 517
    iget-object v0, p1, Lokhttp3/t;->bqg:Lokhttp3/b;

    iput-object v0, p0, Lokhttp3/t$a;->bqg:Lokhttp3/b;

    .line 518
    iget-object v0, p1, Lokhttp3/t;->buF:Lokhttp3/b;

    iput-object v0, p0, Lokhttp3/t$a;->buF:Lokhttp3/b;

    .line 519
    iget-object v0, p1, Lokhttp3/t;->buG:Lokhttp3/j;

    iput-object v0, p0, Lokhttp3/t$a;->buG:Lokhttp3/j;

    .line 520
    iget-object v0, p1, Lokhttp3/t;->bqe:Lokhttp3/o;

    iput-object v0, p0, Lokhttp3/t$a;->bqe:Lokhttp3/o;

    .line 521
    iget-boolean v0, p1, Lokhttp3/t;->buH:Z

    iput-boolean v0, p0, Lokhttp3/t$a;->buH:Z

    .line 522
    iget-boolean v0, p1, Lokhttp3/t;->buI:Z

    iput-boolean v0, p0, Lokhttp3/t$a;->buI:Z

    .line 523
    iget-boolean v0, p1, Lokhttp3/t;->buJ:Z

    iput-boolean v0, p0, Lokhttp3/t$a;->buJ:Z

    .line 524
    iget v0, p1, Lokhttp3/t;->buK:I

    iput v0, p0, Lokhttp3/t$a;->buK:I

    .line 525
    iget v0, p1, Lokhttp3/t;->buL:I

    iput v0, p0, Lokhttp3/t$a;->buL:I

    .line 526
    iget v0, p1, Lokhttp3/t;->buM:I

    iput v0, p0, Lokhttp3/t$a;->buM:I

    .line 527
    iget v0, p1, Lokhttp3/t;->buN:I

    iput v0, p0, Lokhttp3/t$a;->buN:I

    .line 528
    iget p1, p1, Lokhttp3/t;->buO:I

    iput p1, p0, Lokhttp3/t$a;->buO:I

    return-void
.end method


# virtual methods
.method public final GD()Lokhttp3/t;
    .locals 1

    .line 1040
    new-instance v0, Lokhttp3/t;

    invoke-direct {v0, p0}, Lokhttp3/t;-><init>(Lokhttp3/t$a;)V

    return-object v0
.end method

.method public final a(JLjava/util/concurrent/TimeUnit;)Lokhttp3/t$a;
    .locals 1

    const-string v0, "timeout"

    .line 567
    invoke-static {v0, p1, p2, p3}, Lokhttp3/internal/c;->a(Ljava/lang/String;JLjava/util/concurrent/TimeUnit;)I

    move-result p1

    iput p1, p0, Lokhttp3/t$a;->buL:I

    return-object p0
.end method

.method public final a(Ljavax/net/ssl/SSLSocketFactory;)Lokhttp3/t$a;
    .locals 1

    if-eqz p1, :cond_0

    .line 767
    iput-object p1, p0, Lokhttp3/t$a;->sslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

    .line 768
    invoke-static {}, Lokhttp3/internal/g/f;->HP()Lokhttp3/internal/g/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lokhttp3/internal/g/f;->d(Ljavax/net/ssl/SSLSocketFactory;)Lokhttp3/internal/i/c;

    move-result-object p1

    iput-object p1, p0, Lokhttp3/t$a;->bqE:Lokhttp3/internal/i/c;

    return-object p0

    .line 766
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "sslSocketFactory == null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a(Lokhttp3/Interceptor;)Lokhttp3/t$a;
    .locals 1

    if-eqz p1, :cond_0

    .line 994
    iget-object v0, p0, Lokhttp3/t$a;->buB:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0

    .line 993
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "interceptor == null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a(Lokhttp3/m;)Lokhttp3/t$a;
    .locals 1

    if-eqz p1, :cond_0

    .line 714
    iput-object p1, p0, Lokhttp3/t$a;->cookieJar:Lokhttp3/m;

    return-object p0

    .line 713
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "cookieJar == null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final b(JLjava/util/concurrent/TimeUnit;)Lokhttp3/t$a;
    .locals 1

    const-string v0, "timeout"

    .line 596
    invoke-static {v0, p1, p2, p3}, Lokhttp3/internal/c;->a(Ljava/lang/String;JLjava/util/concurrent/TimeUnit;)I

    move-result p1

    iput p1, p0, Lokhttp3/t$a;->buM:I

    return-object p0
.end method

.method public final c(JLjava/util/concurrent/TimeUnit;)Lokhttp3/t$a;
    .locals 1

    const-string v0, "timeout"

    .line 626
    invoke-static {v0, p1, p2, p3}, Lokhttp3/internal/c;->a(Ljava/lang/String;JLjava/util/concurrent/TimeUnit;)I

    move-result p1

    iput p1, p0, Lokhttp3/t$a;->buN:I

    return-object p0
.end method
