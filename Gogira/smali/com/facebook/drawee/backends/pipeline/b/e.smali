.class public final Lcom/facebook/drawee/backends/pipeline/b/e;
.super Ljava/lang/Object;
.source "ImagePerfData.java"


# instance fields
.field private final Jc:I

.field private final Je:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final Jg:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final Jh:Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final Ji:Lcom/facebook/imagepipeline/request/b;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final Jj:Lcom/facebook/imagepipeline/f/f;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final Jk:J

.field private final Jl:J

.field private final Jm:J

.field private final Jn:J

.field private final Jo:J

.field private final Jp:J

.field private final Jq:J

.field private final Jr:Z

.field private final Js:I

.field private final Jt:I

.field private final Ju:I

.field private final Jv:J

.field private final Jw:J

.field private final Jx:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/imagepipeline/request/b;Ljava/lang/Object;Lcom/facebook/imagepipeline/f/f;JJJJJJJIZIIIJJLjava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/imagepipeline/request/b;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/imagepipeline/f/f;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p29    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    move-object v0, p0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 66
    iput-object v1, v0, Lcom/facebook/drawee/backends/pipeline/b/e;->Je:Ljava/lang/String;

    move-object v1, p2

    .line 67
    iput-object v1, v0, Lcom/facebook/drawee/backends/pipeline/b/e;->Jg:Ljava/lang/String;

    move-object v1, p3

    .line 68
    iput-object v1, v0, Lcom/facebook/drawee/backends/pipeline/b/e;->Ji:Lcom/facebook/imagepipeline/request/b;

    move-object v1, p4

    .line 69
    iput-object v1, v0, Lcom/facebook/drawee/backends/pipeline/b/e;->Jh:Ljava/lang/Object;

    move-object v1, p5

    .line 70
    iput-object v1, v0, Lcom/facebook/drawee/backends/pipeline/b/e;->Jj:Lcom/facebook/imagepipeline/f/f;

    move-wide v1, p6

    .line 71
    iput-wide v1, v0, Lcom/facebook/drawee/backends/pipeline/b/e;->Jk:J

    move-wide v1, p8

    .line 72
    iput-wide v1, v0, Lcom/facebook/drawee/backends/pipeline/b/e;->Jl:J

    move-wide v1, p10

    .line 73
    iput-wide v1, v0, Lcom/facebook/drawee/backends/pipeline/b/e;->Jm:J

    move-wide v1, p12

    .line 74
    iput-wide v1, v0, Lcom/facebook/drawee/backends/pipeline/b/e;->Jn:J

    move-wide/from16 v1, p14

    .line 75
    iput-wide v1, v0, Lcom/facebook/drawee/backends/pipeline/b/e;->Jo:J

    move-wide/from16 v1, p16

    .line 76
    iput-wide v1, v0, Lcom/facebook/drawee/backends/pipeline/b/e;->Jp:J

    move-wide/from16 v1, p18

    .line 77
    iput-wide v1, v0, Lcom/facebook/drawee/backends/pipeline/b/e;->Jq:J

    move/from16 v1, p20

    .line 78
    iput v1, v0, Lcom/facebook/drawee/backends/pipeline/b/e;->Jc:I

    move/from16 v1, p21

    .line 79
    iput-boolean v1, v0, Lcom/facebook/drawee/backends/pipeline/b/e;->Jr:Z

    move/from16 v1, p22

    .line 80
    iput v1, v0, Lcom/facebook/drawee/backends/pipeline/b/e;->Js:I

    move/from16 v1, p23

    .line 81
    iput v1, v0, Lcom/facebook/drawee/backends/pipeline/b/e;->Jt:I

    move/from16 v1, p24

    .line 82
    iput v1, v0, Lcom/facebook/drawee/backends/pipeline/b/e;->Ju:I

    move-wide/from16 v1, p25

    .line 83
    iput-wide v1, v0, Lcom/facebook/drawee/backends/pipeline/b/e;->Jv:J

    move-wide/from16 v1, p27

    .line 84
    iput-wide v1, v0, Lcom/facebook/drawee/backends/pipeline/b/e;->Jw:J

    move-object/from16 v1, p29

    .line 85
    iput-object v1, v0, Lcom/facebook/drawee/backends/pipeline/b/e;->Jx:Ljava/lang/String;

    return-void
.end method
