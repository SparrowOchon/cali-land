.class public final Lcom/facebook/drawee/backends/pipeline/b/h;
.super Ljava/lang/Object;
.source "ImagePerfState.java"


# instance fields
.field public JG:I

.field public Jc:I

.field public Je:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Jg:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Jh:Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Ji:Lcom/facebook/imagepipeline/request/b;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Jj:Lcom/facebook/imagepipeline/f/f;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Jk:J

.field public Jl:J

.field public Jm:J

.field public Jn:J

.field public Jo:J

.field public Jp:J

.field public Jq:J

.field public Jr:Z

.field public Js:I

.field public Jt:I

.field public Ju:I

.field public Jv:J

.field public Jw:J

.field public Jx:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    .line 25
    iput-wide v0, p0, Lcom/facebook/drawee/backends/pipeline/b/h;->Jk:J

    .line 26
    iput-wide v0, p0, Lcom/facebook/drawee/backends/pipeline/b/h;->Jl:J

    .line 27
    iput-wide v0, p0, Lcom/facebook/drawee/backends/pipeline/b/h;->Jm:J

    .line 28
    iput-wide v0, p0, Lcom/facebook/drawee/backends/pipeline/b/h;->Jn:J

    .line 29
    iput-wide v0, p0, Lcom/facebook/drawee/backends/pipeline/b/h;->Jo:J

    .line 32
    iput-wide v0, p0, Lcom/facebook/drawee/backends/pipeline/b/h;->Jp:J

    .line 33
    iput-wide v0, p0, Lcom/facebook/drawee/backends/pipeline/b/h;->Jq:J

    const/4 v2, -0x1

    .line 36
    iput v2, p0, Lcom/facebook/drawee/backends/pipeline/b/h;->Jc:I

    .line 40
    iput v2, p0, Lcom/facebook/drawee/backends/pipeline/b/h;->Js:I

    .line 41
    iput v2, p0, Lcom/facebook/drawee/backends/pipeline/b/h;->Jt:I

    .line 44
    iput v2, p0, Lcom/facebook/drawee/backends/pipeline/b/h;->JG:I

    .line 46
    iput v2, p0, Lcom/facebook/drawee/backends/pipeline/b/h;->Ju:I

    .line 47
    iput-wide v0, p0, Lcom/facebook/drawee/backends/pipeline/b/h;->Jv:J

    .line 48
    iput-wide v0, p0, Lcom/facebook/drawee/backends/pipeline/b/h;->Jw:J

    return-void
.end method


# virtual methods
.method public final gu()Lcom/facebook/drawee/backends/pipeline/b/e;
    .locals 34

    move-object/from16 v0, p0

    .line 171
    new-instance v31, Lcom/facebook/drawee/backends/pipeline/b/e;

    move-object/from16 v1, v31

    iget-object v2, v0, Lcom/facebook/drawee/backends/pipeline/b/h;->Je:Ljava/lang/String;

    iget-object v3, v0, Lcom/facebook/drawee/backends/pipeline/b/h;->Jg:Ljava/lang/String;

    iget-object v4, v0, Lcom/facebook/drawee/backends/pipeline/b/h;->Ji:Lcom/facebook/imagepipeline/request/b;

    iget-object v5, v0, Lcom/facebook/drawee/backends/pipeline/b/h;->Jh:Ljava/lang/Object;

    iget-object v6, v0, Lcom/facebook/drawee/backends/pipeline/b/h;->Jj:Lcom/facebook/imagepipeline/f/f;

    iget-wide v7, v0, Lcom/facebook/drawee/backends/pipeline/b/h;->Jk:J

    iget-wide v9, v0, Lcom/facebook/drawee/backends/pipeline/b/h;->Jl:J

    iget-wide v11, v0, Lcom/facebook/drawee/backends/pipeline/b/h;->Jm:J

    iget-wide v13, v0, Lcom/facebook/drawee/backends/pipeline/b/h;->Jn:J

    move-object/from16 v32, v1

    move-object/from16 v33, v2

    iget-wide v1, v0, Lcom/facebook/drawee/backends/pipeline/b/h;->Jo:J

    move-wide v15, v1

    iget-wide v1, v0, Lcom/facebook/drawee/backends/pipeline/b/h;->Jp:J

    move-wide/from16 v17, v1

    iget-wide v1, v0, Lcom/facebook/drawee/backends/pipeline/b/h;->Jq:J

    move-wide/from16 v19, v1

    iget v1, v0, Lcom/facebook/drawee/backends/pipeline/b/h;->Jc:I

    move/from16 v21, v1

    iget-boolean v1, v0, Lcom/facebook/drawee/backends/pipeline/b/h;->Jr:Z

    move/from16 v22, v1

    iget v1, v0, Lcom/facebook/drawee/backends/pipeline/b/h;->Js:I

    move/from16 v23, v1

    iget v1, v0, Lcom/facebook/drawee/backends/pipeline/b/h;->Jt:I

    move/from16 v24, v1

    iget v1, v0, Lcom/facebook/drawee/backends/pipeline/b/h;->Ju:I

    move/from16 v25, v1

    iget-wide v1, v0, Lcom/facebook/drawee/backends/pipeline/b/h;->Jv:J

    move-wide/from16 v26, v1

    iget-wide v1, v0, Lcom/facebook/drawee/backends/pipeline/b/h;->Jw:J

    move-wide/from16 v28, v1

    iget-object v1, v0, Lcom/facebook/drawee/backends/pipeline/b/h;->Jx:Ljava/lang/String;

    move-object/from16 v30, v1

    move-object/from16 v1, v32

    move-object/from16 v2, v33

    invoke-direct/range {v1 .. v30}, Lcom/facebook/drawee/backends/pipeline/b/e;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/imagepipeline/request/b;Ljava/lang/Object;Lcom/facebook/imagepipeline/f/f;JJJJJJJIZIIIJJLjava/lang/String;)V

    return-object v31
.end method

.method public final setVisible(Z)V
    .locals 0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x2

    .line 163
    :goto_0
    iput p1, p0, Lcom/facebook/drawee/backends/pipeline/b/h;->Ju:I

    return-void
.end method
