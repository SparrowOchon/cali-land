.class public abstract Lcom/facebook/drawee/controller/a;
.super Ljava/lang/Object;
.source "AbstractDraweeControllerBuilder.java"

# interfaces
.implements Lcom/facebook/drawee/d/d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/facebook/drawee/controller/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<BUI",
        "LDER:Lcom/facebook/drawee/controller/a<",
        "TBUI",
        "LDER;",
        "TREQUEST;TIMAGE;TINFO;>;REQUEST:",
        "Ljava/lang/Object;",
        "IMAGE:",
        "Ljava/lang/Object;",
        "INFO:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/drawee/d/d;"
    }
.end annotation


# static fields
.field private static final KK:Lcom/facebook/drawee/controller/ControllerListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/drawee/controller/ControllerListener<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final KL:Ljava/lang/NullPointerException;

.field protected static final KS:Ljava/util/concurrent/atomic/AtomicLong;


# instance fields
.field private IP:Lcom/facebook/common/d/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/d/k<",
            "Lcom/facebook/b/c<",
            "TIMAGE;>;>;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final IY:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/facebook/drawee/controller/ControllerListener;",
            ">;"
        }
    .end annotation
.end field

.field protected Jh:Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private KD:Z

.field private KE:Ljava/lang/String;

.field protected KM:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TREQUEST;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private KN:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TREQUEST;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public KO:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TREQUEST;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public KP:Z

.field public KQ:Z

.field protected KR:Lcom/facebook/drawee/d/a;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Kp:Z

.field private Kw:Lcom/facebook/drawee/controller/c;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Kx:Lcom/facebook/drawee/controller/ControllerListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/drawee/controller/ControllerListener<",
            "-TINFO;>;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 40
    new-instance v0, Lcom/facebook/drawee/controller/a$1;

    invoke-direct {v0}, Lcom/facebook/drawee/controller/a$1;-><init>()V

    sput-object v0, Lcom/facebook/drawee/controller/a;->KK:Lcom/facebook/drawee/controller/ControllerListener;

    .line 50
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "No image request was specified!"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/drawee/controller/a;->KL:Ljava/lang/NullPointerException;

    .line 73
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    sput-object v0, Lcom/facebook/drawee/controller/a;->KS:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Set<",
            "Lcom/facebook/drawee/controller/ControllerListener;",
            ">;)V"
        }
    .end annotation

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p1, p0, Lcom/facebook/drawee/controller/a;->mContext:Landroid/content/Context;

    .line 79
    iput-object p2, p0, Lcom/facebook/drawee/controller/a;->IY:Ljava/util/Set;

    const/4 p1, 0x0

    .line 1085
    iput-object p1, p0, Lcom/facebook/drawee/controller/a;->Jh:Ljava/lang/Object;

    .line 1086
    iput-object p1, p0, Lcom/facebook/drawee/controller/a;->KM:Ljava/lang/Object;

    .line 1087
    iput-object p1, p0, Lcom/facebook/drawee/controller/a;->KN:Ljava/lang/Object;

    .line 1088
    iput-object p1, p0, Lcom/facebook/drawee/controller/a;->KO:[Ljava/lang/Object;

    const/4 p2, 0x1

    .line 1089
    iput-boolean p2, p0, Lcom/facebook/drawee/controller/a;->KP:Z

    .line 1090
    iput-object p1, p0, Lcom/facebook/drawee/controller/a;->Kx:Lcom/facebook/drawee/controller/ControllerListener;

    .line 1091
    iput-object p1, p0, Lcom/facebook/drawee/controller/a;->Kw:Lcom/facebook/drawee/controller/c;

    const/4 p2, 0x0

    .line 1092
    iput-boolean p2, p0, Lcom/facebook/drawee/controller/a;->Kp:Z

    .line 1093
    iput-boolean p2, p0, Lcom/facebook/drawee/controller/a;->KQ:Z

    .line 1094
    iput-object p1, p0, Lcom/facebook/drawee/controller/a;->KR:Lcom/facebook/drawee/d/a;

    .line 1095
    iput-object p1, p0, Lcom/facebook/drawee/controller/a;->KE:Ljava/lang/String;

    return-void
.end method

.method private a(Lcom/facebook/drawee/d/a;Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/common/d/k;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/drawee/d/a;",
            "Ljava/lang/String;",
            "TREQUEST;)",
            "Lcom/facebook/common/d/k<",
            "Lcom/facebook/b/c<",
            "TIMAGE;>;>;"
        }
    .end annotation

    .line 388
    sget-object v0, Lcom/facebook/drawee/controller/a$a;->KZ:Lcom/facebook/drawee/controller/a$a;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/facebook/drawee/controller/a;->a(Lcom/facebook/drawee/d/a;Ljava/lang/String;Ljava/lang/Object;Lcom/facebook/drawee/controller/a$a;)Lcom/facebook/common/d/k;

    move-result-object p1

    return-object p1
.end method

.method private a(Lcom/facebook/drawee/d/a;Ljava/lang/String;Ljava/lang/Object;Lcom/facebook/drawee/controller/a$a;)Lcom/facebook/common/d/k;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/drawee/d/a;",
            "Ljava/lang/String;",
            "TREQUEST;",
            "Lcom/facebook/drawee/controller/a$a;",
            ")",
            "Lcom/facebook/common/d/k<",
            "Lcom/facebook/b/c<",
            "TIMAGE;>;>;"
        }
    .end annotation

    .line 17114
    iget-object v5, p0, Lcom/facebook/drawee/controller/a;->Jh:Ljava/lang/Object;

    .line 399
    new-instance v7, Lcom/facebook/drawee/controller/a$2;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/facebook/drawee/controller/a$2;-><init>(Lcom/facebook/drawee/controller/a;Lcom/facebook/drawee/d/a;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Lcom/facebook/drawee/controller/a$a;)V

    return-object v7
.end method


# virtual methods
.method public final H(Ljava/lang/Object;)Lcom/facebook/drawee/controller/a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TBUI",
            "LDER;"
        }
    .end annotation

    .line 107
    iput-object p1, p0, Lcom/facebook/drawee/controller/a;->Jh:Ljava/lang/Object;

    return-object p0
.end method

.method public final I(Ljava/lang/Object;)Lcom/facebook/drawee/controller/a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TREQUEST;)TBUI",
            "LDER;"
        }
    .end annotation

    .line 119
    iput-object p1, p0, Lcom/facebook/drawee/controller/a;->KM:Ljava/lang/Object;

    return-object p0
.end method

.method protected abstract a(Lcom/facebook/drawee/d/a;Ljava/lang/Object;Ljava/lang/Object;Lcom/facebook/drawee/controller/a$a;)Lcom/facebook/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/drawee/d/a;",
            "TREQUEST;",
            "Ljava/lang/Object;",
            "Lcom/facebook/drawee/controller/a$a;",
            ")",
            "Lcom/facebook/b/c<",
            "TIMAGE;>;"
        }
    .end annotation
.end method

.method protected final a(Lcom/facebook/drawee/d/a;Ljava/lang/String;)Lcom/facebook/common/d/k;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/drawee/d/a;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/common/d/k<",
            "Lcom/facebook/b/c<",
            "TIMAGE;>;>;"
        }
    .end annotation

    .line 334
    iget-object v0, p0, Lcom/facebook/drawee/controller/a;->IP:Lcom/facebook/common/d/k;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    .line 341
    iget-object v1, p0, Lcom/facebook/drawee/controller/a;->KM:Ljava/lang/Object;

    const/4 v2, 0x2

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    .line 342
    invoke-direct {p0, p1, p2, v1}, Lcom/facebook/drawee/controller/a;->a(Lcom/facebook/drawee/d/a;Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/common/d/k;

    move-result-object v0

    goto :goto_2

    .line 343
    :cond_1
    iget-object v1, p0, Lcom/facebook/drawee/controller/a;->KO:[Ljava/lang/Object;

    if-eqz v1, :cond_4

    .line 344
    iget-boolean v0, p0, Lcom/facebook/drawee/controller/a;->KP:Z

    .line 15370
    new-instance v4, Ljava/util/ArrayList;

    array-length v5, v1

    mul-int/lit8 v5, v5, 0x2

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    .line 15373
    :goto_0
    array-length v5, v1

    if-ge v0, v5, :cond_2

    .line 15374
    aget-object v5, v1, v0

    sget-object v6, Lcom/facebook/drawee/controller/a$a;->Lb:Lcom/facebook/drawee/controller/a$a;

    .line 15375
    invoke-direct {p0, p1, p2, v5, v6}, Lcom/facebook/drawee/controller/a;->a(Lcom/facebook/drawee/d/a;Ljava/lang/String;Ljava/lang/Object;Lcom/facebook/drawee/controller/a$a;)Lcom/facebook/common/d/k;

    move-result-object v5

    .line 15374
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    .line 15379
    :goto_1
    array-length v5, v1

    if-ge v0, v5, :cond_3

    .line 15380
    aget-object v5, v1, v0

    invoke-direct {p0, p1, p2, v5}, Lcom/facebook/drawee/controller/a;->a(Lcom/facebook/drawee/d/a;Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/common/d/k;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 16037
    :cond_3
    new-instance v0, Lcom/facebook/b/f;

    invoke-direct {v0, v4}, Lcom/facebook/b/f;-><init>(Ljava/util/List;)V

    :cond_4
    :goto_2
    if-eqz v0, :cond_5

    .line 350
    iget-object v1, p0, Lcom/facebook/drawee/controller/a;->KN:Ljava/lang/Object;

    if-eqz v1, :cond_5

    .line 351
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 352
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 353
    iget-object v0, p0, Lcom/facebook/drawee/controller/a;->KN:Ljava/lang/Object;

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/drawee/controller/a;->a(Lcom/facebook/drawee/d/a;Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/common/d/k;

    move-result-object p1

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 16072
    new-instance v0, Lcom/facebook/b/g;

    invoke-direct {v0, v1, v3}, Lcom/facebook/b/g;-><init>(Ljava/util/List;Z)V

    :cond_5
    if-nez v0, :cond_6

    .line 359
    sget-object p1, Lcom/facebook/drawee/controller/a;->KL:Ljava/lang/NullPointerException;

    .line 17036
    new-instance v0, Lcom/facebook/b/d$1;

    invoke-direct {v0, p1}, Lcom/facebook/b/d$1;-><init>(Ljava/lang/Throwable;)V

    :cond_6
    return-object v0
.end method

.method public final a(Lcom/facebook/drawee/d/a;)Lcom/facebook/drawee/controller/a;
    .locals 0
    .param p1    # Lcom/facebook/drawee/d/a;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/drawee/d/a;",
            ")TBUI",
            "LDER;"
        }
    .end annotation

    .line 274
    iput-object p1, p0, Lcom/facebook/drawee/controller/a;->KR:Lcom/facebook/drawee/d/a;

    return-object p0
.end method

.method public final bridge synthetic b(Lcom/facebook/drawee/d/a;)Lcom/facebook/drawee/d/d;
    .locals 0
    .param p1    # Lcom/facebook/drawee/d/a;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .line 17274
    iput-object p1, p0, Lcom/facebook/drawee/controller/a;->KR:Lcom/facebook/drawee/d/a;

    return-object p0
.end method

.method public final gG()Lcom/facebook/drawee/controller/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TBUI",
            "LDER;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 202
    iput-boolean v0, p0, Lcom/facebook/drawee/controller/a;->Kp:Z

    return-object p0
.end method

.method public final gH()Lcom/facebook/drawee/controller/AbstractDraweeController;
    .locals 4

    .line 6300
    iget-object v0, p0, Lcom/facebook/drawee/controller/a;->KO:[Ljava/lang/Object;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/drawee/controller/a;->KM:Ljava/lang/Object;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    const-string v3, "Cannot specify both ImageRequest and FirstAvailableImageRequests!"

    invoke-static {v0, v3}, Lcom/facebook/common/d/i;->a(ZLjava/lang/Object;)V

    .line 6303
    iget-object v0, p0, Lcom/facebook/drawee/controller/a;->IP:Lcom/facebook/common/d/k;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/drawee/controller/a;->KO:[Ljava/lang/Object;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/facebook/drawee/controller/a;->KM:Ljava/lang/Object;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/facebook/drawee/controller/a;->KN:Ljava/lang/Object;

    if-nez v0, :cond_3

    :cond_2
    const/4 v1, 0x1

    :cond_3
    const-string v0, "Cannot specify DataSourceSupplier with other ImageRequests! Use one or the other."

    invoke-static {v1, v0}, Lcom/facebook/common/d/i;->a(ZLjava/lang/Object;)V

    .line 290
    iget-object v0, p0, Lcom/facebook/drawee/controller/a;->KM:Ljava/lang/Object;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/facebook/drawee/controller/a;->KO:[Ljava/lang/Object;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/facebook/drawee/controller/a;->KN:Ljava/lang/Object;

    if-eqz v0, :cond_4

    .line 291
    iput-object v0, p0, Lcom/facebook/drawee/controller/a;->KM:Ljava/lang/Object;

    const/4 v0, 0x0

    .line 292
    iput-object v0, p0, Lcom/facebook/drawee/controller/a;->KN:Ljava/lang/Object;

    .line 6311
    :cond_4
    invoke-static {}, Lcom/facebook/imagepipeline/i/b;->isTracing()Z

    .line 6314
    invoke-virtual {p0}, Lcom/facebook/drawee/controller/a;->gr()Lcom/facebook/drawee/controller/AbstractDraweeController;

    move-result-object v0

    .line 7219
    iget-boolean v1, p0, Lcom/facebook/drawee/controller/a;->KD:Z

    .line 7243
    iput-boolean v1, v0, Lcom/facebook/drawee/controller/AbstractDraweeController;->KD:Z

    .line 7268
    iget-object v1, p0, Lcom/facebook/drawee/controller/a;->KE:Ljava/lang/String;

    .line 8255
    iput-object v1, v0, Lcom/facebook/drawee/controller/AbstractDraweeController;->KE:Ljava/lang/String;

    .line 9256
    iget-object v1, p0, Lcom/facebook/drawee/controller/a;->Kw:Lcom/facebook/drawee/controller/c;

    .line 9299
    iput-object v1, v0, Lcom/facebook/drawee/controller/AbstractDraweeController;->Kw:Lcom/facebook/drawee/controller/c;

    .line 9430
    iget-boolean v1, p0, Lcom/facebook/drawee/controller/a;->Kp:Z

    if-eqz v1, :cond_6

    .line 10222
    iget-object v1, v0, Lcom/facebook/drawee/controller/AbstractDraweeController;->Ku:Lcom/facebook/drawee/a/c;

    if-nez v1, :cond_5

    .line 10223
    new-instance v1, Lcom/facebook/drawee/a/c;

    invoke-direct {v1}, Lcom/facebook/drawee/a/c;-><init>()V

    iput-object v1, v0, Lcom/facebook/drawee/controller/AbstractDraweeController;->Ku:Lcom/facebook/drawee/a/c;

    .line 10225
    :cond_5
    iget-object v1, v0, Lcom/facebook/drawee/controller/AbstractDraweeController;->Ku:Lcom/facebook/drawee/a/c;

    .line 9433
    iget-boolean v2, p0, Lcom/facebook/drawee/controller/a;->Kp:Z

    .line 11050
    iput-boolean v2, v1, Lcom/facebook/drawee/a/c;->Kp:Z

    .line 12230
    iget-object v1, v0, Lcom/facebook/drawee/controller/AbstractDraweeController;->Kv:Lcom/facebook/drawee/c/a;

    if-nez v1, :cond_6

    .line 11441
    iget-object v1, p0, Lcom/facebook/drawee/controller/a;->mContext:Landroid/content/Context;

    .line 13051
    new-instance v2, Lcom/facebook/drawee/c/a;

    invoke-direct {v2, v1}, Lcom/facebook/drawee/c/a;-><init>(Landroid/content/Context;)V

    .line 13235
    iput-object v2, v0, Lcom/facebook/drawee/controller/AbstractDraweeController;->Kv:Lcom/facebook/drawee/c/a;

    .line 13236
    iget-object v1, v0, Lcom/facebook/drawee/controller/AbstractDraweeController;->Kv:Lcom/facebook/drawee/c/a;

    if-eqz v1, :cond_6

    .line 13237
    iget-object v1, v0, Lcom/facebook/drawee/controller/AbstractDraweeController;->Kv:Lcom/facebook/drawee/c/a;

    .line 14071
    iput-object v0, v1, Lcom/facebook/drawee/c/a;->NF:Lcom/facebook/drawee/c/a$a;

    .line 14415
    :cond_6
    iget-object v1, p0, Lcom/facebook/drawee/controller/a;->IY:Ljava/util/Set;

    if-eqz v1, :cond_7

    .line 14416
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/drawee/controller/ControllerListener;

    .line 14417
    invoke-virtual {v0, v2}, Lcom/facebook/drawee/controller/AbstractDraweeController;->a(Lcom/facebook/drawee/controller/ControllerListener;)V

    goto :goto_2

    .line 14420
    :cond_7
    iget-object v1, p0, Lcom/facebook/drawee/controller/a;->Kx:Lcom/facebook/drawee/controller/ControllerListener;

    if-eqz v1, :cond_8

    .line 14421
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/controller/AbstractDraweeController;->a(Lcom/facebook/drawee/controller/ControllerListener;)V

    .line 14423
    :cond_8
    iget-boolean v1, p0, Lcom/facebook/drawee/controller/a;->KQ:Z

    if-eqz v1, :cond_9

    .line 14424
    sget-object v1, Lcom/facebook/drawee/controller/a;->KK:Lcom/facebook/drawee/controller/ControllerListener;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/controller/AbstractDraweeController;->a(Lcom/facebook/drawee/controller/ControllerListener;)V

    .line 6320
    :cond_9
    invoke-static {}, Lcom/facebook/imagepipeline/i/b;->isTracing()Z

    return-object v0
.end method

.method public final synthetic gI()Lcom/facebook/drawee/d/a;
    .locals 1

    .line 33
    invoke-virtual {p0}, Lcom/facebook/drawee/controller/a;->gH()Lcom/facebook/drawee/controller/AbstractDraweeController;

    move-result-object v0

    return-object v0
.end method

.method protected abstract gr()Lcom/facebook/drawee/controller/AbstractDraweeController;
.end method

.method public final u(Z)Lcom/facebook/drawee/controller/a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TBUI",
            "LDER;"
        }
    .end annotation

    .line 224
    iput-boolean p1, p0, Lcom/facebook/drawee/controller/a;->KQ:Z

    return-object p0
.end method
