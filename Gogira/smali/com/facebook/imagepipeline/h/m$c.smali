.class abstract Lcom/facebook/imagepipeline/h/m$c;
.super Lcom/facebook/imagepipeline/h/n;
.source "DecodeProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/imagepipeline/h/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x400
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/imagepipeline/h/n<",
        "Lcom/facebook/imagepipeline/f/e;",
        "Lcom/facebook/common/references/CloseableReference<",
        "Lcom/facebook/imagepipeline/f/c;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final Ra:Lcom/facebook/imagepipeline/a/b;

.field private final TAG:Ljava/lang/String;

.field final WB:Lcom/facebook/imagepipeline/h/ak;

.field final synthetic WE:Lcom/facebook/imagepipeline/h/m;

.field final WH:Lcom/facebook/imagepipeline/h/u;

.field private Wn:Z

.field private final Wo:Lcom/facebook/imagepipeline/h/am;


# direct methods
.method public constructor <init>(Lcom/facebook/imagepipeline/h/m;Lcom/facebook/imagepipeline/h/k;Lcom/facebook/imagepipeline/h/ak;ZI)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/h/k<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lcom/facebook/imagepipeline/f/c;",
            ">;>;",
            "Lcom/facebook/imagepipeline/h/ak;",
            "ZI)V"
        }
    .end annotation

    .line 143
    iput-object p1, p0, Lcom/facebook/imagepipeline/h/m$c;->WE:Lcom/facebook/imagepipeline/h/m;

    .line 144
    invoke-direct {p0, p2}, Lcom/facebook/imagepipeline/h/n;-><init>(Lcom/facebook/imagepipeline/h/k;)V

    const-string p2, "ProgressiveDecoder"

    .line 127
    iput-object p2, p0, Lcom/facebook/imagepipeline/h/m$c;->TAG:Ljava/lang/String;

    .line 145
    iput-object p3, p0, Lcom/facebook/imagepipeline/h/m$c;->WB:Lcom/facebook/imagepipeline/h/ak;

    .line 146
    invoke-interface {p3}, Lcom/facebook/imagepipeline/h/ak;->ji()Lcom/facebook/imagepipeline/h/am;

    move-result-object p2

    iput-object p2, p0, Lcom/facebook/imagepipeline/h/m$c;->Wo:Lcom/facebook/imagepipeline/h/am;

    .line 147
    invoke-interface {p3}, Lcom/facebook/imagepipeline/h/ak;->jh()Lcom/facebook/imagepipeline/request/b;

    move-result-object p2

    .line 1187
    iget-object p2, p2, Lcom/facebook/imagepipeline/request/b;->Ra:Lcom/facebook/imagepipeline/a/b;

    .line 147
    iput-object p2, p0, Lcom/facebook/imagepipeline/h/m$c;->Ra:Lcom/facebook/imagepipeline/a/b;

    const/4 p2, 0x0

    .line 148
    iput-boolean p2, p0, Lcom/facebook/imagepipeline/h/m$c;->Wn:Z

    .line 149
    new-instance p2, Lcom/facebook/imagepipeline/h/m$c$1;

    invoke-direct {p2, p0, p1, p3, p5}, Lcom/facebook/imagepipeline/h/m$c$1;-><init>(Lcom/facebook/imagepipeline/h/m$c;Lcom/facebook/imagepipeline/h/m;Lcom/facebook/imagepipeline/h/ak;I)V

    .line 170
    new-instance p3, Lcom/facebook/imagepipeline/h/u;

    .line 2047
    iget-object p5, p1, Lcom/facebook/imagepipeline/h/m;->mExecutor:Ljava/util/concurrent/Executor;

    .line 170
    iget-object v0, p0, Lcom/facebook/imagepipeline/h/m$c;->Ra:Lcom/facebook/imagepipeline/a/b;

    iget v0, v0, Lcom/facebook/imagepipeline/a/b;->RU:I

    invoke-direct {p3, p5, p2, v0}, Lcom/facebook/imagepipeline/h/u;-><init>(Ljava/util/concurrent/Executor;Lcom/facebook/imagepipeline/h/u$a;I)V

    iput-object p3, p0, Lcom/facebook/imagepipeline/h/m$c;->WH:Lcom/facebook/imagepipeline/h/u;

    .line 171
    iget-object p2, p0, Lcom/facebook/imagepipeline/h/m$c;->WB:Lcom/facebook/imagepipeline/h/ak;

    new-instance p3, Lcom/facebook/imagepipeline/h/m$c$2;

    invoke-direct {p3, p0, p1, p4}, Lcom/facebook/imagepipeline/h/m$c$2;-><init>(Lcom/facebook/imagepipeline/h/m$c;Lcom/facebook/imagepipeline/h/m;Z)V

    invoke-interface {p2, p3}, Lcom/facebook/imagepipeline/h/ak;->a(Lcom/facebook/imagepipeline/h/al;)V

    return-void
.end method

.method private A(Z)V
    .locals 1

    .line 380
    monitor-enter p0

    if-eqz p1, :cond_1

    .line 381
    :try_start_0
    iget-boolean p1, p0, Lcom/facebook/imagepipeline/h/m$c;->Wn:Z

    if-eqz p1, :cond_0

    goto :goto_0

    .line 3021
    :cond_0
    iget-object p1, p0, Lcom/facebook/imagepipeline/h/n;->WN:Lcom/facebook/imagepipeline/h/k;

    const/high16 v0, 0x3f800000    # 1.0f

    .line 384
    invoke-interface {p1, v0}, Lcom/facebook/imagepipeline/h/k;->m(F)V

    const/4 p1, 0x1

    .line 385
    iput-boolean p1, p0, Lcom/facebook/imagepipeline/h/m$c;->Wn:Z

    .line 386
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 387
    iget-object p1, p0, Lcom/facebook/imagepipeline/h/m$c;->WH:Lcom/facebook/imagepipeline/h/u;

    invoke-virtual {p1}, Lcom/facebook/imagepipeline/h/u;->ju()V

    return-void

    .line 382
    :cond_1
    :goto_0
    :try_start_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 386
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method private a(Lcom/facebook/imagepipeline/f/c;JLcom/facebook/imagepipeline/f/h;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;
    .locals 16
    .param p1    # Lcom/facebook/imagepipeline/f/c;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/f/c;",
            "J",
            "Lcom/facebook/imagepipeline/f/h;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p6

    move-object/from16 v3, p7

    move-object/from16 v4, p8

    move-object/from16 v5, p9

    .line 334
    iget-object v6, v0, Lcom/facebook/imagepipeline/h/m$c;->Wo:Lcom/facebook/imagepipeline/h/am;

    iget-object v7, v0, Lcom/facebook/imagepipeline/h/m$c;->WB:Lcom/facebook/imagepipeline/h/ak;

    invoke-interface {v7}, Lcom/facebook/imagepipeline/h/ak;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/facebook/imagepipeline/h/am;->aw(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    const/4 v1, 0x0

    return-object v1

    .line 337
    :cond_0
    invoke-static/range {p2 .. p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    .line 338
    invoke-interface/range {p4 .. p4}, Lcom/facebook/imagepipeline/f/h;->iK()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v7

    .line 339
    invoke-static/range {p5 .. p5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v8

    .line 340
    instance-of v9, v1, Lcom/facebook/imagepipeline/f/d;

    const-string v10, "sampleSize"

    const-string v11, "requestedImageSize"

    const-string v12, "imageFormat"

    const-string v13, "encodedImageSize"

    const-string v14, "isFinal"

    const-string v15, "hasGoodQuality"

    const-string v0, "queueTime"

    if-eqz v9, :cond_1

    .line 341
    check-cast v1, Lcom/facebook/imagepipeline/f/d;

    .line 2165
    iget-object v1, v1, Lcom/facebook/imagepipeline/f/d;->mBitmap:Landroid/graphics/Bitmap;

    .line 342
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, "x"

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 345
    new-instance v5, Ljava/util/HashMap;

    const/16 v9, 0x8

    invoke-direct {v5, v9}, Ljava/util/HashMap;-><init>(I)V

    const-string v9, "bitmapSize"

    .line 346
    invoke-interface {v5, v9, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 347
    invoke-interface {v5, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    invoke-interface {v5, v15, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 349
    invoke-interface {v5, v14, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    invoke-interface {v5, v13, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    invoke-interface {v5, v12, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    invoke-interface {v5, v11, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v1, p9

    .line 353
    invoke-interface {v5, v10, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    invoke-static {v5}, Lcom/facebook/common/d/f;->m(Ljava/util/Map;)Lcom/facebook/common/d/f;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v1, v5

    .line 356
    new-instance v5, Ljava/util/HashMap;

    const/4 v9, 0x7

    invoke-direct {v5, v9}, Ljava/util/HashMap;-><init>(I)V

    .line 357
    invoke-interface {v5, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 358
    invoke-interface {v5, v15, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 359
    invoke-interface {v5, v14, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 360
    invoke-interface {v5, v13, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 361
    invoke-interface {v5, v12, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    invoke-interface {v5, v11, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 363
    invoke-interface {v5, v10, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 364
    invoke-static {v5}, Lcom/facebook/common/d/f;->m(Ljava/util/Map;)Lcom/facebook/common/d/f;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/facebook/imagepipeline/h/m$c;Lcom/facebook/imagepipeline/f/e;I)V
    .locals 20

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    move/from16 v0, p2

    const-string v13, "DecodeProducer"

    .line 5237
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/imagepipeline/f/e;->iC()Lcom/facebook/d/c;

    move-result-object v1

    sget-object v2, Lcom/facebook/d/b;->PD:Lcom/facebook/d/c;

    if-eq v1, v2, :cond_1

    invoke-static/range {p2 .. p2}, Lcom/facebook/imagepipeline/h/m$c;->aE(I)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    return-void

    .line 5241
    :cond_1
    :goto_0
    invoke-direct/range {p0 .. p0}, Lcom/facebook/imagepipeline/h/m$c;->isFinished()Z

    move-result v1

    if-nez v1, :cond_b

    invoke-static/range {p1 .. p1}, Lcom/facebook/imagepipeline/f/e;->f(Lcom/facebook/imagepipeline/f/e;)Z

    move-result v1

    if-nez v1, :cond_2

    goto/16 :goto_9

    .line 5245
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/imagepipeline/f/e;->iC()Lcom/facebook/d/c;

    move-result-object v1

    const-string v2, "unknown"

    if-eqz v1, :cond_3

    .line 6062
    iget-object v1, v1, Lcom/facebook/d/c;->mName:Ljava/lang/String;

    move-object v7, v1

    goto :goto_1

    :cond_3
    move-object v7, v2

    .line 5251
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/imagepipeline/f/e;->getWidth()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "x"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/imagepipeline/f/e;->getHeight()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 6269
    iget v1, v12, Lcom/facebook/imagepipeline/f/e;->UE:I

    .line 5252
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    .line 5253
    invoke-static/range {p2 .. p2}, Lcom/facebook/imagepipeline/h/m$c;->aD(I)Z

    move-result v6

    const/4 v4, 0x1

    if-eqz v6, :cond_4

    const/16 v5, 0x8

    .line 5254
    invoke-static {v0, v5}, Lcom/facebook/imagepipeline/h/m$c;->n(II)Z

    move-result v5

    if-nez v5, :cond_4

    const/4 v5, 0x1

    goto :goto_2

    :cond_4
    const/4 v5, 0x0

    :goto_2
    const/4 v9, 0x4

    .line 5255
    invoke-static {v0, v9}, Lcom/facebook/imagepipeline/h/m$c;->n(II)Z

    move-result v14

    .line 5257
    iget-object v15, v11, Lcom/facebook/imagepipeline/h/m$c;->WB:Lcom/facebook/imagepipeline/h/ak;

    invoke-interface {v15}, Lcom/facebook/imagepipeline/h/ak;->jh()Lcom/facebook/imagepipeline/request/b;

    move-result-object v15

    .line 7166
    iget-object v15, v15, Lcom/facebook/imagepipeline/request/b;->QY:Lcom/facebook/imagepipeline/a/e;

    if-eqz v15, :cond_5

    .line 5259
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, v15, Lcom/facebook/imagepipeline/a/e;->width:I

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, v15, Lcom/facebook/imagepipeline/a/e;->height:I

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v15, v1

    goto :goto_3

    :cond_5
    move-object v15, v2

    .line 5264
    :goto_3
    :try_start_0
    iget-object v1, v11, Lcom/facebook/imagepipeline/h/m$c;->WH:Lcom/facebook/imagepipeline/h/u;

    invoke-virtual {v1}, Lcom/facebook/imagepipeline/h/u;->jx()J

    move-result-wide v17

    .line 5265
    iget-object v1, v11, Lcom/facebook/imagepipeline/h/m$c;->WB:Lcom/facebook/imagepipeline/h/ak;

    invoke-interface {v1}, Lcom/facebook/imagepipeline/h/ak;->jh()Lcom/facebook/imagepipeline/request/b;

    move-result-object v1

    .line 8150
    iget-object v1, v1, Lcom/facebook/imagepipeline/request/b;->YA:Landroid/net/Uri;

    .line 5265
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    if-nez v5, :cond_7

    if-eqz v14, :cond_6

    goto :goto_4

    .line 5268
    :cond_6
    invoke-virtual/range {p0 .. p1}, Lcom/facebook/imagepipeline/h/m$c;->g(Lcom/facebook/imagepipeline/f/e;)I

    move-result v2

    goto :goto_5

    .line 5267
    :cond_7
    :goto_4
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/imagepipeline/f/e;->getSize()I

    move-result v2

    :goto_5
    if-nez v5, :cond_9

    if-eqz v14, :cond_8

    goto :goto_6

    .line 5271
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/imagepipeline/h/m$c;->iy()Lcom/facebook/imagepipeline/f/h;

    move-result-object v3

    goto :goto_7

    .line 5269
    :cond_9
    :goto_6
    sget-object v3, Lcom/facebook/imagepipeline/f/g;->UI:Lcom/facebook/imagepipeline/f/h;

    :goto_7
    move-object v5, v3

    .line 5273
    iget-object v3, v11, Lcom/facebook/imagepipeline/h/m$c;->Wo:Lcom/facebook/imagepipeline/h/am;

    iget-object v14, v11, Lcom/facebook/imagepipeline/h/m$c;->WB:Lcom/facebook/imagepipeline/h/ak;

    invoke-interface {v14}, Lcom/facebook/imagepipeline/h/ak;->getId()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v3, v14, v13}, Lcom/facebook/imagepipeline/h/am;->u(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 5277
    :try_start_1
    iget-object v14, v11, Lcom/facebook/imagepipeline/h/m$c;->WE:Lcom/facebook/imagepipeline/h/m;

    .line 9047
    iget-object v14, v14, Lcom/facebook/imagepipeline/h/m;->SU:Lcom/facebook/imagepipeline/d/c;

    .line 5277
    iget-object v3, v11, Lcom/facebook/imagepipeline/h/m$c;->Ra:Lcom/facebook/imagepipeline/a/b;

    invoke-interface {v14, v12, v2, v5, v3}, Lcom/facebook/imagepipeline/d/c;->a(Lcom/facebook/imagepipeline/f/e;ILcom/facebook/imagepipeline/f/h;Lcom/facebook/imagepipeline/a/b;)Lcom/facebook/imagepipeline/f/c;

    move-result-object v14
    :try_end_1
    .catch Lcom/facebook/imagepipeline/d/a; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 10269
    :try_start_2
    iget v1, v12, Lcom/facebook/imagepipeline/f/e;->UE:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eq v1, v4, :cond_a

    or-int/lit8 v0, v0, 0x10

    :cond_a
    move-object/from16 v1, p0

    move-object v2, v14

    move-wide/from16 v3, v17

    move-object v9, v15

    .line 5308
    :try_start_3
    invoke-direct/range {v1 .. v10}, Lcom/facebook/imagepipeline/h/m$c;->a(Lcom/facebook/imagepipeline/f/c;JLcom/facebook/imagepipeline/f/h;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    .line 5317
    iget-object v2, v11, Lcom/facebook/imagepipeline/h/m$c;->Wo:Lcom/facebook/imagepipeline/h/am;

    iget-object v3, v11, Lcom/facebook/imagepipeline/h/m$c;->WB:Lcom/facebook/imagepipeline/h/ak;

    .line 5318
    invoke-interface {v3}, Lcom/facebook/imagepipeline/h/ak;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v13, v1}, Lcom/facebook/imagepipeline/h/am;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 10394
    invoke-static {v14}, Lcom/facebook/common/references/CloseableReference;->b(Ljava/io/Closeable;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 10396
    :try_start_4
    invoke-static {v0}, Lcom/facebook/imagepipeline/h/m$c;->aD(I)Z

    move-result v2

    invoke-direct {v11, v2}, Lcom/facebook/imagepipeline/h/m$c;->A(Z)V

    .line 11021
    iget-object v2, v11, Lcom/facebook/imagepipeline/h/n;->WN:Lcom/facebook/imagepipeline/h/k;

    .line 10397
    invoke-interface {v2, v1, v0}, Lcom/facebook/imagepipeline/h/k;->b(Ljava/lang/Object;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 10399
    :try_start_5
    invoke-static {v1}, Lcom/facebook/common/references/CloseableReference;->c(Lcom/facebook/common/references/CloseableReference;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 5321
    invoke-static/range {p1 .. p1}, Lcom/facebook/imagepipeline/f/e;->e(Lcom/facebook/imagepipeline/f/e;)V

    return-void

    :catchall_0
    move-exception v0

    .line 10399
    :try_start_6
    invoke-static {v1}, Lcom/facebook/common/references/CloseableReference;->c(Lcom/facebook/common/references/CloseableReference;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catch_0
    move-exception v0

    move-object v2, v14

    goto :goto_8

    :catch_1
    move-exception v0

    const/4 v2, 0x0

    goto :goto_8

    :catch_2
    move-exception v0

    .line 10027
    :try_start_7
    iget-object v2, v0, Lcom/facebook/imagepipeline/d/a;->mEncodedImage:Lcom/facebook/imagepipeline/f/e;

    const-string v3, "ProgressiveDecoder"

    const-string v14, "%s, {uri: %s, firstEncodedBytes: %s, length: %d}"

    new-array v9, v9, [Ljava/lang/Object;

    .line 5283
    invoke-virtual {v0}, Lcom/facebook/imagepipeline/d/a;->getMessage()Ljava/lang/String;

    move-result-object v19

    const/16 v16, 0x0

    aput-object v19, v9, v16

    aput-object v1, v9, v4

    const/4 v1, 0x2

    .line 5285
    invoke-virtual {v2}, Lcom/facebook/imagepipeline/f/e;->iF()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v1

    const/4 v1, 0x3

    .line 5287
    invoke-virtual {v2}, Lcom/facebook/imagepipeline/f/e;->getSize()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v9, v1

    .line 5280
    invoke-static {v3, v14, v9}, Lcom/facebook/common/e/a;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 5288
    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :goto_8
    move-object/from16 v1, p0

    move-wide/from16 v3, v17

    move-object v9, v15

    .line 5294
    :try_start_8
    invoke-direct/range {v1 .. v10}, Lcom/facebook/imagepipeline/h/m$c;->a(Lcom/facebook/imagepipeline/f/c;JLcom/facebook/imagepipeline/f/h;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    .line 5303
    iget-object v2, v11, Lcom/facebook/imagepipeline/h/m$c;->Wo:Lcom/facebook/imagepipeline/h/am;

    iget-object v3, v11, Lcom/facebook/imagepipeline/h/m$c;->WB:Lcom/facebook/imagepipeline/h/ak;

    .line 5304
    invoke-interface {v3}, Lcom/facebook/imagepipeline/h/ak;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v13, v0, v1}, Lcom/facebook/imagepipeline/h/am;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    .line 5305
    invoke-direct {v11, v0}, Lcom/facebook/imagepipeline/h/m$c;->handleError(Ljava/lang/Throwable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 5321
    invoke-static/range {p1 .. p1}, Lcom/facebook/imagepipeline/f/e;->e(Lcom/facebook/imagepipeline/f/e;)V

    return-void

    :catchall_1
    move-exception v0

    invoke-static/range {p1 .. p1}, Lcom/facebook/imagepipeline/f/e;->e(Lcom/facebook/imagepipeline/f/e;)V

    throw v0

    :cond_b
    :goto_9
    return-void
.end method

.method private handleError(Ljava/lang/Throwable;)V
    .locals 1

    const/4 v0, 0x1

    .line 407
    invoke-direct {p0, v0}, Lcom/facebook/imagepipeline/h/m$c;->A(Z)V

    .line 4021
    iget-object v0, p0, Lcom/facebook/imagepipeline/h/n;->WN:Lcom/facebook/imagepipeline/h/k;

    .line 408
    invoke-interface {v0, p1}, Lcom/facebook/imagepipeline/h/k;->i(Ljava/lang/Throwable;)V

    return-void
.end method

.method private declared-synchronized isFinished()Z
    .locals 1

    monitor-enter p0

    .line 372
    :try_start_0
    iget-boolean v0, p0, Lcom/facebook/imagepipeline/h/m$c;->Wn:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;I)V
    .locals 2

    .line 124
    check-cast p1, Lcom/facebook/imagepipeline/f/e;

    .line 5192
    :try_start_0
    invoke-static {}, Lcom/facebook/imagepipeline/i/b;->isTracing()Z

    .line 5195
    invoke-static {p2}, Lcom/facebook/imagepipeline/h/m$c;->aD(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5196
    invoke-static {p1}, Lcom/facebook/imagepipeline/f/e;->f(Lcom/facebook/imagepipeline/f/e;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 5197
    new-instance p1, Lcom/facebook/common/j/a;

    const-string p2, "Encoded image is not valid."

    invoke-direct {p1, p2}, Lcom/facebook/common/j/a;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/facebook/imagepipeline/h/m$c;->handleError(Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5208
    invoke-static {}, Lcom/facebook/imagepipeline/i/b;->isTracing()Z

    return-void

    .line 5200
    :cond_0
    :try_start_1
    invoke-virtual {p0, p1, p2}, Lcom/facebook/imagepipeline/h/m$c;->a(Lcom/facebook/imagepipeline/f/e;I)Z

    move-result p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez p1, :cond_1

    .line 5208
    invoke-static {}, Lcom/facebook/imagepipeline/i/b;->isTracing()Z

    return-void

    :cond_1
    const/4 p1, 0x4

    .line 5203
    :try_start_2
    invoke-static {p2, p1}, Lcom/facebook/imagepipeline/h/m$c;->n(II)Z

    move-result p1

    if-nez v0, :cond_2

    if-nez p1, :cond_2

    .line 5204
    iget-object p1, p0, Lcom/facebook/imagepipeline/h/m$c;->WB:Lcom/facebook/imagepipeline/h/ak;

    invoke-interface {p1}, Lcom/facebook/imagepipeline/h/ak;->jm()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 5205
    :cond_2
    iget-object p1, p0, Lcom/facebook/imagepipeline/h/m$c;->WH:Lcom/facebook/imagepipeline/h/u;

    invoke-virtual {p1}, Lcom/facebook/imagepipeline/h/u;->jv()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 5208
    :cond_3
    invoke-static {}, Lcom/facebook/imagepipeline/i/b;->isTracing()Z

    return-void

    :catchall_0
    move-exception p1

    invoke-static {}, Lcom/facebook/imagepipeline/i/b;->isTracing()Z

    .line 5209
    throw p1
.end method

.method protected a(Lcom/facebook/imagepipeline/f/e;I)Z
    .locals 1

    .line 231
    iget-object v0, p0, Lcom/facebook/imagepipeline/h/m$c;->WH:Lcom/facebook/imagepipeline/h/u;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/imagepipeline/h/u;->c(Lcom/facebook/imagepipeline/f/e;I)Z

    move-result p1

    return p1
.end method

.method protected abstract g(Lcom/facebook/imagepipeline/f/e;)I
.end method

.method public final h(Ljava/lang/Throwable;)V
    .locals 0

    .line 221
    invoke-direct {p0, p1}, Lcom/facebook/imagepipeline/h/m$c;->handleError(Ljava/lang/Throwable;)V

    return-void
.end method

.method public final iu()V
    .locals 0

    .line 226
    invoke-virtual {p0}, Lcom/facebook/imagepipeline/h/m$c;->jt()V

    return-void
.end method

.method protected abstract iy()Lcom/facebook/imagepipeline/f/h;
.end method

.method final jt()V
    .locals 1

    const/4 v0, 0x1

    .line 415
    invoke-direct {p0, v0}, Lcom/facebook/imagepipeline/h/m$c;->A(Z)V

    .line 5021
    iget-object v0, p0, Lcom/facebook/imagepipeline/h/n;->WN:Lcom/facebook/imagepipeline/h/k;

    .line 416
    invoke-interface {v0}, Lcom/facebook/imagepipeline/h/k;->fB()V

    return-void
.end method

.method protected final l(F)V
    .locals 1

    const v0, 0x3f7d70a4    # 0.99f

    mul-float p1, p1, v0

    .line 216
    invoke-super {p0, p1}, Lcom/facebook/imagepipeline/h/n;->l(F)V

    return-void
.end method
