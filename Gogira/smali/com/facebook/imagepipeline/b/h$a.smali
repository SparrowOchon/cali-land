.class public final Lcom/facebook/imagepipeline/b/h$a;
.super Ljava/lang/Object;
.source "ImagePipelineConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/imagepipeline/b/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field IS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/facebook/imagepipeline/g/c;",
            ">;"
        }
    .end annotation
.end field

.field Os:Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

.field Oy:Landroid/graphics/Bitmap$Config;

.field Pd:Lcom/facebook/imagepipeline/b/e;

.field Rk:Lcom/facebook/imagepipeline/cache/m;

.field SD:Lcom/facebook/common/d/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/d/k<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field SI:Lcom/facebook/imagepipeline/cache/f;

.field public SO:Lcom/facebook/common/d/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/d/k<",
            "Lcom/facebook/imagepipeline/cache/MemoryCacheParams;",
            ">;"
        }
    .end annotation
.end field

.field SQ:Lcom/facebook/imagepipeline/cache/h$a;

.field public SR:Z

.field SS:Lcom/facebook/imagepipeline/b/f;

.field ST:Lcom/facebook/common/d/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/d/k<",
            "Lcom/facebook/imagepipeline/cache/MemoryCacheParams;",
            ">;"
        }
    .end annotation
.end field

.field SU:Lcom/facebook/imagepipeline/d/c;

.field SV:Lcom/facebook/imagepipeline/j/d;

.field SW:Ljava/lang/Integer;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public SX:Lcom/facebook/cache/disk/DiskCacheConfig;

.field SY:Lcom/facebook/common/g/b;

.field Ta:Lcom/facebook/imagepipeline/h/af;

.field Tc:Lcom/facebook/imagepipeline/memory/ac;

.field Td:Lcom/facebook/imagepipeline/d/e;

.field Te:Z

.field public Tf:Lcom/facebook/cache/disk/DiskCacheConfig;

.field Tg:Lcom/facebook/imagepipeline/d/d;

.field Ti:Z

.field Tl:Ljava/lang/Integer;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field Tm:I

.field public final Tn:Lcom/facebook/imagepipeline/b/i$a;

.field final mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 449
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 424
    iput-boolean v0, p0, Lcom/facebook/imagepipeline/b/h$a;->SR:Z

    const/4 v0, 0x0

    .line 430
    iput-object v0, p0, Lcom/facebook/imagepipeline/b/h$a;->SW:Ljava/lang/Integer;

    .line 434
    iput-object v0, p0, Lcom/facebook/imagepipeline/b/h$a;->Tl:Ljava/lang/Integer;

    const/4 v0, 0x1

    .line 440
    iput-boolean v0, p0, Lcom/facebook/imagepipeline/b/h$a;->Te:Z

    const/4 v1, -0x1

    .line 444
    iput v1, p0, Lcom/facebook/imagepipeline/b/h$a;->Tm:I

    .line 445
    new-instance v1, Lcom/facebook/imagepipeline/b/i$a;

    invoke-direct {v1, p0}, Lcom/facebook/imagepipeline/b/i$a;-><init>(Lcom/facebook/imagepipeline/b/h$a;)V

    iput-object v1, p0, Lcom/facebook/imagepipeline/b/h$a;->Tn:Lcom/facebook/imagepipeline/b/i$a;

    .line 447
    iput-boolean v0, p0, Lcom/facebook/imagepipeline/b/h$a;->Ti:Z

    .line 451
    invoke-static {p1}, Lcom/facebook/common/d/i;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    iput-object p1, p0, Lcom/facebook/imagepipeline/b/h$a;->mContext:Landroid/content/Context;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;B)V
    .locals 0

    .line 417
    invoke-direct {p0, p1}, Lcom/facebook/imagepipeline/b/h$a;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final hV()Lcom/facebook/imagepipeline/b/h;
    .locals 2

    .line 614
    new-instance v0, Lcom/facebook/imagepipeline/b/h;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/facebook/imagepipeline/b/h;-><init>(Lcom/facebook/imagepipeline/b/h$a;B)V

    return-object v0
.end method
