.class public final Lcom/google/android/gms/internal/measurement/bm;
.super Ljava/lang/Object;


# instance fields
.field private final apg:Z

.field private final asA:Z

.field private final asB:Z

.field private final asC:Z

.field private final asD:Lcom/google/android/gms/internal/measurement/bq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/bq<",
            "Landroid/content/Context;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final asw:Ljava/lang/String;

.field final asx:Landroid/net/Uri;

.field final asy:Ljava/lang/String;

.field final asz:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/net/Uri;)V
    .locals 1

    const-string v0, ""

    .line 1
    invoke-direct {p0, p1, v0, v0}, Lcom/google/android/gms/internal/measurement/bm;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/bm;->asw:Ljava/lang/String;

    .line 5
    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/bm;->asx:Landroid/net/Uri;

    .line 6
    iput-object p2, p0, Lcom/google/android/gms/internal/measurement/bm;->asy:Ljava/lang/String;

    .line 7
    iput-object p3, p0, Lcom/google/android/gms/internal/measurement/bm;->asz:Ljava/lang/String;

    const/4 p1, 0x0

    .line 8
    iput-boolean p1, p0, Lcom/google/android/gms/internal/measurement/bm;->asA:Z

    .line 9
    iput-boolean p1, p0, Lcom/google/android/gms/internal/measurement/bm;->asB:Z

    .line 10
    iput-boolean p1, p0, Lcom/google/android/gms/internal/measurement/bm;->asC:Z

    .line 11
    iput-boolean p1, p0, Lcom/google/android/gms/internal/measurement/bm;->apg:Z

    .line 12
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/bm;->asD:Lcom/google/android/gms/internal/measurement/bq;

    return-void
.end method
