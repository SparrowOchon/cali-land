.class public final Lcom/discord/utilities/textprocessing/node/EmojiNode;
.super Lcom/discord/simpleast/core/node/a;
.source "EmojiNode.kt"

# interfaces
.implements Lcom/discord/utilities/textprocessing/node/Spoilerable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;,
        Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;",
        ">",
        "Lcom/discord/simpleast/core/node/a<",
        "TT;>;",
        "Lcom/discord/utilities/textprocessing/node/Spoilerable;"
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;

.field private static final EMOJI_SIZE:I

.field private static final JUMBOIFY_SCALE_FACTOR:I = 0x2


# instance fields
.field private height:I

.field private isRevealed:Z

.field private final urlProvider:Lkotlin/jvm/functions/Function3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function3<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private verticalAlignment:I

.field private width:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->Companion:Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;

    const/16 v0, 0x10

    .line 97
    invoke-static {v0}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v0

    sput v0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->EMOJI_SIZE:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lkotlin/jvm/functions/Function3;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Ljava/lang/Boolean;",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/discord/utilities/textprocessing/node/EmojiNode;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function3;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lkotlin/jvm/functions/Function3;I)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Ljava/lang/Boolean;",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/discord/utilities/textprocessing/node/EmojiNode;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function3;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lkotlin/jvm/functions/Function3;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Ljava/lang/Boolean;",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ">;II)V"
        }
    .end annotation

    const-string v0, "emojiName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "urlProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0, p1}, Lcom/discord/simpleast/core/node/a;-><init>(Ljava/lang/String;)V

    iput-object p2, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->urlProvider:Lkotlin/jvm/functions/Function3;

    iput p3, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->width:I

    iput p4, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->height:I

    const/4 p1, 0x2

    .line 32
    iput p1, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->verticalAlignment:I

    const/4 p1, 0x1

    .line 34
    iput-boolean p1, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->isRevealed:Z

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lkotlin/jvm/functions/Function3;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_0

    .line 28
    sget p3, Lcom/discord/utilities/textprocessing/node/EmojiNode;->EMOJI_SIZE:I

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    .line 29
    sget p4, Lcom/discord/utilities/textprocessing/node/EmojiNode;->EMOJI_SIZE:I

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/utilities/textprocessing/node/EmojiNode;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function3;II)V

    return-void
.end method

.method public static final synthetic access$getEMOJI_SIZE$cp()I
    .locals 1

    .line 25
    sget v0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->EMOJI_SIZE:I

    return v0
.end method

.method public static final renderEmoji(Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;Lcom/discord/models/domain/ModelMessageReaction$Emoji;Z)V
    .locals 7

    sget-object v0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->Companion:Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    invoke-static/range {v0 .. v6}, Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;->renderEmoji$default(Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;Lcom/discord/models/domain/ModelMessageReaction$Emoji;ZIILjava/lang/Object;)V

    return-void
.end method

.method public static final renderEmoji(Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;Lcom/discord/models/domain/ModelMessageReaction$Emoji;ZI)V
    .locals 1

    sget-object v0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->Companion:Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;->renderEmoji(Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;Lcom/discord/models/domain/ModelMessageReaction$Emoji;ZI)V

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .line 85
    instance-of v0, p1, Lcom/discord/utilities/textprocessing/node/EmojiNode;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/utilities/textprocessing/node/EmojiNode;

    invoke-virtual {p1}, Lcom/discord/utilities/textprocessing/node/EmojiNode;->getContent()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/utilities/textprocessing/node/EmojiNode;->getContent()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/discord/utilities/textprocessing/node/EmojiNode;->width:I

    iget v1, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->width:I

    if-ne v0, v1, :cond_0

    iget p1, p1, Lcom/discord/utilities/textprocessing/node/EmojiNode;->height:I

    iget v0, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->height:I

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public final isRevealed()Z
    .locals 1

    .line 34
    iget-boolean v0, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->isRevealed:Z

    return v0
.end method

.method public final jumboify()V
    .locals 1

    .line 80
    iget v0, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->width:I

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->width:I

    .line 81
    iget v0, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->height:I

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->height:I

    const/4 v0, 0x1

    .line 82
    iput v0, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->verticalAlignment:I

    return-void
.end method

.method public final render(Landroid/text/SpannableStringBuilder;Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/text/SpannableStringBuilder;",
            "TT;)V"
        }
    .end annotation

    const-string v0, "builder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "renderContext"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-interface {p2}, Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 39
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 40
    invoke-virtual {p0}, Lcom/discord/utilities/textprocessing/node/EmojiNode;->getContent()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 43
    iget-object v2, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->urlProvider:Lkotlin/jvm/functions/Function3;

    invoke-interface {p2}, Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;->isAnimationEnabled()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 44
    iget v4, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->width:I

    invoke-static {v4}, Lcom/discord/utilities/icon/IconUtils;->getMediaProxySize(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {p2}, Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lkotlin/jvm/functions/Function3;->invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 45
    iget v3, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->width:I

    iget v4, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->height:I

    const/4 v5, 0x1

    .line 42
    invoke-static {v2, v3, v4, v5}, Lcom/discord/utilities/images/MGImages;->getImageRequest(Ljava/lang/String;IIZ)Lcom/facebook/imagepipeline/request/ImageRequestBuilder;

    move-result-object v2

    .line 47
    invoke-virtual {v2}, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->jN()Lcom/facebook/imagepipeline/request/b;

    move-result-object v2

    .line 49
    invoke-static {}, Lcom/facebook/drawee/backends/pipeline/c;->gk()Lcom/facebook/drawee/backends/pipeline/e;

    move-result-object v3

    .line 50
    invoke-virtual {v3, v2}, Lcom/facebook/drawee/backends/pipeline/e;->I(Ljava/lang/Object;)Lcom/facebook/drawee/controller/a;

    move-result-object v2

    check-cast v2, Lcom/facebook/drawee/backends/pipeline/e;

    .line 51
    invoke-virtual {p0}, Lcom/discord/utilities/textprocessing/node/EmojiNode;->isRevealed()Z

    move-result v3

    .line 1224
    iput-boolean v3, v2, Lcom/facebook/drawee/controller/a;->KQ:Z

    .line 51
    check-cast v2, Lcom/facebook/drawee/backends/pipeline/e;

    .line 52
    invoke-virtual {v2}, Lcom/facebook/drawee/backends/pipeline/e;->gH()Lcom/facebook/drawee/controller/AbstractDraweeController;

    move-result-object v2

    .line 55
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/drawee/generic/a;->a(Landroid/content/res/Resources;)Lcom/facebook/drawee/generic/a;

    move-result-object v3

    .line 56
    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    const/4 v6, 0x0

    invoke-direct {v4, v6}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    check-cast v4, Landroid/graphics/drawable/Drawable;

    .line 2176
    iput-object v4, v3, Lcom/facebook/drawee/generic/a;->Nj:Landroid/graphics/drawable/Drawable;

    .line 57
    sget-object v4, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->MS:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    invoke-virtual {v3, v4}, Lcom/facebook/drawee/generic/a;->b(Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;)Lcom/facebook/drawee/generic/a;

    move-result-object v3

    .line 59
    invoke-virtual {p0}, Lcom/discord/utilities/textprocessing/node/EmojiNode;->isRevealed()Z

    move-result v4

    if-nez v4, :cond_2

    .line 62
    instance-of v4, p2, Lcom/discord/utilities/textprocessing/node/SpoilerNode$RenderContext;

    if-nez v4, :cond_0

    const/4 p2, 0x0

    :cond_0
    check-cast p2, Lcom/discord/utilities/textprocessing/node/SpoilerNode$RenderContext;

    if-eqz p2, :cond_1

    invoke-interface {p2}, Lcom/discord/utilities/textprocessing/node/SpoilerNode$RenderContext;->getSpoilerColorRes()I

    move-result p2

    goto :goto_0

    :cond_1
    const p2, 0x7f04036f

    .line 63
    invoke-static {v0, p2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result p2

    .line 64
    :goto_0
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, p2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v0}, Lcom/facebook/drawee/generic/a;->i(Landroid/graphics/drawable/Drawable;)Lcom/facebook/drawee/generic/a;

    .line 67
    :cond_2
    move-object p2, p1

    check-cast p2, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    .line 69
    invoke-virtual {v3}, Lcom/facebook/drawee/generic/a;->gZ()Lcom/facebook/drawee/generic/GenericDraweeHierarchy;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/d/b;

    .line 70
    check-cast v2, Lcom/facebook/drawee/d/a;

    .line 72
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p1

    sub-int/2addr p1, v5

    .line 73
    iget v3, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->width:I

    .line 74
    iget v4, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->height:I

    .line 76
    iget v7, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->verticalAlignment:I

    .line 3152
    invoke-static {v0}, Lcom/facebook/drawee/view/b;->a(Lcom/facebook/drawee/d/b;)Lcom/facebook/drawee/view/b;

    move-result-object v0

    .line 3153
    invoke-virtual {v0, v2}, Lcom/facebook/drawee/view/b;->setController(Lcom/facebook/drawee/d/a;)V

    .line 4098
    invoke-virtual {p2}, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;->length()I

    move-result v2

    if-ge p1, v2, :cond_6

    .line 4104
    invoke-virtual {v0}, Lcom/facebook/drawee/view/b;->getTopLevelDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 4106
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 4107
    invoke-virtual {v2, v6, v6, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 4109
    :cond_3
    iget-object v3, p2, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;->NP:Lcom/facebook/drawee/span/DraweeSpanStringBuilder$a;

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 4111
    :cond_4
    new-instance v2, Lcom/facebook/drawee/span/a;

    invoke-direct {v2, v0, v7}, Lcom/facebook/drawee/span/a;-><init>(Lcom/facebook/drawee/view/b;I)V

    .line 4206
    iget-object v0, v0, Lcom/facebook/drawee/view/b;->Ob:Lcom/facebook/drawee/d/a;

    .line 4113
    instance-of v3, v0, Lcom/facebook/drawee/controller/AbstractDraweeController;

    if-eqz v3, :cond_5

    .line 4114
    check-cast v0, Lcom/facebook/drawee/controller/AbstractDraweeController;

    new-instance v3, Lcom/facebook/drawee/span/DraweeSpanStringBuilder$b;

    invoke-direct {v3, p2, v2, v6, v4}, Lcom/facebook/drawee/span/DraweeSpanStringBuilder$b;-><init>(Lcom/facebook/drawee/span/DraweeSpanStringBuilder;Lcom/facebook/drawee/span/a;ZI)V

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/controller/AbstractDraweeController;->a(Lcom/facebook/drawee/controller/ControllerListener;)V

    .line 4117
    :cond_5
    iget-object v0, p2, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;->NO:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/2addr p1, v5

    const/16 v0, 0x21

    .line 4118
    invoke-virtual {p2, v2, v1, p1, v0}, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_6
    return-void
.end method

.method public final bridge synthetic render(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;)V
    .locals 0

    .line 25
    check-cast p2, Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;

    invoke-virtual {p0, p1, p2}, Lcom/discord/utilities/textprocessing/node/EmojiNode;->render(Landroid/text/SpannableStringBuilder;Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;)V

    return-void
.end method

.method public final setRevealed(Z)V
    .locals 0

    .line 34
    iput-boolean p1, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->isRevealed:Z

    return-void
.end method
