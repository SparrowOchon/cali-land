.class public final Lcom/discord/utilities/nitro/BoostUtils;
.super Ljava/lang/Object;
.source "BoostUtils.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/nitro/BoostUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8
    new-instance v0, Lcom/discord/utilities/nitro/BoostUtils;

    invoke-direct {v0}, Lcom/discord/utilities/nitro/BoostUtils;-><init>()V

    sput-object v0, Lcom/discord/utilities/nitro/BoostUtils;->INSTANCE:Lcom/discord/utilities/nitro/BoostUtils;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final getCurrentTierSubs(I)I
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x2

    if-eq p1, v0, :cond_2

    if-eq p1, v1, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    const/16 p1, 0x14

    return p1

    :cond_1
    const/16 p1, 0xa

    return p1

    :cond_2
    return v1
.end method

.method private final getNextTierSubs(I)I
    .locals 2

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    return v0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    const/16 p1, 0x14

    return p1

    :cond_2
    const/16 p1, 0xa

    return p1
.end method


# virtual methods
.method public final calculatePercentToNextTier(II)I
    .locals 1

    const/4 v0, 0x3

    if-lt p1, v0, :cond_0

    const/16 p1, 0x64

    return p1

    .line 32
    :cond_0
    invoke-direct {p0, p1}, Lcom/discord/utilities/nitro/BoostUtils;->getNextTierSubs(I)I

    move-result v0

    .line 33
    invoke-direct {p0, p1}, Lcom/discord/utilities/nitro/BoostUtils;->getCurrentTierSubs(I)I

    move-result p1

    sub-int/2addr p2, p1

    int-to-float p1, p2

    int-to-float p2, v0

    div-float/2addr p1, p2

    const/high16 p2, 0x42c80000    # 100.0f

    mul-float p1, p1, p2

    .line 35
    invoke-static {p1}, Lkotlin/e/a;->C(F)I

    move-result p1

    return p1
.end method

.method public final calculateTotalBoostCount(Ljava/util/List;)I
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelSubscription;",
            ">;)I"
        }
    .end annotation

    const-string v0, "subscriptions"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    check-cast p1, Ljava/lang/Iterable;

    .line 82
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 83
    check-cast v2, Lcom/discord/models/domain/ModelSubscription;

    .line 45
    invoke-virtual {v2}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object v3

    sget-object v4, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_MONTH_TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    if-eq v3, v4, :cond_1

    .line 46
    invoke-virtual {v2}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object v3

    sget-object v4, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_YEAR_TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    if-eq v3, v4, :cond_1

    .line 47
    invoke-virtual {v2}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object v3

    sget-object v4, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_MONTH_LEGACY:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    if-eq v3, v4, :cond_1

    .line 48
    invoke-virtual {v2}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object v3

    sget-object v4, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_YEAR_LEGACY:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    if-ne v3, v4, :cond_0

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    goto :goto_2

    :cond_1
    :goto_1
    const/4 v3, 0x1

    .line 52
    :goto_2
    invoke-virtual {v2}, Lcom/discord/models/domain/ModelSubscription;->getAdditionalPlanIds()Ljava/util/Map;

    move-result-object v4

    if-eqz v4, :cond_2

    sget-object v5, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_GUILD_MONTH:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getPlanId()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto :goto_3

    :cond_2
    const/4 v4, 0x0

    :goto_3
    add-int/2addr v3, v4

    .line 54
    invoke-virtual {v2}, Lcom/discord/models/domain/ModelSubscription;->getAdditionalPlanIds()Ljava/util/Map;

    move-result-object v2

    if-eqz v2, :cond_3

    sget-object v4, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_GUILD_YEAR:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getPlanId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_4

    :cond_3
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v3, v2

    add-int/2addr v1, v3

    goto :goto_0

    :cond_4
    return v1
.end method

.method public final calculateTotalProgress(II)I
    .locals 2

    const/4 v0, 0x3

    if-lt p1, v0, :cond_0

    const/16 p1, 0x64

    return p1

    .line 18
    :cond_0
    invoke-direct {p0, p1}, Lcom/discord/utilities/nitro/BoostUtils;->getNextTierSubs(I)I

    move-result v0

    .line 19
    invoke-direct {p0, p1}, Lcom/discord/utilities/nitro/BoostUtils;->getCurrentTierSubs(I)I

    move-result v1

    sub-int/2addr p2, v1

    int-to-float p2, p2

    sub-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr p2, v0

    int-to-float p1, p1

    const v0, 0x42053333    # 33.3f

    mul-float p1, p1, v0

    mul-float p2, p2, v0

    add-float/2addr p1, p2

    .line 24
    invoke-static {p1}, Lkotlin/e/a;->C(F)I

    move-result p1

    return p1
.end method
