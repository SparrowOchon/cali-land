.class public final Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey$Companion;
.super Ljava/lang/Object;
.source "SurveyUtils.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 111
    invoke-direct {p0}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromBucket(Lcom/discord/models/domain/ModelUser;I)Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;
    .locals 2

    const-string v0, "meUser"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eq p2, v1, :cond_0

    move-object p2, v0

    goto :goto_0

    :cond_0
    const-string p2, "PCP9PW2"

    :goto_0
    if-eqz p2, :cond_1

    .line 123
    new-instance v0, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;

    invoke-direct {v0, p1, p2}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;-><init>(Lcom/discord/models/domain/ModelUser;Ljava/lang/String;)V

    :cond_1
    return-object v0
.end method
