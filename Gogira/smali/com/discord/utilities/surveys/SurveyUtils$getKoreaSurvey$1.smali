.class final Lcom/discord/utilities/surveys/SurveyUtils$getKoreaSurvey$1;
.super Ljava/lang/Object;
.source "SurveyUtils.kt"

# interfaces
.implements Lrx/functions/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/surveys/SurveyUtils;->getKoreaSurvey(Lcom/discord/models/domain/ModelUser;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/b<",
        "TT;",
        "Lrx/Observable<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic $meUser:Lcom/discord/models/domain/ModelUser;


# direct methods
.method constructor <init>(Lcom/discord/models/domain/ModelUser;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/surveys/SurveyUtils$getKoreaSurvey$1;->$meUser:Lcom/discord/models/domain/ModelUser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 12
    check-cast p1, Lcom/discord/models/domain/ModelExperiment;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/surveys/SurveyUtils$getKoreaSurvey$1;->call(Lcom/discord/models/domain/ModelExperiment;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/models/domain/ModelExperiment;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelExperiment;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 52
    sget-object v0, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;->Companion:Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey$Companion;

    iget-object v1, p0, Lcom/discord/utilities/surveys/SurveyUtils$getKoreaSurvey$1;->$meUser:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelExperiment;->getBucket()I

    move-result p1

    invoke-virtual {v0, v1, p1}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey$Companion;->createFromBucket(Lcom/discord/models/domain/ModelUser;I)Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;

    move-result-object p1

    invoke-static {p1}, Lrx/Observable;->bI(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    .line 55
    invoke-static {p1}, Lrx/Observable;->bI(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
