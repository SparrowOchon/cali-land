.class public final Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;
.super Lcom/discord/utilities/surveys/SurveyUtils$Survey;
.source "SurveyUtils.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/surveys/SurveyUtils$Survey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "KoreaSurvey"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey$Companion;


# instance fields
.field private final meUser:Lcom/discord/models/domain/ModelUser;

.field private final surveyId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;->Companion:Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/models/domain/ModelUser;Ljava/lang/String;)V
    .locals 2

    const-string v0, "meUser"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "surveyId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "NOTICE_KEY_KOREA_SURVEY"

    const/4 v1, 0x0

    .line 107
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/discord/utilities/surveys/SurveyUtils$Survey;-><init>(Lcom/discord/models/domain/ModelUser;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;->meUser:Lcom/discord/models/domain/ModelUser;

    iput-object p2, p0, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;->surveyId:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;Lcom/discord/models/domain/ModelUser;Ljava/lang/String;ILjava/lang/Object;)Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    invoke-virtual {p0}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;->getMeUser()Lcom/discord/models/domain/ModelUser;

    move-result-object p1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    invoke-virtual {p0}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;->getSurveyId()Ljava/lang/String;

    move-result-object p2

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;->copy(Lcom/discord/models/domain/ModelUser;Ljava/lang/String;)Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelUser;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;->getMeUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;->getSurveyId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelUser;Ljava/lang/String;)Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;
    .locals 1

    const-string v0, "meUser"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "surveyId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;

    invoke-direct {v0, p1, p2}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;-><init>(Lcom/discord/models/domain/ModelUser;Ljava/lang/String;)V

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;

    invoke-virtual {p0}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;->getMeUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;->getMeUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;->getSurveyId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;->getSurveyId()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getMeUser()Lcom/discord/models/domain/ModelUser;
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;->meUser:Lcom/discord/models/domain/ModelUser;

    return-object v0
.end method

.method public final getSurveyId()Ljava/lang/String;
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;->surveyId:Ljava/lang/String;

    return-object v0
.end method

.method public final getSurveyUrl()Ljava/lang/String;
    .locals 3

    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://ko.surveymonkey.com/r/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;->getSurveyId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "?id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;->getMeUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;->getMeUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;->getSurveyId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "KoreaSurvey(meUser="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;->getMeUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", surveyId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$KoreaSurvey;->getSurveyId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
