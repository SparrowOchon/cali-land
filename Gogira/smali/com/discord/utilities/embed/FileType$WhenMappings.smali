.class public final synthetic Lcom/discord/utilities/embed/FileType$WhenMappings;
.super Ljava/lang/Object;


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/discord/utilities/embed/FileType;->values()[Lcom/discord/utilities/embed/FileType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/discord/utilities/embed/FileType$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/discord/utilities/embed/FileType;->VIDEO:Lcom/discord/utilities/embed/FileType;

    invoke-virtual {v1}, Lcom/discord/utilities/embed/FileType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/discord/utilities/embed/FileType$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/discord/utilities/embed/FileType;->ACROBAT:Lcom/discord/utilities/embed/FileType;

    invoke-virtual {v1}, Lcom/discord/utilities/embed/FileType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1

    sget-object v0, Lcom/discord/utilities/embed/FileType$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/discord/utilities/embed/FileType;->AE:Lcom/discord/utilities/embed/FileType;

    invoke-virtual {v1}, Lcom/discord/utilities/embed/FileType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1

    sget-object v0, Lcom/discord/utilities/embed/FileType$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/discord/utilities/embed/FileType;->SKETCH:Lcom/discord/utilities/embed/FileType;

    invoke-virtual {v1}, Lcom/discord/utilities/embed/FileType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    sget-object v0, Lcom/discord/utilities/embed/FileType$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/discord/utilities/embed/FileType;->AI:Lcom/discord/utilities/embed/FileType;

    invoke-virtual {v1}, Lcom/discord/utilities/embed/FileType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1

    sget-object v0, Lcom/discord/utilities/embed/FileType$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/discord/utilities/embed/FileType;->ARCHIVE:Lcom/discord/utilities/embed/FileType;

    invoke-virtual {v1}, Lcom/discord/utilities/embed/FileType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1

    sget-object v0, Lcom/discord/utilities/embed/FileType$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/discord/utilities/embed/FileType;->CODE:Lcom/discord/utilities/embed/FileType;

    invoke-virtual {v1}, Lcom/discord/utilities/embed/FileType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1

    sget-object v0, Lcom/discord/utilities/embed/FileType$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/discord/utilities/embed/FileType;->DOCUMENT:Lcom/discord/utilities/embed/FileType;

    invoke-virtual {v1}, Lcom/discord/utilities/embed/FileType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1

    sget-object v0, Lcom/discord/utilities/embed/FileType$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/discord/utilities/embed/FileType;->SPREADSHEET:Lcom/discord/utilities/embed/FileType;

    invoke-virtual {v1}, Lcom/discord/utilities/embed/FileType;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1

    sget-object v0, Lcom/discord/utilities/embed/FileType$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/discord/utilities/embed/FileType;->WEBCODE:Lcom/discord/utilities/embed/FileType;

    invoke-virtual {v1}, Lcom/discord/utilities/embed/FileType;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1

    return-void
.end method
