.class final Lcom/discord/views/OverlayMenuView$n;
.super Lkotlin/jvm/internal/l;
.source "OverlayMenuView.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/views/OverlayMenuView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/l;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Pair<",
        "+",
        "Ljava/lang/Boolean;",
        "+",
        "Lcom/discord/models/domain/ModelVoice$OutputMode;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/discord/views/OverlayMenuView;


# direct methods
.method constructor <init>(Lcom/discord/views/OverlayMenuView;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/views/OverlayMenuView$n;->this$0:Lcom/discord/views/OverlayMenuView;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/l;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .line 36
    check-cast p1, Lkotlin/Pair;

    .line 1000
    iget-object v0, p1, Lkotlin/Pair;->first:Ljava/lang/Object;

    .line 36
    check-cast v0, Ljava/lang/Boolean;

    .line 2000
    iget-object p1, p1, Lkotlin/Pair;->second:Ljava/lang/Object;

    .line 36
    check-cast p1, Lcom/discord/models/domain/ModelVoice$OutputMode;

    .line 2193
    iget-object v1, p0, Lcom/discord/views/OverlayMenuView$n;->this$0:Lcom/discord/views/OverlayMenuView;

    invoke-static {v1}, Lcom/discord/views/OverlayMenuView;->e(Lcom/discord/views/OverlayMenuView;)Landroid/widget/ImageView;

    move-result-object v1

    const-string v2, "isMuted"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setActivated(Z)V

    .line 2194
    iget-object v0, p0, Lcom/discord/views/OverlayMenuView$n;->this$0:Lcom/discord/views/OverlayMenuView;

    invoke-static {v0}, Lcom/discord/views/OverlayMenuView;->f(Lcom/discord/views/OverlayMenuView;)Landroid/widget/ImageView;

    move-result-object v0

    sget-object v1, Lcom/discord/models/domain/ModelVoice$OutputMode;->SPEAKER:Lcom/discord/models/domain/ModelVoice$OutputMode;

    if-ne p1, v1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setActivated(Z)V

    .line 36
    sget-object p1, Lkotlin/Unit;->bjS:Lkotlin/Unit;

    return-object p1
.end method
