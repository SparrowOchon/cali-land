.class public final Lcom/discord/app/DiscordConnectService$a;
.super Ljava/lang/Object;
.source "DiscordConnectService.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/app/DiscordConnectService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(B)V
    .locals 0

    .line 184
    invoke-direct {p0}, Lcom/discord/app/DiscordConnectService$a;-><init>()V

    return-void
.end method

.method static log(Ljava/lang/String;)V
    .locals 6

    .line 198
    sget-object v0, Lcom/discord/app/AppLog;->uB:Lcom/discord/app/AppLog;

    const-class v1, Lcom/discord/app/DiscordConnectService;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "DiscordConnectService::class.java.simpleName"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v2, p0

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/logging/Logger;->i$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    return-void
.end method
