.class public Lcom/discord/app/AppActivity;
.super Lcom/discord/app/c;
.source "AppActivity.kt"

# interfaces
.implements Lcom/discord/app/AppComponent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/app/AppActivity$Main;,
        Lcom/discord/app/AppActivity$AppAction;,
        Lcom/discord/app/AppActivity$b;,
        Lcom/discord/app/AppActivity$a;
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field static tT:Z

.field private static final tU:Landroid/content/Intent;

.field private static tV:Z

.field public static final tW:Lcom/discord/app/AppActivity$a;


# instance fields
.field private final paused:Lrx/subjects/Subject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/Subject<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field final tN:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Landroidx/fragment/app/Fragment;",
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/content/Intent;",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation
.end field

.field private tO:I

.field private tP:Ljava/lang/String;

.field private final tQ:Lkotlin/Lazy;

.field private final tR:Lkotlin/Lazy;

.field private tS:Landroid/content/Intent;

.field public toolbar:Landroidx/appcompat/widget/Toolbar;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-class v0, Lcom/discord/app/AppActivity;

    const/4 v1, 0x2

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "userSettings"

    const-string v5, "getUserSettings()Lcom/discord/stores/StoreUserSettings;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v0

    const-string v4, "screen"

    const-string v5, "getScreen()Ljava/lang/Class;"

    invoke-direct {v2, v0, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aput-object v0, v1, v2

    sput-object v1, Lcom/discord/app/AppActivity;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/app/AppActivity$a;

    invoke-direct {v0, v3}, Lcom/discord/app/AppActivity$a;-><init>(B)V

    sput-object v0, Lcom/discord/app/AppActivity;->tW:Lcom/discord/app/AppActivity$a;

    .line 534
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    sput-object v0, Lcom/discord/app/AppActivity;->tU:Landroid/content/Intent;

    .line 545
    sput-boolean v2, Lcom/discord/app/AppActivity;->tV:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 51
    invoke-direct {p0}, Lcom/discord/app/c;-><init>()V

    .line 53
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/discord/app/AppActivity;->tN:Ljava/util/LinkedHashMap;

    const/4 v0, -0x1

    .line 60
    iput v0, p0, Lcom/discord/app/AppActivity;->tO:I

    const-string v0, ""

    .line 61
    iput-object v0, p0, Lcom/discord/app/AppActivity;->tP:Ljava/lang/String;

    .line 63
    invoke-static {}, Lrx/subjects/PublishSubject;->Lt()Lrx/subjects/PublishSubject;

    move-result-object v0

    const-string v1, "PublishSubject.create()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lrx/subjects/Subject;

    iput-object v0, p0, Lcom/discord/app/AppActivity;->paused:Lrx/subjects/Subject;

    .line 84
    sget-object v0, Lkotlin/j;->bjN:Lkotlin/j;

    sget-object v1, Lcom/discord/app/AppActivity$m;->uf:Lcom/discord/app/AppActivity$m;

    check-cast v1, Lkotlin/jvm/functions/Function0;

    const-string v2, "mode"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "initializer"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3034
    sget-object v2, Lkotlin/g;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lkotlin/j;->ordinal()I

    move-result v0

    aget v0, v2, v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    .line 3037
    new-instance v0, Lkotlin/u;

    invoke-direct {v0, v1}, Lkotlin/u;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lkotlin/Lazy;

    goto :goto_0

    :cond_0
    new-instance v0, Lkotlin/k;

    invoke-direct {v0}, Lkotlin/k;-><init>()V

    throw v0

    .line 3036
    :cond_1
    new-instance v0, Lkotlin/o;

    invoke-direct {v0, v1}, Lkotlin/o;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lkotlin/Lazy;

    goto :goto_0

    .line 3035
    :cond_2
    new-instance v0, Lkotlin/p;

    invoke-direct {v0, v1}, Lkotlin/p;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lkotlin/Lazy;

    .line 84
    :goto_0
    iput-object v0, p0, Lcom/discord/app/AppActivity;->tQ:Lkotlin/Lazy;

    .line 89
    new-instance v0, Lcom/discord/app/AppActivity$i;

    invoke-direct {v0, p0}, Lcom/discord/app/AppActivity$i;-><init>(Lcom/discord/app/AppActivity;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {v0}, Lkotlin/f;->b(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/app/AppActivity;->tR:Lkotlin/Lazy;

    .line 106
    sget-object v0, Lcom/discord/app/AppActivity;->tU:Landroid/content/Intent;

    iput-object v0, p0, Lcom/discord/app/AppActivity;->tS:Landroid/content/Intent;

    return-void
.end method

.method public static final synthetic a(Lcom/discord/app/AppActivity;)Lcom/discord/stores/StoreUserSettings;
    .locals 0

    .line 51
    invoke-direct {p0}, Lcom/discord/app/AppActivity;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object p0

    return-object p0
.end method

.method private final a(Landroid/content/Intent;)V
    .locals 2

    .line 453
    sget-object v0, Lcom/discord/utilities/intent/IntentUtils;->INSTANCE:Lcom/discord/utilities/intent/IntentUtils;

    move-object v1, p0

    check-cast v1, Landroid/content/Context;

    invoke-virtual {v0, p1, v1}, Lcom/discord/utilities/intent/IntentUtils;->consumeExternalRoutingIntent(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result p1

    sput-boolean p1, Lcom/discord/app/AppActivity;->tT:Z

    return-void
.end method

.method public static final synthetic a(Lcom/discord/app/AppActivity;I)V
    .locals 0

    .line 51
    iput p1, p0, Lcom/discord/app/AppActivity;->tO:I

    return-void
.end method

.method public static final synthetic a(Lcom/discord/app/AppActivity;Landroid/content/Intent;)V
    .locals 0

    .line 51
    invoke-direct {p0, p1}, Lcom/discord/app/AppActivity;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public static final synthetic a(Lcom/discord/app/AppActivity;Ljava/lang/String;)V
    .locals 1

    .line 3438
    sget-object v0, Lcom/discord/app/f;->uP:Lcom/discord/app/f;

    invoke-static {}, Lcom/discord/app/f;->dw()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/discord/app/AppActivity;->i(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "light"

    .line 3439
    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const p1, 0x7f13001e

    goto :goto_0

    :cond_0
    const-string v0, "dark"

    .line 3440
    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "pureEvil"

    .line 3441
    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const p1, 0x7f130019

    goto :goto_0

    :cond_1
    const p1, 0x7f130016

    .line 3445
    :goto_0
    invoke-virtual {p0, p1}, Lcom/discord/app/AppActivity;->setTheme(I)V

    return-void
.end method

.method public static final synthetic a(Lcom/discord/app/AppActivity;Lcom/discord/app/AppActivity$b;)Z
    .locals 5

    .line 4498
    iget-object v0, p1, Lcom/discord/app/AppActivity$b;->tY:Ljava/lang/String;

    .line 4405
    invoke-static {v0}, Lcom/discord/models/domain/ModelUserSettings;->getLocaleObject(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v0

    const-string v1, "ModelUserSettings.getLoc\u2026bject(model.localeString)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/discord/app/AppActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 5498
    iget-object p1, p1, Lcom/discord/app/AppActivity$b;->tY:Ljava/lang/String;

    .line 4406
    invoke-direct {p0, p1, v1}, Lcom/discord/app/AppActivity;->b(Ljava/lang/String;Z)V

    return v1

    .line 6497
    :cond_0
    iget-object v0, p1, Lcom/discord/app/AppActivity$b;->tX:Ljava/lang/String;

    .line 7417
    sget-object v2, Lcom/discord/app/f;->uP:Lcom/discord/app/f;

    invoke-static {}, Lcom/discord/app/f;->dw()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/discord/app/AppActivity;->i(Ljava/util/List;)Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 7419
    :cond_1
    new-instance v2, Lcom/discord/app/AppActivity$k;

    invoke-direct {v2, p0}, Lcom/discord/app/AppActivity$k;-><init>(Lcom/discord/app/AppActivity;)V

    const v4, 0x7f04038e

    .line 7422
    invoke-virtual {v2, v4, v1}, Lcom/discord/app/AppActivity$k;->b(IZ)Landroid/util/TypedValue;

    move-result-object v2

    iget-object v2, v2, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    .line 7424
    invoke-static {v2, v0}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v1

    :goto_0
    if-nez v0, :cond_6

    .line 7499
    iget p1, p1, Lcom/discord/app/AppActivity$b;->fontScale:I

    .line 8349
    sget-object v0, Lcom/discord/utilities/font/FontUtils;->INSTANCE:Lcom/discord/utilities/font/FontUtils;

    invoke-virtual {p0}, Lcom/discord/app/AppActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v4, "contentResolver"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/discord/utilities/font/FontUtils;->getSystemFontScaleInt(Landroid/content/ContentResolver;)I

    move-result v0

    const/4 v2, -0x1

    if-ne p1, v2, :cond_2

    .line 8351
    iget v4, p0, Lcom/discord/app/AppActivity;->tO:I

    if-ne v4, v0, :cond_3

    :cond_2
    if-eq p1, v2, :cond_4

    iget p0, p0, Lcom/discord/app/AppActivity;->tO:I

    if-eq p0, p1, :cond_4

    :cond_3
    const/4 p0, 0x1

    goto :goto_1

    :cond_4
    const/4 p0, 0x0

    :goto_1
    if-eqz p0, :cond_5

    goto :goto_2

    :cond_5
    return v3

    :cond_6
    :goto_2
    return v1
.end method

.method private final a(Ljava/util/Locale;)Z
    .locals 6

    .line 376
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "resources"

    const/16 v4, 0x18

    if-lt v0, v4, :cond_2

    .line 377
    invoke-virtual {p0}, Lcom/discord/app/AppActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v3}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    const-string v4, "resources.configuration"

    invoke-static {v0, v4}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/res/Configuration;->getLocales()Landroid/os/LocaleList;

    move-result-object v0

    const-string v5, "resources.configuration.locales"

    invoke-static {v0, v5}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/LocaleList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/discord/app/AppActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v3}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-static {v0, v4}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/res/Configuration;->getLocales()Landroid/os/LocaleList;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/LocaleList;->get(I)Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0, p1}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/2addr p1, v2

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    return v1

    :cond_1
    :goto_0
    return v2

    .line 380
    :cond_2
    invoke-virtual {p0}, Lcom/discord/app/AppActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v3}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/discord/app/AppActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v3}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/2addr p1, v2

    if-eqz p1, :cond_3

    goto :goto_1

    :cond_3
    return v1

    :cond_4
    :goto_1
    return v2
.end method

.method public static final synthetic b(Lcom/discord/app/AppActivity;Landroid/content/Intent;)Lcom/discord/app/j$a;
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const-string v1, "transition"

    .line 3462
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v0

    :goto_0
    instance-of v1, p1, Lcom/discord/app/j$c;

    if-nez v1, :cond_1

    move-object p1, v0

    :cond_1
    check-cast p1, Lcom/discord/app/j$c;

    if-eqz p1, :cond_2

    move-object p0, p1

    goto :goto_1

    .line 3465
    :cond_2
    sget-object p1, Lcom/discord/app/f;->uP:Lcom/discord/app/f;

    invoke-static {}, Lcom/discord/app/f;->du()Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/discord/app/AppActivity;->i(Ljava/util/List;)Z

    move-result p0

    if-eqz p0, :cond_3

    sget-object p0, Lcom/discord/app/j$c;->vS:Lcom/discord/app/j$c;

    goto :goto_1

    :cond_3
    move-object p0, v0

    :goto_1
    if-eqz p0, :cond_4

    .line 4058
    iget-object p0, p0, Lcom/discord/app/j$c;->animations:Lcom/discord/app/j$a;

    return-object p0

    :cond_4
    return-object v0
.end method

.method public static final synthetic b(Lcom/discord/app/AppActivity;)V
    .locals 1

    const/4 v0, 0x1

    .line 51
    invoke-direct {p0, v0}, Lcom/discord/app/AppActivity;->n(Z)V

    return-void
.end method

.method public static final synthetic b(Lcom/discord/app/AppActivity;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 51
    invoke-direct {p0, p1, v0}, Lcom/discord/app/AppActivity;->b(Ljava/lang/String;Z)V

    return-void
.end method

.method private final b(Ljava/lang/String;Z)V
    .locals 3

    .line 359
    invoke-static {p1}, Lcom/discord/models/domain/ModelUserSettings;->getLocaleObject(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object p1

    const-string v0, "locale"

    .line 361
    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/app/AppActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2391
    invoke-static {p1}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    .line 2393
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    const-string v2, "resources"

    if-lt v0, v1, :cond_0

    .line 2394
    invoke-virtual {p0}, Lcom/discord/app/AppActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Configuration;->setLocale(Ljava/util/Locale;)V

    goto :goto_0

    .line 2397
    :cond_0
    invoke-virtual {p0}, Lcom/discord/app/AppActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iput-object p1, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 2401
    :goto_0
    invoke-virtual {p0}, Lcom/discord/app/AppActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p0}, Lcom/discord/app/AppActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/app/AppActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    if-eqz p2, :cond_1

    const/4 p1, 0x1

    .line 364
    invoke-direct {p0, p1}, Lcom/discord/app/AppActivity;->n(Z)V

    :cond_1
    return-void
.end method

.method public static final synthetic dn()Z
    .locals 1

    .line 51
    sget-boolean v0, Lcom/discord/app/AppActivity;->tV:Z

    return v0
.end method

.method public static final synthetic do()V
    .locals 1

    const/4 v0, 0x0

    .line 51
    sput-boolean v0, Lcom/discord/app/AppActivity;->tV:Z

    return-void
.end method

.method private final getUserSettings()Lcom/discord/stores/StoreUserSettings;
    .locals 1

    iget-object v0, p0, Lcom/discord/app/AppActivity;->tQ:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StoreUserSettings;

    return-object v0
.end method

.method private final n(Z)V
    .locals 2

    .line 325
    invoke-virtual {p0}, Lcom/discord/app/AppActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    sget-object v0, Lcom/discord/app/j$c;->vQ:Lcom/discord/app/j$c;

    check-cast v0, Ljava/io/Serializable;

    const-string v1, "transition"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 326
    move-object p1, p0

    check-cast p1, Landroid/content/Context;

    invoke-virtual {p0}, Lcom/discord/app/AppActivity;->getScreen()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/app/AppActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/discord/app/f;->a(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;)V

    .line 327
    invoke-virtual {p0}, Lcom/discord/app/AppActivity;->finish()V

    return-void
.end method


# virtual methods
.method public final a(Landroidx/appcompat/widget/Toolbar;)V
    .locals 2

    .line 67
    iput-object p1, p0, Lcom/discord/app/AppActivity;->toolbar:Landroidx/appcompat/widget/Toolbar;

    .line 69
    invoke-virtual {p0}, Lcom/discord/app/AppActivity;->dm()Lcom/discord/views/ToolbarTitleLayout;

    move-result-object p1

    if-nez p1, :cond_1

    .line 70
    iget-object p1, p0, Lcom/discord/app/AppActivity;->toolbar:Landroidx/appcompat/widget/Toolbar;

    if-eqz p1, :cond_0

    new-instance v0, Lcom/discord/views/ToolbarTitleLayout;

    move-object v1, p0

    check-cast v1, Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/discord/views/ToolbarTitleLayout;-><init>(Landroid/content/Context;)V

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/Toolbar;->addView(Landroid/view/View;)V

    .line 71
    :cond_0
    iget-object p1, p0, Lcom/discord/app/AppActivity;->toolbar:Landroidx/appcompat/widget/Toolbar;

    if-eqz p1, :cond_1

    new-instance v0, Lcom/discord/app/AppActivity$l;

    invoke-direct {v0, p0}, Lcom/discord/app/AppActivity$l;-><init>(Lcom/discord/app/AppActivity;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    return-void
.end method

.method public final a(Lkotlin/reflect/b;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/b<",
            "+",
            "Lcom/discord/app/AppComponent;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 275
    invoke-static {p1}, Lkotlin/jvm/a;->b(Lkotlin/reflect/b;)Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p0}, Lcom/discord/app/AppActivity;->getScreen()Ljava/lang/Class;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 3

    .line 267
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-gt v0, v1, :cond_0

    goto :goto_0

    :cond_0
    if-nez p1, :cond_1

    .line 270
    move-object p1, p0

    check-cast p1, Landroid/content/Context;

    .line 2331
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "oldContext.resources"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 2332
    sget-object v1, Lcom/discord/utilities/font/FontUtils;->INSTANCE:Lcom/discord/utilities/font/FontUtils;

    invoke-virtual {v1, p1}, Lcom/discord/utilities/font/FontUtils;->getTargetFontScaleFloat(Landroid/content/Context;)F

    move-result v1

    .line 2333
    iput v1, v0, Landroid/content/res/Configuration;->fontScale:F

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float v1, v1, v2

    .line 2335
    invoke-static {v1}, Lkotlin/e/a;->C(F)I

    move-result v1

    iput v1, p0, Lcom/discord/app/AppActivity;->tO:I

    .line 2337
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_2

    .line 2338
    invoke-virtual {p1, v0}, Landroid/content/Context;->createConfigurationContext(Landroid/content/res/Configuration;)Landroid/content/Context;

    move-result-object p1

    const-string v0, "oldContext.createConfigurationContext(config)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    .line 270
    :cond_2
    :goto_0
    invoke-super {p0, p1}, Lcom/discord/app/c;->attachBaseContext(Landroid/content/Context;)V

    return-void
.end method

.method final dm()Lcom/discord/views/ToolbarTitleLayout;
    .locals 3

    .line 82
    iget-object v0, p0, Lcom/discord/app/AppActivity;->toolbar:Landroidx/appcompat/widget/Toolbar;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroidx/appcompat/widget/Toolbar;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    instance-of v2, v0, Lcom/discord/views/ToolbarTitleLayout;

    if-nez v2, :cond_1

    move-object v0, v1

    :cond_1
    check-cast v0, Lcom/discord/views/ToolbarTitleLayout;

    return-object v0
.end method

.method public final getMostRecentIntent()Landroid/content/Intent;
    .locals 2

    .line 107
    iget-object v0, p0, Lcom/discord/app/AppActivity;->tS:Landroid/content/Intent;

    sget-object v1, Lcom/discord/app/AppActivity;->tU:Landroid/content/Intent;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/discord/app/AppActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/discord/app/AppActivity;->tU:Landroid/content/Intent;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    :cond_0
    return-object v0
.end method

.method public getPaused()Lrx/subjects/Subject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/Subject<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 63
    iget-object v0, p0, Lcom/discord/app/AppActivity;->paused:Lrx/subjects/Subject;

    return-object v0
.end method

.method protected getScreen()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/discord/app/AppComponent;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/app/AppActivity;->tR:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    return-object v0
.end method

.method public final hideKeyboard(Landroid/view/View;)V
    .locals 2

    .line 321
    move-object v0, p0

    check-cast v0, Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Lcom/discord/utilities/keyboard/Keyboard;->setKeyboardOpen(Landroid/app/Activity;ZLandroid/view/View;)V

    return-void
.end method

.method public final i(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lkotlin/reflect/b<",
            "+",
            "Lcom/discord/app/AppComponent;",
            ">;>;)Z"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 274
    check-cast p1, Ljava/lang/Iterable;

    .line 550
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lkotlin/reflect/b;

    .line 274
    invoke-static {v1}, Lkotlin/jvm/a;->b(Lkotlin/reflect/b;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0}, Lcom/discord/app/AppActivity;->getScreen()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    const/4 p1, 0x1

    return p1

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .line 143
    new-instance v0, Lcom/discord/app/AppActivity$c;

    invoke-direct {v0, p0}, Lcom/discord/app/AppActivity$c;-><init>(Lcom/discord/app/AppActivity;)V

    .line 160
    new-instance v1, Lcom/discord/app/AppActivity$d;

    invoke-direct {v1, p0}, Lcom/discord/app/AppActivity$d;-><init>(Lcom/discord/app/AppActivity;)V

    .line 176
    new-instance v2, Lcom/discord/app/AppActivity$e;

    invoke-direct {v2, p0}, Lcom/discord/app/AppActivity$e;-><init>(Lcom/discord/app/AppActivity;)V

    .line 188
    new-instance v3, Lcom/discord/app/AppActivity$f;

    invoke-direct {v3, p0}, Lcom/discord/app/AppActivity$f;-><init>(Lcom/discord/app/AppActivity;)V

    .line 199
    new-instance v4, Lcom/discord/app/AppActivity$g;

    invoke-direct {v4, p0}, Lcom/discord/app/AppActivity$g;-><init>(Lcom/discord/app/AppActivity;)V

    .line 213
    :try_start_0
    invoke-virtual {v0}, Lcom/discord/app/AppActivity$c;->invoke()V

    .line 214
    invoke-virtual {v1}, Lcom/discord/app/AppActivity$d;->invoke()V

    .line 216
    invoke-super {p0, p1}, Lcom/discord/app/c;->onCreate(Landroid/os/Bundle;)V

    .line 218
    invoke-virtual {v2}, Lcom/discord/app/AppActivity$e;->invoke()V

    .line 219
    invoke-virtual {v3}, Lcom/discord/app/AppActivity$f;->invoke()V

    .line 220
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x10

    if-gt p1, v0, :cond_0

    .line 221
    move-object p1, p0

    check-cast p1, Landroid/content/Context;

    invoke-virtual {v4, p1}, Lcom/discord/app/AppActivity$g;->w(Landroid/content/Context;)V

    .line 223
    :cond_0
    const-class p1, Lcom/discord/widgets/main/WidgetMain;

    invoke-static {p1}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/discord/app/AppActivity;->a(Lkotlin/reflect/b;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 224
    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    .line 225
    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getAnalytics()Lcom/discord/stores/StoreAnalytics;

    move-result-object p1

    .line 226
    invoke-virtual {p0}, Lcom/discord/app/AppActivity;->getScreen()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/discord/stores/StoreAnalytics;->appUiViewed(Ljava/lang/Class;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 229
    const-class v0, Lcom/discord/widgets/debugging/WidgetFatalCrash;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/discord/app/AppActivity;->a(Lkotlin/reflect/b;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 230
    sget-object v0, Lcom/discord/widgets/debugging/WidgetFatalCrash;->Companion:Lcom/discord/widgets/debugging/WidgetFatalCrash$Companion;

    move-object v1, p0

    check-cast v1, Landroid/content/Context;

    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0}, Lcom/discord/app/AppActivity;->getScreen()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "screen.name"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, p1, v2}, Lcom/discord/widgets/debugging/WidgetFatalCrash$Companion;->launch(Landroid/content/Context;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 232
    :cond_1
    invoke-virtual {p0}, Lcom/discord/app/AppActivity;->finish()V

    :cond_2
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .line 262
    iget-object v0, p0, Lcom/discord/app/AppActivity;->tN:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 263
    invoke-super {p0}, Lcom/discord/app/c;->onDestroy()V

    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .line 117
    invoke-super {p0, p1}, Lcom/discord/app/c;->onNewIntent(Landroid/content/Intent;)V

    if-nez p1, :cond_0

    .line 119
    sget-object p1, Lcom/discord/app/AppActivity;->tU:Landroid/content/Intent;

    :cond_0
    iput-object p1, p0, Lcom/discord/app/AppActivity;->tS:Landroid/content/Intent;

    .line 120
    invoke-virtual {p0}, Lcom/discord/app/AppActivity;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/app/AppActivity;->a(Landroid/content/Intent;)V

    .line 121
    invoke-virtual {p0}, Lcom/discord/app/AppActivity;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object p1

    .line 122
    iget-object v0, p0, Lcom/discord/app/AppActivity;->tN:Ljava/util/LinkedHashMap;

    check-cast v0, Ljava/util/Map;

    .line 548
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 122
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-interface {v1, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onPause()V
    .locals 2

    .line 257
    invoke-super {p0}, Lcom/discord/app/c;->onPause()V

    .line 258
    invoke-virtual {p0}, Lcom/discord/app/AppActivity;->getPaused()Lrx/subjects/Subject;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lrx/subjects/Subject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public onResume()V
    .locals 13

    .line 237
    invoke-super {p0}, Lcom/discord/app/c;->onResume()V

    .line 239
    sget-object v0, Lcom/discord/app/f;->uP:Lcom/discord/app/f;

    invoke-static {p0}, Lcom/discord/app/f;->c(Lcom/discord/app/AppActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 241
    :cond_0
    invoke-direct {p0}, Lcom/discord/app/AppActivity;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserSettings;->getLocale()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/app/AppActivity;->tP:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/discord/app/AppActivity;->tP:Ljava/lang/String;

    const-string v2, ""

    invoke-static {v0, v2}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_1

    .line 242
    invoke-direct {p0, v1}, Lcom/discord/app/AppActivity;->n(Z)V

    return-void

    .line 245
    :cond_1
    invoke-direct {p0}, Lcom/discord/app/AppActivity;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserSettings;->getLocale()Ljava/lang/String;

    move-result-object v0

    const-string v2, "userSettings.locale"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/app/AppActivity;->tP:Ljava/lang/String;

    .line 247
    sget-object v0, Lcom/discord/app/AppActivity$b;->tZ:Lcom/discord/app/AppActivity$b$a;

    .line 1504
    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    .line 1505
    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object v0

    .line 1506
    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreUserSettings;->getThemeObservable(Z)Lrx/Observable;

    move-result-object v0

    .line 1507
    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    .line 1508
    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreUserSettings;->getLocaleObservable()Lrx/Observable;

    move-result-object v1

    .line 1510
    sget-object v2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    .line 1511
    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/stores/StoreUserSettings;->getFontScaleObs()Lrx/Observable;

    move-result-object v2

    .line 1513
    sget-object v3, Lcom/discord/app/AppActivity$b$a$a;->ua:Lcom/discord/app/AppActivity$b$a$a;

    check-cast v3, Lkotlin/jvm/functions/Function3;

    if-eqz v3, :cond_2

    new-instance v4, Lcom/discord/app/b;

    invoke-direct {v4, v3}, Lcom/discord/app/b;-><init>(Lkotlin/jvm/functions/Function3;)V

    move-object v3, v4

    :cond_2
    check-cast v3, Lrx/functions/Func3;

    .line 1503
    invoke-static {v0, v1, v2, v3}, Lrx/Observable;->a(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026Obs,\n            ::Model)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    .line 248
    move-object v1, p0

    check-cast v1, Lcom/discord/app/AppComponent;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v3, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    .line 249
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    new-instance v0, Lcom/discord/app/AppActivity$h;

    invoke-direct {v0, p0}, Lcom/discord/app/AppActivity$h;-><init>(Lcom/discord/app/AppActivity;)V

    move-object v10, v0

    check-cast v10, Lkotlin/jvm/functions/Function1;

    const/16 v11, 0x1e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final setActionBarSubtitle(Ljava/lang/CharSequence;)Lkotlin/Unit;
    .locals 1

    .line 286
    invoke-virtual {p0}, Lcom/discord/app/AppActivity;->dm()Lcom/discord/views/ToolbarTitleLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/discord/views/ToolbarTitleLayout;->setSubtitle(Ljava/lang/CharSequence;)V

    sget-object p1, Lkotlin/Unit;->bjS:Lkotlin/Unit;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public final setActionBarTitle(Ljava/lang/CharSequence;)Lkotlin/Unit;
    .locals 1

    .line 280
    invoke-virtual {p0}, Lcom/discord/app/AppActivity;->dm()Lcom/discord/views/ToolbarTitleLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/discord/views/ToolbarTitleLayout;->setTitle(Ljava/lang/CharSequence;)V

    sget-object p1, Lkotlin/Unit;->bjS:Lkotlin/Unit;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public final showKeyboard(Landroid/view/View;)V
    .locals 2

    .line 317
    move-object v0, p0

    check-cast v0, Landroid/app/Activity;

    const/4 v1, 0x1

    invoke-static {v0, v1, p1}, Lcom/discord/utilities/keyboard/Keyboard;->setKeyboardOpen(Landroid/app/Activity;ZLandroid/view/View;)V

    return-void
.end method
