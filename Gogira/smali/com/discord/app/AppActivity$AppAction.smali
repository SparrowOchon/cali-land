.class public final Lcom/discord/app/AppActivity$AppAction;
.super Lcom/discord/app/AppActivity;
.source "AppActivity.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/app/AppActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AppAction"
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final tR:Lkotlin/Lazy;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/v;

    const-class v2, Lcom/discord/app/AppActivity$AppAction;

    invoke-static {v2}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v2

    const-string v3, "screen"

    const-string v4, "getScreen()Ljava/lang/Class;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/discord/app/AppActivity$AppAction;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 478
    invoke-direct {p0}, Lcom/discord/app/AppActivity;-><init>()V

    .line 480
    new-instance v0, Lcom/discord/app/AppActivity$AppAction$a;

    invoke-direct {v0, p0}, Lcom/discord/app/AppActivity$AppAction$a;-><init>(Lcom/discord/app/AppActivity$AppAction;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {v0}, Lkotlin/f;->b(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/app/AppActivity$AppAction;->tR:Lkotlin/Lazy;

    return-void
.end method


# virtual methods
.method protected final getScreen()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/discord/app/AppComponent;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/app/AppActivity$AppAction;->tR:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 489
    invoke-super {p0, p1}, Lcom/discord/app/AppActivity;->onCreate(Landroid/os/Bundle;)V

    .line 2051
    sget-boolean p1, Lcom/discord/app/AppActivity;->tT:Z

    if-eqz p1, :cond_0

    .line 491
    invoke-virtual {p0}, Lcom/discord/app/AppActivity$AppAction;->finish()V

    :cond_0
    return-void
.end method
