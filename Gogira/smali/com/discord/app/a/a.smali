.class public final Lcom/discord/app/a/a;
.super Ljava/lang/Object;
.source "RoutingPatterns.kt"


# static fields
.field private static final HOST:Ljava/lang/String;

.field private static final HOST_GIFT:Ljava/lang/String;

.field private static final HOST_INVITE:Ljava/lang/String;

.field private static final we:Ljava/lang/String;

.field private static final wf:Ljava/lang/String;

.field private static final wg:Ljava/lang/String;

.field private static final wh:Lkotlin/text/Regex;

.field private static final wi:Lkotlin/text/Regex;

.field public static final wj:Lkotlin/text/Regex;

.field public static final wk:Lkotlin/text/Regex;

.field private static final wl:Lkotlin/text/Regex;

.field private static final wm:Lkotlin/text/Regex;

.field private static final wn:Lkotlin/text/Regex;

.field private static final wo:Lkotlin/text/Regex;

.field private static final wp:Lkotlin/text/Regex;

.field private static final wq:Lkotlin/text/Regex;

.field private static final wr:Lkotlin/text/Regex;

.field private static final ws:Lkotlin/text/Regex;

.field public static final wt:Lcom/discord/app/a/a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 10
    new-instance v0, Lcom/discord/app/a/a;

    invoke-direct {v0}, Lcom/discord/app/a/a;-><init>()V

    sput-object v0, Lcom/discord/app/a/a;->wt:Lcom/discord/app/a/a;

    const-string v0, "https://discordapp.com"

    .line 16
    invoke-static {v0}, Lcom/discord/app/a/a;->ab(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/discord/app/a/a;->HOST:Ljava/lang/String;

    const-string v0, "https://discord.gift"

    .line 17
    invoke-static {v0}, Lcom/discord/app/a/a;->ab(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/discord/app/a/a;->HOST_GIFT:Ljava/lang/String;

    const-string v0, "https://discord.gg"

    .line 18
    invoke-static {v0}, Lcom/discord/app/a/a;->ab(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/discord/app/a/a;->HOST_INVITE:Ljava/lang/String;

    .line 20
    sget-object v0, Lcom/discord/app/a/a;->HOST:Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "\\."

    const-string v3, "."

    .line 1075
    invoke-static {v0, v3, v2, v1}, Lkotlin/text/l;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 20
    sput-object v0, Lcom/discord/app/a/a;->we:Ljava/lang/String;

    .line 21
    sget-object v0, Lcom/discord/app/a/a;->HOST_GIFT:Ljava/lang/String;

    .line 2075
    invoke-static {v0, v3, v2, v1}, Lkotlin/text/l;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 21
    sput-object v0, Lcom/discord/app/a/a;->wf:Ljava/lang/String;

    .line 22
    sget-object v0, Lcom/discord/app/a/a;->HOST_INVITE:Ljava/lang/String;

    .line 3075
    invoke-static {v0, v3, v2, v1}, Lkotlin/text/l;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 22
    sput-object v0, Lcom/discord/app/a/a;->wg:Ljava/lang/String;

    .line 32
    sget-object v0, Lkotlin/text/k;->bmi:Lkotlin/text/k;

    new-instance v1, Lkotlin/text/Regex;

    const-string v2, "^/(?:(invite|gift)/)?([\\w-]+)/?$"

    invoke-direct {v1, v2, v0}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lkotlin/text/k;)V

    sput-object v1, Lcom/discord/app/a/a;->wh:Lkotlin/text/Regex;

    .line 40
    sget-object v0, Lkotlin/text/k;->bmi:Lkotlin/text/k;

    new-instance v1, Lkotlin/text/Regex;

    const-string v2, "^/(?:invite\\\\/)?([\\w-]+)/?$"

    invoke-direct {v1, v2, v0}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lkotlin/text/k;)V

    sput-object v1, Lcom/discord/app/a/a;->wi:Lkotlin/text/Regex;

    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "(?:https?://(?:(?:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/discord/app/a/a;->we:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/invite)|(?:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/discord/app/a/a;->wg:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "))|(?:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/discord/app/a/a;->wg:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "))/([\\w-]+)/?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 55
    sget-object v2, Lkotlin/text/k;->bmi:Lkotlin/text/k;

    new-instance v3, Lkotlin/text/Regex;

    invoke-direct {v3, v0, v2}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lkotlin/text/k;)V

    sput-object v3, Lcom/discord/app/a/a;->wj:Lkotlin/text/Regex;

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "(?:https?://)?(?:(?:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/discord/app/a/a;->we:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/gifts)|(?:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/discord/app/a/a;->wf:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 69
    sget-object v1, Lkotlin/text/k;->bmi:Lkotlin/text/k;

    new-instance v2, Lkotlin/text/Regex;

    invoke-direct {v2, v0, v1}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lkotlin/text/k;)V

    sput-object v2, Lcom/discord/app/a/a;->wk:Lkotlin/text/Regex;

    .line 77
    sget-object v0, Lkotlin/text/k;->bmi:Lkotlin/text/k;

    new-instance v1, Lkotlin/text/Regex;

    const-string v2, "^/connect(?:/(\\d+))?/?$"

    invoke-direct {v1, v2, v0}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lkotlin/text/k;)V

    sput-object v1, Lcom/discord/app/a/a;->wl:Lkotlin/text/Regex;

    .line 86
    sget-object v0, Lkotlin/text/k;->bmi:Lkotlin/text/k;

    new-instance v1, Lkotlin/text/Regex;

    const-string v2, "^/channels/((?:@me)|(?:\\d+))/(\\d+)(?:/(\\d+))?/?$"

    invoke-direct {v1, v2, v0}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lkotlin/text/k;)V

    sput-object v1, Lcom/discord/app/a/a;->wm:Lkotlin/text/Regex;

    .line 95
    sget-object v0, Lkotlin/text/k;->bmi:Lkotlin/text/k;

    new-instance v1, Lkotlin/text/Regex;

    const-string v2, "^/lurk/(\\d+)(?:/(\\d+))?/?$"

    invoke-direct {v1, v2, v0}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lkotlin/text/k;)V

    sput-object v1, Lcom/discord/app/a/a;->wn:Lkotlin/text/Regex;

    .line 102
    sget-object v0, Lkotlin/text/k;->bmi:Lkotlin/text/k;

    new-instance v1, Lkotlin/text/Regex;

    const-string v2, "^/channels/@me/user/(\\d+)/?$"

    invoke-direct {v1, v2, v0}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lkotlin/text/k;)V

    sput-object v1, Lcom/discord/app/a/a;->wo:Lkotlin/text/Regex;

    .line 108
    sget-object v0, Lkotlin/text/k;->bmi:Lkotlin/text/k;

    new-instance v1, Lkotlin/text/Regex;

    const-string v2, "^/profile/(\\d+)/?$"

    invoke-direct {v1, v2, v0}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lkotlin/text/k;)V

    sput-object v1, Lcom/discord/app/a/a;->wp:Lkotlin/text/Regex;

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "^(?:ptb|canary)."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/discord/app/a/a;->we:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lkotlin/text/k;->bmi:Lkotlin/text/k;

    new-instance v2, Lkotlin/text/Regex;

    invoke-direct {v2, v0, v1}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lkotlin/text/k;)V

    sput-object v2, Lcom/discord/app/a/a;->wq:Lkotlin/text/Regex;

    .line 118
    sget-object v0, Lkotlin/text/k;->bmi:Lkotlin/text/k;

    new-instance v1, Lkotlin/text/Regex;

    const-string v2, "^/settings(/\\w+)*/?$"

    invoke-direct {v1, v2, v0}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lkotlin/text/k;)V

    sput-object v1, Lcom/discord/app/a/a;->wr:Lkotlin/text/Regex;

    .line 124
    sget-object v0, Lkotlin/text/k;->bmi:Lkotlin/text/k;

    new-instance v1, Lkotlin/text/Regex;

    const-string v2, "^/oauth2/authorize/?$"

    invoke-direct {v1, v2, v0}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lkotlin/text/k;)V

    sput-object v1, Lcom/discord/app/a/a;->ws:Lkotlin/text/Regex;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/net/Uri;)Z
    .locals 3

    const-string v0, "$this$isInviteLink"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/discord/app/a/a;->HOST_INVITE:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/text/l;->af(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    .line 137
    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/discord/app/a/a;->HOST:Ljava/lang/String;

    invoke-static {v0, v2}, Lkotlin/text/l;->af(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    check-cast p0, Ljava/lang/CharSequence;

    sget-object v0, Lcom/discord/app/a/a;->wi:Lkotlin/text/Regex;

    invoke-virtual {v0, p0}, Lkotlin/text/Regex;->g(Ljava/lang/CharSequence;)Z

    move-result p0

    if-ne p0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    return p0

    :cond_1
    :goto_0
    return v1
.end method

.method public static aa(Ljava/lang/String;)Z
    .locals 1

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return p0

    .line 129
    :cond_0
    sget-object v0, Lcom/discord/app/a/a;->HOST:Ljava/lang/String;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 130
    sget-object v0, Lcom/discord/app/a/a;->HOST_GIFT:Ljava/lang/String;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 131
    sget-object v0, Lcom/discord/app/a/a;->HOST_INVITE:Ljava/lang/String;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 132
    :cond_1
    sget-object v0, Lcom/discord/app/a/a;->wq:Lkotlin/text/Regex;

    check-cast p0, Ljava/lang/CharSequence;

    invoke-virtual {v0, p0}, Lkotlin/text/Regex;->g(Ljava/lang/CharSequence;)Z

    move-result p0

    return p0

    :cond_2
    :goto_0
    const/4 p0, 0x1

    return p0
.end method

.method private static ab(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 139
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    const-string v0, "Uri.parse(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object p0

    if-nez p0, :cond_0

    const-string p0, ""

    :cond_0
    return-object p0
.end method

.method public static dF()Ljava/lang/String;
    .locals 1

    .line 16
    sget-object v0, Lcom/discord/app/a/a;->HOST:Ljava/lang/String;

    return-object v0
.end method

.method public static dG()Ljava/lang/String;
    .locals 1

    .line 18
    sget-object v0, Lcom/discord/app/a/a;->HOST_INVITE:Ljava/lang/String;

    return-object v0
.end method

.method public static dH()Lkotlin/text/Regex;
    .locals 1

    .line 32
    sget-object v0, Lcom/discord/app/a/a;->wh:Lkotlin/text/Regex;

    return-object v0
.end method

.method public static dI()Lkotlin/text/Regex;
    .locals 1

    .line 40
    sget-object v0, Lcom/discord/app/a/a;->wi:Lkotlin/text/Regex;

    return-object v0
.end method

.method public static dJ()Lkotlin/text/Regex;
    .locals 1

    .line 76
    sget-object v0, Lcom/discord/app/a/a;->wl:Lkotlin/text/Regex;

    return-object v0
.end method

.method public static dK()Lkotlin/text/Regex;
    .locals 1

    .line 85
    sget-object v0, Lcom/discord/app/a/a;->wm:Lkotlin/text/Regex;

    return-object v0
.end method

.method public static dL()Lkotlin/text/Regex;
    .locals 1

    .line 94
    sget-object v0, Lcom/discord/app/a/a;->wn:Lkotlin/text/Regex;

    return-object v0
.end method

.method public static dM()Lkotlin/text/Regex;
    .locals 1

    .line 101
    sget-object v0, Lcom/discord/app/a/a;->wo:Lkotlin/text/Regex;

    return-object v0
.end method

.method public static dN()Lkotlin/text/Regex;
    .locals 1

    .line 108
    sget-object v0, Lcom/discord/app/a/a;->wp:Lkotlin/text/Regex;

    return-object v0
.end method

.method public static dO()Lkotlin/text/Regex;
    .locals 1

    .line 118
    sget-object v0, Lcom/discord/app/a/a;->wr:Lkotlin/text/Regex;

    return-object v0
.end method

.method public static dP()Lkotlin/text/Regex;
    .locals 1

    .line 124
    sget-object v0, Lcom/discord/app/a/a;->ws:Lkotlin/text/Regex;

    return-object v0
.end method
