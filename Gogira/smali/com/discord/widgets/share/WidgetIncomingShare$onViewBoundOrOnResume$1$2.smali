.class final Lcom/discord/widgets/share/WidgetIncomingShare$onViewBoundOrOnResume$1$2;
.super Ljava/lang/Object;
.source "WidgetIncomingShare.kt"

# interfaces
.implements Lrx/functions/Func5;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/share/WidgetIncomingShare$onViewBoundOrOnResume$1;->call(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "T4:",
        "Ljava/lang/Object;",
        "T5:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func5<",
        "TT1;TT2;TT3;TT4;TT5;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic $receiver:Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;


# direct methods
.method constructor <init>(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/share/WidgetIncomingShare$onViewBoundOrOnResume$1$2;->$receiver:Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;Ljava/lang/String;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;Ljava/lang/Boolean;Lcom/discord/models/domain/ModelUser;)Lcom/discord/widgets/share/WidgetIncomingShare$Model;
    .locals 10

    .line 161
    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare$onViewBoundOrOnResume$1$2;->$receiver:Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;

    .line 162
    instance-of v1, v0, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemGuild;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemGuild;

    invoke-virtual {v0}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemGuild;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getMaxFileSizeMB()I

    move-result v0

    goto :goto_0

    .line 163
    :cond_0
    instance-of v1, v0, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemChannel;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemChannel;

    invoke-virtual {v0}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemChannel;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getMaxFileSizeMB()I

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    const-string v1, "contentModel"

    .line 167
    invoke-static {p1, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "searchModel"

    invoke-static {p3, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v6, p0, Lcom/discord/widgets/share/WidgetIncomingShare$onViewBoundOrOnResume$1$2;->$receiver:Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;

    const-string v1, "isOnCooldown"

    invoke-static {p4, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    const-string p4, "meUser"

    invoke-static {p5, p4}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p5}, Lcom/discord/models/domain/ModelUser;->getMaxFileSizeMB()I

    move-result p4

    invoke-static {v0, p4}, Ljava/lang/Math;->max(II)I

    move-result v8

    invoke-virtual {p5}, Lcom/discord/models/domain/ModelUser;->isPremium()Z

    move-result v9

    new-instance p4, Lcom/discord/widgets/share/WidgetIncomingShare$Model;

    move-object v2, p4

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v2 .. v9}, Lcom/discord/widgets/share/WidgetIncomingShare$Model;-><init>(Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;Ljava/lang/String;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;ZIZ)V

    return-object p4
.end method

.method public final bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 55
    check-cast p1, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;

    check-cast p2, Ljava/lang/String;

    check-cast p3, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;

    check-cast p4, Ljava/lang/Boolean;

    check-cast p5, Lcom/discord/models/domain/ModelUser;

    invoke-virtual/range {p0 .. p5}, Lcom/discord/widgets/share/WidgetIncomingShare$onViewBoundOrOnResume$1$2;->call(Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;Ljava/lang/String;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;Ljava/lang/Boolean;Lcom/discord/models/domain/ModelUser;)Lcom/discord/widgets/share/WidgetIncomingShare$Model;

    move-result-object p1

    return-object p1
.end method
