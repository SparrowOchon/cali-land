.class public final enum Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;
.super Ljava/lang/Enum;
.source "BoostInProgressViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BoostState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

.field public static final enum CALL_IN_PROGRESS:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

.field public static final enum COMPLETED:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

.field public static final enum ERROR:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

.field public static final enum NOT_IN_PROGRESS:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    new-instance v1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    const/4 v2, 0x0

    const-string v3, "NOT_IN_PROGRESS"

    invoke-direct {v1, v3, v2}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;->NOT_IN_PROGRESS:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    const/4 v2, 0x1

    const-string v3, "CALL_IN_PROGRESS"

    invoke-direct {v1, v3, v2}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;->CALL_IN_PROGRESS:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    const/4 v2, 0x2

    const-string v3, "COMPLETED"

    invoke-direct {v1, v3, v2}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;->COMPLETED:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    const/4 v2, 0x3

    const-string v3, "ERROR"

    invoke-direct {v1, v3, v2}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;->ERROR:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    aput-object v1, v0, v2

    sput-object v0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;->$VALUES:[Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 147
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;
    .locals 1

    const-class v0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    return-object p0
.end method

.method public static values()[Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;
    .locals 1

    sget-object v0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;->$VALUES:[Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    invoke-virtual {v0}, [Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    return-object v0
.end method
