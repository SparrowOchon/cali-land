.class public final Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;
.super Lcom/discord/app/AppFragment;
.source "WidgetServerBoostConfirmation.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation$Companion;
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field private static final COOLDOWN_DAYS:I = 0x7

.field public static final Companion:Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation$Companion;

.field private static final INTENT_EXTRA_GUILD_ID:Ljava/lang/String; = "GUILD_ID"

.field private static final VIEW_INDEX_BOOSTED:I = 0x1

.field private static final VIEW_INDEX_LOADED:I


# instance fields
.field private final avatar$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final cancel$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final close$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final dimmer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final error$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private guildId:J

.field private final level$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final name$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final select$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;

.field private final warning$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-class v0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;

    const/16 v1, 0xa

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "flipper"

    const-string v5, "getFlipper()Lcom/discord/app/AppViewFlipper;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "warning"

    const-string v5, "getWarning()Landroid/widget/TextView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "avatar"

    const-string v5, "getAvatar()Lcom/facebook/drawee/view/SimpleDraweeView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "name"

    const-string v5, "getName()Landroid/widget/TextView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "level"

    const-string v5, "getLevel()Landroid/widget/TextView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x4

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "select"

    const-string v5, "getSelect()Landroid/view/View;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x5

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "cancel"

    const-string v5, "getCancel()Landroid/view/View;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x6

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "close"

    const-string v5, "getClose()Landroid/view/View;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x7

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "error"

    const-string v5, "getError()Landroid/view/View;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0x8

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v0

    const-string v3, "dimmer"

    const-string v4, "getDimmer()Lcom/discord/utilities/dimmer/DimmerView;"

    invoke-direct {v2, v0, v3, v4}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aput-object v0, v1, v2

    sput-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->Companion:Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 25
    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a00e2

    .line 27
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00e7

    .line 28
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->warning$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00dd

    .line 29
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->avatar$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00e3

    .line 30
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->name$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00e4

    .line 31
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->level$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00e6

    .line 33
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->select$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00df

    .line 34
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->cancel$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00e0

    .line 35
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->close$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00e1

    .line 36
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->error$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a026f

    .line 38
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->dimmer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const-wide/16 v0, -0x1

    .line 42
    iput-wide v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->guildId:J

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->configureUI(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;)V

    return-void
.end method

.method public static final synthetic access$getGuildId$p(Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;)J
    .locals 2

    .line 25
    iget-wide v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->guildId:J

    return-wide v0
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;)Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;
    .locals 1

    .line 25
    iget-object p0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->viewModel:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;

    if-nez p0, :cond_0

    const-string v0, "viewModel"

    invoke-static {v0}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setGuildId$p(Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;J)V
    .locals 0

    .line 25
    iput-wide p1, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->guildId:J

    return-void
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;)V
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->viewModel:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;

    return-void
.end method

.method private final configureGuildCard(Lcom/discord/models/domain/ModelGuild;)V
    .locals 17

    move-object/from16 v0, p0

    .line 140
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getAvatar()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Landroid/widget/ImageView;

    const/4 v1, 0x0

    const/4 v9, 0x0

    const/4 v3, 0x6

    move-object/from16 v10, p1

    invoke-static {v10, v9, v1, v3, v9}, Lcom/discord/utilities/icon/IconUtils;->getForGuild$default(Lcom/discord/models/domain/ModelGuild;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1c

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lcom/discord/utilities/icon/IconUtils;->setIcon$default(Landroid/widget/ImageView;Ljava/lang/String;ILkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    .line 141
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getName()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuild;->getPremiumTier()I

    move-result v2

    if-eqz v2, :cond_3

    const/4 v3, 0x1

    const-string v4, "requireContext()"

    const/4 v5, 0x2

    if-eq v2, v3, :cond_2

    if-eq v2, v5, :cond_1

    const/4 v3, 0x3

    if-eq v2, v3, :cond_0

    goto :goto_0

    .line 157
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getLevel()Landroid/widget/TextView;

    move-result-object v2

    const v3, 0x7f120e11

    invoke-virtual {v0, v3}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getLevel()Landroid/widget/TextView;

    move-result-object v10

    invoke-virtual/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->requireContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v4}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const v3, 0x7f040153

    invoke-static {v2, v3, v1, v5, v9}, Lcom/discord/utilities/drawable/DrawableCompat;->getThemedDrawableRes$default(Landroid/content/Context;IIILjava/lang/Object;)I

    move-result v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xe

    const/16 v16, 0x0

    invoke-static/range {v10 .. v16}, Lcom/discord/utilities/drawable/DrawableCompat;->setCompoundDrawablesCompat$default(Landroid/widget/TextView;IIIIILjava/lang/Object;)V

    :goto_0
    return-void

    .line 153
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getLevel()Landroid/widget/TextView;

    move-result-object v2

    const v3, 0x7f120e10

    invoke-virtual {v0, v3}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 154
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getLevel()Landroid/widget/TextView;

    move-result-object v10

    invoke-virtual/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->requireContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v4}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const v3, 0x7f040152

    invoke-static {v2, v3, v1, v5, v9}, Lcom/discord/utilities/drawable/DrawableCompat;->getThemedDrawableRes$default(Landroid/content/Context;IIILjava/lang/Object;)I

    move-result v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xe

    const/16 v16, 0x0

    invoke-static/range {v10 .. v16}, Lcom/discord/utilities/drawable/DrawableCompat;->setCompoundDrawablesCompat$default(Landroid/widget/TextView;IIIIILjava/lang/Object;)V

    return-void

    .line 149
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getLevel()Landroid/widget/TextView;

    move-result-object v2

    const v3, 0x7f120e0f

    invoke-virtual {v0, v3}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getLevel()Landroid/widget/TextView;

    move-result-object v10

    invoke-virtual/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->requireContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v4}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const v3, 0x7f040151

    invoke-static {v2, v3, v1, v5, v9}, Lcom/discord/utilities/drawable/DrawableCompat;->getThemedDrawableRes$default(Landroid/content/Context;IIILjava/lang/Object;)I

    move-result v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xe

    const/16 v16, 0x0

    invoke-static/range {v10 .. v16}, Lcom/discord/utilities/drawable/DrawableCompat;->setCompoundDrawablesCompat$default(Landroid/widget/TextView;IIIIILjava/lang/Object;)V

    return-void

    .line 145
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getLevel()Landroid/widget/TextView;

    move-result-object v1

    const v2, 0x7f120ddb

    invoke-virtual {v0, v2}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getLevel()Landroid/widget/TextView;

    move-result-object v3

    const v4, 0x7f0802a3

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0xe

    const/4 v9, 0x0

    invoke-static/range {v3 .. v9}, Lcom/discord/utilities/drawable/DrawableCompat;->setCompoundDrawablesCompat$default(Landroid/widget/TextView;IIIIILjava/lang/Object;)V

    return-void
.end method

.method private final configureToolbar(Ljava/lang/String;)V
    .locals 1

    const v0, 0x7f120de6

    .line 135
    invoke-virtual {p0, v0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->setActionBarTitle(I)Lkotlin/Unit;

    .line 136
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->setActionBarSubtitle(Ljava/lang/CharSequence;)Lkotlin/Unit;

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;)V
    .locals 13

    .line 94
    instance-of v0, p1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Uninitialized;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_6

    .line 97
    check-cast p1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;

    .line 99
    invoke-virtual {p1}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    if-nez v0, :cond_1

    .line 100
    invoke-virtual {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->finish()V

    return-void

    .line 104
    :cond_1
    invoke-virtual {p1}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "state.guild.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->configureToolbar(Ljava/lang/String;)V

    .line 105
    invoke-virtual {p1}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->configureGuildCard(Lcom/discord/models/domain/ModelGuild;)V

    .line 107
    invoke-virtual {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f100094

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Object;

    const/4 v4, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v3, v6

    invoke-virtual {v0, v1, v4, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getQuantityStr\u2026DOWN_DAYS, COOLDOWN_DAYS)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getWarning()Landroid/widget/TextView;

    move-result-object v1

    sget-object v7, Lcom/discord/utilities/textprocessing/Parsers;->INSTANCE:Lcom/discord/utilities/textprocessing/Parsers;

    invoke-virtual {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->requireContext()Landroid/content/Context;

    move-result-object v8

    const-string v3, "requireContext()"

    invoke-static {v8, v3}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const v3, 0x7f120df7

    new-array v4, v2, [Ljava/lang/Object;

    aput-object v0, v4, v6

    invoke-virtual {p0, v3, v4}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "getString(R.string.premi\u2026down_warning, timeString)"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v9, v0

    check-cast v9, Ljava/lang/CharSequence;

    const/4 v10, 0x0

    const/4 v11, 0x4

    const/4 v12, 0x0

    invoke-static/range {v7 .. v12}, Lcom/discord/utilities/textprocessing/Parsers;->parseMarkdown$default(Lcom/discord/utilities/textprocessing/Parsers;Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    invoke-virtual {p1}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;->getBoostState()Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    move-result-object p1

    sget-object v0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x0

    const/4 v1, 0x4

    const/4 v3, 0x2

    if-eq p1, v2, :cond_5

    if-eq p1, v3, :cond_4

    const/4 v4, 0x3

    if-eq p1, v4, :cond_3

    if-eq p1, v1, :cond_2

    goto :goto_0

    .line 127
    :cond_2
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object p1

    invoke-virtual {p1, v2}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    .line 128
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getDimmer()Lcom/discord/utilities/dimmer/DimmerView;

    move-result-object p1

    invoke-static {p1, v6, v6, v3, v0}, Lcom/discord/utilities/dimmer/DimmerView;->setDimmed$default(Lcom/discord/utilities/dimmer/DimmerView;ZZILjava/lang/Object;)V

    .line 129
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getError()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    .line 122
    :cond_3
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object p1

    invoke-virtual {p1, v6}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    .line 123
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getDimmer()Lcom/discord/utilities/dimmer/DimmerView;

    move-result-object p1

    invoke-static {p1, v6, v6, v3, v0}, Lcom/discord/utilities/dimmer/DimmerView;->setDimmed$default(Lcom/discord/utilities/dimmer/DimmerView;ZZILjava/lang/Object;)V

    .line 124
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getError()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v6}, Landroid/view/View;->setVisibility(I)V

    return-void

    .line 117
    :cond_4
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object p1

    invoke-virtual {p1, v6}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    .line 118
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getDimmer()Lcom/discord/utilities/dimmer/DimmerView;

    move-result-object p1

    invoke-static {p1, v2, v6, v3, v0}, Lcom/discord/utilities/dimmer/DimmerView;->setDimmed$default(Lcom/discord/utilities/dimmer/DimmerView;ZZILjava/lang/Object;)V

    .line 119
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getError()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    .line 112
    :cond_5
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object p1

    invoke-virtual {p1, v6}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    .line 113
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getDimmer()Lcom/discord/utilities/dimmer/DimmerView;

    move-result-object p1

    invoke-static {p1, v6, v6, v3, v0}, Lcom/discord/utilities/dimmer/DimmerView;->setDimmed$default(Lcom/discord/utilities/dimmer/DimmerView;ZZILjava/lang/Object;)V

    .line 114
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getError()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    .line 97
    :cond_6
    new-instance p1, Lkotlin/r;

    const-string v0, "null cannot be cast to non-null type com.discord.widgets.servers.boosting.BoostInProgressViewModel.State.Loaded"

    invoke-direct {p1, v0}, Lkotlin/r;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static final create(Landroid/content/Context;J)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->Companion:Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation$Companion;

    invoke-virtual {v0, p0, p1, p2}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation$Companion;->create(Landroid/content/Context;J)V

    return-void
.end method

.method private final getAvatar()Lcom/facebook/drawee/view/SimpleDraweeView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->avatar$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/SimpleDraweeView;

    return-object v0
.end method

.method private final getCancel()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->cancel$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getClose()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->close$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getDimmer()Lcom/discord/utilities/dimmer/DimmerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->dimmer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/dimmer/DimmerView;

    return-object v0
.end method

.method private final getError()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->error$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getFlipper()Lcom/discord/app/AppViewFlipper;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/app/AppViewFlipper;

    return-object v0
.end method

.method private final getLevel()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->level$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getName()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->name$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getSelect()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->select$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getWarning()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->warning$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public final getContentViewResId()I
    .locals 1

    const v0, 0x7f0d016c

    return v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .line 52
    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onActivityCreated(Landroid/os/Bundle;)V

    const/4 p1, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x3

    .line 54
    invoke-static {p0, v0, p1, v1, p1}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled$default(Lcom/discord/app/AppFragment;ZLjava/lang/Integer;ILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 47
    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onCreate(Landroid/os/Bundle;)V

    .line 48
    invoke-virtual {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "GUILD_ID"

    const-wide/16 v1, -0x1

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->guildId:J

    return-void
.end method

.method public final onResume()V
    .locals 10

    .line 74
    invoke-super {p0}, Lcom/discord/app/AppFragment;->onResume()V

    .line 76
    iget-wide v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->guildId:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    const-wide/16 v2, -0x1

    cmp-long v4, v0, v2

    if-nez v4, :cond_1

    .line 77
    :cond_0
    invoke-virtual {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getAppActivity()Lcom/discord/app/AppActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/app/AppActivity;->finish()V

    .line 80
    :cond_1
    invoke-virtual {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroidx/lifecycle/ViewModelProviders;->of(Landroidx/fragment/app/FragmentActivity;)Landroidx/lifecycle/ViewModelProvider;

    move-result-object v0

    const-class v1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;

    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object v0

    const-string v1, "ViewModelProviders.of(re\u2026essViewModel::class.java)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->viewModel:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;

    .line 82
    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->viewModel:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;

    const-string v1, "viewModel"

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_2
    iget-wide v2, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->guildId:J

    invoke-virtual {v0, v2, v3}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->setGuildId(J)V

    .line 84
    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->viewModel:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    .line 85
    :cond_3
    invoke-virtual {v0}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->getState()Lrx/Observable;

    move-result-object v0

    .line 86
    move-object v1, p0

    check-cast v1, Lcom/discord/app/AppComponent;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v3, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Lrx/Observable;->JL()Lrx/Observable;

    move-result-object v1

    const-string v0, "viewModel\n        .getSt\u2026  .distinctUntilChanged()"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    const-class v2, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 90
    new-instance v0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation$onResume$1;

    move-object v7, p0

    check-cast v7, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;

    invoke-direct {v0, v7}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation$onResume$1;-><init>(Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;)V

    move-object v7, v0

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/16 v8, 0x1e

    const/4 v9, 0x0

    .line 88
    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final onViewBound(Landroid/view/View;)V
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    .line 60
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getSelect()Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation$onViewBound$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation$onViewBound$1;-><init>(Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getCancel()Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation$onViewBound$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation$onViewBound$2;-><init>(Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->getClose()Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation$onViewBound$3;

    invoke-direct {v0, p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation$onViewBound$3;-><init>(Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
