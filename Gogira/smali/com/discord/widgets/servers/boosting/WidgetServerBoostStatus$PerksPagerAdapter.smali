.class final Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$PerksPagerAdapter;
.super Landroidx/viewpager/widget/PagerAdapter;
.source "WidgetServerBoostStatus.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "PerksPagerAdapter"
.end annotation


# instance fields
.field private premiumTier:I

.field private subscriptionCount:I

.field private final views:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Integer;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 266
    invoke-direct {p0}, Landroidx/viewpager/widget/PagerAdapter;-><init>()V

    .line 271
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$PerksPagerAdapter;->views:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method public final configureViews()V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x3

    if-gt v0, v1, :cond_1

    .line 298
    iget-object v1, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$PerksPagerAdapter;->views:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 299
    instance-of v2, v1, Lcom/discord/widgets/servers/boosting/BoostPerkView;

    if-eqz v2, :cond_0

    .line 300
    check-cast v1, Lcom/discord/widgets/servers/boosting/BoostPerkView;

    iget v2, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$PerksPagerAdapter;->premiumTier:I

    invoke-virtual {v1, v0, v2}, Lcom/discord/widgets/servers/boosting/BoostPerkView;->configure(II)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    const-string p2, "container"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "view"

    invoke-static {p3, p2}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 293
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public final getCount()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public final getPremiumTier()I
    .locals 1

    .line 268
    iget v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$PerksPagerAdapter;->premiumTier:I

    return v0
.end method

.method public final getSubscriptionCount()I
    .locals 1

    .line 269
    iget v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$PerksPagerAdapter;->subscriptionCount:I

    return v0
.end method

.method public final instantiateItem(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 7

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 276
    new-instance v0, Lcom/discord/widgets/servers/boosting/BoostPerkView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v1, "container.context"

    invoke-static {v2, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/servers/boosting/BoostPerkView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iget v1, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$PerksPagerAdapter;->premiumTier:I

    invoke-virtual {v0, p2, v1}, Lcom/discord/widgets/servers/boosting/BoostPerkView;->configure(II)V

    check-cast v0, Landroid/view/View;

    goto :goto_0

    .line 275
    :cond_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d0077

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 278
    :goto_0
    iget-object v1, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$PerksPagerAdapter;->views:Ljava/util/HashMap;

    check-cast v1, Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "view"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p2, :cond_2

    const p2, 0x7f0a05cd

    .line 281
    invoke-virtual {v0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v1, "view.findViewById<TextVi\u2026_boost_status_empty_text)"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 282
    iget v2, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$PerksPagerAdapter;->subscriptionCount:I

    if-nez v2, :cond_1

    const v2, 0x7f120dea

    goto :goto_1

    :cond_1
    const v2, 0x7f120dec

    .line 281
    :goto_1
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 286
    :cond_2
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method public final bridge synthetic instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 0

    .line 266
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$PerksPagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public final isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "any"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 290
    invoke-static {p1, p2}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final setPremiumTier(I)V
    .locals 0

    .line 268
    iput p1, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$PerksPagerAdapter;->premiumTier:I

    return-void
.end method

.method public final setSubscriptionCount(I)V
    .locals 0

    .line 269
    iput p1, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$PerksPagerAdapter;->subscriptionCount:I

    return-void
.end method
