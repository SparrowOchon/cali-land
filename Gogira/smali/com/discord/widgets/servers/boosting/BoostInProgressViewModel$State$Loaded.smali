.class public final Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;
.super Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;
.source "BoostInProgressViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Loaded"
.end annotation


# instance fields
.field private final boostState:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

.field private final guild:Lcom/discord/models/domain/ModelGuild;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;Lcom/discord/models/domain/ModelGuild;)V
    .locals 1

    const-string v0, "boostState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 159
    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;-><init>(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;->boostState:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    iput-object p2, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;->guild:Lcom/discord/models/domain/ModelGuild;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;Lcom/discord/models/domain/ModelGuild;ILjava/lang/Object;)Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;->getBoostState()Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    move-result-object p1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;->guild:Lcom/discord/models/domain/ModelGuild;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;->copy(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;Lcom/discord/models/domain/ModelGuild;)Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;->getBoostState()Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;->guild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public final copy(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;Lcom/discord/models/domain/ModelGuild;)Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;
    .locals 1

    const-string v0, "boostState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;-><init>(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;Lcom/discord/models/domain/ModelGuild;)V

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;

    invoke-virtual {p0}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;->getBoostState()Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;->getBoostState()Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;->guild:Lcom/discord/models/domain/ModelGuild;

    iget-object p1, p1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBoostState()Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;->boostState:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    return-object v0
.end method

.method public final getGuild()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;->guild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;->getBoostState()Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;->guild:Lcom/discord/models/domain/ModelGuild;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Loaded(boostState="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;->getBoostState()Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", guild="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
