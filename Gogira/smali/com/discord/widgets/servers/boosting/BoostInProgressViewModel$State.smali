.class public abstract Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;
.super Ljava/lang/Object;
.source "BoostInProgressViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Uninitialized;,
        Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;
    }
.end annotation


# instance fields
.field private final boostState:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;


# direct methods
.method private constructor <init>(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;)V
    .locals 0

    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;->boostState:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 154
    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;-><init>(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;)V

    return-void
.end method


# virtual methods
.method public getBoostState()Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;->boostState:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    return-object v0
.end method
