.class final Lcom/discord/widgets/servers/boosting/WidgetServerRemoveBoostConfirmation$onViewBound$1;
.super Ljava/lang/Object;
.source "WidgetServerRemoveBoostConfirmation.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/servers/boosting/WidgetServerRemoveBoostConfirmation;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/discord/widgets/servers/boosting/WidgetServerRemoveBoostConfirmation;


# direct methods
.method constructor <init>(Lcom/discord/widgets/servers/boosting/WidgetServerRemoveBoostConfirmation;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/boosting/WidgetServerRemoveBoostConfirmation$onViewBound$1;->this$0:Lcom/discord/widgets/servers/boosting/WidgetServerRemoveBoostConfirmation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .line 58
    iget-object p1, p0, Lcom/discord/widgets/servers/boosting/WidgetServerRemoveBoostConfirmation$onViewBound$1;->this$0:Lcom/discord/widgets/servers/boosting/WidgetServerRemoveBoostConfirmation;

    invoke-static {p1}, Lcom/discord/widgets/servers/boosting/WidgetServerRemoveBoostConfirmation;->access$getViewModel$p(Lcom/discord/widgets/servers/boosting/WidgetServerRemoveBoostConfirmation;)Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerRemoveBoostConfirmation$onViewBound$1;->this$0:Lcom/discord/widgets/servers/boosting/WidgetServerRemoveBoostConfirmation;

    invoke-static {v0}, Lcom/discord/widgets/servers/boosting/WidgetServerRemoveBoostConfirmation;->access$getGuildId$p(Lcom/discord/widgets/servers/boosting/WidgetServerRemoveBoostConfirmation;)J

    move-result-wide v0

    iget-object v2, p0, Lcom/discord/widgets/servers/boosting/WidgetServerRemoveBoostConfirmation$onViewBound$1;->this$0:Lcom/discord/widgets/servers/boosting/WidgetServerRemoveBoostConfirmation;

    invoke-static {v2}, Lcom/discord/widgets/servers/boosting/WidgetServerRemoveBoostConfirmation;->access$getSubscriptionId$p(Lcom/discord/widgets/servers/boosting/WidgetServerRemoveBoostConfirmation;)J

    move-result-wide v2

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->removeBoostServer(JJ)V

    return-void
.end method
