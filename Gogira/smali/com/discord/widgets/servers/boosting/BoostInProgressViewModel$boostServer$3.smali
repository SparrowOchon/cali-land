.class final Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$boostServer$3;
.super Lkotlin/jvm/internal/l;
.source "BoostInProgressViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->boostServer(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/l;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/models/domain/ModelPremiumGuildSubscription;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;


# direct methods
.method constructor <init>(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$boostServer$3;->this$0:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/l;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lcom/discord/models/domain/ModelPremiumGuildSubscription;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$boostServer$3;->invoke(Lcom/discord/models/domain/ModelPremiumGuildSubscription;)V

    sget-object p1, Lkotlin/Unit;->bjS:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/models/domain/ModelPremiumGuildSubscription;)V
    .locals 2

    .line 63
    sget-object v0, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->INSTANCE:Lcom/discord/stores/StorePremiumGuildSubscription$Actions;

    const-string v1, "newSubscription"

    .line 64
    invoke-static {p1, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->appendNewPremiumGuildSubscription(Lcom/discord/models/domain/ModelPremiumGuildSubscription;)V

    .line 65
    iget-object p1, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$boostServer$3;->this$0:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;

    invoke-static {p1}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->access$handleBoostCompleted(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;)V

    return-void
.end method
