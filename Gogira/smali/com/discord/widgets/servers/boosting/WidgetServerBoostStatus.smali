.class public final Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;
.super Lcom/discord/app/AppFragment;
.source "WidgetServerBoostStatus.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$PerksPagerAdapter;,
        Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Model;,
        Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Companion;
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Companion;

.field private static final INTENT_EXTRA_GUILD_ID:Ljava/lang/String; = "GUILD_ID"

.field private static final VIEW_INDEX_BUTTON_BOOST:I = 0x0

.field private static final VIEW_INDEX_BUTTON_MANAGE:I = 0x2

.field private static final VIEW_INDEX_BUTTON_UPGRADE:I = 0x1

.field private static final VIEW_INDEX_LOADED:I = 0x2

.field private static final VIEW_INDEX_LOADING:I = 0x0

.field private static final VIEW_INDEX_LOAD_FAILED:I = 0x1


# instance fields
.field private final boostButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final boostInfo$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final boostNumber$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final boostProgressBar$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final boostTier0Iv$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final boostTier1Iv$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final boostTier1Tv$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final boostTier2Iv$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final boostTier2Tv$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final boostTier3Iv$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final boostTier3Tv$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final buttonFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final buttonInfo$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guildId$delegate:Lkotlin/Lazy;

.field private levelBackgrounds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private levelText:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private final manageButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private pagerAdapter:Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$PerksPagerAdapter;

.field private final retry$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final upgradeButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final viewPager$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private wasPagerPageSet:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-class v0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;

    const/16 v1, 0x13

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "boostNumber"

    const-string v5, "getBoostNumber()Landroid/widget/TextView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "boostInfo"

    const-string v5, "getBoostInfo()Landroid/widget/TextView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "boostProgressBar"

    const-string v5, "getBoostProgressBar()Landroid/widget/ProgressBar;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "boostTier0Iv"

    const-string v5, "getBoostTier0Iv()Landroid/widget/ImageView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "boostTier1Iv"

    const-string v5, "getBoostTier1Iv()Landroid/widget/ImageView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x4

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "boostTier1Tv"

    const-string v5, "getBoostTier1Tv()Landroid/widget/TextView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x5

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "boostTier2Iv"

    const-string v5, "getBoostTier2Iv()Landroid/widget/ImageView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x6

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "boostTier2Tv"

    const-string v5, "getBoostTier2Tv()Landroid/widget/TextView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x7

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "boostTier3Iv"

    const-string v5, "getBoostTier3Iv()Landroid/widget/ImageView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0x8

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "boostTier3Tv"

    const-string v5, "getBoostTier3Tv()Landroid/widget/TextView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0x9

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "flipper"

    const-string v5, "getFlipper()Lcom/discord/app/AppViewFlipper;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0xa

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "retry"

    const-string v5, "getRetry()Landroid/widget/Button;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0xb

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "buttonFlipper"

    const-string v5, "getButtonFlipper()Lcom/discord/app/AppViewFlipper;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0xc

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "boostButton"

    const-string v5, "getBoostButton()Landroid/widget/Button;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0xd

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "upgradeButton"

    const-string v5, "getUpgradeButton()Landroid/widget/Button;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0xe

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "manageButton"

    const-string v5, "getManageButton()Landroid/widget/Button;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0xf

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "buttonInfo"

    const-string v5, "getButtonInfo()Landroid/widget/TextView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0x10

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "viewPager"

    const-string v5, "getViewPager()Lcom/discord/utilities/simple_pager/SimplePager;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0x11

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v0

    const-string v3, "guildId"

    const-string v4, "getGuildId()J"

    invoke-direct {v2, v0, v3, v4}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/KProperty;

    const/16 v2, 0x12

    aput-object v0, v1, v2

    sput-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->Companion:Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 43
    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a00f9

    .line 45
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->boostNumber$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00f8

    .line 46
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->boostInfo$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00d1

    .line 48
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->boostProgressBar$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00d3

    .line 49
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->boostTier0Iv$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00d5

    .line 50
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->boostTier1Iv$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00d6

    .line 51
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->boostTier1Tv$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00d8

    .line 52
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->boostTier2Iv$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00d9

    .line 53
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->boostTier2Tv$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00db

    .line 54
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->boostTier3Iv$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00dc

    .line 55
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->boostTier3Tv$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00f7

    .line 57
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00fa

    .line 58
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->retry$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00f3

    .line 59
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->buttonFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00f1

    .line 60
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->boostButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00f6

    .line 61
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->upgradeButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00f5

    .line 62
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->manageButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00f4

    .line 63
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->buttonInfo$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00fb

    .line 68
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->viewPager$delegate:Lkotlin/properties/ReadOnlyProperty;

    .line 71
    new-instance v0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$PerksPagerAdapter;

    invoke-direct {v0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$PerksPagerAdapter;-><init>()V

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->pagerAdapter:Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$PerksPagerAdapter;

    .line 73
    new-instance v0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$guildId$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$guildId$2;-><init>(Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {v0}, Lkotlin/f;->b(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->guildId$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public static final synthetic access$configureLevelBubbles(Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;I)V
    .locals 0

    .line 43
    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->configureLevelBubbles(I)V

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Model;)V
    .locals 0

    .line 43
    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->configureUI(Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Model;)V

    return-void
.end method

.method public static final synthetic access$fetchData(Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;)V
    .locals 0

    .line 43
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->fetchData()V

    return-void
.end method

.method public static final synthetic access$getGuildId$p(Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;)J
    .locals 2

    .line 43
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getGuildId()J

    move-result-wide v0

    return-wide v0
.end method

.method public static final synthetic access$getMostRecentIntent$p(Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;)Landroid/content/Intent;
    .locals 0

    .line 43
    invoke-virtual {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object p0

    return-object p0
.end method

.method private final configureLevelBubbles(I)V
    .locals 6

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x3

    if-gt v0, v1, :cond_6

    .line 243
    iget-object v1, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->levelBackgrounds:Ljava/util/List;

    if-nez v1, :cond_0

    const-string v2, "levelBackgrounds"

    invoke-static {v2}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 244
    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    const/16 v3, 0x20

    const/16 v4, 0x14

    if-ne p1, v0, :cond_1

    const/16 v5, 0x20

    goto :goto_1

    :cond_1
    const/16 v5, 0x14

    .line 245
    :goto_1
    invoke-static {v5}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v5

    iput v5, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ne p1, v0, :cond_2

    goto :goto_2

    :cond_2
    const/16 v3, 0x14

    .line 246
    :goto_2
    invoke-static {v3}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v3

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 247
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 249
    iget-object v1, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->levelText:Ljava/util/List;

    if-nez v1, :cond_3

    const-string v2, "levelText"

    invoke-static {v2}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_3
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_5

    if-ne p1, v0, :cond_4

    .line 251
    move-object v2, v1

    check-cast v2, Landroid/view/View;

    const v3, 0x7f040291

    invoke-static {v2, v3}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result v2

    goto :goto_3

    .line 252
    :cond_4
    move-object v2, v1

    check-cast v2, Landroid/view/View;

    const v3, 0x7f04029b

    invoke-static {v2, v3}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result v2

    .line 250
    :goto_3
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_6
    return-void
.end method

.method private final configureProgressBar(II)V
    .locals 6

    .line 214
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getBoostProgressBar()Landroid/widget/ProgressBar;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/nitro/BoostUtils;->INSTANCE:Lcom/discord/utilities/nitro/BoostUtils;

    invoke-virtual {v1, p1, p2}, Lcom/discord/utilities/nitro/BoostUtils;->calculateTotalProgress(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 215
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getBoostProgressBar()Landroid/widget/ProgressBar;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz p1, :cond_3

    if-eq p1, v4, :cond_2

    if-eq p1, v2, :cond_1

    if-eq p1, v1, :cond_0

    const/4 v5, 0x0

    goto :goto_0

    :cond_0
    const v5, 0x7f120e11

    goto :goto_0

    :cond_1
    const v5, 0x7f120e10

    goto :goto_0

    :cond_2
    const v5, 0x7f120e0f

    goto :goto_0

    :cond_3
    const v5, 0x7f120ddb

    :goto_0
    invoke-virtual {p0, v5}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getString(I)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v0, v5}, Landroid/widget/ProgressBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 223
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getBoostTier0Iv()Landroid/widget/ImageView;

    move-result-object v0

    if-lez p2, :cond_4

    const/4 p2, 0x1

    goto :goto_1

    :cond_4
    const/4 p2, 0x0

    :goto_1
    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 224
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getBoostTier1Iv()Landroid/widget/ImageView;

    move-result-object p2

    if-lez p1, :cond_5

    const/4 v0, 0x1

    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    :goto_2
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 225
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getBoostTier2Iv()Landroid/widget/ImageView;

    move-result-object p2

    if-lt p1, v2, :cond_6

    const/4 v0, 0x1

    goto :goto_3

    :cond_6
    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 226
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getBoostTier3Iv()Landroid/widget/ImageView;

    move-result-object p2

    if-lt p1, v1, :cond_7

    const/4 v3, 0x1

    :cond_7
    invoke-virtual {p2, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    return-void
.end method

.method private final configureToolbar(Ljava/lang/String;)V
    .locals 1

    const v0, 0x7f120de6

    .line 209
    invoke-virtual {p0, v0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->setActionBarTitle(I)Lkotlin/Unit;

    .line 210
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->setActionBarSubtitle(Ljava/lang/CharSequence;)Lkotlin/Unit;

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Model;)V
    .locals 26

    move-object/from16 v0, p0

    .line 121
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Model;->component1()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Model;->component2()Lcom/discord/models/domain/ModelGuild;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Model;->component3()Lcom/discord/stores/StorePremiumGuildSubscription$State;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Model;->component4()Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    move-result-object v4

    if-nez v2, :cond_1

    .line 124
    invoke-virtual/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getAppActivity()Lcom/discord/app/AppActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/app/AppActivity;->finish()V

    :cond_0
    return-void

    .line 129
    :cond_1
    sget-object v5, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loading;->INSTANCE:Lcom/discord/stores/StorePremiumGuildSubscription$State$Loading;

    invoke-static {v3, v5}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    const/4 v6, 0x0

    if-nez v5, :cond_1c

    sget-object v5, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loading;->INSTANCE:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loading;

    invoke-static {v4, v5}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    goto/16 :goto_d

    .line 133
    :cond_2
    sget-object v5, Lcom/discord/stores/StorePremiumGuildSubscription$State$Failure;->INSTANCE:Lcom/discord/stores/StorePremiumGuildSubscription$State$Failure;

    invoke-static {v3, v5}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    const/4 v7, 0x1

    if-nez v5, :cond_1b

    sget-object v5, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Failure;->INSTANCE:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Failure;

    invoke-static {v4, v5}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    goto/16 :goto_c

    .line 140
    :cond_3
    instance-of v5, v3, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    const/4 v8, 0x2

    if-eqz v5, :cond_4

    instance-of v5, v4, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;

    if-eqz v5, :cond_4

    .line 141
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v5

    invoke-virtual {v5, v8}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    :cond_4
    if-eqz v3, :cond_1a

    .line 145
    check-cast v3, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    invoke-virtual {v3}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->component1()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v3}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->component2()Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;

    move-result-object v3

    if-eqz v4, :cond_19

    .line 146
    check-cast v4, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;

    invoke-virtual {v4}, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;->getSubscriptions()Ljava/util/List;

    move-result-object v4

    .line 147
    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuild;->getPremiumTier()I

    move-result v9

    .line 148
    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuild;->getPremiumSubscriptionCount()Ljava/lang/Integer;

    move-result-object v10

    if-eqz v10, :cond_5

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    goto :goto_0

    :cond_5
    const/4 v10, 0x0

    .line 149
    :goto_0
    move-object v11, v5

    check-cast v11, Ljava/lang/Iterable;

    .line 357
    instance-of v12, v11, Ljava/util/Collection;

    if-eqz v12, :cond_6

    move-object v12, v11

    check-cast v12, Ljava/util/Collection;

    invoke-interface {v12}, Ljava/util/Collection;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_9

    .line 358
    :cond_6
    invoke-interface {v11}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_7
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_9

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/discord/models/domain/ModelPremiumGuildSubscription;

    .line 149
    invoke-virtual {v12}, Lcom/discord/models/domain/ModelPremiumGuildSubscription;->getGuildId()J

    move-result-wide v12

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v14

    cmp-long v16, v12, v14

    if-nez v16, :cond_8

    const/4 v12, 0x1

    goto :goto_1

    :cond_8
    const/4 v12, 0x0

    :goto_1
    if-eqz v12, :cond_7

    const/4 v11, 0x1

    goto :goto_2

    :cond_9
    const/4 v11, 0x0

    :goto_2
    if-eqz v3, :cond_a

    .line 150
    invoke-virtual {v3}, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;->getExpiresAtTimestamp()J

    move-result-wide v12

    goto :goto_3

    :cond_a
    const-wide/16 v12, 0x0

    .line 151
    :goto_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    cmp-long v3, v12, v14

    if-lez v3, :cond_b

    const/4 v3, 0x1

    goto :goto_4

    :cond_b
    const/4 v3, 0x0

    .line 152
    :goto_4
    move-object v12, v4

    check-cast v12, Ljava/lang/Iterable;

    .line 360
    instance-of v13, v12, Ljava/util/Collection;

    if-eqz v13, :cond_c

    move-object v13, v12

    check-cast v13, Ljava/util/Collection;

    invoke-interface {v13}, Ljava/util/Collection;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_e

    .line 361
    :cond_c
    invoke-interface {v12}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_d
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_e

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/discord/models/domain/ModelSubscription;

    .line 152
    invoke-virtual {v13}, Lcom/discord/models/domain/ModelSubscription;->isAppleSubscription()Z

    move-result v13

    if-eqz v13, :cond_d

    const/4 v12, 0x1

    goto :goto_5

    :cond_e
    const/4 v12, 0x0

    .line 153
    :goto_5
    sget-object v13, Lcom/discord/utilities/nitro/BoostUtils;->INSTANCE:Lcom/discord/utilities/nitro/BoostUtils;

    invoke-virtual {v13, v4}, Lcom/discord/utilities/nitro/BoostUtils;->calculateTotalBoostCount(Ljava/util/List;)I

    move-result v4

    check-cast v5, Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->size()I

    move-result v5

    sub-int/2addr v4, v5

    if-lez v4, :cond_f

    const/4 v4, 0x1

    goto :goto_6

    :cond_f
    const/4 v4, 0x0

    .line 155
    :goto_6
    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->configureToolbar(Ljava/lang/String;)V

    .line 156
    invoke-direct {v0, v9, v10}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->configureProgressBar(II)V

    .line 157
    invoke-direct {v0, v9, v10}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->configureViewpager(II)V

    .line 159
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getBoostNumber()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v9, 0x7f100090

    new-array v13, v7, [Ljava/lang/Object;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v13, v6

    invoke-virtual {v5, v9, v10, v13}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelUser;->getPremiumType()I

    move-result v2

    const v5, 0x7f0600bd

    if-eqz v2, :cond_14

    if-eq v2, v7, :cond_14

    if-eq v2, v8, :cond_10

    goto :goto_8

    :cond_10
    if-eqz v4, :cond_11

    if-nez v3, :cond_11

    .line 185
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getButtonFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    .line 186
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getBoostButton()Landroid/widget/Button;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$configureUI$4;

    invoke-direct {v2, v0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$configureUI$4;-><init>(Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 189
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getButtonInfo()Landroid/widget/TextView;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    .line 191
    :cond_11
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getButtonFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    .line 192
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getManageButton()Landroid/widget/Button;

    move-result-object v1

    sget-object v2, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$configureUI$5;->INSTANCE:Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$configureUI$5;

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 195
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getButtonInfo()Landroid/widget/TextView;

    move-result-object v1

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getButtonInfo()Landroid/widget/TextView;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-static {v2, v5}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/view/View;I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 196
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getButtonInfo()Landroid/widget/TextView;

    move-result-object v4

    const v5, 0x7f0801d2

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xe

    const/4 v10, 0x0

    invoke-static/range {v4 .. v10}, Lcom/discord/utilities/drawable/DrawableCompat;->setCompoundDrawablesCompat$default(Landroid/widget/TextView;IIIIILjava/lang/Object;)V

    .line 197
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getButtonInfo()Landroid/widget/TextView;

    move-result-object v1

    if-eqz v3, :cond_12

    const v2, 0x7f120def

    goto :goto_7

    :cond_12
    if-eqz v11, :cond_13

    const v2, 0x7f120ded

    goto :goto_7

    :cond_13
    const v2, 0x7f120dee

    :goto_7
    invoke-virtual {v0, v2}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v1, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    :goto_8
    return-void

    .line 164
    :cond_14
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getButtonFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    .line 165
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getUpgradeButton()Landroid/widget/Button;

    move-result-object v2

    .line 166
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelUser;->getPremiumType()I

    move-result v1

    if-nez v1, :cond_15

    const v1, 0x7f120de1

    goto :goto_9

    :cond_15
    const v1, 0x7f120de3

    .line 165
    :goto_9
    invoke-virtual {v2, v1}, Landroid/widget/Button;->setText(I)V

    .line 169
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getUpgradeButton()Landroid/widget/Button;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    xor-int/lit8 v2, v12, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v8, v4}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setEnabledAlpha$default(Landroid/view/View;ZFILjava/lang/Object;)V

    .line 170
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getUpgradeButton()Landroid/widget/Button;

    move-result-object v1

    sget-object v2, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$configureUI$2;->INSTANCE:Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$configureUI$2;

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 174
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getButtonInfo()Landroid/widget/TextView;

    move-result-object v1

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getButtonInfo()Landroid/widget/TextView;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    if-eqz v12, :cond_16

    const v3, 0x7f060121

    invoke-static {v2, v3}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/view/View;I)I

    move-result v2

    goto :goto_a

    :cond_16
    invoke-static {v2, v5}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/view/View;I)I

    move-result v2

    :goto_a
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 175
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getButtonInfo()Landroid/widget/TextView;

    move-result-object v13

    if-eqz v12, :cond_17

    const v1, 0x7f0801d3

    const v14, 0x7f0801d3

    goto :goto_b

    :cond_17
    const/4 v14, 0x0

    :goto_b
    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0xe

    const/16 v19, 0x0

    invoke-static/range {v13 .. v19}, Lcom/discord/utilities/drawable/DrawableCompat;->setCompoundDrawablesCompat$default(Landroid/widget/TextView;IIIIILjava/lang/Object;)V

    if-eqz v12, :cond_18

    .line 178
    invoke-virtual/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "requireContext()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const v2, 0x7f120de4

    new-array v3, v7, [Ljava/lang/Object;

    const-string v4, "https://support.apple.com/en-us/HT202039"

    aput-object v4, v3, v6

    invoke-virtual {v0, v2, v3}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "getString(R.string.premi\u2026pple.com/en-us/HT202039\")"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v21, v2

    check-cast v21, Ljava/lang/CharSequence;

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0xc

    const/16 v25, 0x0

    move-object/from16 v20, v1

    invoke-static/range {v20 .. v25}, Lcom/discord/utilities/textprocessing/Parsers;->parseMaskedLinks$default(Landroid/content/Context;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 180
    :cond_18
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getButtonInfo()Landroid/widget/TextView;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 181
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getButtonInfo()Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$configureUI$3;

    invoke-direct {v2, v0, v12}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$configureUI$3;-><init>(Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;Z)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    .line 146
    :cond_19
    new-instance v1, Lkotlin/r;

    const-string v2, "null cannot be cast to non-null type com.discord.stores.StoreSubscriptions.SubscriptionsState.Loaded"

    invoke-direct {v1, v2}, Lkotlin/r;-><init>(Ljava/lang/String;)V

    throw v1

    .line 145
    :cond_1a
    new-instance v1, Lkotlin/r;

    const-string v2, "null cannot be cast to non-null type com.discord.stores.StorePremiumGuildSubscription.State.Loaded"

    invoke-direct {v1, v2}, Lkotlin/r;-><init>(Ljava/lang/String;)V

    throw v1

    .line 134
    :cond_1b
    :goto_c
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    .line 135
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getRetry()Landroid/widget/Button;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$configureUI$1;

    invoke-direct {v2, v0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$configureUI$1;-><init>(Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    .line 130
    :cond_1c
    :goto_d
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    return-void
.end method

.method private final configureViewpager(II)V
    .locals 1

    .line 230
    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->pagerAdapter:Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$PerksPagerAdapter;

    invoke-virtual {v0, p1}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$PerksPagerAdapter;->setPremiumTier(I)V

    .line 231
    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->pagerAdapter:Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$PerksPagerAdapter;

    invoke-virtual {v0, p2}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$PerksPagerAdapter;->setSubscriptionCount(I)V

    .line 232
    iget-object p2, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->pagerAdapter:Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$PerksPagerAdapter;

    invoke-virtual {p2}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$PerksPagerAdapter;->configureViews()V

    .line 234
    iget-boolean p2, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->wasPagerPageSet:Z

    if-nez p2, :cond_0

    .line 235
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getViewPager()Lcom/discord/utilities/simple_pager/SimplePager;

    move-result-object p2

    const/4 v0, 0x0

    invoke-virtual {p2, p1, v0}, Lcom/discord/utilities/simple_pager/SimplePager;->setCurrentItem(IZ)V

    .line 236
    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->configureLevelBubbles(I)V

    const/4 p1, 0x1

    .line 237
    iput-boolean p1, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->wasPagerPageSet:Z

    :cond_0
    return-void
.end method

.method public static final create(Landroid/content/Context;J)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->Companion:Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Companion;

    invoke-virtual {v0, p0, p1, p2}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Companion;->create(Landroid/content/Context;J)V

    return-void
.end method

.method private final fetchData()V
    .locals 1

    .line 258
    sget-object v0, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->INSTANCE:Lcom/discord/stores/StorePremiumGuildSubscription$Actions;

    .line 259
    invoke-virtual {v0}, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->fetchUserGuildPremiumState()V

    .line 263
    invoke-static {}, Lcom/discord/stores/StoreSubscriptions$Actions;->fetchSubscriptions()V

    return-void
.end method

.method private final getBoostButton()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->boostButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getBoostInfo()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->boostInfo$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getBoostNumber()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->boostNumber$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getBoostProgressBar()Landroid/widget/ProgressBar;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->boostProgressBar$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    return-object v0
.end method

.method private final getBoostTier0Iv()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->boostTier0Iv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getBoostTier1Iv()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->boostTier1Iv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getBoostTier1Tv()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->boostTier1Tv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getBoostTier2Iv()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->boostTier2Iv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getBoostTier2Tv()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->boostTier2Tv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getBoostTier3Iv()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->boostTier3Iv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getBoostTier3Tv()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->boostTier3Tv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getButtonFlipper()Lcom/discord/app/AppViewFlipper;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->buttonFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/app/AppViewFlipper;

    return-object v0
.end method

.method private final getButtonInfo()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->buttonInfo$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x10

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getFlipper()Lcom/discord/app/AppViewFlipper;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/app/AppViewFlipper;

    return-object v0
.end method

.method private final getGuildId()J
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->guildId$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method private final getManageButton()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->manageButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xf

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getRetry()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->retry$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getUpgradeButton()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->upgradeButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getViewPager()Lcom/discord/utilities/simple_pager/SimplePager;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->viewPager$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x11

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/simple_pager/SimplePager;

    return-object v0
.end method


# virtual methods
.method public final getContentViewResId()I
    .locals 1

    const v0, 0x7f0d016d

    return v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .line 80
    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onActivityCreated(Landroid/os/Bundle;)V

    const/4 p1, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x3

    .line 82
    invoke-static {p0, v0, p1, v1, p1}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled$default(Lcom/discord/app/AppFragment;ZLjava/lang/Integer;ILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    .line 83
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getViewPager()Lcom/discord/utilities/simple_pager/SimplePager;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/discord/utilities/simple_pager/SimplePager;->setWrapHeight(Z)V

    .line 84
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getViewPager()Lcom/discord/utilities/simple_pager/SimplePager;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->pagerAdapter:Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$PerksPagerAdapter;

    check-cast v0, Landroidx/viewpager/widget/PagerAdapter;

    invoke-virtual {p1, v0}, Lcom/discord/utilities/simple_pager/SimplePager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    .line 85
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getViewPager()Lcom/discord/utilities/simple_pager/SimplePager;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$onActivityCreated$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$onActivityCreated$1;-><init>(Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;)V

    check-cast v0, Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

    invoke-virtual {p1, v0}, Lcom/discord/utilities/simple_pager/SimplePager;->addOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    return-void
.end method

.method public final onViewBound(Landroid/view/View;)V
    .locals 10

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 97
    sget-object v2, Lcom/discord/app/e;->uy:Lcom/discord/app/e;

    sget-wide v2, Lcom/discord/app/e;->ux:J

    invoke-static {v2, v3}, Lcom/discord/app/e;->k(J)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const v2, 0x7f120dde

    invoke-virtual {p0, v2, v1}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "getString(R.string.premi\u2026elpDesk.SERVER_BOOSTING))"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getBoostInfo()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string p1, "view.context"

    invoke-static {v4, p1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v5, v1

    check-cast v5, Ljava/lang/CharSequence;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0xc

    const/4 v9, 0x0

    invoke-static/range {v4 .. v9}, Lcom/discord/utilities/textprocessing/Parsers;->parseMaskedLinks$default(Landroid/content/Context;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 p1, 0x4

    new-array v1, p1, [Landroid/widget/ImageView;

    .line 100
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getBoostTier0Iv()Landroid/widget/ImageView;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getBoostTier1Iv()Landroid/widget/ImageView;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getBoostTier2Iv()Landroid/widget/ImageView;

    move-result-object v2

    const/4 v4, 0x2

    aput-object v2, v1, v4

    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getBoostTier3Iv()Landroid/widget/ImageView;

    move-result-object v2

    const/4 v5, 0x3

    aput-object v2, v1, v5

    invoke-static {v1}, Lkotlin/a/m;->k([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->levelBackgrounds:Ljava/util/List;

    new-array p1, p1, [Landroid/widget/TextView;

    const/4 v1, 0x0

    aput-object v1, p1, v3

    .line 101
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getBoostTier1Tv()Landroid/widget/TextView;

    move-result-object v1

    aput-object v1, p1, v0

    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getBoostTier2Tv()Landroid/widget/TextView;

    move-result-object v0

    aput-object v0, p1, v4

    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getBoostTier3Tv()Landroid/widget/TextView;

    move-result-object v0

    aput-object v0, p1, v5

    invoke-static {p1}, Lkotlin/a/m;->k([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->levelText:Ljava/util/List;

    .line 103
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->fetchData()V

    return-void
.end method

.method public final onViewBoundOrOnResume()V
    .locals 13

    .line 107
    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    .line 109
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getGuildId()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getGuildId()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v4, v0, v2

    if-nez v4, :cond_1

    .line 110
    :cond_0
    invoke-virtual {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getAppActivity()Lcom/discord/app/AppActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/app/AppActivity;->finish()V

    .line 113
    :cond_1
    sget-object v0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Model;->Companion:Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Model$Companion;

    .line 114
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->getGuildId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Model$Companion;->get(J)Lrx/Observable;

    move-result-object v0

    .line 115
    invoke-virtual {v0}, Lrx/Observable;->JL()Lrx/Observable;

    move-result-object v0

    const-string v1, "Model\n        .get(guild\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    move-object v1, p0

    check-cast v1, Lcom/discord/app/AppComponent;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v3, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    .line 117
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {p0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;->requireContext()Landroid/content/Context;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    new-instance v0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$onViewBoundOrOnResume$1;

    move-object v1, p0

    check-cast v1, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;

    invoke-direct {v0, v1}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus;)V

    move-object v10, v0

    check-cast v10, Lkotlin/jvm/functions/Function1;

    const/16 v11, 0x1c

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
