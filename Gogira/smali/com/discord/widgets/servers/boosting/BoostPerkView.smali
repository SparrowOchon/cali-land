.class public final Lcom/discord/widgets/servers/boosting/BoostPerkView;
.super Landroid/widget/RelativeLayout;
.source "BoostPerkView.kt"


# instance fields
.field private adapter:Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter;

.field private container:Landroid/widget/LinearLayout;

.field private contentRecycler:Landroidx/recyclerview/widget/RecyclerView;

.field private contentText:Landroid/widget/TextView;

.field private header:Landroid/view/View;

.field private headerBoostText:Landroid/widget/TextView;

.field private headerText:Landroid/widget/TextView;

.field private headerUnlocked:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/BoostPerkView;->initialize()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/BoostPerkView;->initialize()V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 40
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/servers/boosting/BoostPerkView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 36
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/servers/boosting/BoostPerkView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private final initialize()V
    .locals 4

    .line 136
    invoke-virtual {p0}, Lcom/discord/widgets/servers/boosting/BoostPerkView;->getContext()Landroid/content/Context;

    move-result-object v0

    move-object v1, p0

    check-cast v1, Landroid/view/ViewGroup;

    const v2, 0x7f0d0093

    invoke-static {v0, v2, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const v0, 0x7f0a0504

    .line 138
    invoke-virtual {p0, v0}, Lcom/discord/widgets/servers/boosting/BoostPerkView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.perks_level_header)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostPerkView;->header:Landroid/view/View;

    const v0, 0x7f0a0506

    .line 139
    invoke-virtual {p0, v0}, Lcom/discord/widgets/servers/boosting/BoostPerkView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.perks_level_header_text)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostPerkView;->headerText:Landroid/widget/TextView;

    const v0, 0x7f0a0505

    .line 140
    invoke-virtual {p0, v0}, Lcom/discord/widgets/servers/boosting/BoostPerkView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.perks_level_header_boosts)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostPerkView;->headerBoostText:Landroid/widget/TextView;

    const v0, 0x7f0a0507

    .line 141
    invoke-virtual {p0, v0}, Lcom/discord/widgets/servers/boosting/BoostPerkView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.perks_level_header_unlocked)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostPerkView;->headerUnlocked:Landroid/view/View;

    const v0, 0x7f0a0502

    .line 142
    invoke-virtual {p0, v0}, Lcom/discord/widgets/servers/boosting/BoostPerkView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.perks_level_contents_header)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostPerkView;->contentText:Landroid/widget/TextView;

    const v0, 0x7f0a0503

    .line 143
    invoke-virtual {p0, v0}, Lcom/discord/widgets/servers/boosting/BoostPerkView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.perks_level_contents_recycler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostPerkView;->contentRecycler:Landroidx/recyclerview/widget/RecyclerView;

    const v0, 0x7f0a0231

    .line 144
    invoke-virtual {p0, v0}, Lcom/discord/widgets/servers/boosting/BoostPerkView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostPerkView;->container:Landroid/widget/LinearLayout;

    .line 146
    sget-object v0, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->Companion:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;

    new-instance v1, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter;

    iget-object v2, p0, Lcom/discord/widgets/servers/boosting/BoostPerkView;->contentRecycler:Landroidx/recyclerview/widget/RecyclerView;

    const-string v3, "contentRecycler"

    if-nez v2, :cond_0

    invoke-static {v3}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_0
    invoke-direct {v1, v2}, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    check-cast v1, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    invoke-virtual {v0, v1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter;

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostPerkView;->adapter:Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter;

    .line 147
    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostPerkView;->contentRecycler:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_1

    invoke-static {v3}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/discord/widgets/servers/boosting/BoostPerkView$initialize$1;

    invoke-direct {v1}, Lcom/discord/widgets/servers/boosting/BoostPerkView$initialize$1;-><init>()V

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$OnItemTouchListener;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->addOnItemTouchListener(Landroidx/recyclerview/widget/RecyclerView$OnItemTouchListener;)V

    return-void
.end method


# virtual methods
.method public final configure(II)V
    .locals 18

    move-object/from16 v0, p0

    move/from16 v1, p1

    .line 47
    sget-object v2, Lcom/discord/widgets/servers/boosting/BoostPerkView$configure$1;->INSTANCE:Lcom/discord/widgets/servers/boosting/BoostPerkView$configure$1;

    const/4 v3, 0x1

    const/4 v4, 0x0

    move/from16 v5, p2

    if-lt v5, v1, :cond_0

    const/4 v5, 0x1

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    :goto_0
    const-string v6, "header"

    if-eqz v5, :cond_2

    .line 52
    iget-object v7, v0, Lcom/discord/widgets/servers/boosting/BoostPerkView;->header:Landroid/view/View;

    if-nez v7, :cond_1

    invoke-static {v6}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_1
    const v6, 0x7f0800c2

    invoke-virtual {v7, v6}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1

    .line 54
    :cond_2
    iget-object v7, v0, Lcom/discord/widgets/servers/boosting/BoostPerkView;->header:Landroid/view/View;

    if-nez v7, :cond_3

    invoke-static {v6}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_3
    move-object v6, v0

    check-cast v6, Landroid/view/View;

    const v8, 0x7f0402ac

    invoke-static {v6, v8}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result v6

    invoke-virtual {v7, v6}, Landroid/view/View;->setBackgroundColor(I)V

    :goto_1
    const/4 v6, 0x3

    const/4 v7, 0x2

    if-eq v1, v3, :cond_6

    if-eq v1, v7, :cond_5

    if-eq v1, v6, :cond_4

    const/4 v8, 0x0

    goto :goto_2

    :cond_4
    const/16 v8, 0x14

    goto :goto_2

    :cond_5
    const/16 v8, 0xa

    goto :goto_2

    :cond_6
    const/4 v8, 0x2

    .line 64
    :goto_2
    iget-object v9, v0, Lcom/discord/widgets/servers/boosting/BoostPerkView;->headerText:Landroid/widget/TextView;

    const-string v10, "headerText"

    if-nez v9, :cond_7

    invoke-static {v10}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_7
    if-eq v1, v3, :cond_a

    if-eq v1, v7, :cond_9

    if-eq v1, v6, :cond_8

    const/4 v11, 0x0

    goto :goto_3

    :cond_8
    const v11, 0x7f1208bc

    goto :goto_3

    :cond_9
    const v11, 0x7f1208bb

    goto :goto_3

    :cond_a
    const v11, 0x7f1208ba

    :goto_3
    invoke-static {v0, v11}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v11

    check-cast v11, Ljava/lang/CharSequence;

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eq v1, v3, :cond_f

    if-eq v1, v7, :cond_d

    if-eq v1, v6, :cond_b

    const/4 v12, 0x0

    goto :goto_4

    :cond_b
    if-eqz v5, :cond_c

    const v9, 0x7f080323

    const v12, 0x7f080323

    goto :goto_4

    :cond_c
    const v9, 0x7f080324

    const v12, 0x7f080324

    goto :goto_4

    :cond_d
    if-eqz v5, :cond_e

    const v9, 0x7f080321

    const v12, 0x7f080321

    goto :goto_4

    :cond_e
    const v9, 0x7f080322

    const v12, 0x7f080322

    goto :goto_4

    :cond_f
    if-eqz v5, :cond_10

    const v9, 0x7f08031f

    const v12, 0x7f08031f

    goto :goto_4

    :cond_10
    const v9, 0x7f080320

    const v12, 0x7f080320

    .line 76
    :goto_4
    iget-object v11, v0, Lcom/discord/widgets/servers/boosting/BoostPerkView;->headerText:Landroid/widget/TextView;

    if-nez v11, :cond_11

    invoke-static {v10}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_11
    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0xe

    const/16 v17, 0x0

    invoke-static/range {v11 .. v17}, Lcom/discord/utilities/drawable/DrawableCompat;->setCompoundDrawablesCompat$default(Landroid/widget/TextView;IIIIILjava/lang/Object;)V

    .line 77
    iget-object v9, v0, Lcom/discord/widgets/servers/boosting/BoostPerkView;->headerText:Landroid/widget/TextView;

    if-nez v9, :cond_12

    invoke-static {v10}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_12
    invoke-virtual/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/BoostPerkView;->getContext()Landroid/content/Context;

    move-result-object v10

    if-eqz v5, :cond_13

    const v11, 0x7f060142

    invoke-static {v10, v11}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result v10

    goto :goto_5

    :cond_13
    const v11, 0x7f040298

    invoke-static {v10, v11}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v10

    :goto_5
    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 79
    iget-object v9, v0, Lcom/discord/widgets/servers/boosting/BoostPerkView;->headerBoostText:Landroid/widget/TextView;

    const-string v10, "headerBoostText"

    if-nez v9, :cond_14

    invoke-static {v10}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_14
    invoke-virtual/range {p0 .. p0}, Lcom/discord/widgets/servers/boosting/BoostPerkView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f10006d

    new-array v13, v3, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v13, v4

    invoke-virtual {v11, v12, v8, v13}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    check-cast v8, Ljava/lang/CharSequence;

    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    iget-object v8, v0, Lcom/discord/widgets/servers/boosting/BoostPerkView;->headerBoostText:Landroid/widget/TextView;

    if-nez v8, :cond_15

    invoke-static {v10}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_15
    check-cast v8, Landroid/view/View;

    xor-int/lit8 v9, v5, 0x1

    const/4 v10, 0x0

    invoke-static {v8, v9, v4, v7, v10}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    .line 81
    iget-object v8, v0, Lcom/discord/widgets/servers/boosting/BoostPerkView;->headerUnlocked:Landroid/view/View;

    if-nez v8, :cond_16

    const-string v9, "headerUnlocked"

    invoke-static {v9}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_16
    invoke-static {v8, v5, v4, v7, v10}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    .line 83
    iget-object v8, v0, Lcom/discord/widgets/servers/boosting/BoostPerkView;->contentText:Landroid/widget/TextView;

    if-nez v8, :cond_17

    const-string v9, "contentText"

    invoke-static {v9}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_17
    if-eq v1, v3, :cond_18

    const v9, 0x7f1208b8

    .line 85
    invoke-static {v0, v9}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v9

    check-cast v9, Ljava/lang/CharSequence;

    goto :goto_6

    :cond_18
    const v9, 0x7f1208b6

    .line 84
    invoke-static {v0, v9}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v9

    check-cast v9, Ljava/lang/CharSequence;

    .line 83
    :goto_6
    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v8, 0x32

    const/4 v9, 0x5

    const/4 v10, 0x4

    const v11, 0x7f1208b3

    const v12, 0x7f0802a6

    const v13, 0x7f1208b4

    const v14, 0x7f080367

    if-eq v1, v3, :cond_1b

    if-eq v1, v7, :cond_1a

    if-eq v1, v6, :cond_19

    .line 1069
    sget-object v1, Lkotlin/a/y;->bkh:Lkotlin/a/y;

    check-cast v1, Ljava/util/List;

    goto/16 :goto_7

    :cond_19
    new-array v1, v10, [Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;

    .line 119
    new-instance v8, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;

    invoke-virtual {v2, v14, v5}, Lcom/discord/widgets/servers/boosting/BoostPerkView$configure$1;->invoke(IZ)I

    move-result v9

    new-array v10, v7, [Ljava/lang/Object;

    const/16 v14, 0x64

    .line 120
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v10, v4

    invoke-static {v6, v4}, Lcom/discord/models/domain/ModelGuild;->getEmojiMaxCount(IZ)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v10, v3

    invoke-static {v0, v13, v10}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 119
    invoke-direct {v8, v9, v10}, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;-><init>(ILjava/lang/String;)V

    aput-object v8, v1, v4

    .line 121
    new-instance v8, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;

    invoke-virtual {v2, v12, v5}, Lcom/discord/widgets/servers/boosting/BoostPerkView$configure$1;->invoke(IZ)I

    move-result v9

    new-array v10, v3, [Ljava/lang/Object;

    .line 122
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    sget-object v13, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {v12, v13}, Lcom/discord/models/domain/ModelGuild;->getMaxVoiceBitrateKbps(Ljava/lang/Integer;Ljava/lang/Boolean;)I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v4

    invoke-static {v0, v11, v10}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 121
    invoke-direct {v8, v9, v10}, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;-><init>(ILjava/lang/String;)V

    aput-object v8, v1, v3

    .line 123
    new-instance v8, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;

    const v9, 0x7f080394

    invoke-virtual {v2, v9, v5}, Lcom/discord/widgets/servers/boosting/BoostPerkView$configure$1;->invoke(IZ)I

    move-result v9

    const v10, 0x7f1208b5

    new-array v11, v3, [Ljava/lang/Object;

    const v12, 0x7f1205a4

    new-array v3, v3, [Ljava/lang/Object;

    .line 124
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-static {v13}, Lcom/discord/models/domain/ModelGuild;->getMaxFileSizeMB(Ljava/lang/Integer;)I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v3, v4

    invoke-static {v0, v12, v3}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v11, v4

    invoke-static {v0, v10, v11}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 123
    invoke-direct {v8, v9, v3}, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;-><init>(ILjava/lang/String;)V

    aput-object v8, v1, v7

    .line 125
    new-instance v3, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;

    const v4, 0x7f08036d

    invoke-virtual {v2, v4, v5}, Lcom/discord/widgets/servers/boosting/BoostPerkView$configure$1;->invoke(IZ)I

    move-result v2

    const v4, 0x7f1208b2

    .line 126
    invoke-static {v0, v4}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v4

    .line 125
    invoke-direct {v3, v2, v4}, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;-><init>(ILjava/lang/String;)V

    aput-object v3, v1, v6

    .line 118
    invoke-static {v1}, Lkotlin/a/m;->k([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    goto/16 :goto_7

    :cond_1a
    new-array v1, v9, [Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;

    .line 105
    new-instance v9, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;

    invoke-virtual {v2, v14, v5}, Lcom/discord/widgets/servers/boosting/BoostPerkView$configure$1;->invoke(IZ)I

    move-result v14

    new-array v15, v7, [Ljava/lang/Object;

    .line 106
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v15, v4

    invoke-static {v7, v4}, Lcom/discord/models/domain/ModelGuild;->getEmojiMaxCount(IZ)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v15, v3

    invoke-static {v0, v13, v15}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 105
    invoke-direct {v9, v14, v8}, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;-><init>(ILjava/lang/String;)V

    aput-object v9, v1, v4

    .line 107
    new-instance v8, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;

    invoke-virtual {v2, v12, v5}, Lcom/discord/widgets/servers/boosting/BoostPerkView$configure$1;->invoke(IZ)I

    move-result v9

    new-array v12, v3, [Ljava/lang/Object;

    .line 108
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    sget-object v14, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {v13, v14}, Lcom/discord/models/domain/ModelGuild;->getMaxVoiceBitrateKbps(Ljava/lang/Integer;Ljava/lang/Boolean;)I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v12, v4

    invoke-static {v0, v11, v12}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 107
    invoke-direct {v8, v9, v11}, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;-><init>(ILjava/lang/String;)V

    aput-object v8, v1, v3

    .line 109
    new-instance v8, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;

    const v9, 0x7f080394

    invoke-virtual {v2, v9, v5}, Lcom/discord/widgets/servers/boosting/BoostPerkView$configure$1;->invoke(IZ)I

    move-result v9

    const v11, 0x7f1208b5

    new-array v12, v3, [Ljava/lang/Object;

    const v13, 0x7f1205a4

    new-array v3, v3, [Ljava/lang/Object;

    .line 110
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-static {v14}, Lcom/discord/models/domain/ModelGuild;->getMaxFileSizeMB(Ljava/lang/Integer;)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v3, v4

    invoke-static {v0, v13, v3}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v12, v4

    invoke-static {v0, v11, v12}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 109
    invoke-direct {v8, v9, v3}, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;-><init>(ILjava/lang/String;)V

    aput-object v8, v1, v7

    .line 111
    new-instance v3, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;

    const v4, 0x7f0802c2

    invoke-virtual {v2, v4, v5}, Lcom/discord/widgets/servers/boosting/BoostPerkView$configure$1;->invoke(IZ)I

    move-result v4

    const v7, 0x7f1208b0

    .line 112
    invoke-static {v0, v7}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v7

    .line 111
    invoke-direct {v3, v4, v7}, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;-><init>(ILjava/lang/String;)V

    aput-object v3, v1, v6

    .line 113
    new-instance v3, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;

    const v4, 0x7f0802a1

    invoke-virtual {v2, v4, v5}, Lcom/discord/widgets/servers/boosting/BoostPerkView$configure$1;->invoke(IZ)I

    move-result v2

    const v4, 0x7f1208b1

    .line 114
    invoke-static {v0, v4}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v4

    .line 113
    invoke-direct {v3, v2, v4}, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;-><init>(ILjava/lang/String;)V

    aput-object v3, v1, v10

    .line 104
    invoke-static {v1}, Lkotlin/a/m;->k([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    goto/16 :goto_7

    :cond_1b
    new-array v1, v9, [Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;

    .line 91
    new-instance v9, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;

    invoke-virtual {v2, v14, v5}, Lcom/discord/widgets/servers/boosting/BoostPerkView$configure$1;->invoke(IZ)I

    move-result v14

    new-array v15, v7, [Ljava/lang/Object;

    .line 92
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v15, v4

    invoke-static {v3, v4}, Lcom/discord/models/domain/ModelGuild;->getEmojiMaxCount(IZ)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v15, v3

    invoke-static {v0, v13, v15}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 91
    invoke-direct {v9, v14, v8}, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;-><init>(ILjava/lang/String;)V

    aput-object v9, v1, v4

    .line 93
    new-instance v8, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;

    invoke-virtual {v2, v12, v5}, Lcom/discord/widgets/servers/boosting/BoostPerkView$configure$1;->invoke(IZ)I

    move-result v9

    new-array v12, v3, [Ljava/lang/Object;

    .line 94
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    sget-object v14, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {v13, v14}, Lcom/discord/models/domain/ModelGuild;->getMaxVoiceBitrateKbps(Ljava/lang/Integer;Ljava/lang/Boolean;)I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v12, v4

    invoke-static {v0, v11, v12}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 93
    invoke-direct {v8, v9, v4}, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;-><init>(ILjava/lang/String;)V

    aput-object v8, v1, v3

    .line 95
    new-instance v3, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;

    const v4, 0x7f0802fe

    invoke-virtual {v2, v4, v5}, Lcom/discord/widgets/servers/boosting/BoostPerkView$configure$1;->invoke(IZ)I

    move-result v4

    const v8, 0x7f1208ad

    .line 96
    invoke-static {v0, v8}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v8

    .line 95
    invoke-direct {v3, v4, v8}, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;-><init>(ILjava/lang/String;)V

    aput-object v3, v1, v7

    .line 97
    new-instance v3, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;

    const v4, 0x7f0802c2

    invoke-virtual {v2, v4, v5}, Lcom/discord/widgets/servers/boosting/BoostPerkView$configure$1;->invoke(IZ)I

    move-result v4

    const v7, 0x7f1208ae

    .line 98
    invoke-static {v0, v7}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v7

    .line 97
    invoke-direct {v3, v4, v7}, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;-><init>(ILjava/lang/String;)V

    aput-object v3, v1, v6

    .line 99
    new-instance v3, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;

    const v4, 0x7f0802a1

    invoke-virtual {v2, v4, v5}, Lcom/discord/widgets/servers/boosting/BoostPerkView$configure$1;->invoke(IZ)I

    move-result v2

    const v4, 0x7f1208af

    .line 100
    invoke-static {v0, v4}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v4

    .line 99
    invoke-direct {v3, v2, v4}, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter$BoostPerkViewListItem;-><init>(ILjava/lang/String;)V

    aput-object v3, v1, v10

    .line 90
    invoke-static {v1}, Lkotlin/a/m;->k([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 132
    :goto_7
    iget-object v2, v0, Lcom/discord/widgets/servers/boosting/BoostPerkView;->adapter:Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter;

    if-nez v2, :cond_1c

    const-string v3, "adapter"

    invoke-static {v3}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_1c
    invoke-virtual {v2, v1}, Lcom/discord/widgets/servers/boosting/BoostPerkViewAdapter;->configure(Ljava/util/List;)V

    return-void
.end method
