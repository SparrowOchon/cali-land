.class final Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation$onViewBound$1;
.super Ljava/lang/Object;
.source "WidgetServerBoostConfirmation.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;


# direct methods
.method constructor <init>(Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation$onViewBound$1;->this$0:Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    .line 61
    iget-object p1, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation$onViewBound$1;->this$0:Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;

    invoke-static {p1}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->access$getViewModel$p(Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;)Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation$onViewBound$1;->this$0:Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;

    invoke-static {v0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->access$getGuildId$p(Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->boostServer(J)V

    return-void
.end method
