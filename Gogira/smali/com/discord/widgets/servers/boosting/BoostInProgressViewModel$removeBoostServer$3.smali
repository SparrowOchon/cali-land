.class final Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$removeBoostServer$3;
.super Lkotlin/jvm/internal/l;
.source "BoostInProgressViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->removeBoostServer(JJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/l;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Void;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic $subscriptionId:J

.field final synthetic this$0:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;


# direct methods
.method constructor <init>(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;J)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$removeBoostServer$3;->this$0:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;

    iput-wide p2, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$removeBoostServer$3;->$subscriptionId:J

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/l;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$removeBoostServer$3;->invoke(Ljava/lang/Void;)V

    sget-object p1, Lkotlin/Unit;->bjS:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/Void;)V
    .locals 2

    .line 84
    sget-object p1, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->INSTANCE:Lcom/discord/stores/StorePremiumGuildSubscription$Actions;

    .line 85
    iget-wide v0, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$removeBoostServer$3;->$subscriptionId:J

    invoke-virtual {p1, v0, v1}, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->removeNewPremiumGuildSubscription(J)V

    .line 86
    iget-object p1, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$removeBoostServer$3;->this$0:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;

    invoke-static {p1}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->access$handleBoostCompleted(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;)V

    return-void
.end method
