.class public final Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;
.super Landroidx/lifecycle/ViewModel;
.source "BoostInProgressViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;,
        Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;
    }
.end annotation


# instance fields
.field private final boostStateSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;",
            ">;"
        }
    .end annotation
.end field

.field private boostSubscription:Lrx/Subscription;

.field private state:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;

.field private storeSubscription:Lrx/Subscription;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 17
    invoke-direct {p0}, Landroidx/lifecycle/ViewModel;-><init>()V

    .line 22
    new-instance v0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Uninitialized;

    sget-object v1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;->NOT_IN_PROGRESS:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    invoke-direct {v0, v1}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Uninitialized;-><init>(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;)V

    check-cast v0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->state:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;

    .line 23
    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->state:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;

    invoke-static {v0}, Lrx/subjects/BehaviorSubject;->bT(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    const-string v1, "BehaviorSubject.create(state)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->boostStateSubject:Lrx/subjects/BehaviorSubject;

    return-void
.end method

.method public static final synthetic access$getBoostSubscription$p(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;)Lrx/Subscription;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->boostSubscription:Lrx/Subscription;

    return-object p0
.end method

.method public static final synthetic access$getStoreSubscription$p(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;)Lrx/Subscription;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->storeSubscription:Lrx/Subscription;

    return-object p0
.end method

.method public static final synthetic access$handleBoostCompleted(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->handleBoostCompleted()V

    return-void
.end method

.method public static final synthetic access$handleBoostError(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->handleBoostError()V

    return-void
.end method

.method public static final synthetic access$handleGetGuild(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;Lcom/discord/models/domain/ModelGuild;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->handleGetGuild(Lcom/discord/models/domain/ModelGuild;)V

    return-void
.end method

.method public static final synthetic access$setBoostSubscription$p(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;Lrx/Subscription;)V
    .locals 0

    .line 17
    iput-object p1, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->boostSubscription:Lrx/Subscription;

    return-void
.end method

.method public static final synthetic access$setStoreSubscription$p(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;Lrx/Subscription;)V
    .locals 0

    .line 17
    iput-object p1, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->storeSubscription:Lrx/Subscription;

    return-void
.end method

.method private final handleBoostCompleted()V
    .locals 3

    .line 119
    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->state:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;

    .line 121
    instance-of v1, v0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Uninitialized;

    if-eqz v1, :cond_0

    .line 122
    new-instance v0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Uninitialized;

    sget-object v1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;->COMPLETED:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    invoke-direct {v0, v1}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Uninitialized;-><init>(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;)V

    check-cast v0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;

    goto :goto_0

    .line 124
    :cond_0
    instance-of v1, v0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;

    if-eqz v1, :cond_1

    .line 125
    new-instance v1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;

    sget-object v2, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;->COMPLETED:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    check-cast v0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;

    invoke-virtual {v0}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;-><init>(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;Lcom/discord/models/domain/ModelGuild;)V

    move-object v0, v1

    check-cast v0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;

    .line 120
    :goto_0
    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->state:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;

    .line 129
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->publishState()V

    return-void

    .line 125
    :cond_1
    new-instance v0, Lkotlin/k;

    invoke-direct {v0}, Lkotlin/k;-><init>()V

    throw v0
.end method

.method private final handleBoostError()V
    .locals 3

    .line 134
    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->state:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;

    .line 136
    instance-of v1, v0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Uninitialized;

    if-eqz v1, :cond_0

    .line 137
    new-instance v0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Uninitialized;

    sget-object v1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;->ERROR:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    invoke-direct {v0, v1}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Uninitialized;-><init>(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;)V

    check-cast v0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;

    goto :goto_0

    .line 139
    :cond_0
    instance-of v1, v0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;

    if-eqz v1, :cond_1

    .line 140
    new-instance v1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;

    sget-object v2, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;->ERROR:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    check-cast v0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;

    invoke-virtual {v0}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;-><init>(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;Lcom/discord/models/domain/ModelGuild;)V

    move-object v0, v1

    check-cast v0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;

    .line 135
    :goto_0
    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->state:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;

    .line 144
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->publishState()V

    return-void

    .line 140
    :cond_1
    new-instance v0, Lkotlin/k;

    invoke-direct {v0}, Lkotlin/k;-><init>()V

    throw v0
.end method

.method private final handleBoostStarted()V
    .locals 3

    .line 104
    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->state:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;

    .line 106
    instance-of v1, v0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Uninitialized;

    if-eqz v1, :cond_0

    .line 107
    new-instance v0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Uninitialized;

    sget-object v1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;->CALL_IN_PROGRESS:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    invoke-direct {v0, v1}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Uninitialized;-><init>(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;)V

    check-cast v0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;

    goto :goto_0

    .line 109
    :cond_0
    instance-of v1, v0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;

    if-eqz v1, :cond_1

    .line 110
    new-instance v1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;

    sget-object v2, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;->CALL_IN_PROGRESS:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    check-cast v0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;

    invoke-virtual {v0}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;-><init>(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;Lcom/discord/models/domain/ModelGuild;)V

    move-object v0, v1

    check-cast v0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;

    .line 105
    :goto_0
    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->state:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;

    .line 114
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->publishState()V

    return-void

    .line 110
    :cond_1
    new-instance v0, Lkotlin/k;

    invoke-direct {v0}, Lkotlin/k;-><init>()V

    throw v0
.end method

.method private final handleGetGuild(Lcom/discord/models/domain/ModelGuild;)V
    .locals 2

    .line 97
    new-instance v0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;

    iget-object v1, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->state:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;

    invoke-virtual {v1}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;->getBoostState()Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State$Loaded;-><init>(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;Lcom/discord/models/domain/ModelGuild;)V

    check-cast v0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;

    iput-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->state:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;

    .line 99
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->publishState()V

    return-void
.end method

.method private final publishState()V
    .locals 2

    .line 92
    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->boostStateSubject:Lrx/subjects/BehaviorSubject;

    iget-object v1, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->state:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final boostServer(J)V
    .locals 9

    .line 50
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->handleBoostStarted()V

    .line 52
    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    .line 54
    invoke-virtual {v0, p1, p2}, Lcom/discord/utilities/rest/RestAPI;->subscribeToGuild(J)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 55
    invoke-static {p1, p2, v0, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    .line 56
    invoke-static {}, Lrx/android/b/a;->JY()Lrx/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->a(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    const-string p1, "RestAPI\n        .api\n   \u2026dSchedulers.mainThread())"

    invoke-static {v0, p1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    const-class v1, Lcom/discord/stores/StorePremiumGuildSubscription;

    .line 59
    new-instance p1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$boostServer$1;

    invoke-direct {p1, p0}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$boostServer$1;-><init>(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;)V

    move-object v3, p1

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 60
    new-instance p1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$boostServer$2;

    invoke-direct {p1, p0}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$boostServer$2;-><init>(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;)V

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 61
    new-instance p1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$boostServer$3;

    invoke-direct {p1, p0}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$boostServer$3;-><init>(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;)V

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x12

    const/4 v8, 0x0

    .line 57
    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final getState()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$State;",
            ">;"
        }
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->boostStateSubject:Lrx/subjects/BehaviorSubject;

    check-cast v0, Lrx/Observable;

    return-object v0
.end method

.method public final onCleared()V
    .locals 1

    .line 26
    invoke-super {p0}, Landroidx/lifecycle/ViewModel;->onCleared()V

    .line 27
    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->storeSubscription:Lrx/Subscription;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    .line 28
    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->boostSubscription:Lrx/Subscription;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_1
    return-void
.end method

.method public final removeBoostServer(JJ)V
    .locals 9

    .line 71
    invoke-direct {p0}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->handleBoostStarted()V

    .line 73
    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    .line 75
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/discord/utilities/rest/RestAPI;->unsubscribeToGuild(JJ)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 76
    invoke-static {p1, p2, v0, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    .line 77
    invoke-static {}, Lrx/android/b/a;->JY()Lrx/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->a(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    const-string p1, "RestAPI\n        .api\n   \u2026dSchedulers.mainThread())"

    invoke-static {v0, p1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    const-class v1, Lcom/discord/stores/StorePremiumGuildSubscription;

    .line 80
    new-instance p1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$removeBoostServer$1;

    invoke-direct {p1, p0}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$removeBoostServer$1;-><init>(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;)V

    move-object v3, p1

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 81
    new-instance p1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$removeBoostServer$2;

    invoke-direct {p1, p0}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$removeBoostServer$2;-><init>(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;)V

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 82
    new-instance p1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$removeBoostServer$3;

    invoke-direct {p1, p0, p3, p4}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$removeBoostServer$3;-><init>(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;J)V

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x12

    const/4 v8, 0x0

    .line 78
    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final setGuildId(J)V
    .locals 9

    .line 35
    iget-object v0, p0, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;->storeSubscription:Lrx/Subscription;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    .line 37
    :cond_0
    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    .line 38
    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v0

    .line 39
    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreGuilds;->get(J)Lrx/Observable;

    move-result-object p1

    .line 40
    invoke-virtual {p1}, Lrx/Observable;->JL()Lrx/Observable;

    move-result-object p1

    .line 41
    invoke-static {}, Lrx/android/b/a;->JY()Lrx/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->b(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    const-string p1, "StoreStream\n        .get\u2026dSchedulers.mainThread())"

    invoke-static {v0, p1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    const-class v1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;

    const/4 v2, 0x0

    .line 44
    new-instance p1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$setGuildId$1;

    invoke-direct {p1, p0}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$setGuildId$1;-><init>(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;)V

    move-object v3, p1

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 45
    new-instance p1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$setGuildId$2;

    move-object p2, p0

    check-cast p2, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;

    invoke-direct {p1, p2}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$setGuildId$2;-><init>(Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel;)V

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/16 v7, 0x1a

    const/4 v8, 0x0

    .line 42
    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
