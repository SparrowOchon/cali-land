.class public final synthetic Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation$WhenMappings;
.super Ljava/lang/Object;


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;->values()[Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;->NOT_IN_PROGRESS:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    invoke-virtual {v1}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;->CALL_IN_PROGRESS:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    invoke-virtual {v1}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1

    sget-object v0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;->ERROR:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    invoke-virtual {v1}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1

    sget-object v0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;->COMPLETED:Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;

    invoke-virtual {v1}, Lcom/discord/widgets/servers/boosting/BoostInProgressViewModel$BoostState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    return-void
.end method
