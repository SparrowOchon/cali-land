.class final Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Model$Companion$get$1;
.super Ljava/lang/Object;
.source "WidgetServerBoostStatus.kt"

# interfaces
.implements Lrx/functions/Func4;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Model$Companion;->get(J)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "T4:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func4<",
        "TT1;TT2;TT3;TT4;TR;>;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Model$Companion$get$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Model$Companion$get$1;

    invoke-direct {v0}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Model$Companion$get$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Model$Companion$get$1;->INSTANCE:Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Model$Companion$get$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StorePremiumGuildSubscription$State;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Model;
    .locals 2

    .line 332
    new-instance v0, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Model;

    const-string v1, "me"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "premiumGuildSubscriptionsState"

    invoke-static {p3, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "premiumSubscriptionState"

    invoke-static {p4, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Model;-><init>(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StorePremiumGuildSubscription$State;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)V

    return-object v0
.end method

.method public final bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 313
    check-cast p1, Lcom/discord/models/domain/ModelUser;

    check-cast p2, Lcom/discord/models/domain/ModelGuild;

    check-cast p3, Lcom/discord/stores/StorePremiumGuildSubscription$State;

    check-cast p4, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Model$Companion$get$1;->call(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StorePremiumGuildSubscription$State;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)Lcom/discord/widgets/servers/boosting/WidgetServerBoostStatus$Model;

    move-result-object p1

    return-object p1
.end method
