.class public final Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;
.super Lcom/discord/app/k;
.source "WidgetUserSheetViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;,
        Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;
    }
.end annotation


# instance fields
.field private final channelIdSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final storeApplicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

.field private final storeChannels:Lcom/discord/stores/StoreChannels;

.field private final storeGuilds:Lcom/discord/stores/StoreGuilds;

.field private final storeMediaSettings:Lcom/discord/stores/StoreMediaSettings;

.field private final storePermissions:Lcom/discord/stores/StorePermissions;

.field private final storeStateRxScheduler:Lrx/Scheduler;

.field private final storeUser:Lcom/discord/stores/StoreUser;

.field private final storeUserPresence:Lcom/discord/stores/StoreUserPresence;

.field private final storeVoiceStates:Lcom/discord/stores/StoreVoiceStates;

.field private final subscribeOnInit:Z

.field private final userIdSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final viewStateSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreVoiceStates;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreUserPresence;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannels;Lrx/Scheduler;Z)V
    .locals 11

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    const-string v10, "storeGuilds"

    invoke-static {p1, v10}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "storeVoiceStates"

    invoke-static {p2, v10}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "storeMediaSettings"

    invoke-static {p3, v10}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "storeUserPresence"

    invoke-static {p4, v10}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "storePermissions"

    invoke-static {v5, v10}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "storeApplicationStreaming"

    invoke-static {v6, v10}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "storeUser"

    invoke-static {v7, v10}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "storeChannels"

    invoke-static {v8, v10}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "storeStateRxScheduler"

    invoke-static {v9, v10}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Lcom/discord/app/k;-><init>()V

    iput-object v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeGuilds:Lcom/discord/stores/StoreGuilds;

    iput-object v2, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeVoiceStates:Lcom/discord/stores/StoreVoiceStates;

    iput-object v3, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeMediaSettings:Lcom/discord/stores/StoreMediaSettings;

    iput-object v4, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeUserPresence:Lcom/discord/stores/StoreUserPresence;

    iput-object v5, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storePermissions:Lcom/discord/stores/StorePermissions;

    iput-object v6, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeApplicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

    iput-object v7, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeUser:Lcom/discord/stores/StoreUser;

    iput-object v8, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeChannels:Lcom/discord/stores/StoreChannels;

    iput-object v9, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeStateRxScheduler:Lrx/Scheduler;

    move/from16 v1, p10

    iput-boolean v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->subscribeOnInit:Z

    .line 63
    invoke-static {}, Lrx/subjects/BehaviorSubject;->Ls()Lrx/subjects/BehaviorSubject;

    move-result-object v1

    iput-object v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->userIdSubject:Lrx/subjects/BehaviorSubject;

    .line 64
    invoke-static {}, Lrx/subjects/BehaviorSubject;->Ls()Lrx/subjects/BehaviorSubject;

    move-result-object v1

    iput-object v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->channelIdSubject:Lrx/subjects/BehaviorSubject;

    .line 66
    invoke-static {}, Lrx/subjects/BehaviorSubject;->Ls()Lrx/subjects/BehaviorSubject;

    move-result-object v1

    iput-object v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->viewStateSubject:Lrx/subjects/BehaviorSubject;

    .line 69
    iget-boolean v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->subscribeOnInit:Z

    if-eqz v1, :cond_0

    .line 70
    invoke-virtual {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->observeViewStateFromStores$app_productionDiscordExternalRelease()Lrx/Observable;

    move-result-object v1

    .line 71
    invoke-virtual {v1}, Lrx/Observable;->JL()Lrx/Observable;

    move-result-object v1

    const-string v2, "observeViewStateFromStor\u2026  .distinctUntilChanged()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    move-object v2, v0

    check-cast v2, Lcom/discord/app/AppComponent;

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v1, v2, v4, v3, v4}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    .line 74
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 75
    new-instance v7, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$1;

    iget-object v8, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->viewStateSubject:Lrx/subjects/BehaviorSubject;

    invoke-direct {v7, v8}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$1;-><init>(Lrx/subjects/BehaviorSubject;)V

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/16 v8, 0x1e

    const/4 v9, 0x0

    move-object p1, v1

    move-object p2, v2

    move-object p3, v3

    move-object p4, v4

    move-object/from16 p5, v5

    move-object/from16 p6, v6

    move-object/from16 p7, v7

    move/from16 p8, v8

    move-object/from16 p9, v9

    .line 73
    invoke-static/range {p1 .. p9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public static final synthetic access$observeViewStateFromUsersAndChannel(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->observeViewStateFromUsersAndChannel(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method private final observeStoreState(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelUser;",
            "Lcom/discord/models/domain/ModelUser;",
            "Lcom/discord/models/domain/ModelChannel;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    .line 164
    iget-object v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeGuilds:Lcom/discord/stores/StoreGuilds;

    .line 165
    invoke-virtual/range {p3 .. p3}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v2

    const-string v3, "channel.guildId"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Long;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const/4 v7, 0x0

    aput-object v6, v2, v7

    invoke-virtual/range {p2 .. p2}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const/4 v7, 0x1

    aput-object v6, v2, v7

    invoke-static {v2}, Lkotlin/a/m;->k([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-virtual {v1, v4, v5, v2}, Lcom/discord/stores/StoreGuilds;->getComputed(JLjava/util/Collection;)Lrx/Observable;

    move-result-object v8

    .line 166
    iget-object v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeGuilds:Lcom/discord/stores/StoreGuilds;

    .line 167
    invoke-virtual/range {p3 .. p3}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2, v3}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/discord/stores/StoreGuilds;->getRoles(J)Lrx/Observable;

    move-result-object v9

    .line 168
    iget-object v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeVoiceStates:Lcom/discord/stores/StoreVoiceStates;

    .line 169
    invoke-virtual/range {p3 .. p3}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2, v3}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual/range {p3 .. p3}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v10

    invoke-virtual {v1, v4, v5, v10, v11}, Lcom/discord/stores/StoreVoiceStates;->get(JJ)Lrx/Observable;

    move-result-object v10

    .line 170
    iget-object v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeMediaSettings:Lcom/discord/stores/StoreMediaSettings;

    .line 171
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/discord/stores/StoreMediaSettings;->getUserMuted(J)Lrx/Observable;

    move-result-object v11

    .line 172
    iget-object v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeMediaSettings:Lcom/discord/stores/StoreMediaSettings;

    .line 173
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/discord/stores/StoreMediaSettings;->getUserOutputVolume(J)Lrx/Observable;

    move-result-object v12

    .line 174
    sget-object v1, Lcom/discord/widgets/user/ModelRichPresence;->Companion:Lcom/discord/widgets/user/ModelRichPresence$Companion;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v4

    iget-object v2, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeUserPresence:Lcom/discord/stores/StoreUserPresence;

    invoke-virtual {v1, v4, v5, v2}, Lcom/discord/widgets/user/ModelRichPresence$Companion;->get(JLcom/discord/stores/StoreUserPresence;)Lrx/Observable;

    move-result-object v13

    .line 175
    iget-object v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeGuilds:Lcom/discord/stores/StoreGuilds;

    .line 176
    invoke-virtual/range {p3 .. p3}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2, v3}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/discord/stores/StoreGuilds;->get(J)Lrx/Observable;

    move-result-object v14

    .line 177
    iget-object v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storePermissions:Lcom/discord/stores/StorePermissions;

    .line 178
    invoke-virtual/range {p3 .. p3}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2, v3}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/discord/stores/StorePermissions;->getForGuild(J)Lrx/Observable;

    move-result-object v15

    .line 179
    sget-object v1, Lcom/discord/utilities/streams/StreamContext;->Companion:Lcom/discord/utilities/streams/StreamContext$Companion;

    .line 181
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v2

    .line 183
    iget-object v4, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeApplicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

    .line 180
    invoke-virtual {v1, v2, v3, v7, v4}, Lcom/discord/utilities/streams/StreamContext$Companion;->get(JZLcom/discord/stores/StoreApplicationStreaming;)Lrx/Observable;

    move-result-object v16

    .line 184
    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeStoreState$1;->INSTANCE:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeStoreState$1;

    move-object/from16 v17, v1

    check-cast v17, Lrx/functions/Func9;

    .line 188
    sget-object v20, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeStateRxScheduler:Lrx/Scheduler;

    const-wide/16 v18, 0xfa

    move-object/from16 v21, v1

    .line 163
    invoke-static/range {v8 .. v21}, Lcom/discord/utilities/rx/ObservableWithLeadingEdgeThrottle;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func9;JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/Observable;

    move-result-object v1

    const-string v2, "ObservableWithLeadingEdg\u2026S, storeStateRxScheduler)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v1
.end method

.method private final observeViewStateFromUsersAndChannel(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelUser;",
            "Lcom/discord/models/domain/ModelUser;",
            "Lcom/discord/models/domain/ModelChannel;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;",
            ">;"
        }
    .end annotation

    .line 121
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->observeStoreState(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;

    move-result-object v0

    .line 122
    new-instance v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromUsersAndChannel$1;

    invoke-direct {v1, p2, p1, p3}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromUsersAndChannel$1;-><init>(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelChannel;)V

    check-cast v1, Lrx/functions/b;

    invoke-virtual {v0, v1}, Lrx/Observable;->e(Lrx/functions/b;)Lrx/Observable;

    move-result-object p1

    const-string p2, "observeStoreState(user, \u2026 = guild?.name)\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public final getStoreApplicationStreaming()Lcom/discord/stores/StoreApplicationStreaming;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeApplicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

    return-object v0
.end method

.method public final getStoreChannels()Lcom/discord/stores/StoreChannels;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeChannels:Lcom/discord/stores/StoreChannels;

    return-object v0
.end method

.method public final getStoreGuilds()Lcom/discord/stores/StoreGuilds;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeGuilds:Lcom/discord/stores/StoreGuilds;

    return-object v0
.end method

.method public final getStoreMediaSettings()Lcom/discord/stores/StoreMediaSettings;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeMediaSettings:Lcom/discord/stores/StoreMediaSettings;

    return-object v0
.end method

.method public final getStorePermissions()Lcom/discord/stores/StorePermissions;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storePermissions:Lcom/discord/stores/StorePermissions;

    return-object v0
.end method

.method public final getStoreStateRxScheduler()Lrx/Scheduler;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeStateRxScheduler:Lrx/Scheduler;

    return-object v0
.end method

.method public final getStoreUser()Lcom/discord/stores/StoreUser;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeUser:Lcom/discord/stores/StoreUser;

    return-object v0
.end method

.method public final getStoreUserPresence()Lcom/discord/stores/StoreUserPresence;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeUserPresence:Lcom/discord/stores/StoreUserPresence;

    return-object v0
.end method

.method public final getStoreVoiceStates()Lcom/discord/stores/StoreVoiceStates;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeVoiceStates:Lcom/discord/stores/StoreVoiceStates;

    return-object v0
.end method

.method public final getSubscribeOnInit()Z
    .locals 1

    .line 29
    iget-boolean v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->subscribeOnInit:Z

    return v0
.end method

.method public final observeViewState()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;",
            ">;"
        }
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->viewStateSubject:Lrx/subjects/BehaviorSubject;

    const-string v1, "viewStateSubject"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lrx/Observable;

    return-object v0
.end method

.method public final observeViewStateFromStores$app_productionDiscordExternalRelease()Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;",
            ">;"
        }
    .end annotation

    .line 92
    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->userIdSubject:Lrx/subjects/BehaviorSubject;

    check-cast v0, Lrx/Observable;

    .line 93
    iget-object v1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->channelIdSubject:Lrx/subjects/BehaviorSubject;

    check-cast v1, Lrx/Observable;

    .line 94
    sget-object v2, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$1;->INSTANCE:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$1;

    check-cast v2, Lrx/functions/Func2;

    .line 91
    invoke-static {v0, v1, v2}, Lrx/Observable;->a(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    .line 95
    new-instance v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$2;

    invoke-direct {v1, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$2;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;)V

    check-cast v1, Lrx/functions/b;

    invoke-virtual {v0, v1}, Lrx/Observable;->g(Lrx/functions/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026              }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    .line 114
    invoke-virtual {v0}, Lrx/Observable;->JL()Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final updateChannelId(J)V
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->channelIdSubject:Lrx/subjects/BehaviorSubject;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public final updateUserId(J)V
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->userIdSubject:Lrx/subjects/BehaviorSubject;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method
