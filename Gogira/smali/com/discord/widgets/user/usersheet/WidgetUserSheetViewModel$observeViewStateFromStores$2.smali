.class final Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$2;
.super Ljava/lang/Object;
.source "WidgetUserSheetViewModel.kt"

# interfaces
.implements Lrx/functions/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->observeViewStateFromStores$app_productionDiscordExternalRelease()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/b<",
        "TT;",
        "Lrx/Observable<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;


# direct methods
.method constructor <init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$2;->this$0:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 19
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$2;->call(Lkotlin/Pair;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lkotlin/Pair;)Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;)",
            "Lrx/Observable<",
            "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;",
            ">;"
        }
    .end annotation

    .line 1000
    iget-object v0, p1, Lkotlin/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    .line 2000
    iget-object p1, p1, Lkotlin/Pair;->second:Ljava/lang/Object;

    check-cast p1, Ljava/lang/Long;

    .line 98
    iget-object v1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$2;->this$0:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    invoke-virtual {v1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->getStoreUser()Lcom/discord/stores/StoreUser;

    move-result-object v1

    const-string v2, "userId"

    .line 99
    invoke-static {v0, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/discord/stores/StoreUser;->getUser(J)Lrx/Observable;

    move-result-object v0

    .line 100
    iget-object v1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$2;->this$0:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    invoke-virtual {v1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->getStoreUser()Lcom/discord/stores/StoreUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreUser;->getMe()Lrx/Observable;

    move-result-object v1

    .line 102
    sget-object v2, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$2$1;->INSTANCE:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$2$1;

    check-cast v2, Lrx/functions/Func2;

    .line 97
    invoke-static {v0, v1, v2}, Lrx/Observable;->a(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    .line 103
    new-instance v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$2$2;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$2$2;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$2;Ljava/lang/Long;)V

    check-cast v1, Lrx/functions/b;

    invoke-virtual {v0, v1}, Lrx/Observable;->g(Lrx/functions/b;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
