.class final Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromUsersAndChannel$1;
.super Ljava/lang/Object;
.source "WidgetUserSheetViewModel.kt"

# interfaces
.implements Lrx/functions/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->observeViewStateFromUsersAndChannel(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/b<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic $channel:Lcom/discord/models/domain/ModelChannel;

.field final synthetic $me:Lcom/discord/models/domain/ModelUser;

.field final synthetic $user:Lcom/discord/models/domain/ModelUser;


# direct methods
.method constructor <init>(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelChannel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromUsersAndChannel$1;->$me:Lcom/discord/models/domain/ModelUser;

    iput-object p2, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromUsersAndChannel$1;->$user:Lcom/discord/models/domain/ModelUser;

    iput-object p3, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromUsersAndChannel$1;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;)Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;
    .locals 27

    move-object/from16 v0, p0

    .line 123
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getComputedMembers()Ljava/util/Map;

    move-result-object v1

    .line 124
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getGuildRoles()Ljava/util/Map;

    move-result-object v9

    .line 125
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v10

    .line 126
    iget-object v2, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromUsersAndChannel$1;->$me:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v2

    iget-object v4, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromUsersAndChannel$1;->$user:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v4

    const/4 v11, 0x1

    const/4 v12, 0x0

    cmp-long v6, v2, v4

    if-nez v6, :cond_0

    const/4 v15, 0x1

    goto :goto_0

    :cond_0
    const/4 v15, 0x0

    .line 127
    :goto_0
    iget-object v2, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromUsersAndChannel$1;->$user:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelGuildMember$Computed;

    .line 128
    iget-object v3, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromUsersAndChannel$1;->$me:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelGuildMember$Computed;

    if-eqz v2, :cond_3

    .line 131
    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuildMember$Computed;->getRoles()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_3

    check-cast v3, Ljava/lang/Iterable;

    .line 192
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    check-cast v4, Ljava/util/Collection;

    .line 201
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 200
    check-cast v5, Ljava/lang/Long;

    .line 130
    invoke-interface {v9, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/discord/models/domain/ModelGuildRole;

    if-eqz v5, :cond_1

    .line 200
    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 203
    :cond_2
    check-cast v4, Ljava/util/List;

    check-cast v4, Ljava/lang/Iterable;

    .line 131
    invoke-static {v4}, Lkotlin/a/m;->p(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v3

    if-nez v3, :cond_4

    .line 1069
    :cond_3
    sget-object v3, Lkotlin/a/y;->bkh:Lkotlin/a/y;

    check-cast v3, Ljava/util/List;

    :cond_4
    move-object/from16 v21, v3

    const/4 v13, 0x0

    if-eqz v2, :cond_5

    .line 132
    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuildMember$Computed;->getNick()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v22, v3

    goto :goto_2

    :cond_5
    move-object/from16 v22, v13

    :goto_2
    if-eqz v2, :cond_7

    if-eqz v1, :cond_7

    if-nez v10, :cond_6

    goto :goto_3

    .line 135
    :cond_6
    sget-object v3, Lcom/discord/utilities/permissions/ManageUserContext;->Companion:Lcom/discord/utilities/permissions/ManageUserContext$Companion;

    iget-object v4, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromUsersAndChannel$1;->$me:Lcom/discord/models/domain/ModelUser;

    iget-object v5, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromUsersAndChannel$1;->$user:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuildMember$Computed;->getRoles()Ljava/util/List;

    move-result-object v1

    const-string v6, "memberMe.roles"

    invoke-static {v1, v6}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v6, v1

    check-cast v6, Ljava/util/Collection;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuildMember$Computed;->getRoles()Ljava/util/List;

    move-result-object v1

    const-string v2, "member.roles"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v7, v1

    check-cast v7, Ljava/util/Collection;

    .line 136
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getPermissions()Ljava/lang/Integer;

    move-result-object v8

    move-object v2, v3

    move-object v3, v10

    .line 135
    invoke-virtual/range {v2 .. v9}, Lcom/discord/utilities/permissions/ManageUserContext$Companion;->from(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelUser;Ljava/util/Collection;Ljava/util/Collection;Ljava/lang/Integer;Ljava/util/Map;)Lcom/discord/utilities/permissions/ManageUserContext;

    move-result-object v1

    move-object/from16 v24, v1

    goto :goto_4

    :cond_7
    :goto_3
    move-object/from16 v24, v13

    .line 137
    :goto_4
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getVoiceStates()Ljava/util/Map;

    move-result-object v1

    iget-object v2, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromUsersAndChannel$1;->$user:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/discord/models/domain/ModelVoice$State;

    .line 139
    iget-object v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromUsersAndChannel$1;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getOwnerId()J

    move-result-wide v1

    iget-object v3, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromUsersAndChannel$1;->$me:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v3

    cmp-long v5, v1, v3

    if-nez v5, :cond_8

    const/16 v16, 0x1

    goto :goto_5

    :cond_8
    const/16 v16, 0x0

    .line 141
    :goto_5
    new-instance v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;

    .line 142
    iget-object v14, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromUsersAndChannel$1;->$user:Lcom/discord/models/domain/ModelUser;

    .line 145
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getMuted()Z

    move-result v17

    .line 146
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getOutputVolume()I

    move-result v18

    .line 148
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getRichPresence()Lcom/discord/widgets/user/ModelRichPresence;

    move-result-object v20

    .line 151
    iget-object v2, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromUsersAndChannel$1;->$channel:Lcom/discord/models/domain/ModelChannel;

    .line 153
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getStreamContext()Lcom/discord/utilities/streams/StreamContext;

    move-result-object v25

    if-eqz v10, :cond_9

    .line 154
    invoke-virtual {v10}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v26, v3

    goto :goto_6

    :cond_9
    move-object/from16 v26, v13

    :goto_6
    move-object v13, v1

    move-object/from16 v23, v2

    .line 141
    invoke-direct/range {v13 .. v26}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;-><init>(Lcom/discord/models/domain/ModelUser;ZZZILcom/discord/models/domain/ModelVoice$State;Lcom/discord/widgets/user/ModelRichPresence;Ljava/util/List;Ljava/lang/String;Lcom/discord/models/domain/ModelChannel;Lcom/discord/utilities/permissions/ManageUserContext;Lcom/discord/utilities/streams/StreamContext;Ljava/lang/String;)V

    return-object v1
.end method

.method public final bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 19
    check-cast p1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromUsersAndChannel$1;->call(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;)Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;

    move-result-object p1

    return-object p1
.end method
