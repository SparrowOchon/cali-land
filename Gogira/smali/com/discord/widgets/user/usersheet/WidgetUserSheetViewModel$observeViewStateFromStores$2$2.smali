.class final Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$2$2;
.super Ljava/lang/Object;
.source "WidgetUserSheetViewModel.kt"

# interfaces
.implements Lrx/functions/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$2;->call(Lkotlin/Pair;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/b<",
        "TT;",
        "Lrx/Observable<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic $channelId:Ljava/lang/Long;

.field final synthetic this$0:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$2;


# direct methods
.method constructor <init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$2;Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$2$2;->this$0:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$2;

    iput-object p2, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$2$2;->$channelId:Ljava/lang/Long;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 19
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$2$2;->call(Lkotlin/Pair;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lkotlin/Pair;)Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+",
            "Lcom/discord/models/domain/ModelUser;",
            "+",
            "Lcom/discord/models/domain/ModelUser;",
            ">;)",
            "Lrx/Observable<",
            "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;",
            ">;"
        }
    .end annotation

    .line 1000
    iget-object v0, p1, Lkotlin/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/discord/models/domain/ModelUser;

    .line 2000
    iget-object p1, p1, Lkotlin/Pair;->second:Ljava/lang/Object;

    check-cast p1, Lcom/discord/models/domain/ModelUser;

    if-eqz v0, :cond_0

    .line 105
    iget-object v1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$2$2;->this$0:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$2;

    iget-object v1, v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$2;->this$0:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    invoke-virtual {v1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->getStoreChannels()Lcom/discord/stores/StoreChannels;

    move-result-object v1

    .line 106
    iget-object v2, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$2$2;->$channelId:Ljava/lang/Long;

    const-string v3, "channelId"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/discord/stores/StoreChannels;->get(J)Lrx/Observable;

    move-result-object v1

    .line 107
    new-instance v2, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$2$2$$special$$inlined$let$lambda$1;

    invoke-direct {v2, p0, v0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$2$2$$special$$inlined$let$lambda$1;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$observeViewStateFromStores$2$2;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelUser;)V

    check-cast v2, Lrx/functions/b;

    invoke-virtual {v1, v2}, Lrx/Observable;->g(Lrx/functions/b;)Lrx/Observable;

    move-result-object p1

    if-nez p1, :cond_1

    :cond_0
    const/4 p1, 0x0

    .line 110
    invoke-static {p1}, Lrx/Observable;->bI(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    :cond_1
    return-object p1
.end method
