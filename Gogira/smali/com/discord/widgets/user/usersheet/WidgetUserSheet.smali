.class public final Lcom/discord/widgets/user/usersheet/WidgetUserSheet;
.super Lcom/discord/app/AppBottomSheet;
.source "WidgetUserSheet.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field private static final ARG_CHANNEL_ID:Ljava/lang/String; = "ARG_CHANNEL_ID"

.field private static final ARG_GUILD_ID:Ljava/lang/String; = "ARG_GUILD_ID"

.field private static final ARG_USER_ID:Ljava/lang/String; = "ARG_USER_ID"

.field public static final Companion:Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;

.field private static final REQUEST_CODE_MOVE_USER:I = 0xfa0


# instance fields
.field private final actionsDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final activityDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private activityViewHolder:Lcom/discord/widgets/user/ViewHolderUserRichPresence;

.field private final administrativeDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final administrativeHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final audioSettingsContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final audioSettingsDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final avatar$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final banButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final copyIdButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final developerDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final developerHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final editMemberButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guildContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guildHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final header$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final imagesChangeDetector:Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;

.field private final kickButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private onMoveChannelSelected:Lkotlin/jvm/functions/Function3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Ljava/lang/Long;",
            "-",
            "Ljava/lang/String;",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final presenceIndicator$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final primaryName$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final richPresenceContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private rolesAdapter:Lcom/discord/widgets/roles/RolesAdapter;

.field private final rolesRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final secondaryName$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final sendMessageButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final serverDeafenButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final serverMoveUserButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final serverMuteButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final userMutedCheck$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final userVolumeSeekbar$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

.field private final viewProfileButton$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-class v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const/16 v1, 0x1c

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "header"

    const-string v5, "getHeader()Landroid/view/View;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "primaryName"

    const-string v5, "getPrimaryName()Landroid/widget/TextView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "secondaryName"

    const-string v5, "getSecondaryName()Landroid/widget/TextView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "presenceIndicator"

    const-string v5, "getPresenceIndicator()Landroid/widget/ImageView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "avatar"

    const-string v5, "getAvatar()Landroid/widget/ImageView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x4

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "richPresenceContainer"

    const-string v5, "getRichPresenceContainer()Landroid/widget/FrameLayout;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x5

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "activityDivider"

    const-string v5, "getActivityDivider()Landroid/view/View;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x6

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "rolesRecycler"

    const-string v5, "getRolesRecycler()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x7

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "guildContainer"

    const-string v5, "getGuildContainer()Landroid/view/ViewGroup;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0x8

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "guildHeader"

    const-string v5, "getGuildHeader()Landroid/widget/TextView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0x9

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "audioSettingsDivider"

    const-string v5, "getAudioSettingsDivider()Landroid/view/View;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0xa

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "audioSettingsContainer"

    const-string v5, "getAudioSettingsContainer()Landroid/view/ViewGroup;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0xb

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "userMutedCheck"

    const-string v5, "getUserMutedCheck()Landroidx/appcompat/widget/SwitchCompat;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0xc

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "userVolumeSeekbar"

    const-string v5, "getUserVolumeSeekbar()Landroid/widget/SeekBar;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0xd

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "actionsDivider"

    const-string v5, "getActionsDivider()Landroid/view/View;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0xe

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "viewProfileButton"

    const-string v5, "getViewProfileButton()Landroid/widget/TextView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0xf

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "sendMessageButton"

    const-string v5, "getSendMessageButton()Landroid/view/View;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0x10

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "administrativeHeader"

    const-string v5, "getAdministrativeHeader()Landroid/view/View;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0x11

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "administrativeDivider"

    const-string v5, "getAdministrativeDivider()Landroid/view/View;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0x12

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "editMemberButton"

    const-string v5, "getEditMemberButton()Landroid/view/View;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0x13

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "kickButton"

    const-string v5, "getKickButton()Landroid/widget/TextView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0x14

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "banButton"

    const-string v5, "getBanButton()Landroid/widget/TextView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0x15

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "serverMuteButton"

    const-string v5, "getServerMuteButton()Landroid/widget/TextView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0x16

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "serverDeafenButton"

    const-string v5, "getServerDeafenButton()Landroid/widget/TextView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0x17

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "serverMoveUserButton"

    const-string v5, "getServerMoveUserButton()Landroid/view/View;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0x18

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "developerHeader"

    const-string v5, "getDeveloperHeader()Landroid/view/View;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0x19

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "developerDivider"

    const-string v5, "getDeveloperDivider()Landroid/view/View;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v3, 0x1a

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v0

    const-string v3, "copyIdButton"

    const-string v4, "getCopyIdButton()Landroid/view/View;"

    invoke-direct {v2, v0, v3, v4}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/KProperty;

    const/16 v2, 0x1b

    aput-object v0, v1, v2

    sput-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->Companion:Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 59
    invoke-direct {p0}, Lcom/discord/app/AppBottomSheet;-><init>()V

    const v0, 0x7f0a07c6

    .line 61
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->header$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07cb

    .line 62
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->primaryName$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07ce

    .line 63
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->secondaryName$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0784

    .line 64
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->presenceIndicator$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0782

    .line 65
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->avatar$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0566

    .line 67
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->richPresenceContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07ba

    .line 69
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->activityDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07cd

    .line 72
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->rolesRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07c4

    .line 73
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->guildContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07c5

    .line 74
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->guildHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07be

    .line 76
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->audioSettingsDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07bd

    .line 77
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->audioSettingsContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07c9

    .line 78
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->userMutedCheck$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07d4

    .line 79
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->userVolumeSeekbar$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07b9

    .line 81
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->actionsDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07cc

    .line 82
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->viewProfileButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07c8

    .line 83
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->sendMessageButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07bc

    .line 84
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->administrativeHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07bb

    .line 85
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->administrativeDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07d0

    .line 86
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->editMemberButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07c7

    .line 87
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->kickButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07c0

    .line 88
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->banButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07d2

    .line 89
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->serverMuteButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07cf

    .line 90
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->serverDeafenButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07d1

    .line 91
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->serverMoveUserButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07c3

    .line 92
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->developerHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07c2

    .line 93
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->developerDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07c1

    .line 94
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->copyIdButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    .line 96
    new-instance v0, Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;

    invoke-direct {v0}, Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;-><init>()V

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->imagesChangeDetector:Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;)V
    .locals 0

    .line 59
    invoke-direct {p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->configureUI(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;)V

    return-void
.end method

.method private final configureAdministrativeSection(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;)V
    .locals 20

    move-object/from16 v0, p0

    .line 277
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->getManageUserContext()Lcom/discord/utilities/permissions/ManageUserContext;

    move-result-object v1

    .line 278
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v2

    .line 279
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v3

    .line 280
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->isServerMuted()Z

    move-result v4

    .line 281
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->getVoiceState()Lcom/discord/models/domain/ModelVoice$State;

    move-result-object v5

    .line 282
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->isServerDeafened()Z

    move-result v6

    .line 284
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getEditMemberButton()Landroid/view/View;

    move-result-object v7

    const/4 v8, 0x1

    const/4 v9, 0x0

    if-eqz v1, :cond_1

    .line 285
    invoke-virtual {v1}, Lcom/discord/utilities/permissions/ManageUserContext;->getCanChangeNickname()Z

    move-result v10

    if-nez v10, :cond_0

    invoke-virtual {v1}, Lcom/discord/utilities/permissions/ManageUserContext;->getCanManageRoles()Z

    move-result v10

    if-eqz v10, :cond_1

    :cond_0
    const/4 v10, 0x1

    goto :goto_0

    :cond_1
    const/4 v10, 0x0

    :goto_0
    const/4 v11, 0x0

    const/4 v12, 0x2

    .line 284
    invoke-static {v7, v10, v9, v12, v11}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    .line 286
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getEditMemberButton()Landroid/view/View;

    move-result-object v7

    new-instance v10, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureAdministrativeSection$2;

    invoke-direct {v10, v0, v2, v3}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureAdministrativeSection$2;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelUser;)V

    check-cast v10, Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 290
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelChannel;->isMultiUserDM()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 291
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getKickButton()Landroid/widget/TextView;

    move-result-object v7

    check-cast v7, Landroid/view/View;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->isManaged()Z

    move-result v10

    if-nez v10, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->isChannelOwner()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->isMe()Z

    move-result v10

    if-nez v10, :cond_2

    const/4 v10, 0x1

    goto :goto_1

    :cond_2
    const/4 v10, 0x0

    :goto_1
    invoke-static {v7, v10, v9, v12, v11}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    .line 292
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getKickButton()Landroid/widget/TextView;

    move-result-object v7

    const v10, 0x7f120edd

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setText(I)V

    .line 293
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getKickButton()Landroid/widget/TextView;

    move-result-object v7

    new-instance v10, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureAdministrativeSection$3;

    invoke-direct {v10, v0, v2, v3}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureAdministrativeSection$3;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelUser;)V

    check-cast v10, Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    .line 302
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getKickButton()Landroid/widget/TextView;

    move-result-object v7

    check-cast v7, Landroid/view/View;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/discord/utilities/permissions/ManageUserContext;->getCanKick()Z

    move-result v10

    if-ne v10, v8, :cond_4

    const/4 v10, 0x1

    goto :goto_2

    :cond_4
    const/4 v10, 0x0

    :goto_2
    invoke-static {v7, v10, v9, v12, v11}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    .line 303
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getKickButton()Landroid/widget/TextView;

    move-result-object v7

    const v10, 0x7f120aa8

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setText(I)V

    .line 304
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getKickButton()Landroid/widget/TextView;

    move-result-object v7

    new-instance v10, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureAdministrativeSection$4;

    invoke-direct {v10, v0, v3, v2}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureAdministrativeSection$4;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelChannel;)V

    check-cast v10, Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 310
    :goto_3
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getBanButton()Landroid/widget/TextView;

    move-result-object v7

    check-cast v7, Landroid/view/View;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/discord/utilities/permissions/ManageUserContext;->getCanBan()Z

    move-result v10

    if-ne v10, v8, :cond_5

    const/4 v10, 0x1

    goto :goto_4

    :cond_5
    const/4 v10, 0x0

    :goto_4
    invoke-static {v7, v10, v9, v12, v11}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    .line 311
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getBanButton()Landroid/widget/TextView;

    move-result-object v7

    new-instance v10, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureAdministrativeSection$5;

    invoke-direct {v10, v0, v3, v2}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureAdministrativeSection$5;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelChannel;)V

    check-cast v10, Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 313
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getServerMuteButton()Landroid/widget/TextView;

    move-result-object v7

    check-cast v7, Landroid/view/View;

    if-eqz v5, :cond_6

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/discord/utilities/permissions/ManageUserContext;->getCanMute()Z

    move-result v10

    if-ne v10, v8, :cond_6

    const/4 v10, 0x1

    goto :goto_5

    :cond_6
    const/4 v10, 0x0

    :goto_5
    invoke-static {v7, v10, v9, v12, v11}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    .line 314
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getServerMuteButton()Landroid/widget/TextView;

    move-result-object v7

    new-instance v10, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureAdministrativeSection$6;

    invoke-direct {v10, v0, v2, v3, v4}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureAdministrativeSection$6;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelUser;Z)V

    check-cast v10, Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 323
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getServerMuteButton()Landroid/widget/TextView;

    move-result-object v13

    if-eqz v4, :cond_7

    const v7, 0x7f0802ec

    const v14, 0x7f0802ec

    goto :goto_6

    :cond_7
    const v7, 0x7f0802ef

    const v14, 0x7f0802ef

    :goto_6
    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0xe

    const/16 v19, 0x0

    invoke-static/range {v13 .. v19}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setCompoundDrawableWithIntrinsicBounds$default(Landroid/widget/TextView;IIIIILjava/lang/Object;)V

    .line 330
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getServerMuteButton()Landroid/widget/TextView;

    move-result-object v7

    if-eqz v4, :cond_8

    const v4, 0x7f121001

    goto :goto_7

    :cond_8
    const v4, 0x7f120ff4

    :goto_7
    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setText(I)V

    .line 337
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getServerDeafenButton()Landroid/widget/TextView;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    if-eqz v5, :cond_9

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/discord/utilities/permissions/ManageUserContext;->getCanDeafen()Z

    move-result v7

    if-ne v7, v8, :cond_9

    const/4 v7, 0x1

    goto :goto_8

    :cond_9
    const/4 v7, 0x0

    :goto_8
    invoke-static {v4, v7, v9, v12, v11}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    .line 338
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getServerDeafenButton()Landroid/widget/TextView;

    move-result-object v4

    new-instance v7, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureAdministrativeSection$7;

    invoke-direct {v7, v0, v2, v3, v6}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureAdministrativeSection$7;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelUser;Z)V

    check-cast v7, Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 347
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getServerDeafenButton()Landroid/widget/TextView;

    move-result-object v13

    if-eqz v6, :cond_a

    const v4, 0x7f0802a9

    const v14, 0x7f0802a9

    goto :goto_9

    :cond_a
    const v4, 0x7f0802af

    const v14, 0x7f0802af

    :goto_9
    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0xe

    const/16 v19, 0x0

    invoke-static/range {v13 .. v19}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setCompoundDrawableWithIntrinsicBounds$default(Landroid/widget/TextView;IIIIILjava/lang/Object;)V

    .line 354
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getServerDeafenButton()Landroid/widget/TextView;

    move-result-object v4

    if-eqz v6, :cond_b

    const v6, 0x7f121000

    goto :goto_a

    :cond_b
    const v6, 0x7f120fea

    :goto_a
    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(I)V

    .line 361
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getServerMoveUserButton()Landroid/view/View;

    move-result-object v4

    if-eqz v5, :cond_c

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/discord/utilities/permissions/ManageUserContext;->getCanMove()Z

    move-result v1

    if-ne v1, v8, :cond_c

    const/4 v1, 0x1

    goto :goto_b

    :cond_c
    const/4 v1, 0x0

    :goto_b
    invoke-static {v4, v1, v9, v12, v11}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    .line 362
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getServerMoveUserButton()Landroid/view/View;

    move-result-object v1

    new-instance v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureAdministrativeSection$8;

    invoke-direct {v4, v0, v2}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureAdministrativeSection$8;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;Lcom/discord/models/domain/ModelChannel;)V

    check-cast v4, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 366
    new-instance v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureAdministrativeSection$9;

    invoke-direct {v1, v0, v2, v3}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureAdministrativeSection$9;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelUser;)V

    check-cast v1, Lkotlin/jvm/functions/Function3;

    iput-object v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->onMoveChannelSelected:Lkotlin/jvm/functions/Function3;

    const/4 v1, 0x5

    new-array v2, v1, [Landroid/view/View;

    .line 376
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getKickButton()Landroid/widget/TextView;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    aput-object v3, v2, v9

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getBanButton()Landroid/widget/TextView;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    aput-object v3, v2, v8

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getServerMuteButton()Landroid/widget/TextView;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    aput-object v3, v2, v12

    const/4 v3, 0x3

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getServerDeafenButton()Landroid/widget/TextView;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getServerMoveUserButton()Landroid/view/View;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x0

    :goto_c
    if-ge v3, v1, :cond_f

    .line 468
    aget-object v4, v2, v3

    .line 377
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_d

    const/4 v4, 0x1

    goto :goto_d

    :cond_d
    const/4 v4, 0x0

    :goto_d
    if-eqz v4, :cond_e

    goto :goto_e

    :cond_e
    add-int/lit8 v3, v3, 0x1

    goto :goto_c

    :cond_f
    const/4 v8, 0x0

    .line 379
    :goto_e
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getAdministrativeHeader()Landroid/view/View;

    move-result-object v1

    invoke-static {v1, v8, v9, v12, v11}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    .line 380
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getAdministrativeDivider()Landroid/view/View;

    move-result-object v1

    invoke-static {v1, v8, v9, v12, v11}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    return-void
.end method

.method private final configureDeveloperSection(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;)V
    .locals 5

    .line 384
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getCopyIdButton()Landroid/view/View;

    move-result-object v0

    .line 385
    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreUserSettings;->getDeveloperMode()Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v0, v1, v4, v3, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    .line 386
    new-instance v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureDeveloperSection$$inlined$apply$lambda$1;

    invoke-direct {v1, v0, p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureDeveloperSection$$inlined$apply$lambda$1;-><init>(Landroid/view/View;Lcom/discord/widgets/user/usersheet/WidgetUserSheet;Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 392
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getCopyIdButton()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 394
    :goto_0
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getDeveloperHeader()Landroid/view/View;

    move-result-object v0

    invoke-static {v0, p1, v4, v3, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    .line 395
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getDeveloperDivider()Landroid/view/View;

    move-result-object v0

    invoke-static {v0, p1, v4, v3, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    return-void
.end method

.method private final configureRoles(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;)V
    .locals 6

    .line 246
    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->getRoleItems()Ljava/util/List;

    move-result-object v0

    .line 247
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getGuildContainer()Landroid/view/ViewGroup;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    move-object v2, v0

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static {v1, v2, v3, v4, v5}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    .line 248
    iget-object v1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->rolesAdapter:Lcom/discord/widgets/roles/RolesAdapter;

    if-eqz v1, :cond_0

    invoke-virtual {v1, v0}, Lcom/discord/widgets/roles/RolesAdapter;->setData(Ljava/util/List;)V

    .line 250
    :cond_0
    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->getGuildName()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 252
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getGuildHeader()Landroid/widget/TextView;

    move-result-object v0

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;)V
    .locals 28

    move-object/from16 v6, p0

    if-nez p1, :cond_0

    .line 174
    invoke-virtual/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->dismiss()V

    return-void

    .line 178
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->getPresence()Lcom/discord/models/domain/ModelPresence;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->getStreamContext()Lcom/discord/utilities/streams/StreamContext;

    move-result-object v1

    const/4 v7, 0x1

    const/4 v8, 0x0

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getPresenceIndicator()Landroid/widget/ImageView;

    move-result-object v2

    const/4 v3, 0x0

    const/16 v4, 0x8

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/presence/PresenceUtils;->setPresence$default(Lcom/discord/models/domain/ModelPresence;ZLandroid/widget/ImageView;Landroid/widget/TextView;ILjava/lang/Object;)V

    .line 179
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->getPresence()Lcom/discord/models/domain/ModelPresence;

    move-result-object v0

    const/4 v9, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelPresence;->getPrimaryActivity()Lcom/discord/models/domain/ModelPresence$Activity;

    move-result-object v0

    move-object v10, v0

    goto :goto_1

    :cond_2
    move-object v10, v9

    .line 180
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v11

    .line 181
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->getUserNickname()Ljava/lang/String;

    move-result-object v0

    .line 182
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->isMe()Z

    move-result v12

    .line 184
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getActivityDivider()Landroid/view/View;

    move-result-object v1

    if-nez v10, :cond_3

    if-nez v12, :cond_3

    const/4 v2, 0x1

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    const/4 v13, 0x2

    invoke-static {v1, v2, v8, v13, v9}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    if-nez v12, :cond_4

    .line 186
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getActionsDivider()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v6, v1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->setPeekHeightBottomView(Landroid/view/View;)V

    .line 189
    :cond_4
    sget-object v1, Lcom/discord/widgets/user/ViewHolderUserRichPresence;->Companion:Lcom/discord/widgets/user/ViewHolderUserRichPresence$Companion;

    .line 190
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getRichPresenceContainer()Landroid/widget/FrameLayout;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 192
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->getStreamContext()Lcom/discord/utilities/streams/StreamContext;

    move-result-object v3

    .line 193
    iget-object v4, v6, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->activityViewHolder:Lcom/discord/widgets/user/ViewHolderUserRichPresence;

    .line 189
    invoke-virtual {v1, v2, v10, v3, v4}, Lcom/discord/widgets/user/ViewHolderUserRichPresence$Companion;->setRichPresence(Landroid/view/ViewGroup;Lcom/discord/models/domain/ModelPresence$Activity;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/widgets/user/ViewHolderUserRichPresence;)Lcom/discord/widgets/user/ViewHolderUserRichPresence;

    move-result-object v1

    .line 194
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->getRichPresence()Lcom/discord/widgets/user/ModelRichPresence;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->getStreamContext()Lcom/discord/utilities/streams/StreamContext;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->isMe()Z

    move-result v4

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/discord/widgets/user/ViewHolderUserRichPresence;->configureUi(Lcom/discord/widgets/user/ModelRichPresence;Lcom/discord/utilities/streams/StreamContext;ZLcom/discord/models/domain/ModelUser;)V

    .line 195
    move-object v2, v6

    check-cast v2, Lcom/discord/app/AppComponent;

    invoke-virtual {v1, v10, v2}, Lcom/discord/widgets/user/ViewHolderUserRichPresence;->configureUiTimestamp(Lcom/discord/models/domain/ModelPresence$Activity;Lcom/discord/app/AppComponent;)V

    .line 193
    iput-object v1, v6, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->activityViewHolder:Lcom/discord/widgets/user/ViewHolderUserRichPresence;

    .line 198
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getPrimaryName()Landroid/widget/TextView;

    move-result-object v1

    invoke-direct {v6, v11, v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getPrimaryNameTextForUser(Lcom/discord/models/domain/ModelUser;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getSecondaryName()Landroid/widget/TextView;

    move-result-object v1

    invoke-direct {v6, v11, v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getSecondaryNameTextForUser(Lcom/discord/models/domain/ModelUser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v1, v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 201
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getAvatar()Landroid/widget/ImageView;

    move-result-object v14

    invoke-static {v11, v7}, Lcom/discord/utilities/icon/IconUtils;->getForUser(Lcom/discord/models/domain/ModelUser;Z)Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    const/16 v17, 0x0

    iget-object v0, v6, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->imagesChangeDetector:Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;

    move-object/from16 v18, v0

    check-cast v18, Lcom/discord/utilities/images/MGImages$ChangeDetector;

    const/16 v19, 0xc

    const/16 v20, 0x0

    invoke-static/range {v14 .. v20}, Lcom/discord/utilities/icon/IconUtils;->setIcon$default(Landroid/widget/ImageView;Ljava/lang/String;ILkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    .line 203
    invoke-direct/range {p0 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->configureRoles(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;)V

    .line 205
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getViewProfileButton()Landroid/widget/TextView;

    move-result-object v14

    const v15, 0x7f0401b2

    const v5, 0x7f040165

    if-eqz v12, :cond_5

    const v0, 0x7f12100b

    .line 207
    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setText(I)V

    .line 208
    new-instance v16, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureUI$$inlined$apply$lambda$1;

    move-object/from16 v0, v16

    move-object v1, v14

    move-object/from16 v2, p0

    move v3, v12

    move-object v4, v11

    const v8, 0x7f040165

    move-object/from16 v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureUI$$inlined$apply$lambda$1;-><init>(Landroid/widget/TextView;Lcom/discord/widgets/user/usersheet/WidgetUserSheet;ZLcom/discord/models/domain/ModelUser;Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;)V

    move-object/from16 v0, v16

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    move-object v0, v14

    check-cast v0, Landroid/view/View;

    invoke-static {v0, v15, v8}, Lcom/discord/utilities/drawable/DrawableCompat;->getThemedDrawableRes(Landroid/view/View;II)I

    move-result v22

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0xe

    const/16 v27, 0x0

    move-object/from16 v21, v14

    invoke-static/range {v21 .. v27}, Lcom/discord/utilities/drawable/DrawableCompat;->setCompoundDrawablesCompat$default(Landroid/widget/TextView;IIIIILjava/lang/Object;)V

    goto :goto_3

    :cond_5
    const v8, 0x7f040165

    const v0, 0x7f120e84

    .line 215
    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setText(I)V

    .line 216
    new-instance v16, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureUI$$inlined$apply$lambda$2;

    move-object/from16 v0, v16

    move-object v1, v14

    move-object/from16 v2, p0

    move v3, v12

    move-object v4, v11

    move-object/from16 v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureUI$$inlined$apply$lambda$2;-><init>(Landroid/widget/TextView;Lcom/discord/widgets/user/usersheet/WidgetUserSheet;ZLcom/discord/models/domain/ModelUser;Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;)V

    move-object/from16 v0, v16

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 221
    move-object v0, v14

    check-cast v0, Landroid/view/View;

    invoke-static {v0, v8, v15}, Lcom/discord/utilities/drawable/DrawableCompat;->getThemedDrawableRes(Landroid/view/View;II)I

    move-result v22

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0xe

    const/16 v27, 0x0

    move-object/from16 v21, v14

    invoke-static/range {v21 .. v27}, Lcom/discord/utilities/drawable/DrawableCompat;->setCompoundDrawablesCompat$default(Landroid/widget/TextView;IIIIILjava/lang/Object;)V

    .line 225
    :goto_3
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getHeader()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureUI$3;

    invoke-direct {v1, v6, v11}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureUI$3;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;Lcom/discord/models/domain/ModelUser;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 230
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getSendMessageButton()Landroid/view/View;

    move-result-object v8

    if-nez v12, :cond_6

    .line 231
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getType()I

    move-result v0

    if-eq v0, v7, :cond_6

    const/4 v0, 0x0

    goto :goto_4

    :cond_6
    const/4 v0, 0x0

    const/4 v7, 0x0

    :goto_4
    invoke-static {v8, v7, v0, v13, v9}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    .line 232
    new-instance v7, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureUI$$inlined$apply$lambda$3;

    move-object v0, v7

    move-object v1, v8

    move-object/from16 v2, p0

    move v3, v12

    move-object/from16 v4, p1

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureUI$$inlined$apply$lambda$3;-><init>(Landroid/view/View;Lcom/discord/widgets/user/usersheet/WidgetUserSheet;ZLcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;Lcom/discord/models/domain/ModelUser;)V

    check-cast v7, Landroid/view/View$OnClickListener;

    invoke-virtual {v8, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 238
    invoke-direct/range {p0 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->configureVoiceSection(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;)V

    .line 239
    invoke-direct/range {p0 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->configureAdministrativeSection(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;)V

    .line 240
    invoke-direct/range {p0 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->configureDeveloperSection(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;)V

    .line 242
    sget-object v0, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    if-eqz v10, :cond_7

    invoke-virtual {v10}, Lcom/discord/models/domain/ModelPresence$Activity;->getName()Ljava/lang/String;

    move-result-object v1

    goto :goto_5

    :cond_7
    move-object v1, v9

    :goto_5
    if-eqz v10, :cond_8

    invoke-virtual {v10}, Lcom/discord/models/domain/ModelPresence$Activity;->getGamePlatform()Ljava/lang/String;

    move-result-object v9

    :cond_8
    invoke-virtual {v0, v1, v9}, Lcom/discord/utilities/analytics/AnalyticsTracker;->openUserSheet(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private final configureVoiceSection(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;)V
    .locals 6

    .line 257
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getUserVolumeSeekbar()Landroid/widget/SeekBar;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->isMe()Z

    move-result v1

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static {v0, v1, v5, v4, v3}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    .line 258
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getUserVolumeSeekbar()Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->getOutputVolume()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 260
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getUserMutedCheck()Landroidx/appcompat/widget/SwitchCompat;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->isMe()Z

    move-result v1

    xor-int/2addr v1, v2

    invoke-static {v0, v1, v5, v4, v3}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    .line 261
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getUserMutedCheck()Landroidx/appcompat/widget/SwitchCompat;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->isUserMuted()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/SwitchCompat;->setChecked(Z)V

    .line 262
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getUserMutedCheck()Landroidx/appcompat/widget/SwitchCompat;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureVoiceSection$1;

    invoke-direct {v1, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureVoiceSection$1;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;)V

    check-cast v1, Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/SwitchCompat;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 270
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getAudioSettingsContainer()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;->isMe()Z

    move-result p1

    xor-int/2addr p1, v2

    invoke-static {v0, p1, v5, v4, v3}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    .line 271
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getAudioSettingsDivider()Landroid/view/View;

    move-result-object p1

    .line 273
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getAudioSettingsContainer()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getUserVolumeSeekbar()Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getUserMutedCheck()Landroidx/appcompat/widget/SwitchCompat;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/appcompat/widget/SwitchCompat;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 271
    :cond_1
    :goto_0
    invoke-static {p1, v2, v5, v4, v3}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    return-void
.end method

.method private final getActionsDivider()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->actionsDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getActivityDivider()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->activityDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getAdministrativeDivider()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->administrativeDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x12

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getAdministrativeHeader()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->administrativeHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x11

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getAudioSettingsContainer()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->audioSettingsContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getAudioSettingsDivider()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->audioSettingsDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getAvatar()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->avatar$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getBanButton()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->banButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x15

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getCopyIdButton()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->copyIdButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x1b

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getDeveloperDivider()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->developerDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x1a

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getDeveloperHeader()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->developerHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x19

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getEditMemberButton()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->editMemberButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x13

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getGuildContainer()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->guildContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getGuildHeader()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->guildHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getHeader()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->header$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getKickButton()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->kickButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x14

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getPresenceIndicator()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->presenceIndicator$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getPrimaryName()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->primaryName$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getPrimaryNameTextForUser(Lcom/discord/models/domain/ModelUser;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .locals 7

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 400
    new-instance v1, Landroid/text/style/AbsoluteSizeSpan;

    invoke-virtual {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0013

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Landroid/text/style/AbsoluteSizeSpan;-><init>(IZ)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 401
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    move-object v4, p0

    check-cast v4, Landroidx/fragment/app/Fragment;

    const v5, 0x7f040292

    invoke-static {v4, v5}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroidx/fragment/app/Fragment;I)I

    move-result v4

    invoke-direct {v1, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    aput-object v1, v0, v3

    .line 399
    invoke-static {v0}, Lkotlin/a/m;->k([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const/16 v1, 0x21

    if-eqz p2, :cond_1

    .line 405
    new-instance p1, Landroid/text/SpannableStringBuilder;

    move-object v3, p2

    check-cast v3, Ljava/lang/CharSequence;

    invoke-direct {p1, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 406
    check-cast v0, Ljava/lang/Iterable;

    .line 470
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 408
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    .line 409
    invoke-virtual {p1, v3, v2, v4, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :cond_0
    return-object p1

    .line 418
    :cond_1
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getUserNameWithDiscriminator()Ljava/lang/String;

    move-result-object p2

    .line 419
    new-instance v4, Landroid/text/SpannableStringBuilder;

    move-object v5, p2

    check-cast v5, Ljava/lang/CharSequence;

    invoke-direct {v4, v5}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 421
    check-cast v0, Ljava/lang/Iterable;

    .line 472
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 423
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getUsername()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    .line 424
    invoke-virtual {v4, v5, v2, v6, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_1

    .line 432
    :cond_2
    new-instance v0, Landroid/text/style/AbsoluteSizeSpan;

    invoke-virtual {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0b0012

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-direct {v0, v2, v3}, Landroid/text/style/AbsoluteSizeSpan;-><init>(IZ)V

    .line 433
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getUsername()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    .line 434
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2

    .line 431
    invoke-virtual {v4, v0, p1, p2, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    return-object v4
.end method

.method private final getRichPresenceContainer()Landroid/widget/FrameLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->richPresenceContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    return-object v0
.end method

.method private final getRolesRecycler()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->rolesRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getSecondaryName()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->secondaryName$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getSecondaryNameTextForUser(Lcom/discord/models/domain/ModelUser;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    if-eqz p2, :cond_0

    .line 442
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getUserNameWithDiscriminator()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private final getSendMessageButton()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->sendMessageButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x10

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getServerDeafenButton()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->serverDeafenButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x17

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getServerMoveUserButton()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->serverMoveUserButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x18

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getServerMuteButton()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->serverMuteButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x16

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getUserMutedCheck()Landroidx/appcompat/widget/SwitchCompat;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->userMutedCheck$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/SwitchCompat;

    return-object v0
.end method

.method private final getUserVolumeSeekbar()Landroid/widget/SeekBar;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->userVolumeSeekbar$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    return-object v0
.end method

.method private final getViewProfileButton()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->viewProfileButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xf

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public static final show(JJLandroidx/fragment/app/FragmentManager;)V
    .locals 9

    sget-object v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->Companion:Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;

    const/4 v6, 0x0

    const/16 v7, 0x8

    const/4 v8, 0x0

    move-wide v1, p0

    move-wide v3, p2

    move-object v5, p4

    invoke-static/range {v0 .. v8}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;->show$default(Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;JJLandroidx/fragment/app/FragmentManager;Ljava/lang/Long;ILjava/lang/Object;)V

    return-void
.end method

.method public static final show(JJLandroidx/fragment/app/FragmentManager;Ljava/lang/Long;)V
    .locals 7

    sget-object v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->Companion:Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;

    move-wide v1, p0

    move-wide v3, p2

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;->show(JJLandroidx/fragment/app/FragmentManager;Ljava/lang/Long;)V

    return-void
.end method


# virtual methods
.method public final bindSubscriptions(Lrx/subscriptions/CompositeSubscription;)V
    .locals 13

    const-string v0, "compositeSubscription"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 157
    invoke-virtual {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_USER_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 158
    invoke-virtual {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "ARG_CHANNEL_ID"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 160
    iget-object v4, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->viewModel:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    const-string v5, "viewModel"

    if-nez v4, :cond_0

    invoke-static {v5}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v4, v0, v1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->updateUserId(J)V

    .line 161
    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->viewModel:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    if-nez v0, :cond_1

    invoke-static {v5}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, v2, v3}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->updateChannelId(J)V

    .line 163
    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->viewModel:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    if-nez v0, :cond_2

    invoke-static {v5}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->observeViewState()Lrx/Observable;

    move-result-object v0

    .line 164
    move-object v1, p0

    check-cast v1, Lcom/discord/app/AppComponent;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v3, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    .line 166
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const/4 v6, 0x0

    .line 167
    new-instance v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$bindSubscriptions$1;

    move-object v1, p0

    check-cast v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    invoke-direct {v0, v1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$bindSubscriptions$1;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;)V

    move-object v10, v0

    check-cast v10, Lkotlin/jvm/functions/Function1;

    .line 168
    new-instance v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$bindSubscriptions$2;

    invoke-direct {v0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$bindSubscriptions$2;-><init>(Lrx/subscriptions/CompositeSubscription;)V

    move-object v7, v0

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x1a

    const/4 v12, 0x0

    .line 165
    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final getContentViewResId()I
    .locals 1

    const v0, 0x7f0d01d8

    return v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    .line 152
    invoke-super {p0, p1, p2, p3}, Lcom/discord/app/AppBottomSheet;->onActivityResult(IILandroid/content/Intent;)V

    .line 153
    sget-object v0, Lcom/discord/widgets/channels/WidgetChannelSelector;->Companion:Lcom/discord/widgets/channels/WidgetChannelSelector$Companion;

    iget-object v3, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->onMoveChannelSelected:Lkotlin/jvm/functions/Function3;

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move v1, p1

    move-object v2, p3

    invoke-static/range {v0 .. v6}, Lcom/discord/widgets/channels/WidgetChannelSelector$Companion;->handleResult$default(Lcom/discord/widgets/channels/WidgetChannelSelector$Companion;ILandroid/content/Intent;Lkotlin/jvm/functions/Function3;ZILjava/lang/Object;)V

    return-void
.end method

.method public final onPause()V
    .locals 7

    .line 138
    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->activityViewHolder:Lcom/discord/widgets/user/ViewHolderUserRichPresence;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/user/ViewHolderUserRichPresence;->disposeSubscriptions()V

    .line 140
    :cond_0
    invoke-virtual {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_USER_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 141
    invoke-virtual {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "ARG_GUILD_ID"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v6, v0, v4

    if-lez v6, :cond_1

    cmp-long v6, v2, v4

    if-lez v6, :cond_1

    .line 143
    sget-object v4, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    .line 144
    invoke-virtual {v4}, Lcom/discord/stores/StoreStream$Companion;->getGuildSubscriptions()Lcom/discord/stores/StoreGuildSubscriptions;

    move-result-object v4

    .line 145
    invoke-virtual {v4, v2, v3, v0, v1}, Lcom/discord/stores/StoreGuildSubscriptions;->unsubscribeUser(JJ)V

    .line 148
    :cond_1
    invoke-super {p0}, Lcom/discord/app/AppBottomSheet;->onPause()V

    return-void
.end method

.method public final onResume()V
    .locals 7

    .line 116
    invoke-super {p0}, Lcom/discord/app/AppBottomSheet;->onResume()V

    .line 118
    invoke-virtual {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_USER_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 119
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getUserVolumeSeekbar()Landroid/widget/SeekBar;

    move-result-object v2

    new-instance v3, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onResume$1;

    invoke-direct {v3, v0, v1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onResume$1;-><init>(J)V

    check-cast v3, Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 129
    invoke-virtual {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "ARG_GUILD_ID"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v6, v0, v4

    if-lez v6, :cond_0

    cmp-long v6, v2, v4

    if-lez v6, :cond_0

    .line 131
    sget-object v4, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    .line 132
    invoke-virtual {v4}, Lcom/discord/stores/StoreStream$Companion;->getGuildSubscriptions()Lcom/discord/stores/StoreGuildSubscriptions;

    move-result-object v4

    .line 133
    invoke-virtual {v4, v2, v3, v0, v1}, Lcom/discord/stores/StoreGuildSubscriptions;->subscribeUser(JJ)V

    :cond_0
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-super {p0, p1, p2}, Lcom/discord/app/AppBottomSheet;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 105
    sget-object p1, Lcom/discord/widgets/roles/RolesAdapter;->Companion:Lcom/discord/widgets/roles/RolesAdapter$Companion;

    .line 106
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getRolesRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p2

    .line 107
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getRolesRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040298

    invoke-static {v0, v1}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v0

    .line 105
    invoke-virtual {p1, p2, v0}, Lcom/discord/widgets/roles/RolesAdapter$Companion;->configure(Landroidx/recyclerview/widget/RecyclerView;I)Lcom/discord/widgets/roles/RolesAdapter;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->rolesAdapter:Lcom/discord/widgets/roles/RolesAdapter;

    .line 110
    invoke-virtual {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    .line 111
    new-instance p2, Lcom/discord/widgets/guilds/invite/WidgetUserSheetViewModelFactory;

    invoke-direct {p2}, Lcom/discord/widgets/guilds/invite/WidgetUserSheetViewModelFactory;-><init>()V

    check-cast p2, Landroidx/lifecycle/ViewModelProvider$Factory;

    .line 109
    invoke-static {p1, p2}, Landroidx/lifecycle/ViewModelProviders;->of(Landroidx/fragment/app/FragmentActivity;Landroidx/lifecycle/ViewModelProvider$Factory;)Landroidx/lifecycle/ViewModelProvider;

    move-result-object p1

    .line 112
    const-class p2, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    invoke-virtual {p1, p2}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string p2, "ViewModelProviders.of(\n \u2026eetViewModel::class.java)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    iput-object p1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->viewModel:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    return-void
.end method
