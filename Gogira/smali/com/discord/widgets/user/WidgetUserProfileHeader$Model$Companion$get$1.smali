.class final Lcom/discord/widgets/user/WidgetUserProfileHeader$Model$Companion$get$1;
.super Ljava/lang/Object;
.source "WidgetUserProfileHeader.kt"

# interfaces
.implements Lrx/functions/Func7;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/user/WidgetUserProfileHeader$Model$Companion;->get(J)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "T4:",
        "Ljava/lang/Object;",
        "T5:",
        "Ljava/lang/Object;",
        "T6:",
        "Ljava/lang/Object;",
        "T7:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func7<",
        "TT1;TT2;TT3;TT4;TT5;TT6;TT7;TR;>;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/user/WidgetUserProfileHeader$Model$Companion$get$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/user/WidgetUserProfileHeader$Model$Companion$get$1;

    invoke-direct {v0}, Lcom/discord/widgets/user/WidgetUserProfileHeader$Model$Companion$get$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/user/WidgetUserProfileHeader$Model$Companion$get$1;->INSTANCE:Lcom/discord/widgets/user/WidgetUserProfileHeader$Model$Companion$get$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/domain/ModelUser;Ljava/lang/Integer;Lcom/discord/models/domain/ModelPresence;Lcom/discord/models/domain/ModelExperiment;Lcom/discord/models/domain/ModelUserProfile;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelApplicationStream;)Lcom/discord/widgets/user/WidgetUserProfileHeader$Model;
    .locals 10

    const/4 v0, 0x0

    if-eqz p4, :cond_0

    .line 261
    invoke-virtual {p4}, Lcom/discord/models/domain/ModelExperiment;->getBucket()I

    move-result v1

    move v7, v1

    goto :goto_0

    :cond_0
    const/4 v7, 0x0

    :goto_0
    if-eqz p1, :cond_2

    .line 263
    invoke-static {p2}, Lcom/discord/models/domain/ModelUserRelationship;->getType(Ljava/lang/Integer;)I

    move-result v4

    if-eqz p7, :cond_1

    const/4 v0, 0x1

    const/4 v9, 0x1

    goto :goto_1

    :cond_1
    const/4 v9, 0x0

    .line 265
    :goto_1
    new-instance v0, Lcom/discord/widgets/user/WidgetUserProfileHeader$Model;

    const-string v1, "profile"

    move-object v6, p5

    invoke-static {p5, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "meUser"

    move-object/from16 v2, p6

    invoke-static {v2, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p6 .. p6}, Lcom/discord/models/domain/ModelUser;->isPremium()Z

    move-result v8

    move-object v2, v0

    move-object v3, p1

    move-object v5, p3

    invoke-direct/range {v2 .. v9}, Lcom/discord/widgets/user/WidgetUserProfileHeader$Model;-><init>(Lcom/discord/models/domain/ModelUser;ILcom/discord/models/domain/ModelPresence;Lcom/discord/models/domain/ModelUserProfile;IZZ)V

    return-object v0

    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 235
    check-cast p1, Lcom/discord/models/domain/ModelUser;

    check-cast p2, Ljava/lang/Integer;

    check-cast p3, Lcom/discord/models/domain/ModelPresence;

    check-cast p4, Lcom/discord/models/domain/ModelExperiment;

    check-cast p5, Lcom/discord/models/domain/ModelUserProfile;

    check-cast p6, Lcom/discord/models/domain/ModelUser;

    check-cast p7, Lcom/discord/models/domain/ModelApplicationStream;

    invoke-virtual/range {p0 .. p7}, Lcom/discord/widgets/user/WidgetUserProfileHeader$Model$Companion$get$1;->call(Lcom/discord/models/domain/ModelUser;Ljava/lang/Integer;Lcom/discord/models/domain/ModelPresence;Lcom/discord/models/domain/ModelExperiment;Lcom/discord/models/domain/ModelUserProfile;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelApplicationStream;)Lcom/discord/widgets/user/WidgetUserProfileHeader$Model;

    move-result-object p1

    return-object p1
.end method
