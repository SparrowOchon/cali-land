.class public final synthetic Lcom/discord/widgets/user/-$$Lambda$OShgKJUNgkGAmZbvdNybwxrNxhs;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/functions/Func5;


# static fields
.field public static final synthetic INSTANCE:Lcom/discord/widgets/user/-$$Lambda$OShgKJUNgkGAmZbvdNybwxrNxhs;


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/user/-$$Lambda$OShgKJUNgkGAmZbvdNybwxrNxhs;

    invoke-direct {v0}, Lcom/discord/widgets/user/-$$Lambda$OShgKJUNgkGAmZbvdNybwxrNxhs;-><init>()V

    sput-object v0, Lcom/discord/widgets/user/-$$Lambda$OShgKJUNgkGAmZbvdNybwxrNxhs;->INSTANCE:Lcom/discord/widgets/user/-$$Lambda$OShgKJUNgkGAmZbvdNybwxrNxhs;

    return-void
.end method

.method private synthetic constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    move-object v0, p1

    check-cast v0, Lcom/discord/models/domain/ModelUserProfile;

    move-object v1, p2

    check-cast v1, Ljava/lang/String;

    move-object v2, p3

    check-cast v2, Lcom/discord/widgets/user/ModelRichPresence;

    check-cast p4, Ljava/lang/Long;

    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    move-object v5, p5

    check-cast v5, Lcom/discord/utilities/streams/StreamContext;

    invoke-static/range {v0 .. v5}, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->create(Lcom/discord/models/domain/ModelUserProfile;Ljava/lang/String;Lcom/discord/widgets/user/ModelRichPresence;JLcom/discord/utilities/streams/StreamContext;)Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;

    move-result-object p1

    return-object p1
.end method
