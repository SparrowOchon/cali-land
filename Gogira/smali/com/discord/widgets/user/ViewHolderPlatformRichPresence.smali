.class public final Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;
.super Lcom/discord/widgets/user/ViewHolderUserRichPresence;
.source "ViewHolderPlatformRichPresence.kt"


# instance fields
.field private final connectButton:Landroid/view/View;

.field private final connectButtonText:Landroid/widget/TextView;

.field private final containerView:Landroid/view/View;

.field private final subscriptions:Lrx/subscriptions/CompositeSubscription;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string v0, "containerView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x3

    .line 29
    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/user/ViewHolderUserRichPresence;-><init>(Landroid/view/View;I)V

    iput-object p1, p0, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->containerView:Landroid/view/View;

    .line 31
    iget-object p1, p0, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->containerView:Landroid/view/View;

    const v0, 0x7f0a056e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->connectButton:Landroid/view/View;

    .line 32
    iget-object p1, p0, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->containerView:Landroid/view/View;

    const v0, 0x7f0a056f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->connectButtonText:Landroid/widget/TextView;

    .line 34
    new-instance p1, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {p1}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    return-void
.end method

.method public static final synthetic access$getConnectButton$p(Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;)Landroid/view/View;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->connectButton:Landroid/view/View;

    return-object p0
.end method

.method private final configureImages(Lcom/discord/utilities/platform/Platform;Lcom/discord/widgets/user/ModelRichPresence;Lcom/discord/utilities/streams/StreamContext;)V
    .locals 12

    .line 92
    sget-object v0, Lcom/discord/utilities/platform/Platform;->NONE:Lcom/discord/utilities/platform/Platform;

    if-eq p1, v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->connectButton:Landroid/view/View;

    invoke-virtual {p1}, Lcom/discord/utilities/platform/Platform;->getColorResId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 94
    iget-object v2, p0, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->connectButtonText:Landroid/widget/TextView;

    const-string v0, "connectButtonText"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/utilities/platform/Platform;->getWhitePlatformImage()I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0xe

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setCompoundDrawableWithIntrinsicBounds$default(Landroid/widget/TextView;IIIIILjava/lang/Object;)V

    .line 97
    :cond_0
    sget-object v0, Lcom/discord/utilities/platform/Platform;->XBOX:Lcom/discord/utilities/platform/Platform;

    const/4 v1, 0x0

    const-string v2, "largeIv"

    if-ne p1, v0, :cond_1

    .line 98
    invoke-virtual {p0}, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->getLargeIv()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object p2

    invoke-static {p2, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/discord/utilities/platform/Platform;->getPlatformImage()I

    move-result p1

    const/4 p3, 0x4

    invoke-static {p2, p1, v1, p3, v1}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;ILcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    return-void

    :cond_1
    if-eqz p2, :cond_2

    .line 101
    invoke-virtual {p2}, Lcom/discord/widgets/user/ModelRichPresence;->getPrimaryActivity()Lcom/discord/models/domain/ModelPresence$Activity;

    move-result-object p1

    goto :goto_0

    :cond_2
    move-object p1, v1

    :goto_0
    invoke-super {p0, p1, p3}, Lcom/discord/widgets/user/ViewHolderUserRichPresence;->configureAssetUi(Lcom/discord/models/domain/ModelPresence$Activity;Lcom/discord/utilities/streams/StreamContext;)V

    .line 103
    invoke-virtual {p0}, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->getLargeIv()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object p1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/drawee/view/SimpleDraweeView;->getVisibility()I

    move-result p1

    if-eqz p1, :cond_5

    if-eqz p2, :cond_4

    .line 105
    invoke-virtual {p2}, Lcom/discord/widgets/user/ModelRichPresence;->getPrimaryApplication()Lcom/discord/models/domain/ModelApplication;

    move-result-object p1

    if-eqz p1, :cond_4

    sget-object v3, Lcom/discord/utilities/icon/IconUtils;->INSTANCE:Lcom/discord/utilities/icon/IconUtils;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelApplication;->getId()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelApplication;->getIcon()Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_3

    const-string p1, ""

    :cond_3
    move-object v6, p1

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    invoke-static/range {v3 .. v9}, Lcom/discord/utilities/icon/IconUtils;->getApplicationIcon$default(Lcom/discord/utilities/icon/IconUtils;JLjava/lang/String;IILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :cond_4
    move-object v4, v1

    .line 106
    invoke-virtual {p0}, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->getLargeIv()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object p1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/facebook/drawee/view/SimpleDraweeView;->setVisibility(I)V

    .line 107
    invoke-virtual {p0}, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->getLargeIv()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object p1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, p1

    check-cast v3, Landroid/widget/ImageView;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x7c

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    :cond_5
    return-void
.end method


# virtual methods
.method public final configureUi(Lcom/discord/widgets/user/ModelRichPresence;Lcom/discord/utilities/streams/StreamContext;ZLcom/discord/models/domain/ModelUser;)V
    .locals 10

    .line 46
    invoke-virtual {p0}, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->disposeSubscriptions()V

    const/4 p3, 0x0

    if-eqz p1, :cond_0

    .line 47
    invoke-virtual {p1}, Lcom/discord/widgets/user/ModelRichPresence;->getPrimaryActivity()Lcom/discord/models/domain/ModelPresence$Activity;

    move-result-object p4

    goto :goto_0

    :cond_0
    move-object p4, p3

    :goto_0
    if-eqz p4, :cond_6

    .line 50
    invoke-virtual {p4}, Lcom/discord/models/domain/ModelPresence$Activity;->isGamePlatform()Z

    move-result v0

    if-nez v0, :cond_1

    goto/16 :goto_3

    .line 54
    :cond_1
    iget-object v0, p0, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->containerView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 57
    invoke-virtual {p4}, Lcom/discord/models/domain/ModelPresence$Activity;->isXboxActivity()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/discord/utilities/platform/Platform;->XBOX:Lcom/discord/utilities/platform/Platform;

    goto :goto_1

    .line 58
    :cond_2
    sget-object v0, Lcom/discord/utilities/platform/Platform;->Companion:Lcom/discord/utilities/platform/Platform$Companion;

    invoke-virtual {p4}, Lcom/discord/models/domain/ModelPresence$Activity;->getPlatform()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    const-string v2, ""

    :cond_3
    invoke-virtual {v0, v2}, Lcom/discord/utilities/platform/Platform$Companion;->from(Ljava/lang/String;)Lcom/discord/utilities/platform/Platform;

    move-result-object v0

    .line 60
    :goto_1
    invoke-virtual {p0}, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->getHeaderTv()Landroid/widget/TextView;

    move-result-object v2

    const-string v3, "headerTv"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->getHeaderTv()Landroid/widget/TextView;

    move-result-object v4

    invoke-static {v4, v3}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "headerTv.context"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3, p4}, Lcom/discord/utilities/presence/PresenceUtils;->getActivityHeader(Landroid/content/Context;Lcom/discord/models/domain/ModelPresence$Activity;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    invoke-virtual {p0}, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->getTitleTv()Landroid/widget/TextView;

    move-result-object v2

    const-string v3, "titleTv"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p4}, Lcom/discord/models/domain/ModelPresence$Activity;->getName()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    invoke-virtual {p0}, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->getTimeTv()Landroid/widget/TextView;

    move-result-object v2

    const-string v3, "timeTv"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p4}, Lcom/discord/models/domain/ModelPresence$Activity;->getTimestamps()Lcom/discord/models/domain/ModelPresence$Timestamps;

    move-result-object p4

    if-eqz p4, :cond_4

    invoke-virtual {p0, p4}, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->friendlyTime(Lcom/discord/models/domain/ModelPresence$Timestamps;)Ljava/lang/CharSequence;

    move-result-object p4

    goto :goto_2

    :cond_4
    move-object p4, p3

    :goto_2
    invoke-static {v2, p4}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 64
    invoke-virtual {p0}, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->getTextContainer()Landroid/view/View;

    move-result-object p4

    const-string v2, "textContainer"

    invoke-static {p4, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-virtual {p4, v2}, Landroid/view/View;->setSelected(Z)V

    .line 66
    iget-object p4, p0, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->connectButton:Landroid/view/View;

    const-string v3, "connectButton"

    invoke-static {p4, v3}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/discord/utilities/platform/Platform;->getEnabled()Z

    move-result v3

    const/4 v4, 0x2

    invoke-static {p4, v3, v1, v4, p3}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    .line 67
    iget-object p3, p0, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->connectButtonText:Landroid/widget/TextView;

    const-string p4, "connectButtonText"

    invoke-static {p3, p4}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p4, p0, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->containerView:Landroid/view/View;

    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p4

    const v3, 0x7f12121b

    new-array v2, v2, [Ljava/lang/Object;

    .line 68
    invoke-virtual {v0}, Lcom/discord/utilities/platform/Platform;->getProperName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v1

    .line 67
    invoke-virtual {p4, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p4

    check-cast p4, Ljava/lang/CharSequence;

    invoke-virtual {p3, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    invoke-virtual {v0}, Lcom/discord/utilities/platform/Platform;->getEnabled()Z

    move-result p3

    if-eqz p3, :cond_5

    .line 71
    sget-object p3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    .line 72
    invoke-virtual {p3}, Lcom/discord/stores/StoreStream$Companion;->getUserConnections()Lcom/discord/stores/StoreUserConnections;

    move-result-object p3

    .line 73
    invoke-virtual {p3}, Lcom/discord/stores/StoreUserConnections;->getConnectedAccounts()Lrx/Observable;

    move-result-object p3

    .line 74
    new-instance p4, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence$configureUi$1;

    invoke-direct {p4, v0}, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence$configureUi$1;-><init>(Lcom/discord/utilities/platform/Platform;)V

    check-cast p4, Lrx/functions/b;

    invoke-virtual {p3, p4}, Lrx/Observable;->e(Lrx/functions/b;)Lrx/Observable;

    move-result-object p3

    .line 75
    invoke-virtual {p3}, Lrx/Observable;->JL()Lrx/Observable;

    move-result-object p3

    const-string p4, "StoreStream\n          .g\u2026  .distinctUntilChanged()"

    invoke-static {p3, p4}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    invoke-static {p3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui(Lrx/Observable;)Lrx/Observable;

    move-result-object v1

    .line 77
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const/4 v3, 0x0

    new-instance p3, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence$configureUi$2;

    iget-object p4, p0, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    invoke-direct {p3, p4}, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence$configureUi$2;-><init>(Lrx/subscriptions/CompositeSubscription;)V

    move-object v4, p3

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance p3, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence$configureUi$3;

    invoke-direct {p3, p0}, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence$configureUi$3;-><init>(Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;)V

    move-object v7, p3

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/16 v8, 0x1a

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 81
    iget-object p3, p0, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->connectButton:Landroid/view/View;

    new-instance p4, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence$configureUi$4;

    invoke-direct {p4, v0}, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence$configureUi$4;-><init>(Lcom/discord/utilities/platform/Platform;)V

    check-cast p4, Landroid/view/View$OnClickListener;

    invoke-virtual {p3, p4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    :cond_5
    invoke-direct {p0, v0, p1, p2}, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->configureImages(Lcom/discord/utilities/platform/Platform;Lcom/discord/widgets/user/ModelRichPresence;Lcom/discord/utilities/streams/StreamContext;)V

    return-void

    .line 51
    :cond_6
    :goto_3
    iget-object p1, p0, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->containerView:Landroid/view/View;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final disposeSubscriptions()V
    .locals 1

    .line 37
    invoke-super {p0}, Lcom/discord/widgets/user/ViewHolderUserRichPresence;->disposeSubscriptions()V

    .line 38
    iget-object v0, p0, Lcom/discord/widgets/user/ViewHolderPlatformRichPresence;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->clear()V

    return-void
.end method
