.class Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;
.super Ljava/lang/Object;
.source "WidgetUserProfileInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/user/WidgetUserProfileInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Model"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/user/WidgetUserProfileInfo$Model$Item;
    }
.end annotation


# instance fields
.field private final connectedAccounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/widgets/user/WidgetUserProfileInfo$Model$Item;",
            ">;"
        }
    .end annotation
.end field

.field private final isMe:Z

.field private final notes:Ljava/lang/CharSequence;

.field private final richPresence:Lcom/discord/widgets/user/ModelRichPresence;

.field private final streamContext:Lcom/discord/utilities/streams/StreamContext;

.field private final user:Lcom/discord/models/domain/ModelUser;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/CharSequence;Lcom/discord/widgets/user/ModelRichPresence;Lcom/discord/models/domain/ModelUser;ZLcom/discord/utilities/streams/StreamContext;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/widgets/user/WidgetUserProfileInfo$Model$Item;",
            ">;",
            "Ljava/lang/CharSequence;",
            "Lcom/discord/widgets/user/ModelRichPresence;",
            "Lcom/discord/models/domain/ModelUser;",
            "Z",
            "Lcom/discord/utilities/streams/StreamContext;",
            ")V"
        }
    .end annotation

    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->connectedAccounts:Ljava/util/List;

    iput-object p2, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->notes:Ljava/lang/CharSequence;

    iput-object p3, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->richPresence:Lcom/discord/widgets/user/ModelRichPresence;

    iput-object p4, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->user:Lcom/discord/models/domain/ModelUser;

    iput-boolean p5, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->isMe:Z

    iput-object p6, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->streamContext:Lcom/discord/utilities/streams/StreamContext;

    return-void
.end method

.method static synthetic access$000(Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;)Ljava/util/List;
    .locals 0

    .line 143
    iget-object p0, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->connectedAccounts:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$100(Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;)Ljava/lang/CharSequence;
    .locals 0

    .line 143
    iget-object p0, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->notes:Ljava/lang/CharSequence;

    return-object p0
.end method

.method static synthetic access$200(Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;)Lcom/discord/widgets/user/ModelRichPresence;
    .locals 0

    .line 143
    iget-object p0, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->richPresence:Lcom/discord/widgets/user/ModelRichPresence;

    return-object p0
.end method

.method static synthetic access$300(Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;)Lcom/discord/utilities/streams/StreamContext;
    .locals 0

    .line 143
    iget-object p0, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->streamContext:Lcom/discord/utilities/streams/StreamContext;

    return-object p0
.end method

.method static synthetic access$400(Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;)Z
    .locals 0

    .line 143
    iget-boolean p0, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->isMe:Z

    return p0
.end method

.method static synthetic access$500(Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;)Lcom/discord/models/domain/ModelUser;
    .locals 0

    .line 143
    iget-object p0, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->user:Lcom/discord/models/domain/ModelUser;

    return-object p0
.end method

.method public static create(Lcom/discord/models/domain/ModelUserProfile;Ljava/lang/String;Lcom/discord/widgets/user/ModelRichPresence;JLcom/discord/utilities/streams/StreamContext;)Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;
    .locals 8

    .line 157
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelUserProfile;->getConnectedAccounts()Ljava/util/List;

    move-result-object v0

    .line 158
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 159
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelConnectedAccount;

    .line 160
    new-instance v3, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model$Item;

    invoke-direct {v3, v1}, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model$Item;-><init>(Lcom/discord/models/domain/ModelConnectedAccount;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 163
    :cond_0
    new-instance v0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;

    .line 167
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelUserProfile;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v5

    .line 168
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelUserProfile;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object p0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v3

    cmp-long p0, p3, v3

    if-nez p0, :cond_1

    const/4 p0, 0x1

    const/4 v6, 0x1

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    const/4 v6, 0x0

    :goto_1
    move-object v1, v0

    move-object v3, p1

    move-object v4, p2

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;-><init>(Ljava/util/List;Ljava/lang/CharSequence;Lcom/discord/widgets/user/ModelRichPresence;Lcom/discord/models/domain/ModelUser;ZLcom/discord/utilities/streams/StreamContext;)V

    return-object v0
.end method

.method public static get(J)Lrx/Observable;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;",
            ">;"
        }
    .end annotation

    .line 176
    invoke-static {}, Lcom/discord/stores/StoreStream;->getUserProfile()Lcom/discord/stores/StoreUserProfile;

    move-result-object v0

    .line 177
    invoke-virtual {v0, p0, p1}, Lcom/discord/stores/StoreUserProfile;->get(J)Lrx/Observable;

    move-result-object v1

    .line 179
    invoke-static {}, Lcom/discord/stores/StoreStream;->getUsersNotes()Lcom/discord/stores/StoreUserNotes;

    move-result-object v0

    .line 180
    invoke-virtual {v0, p0, p1}, Lcom/discord/stores/StoreUserNotes;->getForUserId(J)Lrx/Observable;

    move-result-object v2

    .line 182
    invoke-static {}, Lcom/discord/stores/StoreStream;->getPresences()Lcom/discord/stores/StoreUserPresence;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/discord/widgets/user/ModelRichPresence;->get(JLcom/discord/stores/StoreUserPresence;)Lrx/Observable;

    move-result-object v3

    .line 184
    invoke-static {}, Lcom/discord/stores/StoreStream;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v0

    .line 185
    invoke-virtual {v0}, Lcom/discord/stores/StoreUser;->getMeId()Lrx/Observable;

    move-result-object v4

    sget-object v0, Lcom/discord/utilities/streams/StreamContext;->Companion:Lcom/discord/utilities/streams/StreamContext$Companion;

    const/4 v5, 0x1

    .line 188
    invoke-virtual {v0, p0, p1, v5}, Lcom/discord/utilities/streams/StreamContext$Companion;->get(JZ)Lrx/Observable;

    move-result-object v5

    sget-object v6, Lcom/discord/widgets/user/-$$Lambda$OShgKJUNgkGAmZbvdNybwxrNxhs;->INSTANCE:Lcom/discord/widgets/user/-$$Lambda$OShgKJUNgkGAmZbvdNybwxrNxhs;

    .line 174
    invoke-static/range {v1 .. v6}, Lrx/Observable;->a(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func5;)Lrx/Observable;

    move-result-object p0

    .line 190
    invoke-static {}, Lcom/discord/app/i;->dC()Lrx/Observable$c;

    move-result-object p1

    invoke-virtual {p0, p1}, Lrx/Observable;->a(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method protected canEqual(Ljava/lang/Object;)Z
    .locals 0

    .line 142
    instance-of p1, p1, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 142
    :cond_0
    instance-of v1, p1, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    :cond_1
    check-cast p1, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;

    invoke-virtual {p1, p0}, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->canEqual(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    return v2

    :cond_2
    iget-object v1, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->connectedAccounts:Ljava/util/List;

    iget-object v3, p1, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->connectedAccounts:Ljava/util/List;

    if-nez v1, :cond_3

    if-eqz v3, :cond_4

    goto :goto_0

    :cond_3
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    :goto_0
    return v2

    :cond_4
    iget-object v1, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->notes:Ljava/lang/CharSequence;

    iget-object v3, p1, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->notes:Ljava/lang/CharSequence;

    if-nez v1, :cond_5

    if-eqz v3, :cond_6

    goto :goto_1

    :cond_5
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    :goto_1
    return v2

    :cond_6
    iget-object v1, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->richPresence:Lcom/discord/widgets/user/ModelRichPresence;

    iget-object v3, p1, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->richPresence:Lcom/discord/widgets/user/ModelRichPresence;

    if-nez v1, :cond_7

    if-eqz v3, :cond_8

    goto :goto_2

    :cond_7
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    :goto_2
    return v2

    :cond_8
    iget-object v1, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->user:Lcom/discord/models/domain/ModelUser;

    iget-object v3, p1, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->user:Lcom/discord/models/domain/ModelUser;

    if-nez v1, :cond_9

    if-eqz v3, :cond_a

    goto :goto_3

    :cond_9
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    :goto_3
    return v2

    :cond_a
    iget-boolean v1, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->isMe:Z

    iget-boolean v3, p1, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->isMe:Z

    if-eq v1, v3, :cond_b

    return v2

    :cond_b
    iget-object v1, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->streamContext:Lcom/discord/utilities/streams/StreamContext;

    iget-object p1, p1, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->streamContext:Lcom/discord/utilities/streams/StreamContext;

    if-nez v1, :cond_c

    if-eqz p1, :cond_d

    goto :goto_4

    :cond_c
    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_d

    :goto_4
    return v2

    :cond_d
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 142
    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->connectedAccounts:Ljava/util/List;

    const/16 v1, 0x2b

    if-nez v0, :cond_0

    const/16 v0, 0x2b

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    add-int/lit8 v0, v0, 0x3b

    iget-object v2, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->notes:Ljava/lang/CharSequence;

    mul-int/lit8 v0, v0, 0x3b

    if-nez v2, :cond_1

    const/16 v2, 0x2b

    goto :goto_1

    :cond_1
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :goto_1
    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->richPresence:Lcom/discord/widgets/user/ModelRichPresence;

    mul-int/lit8 v0, v0, 0x3b

    if-nez v2, :cond_2

    const/16 v2, 0x2b

    goto :goto_2

    :cond_2
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :goto_2
    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->user:Lcom/discord/models/domain/ModelUser;

    mul-int/lit8 v0, v0, 0x3b

    if-nez v2, :cond_3

    const/16 v2, 0x2b

    goto :goto_3

    :cond_3
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x3b

    iget-boolean v2, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->isMe:Z

    if-eqz v2, :cond_4

    const/16 v2, 0x4f

    goto :goto_4

    :cond_4
    const/16 v2, 0x61

    :goto_4
    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->streamContext:Lcom/discord/utilities/streams/StreamContext;

    mul-int/lit8 v0, v0, 0x3b

    if-nez v2, :cond_5

    goto :goto_5

    :cond_5
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_5
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 142
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WidgetUserProfileInfo.Model(connectedAccounts="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->connectedAccounts:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", notes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->notes:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", richPresence="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->richPresence:Lcom/discord/widgets/user/ModelRichPresence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", user="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->user:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isMe="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->isMe:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", streamContext="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/user/WidgetUserProfileInfo$Model;->streamContext:Lcom/discord/utilities/streams/StreamContext;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
