.class public final Lcom/discord/widgets/user/ViewHolderMusicRichPresence;
.super Lcom/discord/widgets/user/ViewHolderUserRichPresence;
.source "ViewHolderMusicRichPresence.kt"


# instance fields
.field private final containerView:Landroid/view/View;

.field private final musicDuration:Landroid/widget/TextView;

.field private final musicElapsed:Landroid/widget/TextView;

.field private final musicSuperBar:Lcom/miguelgaeta/super_bar/SuperBar;

.field private final playButton:Landroid/view/View;

.field private final playButtonText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string v0, "containerView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    .line 21
    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/user/ViewHolderUserRichPresence;-><init>(Landroid/view/View;I)V

    iput-object p1, p0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->containerView:Landroid/view/View;

    .line 23
    iget-object p1, p0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->containerView:Landroid/view/View;

    const v0, 0x7f0a056e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->playButton:Landroid/view/View;

    .line 24
    iget-object p1, p0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->containerView:Landroid/view/View;

    const v0, 0x7f0a056f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->playButtonText:Landroid/widget/TextView;

    .line 25
    iget-object p1, p0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->containerView:Landroid/view/View;

    const v0, 0x7f0a0571

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/miguelgaeta/super_bar/SuperBar;

    iput-object p1, p0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->musicSuperBar:Lcom/miguelgaeta/super_bar/SuperBar;

    .line 26
    iget-object p1, p0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->containerView:Landroid/view/View;

    const v0, 0x7f0a056d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->musicElapsed:Landroid/widget/TextView;

    .line 27
    iget-object p1, p0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->containerView:Landroid/view/View;

    const v0, 0x7f0a056c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->musicDuration:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public final configureUi(Lcom/discord/widgets/user/ModelRichPresence;Lcom/discord/utilities/streams/StreamContext;ZLcom/discord/models/domain/ModelUser;)V
    .locals 10

    .line 34
    invoke-virtual {p0}, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->disposeTimer()V

    .line 35
    iget-object v0, p0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->containerView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 36
    invoke-virtual {p1}, Lcom/discord/widgets/user/ModelRichPresence;->getPrimaryActivity()Lcom/discord/models/domain/ModelPresence$Activity;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v1

    :goto_0
    if-eqz p1, :cond_7

    .line 39
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPresence$Activity;->isRichPresence()Z

    move-result v2

    if-nez v2, :cond_1

    goto/16 :goto_4

    .line 43
    :cond_1
    iget-object v2, p0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->containerView:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 46
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPresence$Activity;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v4, "spotify"

    invoke-static {v2, v4}, Lkotlin/text/l;->af(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    .line 48
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPresence$Activity;->getState()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    const/16 v5, 0x3b

    const/16 v6, 0x2c

    .line 1063
    invoke-static {v4, v5, v6, v3}, Lkotlin/text/l;->a(Ljava/lang/String;CCZ)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_2
    move-object v4, v1

    .line 50
    :goto_1
    invoke-virtual {p0}, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->getHeaderTv()Landroid/widget/TextView;

    move-result-object v5

    const-string v6, "headerTv"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->getHeaderTv()Landroid/widget/TextView;

    move-result-object v7

    invoke-static {v7, v6}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v7}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "headerTv.context"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v6, p1}, Lcom/discord/utilities/presence/PresenceUtils;->getActivityHeader(Landroid/content/Context;Lcom/discord/models/domain/ModelPresence$Activity;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    invoke-virtual {p0}, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->getSmallIv()Landroid/widget/ImageView;

    move-result-object v5

    const-string v6, "smallIv"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Landroid/view/View;

    const/4 v6, 0x2

    invoke-static {v5, v3, v3, v6, v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    .line 53
    invoke-virtual {p0}, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->getTitleTv()Landroid/widget/TextView;

    move-result-object v5

    const-string v7, "titleTv"

    invoke-static {v5, v7}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPresence$Activity;->getDetails()Ljava/lang/String;

    move-result-object v7

    check-cast v7, Ljava/lang/CharSequence;

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    invoke-virtual {p0}, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->getDetailsTv()Landroid/widget/TextView;

    move-result-object v5

    const-string v7, "detailsTv"

    invoke-static {v5, v7}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const v7, 0x7f121232

    const/4 v8, 0x1

    new-array v9, v8, [Ljava/lang/Object;

    aput-object v4, v9, v3

    invoke-virtual {v0, v7, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-static {v5, v4}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 56
    invoke-virtual {p0}, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->getTimeTv()Landroid/widget/TextView;

    move-result-object v4

    const-string v5, "timeTv"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f121231

    new-array v7, v8, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPresence$Activity;->getAssets()Lcom/discord/models/domain/ModelPresence$Assets;

    move-result-object v9

    if-eqz v9, :cond_3

    invoke-virtual {v9}, Lcom/discord/models/domain/ModelPresence$Assets;->getLargeText()Ljava/lang/String;

    move-result-object v9

    goto :goto_2

    :cond_3
    move-object v9, v1

    :goto_2
    aput-object v9, v7, v3

    invoke-virtual {v0, v5, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-static {v4, v5}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 58
    invoke-virtual {p0}, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->getTextContainer()Landroid/view/View;

    move-result-object v4

    const-string v5, "textContainer"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4, v8}, Landroid/view/View;->setSelected(Z)V

    .line 60
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->configureAssetUi(Lcom/discord/models/domain/ModelPresence$Activity;Lcom/discord/utilities/streams/StreamContext;)V

    .line 62
    iget-object p2, p0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->playButton:Landroid/view/View;

    const-string v4, "playButton"

    invoke-static {p2, v4}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, v2, v3, v6, v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    .line 63
    iget-object p2, p0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->musicSuperBar:Lcom/miguelgaeta/super_bar/SuperBar;

    const-string v5, "musicSuperBar"

    invoke-static {p2, v5}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/view/View;

    invoke-static {p2, v2, v3, v6, v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    .line 64
    iget-object p2, p0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->musicDuration:Landroid/widget/TextView;

    const-string v5, "musicDuration"

    invoke-static {p2, v5}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/view/View;

    invoke-static {p2, v2, v3, v6, v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    .line 65
    iget-object p2, p0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->musicElapsed:Landroid/widget/TextView;

    const-string v5, "musicElapsed"

    invoke-static {p2, v5}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/view/View;

    invoke-static {p2, v2, v3, v6, v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    const-string p2, "playButtonText"

    if-eqz p3, :cond_4

    .line 68
    iget-object v1, p0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->playButtonText:Landroid/widget/TextView;

    invoke-static {v1, p2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const p2, 0x7f121217

    invoke-virtual {v0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object p2, p0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->playButton:Landroid/view/View;

    invoke-static {p2, v4}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, v3}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_3

    .line 71
    :cond_4
    iget-object v1, p0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->playButtonText:Landroid/widget/TextView;

    invoke-static {v1, p2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const p2, 0x7f121235

    new-array v2, v8, [Ljava/lang/Object;

    .line 72
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPresence$Activity;->getName()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_5

    const/16 v5, 0x3f

    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v5

    :cond_5
    aput-object v5, v2, v3

    .line 71
    invoke-virtual {v0, p2, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    iget-object p2, p0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->playButton:Landroid/view/View;

    invoke-static {p2, v4}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, v8}, Landroid/view/View;->setEnabled(Z)V

    .line 76
    :goto_3
    sget-object p2, Lcom/discord/utilities/integrations/SpotifyHelper;->INSTANCE:Lcom/discord/utilities/integrations/SpotifyHelper;

    const-string v1, "context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Lcom/discord/utilities/integrations/SpotifyHelper;->isSpotifyInstalled(Landroid/content/Context;)Z

    move-result p2

    if-eqz p2, :cond_6

    .line 77
    invoke-virtual {p0}, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->getTitleTv()Landroid/widget/TextView;

    move-result-object p2

    new-instance v0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence$configureUi$1;

    invoke-direct {v0, p1}, Lcom/discord/widgets/user/ViewHolderMusicRichPresence$configureUi$1;-><init>(Lcom/discord/models/domain/ModelPresence$Activity;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    iget-object p2, p0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->playButton:Landroid/view/View;

    new-instance v0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence$configureUi$2;

    invoke-direct {v0, p1}, Lcom/discord/widgets/user/ViewHolderMusicRichPresence$configureUi$2;-><init>(Lcom/discord/models/domain/ModelPresence$Activity;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    invoke-virtual {p0}, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->getLargeIv()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object p2

    new-instance v0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence$configureUi$3;

    invoke-direct {v0, p1, p4, p3}, Lcom/discord/widgets/user/ViewHolderMusicRichPresence$configureUi$3;-><init>(Lcom/discord/models/domain/ModelPresence$Activity;Lcom/discord/models/domain/ModelUser;Z)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Lcom/facebook/drawee/view/SimpleDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    .line 83
    :cond_6
    iget-object p1, p0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->playButton:Landroid/view/View;

    sget-object p2, Lcom/discord/widgets/user/ViewHolderMusicRichPresence$configureUi$4;->INSTANCE:Lcom/discord/widgets/user/ViewHolderMusicRichPresence$configureUi$4;

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    .line 40
    :cond_7
    :goto_4
    iget-object p1, p0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->containerView:Landroid/view/View;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected final setTimeTextViews(Lcom/discord/models/domain/ModelPresence$Timestamps;)V
    .locals 8

    if-eqz p1, :cond_1

    .line 89
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 90
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPresence$Timestamps;->getEndMs()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPresence$Timestamps;->getStartMs()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 91
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPresence$Timestamps;->getEndMs()J

    move-result-wide v4

    cmp-long v6, v0, v4

    if-ltz v6, :cond_0

    move-wide v0, v2

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPresence$Timestamps;->getStartMs()J

    move-result-wide v4

    sub-long/2addr v0, v4

    :goto_0
    long-to-double v4, v0

    long-to-double v6, v2

    .line 92
    invoke-static {v4, v5}, Ljava/lang/Double;->isNaN(D)Z

    invoke-static {v6, v7}, Ljava/lang/Double;->isNaN(D)Z

    div-double/2addr v4, v6

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    mul-double v4, v4, v6

    .line 94
    iget-object p1, p0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->musicSuperBar:Lcom/miguelgaeta/super_bar/SuperBar;

    const-string v6, "musicSuperBar"

    invoke-static {p1, v6}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/miguelgaeta/super_bar/SuperBar;->getConfig()Lcom/miguelgaeta/super_bar/SuperBarConfig;

    move-result-object p1

    const/16 v6, 0x15e

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    double-to-float v4, v4

    invoke-virtual {p1, v6, v4}, Lcom/miguelgaeta/super_bar/SuperBarConfig;->setBarValue(Ljava/lang/Integer;F)V

    .line 95
    iget-object p1, p0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->musicElapsed:Landroid/widget/TextView;

    const-string v4, "musicElapsed"

    invoke-static {p1, v4}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v4, Lcom/discord/utilities/time/TimeUtils;->INSTANCE:Lcom/discord/utilities/time/TimeUtils;

    const/4 v5, 0x0

    invoke-virtual {v4, v0, v1, v5}, Lcom/discord/utilities/time/TimeUtils;->toFriendlyStringSimple(JLjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object p1, p0, Lcom/discord/widgets/user/ViewHolderMusicRichPresence;->musicDuration:Landroid/widget/TextView;

    const-string v0, "musicDuration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/time/TimeUtils;->INSTANCE:Lcom/discord/utilities/time/TimeUtils;

    invoke-virtual {v0, v2, v3, v5}, Lcom/discord/utilities/time/TimeUtils;->toFriendlyStringSimple(JLjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method
