.class public final Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;
.super Ljava/lang/Object;
.source "WidgetSettingsBoostAdapter.kt"

# interfaces
.implements Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BoostItem"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem$Companion;

.field public static final TYPE_AVAILABLE_BOOST:I = 0x1

.field public static final TYPE_BOOSTED_SERVER:I = 0x2

.field public static final TYPE_BOOST_INFO:I


# instance fields
.field private final boostsRemaining:Ljava/lang/Integer;

.field private final cooldownEndsAt:Ljava/lang/Long;

.field private final enabled:Z

.field private final guild:Lcom/discord/models/domain/ModelGuild;

.field private final premiumGuildSubscription:Lcom/discord/models/domain/ModelPremiumGuildSubscription;

.field private final typeInt:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->Companion:Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem$Companion;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Integer;Lcom/discord/models/domain/ModelPremiumGuildSubscription;Lcom/discord/models/domain/ModelGuild;ZI)V
    .locals 0

    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->cooldownEndsAt:Ljava/lang/Long;

    iput-object p2, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->boostsRemaining:Ljava/lang/Integer;

    iput-object p3, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->premiumGuildSubscription:Lcom/discord/models/domain/ModelPremiumGuildSubscription;

    iput-object p4, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->guild:Lcom/discord/models/domain/ModelGuild;

    iput-boolean p5, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->enabled:Z

    iput p6, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->typeInt:I

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Long;Ljava/lang/Integer;Lcom/discord/models/domain/ModelPremiumGuildSubscription;Lcom/discord/models/domain/ModelGuild;ZIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 9

    and-int/lit8 v0, p7, 0x1

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object v3, v1

    goto :goto_0

    :cond_0
    move-object v3, p1

    :goto_0
    and-int/lit8 v0, p7, 0x2

    if-eqz v0, :cond_1

    move-object v4, v1

    goto :goto_1

    :cond_1
    move-object v4, p2

    :goto_1
    and-int/lit8 v0, p7, 0x4

    if-eqz v0, :cond_2

    move-object v5, v1

    goto :goto_2

    :cond_2
    move-object v5, p3

    :goto_2
    and-int/lit8 v0, p7, 0x8

    if-eqz v0, :cond_3

    move-object v6, v1

    goto :goto_3

    :cond_3
    move-object v6, p4

    :goto_3
    and-int/lit8 v0, p7, 0x10

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    const/4 v7, 0x1

    goto :goto_4

    :cond_4
    move v7, p5

    :goto_4
    move-object v2, p0

    move v8, p6

    .line 162
    invoke-direct/range {v2 .. v8}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;-><init>(Ljava/lang/Long;Ljava/lang/Integer;Lcom/discord/models/domain/ModelPremiumGuildSubscription;Lcom/discord/models/domain/ModelGuild;ZI)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;Ljava/lang/Long;Ljava/lang/Integer;Lcom/discord/models/domain/ModelPremiumGuildSubscription;Lcom/discord/models/domain/ModelGuild;ZIILjava/lang/Object;)Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->cooldownEndsAt:Ljava/lang/Long;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->boostsRemaining:Ljava/lang/Integer;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->premiumGuildSubscription:Lcom/discord/models/domain/ModelPremiumGuildSubscription;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->guild:Lcom/discord/models/domain/ModelGuild;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->enabled:Z

    :cond_4
    move v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget p6, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->typeInt:I

    :cond_5
    move v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move-object p6, v1

    move p7, v2

    move p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->copy(Ljava/lang/Long;Ljava/lang/Integer;Lcom/discord/models/domain/ModelPremiumGuildSubscription;Lcom/discord/models/domain/ModelGuild;ZI)Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->cooldownEndsAt:Ljava/lang/Long;

    return-object v0
.end method

.method public final component2()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->boostsRemaining:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component3()Lcom/discord/models/domain/ModelPremiumGuildSubscription;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->premiumGuildSubscription:Lcom/discord/models/domain/ModelPremiumGuildSubscription;

    return-object v0
.end method

.method public final component4()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->guild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->enabled:Z

    return v0
.end method

.method public final component6()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->typeInt:I

    return v0
.end method

.method public final copy(Ljava/lang/Long;Ljava/lang/Integer;Lcom/discord/models/domain/ModelPremiumGuildSubscription;Lcom/discord/models/domain/ModelGuild;ZI)Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;
    .locals 8

    new-instance v7, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;

    move-object v0, v7

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;-><init>(Ljava/lang/Long;Ljava/lang/Integer;Lcom/discord/models/domain/ModelPremiumGuildSubscription;Lcom/discord/models/domain/ModelGuild;ZI)V

    return-object v7
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-eq p0, p1, :cond_3

    instance-of v1, p1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    check-cast p1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;

    iget-object v1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->cooldownEndsAt:Ljava/lang/Long;

    iget-object v3, p1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->cooldownEndsAt:Ljava/lang/Long;

    invoke-static {v1, v3}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->boostsRemaining:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->boostsRemaining:Ljava/lang/Integer;

    invoke-static {v1, v3}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->premiumGuildSubscription:Lcom/discord/models/domain/ModelPremiumGuildSubscription;

    iget-object v3, p1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->premiumGuildSubscription:Lcom/discord/models/domain/ModelPremiumGuildSubscription;

    invoke-static {v1, v3}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->guild:Lcom/discord/models/domain/ModelGuild;

    iget-object v3, p1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-static {v1, v3}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->enabled:Z

    iget-boolean v3, p1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->enabled:Z

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    iget v1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->typeInt:I

    iget p1, p1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->typeInt:I

    if-ne v1, p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    return v2

    :cond_3
    :goto_2
    return v0
.end method

.method public final getBoostsRemaining()Ljava/lang/Integer;
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->boostsRemaining:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getCooldownEndsAt()Ljava/lang/Long;
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->cooldownEndsAt:Ljava/lang/Long;

    return-object v0
.end method

.method public final getEnabled()Z
    .locals 1

    .line 162
    iget-boolean v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->enabled:Z

    return v0
.end method

.method public final getGuild()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->guild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public final getKey()Ljava/lang/String;
    .locals 2

    .line 166
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->cooldownEndsAt:Ljava/lang/Long;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->boostsRemaining:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->premiumGuildSubscription:Lcom/discord/models/domain/ModelPremiumGuildSubscription;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelPremiumGuildSubscription;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_1
    const-string v0, "unused"

    :cond_2
    return-object v0

    :cond_3
    :goto_0
    const-string v0, "cooldown"

    return-object v0
.end method

.method public final getPremiumGuildSubscription()Lcom/discord/models/domain/ModelPremiumGuildSubscription;
    .locals 1

    .line 160
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->premiumGuildSubscription:Lcom/discord/models/domain/ModelPremiumGuildSubscription;

    return-object v0
.end method

.method public final getType()I
    .locals 1

    .line 170
    iget v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->typeInt:I

    return v0
.end method

.method public final getTypeInt()I
    .locals 1

    .line 163
    iget v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->typeInt:I

    return v0
.end method

.method public final hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->cooldownEndsAt:Ljava/lang/Long;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->boostsRemaining:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->premiumGuildSubscription:Lcom/discord/models/domain/ModelPremiumGuildSubscription;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->guild:Lcom/discord/models/domain/ModelGuild;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->enabled:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->typeInt:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BoostItem(cooldownEndsAt="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->cooldownEndsAt:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", boostsRemaining="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->boostsRemaining:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", premiumGuildSubscription="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->premiumGuildSubscription:Lcom/discord/models/domain/ModelPremiumGuildSubscription;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", guild="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->enabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", typeInt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->typeInt:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
