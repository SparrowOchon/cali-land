.class public final Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Loaded;
.super Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model;
.source "WidgetSettingsBoost.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Loaded"
.end annotation


# instance fields
.field private final boostItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;",
            ">;)V"
        }
    .end annotation

    const-string v0, "boostItems"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 140
    invoke-direct {p0, v0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Loaded;->boostItems:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final getBoostItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;",
            ">;"
        }
    .end annotation

    .line 140
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Loaded;->boostItems:Ljava/util/List;

    return-object v0
.end method
