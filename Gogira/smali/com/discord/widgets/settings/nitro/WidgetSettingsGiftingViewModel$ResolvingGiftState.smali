.class public abstract Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingViewModel$ResolvingGiftState;
.super Ljava/lang/Object;
.source "WidgetSettingsGiftingViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ResolvingGiftState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingViewModel$ResolvingGiftState$NotResolving;,
        Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingViewModel$ResolvingGiftState$Error;,
        Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingViewModel$ResolvingGiftState$Resolving;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingViewModel$ResolvingGiftState;-><init>()V

    return-void
.end method
