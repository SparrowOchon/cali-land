.class public final Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;
.super Landroidx/lifecycle/ViewModel;
.source "SettingsPremiumViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;,
        Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$SubscriptionError;,
        Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;
    }
.end annotation


# instance fields
.field private launchPremiumTabStartTimeMs:J

.field private final requestBuyPlanSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;",
            ">;"
        }
    .end annotation
.end field

.field private subscription:Lrx/Subscription;

.field private viewState:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

.field private final viewStateSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 11

    .line 24
    invoke-direct {p0}, Landroidx/lifecycle/ViewModel;-><init>()V

    .line 50
    sget-object v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loading;->INSTANCE:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loading;

    check-cast v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->viewState:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    .line 51
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->viewState:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    invoke-static {v0}, Lrx/subjects/BehaviorSubject;->bT(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->viewStateSubject:Lrx/subjects/BehaviorSubject;

    .line 53
    invoke-static {}, Lrx/subjects/PublishSubject;->Lt()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->requestBuyPlanSubject:Lrx/subjects/PublishSubject;

    .line 58
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->fetchData()V

    .line 61
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x32

    .line 60
    invoke-static {v1, v2, v0}, Lrx/Observable;->g(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v0

    .line 62
    new-instance v1, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$1;-><init>(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;)V

    check-cast v1, Lrx/functions/b;

    invoke-virtual {v0, v1}, Lrx/Observable;->g(Lrx/functions/b;)Lrx/Observable;

    move-result-object v2

    const-string v0, "Observable.timer(\n      \u2026, subscriptions) }\n\n    }"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    const-class v3, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;

    .line 75
    new-instance v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$2;

    move-object v1, p0

    check-cast v1, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;

    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$2;-><init>(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;)V

    move-object v8, v0

    check-cast v8, Lkotlin/jvm/functions/Function1;

    .line 76
    new-instance v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$3;

    invoke-direct {v0, p0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$3;-><init>(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;)V

    move-object v5, v0

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x1a

    const/4 v10, 0x0

    .line 73
    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$buildViewState(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->buildViewState(Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$fetchData(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->fetchData()V

    return-void
.end method

.method public static final synthetic access$getSubscription$p(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;)Lrx/Subscription;
    .locals 1

    .line 24
    iget-object p0, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->subscription:Lrx/Subscription;

    if-nez p0, :cond_0

    const-string v0, "subscription"

    invoke-static {v0}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$onCancelError(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->onCancelError()V

    return-void
.end method

.method public static final synthetic access$onChangePaymentSourceError(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->onChangePaymentSourceError()V

    return-void
.end method

.method public static final synthetic access$onResubscribeError(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->onResubscribeError()V

    return-void
.end method

.method public static final synthetic access$onUpgradeError(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->onUpgradeError()V

    return-void
.end method

.method public static final synthetic access$publish(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->publish(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$setSubscription$p(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;Lrx/Subscription;)V
    .locals 0

    .line 24
    iput-object p1, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->subscription:Lrx/Subscription;

    return-void
.end method

.method private final buildViewState(Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;
    .locals 7

    .line 224
    instance-of v0, p1, Lcom/discord/stores/StorePaymentSources$PaymentSourcesState$Loaded;

    if-eqz v0, :cond_3

    .line 225
    instance-of v0, p2, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;

    if-eqz v0, :cond_3

    .line 226
    check-cast p2, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;

    invoke-virtual {p2}, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;->getSubscriptions()Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/lang/Iterable;

    .line 271
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/discord/models/domain/ModelSubscription;

    .line 227
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getType()Lcom/discord/models/domain/ModelSubscription$Type;

    move-result-object v1

    sget-object v2, Lcom/discord/models/domain/ModelSubscription$Type;->PREMIUM:Lcom/discord/models/domain/ModelSubscription$Type;

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    .line 226
    :goto_1
    move-object v2, v0

    check-cast v2, Lcom/discord/models/domain/ModelSubscription;

    .line 230
    new-instance p2, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    .line 232
    check-cast p1, Lcom/discord/stores/StorePaymentSources$PaymentSourcesState$Loaded;

    invoke-virtual {p1}, Lcom/discord/stores/StorePaymentSources$PaymentSourcesState$Loaded;->getPaymentSources()Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p2

    .line 230
    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;-><init>(Lcom/discord/models/domain/ModelSubscription;Ljava/util/List;ZLcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$SubscriptionError;Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;)V

    check-cast p2, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    return-object p2

    .line 238
    :cond_3
    instance-of p1, p1, Lcom/discord/stores/StorePaymentSources$PaymentSourcesState$Failure;

    if-nez p1, :cond_5

    .line 239
    instance-of p1, p2, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Failure;

    if-eqz p1, :cond_4

    goto :goto_2

    .line 243
    :cond_4
    sget-object p1, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loading;->INSTANCE:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loading;

    check-cast p1, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    return-object p1

    .line 240
    :cond_5
    :goto_2
    sget-object p1, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Failure;->INSTANCE:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Failure;

    check-cast p1, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    return-object p1
.end method

.method private final fetchData()V
    .locals 0

    .line 259
    invoke-static {}, Lcom/discord/stores/StorePaymentSources$Actions;->fetchPaymentSources()V

    .line 263
    invoke-static {}, Lcom/discord/stores/StoreSubscriptions$Actions;->fetchSubscriptions()V

    return-void
.end method

.method private final markBusy()V
    .locals 9

    .line 247
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->viewState:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    instance-of v1, v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    if-nez v1, :cond_1

    return-void

    :cond_1
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x13

    const/4 v8, 0x0

    .line 248
    invoke-static/range {v1 .. v8}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->copy$default(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;Lcom/discord/models/domain/ModelSubscription;Ljava/util/List;ZLcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$SubscriptionError;Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;ILjava/lang/Object;)Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    invoke-direct {p0, v0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->publish(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;)V

    return-void
.end method

.method private final onCancelError()V
    .locals 9

    .line 197
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->viewState:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    instance-of v1, v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    if-nez v1, :cond_1

    return-void

    .line 198
    :cond_1
    invoke-virtual {v1}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->getDialog()Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;

    move-result-object v0

    instance-of v0, v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog$CancelPremium;

    if-nez v0, :cond_2

    return-void

    :cond_2
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 199
    new-instance v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog$CancelPremium;

    const/4 v6, 0x1

    invoke-direct {v0, v6}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog$CancelPremium;-><init>(Z)V

    move-object v6, v0

    check-cast v6, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;

    const/16 v7, 0xb

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->copy$default(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;Lcom/discord/models/domain/ModelSubscription;Ljava/util/List;ZLcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$SubscriptionError;Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;ILjava/lang/Object;)Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    invoke-direct {p0, v0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->publish(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;)V

    return-void
.end method

.method private final onChangePaymentSourceError()V
    .locals 9

    .line 216
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->viewState:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    instance-of v1, v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    if-nez v1, :cond_1

    return-void

    :cond_1
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 217
    sget-object v5, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$SubscriptionError;->GENERIC:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$SubscriptionError;

    const/4 v6, 0x0

    const/16 v7, 0x13

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->copy$default(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;Lcom/discord/models/domain/ModelSubscription;Ljava/util/List;ZLcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$SubscriptionError;Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;ILjava/lang/Object;)Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    invoke-direct {p0, v0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->publish(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;)V

    return-void
.end method

.method private final onDowngradeClicked()V
    .locals 9

    .line 187
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->viewState:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    instance-of v1, v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    if-nez v1, :cond_1

    return-void

    :cond_1
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 188
    sget-object v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog$DowngradePremium;->INSTANCE:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog$DowngradePremium;

    move-object v6, v0

    check-cast v6, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;

    const/16 v7, 0xf

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->copy$default(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;Lcom/discord/models/domain/ModelSubscription;Ljava/util/List;ZLcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$SubscriptionError;Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;ILjava/lang/Object;)Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    invoke-direct {p0, v0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->publish(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;)V

    return-void
.end method

.method private final onResubscribeError()V
    .locals 9

    .line 211
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->viewState:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    instance-of v1, v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    if-nez v1, :cond_1

    return-void

    :cond_1
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 212
    sget-object v5, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$SubscriptionError;->GENERIC:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$SubscriptionError;

    const/4 v6, 0x0

    const/16 v7, 0x13

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->copy$default(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;Lcom/discord/models/domain/ModelSubscription;Ljava/util/List;ZLcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$SubscriptionError;Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;ILjava/lang/Object;)Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    invoke-direct {p0, v0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->publish(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;)V

    return-void
.end method

.method private final onUpgradeClicked(Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;)V
    .locals 11

    .line 192
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->viewState:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    instance-of v1, v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    move-object v3, v0

    check-cast v3, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    if-nez v3, :cond_1

    return-void

    :cond_1
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 193
    new-instance v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog$UpgradePremium;

    const/4 v1, 0x0

    const/4 v8, 0x2

    invoke-direct {v0, p1, v1, v8, v2}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog$UpgradePremium;-><init>(Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object v8, v0

    check-cast v8, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;

    const/16 v9, 0xf

    const/4 v10, 0x0

    invoke-static/range {v3 .. v10}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->copy$default(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;Lcom/discord/models/domain/ModelSubscription;Ljava/util/List;ZLcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$SubscriptionError;Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;ILjava/lang/Object;)Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->publish(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;)V

    return-void
.end method

.method private final onUpgradeError()V
    .locals 11

    .line 203
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->viewState:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    instance-of v1, v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    move-object v3, v0

    check-cast v3, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    if-nez v3, :cond_1

    return-void

    .line 204
    :cond_1
    invoke-virtual {v3}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->getDialog()Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;

    move-result-object v0

    instance-of v0, v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog$UpgradePremium;

    if-nez v0, :cond_2

    return-void

    .line 205
    :cond_2
    invoke-virtual {v3}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->getDialog()Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog$UpgradePremium;

    const/4 v1, 0x1

    invoke-static {v0, v2, v1, v1, v2}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog$UpgradePremium;->copy$default(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog$UpgradePremium;Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;ZILjava/lang/Object;)Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog$UpgradePremium;

    move-result-object v0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 207
    move-object v8, v0

    check-cast v8, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;

    const/16 v9, 0xb

    const/4 v10, 0x0

    invoke-static/range {v3 .. v10}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->copy$default(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;Lcom/discord/models/domain/ModelSubscription;Ljava/util/List;ZLcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$SubscriptionError;Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;ILjava/lang/Object;)Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    invoke-direct {p0, v0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->publish(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;)V

    return-void
.end method

.method private final publish(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;)V
    .locals 1

    .line 252
    iput-object p1, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->viewState:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    .line 253
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->viewStateSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final cancelSubscription()V
    .locals 4

    .line 110
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->viewState:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    instance-of v1, v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    if-nez v0, :cond_1

    return-void

    .line 111
    :cond_1
    invoke-virtual {v0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->getPremiumSubscription()Lcom/discord/models/domain/ModelSubscription;

    move-result-object v0

    if-nez v0, :cond_2

    return-void

    .line 113
    :cond_2
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->markBusy()V

    .line 115
    sget-object v1, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v1}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v1

    .line 117
    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscription;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/discord/utilities/rest/RestAPI;->deleteSubscription(Ljava/lang/String;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 118
    invoke-static {v0, v1, v3, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    .line 119
    invoke-static {}, Lrx/android/b/a;->JY()Lrx/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->a(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    .line 121
    new-instance v1, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$cancelSubscription$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$cancelSubscription$1;-><init>(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;)V

    check-cast v1, Lrx/functions/Action1;

    .line 122
    new-instance v2, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$cancelSubscription$2;

    invoke-direct {v2, p0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$cancelSubscription$2;-><init>(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;)V

    check-cast v2, Lrx/functions/Action1;

    .line 120
    invoke-virtual {v0, v1, v2}, Lrx/Observable;->a(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    return-void
.end method

.method public final changePaymentSource(Lcom/discord/models/domain/ModelPaymentSource;)V
    .locals 10

    const-string v0, "paymentSource"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->viewState:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    instance-of v1, v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    if-nez v0, :cond_1

    return-void

    .line 170
    :cond_1
    invoke-virtual {v0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->getPremiumSubscription()Lcom/discord/models/domain/ModelSubscription;

    move-result-object v0

    if-nez v0, :cond_2

    return-void

    .line 172
    :cond_2
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->markBusy()V

    .line 174
    sget-object v1, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v1}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v1

    .line 176
    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscription;->getId()Ljava/lang/String;

    move-result-object v0

    .line 177
    new-instance v9, Lcom/discord/restapi/RestAPIParams$UpdateSubscription;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPaymentSource;->getId()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x3

    const/4 v8, 0x0

    move-object v3, v9

    invoke-direct/range {v3 .. v8}, Lcom/discord/restapi/RestAPIParams$UpdateSubscription;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 176
    invoke-virtual {v1, v0, v9}, Lcom/discord/utilities/rest/RestAPI;->updateSubscription(Ljava/lang/String;Lcom/discord/restapi/RestAPIParams$UpdateSubscription;)Lrx/Observable;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 178
    invoke-static {p1, v0, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    .line 179
    invoke-static {}, Lrx/android/b/a;->JY()Lrx/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->a(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    .line 181
    new-instance v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$changePaymentSource$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$changePaymentSource$1;-><init>(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;)V

    check-cast v0, Lrx/functions/Action1;

    .line 182
    new-instance v1, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$changePaymentSource$2;

    invoke-direct {v1, p0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$changePaymentSource$2;-><init>(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;)V

    check-cast v1, Lrx/functions/Action1;

    .line 180
    invoke-virtual {p1, v0, v1}, Lrx/Observable;->a(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    return-void
.end method

.method public final getLaunchPremiumTabStartTimeMs()J
    .locals 2

    .line 26
    iget-wide v0, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->launchPremiumTabStartTimeMs:J

    return-wide v0
.end method

.method public final getRequestBuyPlanSubject()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;",
            ">;"
        }
    .end annotation

    .line 81
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->requestBuyPlanSubject:Lrx/subjects/PublishSubject;

    const-string v1, "requestBuyPlanSubject"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lrx/Observable;

    return-object v0
.end method

.method public final getViewState()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;",
            ">;"
        }
    .end annotation

    .line 79
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->viewStateSubject:Lrx/subjects/BehaviorSubject;

    const-string v1, "viewStateSubject"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lrx/Observable;

    return-object v0
.end method

.method public final onCancelClicked()V
    .locals 11

    .line 100
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->viewState:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    instance-of v1, v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    move-object v3, v0

    check-cast v3, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    if-nez v3, :cond_1

    return-void

    :cond_1
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 101
    new-instance v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog$CancelPremium;

    const/4 v1, 0x0

    const/4 v8, 0x1

    invoke-direct {v0, v1, v8, v2}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog$CancelPremium;-><init>(ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object v8, v0

    check-cast v8, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;

    const/16 v9, 0xf

    const/4 v10, 0x0

    invoke-static/range {v3 .. v10}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->copy$default(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;Lcom/discord/models/domain/ModelSubscription;Ljava/util/List;ZLcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$SubscriptionError;Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;ILjava/lang/Object;)Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    invoke-direct {p0, v0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->publish(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;)V

    return-void
.end method

.method public final onCleared()V
    .locals 2

    .line 267
    invoke-super {p0}, Landroidx/lifecycle/ViewModel;->onCleared()V

    .line 268
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->subscription:Lrx/Subscription;

    if-nez v0, :cond_0

    const-string v1, "subscription"

    invoke-static {v1}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    return-void
.end method

.method public final onDialogDismissClicked()V
    .locals 9

    .line 105
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->viewState:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    instance-of v1, v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    if-nez v1, :cond_1

    return-void

    :cond_1
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0xf

    const/4 v8, 0x0

    .line 106
    invoke-static/range {v1 .. v8}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->copy$default(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;Lcom/discord/models/domain/ModelSubscription;Ljava/util/List;ZLcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$SubscriptionError;Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;ILjava/lang/Object;)Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    invoke-direct {p0, v0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->publish(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;)V

    return-void
.end method

.method public final onPlanRequested(Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;)V
    .locals 3

    const-string v0, "requestedPlanType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->viewState:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    instance-of v1, v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    if-nez v0, :cond_1

    return-void

    .line 85
    :cond_1
    invoke-virtual {v0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->getPremiumSubscription()Lcom/discord/models/domain/ModelSubscription;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object v2

    :cond_2
    if-nez v2, :cond_3

    .line 88
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->requestBuyPlanSubject:Lrx/subjects/PublishSubject;

    invoke-virtual {v0, p1}, Lrx/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    return-void

    :cond_3
    if-eq p1, v2, :cond_5

    .line 90
    invoke-static {}, Lcom/discord/utilities/billing/PremiumUtilsKt;->getUPGRADE_ELIGIBILITY()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    if-eqz v0, :cond_4

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->onUpgradeClicked(Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;)V

    return-void

    .line 91
    :cond_4
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->onDowngradeClicked()V

    :cond_5
    return-void
.end method

.method public final onRetryClicked()V
    .locals 0

    .line 96
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->fetchData()V

    return-void
.end method

.method public final resubscribe()V
    .locals 10

    .line 127
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->viewState:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    instance-of v1, v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    if-nez v0, :cond_1

    return-void

    .line 128
    :cond_1
    invoke-virtual {v0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->getPremiumSubscription()Lcom/discord/models/domain/ModelSubscription;

    move-result-object v0

    if-nez v0, :cond_2

    return-void

    .line 129
    :cond_2
    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscription;->getStatus()Lcom/discord/models/domain/ModelSubscription$Status;

    move-result-object v1

    sget-object v3, Lcom/discord/models/domain/ModelSubscription$Status;->CANCELED:Lcom/discord/models/domain/ModelSubscription$Status;

    if-eq v1, v3, :cond_3

    return-void

    .line 131
    :cond_3
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->markBusy()V

    .line 133
    sget-object v1, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v1}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v1

    .line 135
    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscription;->getId()Ljava/lang/String;

    move-result-object v0

    .line 136
    new-instance v9, Lcom/discord/restapi/RestAPIParams$UpdateSubscription;

    .line 137
    sget-object v3, Lcom/discord/models/domain/ModelSubscription$Status;->ACTIVE:Lcom/discord/models/domain/ModelSubscription$Status;

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelSubscription$Status;->getIntRepresentation()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x6

    const/4 v8, 0x0

    move-object v3, v9

    .line 136
    invoke-direct/range {v3 .. v8}, Lcom/discord/restapi/RestAPIParams$UpdateSubscription;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 135
    invoke-virtual {v1, v0, v9}, Lcom/discord/utilities/rest/RestAPI;->updateSubscription(Ljava/lang/String;Lcom/discord/restapi/RestAPIParams$UpdateSubscription;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 139
    invoke-static {v0, v1, v3, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    .line 140
    invoke-static {}, Lrx/android/b/a;->JY()Lrx/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->a(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    .line 142
    new-instance v1, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$resubscribe$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$resubscribe$1;-><init>(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;)V

    check-cast v1, Lrx/functions/Action1;

    .line 143
    new-instance v2, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$resubscribe$2;

    invoke-direct {v2, p0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$resubscribe$2;-><init>(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;)V

    check-cast v2, Lrx/functions/Action1;

    .line 141
    invoke-virtual {v0, v1, v2}, Lrx/Observable;->a(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    return-void
.end method

.method public final setLaunchPremiumTabStartTimeMs(J)V
    .locals 0

    .line 26
    iput-wide p1, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->launchPremiumTabStartTimeMs:J

    return-void
.end method

.method public final upgradeSubscription(Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;)V
    .locals 10

    const-string v0, "targetPlanType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->viewState:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState;

    instance-of v1, v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;

    if-nez v0, :cond_1

    return-void

    .line 149
    :cond_1
    invoke-virtual {v0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->getPremiumSubscription()Lcom/discord/models/domain/ModelSubscription;

    move-result-object v0

    if-nez v0, :cond_2

    return-void

    .line 151
    :cond_2
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->markBusy()V

    .line 153
    sget-object v1, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v1}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v1

    .line 155
    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscription;->getId()Ljava/lang/String;

    move-result-object v0

    .line 156
    new-instance v9, Lcom/discord/restapi/RestAPIParams$UpdateSubscription;

    .line 157
    sget-object v3, Lcom/discord/models/domain/ModelSubscription$Status;->ACTIVE:Lcom/discord/models/domain/ModelSubscription$Status;

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelSubscription$Status;->getIntRepresentation()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 158
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getPlanTypeString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v3, v9

    .line 156
    invoke-direct/range {v3 .. v8}, Lcom/discord/restapi/RestAPIParams$UpdateSubscription;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 155
    invoke-virtual {v1, v0, v9}, Lcom/discord/utilities/rest/RestAPI;->updateSubscription(Ljava/lang/String;Lcom/discord/restapi/RestAPIParams$UpdateSubscription;)Lrx/Observable;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 160
    invoke-static {p1, v0, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    .line 161
    invoke-static {}, Lrx/android/b/a;->JY()Lrx/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->a(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    .line 163
    new-instance v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$upgradeSubscription$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$upgradeSubscription$1;-><init>(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;)V

    check-cast v0, Lrx/functions/Action1;

    .line 164
    new-instance v1, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$upgradeSubscription$2;

    invoke-direct {v1, p0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$upgradeSubscription$2;-><init>(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;)V

    check-cast v1, Lrx/functions/Action1;

    .line 162
    invoke-virtual {p1, v0, v1}, Lrx/Observable;->a(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    return-void
.end method
