.class public final Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$LoadedNoNitro;
.super Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model;
.source "WidgetSettingsBoost.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LoadedNoNitro"
.end annotation


# instance fields
.field private final isNitroClassic:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 1

    const/4 v0, 0x0

    .line 141
    invoke-direct {p0, v0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$LoadedNoNitro;->isNitroClassic:Z

    return-void
.end method


# virtual methods
.method public final isNitroClassic()Z
    .locals 1

    .line 141
    iget-boolean v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$LoadedNoNitro;->isNitroClassic:Z

    return v0
.end method
