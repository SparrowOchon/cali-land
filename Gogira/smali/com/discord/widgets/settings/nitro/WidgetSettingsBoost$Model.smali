.class abstract Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model;
.super Ljava/lang/Object;
.source "WidgetSettingsBoost.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "Model"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Loading;,
        Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Failure;,
        Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Loaded;,
        Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$LoadedNoNitro;,
        Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model;->Companion:Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 137
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model;-><init>()V

    return-void
.end method
