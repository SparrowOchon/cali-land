.class public final Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;
.super Lcom/discord/app/AppFragment;
.source "WidgetSettingsPremium.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$ButtonConfig;,
        Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$Companion;
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field private static final ALL_DIALOG_TAGS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$Companion;

.field private static final DIALOG_TAG_CANCEL_PREMIUM:Ljava/lang/String; = "CancelPremium"

.field private static final DIALOG_TAG_DOWNGRADE_PREMIUM:Ljava/lang/String; = "DowngradePremium"

.field private static final DIALOG_TAG_UPGRADE_PREMIUM:Ljava/lang/String; = "UpgradePremium"

.field private static final INTENT_SECTION:Ljava/lang/String; = "intent_section"

.field public static final SECTION_NITRO:I = 0x1

.field public static final SECTION_NITRO_CLASSIC:I = 0x0

.field private static final VIEW_INDEX_CONTENT:I = 0x0

.field private static final VIEW_INDEX_ERROR:I = 0x2

.field private static final VIEW_INDEX_LOADING:I = 0x1


# instance fields
.field private final activeSubscriptionView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final buyTier1Container$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final buyTier1Gift$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final buyTier1Monthly$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final buyTier1Yearly$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final buyTier2Container$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final buyTier2Gift$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final buyTier2Monthly$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final buyTier2Yearly$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private connection:Landroid/content/ServiceConnection;

.field private final grandfathered$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final legalese$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final retryButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final scrollView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final uploadPerks$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final viewFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const-class v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;

    const/16 v1, 0xf

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "uploadPerks"

    const-string v5, "getUploadPerks()Landroid/widget/TextView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v4

    const-string v5, "buyTier2Yearly"

    const-string v6, "getBuyTier2Yearly()Lcom/discord/views/BoxedButton;"

    invoke-direct {v2, v4, v5, v6}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v5

    const-string v6, "buyTier2Monthly"

    const-string v7, "getBuyTier2Monthly()Landroid/widget/Button;"

    invoke-direct {v2, v5, v6, v7}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v5, 0x2

    aput-object v2, v1, v5

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v6

    const-string v7, "buyTier2Gift"

    const-string v8, "getBuyTier2Gift()Landroid/view/View;"

    invoke-direct {v2, v6, v7, v8}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v6, 0x3

    aput-object v2, v1, v6

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v7

    const-string v8, "buyTier1Yearly"

    const-string v9, "getBuyTier1Yearly()Lcom/discord/views/BoxedButton;"

    invoke-direct {v2, v7, v8, v9}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v7, 0x4

    aput-object v2, v1, v7

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v7

    const-string v8, "buyTier1Monthly"

    const-string v9, "getBuyTier1Monthly()Landroid/widget/Button;"

    invoke-direct {v2, v7, v8, v9}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v7, 0x5

    aput-object v2, v1, v7

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v7

    const-string v8, "buyTier1Gift"

    const-string v9, "getBuyTier1Gift()Landroid/view/View;"

    invoke-direct {v2, v7, v8, v9}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v7, 0x6

    aput-object v2, v1, v7

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v7

    const-string v8, "buyTier1Container"

    const-string v9, "getBuyTier1Container()Landroid/view/View;"

    invoke-direct {v2, v7, v8, v9}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v7, 0x7

    aput-object v2, v1, v7

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v7

    const-string v8, "buyTier2Container"

    const-string v9, "getBuyTier2Container()Landroid/view/View;"

    invoke-direct {v2, v7, v8, v9}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v7, 0x8

    aput-object v2, v1, v7

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v7

    const-string v8, "viewFlipper"

    const-string v9, "getViewFlipper()Landroid/widget/ViewFlipper;"

    invoke-direct {v2, v7, v8, v9}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v7, 0x9

    aput-object v2, v1, v7

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v7

    const-string v8, "activeSubscriptionView"

    const-string v9, "getActiveSubscriptionView()Lcom/discord/views/ActiveSubscriptionView;"

    invoke-direct {v2, v7, v8, v9}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v7, 0xa

    aput-object v2, v1, v7

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v7

    const-string v8, "retryButton"

    const-string v9, "getRetryButton()Landroid/widget/Button;"

    invoke-direct {v2, v7, v8, v9}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v7, 0xb

    aput-object v2, v1, v7

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v7

    const-string v8, "legalese"

    const-string v9, "getLegalese()Landroid/widget/TextView;"

    invoke-direct {v2, v7, v8, v9}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v7, 0xc

    aput-object v2, v1, v7

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v7

    const-string v8, "grandfathered"

    const-string v9, "getGrandfathered()Landroid/widget/TextView;"

    invoke-direct {v2, v7, v8, v9}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/16 v7, 0xd

    aput-object v2, v1, v7

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v0

    const-string v7, "scrollView"

    const-string v8, "getScrollView()Landroid/widget/ScrollView;"

    invoke-direct {v2, v0, v7, v8}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/KProperty;

    const/16 v2, 0xe

    aput-object v0, v1, v2

    sput-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->Companion:Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$Companion;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "CancelPremium"

    aput-object v1, v0, v3

    const-string v1, "DowngradePremium"

    aput-object v1, v0, v4

    const-string v1, "UpgradePremium"

    aput-object v1, v0, v5

    .line 600
    invoke-static {v0}, Lkotlin/a/ak;->o([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->ALL_DIALOG_TAGS:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 45
    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a0514

    .line 47
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->uploadPerks$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0521

    .line 49
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->buyTier2Yearly$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0520

    .line 50
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->buyTier2Monthly$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a051f

    .line 51
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->buyTier2Gift$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a051e

    .line 52
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->buyTier1Yearly$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a051d

    .line 53
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->buyTier1Monthly$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a051c

    .line 54
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->buyTier1Gift$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0524

    .line 56
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->buyTier1Container$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0525

    .line 57
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->buyTier2Container$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0526

    .line 59
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->viewFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0515

    .line 60
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->activeSubscriptionView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0522

    .line 61
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->retryButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0517

    .line 62
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->legalese$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0516

    .line 64
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->grandfathered$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0523

    .line 65
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->scrollView$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;)Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;
    .locals 1

    .line 45
    iget-object p0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->viewModel:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;

    if-nez p0, :cond_0

    const-string v0, "viewModel"

    invoke-static {v0}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$launchAddPaymentMethodFlow(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;)V
    .locals 0

    .line 45
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->launchAddPaymentMethodFlow()V

    return-void
.end method

.method public static final synthetic access$onDropdownItemSelected(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;Lcom/discord/views/ActiveSubscriptionView$DropdownItem;)V
    .locals 0

    .line 45
    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->onDropdownItemSelected(Lcom/discord/views/ActiveSubscriptionView$DropdownItem;)V

    return-void
.end method

.method public static final synthetic access$scrollToSection(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;Ljava/lang/Integer;)V
    .locals 0

    .line 45
    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->scrollToSection(Ljava/lang/Integer;)V

    return-void
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;)V
    .locals 0

    .line 45
    iput-object p1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->viewModel:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;

    return-void
.end method

.method public static final synthetic access$showContent(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;)V
    .locals 0

    .line 45
    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->showContent(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;)V

    return-void
.end method

.method public static final synthetic access$showFailureUI(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;)V
    .locals 0

    .line 45
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->showFailureUI()V

    return-void
.end method

.method public static final synthetic access$showLoadingUI(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;)V
    .locals 0

    .line 45
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->showLoadingUI()V

    return-void
.end method

.method private final configureActiveSubscriptionView(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;)V
    .locals 17

    move-object/from16 v0, p0

    .line 304
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->getPremiumSubscription()Lcom/discord/models/domain/ModelSubscription;

    move-result-object v1

    .line 305
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->getPaymentSources()Ljava/util/List;

    move-result-object v2

    if-nez v1, :cond_0

    .line 308
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getActiveSubscriptionView()Lcom/discord/views/ActiveSubscriptionView;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/discord/views/ActiveSubscriptionView;->setVisibility(I)V

    return-void

    .line 312
    :cond_0
    invoke-direct {v0, v1, v2}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getDropdownItems(Lcom/discord/models/domain/ModelSubscription;Ljava/util/List;)Ljava/util/List;

    move-result-object v15

    .line 314
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->isAppleSubscription()Z

    move-result v3

    const/4 v4, 0x0

    if-nez v3, :cond_6

    .line 315
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getStatus()Lcom/discord/models/domain/ModelSubscription$Status;

    move-result-object v3

    sget-object v5, Lcom/discord/models/domain/ModelSubscription$Status;->ACTIVE:Lcom/discord/models/domain/ModelSubscription$Status;

    if-ne v3, v5, :cond_1

    new-instance v2, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$ButtonConfig;

    const v3, 0x7f12036a

    .line 316
    invoke-virtual {v0, v3}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    .line 317
    new-instance v5, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$configureActiveSubscriptionView$topButtonConfig$1;

    invoke-direct {v5, v0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$configureActiveSubscriptionView$topButtonConfig$1;-><init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;)V

    check-cast v5, Lkotlin/jvm/functions/Function0;

    .line 315
    invoke-direct {v2, v3, v5}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$ButtonConfig;-><init>(Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function0;)V

    goto :goto_1

    .line 319
    :cond_1
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getStatus()Lcom/discord/models/domain/ModelSubscription$Status;

    move-result-object v3

    sget-object v5, Lcom/discord/models/domain/ModelSubscription$Status;->CANCELED:Lcom/discord/models/domain/ModelSubscription$Status;

    if-ne v3, v5, :cond_6

    .line 320
    check-cast v2, Ljava/lang/Iterable;

    .line 630
    instance-of v3, v2, Ljava/util/Collection;

    if-eqz v3, :cond_2

    move-object v3, v2

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    .line 631
    :cond_2
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/models/domain/ModelPaymentSource;

    .line 320
    invoke-virtual {v3}, Lcom/discord/models/domain/ModelPaymentSource;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getPaymentSourceId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v2, 0x1

    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_5

    .line 321
    new-instance v2, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$ButtonConfig;

    const v3, 0x7f120efb

    .line 322
    invoke-virtual {v0, v3}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    .line 323
    new-instance v5, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$configureActiveSubscriptionView$topButtonConfig$3;

    invoke-direct {v5, v0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$configureActiveSubscriptionView$topButtonConfig$3;-><init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;)V

    check-cast v5, Lkotlin/jvm/functions/Function0;

    .line 321
    invoke-direct {v2, v3, v5}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$ButtonConfig;-><init>(Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function0;)V

    goto :goto_1

    .line 326
    :cond_5
    sget-object v2, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$ButtonConfig;->Companion:Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$ButtonConfig$Companion;

    invoke-virtual {v2}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$ButtonConfig$Companion;->getNONE()Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$ButtonConfig;

    move-result-object v2

    goto :goto_1

    .line 329
    :cond_6
    sget-object v2, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$ButtonConfig;->Companion:Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$ButtonConfig$Companion;

    invoke-virtual {v2}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$ButtonConfig$Companion;->getNONE()Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$ButtonConfig;

    move-result-object v2

    .line 333
    :goto_1
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->isAppleSubscription()Z

    move-result v3

    if-eqz v3, :cond_7

    new-instance v3, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$ButtonConfig;

    const v5, 0x7f1202db

    .line 334
    invoke-virtual {v0, v5}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getString(I)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    .line 335
    new-instance v6, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$configureActiveSubscriptionView$bottomButtonConfig$1;

    invoke-direct {v6, v0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$configureActiveSubscriptionView$bottomButtonConfig$1;-><init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;)V

    check-cast v6, Lkotlin/jvm/functions/Function0;

    .line 333
    invoke-direct {v3, v5, v6}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$ButtonConfig;-><init>(Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function0;)V

    goto :goto_2

    .line 337
    :cond_7
    invoke-interface {v15}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_8

    new-instance v3, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$ButtonConfig;

    const v5, 0x7f1202aa

    .line 338
    invoke-virtual {v0, v5}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getString(I)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    .line 339
    new-instance v6, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$configureActiveSubscriptionView$bottomButtonConfig$2;

    invoke-direct {v6, v0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$configureActiveSubscriptionView$bottomButtonConfig$2;-><init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;)V

    check-cast v6, Lkotlin/jvm/functions/Function0;

    .line 337
    invoke-direct {v3, v5, v6}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$ButtonConfig;-><init>(Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function0;)V

    goto :goto_2

    .line 341
    :cond_8
    sget-object v3, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$ButtonConfig;->Companion:Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$ButtonConfig$Companion;

    invoke-virtual {v3}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$ButtonConfig$Companion;->getNONE()Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$ButtonConfig;

    move-result-object v3

    .line 344
    :goto_2
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getActiveSubscriptionView()Lcom/discord/views/ActiveSubscriptionView;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/discord/views/ActiveSubscriptionView;->setVisibility(I)V

    .line 345
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getActiveSubscriptionView()Lcom/discord/views/ActiveSubscriptionView;

    move-result-object v4

    .line 346
    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getHeaderBackground(Lcom/discord/models/domain/ModelSubscription;)I

    move-result v5

    .line 347
    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getHeaderImage(Lcom/discord/models/domain/ModelSubscription;)Lcom/discord/views/ActiveSubscriptionView$HeaderImage;

    move-result-object v6

    .line 348
    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getHeaderLogo(Lcom/discord/models/domain/ModelSubscription;)I

    move-result v7

    .line 349
    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getHeaderText(Lcom/discord/models/domain/ModelSubscription;)Ljava/lang/CharSequence;

    move-result-object v8

    .line 350
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->isAppleSubscription()Z

    move-result v1

    .line 351
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->getSubscriptionError()Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$SubscriptionError;

    move-result-object v9

    invoke-direct {v0, v9}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getSubscriptionErrorText(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$SubscriptionError;)Ljava/lang/CharSequence;

    move-result-object v9

    .line 352
    invoke-virtual {v2}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$ButtonConfig;->getText()Ljava/lang/CharSequence;

    move-result-object v10

    .line 353
    invoke-virtual {v2}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$ButtonConfig;->getOnClick()Lkotlin/jvm/functions/Function0;

    move-result-object v11

    .line 354
    invoke-virtual {v3}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$ButtonConfig;->getText()Ljava/lang/CharSequence;

    move-result-object v13

    .line 355
    invoke-virtual {v3}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$ButtonConfig;->getOnClick()Lkotlin/jvm/functions/Function0;

    move-result-object v14

    .line 356
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->isBusy()Z

    move-result v12

    .line 358
    new-instance v2, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$configureActiveSubscriptionView$1;

    move-object v3, v0

    check-cast v3, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;

    invoke-direct {v2, v3}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$configureActiveSubscriptionView$1;-><init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;)V

    move-object/from16 v16, v2

    check-cast v16, Lkotlin/jvm/functions/Function1;

    move-object v3, v4

    move v4, v5

    move-object v5, v6

    move v6, v7

    move-object v7, v8

    move v8, v1

    .line 345
    invoke-static/range {v3 .. v16}, Lcom/discord/views/ActiveSubscriptionView;->a(Lcom/discord/views/ActiveSubscriptionView;ILcom/discord/views/ActiveSubscriptionView$HeaderImage;ILjava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function0;ZLjava/lang/CharSequence;Lkotlin/jvm/functions/Function0;Ljava/util/List;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final configureButtonText(Lcom/discord/models/domain/ModelSubscription;)V
    .locals 9

    .line 181
    invoke-virtual {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    goto/16 :goto_7

    .line 182
    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getBuyTier2Yearly()Lcom/discord/views/BoxedButton;

    move-result-object v1

    .line 183
    sget-object v2, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_YEAR_TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    .line 184
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object v4

    goto :goto_0

    :cond_1
    move-object v4, v3

    :goto_0
    sget-object v5, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_YEAR_TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    const/4 v6, 0x1

    const/4 v7, 0x0

    if-ne v4, v5, :cond_2

    const/4 v4, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    .line 182
    :goto_1
    invoke-direct {p0, v0, v2, v4}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getPriceText(Landroid/content/Context;Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/discord/views/BoxedButton;->setText(Ljava/lang/String;)V

    .line 185
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getBuyTier2Yearly()Lcom/discord/views/BoxedButton;

    move-result-object v1

    const-string v2, "-16%"

    invoke-virtual {v1, v2}, Lcom/discord/views/BoxedButton;->setBoxedText(Ljava/lang/String;)V

    .line 186
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getBuyTier2Monthly()Landroid/widget/Button;

    move-result-object v1

    .line 187
    sget-object v4, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_MONTH_TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    if-eqz p1, :cond_3

    .line 188
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object v5

    goto :goto_2

    :cond_3
    move-object v5, v3

    :goto_2
    sget-object v8, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_MONTH_TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    if-ne v5, v8, :cond_4

    const/4 v5, 0x1

    goto :goto_3

    :cond_4
    const/4 v5, 0x0

    .line 186
    :goto_3
    invoke-direct {p0, v0, v4, v5}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getPriceText(Landroid/content/Context;Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;Z)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 189
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getBuyTier1Yearly()Lcom/discord/views/BoxedButton;

    move-result-object v1

    .line 190
    sget-object v4, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_YEAR_TIER_1:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    if-eqz p1, :cond_5

    .line 191
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object v5

    goto :goto_4

    :cond_5
    move-object v5, v3

    :goto_4
    sget-object v8, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_YEAR_TIER_1:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    if-ne v5, v8, :cond_6

    const/4 v5, 0x1

    goto :goto_5

    :cond_6
    const/4 v5, 0x0

    .line 189
    :goto_5
    invoke-direct {p0, v0, v4, v5}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getPriceText(Landroid/content/Context;Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/discord/views/BoxedButton;->setText(Ljava/lang/String;)V

    .line 192
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getBuyTier1Yearly()Lcom/discord/views/BoxedButton;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/discord/views/BoxedButton;->setBoxedText(Ljava/lang/String;)V

    .line 193
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getBuyTier1Monthly()Landroid/widget/Button;

    move-result-object v1

    .line 194
    sget-object v2, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_MONTH_TIER_1:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    if-eqz p1, :cond_7

    .line 195
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object v3

    :cond_7
    sget-object p1, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_MONTH_TIER_1:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    if-ne v3, p1, :cond_8

    goto :goto_6

    :cond_8
    const/4 v6, 0x0

    .line 193
    :goto_6
    invoke-direct {p0, v0, v2, v6}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getPriceText(Landroid/content/Context;Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;Z)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v1, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_9
    :goto_7
    return-void
.end method

.method static synthetic configureButtonText$default(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;Lcom/discord/models/domain/ModelSubscription;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 180
    :cond_0
    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->configureButtonText(Lcom/discord/models/domain/ModelSubscription;)V

    return-void
.end method

.method private final configureButtons(Lcom/discord/models/domain/ModelSubscription;)V
    .locals 6

    .line 199
    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->configureButtonText(Lcom/discord/models/domain/ModelSubscription;)V

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    .line 200
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->isAppleSubscription()Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    const/4 p1, 0x4

    new-array p1, p1, [Landroid/view/View;

    const/4 v3, 0x0

    .line 201
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getBuyTier1Monthly()Landroid/widget/Button;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    aput-object v5, p1, v3

    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getBuyTier1Yearly()Lcom/discord/views/BoxedButton;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    aput-object v3, p1, v4

    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getBuyTier2Monthly()Landroid/widget/Button;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    aput-object v3, p1, v1

    const/4 v3, 0x3

    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getBuyTier2Yearly()Lcom/discord/views/BoxedButton;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    aput-object v5, p1, v3

    invoke-static {p1}, Lkotlin/a/m;->k([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 628
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 202
    invoke-static {v3, v4, v0, v1, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setEnabledAlpha$default(Landroid/view/View;ZFILjava/lang/Object;)V

    .line 203
    new-instance v5, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$configureButtons$$inlined$forEach$lambda$1;

    invoke-direct {v5, p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$configureButtons$$inlined$forEach$lambda$1;-><init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;)V

    check-cast v5, Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_0
    return-void

    .line 208
    :cond_1
    invoke-static {}, Lcom/discord/utilities/billing/PremiumUtilsKt;->getUPGRADE_ELIGIBILITY()Ljava/util/Map;

    move-result-object v3

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object p1

    goto :goto_1

    :cond_2
    move-object p1, v2

    :goto_1
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Set;

    if-nez p1, :cond_3

    .line 1034
    sget-object p1, Lkotlin/a/aa;->bkj:Lkotlin/a/aa;

    check-cast p1, Ljava/util/Set;

    .line 210
    :cond_3
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getBuyTier1Monthly()Landroid/widget/Button;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    sget-object v4, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_MONTH_TIER_1:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    invoke-interface {p1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    invoke-static {v3, v4, v0, v1, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setEnabledAlpha$default(Landroid/view/View;ZFILjava/lang/Object;)V

    .line 211
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getBuyTier1Yearly()Lcom/discord/views/BoxedButton;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    sget-object v4, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_YEAR_TIER_1:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    invoke-interface {p1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    invoke-static {v3, v4, v0, v1, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setEnabledAlpha$default(Landroid/view/View;ZFILjava/lang/Object;)V

    .line 212
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getBuyTier2Monthly()Landroid/widget/Button;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    sget-object v4, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_MONTH_TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    invoke-interface {p1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    invoke-static {v3, v4, v0, v1, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setEnabledAlpha$default(Landroid/view/View;ZFILjava/lang/Object;)V

    .line 213
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getBuyTier2Yearly()Lcom/discord/views/BoxedButton;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    sget-object v4, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_YEAR_TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    invoke-interface {p1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    invoke-static {v3, p1, v0, v1, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setEnabledAlpha$default(Landroid/view/View;ZFILjava/lang/Object;)V

    .line 215
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getBuyTier1Monthly()Landroid/widget/Button;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$configureButtons$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$configureButtons$2;-><init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 219
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getBuyTier1Yearly()Lcom/discord/views/BoxedButton;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$configureButtons$3;

    invoke-direct {v0, p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$configureButtons$3;-><init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Lcom/discord/views/BoxedButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 223
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getBuyTier2Monthly()Landroid/widget/Button;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$configureButtons$4;

    invoke-direct {v0, p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$configureButtons$4;-><init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 227
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getBuyTier2Yearly()Lcom/discord/views/BoxedButton;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$configureButtons$5;

    invoke-direct {v0, p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$configureButtons$5;-><init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Lcom/discord/views/BoxedButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final configureGiftingButtons()V
    .locals 9

    .line 233
    new-instance v8, Lcom/discord/utilities/analytics/Traits$Location;

    const-string v1, "User Settings"

    const-string v2, "Discord Nitro"

    const-string v3, "Button CTA"

    const-string v4, "buy"

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/discord/utilities/analytics/Traits$Location;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 240
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getBuyTier1Gift()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$configureGiftingButtons$1;

    invoke-direct {v1, p0, v8}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$configureGiftingButtons$1;-><init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;Lcom/discord/utilities/analytics/Traits$Location;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 248
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getBuyTier2Gift()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$configureGiftingButtons$2;

    invoke-direct {v1, p0, v8}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$configureGiftingButtons$2;-><init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;Lcom/discord/utilities/analytics/Traits$Location;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final configureGrandfatheredHeader(Lcom/discord/models/domain/ModelSubscription;)V
    .locals 7

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 289
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v0

    :goto_0
    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    sget-object v4, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->ordinal()I

    move-result p1

    aget p1, v4, p1

    if-eq p1, v3, :cond_3

    if-eq p1, v1, :cond_2

    :goto_1
    move-object p1, v0

    goto :goto_2

    :cond_2
    const p1, 0x7f120dc5

    new-array v4, v3, [Ljava/lang/Object;

    .line 293
    invoke-virtual {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->requireContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/text/format/DateFormat;->getMediumDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v5

    invoke-static {}, Lcom/discord/utilities/billing/PremiumUtilsKt;->getGRANDFATHERED_YEARLY_END_DATE()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {p0, p1, v4}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_3
    const p1, 0x7f120dc0

    new-array v4, v3, [Ljava/lang/Object;

    .line 291
    invoke-virtual {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->requireContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/text/format/DateFormat;->getMediumDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v5

    invoke-static {}, Lcom/discord/utilities/billing/PremiumUtilsKt;->getGRANDFATHERED_MONTHLY_END_DATE()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {p0, p1, v4}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    :goto_2
    if-eqz p1, :cond_4

    .line 298
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getGrandfathered()Landroid/widget/TextView;

    move-result-object v4

    move-object v5, p1

    check-cast v5, Ljava/lang/CharSequence;

    invoke-static {v5}, Lcom/discord/simpleast/core/a/b;->a(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 300
    :cond_4
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getGrandfathered()Landroid/widget/TextView;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    if-eqz p1, :cond_5

    goto :goto_3

    :cond_5
    const/4 v3, 0x0

    :goto_3
    invoke-static {v4, v3, v2, v1, v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    return-void
.end method

.method private final configureLegalese(Lcom/discord/models/domain/ModelSubscription;)V
    .locals 13

    if-nez p1, :cond_0

    .line 259
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getLegalese()Landroid/widget/TextView;

    move-result-object p1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    .line 263
    :cond_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getInterval()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    if-ne v0, v1, :cond_1

    const v0, 0x7f1202e6

    goto :goto_0

    .line 265
    :cond_1
    new-instance p1, Lkotlin/k;

    invoke-direct {p1}, Lkotlin/k;-><init>()V

    throw p1

    :cond_2
    const v0, 0x7f1202e7

    .line 268
    :goto_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object v3

    sget-object v4, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_0

    const-string v3, ""

    goto :goto_1

    :pswitch_0
    const v3, 0x7f120e47

    .line 274
    invoke-virtual {p0, v3}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :pswitch_1
    const v3, 0x7f120e46

    .line 270
    invoke-virtual {p0, v3}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_1
    const-string v4, "when (premiumSubscriptio\u20262)\n      else -> \"\"\n    }"

    .line 268
    invoke-static {v3, v4}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    .line 278
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getPrice()I

    move-result p1

    invoke-virtual {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->requireContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "requireContext()"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v4}, Lcom/discord/utilities/billing/PremiumUtilsKt;->getFormattedPrice(ILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 279
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getLegalese()Landroid/widget/TextView;

    move-result-object v4

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 280
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getLegalese()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->requireContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v5}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v6

    const v3, 0x7f121139

    .line 283
    invoke-virtual {p0, v3}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v2

    aput-object p1, v5, v1

    .line 281
    invoke-virtual {p0, v0, v5}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "getString(legaleseString\u2026\n            priceString)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v8, p1

    check-cast v8, Ljava/lang/CharSequence;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0xc

    const/4 v12, 0x0

    .line 280
    invoke-static/range {v7 .. v12}, Lcom/discord/utilities/textprocessing/Parsers;->parseMaskedLinks$default(Landroid/content/Context;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v4, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 285
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getLegalese()Landroid/widget/TextView;

    move-result-object p1

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private final dismissAllDialogs(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;)V
    .locals 5

    .line 537
    sget-object v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->ALL_DIALOG_TAGS:Ljava/util/Set;

    check-cast v0, Ljava/lang/Iterable;

    .line 640
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 641
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Ljava/lang/String;

    if-eqz p1, :cond_1

    .line 538
    sget-object v4, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->Companion:Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$Companion;

    invoke-static {v4, p1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$Companion;->access$getTag(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$Companion;Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    invoke-static {v4, v3}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 642
    :cond_2
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 643
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v1, p0

    check-cast v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;

    .line 539
    invoke-direct {v1, v0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->dismissDialog(Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    return-void
.end method

.method static synthetic dismissAllDialogs$default(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 536
    :cond_0
    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->dismissAllDialogs(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;)V

    return-void
.end method

.method private final dismissDialog(Ljava/lang/String;)V
    .locals 1

    .line 557
    invoke-virtual {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->requireFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object p1

    check-cast p1, Landroidx/fragment/app/DialogFragment;

    if-eqz p1, :cond_0

    .line 558
    invoke-virtual {p1}, Landroidx/fragment/app/DialogFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 559
    invoke-virtual {p1}, Landroidx/fragment/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method private final getActiveSubscriptionView()Lcom/discord/views/ActiveSubscriptionView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->activeSubscriptionView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/ActiveSubscriptionView;

    return-object v0
.end method

.method private final getBuyTier1Container()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->buyTier1Container$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getBuyTier1Gift()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->buyTier1Gift$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getBuyTier1Monthly()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->buyTier1Monthly$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getBuyTier1Yearly()Lcom/discord/views/BoxedButton;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->buyTier1Yearly$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/BoxedButton;

    return-object v0
.end method

.method private final getBuyTier2Container()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->buyTier2Container$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getBuyTier2Gift()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->buyTier2Gift$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getBuyTier2Monthly()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->buyTier2Monthly$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getBuyTier2Yearly()Lcom/discord/views/BoxedButton;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->buyTier2Yearly$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/BoxedButton;

    return-object v0
.end method

.method private final getDropdownItems(Lcom/discord/models/domain/ModelSubscription;Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelSubscription;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/ModelPaymentSource;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/discord/views/ActiveSubscriptionView$DropdownItem;",
            ">;"
        }
    .end annotation

    .line 365
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->isAppleSubscription()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1069
    sget-object p1, Lkotlin/a/y;->bkh:Lkotlin/a/y;

    check-cast p1, Ljava/util/List;

    return-object p1

    .line 369
    :cond_0
    move-object v0, p2

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    .line 370
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getPaymentSourceId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 372
    :goto_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    if-eqz v0, :cond_2

    if-nez v1, :cond_2

    .line 374
    sget-object v1, Lcom/discord/views/ActiveSubscriptionView$DropdownItem$b;->AR:Lcom/discord/views/ActiveSubscriptionView$DropdownItem$b;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 377
    :cond_2
    check-cast p2, Ljava/lang/Iterable;

    .line 633
    new-instance v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$getDropdownItems$$inlined$sortedBy$1;

    invoke-direct {v1, p1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$getDropdownItems$$inlined$sortedBy$1;-><init>(Lcom/discord/models/domain/ModelSubscription;)V

    check-cast v1, Ljava/util/Comparator;

    invoke-static {p2, v1}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 634
    new-instance p2, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {p2, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p2, Ljava/util/Collection;

    .line 635
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 636
    check-cast v1, Lcom/discord/models/domain/ModelPaymentSource;

    new-instance v3, Lcom/discord/views/ActiveSubscriptionView$DropdownItem$c;

    .line 384
    invoke-direct {v3, v1}, Lcom/discord/views/ActiveSubscriptionView$DropdownItem$c;-><init>(Lcom/discord/models/domain/ModelPaymentSource;)V

    invoke-interface {p2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 637
    :cond_3
    check-cast p2, Ljava/util/List;

    check-cast p2, Ljava/lang/Iterable;

    .line 638
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/discord/views/ActiveSubscriptionView$DropdownItem$c;

    .line 385
    move-object v1, v2

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    if-eqz v0, :cond_5

    .line 388
    sget-object p1, Lcom/discord/views/ActiveSubscriptionView$DropdownItem$a;->AQ:Lcom/discord/views/ActiveSubscriptionView$DropdownItem$a;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 391
    :cond_5
    check-cast v2, Ljava/util/List;

    return-object v2
.end method

.method private final getGrandfathered()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->grandfathered$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getHeaderBackground(Lcom/discord/models/domain/ModelSubscription;)I
    .locals 3

    .line 429
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getStatus()Lcom/discord/models/domain/ModelSubscription$Status;

    move-result-object v0

    .line 430
    sget-object v1, Lcom/discord/models/domain/ModelSubscription$Status;->CANCELED:Lcom/discord/models/domain/ModelSubscription$Status;

    if-ne v0, v1, :cond_0

    const p1, 0x7f080090

    return p1

    .line 434
    :cond_0
    sget-object v1, Lcom/discord/models/domain/ModelSubscription$Status;->PAST_DUE:Lcom/discord/models/domain/ModelSubscription$Status;

    if-ne v0, v1, :cond_1

    const p1, 0x7f080092

    return p1

    .line 438
    :cond_1
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$WhenMappings;->$EnumSwitchMapping$4:[I

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 445
    new-instance v0, Ljava/lang/Exception;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No Header Background for plan: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :pswitch_0
    const p1, 0x7f08008f

    return p1

    :pswitch_1
    const p1, 0x7f080091

    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private final getHeaderImage(Lcom/discord/models/domain/ModelSubscription;)Lcom/discord/views/ActiveSubscriptionView$HeaderImage;
    .locals 3

    .line 450
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getStatus()Lcom/discord/models/domain/ModelSubscription$Status;

    move-result-object v0

    .line 451
    sget-object v1, Lcom/discord/models/domain/ModelSubscription$Status;->CANCELED:Lcom/discord/models/domain/ModelSubscription$Status;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/discord/models/domain/ModelSubscription$Status;->PAST_DUE:Lcom/discord/models/domain/ModelSubscription$Status;

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 455
    :cond_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$WhenMappings;->$EnumSwitchMapping$5:[I

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 462
    new-instance v0, Ljava/lang/Exception;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No Header Image for plan: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 461
    :pswitch_0
    sget-object p1, Lcom/discord/views/ActiveSubscriptionView$HeaderImage;->AS:Lcom/discord/views/ActiveSubscriptionView$HeaderImage;

    return-object p1

    .line 459
    :pswitch_1
    sget-object p1, Lcom/discord/views/ActiveSubscriptionView$HeaderImage;->AT:Lcom/discord/views/ActiveSubscriptionView$HeaderImage;

    return-object p1

    .line 452
    :cond_1
    :goto_0
    sget-object p1, Lcom/discord/views/ActiveSubscriptionView$HeaderImage;->AU:Lcom/discord/views/ActiveSubscriptionView$HeaderImage;

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private final getHeaderLogo(Lcom/discord/models/domain/ModelSubscription;)I
    .locals 1

    .line 468
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object p1

    sget-object v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$WhenMappings;->$EnumSwitchMapping$6:[I

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const p1, 0x7f080414

    return p1

    :cond_0
    const p1, 0x7f080411

    return p1
.end method

.method private final getHeaderText(Lcom/discord/models/domain/ModelSubscription;)Ljava/lang/CharSequence;
    .locals 16

    move-object/from16 v0, p0

    .line 478
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object v1

    .line 479
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getInterval()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    move-result-object v2

    .line 480
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelSubscription;->getStatus()Lcom/discord/models/domain/ModelSubscription$Status;

    move-result-object v3

    .line 482
    sget-object v4, Lcom/discord/models/domain/ModelSubscription$Status;->PAST_DUE:Lcom/discord/models/domain/ModelSubscription$Status;

    const/4 v5, 0x0

    const/4 v6, 0x1

    const-string v7, "requireContext()"

    if-ne v3, v4, :cond_0

    .line 483
    sget-object v8, Lcom/discord/utilities/time/TimeUtils;->INSTANCE:Lcom/discord/utilities/time/TimeUtils;

    .line 484
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelSubscription;->getCurrentPeriodStart()Ljava/lang/String;

    move-result-object v9

    .line 485
    invoke-virtual/range {p0 .. p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->requireContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10, v7}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x3

    const/16 v14, 0xc

    const/4 v15, 0x0

    .line 483
    invoke-static/range {v8 .. v15}, Lcom/discord/utilities/time/TimeUtils;->renderUtcDate$default(Lcom/discord/utilities/time/TimeUtils;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/text/DateFormat;IILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f120e21

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v1, v3, v5

    .line 488
    invoke-virtual {v0, v2, v3}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "getString(R.string.premi\u2026_past_due, endDateString)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lcom/discord/simpleast/core/a/b;->a(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    return-object v1

    .line 491
    :cond_0
    sget-object v8, Lcom/discord/utilities/time/TimeUtils;->INSTANCE:Lcom/discord/utilities/time/TimeUtils;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelSubscription;->getCurrentPeriodEnd()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p0 .. p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->requireContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10, v7}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0x1c

    const/4 v15, 0x0

    invoke-static/range {v8 .. v15}, Lcom/discord/utilities/time/TimeUtils;->renderUtcDate$default(Lcom/discord/utilities/time/TimeUtils;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/text/DateFormat;IILjava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 494
    sget-object v8, Lcom/discord/models/domain/ModelSubscription$Status;->CANCELED:Lcom/discord/models/domain/ModelSubscription$Status;

    if-eq v3, v8, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelSubscription;->getPaymentSourceId()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelSubscription;->isAppleSubscription()Z

    move-result v3

    if-nez v3, :cond_1

    goto :goto_0

    .line 499
    :cond_1
    new-instance v3, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$getHeaderText$1;

    invoke-direct {v3, v2}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$getHeaderText$1;-><init>(Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;)V

    .line 504
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getPrice()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->requireContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v7}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/discord/utilities/billing/PremiumUtilsKt;->getFormattedPrice(ILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 506
    invoke-virtual {v3}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$getHeaderText$1;->invoke()I

    move-result v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v5

    aput-object v4, v3, v6

    invoke-virtual {v0, v2, v3}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "getString(getHeaderStrin\u2026iceString, endDateString)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    return-object v1

    :cond_2
    :goto_0
    const v1, 0x7f120d8b

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v4, v2, v5

    .line 495
    invoke-virtual {v0, v1, v2}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "getString(R.string.premi\u2026_canceled, endDateString)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    return-object v1
.end method

.method private final getLegalese()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->legalese$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getPriceText(Landroid/content/Context;Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;Z)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    if-eqz p3, :cond_0

    .line 511
    invoke-virtual {p2}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getInterval()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    move-result-object v1

    sget-object v2, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;->MONTHLY:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    if-ne v1, v2, :cond_0

    const p3, 0x7f1202f6

    goto :goto_0

    :cond_0
    if-eqz p3, :cond_1

    .line 512
    invoke-virtual {p2}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getInterval()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    move-result-object p3

    sget-object v1, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;->YEARLY:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    if-ne p3, v1, :cond_1

    const p3, 0x7f1202fb

    goto :goto_0

    .line 513
    :cond_1
    invoke-virtual {p2}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getInterval()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    move-result-object p3

    sget-object v1, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;->MONTHLY:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    if-ne p3, v1, :cond_2

    const p3, 0x7f1202f5

    goto :goto_0

    .line 514
    :cond_2
    invoke-virtual {p2}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getInterval()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    move-result-object p3

    sget-object v1, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;->YEARLY:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    if-ne p3, v1, :cond_3

    const p3, 0x7f1202fa

    goto :goto_0

    :cond_3
    const/4 p3, 0x0

    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 518
    invoke-virtual {p2}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getPrice()I

    move-result p2

    invoke-static {p2, p1}, Lcom/discord/utilities/billing/PremiumUtilsKt;->getFormattedPrice(ILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object p2

    aput-object p2, v1, v0

    invoke-virtual {p1, p3, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "context.getString(timeSt\u2026planType.price, context))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method static synthetic getPriceText$default(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;Landroid/content/Context;Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;ZILjava/lang/Object;)Ljava/lang/String;
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 509
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getPriceText(Landroid/content/Context;Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;Z)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private final getRetryButton()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->retryButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getScrollView()Landroid/widget/ScrollView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->scrollView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    return-object v0
.end method

.method private final getSubscriptionErrorText(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$SubscriptionError;)Ljava/lang/CharSequence;
    .locals 7

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 404
    :cond_0
    sget-object v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$WhenMappings;->$EnumSwitchMapping$3:[I

    invoke-virtual {p1}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$SubscriptionError;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    const p1, 0x7f1205c6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 407
    sget-object v2, Lcom/discord/app/e;->um:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-virtual {p0, p1, v0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "getString(R.string.form_\u2026k.SERVER_STATUS_LOCATION)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    .line 408
    invoke-virtual {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string v0, "requireContext()"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, p1

    check-cast v2, Ljava/lang/CharSequence;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/textprocessing/Parsers;->parseMaskedLinks$default(Landroid/content/Context;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    :cond_1
    new-instance p1, Lkotlin/k;

    invoke-direct {p1}, Lkotlin/k;-><init>()V

    throw p1
.end method

.method private final getUploadPerks()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->uploadPerks$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getViewFlipper()Landroid/widget/ViewFlipper;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->viewFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    return-object v0
.end method

.method private final launchAddPaymentMethodFlow()V
    .locals 3

    .line 564
    sget-object v0, Lcom/discord/utilities/nitro/NitroUtils;->INSTANCE:Lcom/discord/utilities/nitro/NitroUtils;

    move-object v1, p0

    check-cast v1, Lcom/discord/app/AppFragment;

    sget-object v2, Lcom/discord/utilities/intent/IntentUtils$RouteBuilders$Uris;->INSTANCE:Lcom/discord/utilities/intent/IntentUtils$RouteBuilders$Uris;

    invoke-virtual {v2}, Lcom/discord/utilities/intent/IntentUtils$RouteBuilders$Uris;->getSelectSettingsNitro()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/discord/utilities/nitro/NitroUtils;->openWebAddPaymentSource(Lcom/discord/app/AppFragment;Landroid/net/Uri;)V

    return-void
.end method

.method private final onDropdownItemSelected(Lcom/discord/views/ActiveSubscriptionView$DropdownItem;)V
    .locals 2

    .line 395
    instance-of v0, p1, Lcom/discord/views/ActiveSubscriptionView$DropdownItem$c;

    if-eqz v0, :cond_1

    .line 396
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->viewModel:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;

    if-nez v0, :cond_0

    const-string v1, "viewModel"

    invoke-static {v1}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_0
    check-cast p1, Lcom/discord/views/ActiveSubscriptionView$DropdownItem$c;

    .line 1122
    iget-object p1, p1, Lcom/discord/views/ActiveSubscriptionView$DropdownItem$c;->paymentSource:Lcom/discord/models/domain/ModelPaymentSource;

    .line 396
    invoke-virtual {v0, p1}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->changePaymentSource(Lcom/discord/models/domain/ModelPaymentSource;)V

    return-void

    .line 397
    :cond_1
    sget-object v0, Lcom/discord/views/ActiveSubscriptionView$DropdownItem$a;->AQ:Lcom/discord/views/ActiveSubscriptionView$DropdownItem$a;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->launchAddPaymentMethodFlow()V

    return-void

    .line 398
    :cond_2
    sget-object v0, Lcom/discord/views/ActiveSubscriptionView$DropdownItem$b;->AR:Lcom/discord/views/ActiveSubscriptionView$DropdownItem$b;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    return-void

    :cond_3
    new-instance p1, Lkotlin/k;

    invoke-direct {p1}, Lkotlin/k;-><init>()V

    throw p1
.end method

.method private final scrollToSection(Ljava/lang/Integer;)V
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    goto :goto_0

    .line 138
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getBuyTier1Container()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result p1

    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getScrollView()Landroid/widget/ScrollView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getHeight()I

    move-result v1

    add-int/2addr p1, v1

    goto :goto_2

    :cond_1
    :goto_0
    if-nez p1, :cond_2

    goto :goto_1

    .line 139
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/4 v1, 0x1

    if-ne p1, v1, :cond_3

    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getBuyTier2Container()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result p1

    goto :goto_2

    :cond_3
    :goto_1
    const/4 p1, 0x0

    .line 142
    :goto_2
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getScrollView()Landroid/widget/ScrollView;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Landroid/widget/ScrollView;->scrollTo(II)V

    return-void
.end method

.method private final scrollToTop()V
    .locals 2

    .line 133
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getScrollView()Landroid/widget/ScrollView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1}, Landroid/widget/ScrollView;->scrollTo(II)V

    return-void
.end method

.method private final showContent(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;)V
    .locals 10

    .line 146
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getViewFlipper()Landroid/widget/ViewFlipper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 147
    invoke-virtual {p1}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->getPremiumSubscription()Lcom/discord/models/domain/ModelSubscription;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->isNitroSubscription()Z

    move-result v0

    if-nez v0, :cond_1

    .line 148
    sget-object v1, Lcom/discord/app/AppLog;->uB:Lcom/discord/app/AppLog;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Attempting to open WidgetSettingsPremium with non-Nitro subscription: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->getPremiumSubscription()Lcom/discord/models/domain/ModelSubscription;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    .line 149
    invoke-virtual {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->finish()V

    :cond_0
    return-void

    .line 152
    :cond_1
    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->configureActiveSubscriptionView(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;)V

    .line 153
    invoke-virtual {p1}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->getPremiumSubscription()Lcom/discord/models/domain/ModelSubscription;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->configureGrandfatheredHeader(Lcom/discord/models/domain/ModelSubscription;)V

    .line 154
    invoke-virtual {p1}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->getPremiumSubscription()Lcom/discord/models/domain/ModelSubscription;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->configureLegalese(Lcom/discord/models/domain/ModelSubscription;)V

    .line 155
    invoke-virtual {p1}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->getPremiumSubscription()Lcom/discord/models/domain/ModelSubscription;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->configureButtons(Lcom/discord/models/domain/ModelSubscription;)V

    .line 157
    invoke-virtual {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const/4 v1, -0x1

    const-string v2, "intent_section"

    const/4 v3, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v3

    :goto_0
    if-nez v0, :cond_3

    goto :goto_1

    .line 158
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eq v4, v1, :cond_4

    :goto_1
    const-wide/16 v4, 0x12c

    .line 161
    :try_start_0
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v4, v5, v1}, Lrx/Observable;->g(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v1

    const-string v4, "Observable\n            .\u20260, TimeUnit.MILLISECONDS)"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    move-object v4, p0

    check-cast v4, Lcom/discord/app/AppComponent;

    const/4 v5, 0x2

    invoke-static {v1, v4, v3, v5, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    .line 163
    new-instance v4, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$showContent$1;

    invoke-direct {v4, p0, v0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$showContent$1;-><init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;Ljava/lang/Integer;)V

    check-cast v4, Lrx/functions/Action1;

    invoke-virtual {v1, v4}, Lrx/Observable;->b(Lrx/functions/Action1;)Lrx/Subscription;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    .line 167
    sget-object v4, Lcom/discord/app/AppLog;->uB:Lcom/discord/app/AppLog;

    move-object v6, v0

    check-cast v6, Ljava/lang/Throwable;

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    const-string v5, "Error Scrolling to section"

    invoke-static/range {v4 .. v9}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    .line 170
    :goto_2
    invoke-virtual {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 173
    :cond_4
    invoke-virtual {p1}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->getDialog()Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 174
    invoke-virtual {p1}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$ViewState$Loaded;->getDialog()Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->showDialog(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;)V

    return-void

    :cond_5
    const/4 p1, 0x1

    .line 176
    invoke-static {p0, v3, p1, v3}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->dismissAllDialogs$default(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;ILjava/lang/Object;)V

    return-void
.end method

.method private final showDialog(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;)V
    .locals 3

    .line 522
    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->dismissAllDialogs(Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;)V

    .line 524
    invoke-virtual {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->requireFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    const-string v1, "requireFragmentManager()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    .line 525
    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->Companion:Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$Companion;

    invoke-static {v1, p1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$Companion;->access$getTag(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$Companion;Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;)Ljava/lang/String;

    move-result-object v1

    .line 526
    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v2

    check-cast v2, Landroidx/fragment/app/DialogFragment;

    if-eqz v2, :cond_0

    .line 527
    invoke-virtual {v2}, Landroidx/fragment/app/DialogFragment;->isVisible()Z

    move-result v2

    if-nez v2, :cond_3

    .line 529
    :cond_0
    instance-of v2, p1, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog$CancelPremium;

    if-eqz v2, :cond_1

    new-instance p1, Lcom/discord/widgets/settings/nitro/WidgetCancelPremiumDialog;

    invoke-direct {p1}, Lcom/discord/widgets/settings/nitro/WidgetCancelPremiumDialog;-><init>()V

    invoke-virtual {p1, v0, v1}, Lcom/discord/widgets/settings/nitro/WidgetCancelPremiumDialog;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    return-void

    .line 530
    :cond_1
    instance-of v2, p1, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog$DowngradePremium;

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->showDowngradeModal()V

    return-void

    .line 531
    :cond_2
    instance-of p1, p1, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog$UpgradePremium;

    if-eqz p1, :cond_3

    new-instance p1, Lcom/discord/widgets/settings/nitro/WidgetUpgradePremiumDialog;

    invoke-direct {p1}, Lcom/discord/widgets/settings/nitro/WidgetUpgradePremiumDialog;-><init>()V

    invoke-virtual {p1, v0, v1}, Lcom/discord/widgets/settings/nitro/WidgetUpgradePremiumDialog;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    :cond_3
    return-void
.end method

.method private final showDowngradeModal()V
    .locals 3

    .line 545
    new-instance v0, Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;

    invoke-virtual {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "requireContext()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f120e5c

    .line 546
    invoke-virtual {v0, v1}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;->setTitle(I)Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 547
    invoke-virtual {v0, v1}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;->setCancelable(Z)Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;

    move-result-object v0

    const v1, 0x7f120e5b

    .line 548
    invoke-virtual {v0, v1}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;->setMessage(I)Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;

    move-result-object v0

    .line 549
    new-instance v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$showDowngradeModal$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$showDowngradeModal$1;-><init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    const v2, 0x7f120c95

    invoke-virtual {v0, v2, v1}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;->setPositiveButton(ILkotlin/jvm/functions/Function1;)Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;

    move-result-object v0

    const-string v1, "DowngradePremium"

    .line 552
    invoke-virtual {v0, v1}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;->setTag(Ljava/lang/String;)Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;

    move-result-object v0

    .line 553
    invoke-virtual {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->requireFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string v2, "requireFragmentManager()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;->show(Landroidx/fragment/app/FragmentManager;)V

    return-void
.end method

.method private final showFailureUI()V
    .locals 2

    .line 420
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getViewFlipper()Landroid/widget/ViewFlipper;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 421
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getRetryButton()Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$showFailureUI$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$showFailureUI$1;-><init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 424
    invoke-static {p0, v0, v1, v0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->dismissAllDialogs$default(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;ILjava/lang/Object;)V

    return-void
.end method

.method private final showLoadingUI()V
    .locals 2

    .line 414
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getViewFlipper()Landroid/widget/ViewFlipper;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 415
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->scrollToTop()V

    const/4 v0, 0x0

    .line 416
    invoke-static {p0, v0, v1, v0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->dismissAllDialogs$default(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel$Dialog;ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final getContentViewResId()I
    .locals 1

    const v0, 0x7f0d01b8

    return v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .line 575
    invoke-super {p0, p1, p2, p3}, Lcom/discord/app/AppFragment;->onActivityResult(IILandroid/content/Intent;)V

    const p2, 0xfbbc

    if-ne p1, p2, :cond_1

    .line 577
    invoke-virtual {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-static {p1}, Landroidx/lifecycle/ViewModelProviders;->of(Landroidx/fragment/app/FragmentActivity;)Landroidx/lifecycle/ViewModelProvider;

    move-result-object p1

    .line 578
    const-class p2, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;

    invoke-virtual {p1, p2}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string p2, "ViewModelProviders.of(re\u2026iumViewModel::class.java)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    .line 577
    check-cast p1, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;

    .line 579
    invoke-virtual {p1}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->getLaunchPremiumTabStartTimeMs()J

    move-result-wide p2

    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    if-eqz v2, :cond_0

    .line 580
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p2

    invoke-virtual {p1}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->getLaunchPremiumTabStartTimeMs()J

    move-result-wide v0

    sub-long v0, p2, v0

    .line 584
    :cond_0
    sget-object p1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    const-string p2, "Premium Purchase Tab"

    invoke-virtual {p1, p2, v0, v1}, Lcom/discord/utilities/analytics/AnalyticsTracker;->externalViewClosed(Ljava/lang/String;J)V

    :cond_1
    return-void
.end method

.method public final onPause()V
    .locals 2

    .line 127
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->connection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->requireContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    :cond_0
    const/4 v0, 0x0

    .line 128
    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->connection:Landroid/content/ServiceConnection;

    .line 129
    invoke-super {p0}, Lcom/discord/app/AppFragment;->onPause()V

    return-void
.end method

.method public final onResume()V
    .locals 14

    .line 89
    invoke-super {p0}, Lcom/discord/app/AppFragment;->onResume()V

    .line 90
    invoke-virtual {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroidx/lifecycle/ViewModelProviders;->of(Landroidx/fragment/app/FragmentActivity;)Landroidx/lifecycle/ViewModelProvider;

    move-result-object v0

    const-class v1, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;

    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object v0

    const-string v1, "ViewModelProviders.of(re\u2026iumViewModel::class.java)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;

    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->viewModel:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;

    .line 91
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->viewModel:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;

    const-string v1, "viewModel"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    .line 92
    :cond_0
    invoke-virtual {v0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->getViewState()Lrx/Observable;

    move-result-object v0

    const-wide/16 v2, 0xc8

    .line 93
    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v4}, Lrx/Observable;->j(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v0

    .line 94
    invoke-virtual {v0}, Lrx/Observable;->JL()Lrx/Observable;

    move-result-object v0

    const-string v2, "viewModel\n        .getVi\u2026  .distinctUntilChanged()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    move-object v2, p0

    check-cast v2, Lcom/discord/app/AppComponent;

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v0, v2, v4, v3, v4}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v5

    .line 97
    new-instance v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$onResume$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$onResume$1;-><init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;)V

    move-object v11, v0

    check-cast v11, Lkotlin/jvm/functions/Function1;

    .line 104
    const-class v6, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v12, 0x1e

    const/4 v13, 0x0

    .line 96
    invoke-static/range {v5 .. v13}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 106
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->viewModel:Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    .line 107
    :cond_1
    invoke-virtual {v0}, Lcom/discord/widgets/settings/nitro/SettingsPremiumViewModel;->getRequestBuyPlanSubject()Lrx/Observable;

    move-result-object v0

    .line 108
    invoke-static {v0, v2, v4, v3, v4}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v5

    .line 110
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    .line 111
    new-instance v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$onResume$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium$onResume$2;-><init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;)V

    move-object v11, v0

    check-cast v11, Lkotlin/jvm/functions/Function1;

    const/16 v12, 0x1e

    const/4 v13, 0x0

    .line 109
    invoke-static/range {v5 .. v13}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 123
    sget-object v0, Lcom/discord/utilities/nitro/NitroUtils;->INSTANCE:Lcom/discord/utilities/nitro/NitroUtils;

    invoke-virtual {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "requireContext()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/utilities/nitro/NitroUtils;->warmupBillingTabs(Landroid/content/Context;)Landroidx/browser/customtabs/CustomTabsServiceConnection;

    move-result-object v0

    check-cast v0, Landroid/content/ServiceConnection;

    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->connection:Landroid/content/ServiceConnection;

    return-void
.end method

.method public final onViewBound(Landroid/view/View;)V
    .locals 5

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    const p1, 0x7f121255

    .line 75
    invoke-virtual {p0, p1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->setActionBarSubtitle(I)Lkotlin/Unit;

    const p1, 0x7f120e4a

    .line 76
    invoke-virtual {p0, p1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->setActionBarTitle(I)Lkotlin/Unit;

    const/4 p1, 0x3

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 77
    invoke-static {p0, v0, v1, p1, v1}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled$default(Lcom/discord/app/AppFragment;ZLjava/lang/Integer;ILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    .line 79
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getUploadPerks()Landroid/widget/TextView;

    move-result-object v2

    new-array p1, p1, [Ljava/lang/Object;

    const v3, 0x7f1205a7

    .line 80
    invoke-virtual {p0, v3}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, p1, v0

    const v0, 0x7f1205a6

    .line 81
    invoke-virtual {p0, v0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x1

    aput-object v0, p1, v3

    const v0, 0x7f1205a5

    .line 82
    invoke-virtual {p0, v0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x2

    aput-object v0, p1, v4

    const v0, 0x7f120d9b

    .line 79
    invoke-virtual {p0, v0, p1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    invoke-static {p0, v1, v3, v1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->configureButtonText$default(Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;Lcom/discord/models/domain/ModelSubscription;ILjava/lang/Object;)V

    .line 85
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsPremium;->configureGiftingButtons()V

    return-void
.end method
