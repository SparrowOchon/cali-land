.class final Lcom/discord/widgets/settings/nitro/WidgetSettingsGifting$configureUI$4;
.super Lkotlin/jvm/internal/l;
.source "WidgetSettingsGifting.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/nitro/WidgetSettingsGifting;->configureUI(Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingViewModel$ViewState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/l;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Long;",
        "Ljava/lang/Long;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/settings/nitro/WidgetSettingsGifting$configureUI$4;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGifting$configureUI$4;

    invoke-direct {v0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGifting$configureUI$4;-><init>()V

    sput-object v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGifting$configureUI$4;->INSTANCE:Lcom/discord/widgets/settings/nitro/WidgetSettingsGifting$configureUI$4;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/l;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .line 43
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p0, v0, v1, p2}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGifting$configureUI$4;->invoke(JLjava/lang/Long;)V

    sget-object p1, Lkotlin/Unit;->bjS:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(JLjava/lang/Long;)V
    .locals 1

    .line 197
    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    .line 198
    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGifting()Lcom/discord/stores/StoreGifting;

    move-result-object v0

    .line 199
    invoke-virtual {v0, p1, p2, p3}, Lcom/discord/stores/StoreGifting;->generateGiftCode(JLjava/lang/Long;)V

    return-void
.end method
