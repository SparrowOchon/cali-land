.class final Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem$onConfigure$5;
.super Ljava/lang/Object;
.source "WidgetSettingsGiftingAdapter.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->onConfigure(ILcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$GiftItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation


# instance fields
.field final synthetic $data:Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$GiftItem;

.field final synthetic this$0:Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;


# direct methods
.method constructor <init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$GiftItem;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem$onConfigure$5;->this$0:Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;

    iput-object p2, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem$onConfigure$5;->$data:Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$GiftItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    .line 149
    iget-object p1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem$onConfigure$5;->this$0:Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;

    invoke-static {p1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->access$getAdapter$p(Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;)Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter;

    move-result-object p1

    invoke-static {p1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter;->access$getOnGenerateClickListener$p(Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter;)Lkotlin/jvm/functions/Function2;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem$onConfigure$5;->$data:Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$GiftItem;

    invoke-virtual {v0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$GiftItem;->getEntitlement()Lcom/discord/models/domain/ModelEntitlement;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelEntitlement;->getSkuId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem$onConfigure$5;->$data:Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$GiftItem;

    invoke-virtual {v1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$GiftItem;->getEntitlement()Lcom/discord/models/domain/ModelEntitlement;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelEntitlement;->getSubscriptionPlan()Lcom/discord/models/domain/ModelSubscriptionPlan;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscriptionPlan;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-interface {p1, v0, v1}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
