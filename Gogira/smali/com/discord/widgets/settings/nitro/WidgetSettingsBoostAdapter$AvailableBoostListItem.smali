.class public final Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$AvailableBoostListItem;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
.source "WidgetSettingsBoostAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AvailableBoostListItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
        "Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter;",
        "Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final boost$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/v;

    const-class v2, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$AvailableBoostListItem;

    invoke-static {v2}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v2

    const-string v3, "boost"

    const-string v4, "getBoost()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$AvailableBoostListItem;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter;)V
    .locals 1

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    check-cast p1, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    const v0, 0x7f0d00a8

    invoke-direct {p0, v0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;-><init>(ILcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)V

    const p1, 0x7f0a00d0

    .line 80
    invoke-static {p0, p1}, Lkotterknife/b;->a(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$AvailableBoostListItem;->boost$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getAdapter$p(Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$AvailableBoostListItem;)Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$AvailableBoostListItem;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter;

    return-object p0
.end method

.method private final getBoost()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$AvailableBoostListItem;->boost$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$AvailableBoostListItem;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method protected final onConfigure(ILcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;)V
    .locals 4

    const-string v0, "data"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    invoke-super {p0, p1, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->onConfigure(ILjava/lang/Object;)V

    .line 85
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$AvailableBoostListItem;->getBoost()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p2}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->getEnabled()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {p1, v0, v2, v3, v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setEnabledAlpha$default(Landroid/view/View;ZFILjava/lang/Object;)V

    .line 86
    invoke-virtual {p2}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->getEnabled()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 87
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$AvailableBoostListItem;->getBoost()Landroid/view/View;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$AvailableBoostListItem$onConfigure$1;

    invoke-direct {p2, p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$AvailableBoostListItem$onConfigure$1;-><init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$AvailableBoostListItem;)V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    .line 91
    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$AvailableBoostListItem;->getBoost()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final bridge synthetic onConfigure(ILjava/lang/Object;)V
    .locals 0

    .line 77
    check-cast p2, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$AvailableBoostListItem;->onConfigure(ILcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;)V

    return-void
.end method
