.class public final Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem$Companion;
.super Ljava/lang/Object;
.source "WidgetSettingsBoostAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 172
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final createAvailableBoostItem(Z)Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;
    .locals 10

    .line 177
    new-instance v9, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/16 v7, 0xf

    const/4 v8, 0x0

    move-object v0, v9

    move v5, p1

    invoke-direct/range {v0 .. v8}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;-><init>(Ljava/lang/Long;Ljava/lang/Integer;Lcom/discord/models/domain/ModelPremiumGuildSubscription;Lcom/discord/models/domain/ModelGuild;ZIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v9
.end method

.method public final createBoostInfoItem(Ljava/lang/Long;I)Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;
    .locals 10

    .line 183
    new-instance v9, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1c

    const/4 v8, 0x0

    move-object v0, v9

    move-object v1, p1

    invoke-direct/range {v0 .. v8}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;-><init>(Ljava/lang/Long;Ljava/lang/Integer;Lcom/discord/models/domain/ModelPremiumGuildSubscription;Lcom/discord/models/domain/ModelGuild;ZIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v9
.end method

.method public final createBoostedGuildItem(Lcom/discord/models/domain/ModelPremiumGuildSubscription;Lcom/discord/models/domain/ModelGuild;)Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;
    .locals 10

    const-string v0, "premiumGuildSubscription"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    new-instance v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x2

    const/16 v8, 0x13

    const/4 v9, 0x0

    move-object v1, v0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v9}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;-><init>(Ljava/lang/Long;Ljava/lang/Integer;Lcom/discord/models/domain/ModelPremiumGuildSubscription;Lcom/discord/models/domain/ModelGuild;ZIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
