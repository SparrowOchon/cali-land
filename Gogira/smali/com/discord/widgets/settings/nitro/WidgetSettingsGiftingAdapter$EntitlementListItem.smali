.class public final Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
.source "WidgetSettingsGiftingAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EntitlementListItem"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
        "Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter;",
        "Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$GiftItem;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem$Companion;

.field private static final VIEW_INDEX_CODE:I = 0x0

.field private static final VIEW_INDEX_GENERATE:I = 0x1


# instance fields
.field private final code$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final codeContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final container$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final copy$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final divider$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final generate$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final revokeText$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-class v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;

    const/16 v1, 0x8

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "flipper"

    const-string v5, "getFlipper()Lcom/discord/app/AppViewFlipper;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "container"

    const-string v5, "getContainer()Landroid/view/View;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "code"

    const-string v5, "getCode()Landroid/widget/TextView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "codeContainer"

    const-string v5, "getCodeContainer()Landroid/view/View;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "copy"

    const-string v5, "getCopy()Landroid/widget/TextView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x4

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "revokeText"

    const-string v5, "getRevokeText()Landroid/widget/TextView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x5

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "generate"

    const-string v5, "getGenerate()Landroid/view/View;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x6

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v0

    const-string v3, "divider"

    const-string v4, "getDivider()Landroid/view/View;"

    invoke-direct {v2, v0, v3, v4}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aput-object v0, v1, v2

    sput-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->Companion:Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter;)V
    .locals 1

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    check-cast p1, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    const v0, 0x7f0d0088

    invoke-direct {p0, v0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;-><init>(ILcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)V

    const p1, 0x7f0a0315

    .line 91
    invoke-static {p0, p1}, Lkotterknife/b;->a(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0312

    .line 92
    invoke-static {p0, p1}, Lkotterknife/b;->a(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->container$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0310

    .line 93
    invoke-static {p0, p1}, Lkotterknife/b;->a(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->code$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0311

    .line 94
    invoke-static {p0, p1}, Lkotterknife/b;->a(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->codeContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0313

    .line 95
    invoke-static {p0, p1}, Lkotterknife/b;->a(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->copy$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0317

    .line 96
    invoke-static {p0, p1}, Lkotterknife/b;->a(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->revokeText$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0316

    .line 97
    invoke-static {p0, p1}, Lkotterknife/b;->a(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->generate$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0314

    .line 98
    invoke-static {p0, p1}, Lkotterknife/b;->a(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->divider$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getAdapter$p(Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;)Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter;
    .locals 0

    .line 89
    iget-object p0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter;

    return-object p0
.end method

.method private final getCode()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->code$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getCodeContainer()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->codeContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getContainer()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->container$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getCopy()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->copy$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getDivider()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->divider$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getFlipper()Lcom/discord/app/AppViewFlipper;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/app/AppViewFlipper;

    return-object v0
.end method

.method private final getGenerate()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->generate$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getRevokeText()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->revokeText$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method protected final onConfigure(ILcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$GiftItem;)V
    .locals 12

    const-string v0, "data"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    invoke-super {p0, p1, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->onConfigure(ILjava/lang/Object;)V

    .line 103
    invoke-virtual {p2}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$GiftItem;->getExpanded()Ljava/lang/Boolean;

    move-result-object p1

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    const/4 v0, 0x0

    const-string v1, "itemView"

    if-eqz p1, :cond_0

    .line 104
    iget-object p1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->itemView:Landroid/view/View;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->itemView:Landroid/view/View;

    invoke-static {p2, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p2

    .line 105
    iput v0, p2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 104
    invoke-virtual {p1, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    .line 109
    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->itemView:Landroid/view/View;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->itemView:Landroid/view/View;

    invoke-static {v2, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    const/4 v3, -0x2

    .line 110
    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 109
    invoke-virtual {p1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 114
    iget-object p1, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->itemView:Landroid/view/View;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 116
    invoke-virtual {p2}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$GiftItem;->getEntitlement()Lcom/discord/models/domain/ModelEntitlement;

    move-result-object p1

    if-eqz p1, :cond_7

    invoke-virtual {p2}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$GiftItem;->isLastItem()Ljava/lang/Boolean;

    move-result-object p1

    if-nez p1, :cond_1

    goto/16 :goto_4

    .line 118
    :cond_1
    invoke-virtual {p2}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$GiftItem;->getGift()Lcom/discord/models/domain/ModelGift;

    move-result-object p1

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x2

    if-eqz p1, :cond_5

    .line 119
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    .line 120
    invoke-virtual {p2}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$GiftItem;->getWasCopied()Ljava/lang/Boolean;

    move-result-object p1

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-static {p1, v3}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    .line 122
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->getCopy()Landroid/widget/TextView;

    move-result-object v3

    if-eqz p1, :cond_2

    const v4, 0x7f08009a

    goto :goto_0

    :cond_2
    const v4, 0x7f080099

    :goto_0
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 123
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->getCopy()Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->itemView:Landroid/view/View;

    invoke-static {v4, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    if-eqz p1, :cond_3

    const v5, 0x7f120448

    goto :goto_1

    :cond_3
    const v5, 0x7f12044a

    :goto_1
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->getCodeContainer()Landroid/view/View;

    move-result-object v3

    const-string v4, "context"

    if-eqz p1, :cond_4

    .line 125
    invoke-static {v2, v4}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const p1, 0x7f04014b

    invoke-static {v2, p1, v0, v10, v9}, Lcom/discord/utilities/drawable/DrawableCompat;->getThemedDrawableRes$default(Landroid/content/Context;IIILjava/lang/Object;)I

    move-result p1

    goto :goto_2

    .line 126
    :cond_4
    invoke-static {v2, v4}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const p1, 0x7f0402ab

    invoke-static {v2, p1, v0, v10, v9}, Lcom/discord/utilities/drawable/DrawableCompat;->getThemedDrawableRes$default(Landroid/content/Context;IIILjava/lang/Object;)I

    move-result p1

    .line 124
    :goto_2
    invoke-virtual {v3, p1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 128
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->getCopy()Landroid/widget/TextView;

    move-result-object p1

    new-instance v3, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem$onConfigure$3;

    invoke-direct {v3, p0, p2}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem$onConfigure$3;-><init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$GiftItem;)V

    check-cast v3, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 132
    sget-object p1, Lcom/discord/utilities/gifting/GiftingUtils;->INSTANCE:Lcom/discord/utilities/gifting/GiftingUtils;

    .line 133
    invoke-virtual {p2}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$GiftItem;->getGift()Lcom/discord/models/domain/ModelGift;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/discord/models/domain/ModelGift;->getExpiresDiff(J)J

    move-result-wide v3

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const-string v6, "context.resources"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v3, v4, v5}, Lcom/discord/utilities/gifting/GiftingUtils;->getTimeString(JLandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object p1

    .line 135
    iget-object v3, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->itemView:Landroid/view/View;

    invoke-static {v3, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f120790

    new-array v5, v8, [Ljava/lang/Object;

    aput-object p1, v5, v0

    .line 136
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v3, "itemView.context\n       \u2026_mobile, expiresInString)"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "revokeHook"

    const-string v4, "https://www.discordapp.com"

    .line 1075
    invoke-static {p1, v3, v4, v0}, Lkotlin/text/l;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p1

    const-string v3, "$["

    const-string v4, "["

    .line 2075
    invoke-static {p1, v3, v4, v0}, Lkotlin/text/l;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p1

    .line 140
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->getRevokeText()Landroid/widget/TextView;

    move-result-object v11

    move-object v3, p1

    check-cast v3, Ljava/lang/CharSequence;

    .line 141
    new-instance p1, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem$onConfigure$4;

    invoke-direct {p1, p0, p2}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem$onConfigure$4;-><init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$GiftItem;)V

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function2;

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    .line 140
    invoke-static/range {v2 .. v7}, Lcom/discord/utilities/textprocessing/Parsers;->parseMaskedLinks$default(Landroid/content/Context;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v11, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->getCode()Landroid/widget/TextView;

    move-result-object p1

    sget-object v2, Lcom/discord/utilities/gifting/GiftingUtils;->INSTANCE:Lcom/discord/utilities/gifting/GiftingUtils;

    invoke-virtual {p2}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$GiftItem;->getGift()Lcom/discord/models/domain/ModelGift;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelGift;->getCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/discord/utilities/gifting/GiftingUtils;->generateGiftUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 147
    :cond_5
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object p1

    invoke-virtual {p1, v8}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    .line 148
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->getGenerate()Landroid/view/View;

    move-result-object p1

    new-instance v2, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem$onConfigure$5;

    invoke-direct {v2, p0, p2}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem$onConfigure$5;-><init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$GiftItem;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    :goto_3
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->getDivider()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p2}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$GiftItem;->isLastItem()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    xor-int/2addr v2, v8

    invoke-static {p1, v2, v0, v10, v9}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy$default(Landroid/view/View;ZIILjava/lang/Object;)V

    .line 154
    invoke-virtual {p2}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$GiftItem;->isLastItem()Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_6

    .line 155
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->getContainer()Landroid/view/View;

    move-result-object p1

    iget-object p2, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->itemView:Landroid/view/View;

    invoke-static {p2, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    const-string v1, "itemView.context"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const v1, 0x7f0402a8

    invoke-static {p2, v1, v0, v10, v9}, Lcom/discord/utilities/drawable/DrawableCompat;->getThemedDrawableRes$default(Landroid/content/Context;IIILjava/lang/Object;)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundResource(I)V

    return-void

    .line 157
    :cond_6
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->getContainer()Landroid/view/View;

    move-result-object p1

    iget-object p2, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->itemView:Landroid/view/View;

    invoke-static {p2, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    const v0, 0x7f0402a6

    invoke-static {p2, v0}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_7
    :goto_4
    return-void
.end method

.method public final bridge synthetic onConfigure(ILjava/lang/Object;)V
    .locals 0

    .line 89
    check-cast p2, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$GiftItem;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$EntitlementListItem;->onConfigure(ILcom/discord/widgets/settings/nitro/WidgetSettingsGiftingAdapter$GiftItem;)V

    return-void
.end method
