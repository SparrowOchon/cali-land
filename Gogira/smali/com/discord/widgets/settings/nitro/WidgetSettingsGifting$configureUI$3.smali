.class final Lcom/discord/widgets/settings/nitro/WidgetSettingsGifting$configureUI$3;
.super Lkotlin/jvm/internal/l;
.source "WidgetSettingsGifting.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/nitro/WidgetSettingsGifting;->configureUI(Lcom/discord/widgets/settings/nitro/WidgetSettingsGiftingViewModel$ViewState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/l;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/models/domain/ModelGift;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/settings/nitro/WidgetSettingsGifting$configureUI$3;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGifting$configureUI$3;

    invoke-direct {v0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGifting$configureUI$3;-><init>()V

    sput-object v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsGifting$configureUI$3;->INSTANCE:Lcom/discord/widgets/settings/nitro/WidgetSettingsGifting$configureUI$3;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/l;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 43
    check-cast p1, Lcom/discord/models/domain/ModelGift;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsGifting$configureUI$3;->invoke(Lcom/discord/models/domain/ModelGift;)V

    sget-object p1, Lkotlin/Unit;->bjS:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/models/domain/ModelGift;)V
    .locals 1

    const-string v0, "gift"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    .line 193
    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGifting()Lcom/discord/stores/StoreGifting;

    move-result-object v0

    .line 194
    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreGifting;->revokeGiftCode(Lcom/discord/models/domain/ModelGift;)V

    return-void
.end method
