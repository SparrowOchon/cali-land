.class public final Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;
.super Lcom/discord/app/AppFragment;
.source "WidgetSettingsBoost.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model;,
        Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Companion;
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Companion;

.field public static final VIEW_INDEX_FAILURE:I = 0x1

.field public static final VIEW_INDEX_LOADED:I = 0x3

.field public static final VIEW_INDEX_LOADING:I = 0x0

.field public static final VIEW_INDEX_NO_NITRO:I = 0x2


# instance fields
.field private adapter:Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter;

.field private final flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final noNitroBanner$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final noNitroLearnMore$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final recycler$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final retry$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final subtext$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-class v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;

    const/4 v1, 0x6

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "subtext"

    const-string v5, "getSubtext()Landroid/widget/TextView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "retry"

    const-string v5, "getRetry()Landroid/view/View;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "recycler"

    const-string v5, "getRecycler()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "flipper"

    const-string v5, "getFlipper()Lcom/discord/app/AppViewFlipper;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "noNitroBanner"

    const-string v5, "getNoNitroBanner()Landroid/widget/TextView;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x4

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v0

    const-string v3, "noNitroLearnMore"

    const-string v4, "getNoNitroLearnMore()Landroid/view/View;"

    invoke-direct {v2, v0, v3, v4}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aput-object v0, v1, v2

    sput-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->Companion:Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 33
    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a0696

    .line 35
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->subtext$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0695

    .line 36
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->retry$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0694

    .line 37
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->recycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a068f

    .line 38
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0690

    .line 40
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->noNitroBanner$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0691

    .line 41
    invoke-static {p0, v0}, Lkotterknife/b;->c(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->noNitroLearnMore$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->configureUI(Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model;)V

    return-void
.end method

.method public static final synthetic access$fetchData(Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;)V
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->fetchData()V

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model;)V
    .locals 7

    .line 99
    instance-of v0, p1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Loading;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 100
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    return-void

    .line 102
    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Failure;

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    .line 103
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object p1

    invoke-virtual {p1, v2}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    return-void

    .line 105
    :cond_1
    instance-of v0, p1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Loaded;

    if-eqz v0, :cond_3

    .line 106
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    .line 107
    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->adapter:Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter;

    if-nez v0, :cond_2

    const-string v1, "adapter"

    invoke-static {v1}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    .line 108
    :cond_2
    check-cast p1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Loaded;

    invoke-virtual {p1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Loaded;->getBoostItems()Ljava/util/List;

    move-result-object p1

    .line 109
    new-instance v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$configureUI$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$configureUI$1;-><init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 110
    new-instance v2, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$configureUI$2;

    invoke-direct {v2, p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$configureUI$2;-><init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;)V

    check-cast v2, Lkotlin/jvm/functions/Function2;

    .line 107
    invoke-virtual {v0, p1, v2, v1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter;->configure(Ljava/util/List;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function0;)V

    return-void

    .line 114
    :cond_3
    instance-of v0, p1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$LoadedNoNitro;

    if-eqz v0, :cond_5

    .line 115
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v0

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    .line 117
    check-cast p1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$LoadedNoNitro;

    invoke-virtual {p1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$LoadedNoNitro;->isNitroClassic()Z

    move-result p1

    if-eqz p1, :cond_4

    const p1, 0x7f120dd9

    goto :goto_0

    :cond_4
    const p1, 0x7f120dd8

    :goto_0
    new-array v0, v2, [Ljava/lang/Object;

    .line 121
    sget-object v2, Lcom/discord/app/e;->uy:Lcom/discord/app/e;

    sget-wide v2, Lcom/discord/app/e;->ux:J

    invoke-static {v2, v3}, Lcom/discord/app/e;->k(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 116
    invoke-virtual {p0, p1, v0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "getString(\n            i\u2026elpDesk.SERVER_BOOSTING))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->getNoNitroBanner()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "requireContext()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, p1

    check-cast v2, Ljava/lang/CharSequence;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/textprocessing/Parsers;->parseMaskedLinks$default(Landroid/content/Context;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    return-void
.end method

.method private final fetchData()V
    .locals 1

    .line 129
    sget-object v0, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->INSTANCE:Lcom/discord/stores/StorePremiumGuildSubscription$Actions;

    .line 130
    invoke-virtual {v0}, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->fetchUserGuildPremiumState()V

    .line 134
    invoke-static {}, Lcom/discord/stores/StoreSubscriptions$Actions;->fetchSubscriptions()V

    return-void
.end method

.method private final getFlipper()Lcom/discord/app/AppViewFlipper;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/app/AppViewFlipper;

    return-object v0
.end method

.method private final getNoNitroBanner()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->noNitroBanner$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getNoNitroLearnMore()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->noNitroLearnMore$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getRecycler()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->recycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getRetry()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->retry$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getSubtext()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->subtext$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public static final launch(Landroid/content/Context;)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->Companion:Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Companion;

    invoke-virtual {v0, p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Companion;->launch(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final getContentViewResId()I
    .locals 1

    const v0, 0x7f0d01a3

    return v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .line 89
    invoke-super {p0, p1, p2, p3}, Lcom/discord/app/AppFragment;->onActivityResult(IILandroid/content/Intent;)V

    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    const/4 p1, -0x1

    if-ne p2, p1, :cond_2

    if-eqz p3, :cond_0

    const-wide/16 p1, 0x0

    const-string v0, "EXTRA_GUILD_ID"

    .line 92
    invoke-virtual {p3, v0, p1, p2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide p1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    .line 93
    sget-object p3, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation;->Companion:Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation$Companion;

    invoke-virtual {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "requireContext()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3, v0, p1, p2}, Lcom/discord/widgets/servers/boosting/WidgetServerBoostConfirmation$Companion;->create(Landroid/content/Context;J)V

    goto :goto_1

    .line 92
    :cond_1
    new-instance p1, Lkotlin/r;

    const-string p2, "null cannot be cast to non-null type com.discord.models.domain.GuildId /* = kotlin.Long */"

    invoke-direct {p1, p2}, Lkotlin/r;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    :goto_1
    return-void
.end method

.method public final onViewBound(Landroid/view/View;)V
    .locals 8

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x3

    .line 50
    invoke-static {p0, v1, v0, v2, v0}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled$default(Lcom/discord/app/AppFragment;ZLjava/lang/Integer;ILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    const v0, 0x7f121255

    .line 52
    invoke-virtual {p0, v0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->setActionBarSubtitle(I)Lkotlin/Unit;

    const v0, 0x7f120e0d

    .line 53
    invoke-virtual {p0, v0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->setActionBarTitle(I)Lkotlin/Unit;

    .line 55
    sget-object v0, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->Companion:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;

    new-instance v2, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    check-cast v2, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    invoke-virtual {v0, v2}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter;

    iput-object v0, p0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->adapter:Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter;

    .line 57
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->getSubtext()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string p1, "view.context"

    invoke-static {v2, p1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    .line 58
    sget-object v3, Lcom/discord/app/e;->uy:Lcom/discord/app/e;

    sget-wide v3, Lcom/discord/app/e;->ux:J

    invoke-static {v3, v4}, Lcom/discord/app/e;->k(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, p1, v1

    const v1, 0x7f120e0a

    invoke-virtual {p0, v1, p1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "getString(R.string.premi\u2026elpDesk.SERVER_BOOSTING))"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, p1

    check-cast v3, Ljava/lang/CharSequence;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    .line 57
    invoke-static/range {v2 .. v7}, Lcom/discord/utilities/textprocessing/Parsers;->parseMaskedLinks$default(Landroid/content/Context;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->getSubtext()Landroid/widget/TextView;

    move-result-object p1

    sget-object v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$onViewBound$1;->INSTANCE:Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$onViewBound$1;

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->getRetry()Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$onViewBound$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$onViewBound$2;-><init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->getNoNitroLearnMore()Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$onViewBound$3;

    invoke-direct {v0, p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$onViewBound$3;-><init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->fetchData()V

    return-void
.end method

.method public final onViewBoundOrOnResume()V
    .locals 13

    .line 75
    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    .line 77
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;->fetchData()V

    .line 79
    sget-object v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model;->Companion:Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Companion;

    .line 80
    invoke-virtual {v0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Companion;->get()Lrx/Observable;

    move-result-object v0

    .line 81
    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    .line 82
    move-object v1, p0

    check-cast v1, Lcom/discord/app/AppComponent;

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v0, v1, v2, v3, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    .line 83
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    new-instance v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$onViewBoundOrOnResume$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost;)V

    move-object v10, v0

    check-cast v10, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x1e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
