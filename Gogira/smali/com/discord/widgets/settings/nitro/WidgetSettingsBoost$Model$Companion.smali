.class public final Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Companion;
.super Ljava/lang/Object;
.source "WidgetSettingsBoost.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 143
    invoke-direct {p0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$createBoostItems(Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Companion;Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;Ljava/util/Map;)Ljava/util/List;
    .locals 0

    .line 143
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Companion;->createBoostItems(Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;Ljava/util/Map;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private final createBoostItems(Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;Ljava/util/Map;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;",
            "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;",
            ">;"
        }
    .end annotation

    .line 203
    invoke-virtual {p1}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->getCooldown()Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;

    move-result-object v0

    .line 204
    invoke-virtual {p2}, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;->getSubscriptions()Ljava/util/List;

    move-result-object p2

    .line 205
    invoke-virtual {p1}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->getPremiumGuildSubscriptions()Ljava/util/List;

    move-result-object p1

    .line 206
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 208
    sget-object v2, Lcom/discord/utilities/nitro/BoostUtils;->INSTANCE:Lcom/discord/utilities/nitro/BoostUtils;

    invoke-virtual {v2, p2}, Lcom/discord/utilities/nitro/BoostUtils;->calculateTotalBoostCount(Ljava/util/List;)I

    move-result p2

    if-eqz v0, :cond_0

    .line 209
    invoke-virtual {v0}, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;->getRemaining()I

    move-result p2

    goto :goto_0

    :cond_0
    move-object v2, p1

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    sub-int/2addr p2, v2

    :goto_0
    if-nez v0, :cond_1

    if-lez p2, :cond_3

    .line 212
    :cond_1
    sget-object v2, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->Companion:Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem$Companion;

    if-eqz v0, :cond_2

    .line 213
    invoke-virtual {v0}, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;->getExpiresAtTimestamp()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    .line 212
    :goto_1
    invoke-virtual {v2, v0, p2}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem$Companion;->createBoostInfoItem(Ljava/lang/Long;I)Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    if-lez p2, :cond_4

    .line 218
    sget-object p2, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->Companion:Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem$Companion;

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem$Companion;->createAvailableBoostItem(Z)Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;

    move-result-object p2

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 219
    :cond_4
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_5

    .line 220
    sget-object p2, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->Companion:Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem$Companion;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem$Companion;->createAvailableBoostItem(Z)Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;

    move-result-object p2

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 223
    :cond_5
    :goto_2
    check-cast p1, Ljava/lang/Iterable;

    .line 247
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/discord/models/domain/ModelPremiumGuildSubscription;

    .line 224
    sget-object v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;->Companion:Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem$Companion;

    .line 226
    invoke-virtual {p2}, Lcom/discord/models/domain/ModelPremiumGuildSubscription;->getGuildId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelGuild;

    .line 224
    invoke-virtual {v0, p2, v2}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem$Companion;->createBoostedGuildItem(Lcom/discord/models/domain/ModelPremiumGuildSubscription;Lcom/discord/models/domain/ModelGuild;)Lcom/discord/widgets/settings/nitro/WidgetSettingsBoostAdapter$BoostItem;

    move-result-object p2

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 229
    :cond_6
    check-cast v1, Ljava/util/List;

    return-object v1
.end method


# virtual methods
.method public final get()Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model;",
            ">;"
        }
    .end annotation

    .line 147
    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    .line 148
    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPremiumGuildSubscriptions()Lcom/discord/stores/StorePremiumGuildSubscription;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 149
    invoke-static {v0, v1, v2, v1}, Lcom/discord/stores/StorePremiumGuildSubscription;->getPremiumGuildSubscriptionsState$default(Lcom/discord/stores/StorePremiumGuildSubscription;Ljava/lang/Long;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    .line 150
    sget-object v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Companion$get$1;->INSTANCE:Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Companion$get$1;

    check-cast v1, Lrx/functions/b;

    invoke-virtual {v0, v1}, Lrx/Observable;->g(Lrx/functions/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "StoreStream\n            \u2026      }\n                }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    .line 167
    invoke-virtual {v0}, Lrx/Observable;->JL()Lrx/Observable;

    move-result-object v0

    .line 168
    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    .line 169
    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getSubscriptions()Lcom/discord/stores/StoreSubscriptions;

    move-result-object v1

    .line 170
    invoke-virtual {v1}, Lcom/discord/stores/StoreSubscriptions;->getSubscriptions()Lrx/Observable;

    move-result-object v1

    .line 171
    sget-object v2, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Companion$get$2;->INSTANCE:Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Companion$get$2;

    check-cast v2, Lrx/functions/Func2;

    .line 146
    invoke-static {v0, v1, v2}, Lrx/Observable;->a(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026ure\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
