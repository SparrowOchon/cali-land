.class final Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Companion$get$2;
.super Ljava/lang/Object;
.source "WidgetSettingsBoost.kt"

# interfaces
.implements Lrx/functions/Func2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Companion;->get()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func2<",
        "TT1;TT2;TR;>;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Companion$get$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Companion$get$2;

    invoke-direct {v0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Companion$get$2;-><init>()V

    sput-object v0, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Companion$get$2;->INSTANCE:Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Companion$get$2;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lkotlin/Pair;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+",
            "Lcom/discord/stores/StorePremiumGuildSubscription$State;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;>;",
            "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;",
            ")",
            "Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model;"
        }
    .end annotation

    .line 1000
    iget-object v0, p1, Lkotlin/Pair;->first:Ljava/lang/Object;

    .line 172
    check-cast v0, Lcom/discord/stores/StorePremiumGuildSubscription$State;

    .line 2000
    iget-object p1, p1, Lkotlin/Pair;->second:Ljava/lang/Object;

    .line 172
    check-cast p1, Ljava/util/Map;

    .line 175
    instance-of v1, v0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loading;

    if-nez v1, :cond_a

    .line 176
    instance-of v1, p2, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loading;

    if-eqz v1, :cond_0

    goto/16 :goto_4

    .line 179
    :cond_0
    instance-of v1, v0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Failure;

    if-nez v1, :cond_9

    .line 180
    instance-of v1, p2, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Failure;

    if-eqz v1, :cond_1

    goto/16 :goto_3

    .line 183
    :cond_1
    instance-of v1, v0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    if-eqz v1, :cond_8

    .line 184
    instance-of v1, p2, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;

    if-eqz v1, :cond_8

    .line 185
    sget-object v1, Lcom/discord/utilities/nitro/BoostUtils;->INSTANCE:Lcom/discord/utilities/nitro/BoostUtils;

    check-cast p2, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;

    invoke-virtual {p2}, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;->getSubscriptions()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/discord/utilities/nitro/BoostUtils;->calculateTotalBoostCount(Ljava/util/List;)I

    move-result v1

    if-lez v1, :cond_2

    .line 186
    new-instance v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Loaded;

    sget-object v2, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model;->Companion:Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Companion;

    check-cast v0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    invoke-static {v2, v0, p2, p1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Companion;->access$createBoostItems(Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Companion;Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;Ljava/util/Map;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Loaded;-><init>(Ljava/util/List;)V

    check-cast v1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model;

    return-object v1

    .line 188
    :cond_2
    invoke-virtual {p2}, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;->getSubscriptions()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 247
    instance-of p2, p1, Ljava/util/Collection;

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p2, :cond_3

    move-object p2, p1

    check-cast p2, Ljava/util/Collection;

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_7

    .line 248
    :cond_3
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/discord/models/domain/ModelSubscription;

    .line 189
    invoke-virtual {p2}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object v2

    sget-object v3, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_MONTH_TIER_1:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    if-eq v2, v3, :cond_6

    .line 190
    invoke-virtual {p2}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object p2

    sget-object v2, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_YEAR_TIER_1:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    if-ne p2, v2, :cond_5

    goto :goto_0

    :cond_5
    const/4 p2, 0x0

    goto :goto_1

    :cond_6
    :goto_0
    const/4 p2, 0x1

    :goto_1
    if-eqz p2, :cond_4

    goto :goto_2

    :cond_7
    const/4 v0, 0x0

    .line 188
    :goto_2
    new-instance p1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$LoadedNoNitro;

    invoke-direct {p1, v0}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$LoadedNoNitro;-><init>(Z)V

    check-cast p1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model;

    return-object p1

    .line 194
    :cond_8
    sget-object p1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Failure;->INSTANCE:Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Failure;

    check-cast p1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model;

    return-object p1

    .line 181
    :cond_9
    :goto_3
    sget-object p1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Failure;->INSTANCE:Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Failure;

    check-cast p1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model;

    return-object p1

    .line 177
    :cond_a
    :goto_4
    sget-object p1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Loading;->INSTANCE:Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Loading;

    check-cast p1, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model;

    return-object p1
.end method

.method public final bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 143
    check-cast p1, Lkotlin/Pair;

    check-cast p2, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model$Companion$get$2;->call(Lkotlin/Pair;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)Lcom/discord/widgets/settings/nitro/WidgetSettingsBoost$Model;

    move-result-object p1

    return-object p1
.end method
