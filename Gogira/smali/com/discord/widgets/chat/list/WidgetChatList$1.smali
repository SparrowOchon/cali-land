.class Lcom/discord/widgets/chat/list/WidgetChatList$1;
.super Ljava/lang/Object;
.source "WidgetChatList.java"

# interfaces
.implements Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/list/WidgetChatList;->createAdapter(Lcom/discord/stores/StoreChat;)Lcom/discord/widgets/chat/list/WidgetChatListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

.field final synthetic val$chat:Lcom/discord/stores/StoreChat;


# direct methods
.method constructor <init>(Lcom/discord/widgets/chat/list/WidgetChatList;Lcom/discord/stores/StoreChat;)V
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    iput-object p2, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->val$chat:Lcom/discord/stores/StoreChat;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$null$0(Ljava/lang/ref/WeakReference;Ljava/lang/String;)Lkotlin/Unit;
    .locals 3

    .line 221
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/content/Context;

    if-eqz p0, :cond_0

    const v0, 0x7f120538

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 223
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/discord/app/h;->b(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 225
    :cond_0
    sget-object p0, Lkotlin/Unit;->bjS:Lkotlin/Unit;

    return-object p0
.end method

.method static synthetic lambda$null$1(Ljava/lang/ref/WeakReference;Ljava/lang/Throwable;)Lkotlin/Unit;
    .locals 1

    .line 228
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "Could not download attachment due to:  \n"

    invoke-virtual {v0, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/discord/app/AppLog;->i(Ljava/lang/String;)V

    .line 229
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    if-eqz p1, :cond_0

    .line 231
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/content/Context;

    const v0, 0x7f120537

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/discord/app/h;->b(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 233
    :cond_0
    sget-object p0, Lkotlin/Unit;->bjS:Lkotlin/Unit;

    return-object p0
.end method

.method static synthetic lambda$onQuickDownloadClicked$2(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/ref/WeakReference;)V
    .locals 6

    .line 215
    new-instance v4, Lcom/discord/widgets/chat/list/-$$Lambda$WidgetChatList$1$BFhfRVg5QWfvFyc_TB_7fLYtgdc;

    invoke-direct {v4, p3}, Lcom/discord/widgets/chat/list/-$$Lambda$WidgetChatList$1$BFhfRVg5QWfvFyc_TB_7fLYtgdc;-><init>(Ljava/lang/ref/WeakReference;)V

    new-instance v5, Lcom/discord/widgets/chat/list/-$$Lambda$WidgetChatList$1$qlV9j-upun44sMRPg_corjHzRE0;

    invoke-direct {v5, p3}, Lcom/discord/widgets/chat/list/-$$Lambda$WidgetChatList$1$qlV9j-upun44sMRPg_corjHzRE0;-><init>(Ljava/lang/ref/WeakReference;)V

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/io/NetworkUtils;->downloadFile(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method


# virtual methods
.method public onInteractionStateUpdated(Lcom/discord/stores/StoreChat$InteractionState;)V
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->val$chat:Lcom/discord/stores/StoreChat;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreChat;->setInteractionState(Lcom/discord/stores/StoreChat$InteractionState;)V

    return-void
.end method

.method public onMessageAuthorClicked(Lcom/discord/models/domain/ModelMessage;J)V
    .locals 1

    .line 177
    invoke-static {}, Lcom/discord/stores/StoreStream;->getChat()Lcom/discord/stores/StoreChat;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getAuthor()Lcom/discord/models/domain/ModelUser;

    move-result-object p1

    invoke-virtual {v0, p1, p2, p3}, Lcom/discord/stores/StoreChat;->appendMention(Lcom/discord/models/domain/ModelUser;J)V

    return-void
.end method

.method public onMessageAuthorLongClicked(Lcom/discord/models/domain/ModelMessage;Ljava/lang/Long;)V
    .locals 7

    .line 182
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getAuthor()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v3

    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/WidgetChatList;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v5

    move-object v6, p2

    invoke-static/range {v1 .. v6}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->show(JJLandroidx/fragment/app/FragmentManager;Ljava/lang/Long;)V

    return-void
.end method

.method public onMessageBlockedGroupClicked(Lcom/discord/models/domain/ModelMessage;)V
    .locals 3

    .line 187
    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->val$chat:Lcom/discord/stores/StoreChat;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreChat;->toggleBlockedMessageGroup(J)V

    return-void
.end method

.method public onMessageClicked(Lcom/discord/models/domain/ModelMessage;)V
    .locals 0

    return-void
.end method

.method public onMessageLongClicked(Lcom/discord/models/domain/ModelMessage;Ljava/lang/CharSequence;)V
    .locals 6

    .line 164
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v3

    .line 165
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v1

    .line 167
    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/WidgetChatList;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/discord/widgets/chat/list/actions/WidgetChatListActions;->showForChat(Landroidx/fragment/app/FragmentManager;JJLjava/lang/CharSequence;)V

    return-void
.end method

.method public onOldestMessageId(JJ)V
    .locals 0

    return-void
.end method

.method public onQuickAddReactionClicked(JJJ)V
    .locals 0

    .line 197
    new-instance p1, Ljava/util/HashMap;

    const/4 p2, 0x2

    invoke-direct {p1, p2}, Ljava/util/HashMap;-><init>(I)V

    .line 198
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    const-string p3, "channelId"

    invoke-virtual {p1, p3, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    const-string p3, "messageId"

    invoke-virtual {p1, p3, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    iget-object p2, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-static {p2, p1}, Lcom/discord/widgets/chat/input/emoji/WidgetChatInputEmojiPicker;->launchFullscreen(Landroidx/fragment/app/Fragment;Ljava/util/HashMap;)V

    return-void
.end method

.method public onQuickDownloadClicked(Landroid/net/Uri;Ljava/lang/String;)Z
    .locals 4

    .line 208
    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/WidgetChatList;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 213
    :cond_0
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 215
    iget-object v2, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    new-instance v3, Lcom/discord/widgets/chat/list/-$$Lambda$WidgetChatList$1$McoGlmrS2EJUEPJmLY2Zd8DNF08;

    invoke-direct {v3, v0, p1, p2, v1}, Lcom/discord/widgets/chat/list/-$$Lambda$WidgetChatList$1$McoGlmrS2EJUEPJmLY2Zd8DNF08;-><init>(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/ref/WeakReference;)V

    invoke-virtual {v2, v3}, Lcom/discord/widgets/chat/list/WidgetChatList;->requestMediaDownload(Lrx/functions/Action0;)V

    const/4 p1, 0x1

    return p1
.end method

.method public onReactionClicked(JJJLcom/discord/models/domain/ModelMessageReaction;)V
    .locals 8

    .line 192
    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-static {v0}, Lcom/discord/widgets/chat/list/WidgetChatList;->access$000(Lcom/discord/widgets/chat/list/WidgetChatList;)Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;

    move-result-object v1

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-wide v2, p1

    move-wide v4, p3

    move-object v7, p7

    invoke-virtual/range {v1 .. v7}, Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;->toggleReaction(JJLjava/lang/Long;Lcom/discord/models/domain/ModelMessageReaction;)V

    return-void
.end method

.method public onUrlLongClicked(Ljava/lang/String;)V
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/WidgetChatList;->requireFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/discord/widgets/chat/WidgetUrlActions;->launch(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method
