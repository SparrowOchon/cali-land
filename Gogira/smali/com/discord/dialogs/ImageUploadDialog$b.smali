.class final Lcom/discord/dialogs/ImageUploadDialog$b;
.super Ljava/lang/Object;
.source "ImageUploadDialog.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/dialogs/ImageUploadDialog;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation


# instance fields
.field final synthetic wE:Lcom/discord/dialogs/ImageUploadDialog;


# direct methods
.method constructor <init>(Lcom/discord/dialogs/ImageUploadDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/dialogs/ImageUploadDialog$b;->wE:Lcom/discord/dialogs/ImageUploadDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    .line 37
    iget-object p1, p0, Lcom/discord/dialogs/ImageUploadDialog$b;->wE:Lcom/discord/dialogs/ImageUploadDialog;

    invoke-virtual {p1}, Lcom/discord/dialogs/ImageUploadDialog;->getContext()Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/dialogs/ImageUploadDialog$b;->wE:Lcom/discord/dialogs/ImageUploadDialog;

    invoke-virtual {v0}, Lcom/discord/dialogs/ImageUploadDialog;->getUri()Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/dialogs/ImageUploadDialog$b;->wE:Lcom/discord/dialogs/ImageUploadDialog;

    .line 1026
    iget-object v1, v1, Lcom/discord/dialogs/ImageUploadDialog;->mimeType:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string v2, "mimeType"

    invoke-static {v2}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    .line 37
    :cond_0
    iget-object v2, p0, Lcom/discord/dialogs/ImageUploadDialog$b;->wE:Lcom/discord/dialogs/ImageUploadDialog;

    .line 1027
    iget-object v2, v2, Lcom/discord/dialogs/ImageUploadDialog;->wx:Lrx/functions/Action1;

    .line 37
    invoke-static {p1, v0, v1, v2}, Lcom/discord/utilities/images/MGImages;->requestDataUrl(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Lrx/functions/Action1;)V

    .line 38
    iget-object p1, p0, Lcom/discord/dialogs/ImageUploadDialog$b;->wE:Lcom/discord/dialogs/ImageUploadDialog;

    invoke-virtual {p1}, Lcom/discord/dialogs/ImageUploadDialog;->dismiss()V

    return-void
.end method
