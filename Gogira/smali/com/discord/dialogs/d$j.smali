.class final Lcom/discord/dialogs/d$j;
.super Lkotlin/jvm/internal/l;
.source "WidgetGiftAcceptDialog.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/dialogs/d;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/l;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/discord/stores/StoreGifting$GiftState;",
        "+",
        "Lcom/discord/models/domain/ModelUser;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/discord/dialogs/d;


# direct methods
.method constructor <init>(Lcom/discord/dialogs/d;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/dialogs/d$j;->this$0:Lcom/discord/dialogs/d;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/l;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .line 30
    check-cast p1, Lkotlin/Pair;

    .line 1000
    iget-object v0, p1, Lkotlin/Pair;->first:Ljava/lang/Object;

    .line 30
    check-cast v0, Lcom/discord/stores/StoreGifting$GiftState;

    .line 2000
    iget-object p1, p1, Lkotlin/Pair;->second:Ljava/lang/Object;

    .line 30
    check-cast p1, Lcom/discord/models/domain/ModelUser;

    .line 2064
    instance-of v1, v0, Lcom/discord/stores/StoreGifting$GiftState$Resolved;

    if-eqz v1, :cond_3

    .line 2066
    check-cast v0, Lcom/discord/stores/StoreGifting$GiftState$Resolved;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGifting$GiftState$Resolved;->getGift()Lcom/discord/models/domain/ModelGift;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGift;->isAnyNitroGift()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/discord/utilities/nitro/NitroUtils;->INSTANCE:Lcom/discord/utilities/nitro/NitroUtils;

    const-string v2, "me"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/discord/stores/StoreGifting$GiftState$Resolved;->getGift()Lcom/discord/models/domain/ModelGift;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/discord/utilities/nitro/NitroUtils;->isAcceptableNitroGift(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGift;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/discord/dialogs/d$j;->this$0:Lcom/discord/dialogs/d;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGifting$GiftState$Resolved;->getGift()Lcom/discord/models/domain/ModelGift;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/discord/dialogs/d;->a(Lcom/discord/dialogs/d;Lcom/discord/models/domain/ModelGift;)V

    goto :goto_0

    .line 2067
    :cond_0
    invoke-virtual {v0}, Lcom/discord/stores/StoreGifting$GiftState$Resolved;->getGift()Lcom/discord/models/domain/ModelGift;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGift;->getRedeemed()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/dialogs/d$j;->this$0:Lcom/discord/dialogs/d;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGifting$GiftState$Resolved;->getGift()Lcom/discord/models/domain/ModelGift;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/discord/dialogs/d;->b(Lcom/discord/dialogs/d;Lcom/discord/models/domain/ModelGift;)V

    goto :goto_0

    .line 2068
    :cond_1
    invoke-virtual {v0}, Lcom/discord/stores/StoreGifting$GiftState$Resolved;->getGift()Lcom/discord/models/domain/ModelGift;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGift;->getMaxUses()I

    move-result p1

    invoke-virtual {v0}, Lcom/discord/stores/StoreGifting$GiftState$Resolved;->getGift()Lcom/discord/models/domain/ModelGift;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGift;->getUses()I

    move-result v1

    if-ne p1, v1, :cond_2

    iget-object p1, p0, Lcom/discord/dialogs/d$j;->this$0:Lcom/discord/dialogs/d;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGifting$GiftState$Resolved;->getGift()Lcom/discord/models/domain/ModelGift;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/discord/dialogs/d;->c(Lcom/discord/dialogs/d;Lcom/discord/models/domain/ModelGift;)V

    goto :goto_0

    .line 2069
    :cond_2
    iget-object p1, p0, Lcom/discord/dialogs/d$j;->this$0:Lcom/discord/dialogs/d;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGifting$GiftState$Resolved;->getGift()Lcom/discord/models/domain/ModelGift;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/discord/dialogs/d;->d(Lcom/discord/dialogs/d;Lcom/discord/models/domain/ModelGift;)V

    goto :goto_0

    .line 2072
    :cond_3
    instance-of p1, v0, Lcom/discord/stores/StoreGifting$GiftState$Redeeming;

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/discord/dialogs/d$j;->this$0:Lcom/discord/dialogs/d;

    check-cast v0, Lcom/discord/stores/StoreGifting$GiftState$Redeeming;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGifting$GiftState$Redeeming;->getGift()Lcom/discord/models/domain/ModelGift;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/discord/dialogs/d;->e(Lcom/discord/dialogs/d;Lcom/discord/models/domain/ModelGift;)V

    goto :goto_0

    .line 2073
    :cond_4
    instance-of p1, v0, Lcom/discord/stores/StoreGifting$GiftState$RedeemedFailed;

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/discord/dialogs/d$j;->this$0:Lcom/discord/dialogs/d;

    check-cast v0, Lcom/discord/stores/StoreGifting$GiftState$RedeemedFailed;

    invoke-static {p1, v0}, Lcom/discord/dialogs/d;->a(Lcom/discord/dialogs/d;Lcom/discord/stores/StoreGifting$GiftState$RedeemedFailed;)V

    goto :goto_0

    .line 2074
    :cond_5
    iget-object p1, p0, Lcom/discord/dialogs/d$j;->this$0:Lcom/discord/dialogs/d;

    invoke-virtual {p1}, Lcom/discord/dialogs/d;->dismiss()V

    .line 30
    :goto_0
    sget-object p1, Lkotlin/Unit;->bjS:Lkotlin/Unit;

    return-object p1
.end method
