.class final Lcom/discord/dialogs/d$a$a;
.super Lkotlin/jvm/internal/l;
.source "WidgetGiftAcceptDialog.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/dialogs/d$a;->a(Ljava/lang/String;Ljava/lang/String;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/l;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroidx/fragment/app/FragmentActivity;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic $channelId:J

.field final synthetic $giftCode:Ljava/lang/String;

.field final synthetic $source:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0

    iput-object p1, p0, Lcom/discord/dialogs/d$a$a;->$giftCode:Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/dialogs/d$a$a;->$source:Ljava/lang/String;

    iput-wide p3, p0, Lcom/discord/dialogs/d$a$a;->$channelId:J

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/l;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .line 236
    check-cast p1, Landroidx/fragment/app/FragmentActivity;

    const-string v0, "appActivity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1250
    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    .line 1251
    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getAnalytics()Lcom/discord/stores/StoreAnalytics;

    move-result-object v0

    .line 1252
    iget-object v1, p0, Lcom/discord/dialogs/d$a$a;->$giftCode:Ljava/lang/String;

    iget-object v2, p0, Lcom/discord/dialogs/d$a$a;->$source:Ljava/lang/String;

    iget-wide v3, p0, Lcom/discord/dialogs/d$a$a;->$channelId:J

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/discord/stores/StoreAnalytics;->trackOpenGiftAcceptModal(Ljava/lang/String;Ljava/lang/String;J)V

    .line 1254
    new-instance v0, Lcom/discord/dialogs/d;

    invoke-direct {v0}, Lcom/discord/dialogs/d;-><init>()V

    .line 1255
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/discord/dialogs/d$a$a;->$giftCode:Ljava/lang/String;

    const-string v3, "ARG_GIFT_CODE"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/dialogs/d;->setArguments(Landroid/os/Bundle;)V

    .line 1256
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    const-class v1, Lcom/discord/dialogs/d;

    invoke-static {v1}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/discord/dialogs/d;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 1257
    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object p1
.end method
