.class public final Lcom/discord/dialogs/b;
.super Lcom/discord/app/AppDialog;
.source "PremiumUpsellDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/dialogs/b$d;,
        Lcom/discord/dialogs/b$c;,
        Lcom/discord/dialogs/b$b;,
        Lcom/discord/dialogs/b$a;
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final wH:Lcom/discord/dialogs/b$a;


# instance fields
.field private final close$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final learnMore$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final viewPager$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final wF:Lkotlin/properties/ReadOnlyProperty;

.field private wG:Lcom/discord/dialogs/b$d;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-class v0, Lcom/discord/dialogs/b;

    const/4 v1, 0x4

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "viewPager"

    const-string v5, "getViewPager()Landroidx/viewpager/widget/ViewPager;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v4

    const-string v5, "tabDots"

    const-string v6, "getTabDots()Lcom/google/android/material/tabs/TabLayout;"

    invoke-direct {v2, v4, v5, v6}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v4

    const-string v5, "close"

    const-string v6, "getClose()Landroid/view/View;"

    invoke-direct {v2, v4, v5, v6}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v4, 0x2

    aput-object v2, v1, v4

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v0

    const-string v4, "learnMore"

    const-string v5, "getLearnMore()Landroid/view/View;"

    invoke-direct {v2, v0, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aput-object v0, v1, v2

    sput-object v1, Lcom/discord/dialogs/b;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/dialogs/b$a;

    invoke-direct {v0, v3}, Lcom/discord/dialogs/b$a;-><init>(B)V

    sput-object v0, Lcom/discord/dialogs/b;->wH:Lcom/discord/dialogs/b$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 22
    invoke-direct {p0}, Lcom/discord/app/AppDialog;-><init>()V

    const v0, 0x7f0a052d

    .line 24
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/dialogs/b;->viewPager$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0529

    .line 25
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/dialogs/b;->wF:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0528

    .line 27
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/dialogs/b;->close$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a052c

    .line 28
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/dialogs/b;->learnMore$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final a(Landroidx/fragment/app/FragmentManager;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-static/range {v0 .. v7}, Lcom/discord/dialogs/b$a;->a(Landroidx/fragment/app/FragmentManager;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private final dR()Landroidx/viewpager/widget/ViewPager;
    .locals 3

    iget-object v0, p0, Lcom/discord/dialogs/b;->viewPager$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/dialogs/b;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/viewpager/widget/ViewPager;

    return-object v0
.end method


# virtual methods
.method public final getContentViewResId()I
    .locals 1

    const v0, 0x7f0d005b

    return v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 11

    .line 35
    invoke-super {p0, p1}, Lcom/discord/app/AppDialog;->onCreate(Landroid/os/Bundle;)V

    .line 37
    invoke-virtual {p0}, Lcom/discord/dialogs/b;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    const/4 v0, 0x4

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {p1, v2, v1, v0, v1}, Lcom/discord/utilities/keyboard/Keyboard;->setKeyboardOpen$default(Landroid/app/Activity;ZLandroid/view/View;ILjava/lang/Object;)V

    const/4 p1, 0x6

    new-array p1, p1, [Lcom/discord/dialogs/b$b;

    .line 40
    new-instance v1, Lcom/discord/dialogs/b$b;

    const v3, 0x7f120e71

    invoke-virtual {p0, v3}, Lcom/discord/dialogs/b;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "getString(R.string.premi\u2026psell_tag_passive_mobile)"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const v4, 0x7f120e70

    invoke-virtual {p0, v4}, Lcom/discord/dialogs/b;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "getString(R.string.premi\u2026l_tag_description_mobile)"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f080430

    invoke-direct {v1, v5, v3, v4}, Lcom/discord/dialogs/b$b;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    aput-object v1, p1, v2

    .line 41
    new-instance v1, Lcom/discord/dialogs/b$b;

    const v3, 0x7f120e6e

    invoke-virtual {p0, v3}, Lcom/discord/dialogs/b;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "getString(R.string.premi\u2026ell_emoji_passive_mobile)"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const v4, 0x7f120e6d

    invoke-virtual {p0, v4}, Lcom/discord/dialogs/b;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "getString(R.string.premi\u2026emoji_description_mobile)"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f08040c

    invoke-direct {v1, v5, v3, v4}, Lcom/discord/dialogs/b$b;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x1

    aput-object v1, p1, v3

    .line 42
    new-instance v1, Lcom/discord/dialogs/b$b;

    const v4, 0x7f120e68

    invoke-virtual {p0, v4}, Lcom/discord/dialogs/b;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "getString(R.string.premi\u2026ed_emojis_passive_mobile)"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f120e67

    invoke-virtual {p0, v5}, Lcom/discord/dialogs/b;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "getString(R.string.premi\u2026mojis_description_mobile)"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const v6, 0x7f0803e8

    invoke-direct {v1, v6, v4, v5}, Lcom/discord/dialogs/b$b;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x2

    aput-object v1, p1, v4

    .line 43
    new-instance v1, Lcom/discord/dialogs/b$b;

    const v5, 0x7f120e74

    invoke-virtual {p0, v5}, Lcom/discord/dialogs/b;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "getString(R.string.premi\u2026ll_upload_passive_mobile)"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x3

    new-array v7, v6, [Ljava/lang/Object;

    const v8, 0x7f1205a7

    invoke-virtual {p0, v8}, Lcom/discord/dialogs/b;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v2

    const v8, 0x7f1205a6

    invoke-virtual {p0, v8}, Lcom/discord/dialogs/b;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v3

    const v8, 0x7f1205a5

    invoke-virtual {p0, v8}, Lcom/discord/dialogs/b;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v4

    const v4, 0x7f120e73

    invoke-virtual {p0, v4, v7}, Lcom/discord/dialogs/b;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v7, "getString(R.string.premi\u2026ad_limit_premium_tier_1))"

    invoke-static {v4, v7}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const v7, 0x7f080431

    invoke-direct {v1, v7, v5, v4}, Lcom/discord/dialogs/b$b;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    aput-object v1, p1, v6

    .line 44
    new-instance v1, Lcom/discord/dialogs/b$b;

    const v4, 0x7f120e65

    invoke-virtual {p0, v4}, Lcom/discord/dialogs/b;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "getString(R.string.premi\u2026ed_avatar_passive_mobile)"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f120e64

    invoke-virtual {p0, v5}, Lcom/discord/dialogs/b;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "getString(R.string.premi\u2026vatar_description_mobile)"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const v6, 0x7f0803f1

    invoke-direct {v1, v6, v4, v5}, Lcom/discord/dialogs/b$b;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    aput-object v1, p1, v0

    .line 45
    new-instance v0, Lcom/discord/dialogs/b$b;

    const v1, 0x7f120e6b

    invoke-virtual {p0, v1}, Lcom/discord/dialogs/b;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v4, "getString(R.string.premi\u2026ell_badge_passive_mobile)"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const v4, 0x7f120e6a

    invoke-virtual {p0, v4}, Lcom/discord/dialogs/b;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "getString(R.string.premi\u2026badge_description_mobile)"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f0803f4

    invoke-direct {v0, v5, v1, v4}, Lcom/discord/dialogs/b$b;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x5

    aput-object v0, p1, v1

    .line 39
    invoke-static {p1}, Lkotlin/a/m;->k([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 48
    invoke-virtual {p0}, Lcom/discord/dialogs/b;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "extra_page_number"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 49
    :goto_0
    invoke-virtual {p0}, Lcom/discord/dialogs/b;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v4, "<set-?>"

    const-string v5, ""

    const-string v6, "extra_header_string"

    if-eqz v1, :cond_3

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-ne v1, v3, :cond_3

    .line 50
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/dialogs/b$b;

    invoke-virtual {p0}, Lcom/discord/dialogs/b;->getArguments()Landroid/os/Bundle;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-virtual {v7, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_2

    :cond_1
    move-object v7, v5

    :cond_2
    invoke-static {v7, v4}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1127
    iput-object v7, v1, Lcom/discord/dialogs/b$b;->headerText:Ljava/lang/String;

    .line 52
    :cond_3
    invoke-virtual {p0}, Lcom/discord/dialogs/b;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v7, "extra_body_text"

    if-eqz v1, :cond_6

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-ne v1, v3, :cond_6

    .line 53
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/dialogs/b$b;

    invoke-virtual {p0}, Lcom/discord/dialogs/b;->getArguments()Landroid/os/Bundle;

    move-result-object v8

    if-eqz v8, :cond_5

    invoke-virtual {v8, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_4

    goto :goto_1

    :cond_4
    move-object v5, v8

    :cond_5
    :goto_1
    invoke-static {v5, v4}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2127
    iput-object v5, v1, Lcom/discord/dialogs/b$b;->bodyText:Ljava/lang/String;

    .line 56
    :cond_6
    new-instance v1, Lcom/discord/dialogs/b$d;

    invoke-virtual {p0}, Lcom/discord/dialogs/b;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v4

    const-string v5, "childFragmentManager"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v4}, Lcom/discord/dialogs/b$d;-><init>(Landroidx/fragment/app/FragmentManager;)V

    iput-object v1, p0, Lcom/discord/dialogs/b;->wG:Lcom/discord/dialogs/b$d;

    .line 57
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/dialogs/b$b;

    .line 59
    iget-object v4, p0, Lcom/discord/dialogs/b;->wG:Lcom/discord/dialogs/b$d;

    if-nez v4, :cond_7

    const-string v5, "pagerAdapter"

    invoke-static {v5}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_7
    invoke-static {v1}, Lkotlin/a/m;->ba(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    check-cast p1, Ljava/lang/Iterable;

    .line 170
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    check-cast v5, Ljava/util/Collection;

    .line 181
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v8, 0x0

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_b

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    add-int/lit8 v10, v8, 0x1

    if-gez v8, :cond_8

    invoke-static {}, Lkotlin/a/m;->DK()V

    :cond_8
    if-eq v8, v0, :cond_9

    const/4 v8, 0x1

    goto :goto_3

    :cond_9
    const/4 v8, 0x0

    :goto_3
    if-eqz v8, :cond_a

    .line 59
    invoke-interface {v5, v9}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_a
    move v8, v10

    goto :goto_2

    .line 185
    :cond_b
    check-cast v5, Ljava/util/List;

    check-cast v5, Ljava/lang/Iterable;

    .line 59
    invoke-static {v1, v5}, Lkotlin/a/m;->b(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 186
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 187
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 188
    check-cast v1, Lcom/discord/dialogs/b$b;

    .line 60
    sget-object v2, Lcom/discord/dialogs/b$c;->wJ:Lcom/discord/dialogs/b$c$a;

    .line 3127
    iget v2, v1, Lcom/discord/dialogs/b$b;->wI:I

    .line 4127
    iget-object v3, v1, Lcom/discord/dialogs/b$b;->headerText:Ljava/lang/String;

    .line 5127
    iget-object v1, v1, Lcom/discord/dialogs/b$b;->bodyText:Ljava/lang/String;

    const-string v5, "headerText"

    .line 60
    invoke-static {v3, v5}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "bodyText"

    invoke-static {v1, v5}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6116
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    const-string v8, "extra_image_id"

    .line 6117
    invoke-virtual {v5, v8, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 6118
    invoke-virtual {v5, v6, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 6119
    invoke-virtual {v5, v7, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 6122
    new-instance v1, Lcom/discord/dialogs/b$c;

    invoke-direct {v1}, Lcom/discord/dialogs/b$c;-><init>()V

    invoke-virtual {v1, v5}, Lcom/discord/dialogs/b$c;->setArguments(Landroid/os/Bundle;)V

    .line 60
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 189
    :cond_c
    check-cast v0, Ljava/util/List;

    .line 7089
    iput-object v0, v4, Lcom/discord/dialogs/b$d;->wK:Ljava/util/List;

    return-void
.end method

.method public final onViewBound(Landroid/view/View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-super {p0, p1}, Lcom/discord/app/AppDialog;->onViewBound(Landroid/view/View;)V

    .line 67
    invoke-direct {p0}, Lcom/discord/dialogs/b;->dR()Landroidx/viewpager/widget/ViewPager;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/dialogs/b;->wG:Lcom/discord/dialogs/b$d;

    if-nez v0, :cond_0

    const-string v1, "pagerAdapter"

    invoke-static {v1}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroidx/viewpager/widget/PagerAdapter;

    invoke-virtual {p1, v0}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    .line 8000
    iget-object p1, p0, Lcom/discord/dialogs/b;->wF:Lkotlin/properties/ReadOnlyProperty;

    sget-object v0, Lcom/discord/dialogs/b;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-interface {p1, p0, v0}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/android/material/tabs/TabLayout;

    .line 69
    invoke-direct {p0}, Lcom/discord/dialogs/b;->dR()Landroidx/viewpager/widget/ViewPager;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/material/tabs/TabLayout;->setupWithViewPager(Landroidx/viewpager/widget/ViewPager;)V

    .line 9000
    iget-object p1, p0, Lcom/discord/dialogs/b;->close$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v0, Lcom/discord/dialogs/b;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-interface {p1, p0, v0}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    .line 71
    new-instance v0, Lcom/discord/dialogs/b$e;

    invoke-direct {v0, p0}, Lcom/discord/dialogs/b$e;-><init>(Lcom/discord/dialogs/b;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 10000
    iget-object p1, p0, Lcom/discord/dialogs/b;->learnMore$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v0, Lcom/discord/dialogs/b;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    invoke-interface {p1, p0, v0}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    .line 73
    new-instance v0, Lcom/discord/dialogs/b$f;

    invoke-direct {v0, p0}, Lcom/discord/dialogs/b$f;-><init>(Lcom/discord/dialogs/b;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
