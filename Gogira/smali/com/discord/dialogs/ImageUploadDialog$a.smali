.class public final Lcom/discord/dialogs/ImageUploadDialog$a;
.super Ljava/lang/Object;
.source "ImageUploadDialog.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/dialogs/ImageUploadDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(B)V
    .locals 0

    .line 85
    invoke-direct {p0}, Lcom/discord/dialogs/ImageUploadDialog$a;-><init>()V

    return-void
.end method

.method public static a(Landroidx/fragment/app/FragmentManager;Landroid/net/Uri;Lcom/miguelgaeta/media_picker/MediaPicker$Provider;Ljava/lang/String;Lrx/functions/Action1;Lcom/discord/dialogs/ImageUploadDialog$PreviewType;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/FragmentManager;",
            "Landroid/net/Uri;",
            "Lcom/miguelgaeta/media_picker/MediaPicker$Provider;",
            "Ljava/lang/String;",
            "Lrx/functions/Action1<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/discord/dialogs/ImageUploadDialog$PreviewType;",
            ")V"
        }
    .end annotation

    const-string v0, "fragmentManager"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "uri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "provider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mimeType"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "previewType"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    new-instance v0, Lcom/discord/dialogs/ImageUploadDialog;

    invoke-direct {v0}, Lcom/discord/dialogs/ImageUploadDialog;-><init>()V

    const-string v1, "<set-?>"

    .line 96
    invoke-static {p1, v1}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1024
    iput-object p1, v0, Lcom/discord/dialogs/ImageUploadDialog;->uri:Landroid/net/Uri;

    .line 97
    invoke-static {p2, v1}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1025
    iput-object p2, v0, Lcom/discord/dialogs/ImageUploadDialog;->ww:Lcom/miguelgaeta/media_picker/MediaPicker$Provider;

    .line 98
    invoke-static {p3, v1}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1026
    iput-object p3, v0, Lcom/discord/dialogs/ImageUploadDialog;->mimeType:Ljava/lang/String;

    .line 1027
    iput-object p4, v0, Lcom/discord/dialogs/ImageUploadDialog;->wx:Lrx/functions/Action1;

    .line 1028
    iput-object p5, v0, Lcom/discord/dialogs/ImageUploadDialog;->wy:Lcom/discord/dialogs/ImageUploadDialog$PreviewType;

    .line 102
    const-class p1, Lcom/discord/dialogs/ImageUploadDialog;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    const-string p2, "ImageUploadDialog::class.java.name"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p0, p1}, Lcom/discord/dialogs/ImageUploadDialog;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method
