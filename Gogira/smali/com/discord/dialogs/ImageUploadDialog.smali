.class public final Lcom/discord/dialogs/ImageUploadDialog;
.super Lcom/discord/app/AppDialog;
.source "ImageUploadDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/dialogs/ImageUploadDialog$PreviewType;,
        Lcom/discord/dialogs/ImageUploadDialog$a;
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final wz:Lcom/discord/dialogs/ImageUploadDialog$a;


# instance fields
.field private final cancel$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final image$delegate:Lkotlin/properties/ReadOnlyProperty;

.field public mimeType:Ljava/lang/String;

.field public uri:Landroid/net/Uri;

.field private final wu:Lkotlin/properties/ReadOnlyProperty;

.field private final wv:Lkotlin/properties/ReadOnlyProperty;

.field public ww:Lcom/miguelgaeta/media_picker/MediaPicker$Provider;

.field wx:Lrx/functions/Action1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Action1<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field wy:Lcom/discord/dialogs/ImageUploadDialog$PreviewType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-class v0, Lcom/discord/dialogs/ImageUploadDialog;

    const/4 v1, 0x4

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v3

    const-string v4, "upload"

    const-string v5, "getUpload()Landroid/view/View;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v4

    const-string v5, "crop"

    const-string v6, "getCrop()Landroid/view/View;"

    invoke-direct {v2, v4, v5, v6}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v4

    const-string v5, "cancel"

    const-string v6, "getCancel()Landroid/view/View;"

    invoke-direct {v2, v4, v5, v6}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v4, 0x2

    aput-object v2, v1, v4

    new-instance v2, Lkotlin/jvm/internal/v;

    invoke-static {v0}, Lkotlin/jvm/internal/w;->Q(Ljava/lang/Class;)Lkotlin/reflect/b;

    move-result-object v0

    const-string v4, "image"

    const-string v5, "getImage()Lcom/facebook/drawee/view/SimpleDraweeView;"

    invoke-direct {v2, v0, v4, v5}, Lkotlin/jvm/internal/v;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/w;->a(Lkotlin/jvm/internal/u;)Lkotlin/reflect/d;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aput-object v0, v1, v2

    sput-object v1, Lcom/discord/dialogs/ImageUploadDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/dialogs/ImageUploadDialog$a;

    invoke-direct {v0, v3}, Lcom/discord/dialogs/ImageUploadDialog$a;-><init>(B)V

    sput-object v0, Lcom/discord/dialogs/ImageUploadDialog;->wz:Lcom/discord/dialogs/ImageUploadDialog$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 17
    invoke-direct {p0}, Lcom/discord/app/AppDialog;-><init>()V

    const v0, 0x7f0a049e

    .line 19
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/dialogs/ImageUploadDialog;->wu:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0499

    .line 20
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/dialogs/ImageUploadDialog;->wv:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0498

    .line 21
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/dialogs/ImageUploadDialog;->cancel$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a049c

    .line 22
    invoke-static {p0, v0}, Lkotterknife/b;->a(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/dialogs/ImageUploadDialog;->image$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method private final dQ()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/dialogs/ImageUploadDialog;->wv:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/dialogs/ImageUploadDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getImage()Lcom/facebook/drawee/view/SimpleDraweeView;
    .locals 3

    iget-object v0, p0, Lcom/discord/dialogs/ImageUploadDialog;->image$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/dialogs/ImageUploadDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/SimpleDraweeView;

    return-object v0
.end method


# virtual methods
.method public final getContentViewResId()I
    .locals 1

    const v0, 0x7f0d003a

    return v0
.end method

.method public final getUri()Landroid/net/Uri;
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/discord/dialogs/ImageUploadDialog;->uri:Landroid/net/Uri;

    if-nez v0, :cond_0

    const-string v1, "uri"

    invoke-static {v1}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final onViewBound(Landroid/view/View;)V
    .locals 13

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-super {p0, p1}, Lcom/discord/app/AppDialog;->onViewBound(Landroid/view/View;)V

    .line 34
    iget-object v0, p0, Lcom/discord/dialogs/ImageUploadDialog;->mimeType:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v1, "mimeType"

    invoke-static {v1}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_0
    const-string v1, "image/gif"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 1000
    iget-object v1, p0, Lcom/discord/dialogs/ImageUploadDialog;->wu:Lkotlin/properties/ReadOnlyProperty;

    sget-object v2, Lcom/discord/dialogs/ImageUploadDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-interface {v1, p0, v2}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 36
    new-instance v2, Lcom/discord/dialogs/ImageUploadDialog$b;

    invoke-direct {v2, p0}, Lcom/discord/dialogs/ImageUploadDialog$b;-><init>(Lcom/discord/dialogs/ImageUploadDialog;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    invoke-direct {p0}, Lcom/discord/dialogs/ImageUploadDialog;->dQ()Landroid/view/View;

    move-result-object v1

    xor-int/lit8 v2, v0, 0x1

    const/4 v3, 0x4

    invoke-static {v1, v2, v3}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setVisibilityBy(Landroid/view/View;ZI)V

    if-nez v0, :cond_1

    .line 43
    invoke-direct {p0}, Lcom/discord/dialogs/ImageUploadDialog;->dQ()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/discord/dialogs/ImageUploadDialog$c;

    invoke-direct {v1, p0}, Lcom/discord/dialogs/ImageUploadDialog$c;-><init>(Lcom/discord/dialogs/ImageUploadDialog;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2000
    :cond_1
    iget-object v0, p0, Lcom/discord/dialogs/ImageUploadDialog;->cancel$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/dialogs/ImageUploadDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 49
    new-instance v1, Lcom/discord/dialogs/ImageUploadDialog$d;

    invoke-direct {v1, p0}, Lcom/discord/dialogs/ImageUploadDialog$d;-><init>(Lcom/discord/dialogs/ImageUploadDialog;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    iget-object v0, p0, Lcom/discord/dialogs/ImageUploadDialog;->wy:Lcom/discord/dialogs/ImageUploadDialog$PreviewType;

    if-nez v0, :cond_2

    .line 55
    invoke-virtual {p0}, Lcom/discord/dialogs/ImageUploadDialog;->dismiss()V

    return-void

    .line 59
    :cond_2
    invoke-virtual {p0}, Lcom/discord/dialogs/ImageUploadDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2079
    iget v3, v0, Lcom/discord/dialogs/ImageUploadDialog$PreviewType;->previewSizeDimenId:I

    .line 59
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 60
    invoke-direct {p0}, Lcom/discord/dialogs/ImageUploadDialog;->getImage()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/drawee/view/SimpleDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 61
    iput v7, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 62
    iput v7, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 63
    invoke-direct {p0}, Lcom/discord/dialogs/ImageUploadDialog;->getImage()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/facebook/drawee/view/SimpleDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 65
    sget-object v1, Lcom/discord/dialogs/a;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/discord/dialogs/ImageUploadDialog$PreviewType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    if-eq v0, v2, :cond_3

    goto :goto_0

    .line 68
    :cond_3
    invoke-direct {p0}, Lcom/discord/dialogs/ImageUploadDialog;->getImage()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    int-to-float v2, v7

    const v3, 0x7f0402a6

    .line 71
    invoke-static {p1, v3}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    .line 68
    invoke-static {v0, v2, v1, p1}, Lcom/discord/utilities/images/MGImages;->setCornerRadius(Landroid/widget/ImageView;FZLjava/lang/Integer;)V

    .line 76
    :goto_0
    invoke-direct {p0}, Lcom/discord/dialogs/ImageUploadDialog;->getImage()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object p1

    move-object v4, p1

    check-cast v4, Landroid/widget/ImageView;

    iget-object p1, p0, Lcom/discord/dialogs/ImageUploadDialog;->uri:Landroid/net/Uri;

    if-nez p1, :cond_4

    const-string v0, "uri"

    invoke-static {v0}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x70

    const/4 v12, 0x0

    move v6, v7

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    return-void
.end method
