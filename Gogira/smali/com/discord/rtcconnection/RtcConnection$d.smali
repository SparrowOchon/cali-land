.class public final Lcom/discord/rtcconnection/RtcConnection$d;
.super Lkotlin/jvm/internal/l;
.source "RtcConnection.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/rtcconnection/RtcConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/l;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic $endpoint:Ljava/lang/String;

.field final synthetic $sslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

.field final synthetic $token:Ljava/lang/String;

.field final synthetic this$0:Lcom/discord/rtcconnection/RtcConnection;


# direct methods
.method public constructor <init>(Lcom/discord/rtcconnection/RtcConnection;Ljavax/net/ssl/SSLSocketFactory;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/rtcconnection/RtcConnection$d;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    iput-object p2, p0, Lcom/discord/rtcconnection/RtcConnection$d;->$sslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

    iput-object p3, p0, Lcom/discord/rtcconnection/RtcConnection$d;->$endpoint:Ljava/lang/String;

    iput-object p4, p0, Lcom/discord/rtcconnection/RtcConnection$d;->$token:Ljava/lang/String;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/l;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final synthetic invoke()Ljava/lang/Object;
    .locals 13

    .line 1097
    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection$d;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    invoke-static {v0}, Lcom/discord/rtcconnection/RtcConnection;->a(Lcom/discord/rtcconnection/RtcConnection;)Lcom/discord/utilities/networking/Backoff;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/utilities/networking/Backoff;->succeed()V

    .line 1099
    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection$d;->$sslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v0, :cond_0

    const-string v0, "wss"

    goto :goto_0

    :cond_0
    const-string v0, "ws"

    .line 1104
    :goto_0
    iget-object v1, p0, Lcom/discord/rtcconnection/RtcConnection$d;->$endpoint:Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ".gg"

    const-string v4, ".media"

    .line 2075
    invoke-static {v1, v3, v4, v2}, Lkotlin/text/l;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 1106
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "://"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, ":80"

    const-string v4, ":443"

    .line 3075
    invoke-static {v0, v3, v4, v2}, Lkotlin/text/l;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 1111
    iget-object v3, p0, Lcom/discord/rtcconnection/RtcConnection$d;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    invoke-static {v3}, Lcom/discord/rtcconnection/RtcConnection;->b(Lcom/discord/rtcconnection/RtcConnection;)Lcom/discord/rtcconnection/socket/a;

    move-result-object v3

    const/4 v4, 0x0

    if-eqz v3, :cond_1

    .line 4023
    iget-object v3, v3, Lcom/discord/rtcconnection/socket/a;->endpoint:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v3, v4

    .line 1111
    :goto_1
    invoke-static {v3, v6}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/discord/rtcconnection/RtcConnection$d;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    invoke-static {v3}, Lcom/discord/rtcconnection/RtcConnection;->b(Lcom/discord/rtcconnection/RtcConnection;)Lcom/discord/rtcconnection/socket/a;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 4024
    iget-object v4, v3, Lcom/discord/rtcconnection/socket/a;->token:Ljava/lang/String;

    .line 1111
    :cond_2
    iget-object v3, p0, Lcom/discord/rtcconnection/RtcConnection$d;->$token:Ljava/lang/String;

    invoke-static {v4, v3}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/discord/rtcconnection/RtcConnection$d;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    invoke-static {v3}, Lcom/discord/rtcconnection/RtcConnection;->c(Lcom/discord/rtcconnection/RtcConnection;)Lcom/discord/rtcconnection/RtcConnection$State;

    move-result-object v3

    instance-of v3, v3, Lcom/discord/rtcconnection/RtcConnection$State$d;

    if-nez v3, :cond_3

    .line 1112
    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection$d;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    invoke-static {v0}, Lcom/discord/rtcconnection/RtcConnection;->d(Lcom/discord/rtcconnection/RtcConnection;)Lcom/discord/utilities/logging/Logger;

    move-result-object v1

    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection$d;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    invoke-static {v0}, Lcom/discord/rtcconnection/RtcConnection;->e(Lcom/discord/rtcconnection/RtcConnection;)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    const-string v3, "Already connecting, no need to continue."

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/logging/Logger;->w$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    goto/16 :goto_3

    .line 1116
    :cond_3
    iget-object v3, p0, Lcom/discord/rtcconnection/RtcConnection$d;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    invoke-static {v3}, Lcom/discord/rtcconnection/RtcConnection;->b(Lcom/discord/rtcconnection/RtcConnection;)Lcom/discord/rtcconnection/socket/a;

    move-result-object v3

    const/4 v4, 0x1

    if-eqz v3, :cond_4

    .line 4521
    invoke-virtual {v3}, Lcom/discord/rtcconnection/socket/a;->removeAllListeners()V

    .line 4522
    sget-object v5, Lcom/discord/rtcconnection/socket/a$d;->zN:Lcom/discord/rtcconnection/socket/a$d;

    check-cast v5, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v3, v5}, Lcom/discord/rtcconnection/socket/a;->g(Lkotlin/jvm/functions/Function1;)V

    .line 4523
    iput-boolean v4, v3, Lcom/discord/rtcconnection/socket/a;->zE:Z

    .line 1118
    :cond_4
    iget-object v3, p0, Lcom/discord/rtcconnection/RtcConnection$d;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    invoke-static {v3}, Lcom/discord/rtcconnection/RtcConnection;->f(Lcom/discord/rtcconnection/RtcConnection;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1119
    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection$d;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    invoke-static {v0}, Lcom/discord/rtcconnection/RtcConnection;->d(Lcom/discord/rtcconnection/RtcConnection;)Lcom/discord/utilities/logging/Logger;

    move-result-object v3

    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection$d;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    invoke-static {v0}, Lcom/discord/rtcconnection/RtcConnection;->e(Lcom/discord/rtcconnection/RtcConnection;)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0xc

    const/4 v9, 0x0

    const-string v5, "Connect called on destroyed instance."

    invoke-static/range {v3 .. v9}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    .line 1120
    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection$d;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    invoke-static {v0, v2}, Lcom/discord/rtcconnection/RtcConnection;->a(Lcom/discord/rtcconnection/RtcConnection;Z)V

    goto/16 :goto_3

    .line 1124
    :cond_5
    iget-object v3, p0, Lcom/discord/rtcconnection/RtcConnection$d;->$token:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 1125
    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection$d;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    invoke-static {v0}, Lcom/discord/rtcconnection/RtcConnection;->d(Lcom/discord/rtcconnection/RtcConnection;)Lcom/discord/utilities/logging/Logger;

    move-result-object v3

    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection$d;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    invoke-static {v0}, Lcom/discord/rtcconnection/RtcConnection;->e(Lcom/discord/rtcconnection/RtcConnection;)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0xc

    const/4 v9, 0x0

    const-string v5, "Connect called with no token."

    invoke-static/range {v3 .. v9}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    .line 1126
    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection$d;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    invoke-static {v0, v2}, Lcom/discord/rtcconnection/RtcConnection;->a(Lcom/discord/rtcconnection/RtcConnection;Z)V

    goto/16 :goto_3

    .line 1130
    :cond_6
    iget-object v3, p0, Lcom/discord/rtcconnection/RtcConnection$d;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    invoke-static {v3}, Lcom/discord/rtcconnection/RtcConnection;->d(Lcom/discord/rtcconnection/RtcConnection;)Lcom/discord/utilities/logging/Logger;

    move-result-object v7

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "connecting via endpoint: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " token: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/discord/rtcconnection/RtcConnection$d;->$token:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v3, p0, Lcom/discord/rtcconnection/RtcConnection$d;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    invoke-static {v3}, Lcom/discord/rtcconnection/RtcConnection;->e(Lcom/discord/rtcconnection/RtcConnection;)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x4

    const/4 v12, 0x0

    invoke-static/range {v7 .. v12}, Lcom/discord/utilities/logging/Logger;->recordBreadcrumb$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;ILjava/lang/Object;)V

    .line 1132
    iget-object v3, p0, Lcom/discord/rtcconnection/RtcConnection$d;->$endpoint:Ljava/lang/String;

    check-cast v3, Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-nez v3, :cond_7

    const/4 v2, 0x1

    :cond_7
    if-eqz v2, :cond_8

    .line 1134
    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection$d;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    sget-object v1, Lcom/discord/rtcconnection/RtcConnection$State$b;->ym:Lcom/discord/rtcconnection/RtcConnection$State$b;

    check-cast v1, Lcom/discord/rtcconnection/RtcConnection$State;

    invoke-static {v0, v1}, Lcom/discord/rtcconnection/RtcConnection;->a(Lcom/discord/rtcconnection/RtcConnection;Lcom/discord/rtcconnection/RtcConnection$State;)V

    goto :goto_3

    .line 1139
    :cond_8
    :try_start_0
    new-instance v2, Ljava/net/URI;

    invoke-direct {v2, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 1140
    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection$d;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    invoke-virtual {v2}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/discord/rtcconnection/RtcConnection;->a(Lcom/discord/rtcconnection/RtcConnection;Ljava/lang/String;)V

    .line 1141
    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection$d;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    invoke-virtual {v2}, Ljava/net/URI;->getPort()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/discord/rtcconnection/RtcConnection;->a(Lcom/discord/rtcconnection/RtcConnection;Ljava/lang/Integer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    .line 1143
    iget-object v2, p0, Lcom/discord/rtcconnection/RtcConnection$d;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    invoke-static {v2}, Lcom/discord/rtcconnection/RtcConnection;->d(Lcom/discord/rtcconnection/RtcConnection;)Lcom/discord/utilities/logging/Logger;

    move-result-object v2

    check-cast v0, Ljava/lang/Throwable;

    const-string v3, "endpoint"

    invoke-static {v3, v1}, Lkotlin/q;->m(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    invoke-static {v1}, Lkotlin/a/ad;->a(Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v1

    const-string v3, "Failed to parse RTC endpoint"

    invoke-virtual {v2, v3, v0, v1}, Lcom/discord/utilities/logging/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    .line 1151
    :goto_2
    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection$d;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    .line 1145
    new-instance v1, Lcom/discord/rtcconnection/socket/a;

    .line 1147
    iget-object v7, p0, Lcom/discord/rtcconnection/RtcConnection$d;->$token:Ljava/lang/String;

    .line 1148
    iget-object v8, p0, Lcom/discord/rtcconnection/RtcConnection$d;->$sslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

    .line 1149
    invoke-static {v0}, Lcom/discord/rtcconnection/RtcConnection;->d(Lcom/discord/rtcconnection/RtcConnection;)Lcom/discord/utilities/logging/Logger;

    move-result-object v9

    .line 1150
    iget-object v2, p0, Lcom/discord/rtcconnection/RtcConnection$d;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    invoke-static {v2}, Lcom/discord/rtcconnection/RtcConnection;->g(Lcom/discord/rtcconnection/RtcConnection;)Lcom/discord/rtcconnection/mediaengine/MediaEngine;

    move-result-object v2

    invoke-interface {v2}, Lcom/discord/rtcconnection/mediaengine/MediaEngine;->ei()Ljava/util/concurrent/ExecutorService;

    move-result-object v10

    move-object v5, v1

    .line 1145
    invoke-direct/range {v5 .. v10}, Lcom/discord/rtcconnection/socket/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljavax/net/ssl/SSLSocketFactory;Lcom/discord/utilities/logging/Logger;Ljava/util/concurrent/ExecutorService;)V

    .line 1152
    iget-object v2, p0, Lcom/discord/rtcconnection/RtcConnection$d;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    invoke-static {v2}, Lcom/discord/rtcconnection/RtcConnection;->h(Lcom/discord/rtcconnection/RtcConnection;)Lcom/discord/rtcconnection/RtcConnection$n;

    move-result-object v2

    check-cast v2, Lcom/discord/rtcconnection/socket/a$c;

    const-string v3, "listener"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5060
    iget-object v3, v1, Lcom/discord/rtcconnection/socket/a;->listeners:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1153
    invoke-virtual {v1}, Lcom/discord/rtcconnection/socket/a;->connect()Z

    .line 1151
    invoke-static {v0, v1}, Lcom/discord/rtcconnection/RtcConnection;->a(Lcom/discord/rtcconnection/RtcConnection;Lcom/discord/rtcconnection/socket/a;)V

    .line 21
    :goto_3
    sget-object v0, Lkotlin/Unit;->bjS:Lkotlin/Unit;

    return-object v0
.end method
