.class final Lcom/discord/stores/StorePremiumGuildSubscription$getPremiumGuildSubscriptionsState$1;
.super Ljava/lang/Object;
.source "StorePremiumGuildSubscription.kt"

# interfaces
.implements Lrx/functions/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StorePremiumGuildSubscription;->getPremiumGuildSubscriptionsState(Ljava/lang/Long;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/b<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic $guildId:Ljava/lang/Long;


# direct methods
.method constructor <init>(Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StorePremiumGuildSubscription$getPremiumGuildSubscriptionsState$1;->$guildId:Ljava/lang/Long;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/stores/StorePremiumGuildSubscription$State;)Lcom/discord/stores/StorePremiumGuildSubscription$State;
    .locals 8

    .line 74
    instance-of v0, p1, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    if-eqz v0, :cond_5

    .line 75
    iget-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription$getPremiumGuildSubscriptionsState$1;->$guildId:Ljava/lang/Long;

    if-nez v0, :cond_0

    .line 76
    move-object v0, p1

    check-cast v0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    invoke-virtual {v0}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->getPremiumGuildSubscriptions()Ljava/util/List;

    move-result-object v0

    goto :goto_3

    .line 78
    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    invoke-virtual {v0}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->getPremiumGuildSubscriptions()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 164
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 165
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/discord/models/domain/ModelPremiumGuildSubscription;

    .line 78
    invoke-virtual {v3}, Lcom/discord/models/domain/ModelPremiumGuildSubscription;->getGuildId()J

    move-result-wide v3

    iget-object v5, p0, Lcom/discord/stores/StorePremiumGuildSubscription$getPremiumGuildSubscriptionsState$1;->$guildId:Ljava/lang/Long;

    if-nez v5, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v7, v3, v5

    if-nez v7, :cond_3

    const/4 v3, 0x1

    goto :goto_2

    :cond_3
    :goto_1
    const/4 v3, 0x0

    :goto_2
    if-eqz v3, :cond_1

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 166
    :cond_4
    move-object v0, v1

    check-cast v0, Ljava/util/List;

    .line 80
    :goto_3
    new-instance v1, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    check-cast p1, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    invoke-virtual {p1}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->getCooldown()Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;

    move-result-object p1

    invoke-direct {v1, v0, p1}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;-><init>(Ljava/util/List;Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;)V

    check-cast v1, Lcom/discord/stores/StorePremiumGuildSubscription$State;

    return-object v1

    :cond_5
    return-object p1
.end method

.method public final bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Lcom/discord/stores/StorePremiumGuildSubscription$State;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StorePremiumGuildSubscription$getPremiumGuildSubscriptionsState$1;->call(Lcom/discord/stores/StorePremiumGuildSubscription$State;)Lcom/discord/stores/StorePremiumGuildSubscription$State;

    move-result-object p1

    return-object p1
.end method
