.class public final Lcom/discord/stores/StorePremiumGuildSubscription;
.super Ljava/lang/Object;
.source "StorePremiumGuildSubscription.kt"

# interfaces
.implements Lcom/discord/stores/DispatchHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StorePremiumGuildSubscription$State;,
        Lcom/discord/stores/StorePremiumGuildSubscription$Actions;
    }
.end annotation


# instance fields
.field private isDirty:Z

.field private state:Lcom/discord/stores/StorePremiumGuildSubscription$State;

.field private final stateSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/discord/stores/StorePremiumGuildSubscription$State;",
            ">;"
        }
    .end annotation
.end field

.field private final stream:Lcom/discord/stores/StoreStream;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreStream;)V
    .locals 1

    const-string v0, "stream"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->stream:Lcom/discord/stores/StoreStream;

    .line 17
    sget-object p1, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loading;->INSTANCE:Lcom/discord/stores/StorePremiumGuildSubscription$State$Loading;

    check-cast p1, Lcom/discord/stores/StorePremiumGuildSubscription$State;

    iput-object p1, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->state:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    .line 18
    iget-object p1, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->state:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    invoke-static {p1}, Lrx/subjects/BehaviorSubject;->bT(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->stateSubject:Lrx/subjects/BehaviorSubject;

    .line 23
    sget-object p1, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->INSTANCE:Lcom/discord/stores/StorePremiumGuildSubscription$Actions;

    iget-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {p1, p0, v0}, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->init(Lcom/discord/stores/StorePremiumGuildSubscription;Lcom/discord/stores/StoreStream;)V

    return-void
.end method

.method public static synthetic getPremiumGuildSubscriptionsState$default(Lcom/discord/stores/StorePremiumGuildSubscription;Ljava/lang/Long;ILjava/lang/Object;)Lrx/Observable;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 72
    :cond_0
    invoke-virtual {p0, p1}, Lcom/discord/stores/StorePremiumGuildSubscription;->getPremiumGuildSubscriptionsState(Ljava/lang/Long;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final getPremiumGuildSubscriptionsState(Ljava/lang/Long;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/stores/StorePremiumGuildSubscription$State;",
            ">;"
        }
    .end annotation

    .line 73
    iget-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->stateSubject:Lrx/subjects/BehaviorSubject;

    new-instance v1, Lcom/discord/stores/StorePremiumGuildSubscription$getPremiumGuildSubscriptionsState$1;

    invoke-direct {v1, p1}, Lcom/discord/stores/StorePremiumGuildSubscription$getPremiumGuildSubscriptionsState$1;-><init>(Ljava/lang/Long;)V

    check-cast v1, Lrx/functions/b;

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->e(Lrx/functions/b;)Lrx/Observable;

    move-result-object p1

    const-string v0, "stateSubject.map { state\u2026  state\n        }\n      }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final getStream()Lcom/discord/stores/StoreStream;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->stream:Lcom/discord/stores/StoreStream;

    return-object v0
.end method

.method public final handleFetchError()V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .line 42
    sget-object v0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Failure;->INSTANCE:Lcom/discord/stores/StorePremiumGuildSubscription$State$Failure;

    check-cast v0, Lcom/discord/stores/StorePremiumGuildSubscription$State;

    iput-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->state:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    const/4 v0, 0x1

    .line 43
    iput-boolean v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->isDirty:Z

    return-void
.end method

.method public final handleFetchStateSuccess(Ljava/util/List;Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscription;",
            ">;",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;",
            ")V"
        }
    .end annotation

    const-string v0, "premiumGuildSubscriptions"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    new-instance v0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    invoke-direct {v0, p1, p2}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;-><init>(Ljava/util/List;Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;)V

    check-cast v0, Lcom/discord/stores/StorePremiumGuildSubscription$State;

    iput-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->state:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    const/4 p1, 0x1

    .line 49
    iput-boolean p1, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->isDirty:Z

    return-void
.end method

.method public final handleFetchingState()V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .line 36
    sget-object v0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loading;->INSTANCE:Lcom/discord/stores/StorePremiumGuildSubscription$State$Loading;

    check-cast v0, Lcom/discord/stores/StorePremiumGuildSubscription$State;

    iput-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->state:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    const/4 v0, 0x1

    .line 37
    iput-boolean v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->isDirty:Z

    return-void
.end method

.method public final handleNewPremiumGuildSubscription(Lcom/discord/models/domain/ModelPremiumGuildSubscription;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "premiumGuildSubscription"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->state:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    .line 55
    instance-of v1, v0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    if-eqz v1, :cond_0

    .line 56
    check-cast v0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    invoke-virtual {v0}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->getPremiumGuildSubscriptions()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-static {v1, p1}, Lkotlin/a/m;->a(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 57
    new-instance v1, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    invoke-virtual {v0}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->getCooldown()Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;

    move-result-object v0

    invoke-direct {v1, p1, v0}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;-><init>(Ljava/util/List;Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;)V

    check-cast v1, Lcom/discord/stores/StorePremiumGuildSubscription$State;

    iput-object v1, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->state:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    const/4 p1, 0x1

    .line 58
    iput-boolean p1, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->isDirty:Z

    :cond_0
    return-void
.end method

.method public final handleRemovePremiumGuildSubscription(J)V
    .locals 8
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .line 64
    iget-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->state:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    .line 65
    instance-of v1, v0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    if-eqz v1, :cond_3

    .line 66
    check-cast v0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    invoke-virtual {v0}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->getPremiumGuildSubscriptions()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 164
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 165
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v5, v3

    check-cast v5, Lcom/discord/models/domain/ModelPremiumGuildSubscription;

    .line 66
    invoke-virtual {v5}, Lcom/discord/models/domain/ModelPremiumGuildSubscription;->getId()J

    move-result-wide v5

    cmp-long v7, v5, p1

    if-eqz v7, :cond_1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_0

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 166
    :cond_2
    check-cast v2, Ljava/util/List;

    .line 67
    new-instance p1, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    invoke-virtual {v0}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->getCooldown()Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;

    move-result-object p2

    invoke-direct {p1, v2, p2}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;-><init>(Ljava/util/List;Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;)V

    check-cast p1, Lcom/discord/stores/StorePremiumGuildSubscription$State;

    iput-object p1, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->state:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    .line 68
    iput-boolean v4, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->isDirty:Z

    :cond_3
    return-void
.end method

.method public final onDispatchEnded()V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .line 28
    iget-boolean v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->isDirty:Z

    if-eqz v0, :cond_0

    .line 29
    iget-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->stateSubject:Lrx/subjects/BehaviorSubject;

    iget-object v1, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->state:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    const/4 v0, 0x0

    .line 30
    iput-boolean v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->isDirty:Z

    :cond_0
    return-void
.end method
