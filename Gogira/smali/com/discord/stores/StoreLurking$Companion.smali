.class public final Lcom/discord/stores/StoreLurking$Companion;
.super Ljava/lang/Object;
.source "StoreLurking.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreLurking;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 200
    invoke-direct {p0}, Lcom/discord/stores/StoreLurking$Companion;-><init>()V

    return-void
.end method

.method public static final varargs synthetic access$isLurking(Lcom/discord/stores/StoreLurking$Companion;Lcom/discord/models/domain/ModelGuild;[J)Z
    .locals 0

    .line 200
    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreLurking$Companion;->isLurking(Lcom/discord/models/domain/ModelGuild;[J)Z

    move-result p0

    return p0
.end method

.method private final varargs isLurking(Lcom/discord/models/domain/ModelGuild;[J)Z
    .locals 7

    if-eqz p1, :cond_0

    .line 204
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getJoinedAt()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x0

    if-nez v0, :cond_5

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v2

    goto :goto_1

    :cond_1
    const-wide/16 v2, 0x0

    :goto_1
    const-string p1, "$this$contains"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "$this$indexOf"

    .line 1412
    invoke-static {p2, p1}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2331
    array-length p1, p2

    const/4 v0, 0x0

    :goto_2
    if-ge v0, p1, :cond_3

    .line 2332
    aget-wide v4, p2, v0

    cmp-long v6, v2, v4

    if-nez v6, :cond_2

    goto :goto_3

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    const/4 v0, -0x1

    :goto_3
    const/4 p1, 0x1

    if-ltz v0, :cond_4

    const/4 p2, 0x1

    goto :goto_4

    :cond_4
    const/4 p2, 0x0

    :goto_4
    if-eqz p2, :cond_5

    return p1

    :cond_5
    return v1
.end method
