.class public final Lcom/discord/stores/StorePremiumGuildSubscription$Actions;
.super Ljava/lang/Object;
.source "StorePremiumGuildSubscription.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StorePremiumGuildSubscription;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Actions"
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/stores/StorePremiumGuildSubscription$Actions;

.field private static store:Lcom/discord/stores/StorePremiumGuildSubscription;

.field private static stream:Lcom/discord/stores/StoreStream;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 105
    new-instance v0, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;

    invoke-direct {v0}, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;-><init>()V

    sput-object v0, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->INSTANCE:Lcom/discord/stores/StorePremiumGuildSubscription$Actions;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$getStore$p(Lcom/discord/stores/StorePremiumGuildSubscription$Actions;)Lcom/discord/stores/StorePremiumGuildSubscription;
    .locals 1

    .line 105
    sget-object p0, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->store:Lcom/discord/stores/StorePremiumGuildSubscription;

    if-nez p0, :cond_0

    const-string v0, "store"

    invoke-static {v0}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getStream$p(Lcom/discord/stores/StorePremiumGuildSubscription$Actions;)Lcom/discord/stores/StoreStream;
    .locals 1

    .line 105
    sget-object p0, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->stream:Lcom/discord/stores/StoreStream;

    if-nez p0, :cond_0

    const-string v0, "stream"

    invoke-static {v0}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setStore$p(Lcom/discord/stores/StorePremiumGuildSubscription$Actions;Lcom/discord/stores/StorePremiumGuildSubscription;)V
    .locals 0

    .line 105
    sput-object p1, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->store:Lcom/discord/stores/StorePremiumGuildSubscription;

    return-void
.end method

.method public static final synthetic access$setStream$p(Lcom/discord/stores/StorePremiumGuildSubscription$Actions;Lcom/discord/stores/StoreStream;)V
    .locals 0

    .line 105
    sput-object p1, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->stream:Lcom/discord/stores/StoreStream;

    return-void
.end method


# virtual methods
.method public final appendNewPremiumGuildSubscription(Lcom/discord/models/domain/ModelPremiumGuildSubscription;)V
    .locals 2

    const-string v0, "premiumGuildSubscription"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 152
    sget-object v0, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->stream:Lcom/discord/stores/StoreStream;

    if-nez v0, :cond_0

    const-string v1, "stream"

    invoke-static {v1}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/discord/stores/StorePremiumGuildSubscription$Actions$appendNewPremiumGuildSubscription$1;

    invoke-direct {v1, p1}, Lcom/discord/stores/StorePremiumGuildSubscription$Actions$appendNewPremiumGuildSubscription$1;-><init>(Lcom/discord/models/domain/ModelPremiumGuildSubscription;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreStream;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final fetchUserGuildPremiumState()V
    .locals 12

    .line 116
    sget-object v0, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->stream:Lcom/discord/stores/StoreStream;

    if-nez v0, :cond_0

    const-string v1, "stream"

    invoke-static {v1}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_0
    sget-object v1, Lcom/discord/stores/StorePremiumGuildSubscription$Actions$fetchUserGuildPremiumState$1;->INSTANCE:Lcom/discord/stores/StorePremiumGuildSubscription$Actions$fetchUserGuildPremiumState$1;

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreStream;->schedule(Lkotlin/jvm/functions/Function0;)V

    .line 121
    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    .line 123
    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI;->getUserPremiumGuildSubscriptions()Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 124
    invoke-static {v0, v3, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    .line 125
    sget-object v4, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v4}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v4

    .line 127
    invoke-virtual {v4}, Lcom/discord/utilities/rest/RestAPI;->getUserPremiumGuildSubscriptionsCooldown()Lrx/Observable;

    move-result-object v4

    .line 128
    invoke-static {v4, v3, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    .line 129
    sget-object v2, Lcom/discord/stores/StorePremiumGuildSubscription$Actions$fetchUserGuildPremiumState$2;->INSTANCE:Lcom/discord/stores/StorePremiumGuildSubscription$Actions$fetchUserGuildPremiumState$2;

    check-cast v2, Lrx/functions/b;

    invoke-virtual {v1, v2}, Lrx/Observable;->f(Lrx/functions/b;)Lrx/Observable;

    move-result-object v1

    .line 136
    sget-object v2, Lcom/discord/stores/StorePremiumGuildSubscription$Actions$fetchUserGuildPremiumState$3;->INSTANCE:Lcom/discord/stores/StorePremiumGuildSubscription$Actions$fetchUserGuildPremiumState$3;

    check-cast v2, Lrx/functions/Func2;

    .line 120
    invoke-static {v0, v1, v2}, Lrx/Observable;->b(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v3

    const-string v0, "Observable.zip(\n        \u2026bscriptions to cooldown }"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    const-class v4, Lcom/discord/stores/StorePremiumGuildSubscription;

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 139
    sget-object v0, Lcom/discord/stores/StorePremiumGuildSubscription$Actions$fetchUserGuildPremiumState$4;->INSTANCE:Lcom/discord/stores/StorePremiumGuildSubscription$Actions$fetchUserGuildPremiumState$4;

    move-object v7, v0

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x0

    .line 144
    sget-object v0, Lcom/discord/stores/StorePremiumGuildSubscription$Actions$fetchUserGuildPremiumState$5;->INSTANCE:Lcom/discord/stores/StorePremiumGuildSubscription$Actions$fetchUserGuildPremiumState$5;

    move-object v9, v0

    check-cast v9, Lkotlin/jvm/functions/Function1;

    const/16 v10, 0x16

    const/4 v11, 0x0

    .line 137
    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final init(Lcom/discord/stores/StorePremiumGuildSubscription;Lcom/discord/stores/StoreStream;)V
    .locals 1

    const-string v0, "store"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stream"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    sput-object p1, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->store:Lcom/discord/stores/StorePremiumGuildSubscription;

    .line 112
    sput-object p2, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->stream:Lcom/discord/stores/StoreStream;

    return-void
.end method

.method public final removeNewPremiumGuildSubscription(J)V
    .locals 2

    .line 158
    sget-object v0, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->stream:Lcom/discord/stores/StoreStream;

    if-nez v0, :cond_0

    const-string v1, "stream"

    invoke-static {v1}, Lkotlin/jvm/internal/k;->dN(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/discord/stores/StorePremiumGuildSubscription$Actions$removeNewPremiumGuildSubscription$1;

    invoke-direct {v1, p1, p2}, Lcom/discord/stores/StorePremiumGuildSubscription$Actions$removeNewPremiumGuildSubscription$1;-><init>(J)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreStream;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
