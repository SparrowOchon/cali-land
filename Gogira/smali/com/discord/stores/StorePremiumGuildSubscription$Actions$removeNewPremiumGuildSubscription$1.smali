.class final Lcom/discord/stores/StorePremiumGuildSubscription$Actions$removeNewPremiumGuildSubscription$1;
.super Lkotlin/jvm/internal/l;
.source "StorePremiumGuildSubscription.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->removeNewPremiumGuildSubscription(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/l;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic $subscriptionId:J


# direct methods
.method constructor <init>(J)V
    .locals 0

    iput-wide p1, p0, Lcom/discord/stores/StorePremiumGuildSubscription$Actions$removeNewPremiumGuildSubscription$1;->$subscriptionId:J

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/l;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 105
    invoke-virtual {p0}, Lcom/discord/stores/StorePremiumGuildSubscription$Actions$removeNewPremiumGuildSubscription$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->bjS:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    .line 159
    sget-object v0, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->INSTANCE:Lcom/discord/stores/StorePremiumGuildSubscription$Actions;

    invoke-static {v0}, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->access$getStore$p(Lcom/discord/stores/StorePremiumGuildSubscription$Actions;)Lcom/discord/stores/StorePremiumGuildSubscription;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/stores/StorePremiumGuildSubscription$Actions$removeNewPremiumGuildSubscription$1;->$subscriptionId:J

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StorePremiumGuildSubscription;->handleRemovePremiumGuildSubscription(J)V

    return-void
.end method
