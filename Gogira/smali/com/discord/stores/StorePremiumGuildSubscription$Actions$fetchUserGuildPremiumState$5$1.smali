.class final Lcom/discord/stores/StorePremiumGuildSubscription$Actions$fetchUserGuildPremiumState$5$1;
.super Lkotlin/jvm/internal/l;
.source "StorePremiumGuildSubscription.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StorePremiumGuildSubscription$Actions$fetchUserGuildPremiumState$5;->invoke(Lkotlin/Pair;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/l;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic $cooldown:Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;

.field final synthetic $subscriptions:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/util/List;Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StorePremiumGuildSubscription$Actions$fetchUserGuildPremiumState$5$1;->$subscriptions:Ljava/util/List;

    iput-object p2, p0, Lcom/discord/stores/StorePremiumGuildSubscription$Actions$fetchUserGuildPremiumState$5$1;->$cooldown:Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/l;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 105
    invoke-virtual {p0}, Lcom/discord/stores/StorePremiumGuildSubscription$Actions$fetchUserGuildPremiumState$5$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->bjS:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    .line 146
    sget-object v0, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->INSTANCE:Lcom/discord/stores/StorePremiumGuildSubscription$Actions;

    invoke-static {v0}, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->access$getStore$p(Lcom/discord/stores/StorePremiumGuildSubscription$Actions;)Lcom/discord/stores/StorePremiumGuildSubscription;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StorePremiumGuildSubscription$Actions$fetchUserGuildPremiumState$5$1;->$subscriptions:Ljava/util/List;

    const-string v2, "subscriptions"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/discord/stores/StorePremiumGuildSubscription$Actions$fetchUserGuildPremiumState$5$1;->$cooldown:Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StorePremiumGuildSubscription;->handleFetchStateSuccess(Ljava/util/List;Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;)V

    return-void
.end method
