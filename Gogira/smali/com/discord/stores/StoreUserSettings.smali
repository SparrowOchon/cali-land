.class public Lcom/discord/stores/StoreUserSettings;
.super Lcom/discord/stores/Store;
.source "StoreUserSettings.java"


# instance fields
.field private final allowAnimatedEmojisPublisher:Lcom/discord/utilities/persister/Persister;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/persister/Persister<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final collector:Lcom/discord/stores/StoreStream;

.field private final defaultGuildsRestrictedSubject:Lrx/subjects/Subject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/Subject<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final explicitContentFilterSubject:Lrx/subjects/Subject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/Subject<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final fontScalingPublisher:Lcom/discord/utilities/persister/Persister;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/persister/Persister<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final friendSourceFlagsSubject:Lrx/subjects/Subject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/Subject<",
            "Lcom/discord/models/domain/ModelUserSettings$FriendSourceFlags;",
            "Lcom/discord/models/domain/ModelUserSettings$FriendSourceFlags;",
            ">;"
        }
    .end annotation
.end field

.field private final guildFoldersPublisher:Lcom/discord/utilities/persister/Persister;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/persister/Persister<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelGuildFolder;",
            ">;>;"
        }
    .end annotation
.end field

.field private final localePublisher:Lrx/subjects/Subject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/Subject<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final restrictedGuildIdsPublisher:Lcom/discord/utilities/persister/Persister;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/persister/Persister<",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private final showCurrentGame:Lcom/discord/utilities/persister/Persister;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/persister/Persister<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final themePublisher:Lrx/subjects/Subject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/Subject<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreStream;)V
    .locals 3

    .line 54
    invoke-direct {p0}, Lcom/discord/stores/Store;-><init>()V

    .line 39
    new-instance v0, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->Ls()Lrx/subjects/BehaviorSubject;

    move-result-object v1

    invoke-direct {v0, v1}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object v0, p0, Lcom/discord/stores/StoreUserSettings;->explicitContentFilterSubject:Lrx/subjects/Subject;

    .line 40
    new-instance v0, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->Ls()Lrx/subjects/BehaviorSubject;

    move-result-object v1

    invoke-direct {v0, v1}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object v0, p0, Lcom/discord/stores/StoreUserSettings;->defaultGuildsRestrictedSubject:Lrx/subjects/Subject;

    .line 41
    new-instance v0, Lrx/subjects/SerializedSubject;

    .line 43
    invoke-static {}, Lrx/subjects/BehaviorSubject;->Ls()Lrx/subjects/BehaviorSubject;

    move-result-object v1

    invoke-direct {v0, v1}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object v0, p0, Lcom/discord/stores/StoreUserSettings;->friendSourceFlagsSubject:Lrx/subjects/Subject;

    .line 45
    new-instance v0, Lcom/discord/utilities/persister/Persister;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const-string v2, "RESTRICTED_GUILD_IDS"

    invoke-direct {v0, v2, v1}, Lcom/discord/utilities/persister/Persister;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/discord/stores/StoreUserSettings;->restrictedGuildIdsPublisher:Lcom/discord/utilities/persister/Persister;

    .line 46
    new-instance v0, Lcom/discord/utilities/persister/Persister;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const-string v2, "STORE_SETTINGS_FOLDERS_V1"

    invoke-direct {v0, v2, v1}, Lcom/discord/utilities/persister/Persister;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/discord/stores/StoreUserSettings;->guildFoldersPublisher:Lcom/discord/utilities/persister/Persister;

    .line 47
    new-instance v0, Lcom/discord/utilities/persister/Persister;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "STORE_SETTINGS_ALLOW_ANIMATED_EMOJIS"

    invoke-direct {v0, v2, v1}, Lcom/discord/utilities/persister/Persister;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/discord/stores/StoreUserSettings;->allowAnimatedEmojisPublisher:Lcom/discord/utilities/persister/Persister;

    .line 48
    new-instance v0, Lcom/discord/utilities/persister/Persister;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "STORE_SETTINGS_ALLOW_GAME_STATUS"

    invoke-direct {v0, v2, v1}, Lcom/discord/utilities/persister/Persister;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/discord/stores/StoreUserSettings;->showCurrentGame:Lcom/discord/utilities/persister/Persister;

    .line 49
    new-instance v0, Lcom/discord/utilities/persister/Persister;

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "CACHE_KEY_FONT_SCALE"

    invoke-direct {v0, v2, v1}, Lcom/discord/utilities/persister/Persister;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/discord/stores/StoreUserSettings;->fontScalingPublisher:Lcom/discord/utilities/persister/Persister;

    .line 51
    new-instance v0, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->Ls()Lrx/subjects/BehaviorSubject;

    move-result-object v1

    invoke-direct {v0, v1}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object v0, p0, Lcom/discord/stores/StoreUserSettings;->localePublisher:Lrx/subjects/Subject;

    .line 52
    new-instance v0, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->Ls()Lrx/subjects/BehaviorSubject;

    move-result-object v1

    invoke-direct {v0, v1}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object v0, p0, Lcom/discord/stores/StoreUserSettings;->themePublisher:Lrx/subjects/Subject;

    .line 55
    iput-object p1, p0, Lcom/discord/stores/StoreUserSettings;->collector:Lcom/discord/stores/StoreStream;

    return-void
.end method

.method private static cacheAndPublishString(Landroid/content/SharedPreferences;Lrx/subjects/Subject;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            "Lrx/subjects/Subject<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 496
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    invoke-interface {p0, p3, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 497
    invoke-virtual {p1, p2}, Lrx/subjects/Subject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private getAdjustedTheme(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const-string v0, "dark"

    .line 92
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getThemePureEvil()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, "pureEvil"

    :cond_0
    return-object p1
.end method

.method private handleUserSettings(Lcom/discord/models/domain/ModelUserSettings;)V
    .locals 4

    .line 416
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getGuildFolders()Ljava/util/List;

    move-result-object v0

    .line 417
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getRestrictedGuilds()Ljava/util/List;

    move-result-object v1

    .line 419
    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getSyncTextAndImages()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 422
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getInlineEmbedMedia()Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    .line 423
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getInlineEmbedMedia()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p0, v3, v2}, Lcom/discord/stores/StoreUserSettings;->setInlineEmbedMedia(Lcom/discord/app/AppActivity;Z)V

    .line 426
    :cond_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getInlineAttachmentMedia()Ljava/lang/Boolean;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 427
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getInlineAttachmentMedia()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p0, v3, v2}, Lcom/discord/stores/StoreUserSettings;->setInlineAttachmentMedia(Lcom/discord/app/AppActivity;Z)V

    .line 430
    :cond_1
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getRenderEmbeds()Ljava/lang/Boolean;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 431
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getRenderEmbeds()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p0, v3, v2}, Lcom/discord/stores/StoreUserSettings;->setRenderEmbeds(Lcom/discord/app/AppActivity;Z)V

    .line 434
    :cond_2
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getAnimateEmoji()Ljava/lang/Boolean;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 435
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getAnimateEmoji()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p0, v3, v2}, Lcom/discord/stores/StoreUserSettings;->setAllowAnimatedEmojis(Lcom/discord/app/AppActivity;Z)V

    :cond_3
    if-eqz v0, :cond_4

    .line 440
    iget-object v2, p0, Lcom/discord/stores/StoreUserSettings;->guildFoldersPublisher:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v2, v0}, Lcom/discord/utilities/persister/Persister;->set(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    if-eqz v1, :cond_5

    .line 444
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->restrictedGuildIdsPublisher:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0, v1}, Lcom/discord/utilities/persister/Persister;->set(Ljava/lang/Object;)Ljava/lang/Object;

    .line 447
    :cond_5
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getDeveloperMode()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 448
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getDeveloperMode()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreUserSettings;->setDeveloperModeInternal(Z)V

    .line 451
    :cond_6
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getShowCurrentGame()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 452
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->showCurrentGame:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getShowCurrentGame()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/utilities/persister/Persister;->set(Ljava/lang/Object;)Ljava/lang/Object;

    .line 455
    :cond_7
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getTheme()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getThemeSync()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 456
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getTheme()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreUserSettings;->setTheme(Ljava/lang/String;)V

    .line 459
    :cond_8
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getLocale()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getLocaleSync()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 460
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getLocale()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreUserSettings;->setLocale(Ljava/lang/String;)V

    .line 463
    :cond_9
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getExplicitContentFilter()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 464
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->explicitContentFilterSubject:Lrx/subjects/Subject;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getExplicitContentFilter()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/subjects/Subject;->onNext(Ljava/lang/Object;)V

    .line 467
    :cond_a
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getFriendSourceFlags()Lcom/discord/models/domain/ModelUserSettings$FriendSourceFlags;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 468
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->friendSourceFlagsSubject:Lrx/subjects/Subject;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getFriendSourceFlags()Lcom/discord/models/domain/ModelUserSettings$FriendSourceFlags;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/subjects/Subject;->onNext(Ljava/lang/Object;)V

    .line 471
    :cond_b
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getDefaultGuildsRestricted()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 472
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->defaultGuildsRestrictedSubject:Lrx/subjects/Subject;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getDefaultGuildsRestricted()Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lrx/subjects/Subject;->onNext(Ljava/lang/Object;)V

    :cond_c
    return-void
.end method

.method public static synthetic lambda$6CBA5-vxFh5EUYWoVn3NsRjv3Ik(Lcom/discord/stores/StoreUserSettings;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreUserSettings;->getAdjustedTheme(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$getConsents$0(Ljava/lang/Throwable;)Lcom/discord/models/domain/Consents;
    .locals 0

    .line 123
    sget-object p0, Lcom/discord/models/domain/ModelUserConsents;->DEFAULT_CONSENTS:Lcom/discord/models/domain/Consents;

    return-object p0
.end method

.method static synthetic lambda$updateUserSettings$1(Ljava/lang/Integer;Lcom/discord/app/AppActivity;Lcom/discord/models/domain/ModelUserSettings;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 378
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    invoke-static {p1, p0}, Lcom/discord/app/h;->d(Landroid/content/Context;I)V

    :cond_0
    return-void
.end method

.method private setDeveloperModeInternal(Z)V
    .locals 2

    .line 355
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CACHE_KEY_DEVELOPER_MODE"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private setLocale(Ljava/lang/String;)V
    .locals 3

    .line 481
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/discord/stores/StoreUserSettings;->localePublisher:Lrx/subjects/Subject;

    const-string v2, "CACHE_KEY_LOCALE"

    invoke-static {v0, v1, p1, v2}, Lcom/discord/stores/StoreUserSettings;->cacheAndPublishString(Landroid/content/SharedPreferences;Lrx/subjects/Subject;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private setTheme(Ljava/lang/String;)V
    .locals 3

    .line 477
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/discord/stores/StoreUserSettings;->themePublisher:Lrx/subjects/Subject;

    const-string v2, "CACHE_KEY_THEME"

    invoke-static {v0, v1, p1, v2}, Lcom/discord/stores/StoreUserSettings;->cacheAndPublishString(Landroid/content/SharedPreferences;Lrx/subjects/Subject;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V
    .locals 1

    if-nez p0, :cond_0

    return-void

    .line 372
    :cond_0
    invoke-static {}, Lcom/discord/utilities/rest/RestAPI;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    .line 373
    invoke-virtual {v0, p1}, Lcom/discord/utilities/rest/RestAPI;->updateUserSettings(Lcom/discord/restapi/RestAPIParams$UserSettings;)Lrx/Observable;

    move-result-object p1

    .line 374
    invoke-static {}, Lcom/discord/app/i;->dA()Lrx/Observable$c;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->a(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p1

    .line 375
    invoke-static {p0}, Lcom/discord/app/i;->b(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->a(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/discord/stores/-$$Lambda$StoreUserSettings$744JGmz6qmIj2P5epXEJWjZwj2Q;

    invoke-direct {v0, p2, p0}, Lcom/discord/stores/-$$Lambda$StoreUserSettings$744JGmz6qmIj2P5epXEJWjZwj2Q;-><init>(Ljava/lang/Integer;Lcom/discord/app/AppActivity;)V

    .line 376
    invoke-static {v0, p0}, Lcom/discord/app/i;->b(Lrx/functions/Action1;Landroid/content/Context;)Lrx/Observable$c;

    move-result-object p0

    invoke-virtual {p1, p0}, Lrx/Observable;->a(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method


# virtual methods
.method public getAllowAnimatedEmojisObservable()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 104
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->allowAnimatedEmojisPublisher:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0}, Lcom/discord/utilities/persister/Persister;->getObservable()Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->JL()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getBackButtonOpensDrawer()Z
    .locals 3

    .line 171
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "CACHE_KEY_BACK_BUTTON_OPEN_DRAWER"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getConsents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/Consents;",
            ">;"
        }
    .end annotation

    .line 120
    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->api:Lcom/discord/utilities/rest/RestAPI;

    .line 122
    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI;->getConsents()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/stores/-$$Lambda$StoreUserSettings$_69xf_JbApgqUmLJ3UUt0lNXxEU;->INSTANCE:Lcom/discord/stores/-$$Lambda$StoreUserSettings$_69xf_JbApgqUmLJ3UUt0lNXxEU;

    .line 123
    invoke-virtual {v0, v1}, Lrx/Observable;->f(Lrx/functions/b;)Lrx/Observable;

    move-result-object v0

    .line 124
    invoke-static {}, Lcom/discord/app/i;->dA()Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->a(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultGuildsRestricted()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 145
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->defaultGuildsRestrictedSubject:Lrx/subjects/Subject;

    .line 146
    invoke-static {}, Lcom/discord/app/i;->dC()Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/subjects/Subject;->a(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getDeveloperMode()Z
    .locals 3

    .line 183
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "CACHE_KEY_DEVELOPER_MODE"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getExplicitContentFilter()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 140
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->explicitContentFilterSubject:Lrx/subjects/Subject;

    .line 141
    invoke-static {}, Lcom/discord/app/i;->dC()Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/subjects/Subject;->a(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getFontScale()I
    .locals 1

    .line 195
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->fontScalingPublisher:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0}, Lcom/discord/utilities/persister/Persister;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getFontScaleObs()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 191
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->fontScalingPublisher:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0}, Lcom/discord/utilities/persister/Persister;->getObservable()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getFriendSourceFlags()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUserSettings$FriendSourceFlags;",
            ">;"
        }
    .end annotation

    .line 150
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->friendSourceFlagsSubject:Lrx/subjects/Subject;

    .line 151
    invoke-static {}, Lcom/discord/app/i;->dC()Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/subjects/Subject;->a(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getGuildFolders()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelGuildFolder;",
            ">;>;"
        }
    .end annotation

    .line 128
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->guildFoldersPublisher:Lcom/discord/utilities/persister/Persister;

    .line 129
    invoke-virtual {v0}, Lcom/discord/utilities/persister/Persister;->getObservable()Lrx/Observable;

    move-result-object v0

    .line 130
    invoke-static {}, Lcom/discord/app/i;->dC()Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->a(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getInlineAttachmentMedia()Z
    .locals 3

    .line 159
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "CACHE_KEY_INLINE_ATTACHMENT_MEDIA"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getInlineEmbedMedia()Z
    .locals 3

    .line 155
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "CACHE_KEY_INLINE_EMBED_MEDIA"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getLocale()Ljava/lang/String;
    .locals 3

    .line 112
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    sget-object v1, Lcom/discord/models/domain/ModelUserSettings;->LOCALE_DEFAULT:Ljava/lang/String;

    const-string v2, "CACHE_KEY_LOCALE"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLocaleObservable()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 78
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->localePublisher:Lrx/subjects/Subject;

    invoke-virtual {v0}, Lrx/subjects/Subject;->JL()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getLocaleSync()Z
    .locals 3

    .line 116
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "CACHE_KEY_LOCALE_SYNC"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getMobileOverlay()Z
    .locals 3

    .line 175
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "CACHE_KEY_MOBILE_OVERLAY"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getRenderEmbeds()Z
    .locals 3

    .line 163
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "CACHE_KEY_RENDER_EMBEDS"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getRestrictedGuildIds()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    .line 134
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->restrictedGuildIdsPublisher:Lcom/discord/utilities/persister/Persister;

    .line 135
    invoke-virtual {v0}, Lcom/discord/utilities/persister/Persister;->getObservable()Lrx/Observable;

    move-result-object v0

    .line 136
    invoke-static {}, Lcom/discord/app/i;->dC()Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->a(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getShiftEnterToSend()Z
    .locals 3

    .line 179
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "CACHE_KEY_SHIFT_ENTER_TO_SEND"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getShowCurrentGame()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 199
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->showCurrentGame:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0}, Lcom/discord/utilities/persister/Persister;->getObservable()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getSyncTextAndImages()Z
    .locals 3

    .line 167
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "CACHE_KEY_SYNC_TEXT_AND_IMAGES"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getTheme()Ljava/lang/String;
    .locals 3

    .line 82
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "CACHE_KEY_THEME"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "dark"

    .line 86
    invoke-direct {p0, v0}, Lcom/discord/stores/StoreUserSettings;->setTheme(Ljava/lang/String;)V

    .line 88
    :cond_0
    invoke-direct {p0, v0}, Lcom/discord/stores/StoreUserSettings;->getAdjustedTheme(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getThemeObservable()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 72
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->themePublisher:Lrx/subjects/Subject;

    new-instance v1, Lcom/discord/stores/-$$Lambda$StoreUserSettings$6CBA5-vxFh5EUYWoVn3NsRjv3Ik;

    invoke-direct {v1, p0}, Lcom/discord/stores/-$$Lambda$StoreUserSettings$6CBA5-vxFh5EUYWoVn3NsRjv3Ik;-><init>(Lcom/discord/stores/StoreUserSettings;)V

    .line 73
    invoke-virtual {v0, v1}, Lrx/subjects/Subject;->e(Lrx/functions/b;)Lrx/Observable;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Lrx/Observable;->JL()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getThemeObservable(Z)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 66
    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getThemeObservable()Lrx/Observable;

    move-result-object p1

    const-wide/16 v0, 0x5dc

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 67
    invoke-virtual {p1, v0, v1, v2}, Lrx/Observable;->k(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 68
    :cond_0
    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getThemeObservable()Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getThemePureEvil()Z
    .locals 3

    .line 100
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "CACHE_KEY_THEME_PURE_EVIL"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getThemeSync()Z
    .locals 3

    .line 108
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "CACHE_KEY_THEME_SYNC"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getUseChromeCustomTabs()Z
    .locals 3

    .line 187
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "CACHE_KEY_USE_CHROME_CUSTOM_TABS"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V
    .locals 0

    .line 405
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getUserSettings()Lcom/discord/models/domain/ModelUserSettings;

    move-result-object p1

    .line 407
    invoke-direct {p0, p1}, Lcom/discord/stores/StoreUserSettings;->handleUserSettings(Lcom/discord/models/domain/ModelUserSettings;)V

    return-void
.end method

.method public handlePreLogout()V
    .locals 1

    .line 363
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->fontScalingPublisher:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0}, Lcom/discord/utilities/persister/Persister;->clear()Ljava/lang/Object;

    return-void
.end method

.method handleUserSettingsUpdate(Lcom/discord/models/domain/ModelUserSettings;)V
    .locals 0
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .line 412
    invoke-direct {p0, p1}, Lcom/discord/stores/StoreUserSettings;->handleUserSettings(Lcom/discord/models/domain/ModelUserSettings;)V

    return-void
.end method

.method public init(Landroid/content/Context;)V
    .locals 1

    .line 398
    invoke-super {p0, p1}, Lcom/discord/stores/Store;->init(Landroid/content/Context;)V

    .line 400
    iget-object p1, p0, Lcom/discord/stores/StoreUserSettings;->themePublisher:Lrx/subjects/Subject;

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getTheme()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/subjects/Subject;->onNext(Ljava/lang/Object;)V

    .line 401
    iget-object p1, p0, Lcom/discord/stores/StoreUserSettings;->localePublisher:Lrx/subjects/Subject;

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getLocale()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/subjects/Subject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method reset()V
    .locals 2

    .line 384
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->themePublisher:Lrx/subjects/Subject;

    const-string v1, "dark"

    invoke-virtual {v0, v1}, Lrx/subjects/Subject;->onNext(Ljava/lang/Object;)V

    .line 385
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->localePublisher:Lrx/subjects/Subject;

    sget-object v1, Lcom/discord/models/domain/ModelUserSettings;->LOCALE_DEFAULT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lrx/subjects/Subject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public setAllowAnimatedEmojis(Lcom/discord/app/AppActivity;Z)V
    .locals 2

    .line 338
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->allowAnimatedEmojisPublisher:Lcom/discord/utilities/persister/Persister;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/utilities/persister/Persister;->set(Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getSyncTextAndImages()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 341
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-static {p2}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithAllowAnimatedEmojis(Ljava/lang/Boolean;)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object p2

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Lcom/discord/stores/StoreUserSettings;->updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V

    :cond_0
    return-void
.end method

.method public setBackButtonOpensDrawer(Z)V
    .locals 2

    .line 308
    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getBackButtonOpensDrawer()Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 309
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CACHE_KEY_BACK_BUTTON_OPEN_DRAWER"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-void
.end method

.method public setDefaultGuildsRestricted(Lcom/discord/app/AppActivity;ZLjava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/app/AppActivity;",
            "Z",
            "Ljava/util/Collection<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 256
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-static {p2, p3}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithRestrictedGuilds(Ljava/lang/Boolean;Ljava/util/Collection;)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object p2

    const/4 p3, 0x0

    invoke-static {p1, p2, p3}, Lcom/discord/stores/StoreUserSettings;->updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V

    return-void
.end method

.method public setDeveloperMode(Lcom/discord/app/AppActivity;Z)V
    .locals 2

    .line 327
    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getDeveloperMode()Z

    move-result v0

    if-eq v0, p2, :cond_0

    .line 331
    invoke-static {p2}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithDeveloperMode(Z)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object v0

    const v1, 0x7f12114c

    .line 332
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 329
    invoke-static {p1, v0, v1}, Lcom/discord/stores/StoreUserSettings;->updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V

    .line 333
    invoke-direct {p0, p2}, Lcom/discord/stores/StoreUserSettings;->setDeveloperModeInternal(Z)V

    :cond_0
    return-void
.end method

.method public setExplicitContentFilter(Lcom/discord/app/AppActivity;I)V
    .locals 1

    .line 260
    invoke-static {p2}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithExplicitContentFilter(I)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object p2

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Lcom/discord/stores/StoreUserSettings;->updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V

    return-void
.end method

.method public setFontScale(I)V
    .locals 2

    .line 359
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->fontScalingPublisher:Lcom/discord/utilities/persister/Persister;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/discord/utilities/persister/Persister;->set(Ljava/lang/Object;Z)Ljava/lang/Object;

    return-void
.end method

.method public setFriendSourceFlags(Lcom/discord/app/AppActivity;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 0

    .line 268
    invoke-static {p2, p3, p4}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithFriendSourceFlags(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object p2

    const/4 p3, 0x0

    invoke-static {p1, p2, p3}, Lcom/discord/stores/StoreUserSettings;->updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V

    return-void
.end method

.method public setInlineAttachmentMedia(Lcom/discord/app/AppActivity;Z)V
    .locals 2

    .line 282
    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getInlineAttachmentMedia()Z

    move-result v0

    if-eq v0, p2, :cond_0

    .line 283
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CACHE_KEY_INLINE_ATTACHMENT_MEDIA"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 286
    :cond_0
    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getSyncTextAndImages()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 287
    invoke-static {p2}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithInlineAttachmentMedia(Z)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object p2

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Lcom/discord/stores/StoreUserSettings;->updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V

    :cond_1
    return-void
.end method

.method public setInlineEmbedMedia(Lcom/discord/app/AppActivity;Z)V
    .locals 2

    .line 272
    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getInlineEmbedMedia()Z

    move-result v0

    if-eq v0, p2, :cond_0

    .line 273
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CACHE_KEY_INLINE_EMBED_MEDIA"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 276
    :cond_0
    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getSyncTextAndImages()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 277
    invoke-static {p2}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithInlineEmbedMedia(Z)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object p2

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Lcom/discord/stores/StoreUserSettings;->updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V

    :cond_1
    return-void
.end method

.method public setLocale(Lcom/discord/app/AppActivity;Ljava/lang/String;)V
    .locals 1

    .line 234
    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getLocaleSync()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    invoke-static {p2}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithLocale(Ljava/lang/String;)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object p2

    const v0, 0x7f120ab5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/discord/stores/StoreUserSettings;->updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V

    return-void

    .line 237
    :cond_0
    invoke-direct {p0, p2}, Lcom/discord/stores/StoreUserSettings;->setLocale(Ljava/lang/String;)V

    return-void
.end method

.method public setLocaleSync(Z)V
    .locals 2

    .line 228
    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getLocaleSync()Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 229
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CACHE_KEY_LOCALE_SYNC"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-void
.end method

.method public setMobileOverlay(Z)V
    .locals 2

    .line 314
    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getMobileOverlay()Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 315
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CACHE_KEY_MOBILE_OVERLAY"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 316
    invoke-static {p1}, Lcom/discord/utilities/analytics/AnalyticsTracker;->overlayToggled(Z)V

    :cond_0
    return-void
.end method

.method public setRenderEmbeds(Lcom/discord/app/AppActivity;Z)V
    .locals 2

    .line 292
    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getRenderEmbeds()Z

    move-result v0

    if-eq v0, p2, :cond_0

    .line 293
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CACHE_KEY_RENDER_EMBEDS"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 296
    :cond_0
    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getSyncTextAndImages()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 297
    invoke-static {p2}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithRenderEmbeds(Z)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object p2

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Lcom/discord/stores/StoreUserSettings;->updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V

    :cond_1
    return-void
.end method

.method public setRestrictedGuildIds(Lcom/discord/app/AppActivity;Ljava/util/Collection;JZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/app/AppActivity;",
            "Ljava/util/Collection<",
            "Ljava/lang/Long;",
            ">;JZ)V"
        }
    .end annotation

    .line 242
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    if-eqz p5, :cond_0

    .line 244
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_0

    .line 245
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    if-nez p5, :cond_1

    .line 249
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-interface {v0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_1
    const/4 p2, 0x0

    .line 252
    invoke-static {p2, v0}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithRestrictedGuilds(Ljava/lang/Boolean;Ljava/util/Collection;)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object p3

    invoke-static {p1, p3, p2}, Lcom/discord/stores/StoreUserSettings;->updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V

    return-void
.end method

.method public setShiftEnterToSend(Z)V
    .locals 2

    .line 321
    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getShiftEnterToSend()Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 322
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CACHE_KEY_SHIFT_ENTER_TO_SEND"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-void
.end method

.method public setShowCurrentGame(Lcom/discord/app/AppActivity;Z)V
    .locals 2

    .line 350
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->showCurrentGame:Lcom/discord/utilities/persister/Persister;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/utilities/persister/Persister;->set(Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    invoke-static {p2}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithShowCurrentGame(Z)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object p2

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Lcom/discord/stores/StoreUserSettings;->updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V

    return-void
.end method

.method public setSyncTextAndImages(Z)V
    .locals 2

    .line 302
    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getSyncTextAndImages()Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 303
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CACHE_KEY_SYNC_TEXT_AND_IMAGES"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-void
.end method

.method public setSyncTheme(Z)V
    .locals 2

    .line 203
    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getThemeSync()Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 204
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CACHE_KEY_THEME_SYNC"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-void
.end method

.method public setTheme(Lcom/discord/app/AppActivity;Ljava/lang/String;)V
    .locals 4

    .line 209
    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getThemeSync()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, -0x1

    .line 210
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    const v2, 0x67f598ac

    const/4 v3, 0x0

    if-eq v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "pureEvil"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    :cond_1
    :goto_0
    const-string v1, "CACHE_KEY_THEME_PURE_EVIL"

    if-eqz v0, :cond_2

    .line 218
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 219
    invoke-static {p2}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithTheme(Ljava/lang/String;)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object p2

    const v0, 0x7f12114c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/discord/stores/StoreUserSettings;->updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V

    return-void

    .line 212
    :cond_2
    iget-object p2, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {p2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p2

    const/4 v0, 0x1

    invoke-interface {p2, v1, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p2

    invoke-interface {p2}, Landroid/content/SharedPreferences$Editor;->apply()V

    const-string p2, "dark"

    .line 214
    invoke-static {p2}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithTheme(Ljava/lang/String;)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object p2

    const v0, 0x7f12114b

    .line 215
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 213
    invoke-static {p1, p2, v0}, Lcom/discord/stores/StoreUserSettings;->updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V

    return-void

    .line 223
    :cond_3
    invoke-direct {p0, p2}, Lcom/discord/stores/StoreUserSettings;->setTheme(Ljava/lang/String;)V

    return-void
.end method

.method public setUseChromeCustomTabs(Z)V
    .locals 2

    .line 346
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CACHE_KEY_USE_CHROME_CUSTOM_TABS"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method
