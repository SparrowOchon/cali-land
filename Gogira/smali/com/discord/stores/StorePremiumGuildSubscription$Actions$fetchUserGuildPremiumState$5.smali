.class final Lcom/discord/stores/StorePremiumGuildSubscription$Actions$fetchUserGuildPremiumState$5;
.super Lkotlin/jvm/internal/l;
.source "StorePremiumGuildSubscription.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->fetchUserGuildPremiumState()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/l;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Pair<",
        "+",
        "Ljava/util/List<",
        "+",
        "Lcom/discord/models/domain/ModelPremiumGuildSubscription;",
        ">;+",
        "Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/stores/StorePremiumGuildSubscription$Actions$fetchUserGuildPremiumState$5;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/stores/StorePremiumGuildSubscription$Actions$fetchUserGuildPremiumState$5;

    invoke-direct {v0}, Lcom/discord/stores/StorePremiumGuildSubscription$Actions$fetchUserGuildPremiumState$5;-><init>()V

    sput-object v0, Lcom/discord/stores/StorePremiumGuildSubscription$Actions$fetchUserGuildPremiumState$5;->INSTANCE:Lcom/discord/stores/StorePremiumGuildSubscription$Actions$fetchUserGuildPremiumState$5;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/l;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 105
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StorePremiumGuildSubscription$Actions$fetchUserGuildPremiumState$5;->invoke(Lkotlin/Pair;)V

    sget-object p1, Lkotlin/Unit;->bjS:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lkotlin/Pair;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscription;",
            ">;",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;",
            ">;)V"
        }
    .end annotation

    .line 1000
    iget-object v0, p1, Lkotlin/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    .line 2000
    iget-object p1, p1, Lkotlin/Pair;->second:Ljava/lang/Object;

    check-cast p1, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;

    .line 145
    sget-object v1, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->INSTANCE:Lcom/discord/stores/StorePremiumGuildSubscription$Actions;

    invoke-static {v1}, Lcom/discord/stores/StorePremiumGuildSubscription$Actions;->access$getStream$p(Lcom/discord/stores/StorePremiumGuildSubscription$Actions;)Lcom/discord/stores/StoreStream;

    move-result-object v1

    new-instance v2, Lcom/discord/stores/StorePremiumGuildSubscription$Actions$fetchUserGuildPremiumState$5$1;

    invoke-direct {v2, v0, p1}, Lcom/discord/stores/StorePremiumGuildSubscription$Actions$fetchUserGuildPremiumState$5$1;-><init>(Ljava/util/List;Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreStream;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
