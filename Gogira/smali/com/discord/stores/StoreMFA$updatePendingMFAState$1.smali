.class final Lcom/discord/stores/StoreMFA$updatePendingMFAState$1;
.super Lkotlin/jvm/internal/l;
.source "StoreMFA.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreMFA;->updatePendingMFAState(Lcom/discord/stores/StoreMFA$MFAActivationState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/l;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic $newActivationState:Lcom/discord/stores/StoreMFA$MFAActivationState;

.field final synthetic this$0:Lcom/discord/stores/StoreMFA;


# direct methods
.method constructor <init>(Lcom/discord/stores/StoreMFA;Lcom/discord/stores/StoreMFA$MFAActivationState;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreMFA$updatePendingMFAState$1;->this$0:Lcom/discord/stores/StoreMFA;

    iput-object p2, p0, Lcom/discord/stores/StoreMFA$updatePendingMFAState$1;->$newActivationState:Lcom/discord/stores/StoreMFA$MFAActivationState;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/l;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/discord/stores/StoreMFA$updatePendingMFAState$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->bjS:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 6

    .line 33
    iget-object v0, p0, Lcom/discord/stores/StoreMFA$updatePendingMFAState$1;->this$0:Lcom/discord/stores/StoreMFA;

    invoke-static {v0}, Lcom/discord/stores/StoreMFA;->access$getState$p(Lcom/discord/stores/StoreMFA;)Lcom/discord/stores/StoreMFA$State;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreMFA$updatePendingMFAState$1;->$newActivationState:Lcom/discord/stores/StoreMFA$MFAActivationState;

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static {v1, v2, v3, v4, v5}, Lcom/discord/stores/StoreMFA$State;->copy$default(Lcom/discord/stores/StoreMFA$State;Lcom/discord/stores/StoreMFA$MFAActivationState;ZILjava/lang/Object;)Lcom/discord/stores/StoreMFA$State;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/discord/stores/StoreMFA;->access$setState$p(Lcom/discord/stores/StoreMFA;Lcom/discord/stores/StoreMFA$State;)V

    .line 34
    iget-object v0, p0, Lcom/discord/stores/StoreMFA$updatePendingMFAState$1;->this$0:Lcom/discord/stores/StoreMFA;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/discord/stores/StoreMFA;->access$setDirty$p(Lcom/discord/stores/StoreMFA;Z)V

    return-void
.end method
