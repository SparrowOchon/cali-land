.class public final Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;
.super Lcom/discord/stores/StorePremiumGuildSubscription$State;
.source "StorePremiumGuildSubscription.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StorePremiumGuildSubscription$State;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Loaded"
.end annotation


# instance fields
.field private final cooldown:Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;

.field private final premiumGuildSubscriptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscription;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscription;",
            ">;",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;",
            ")V"
        }
    .end annotation

    const-string v0, "premiumGuildSubscriptions"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 102
    invoke-direct {p0, v0}, Lcom/discord/stores/StorePremiumGuildSubscription$State;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->premiumGuildSubscriptions:Ljava/util/List;

    iput-object p2, p0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->cooldown:Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;Ljava/util/List;Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;ILjava/lang/Object;)Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->premiumGuildSubscriptions:Ljava/util/List;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->cooldown:Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->copy(Ljava/util/List;Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;)Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscription;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->premiumGuildSubscriptions:Ljava/util/List;

    return-object v0
.end method

.method public final component2()Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->cooldown:Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;

    return-object v0
.end method

.method public final copy(Ljava/util/List;Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;)Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscription;",
            ">;",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;",
            ")",
            "Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;"
        }
    .end annotation

    const-string v0, "premiumGuildSubscriptions"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/k;->h(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    invoke-direct {v0, p1, p2}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;-><init>(Ljava/util/List;Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;)V

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    iget-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->premiumGuildSubscriptions:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->premiumGuildSubscriptions:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->cooldown:Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;

    iget-object p1, p1, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->cooldown:Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/k;->n(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCooldown()Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->cooldown:Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;

    return-object v0
.end method

.method public final getPremiumGuildSubscriptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscription;",
            ">;"
        }
    .end annotation

    .line 98
    iget-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->premiumGuildSubscriptions:Ljava/util/List;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->premiumGuildSubscriptions:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->cooldown:Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Loaded(premiumGuildSubscriptions="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->premiumGuildSubscriptions:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cooldown="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->cooldown:Lcom/discord/models/domain/ModelPremiumGuildSubscriptionCooldown;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
