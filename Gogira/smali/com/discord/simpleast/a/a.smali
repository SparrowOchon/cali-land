.class public final Lcom/discord/simpleast/a/a;
.super Ljava/lang/Object;
.source "MarkdownRules.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/simpleast/a/a$c;,
        Lcom/discord/simpleast/a/a$b;,
        Lcom/discord/simpleast/a/a$a;
    }
.end annotation


# static fields
.field private static final AA:Ljava/util/regex/Pattern;

.field private static final AB:Ljava/util/regex/Pattern;

.field public static final AC:Lcom/discord/simpleast/a/a;

.field private static final Ay:Ljava/util/regex/Pattern;

.field private static final Az:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 25
    new-instance v0, Lcom/discord/simpleast/a/a;

    invoke-direct {v0}, Lcom/discord/simpleast/a/a;-><init>()V

    sput-object v0, Lcom/discord/simpleast/a/a;->AC:Lcom/discord/simpleast/a/a;

    const/4 v0, 0x0

    const-string v1, "^\\*[ \\t](.*)(?=\\n|$)"

    .line 34
    invoke-static {v1, v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string v2, "java.util.regex.Pattern.compile(this, flags)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v1, Lcom/discord/simpleast/a/a;->Ay:Ljava/util/regex/Pattern;

    const-string v1, "^\\s*(#+)[ \\t](.*) *(?=\\n|$)"

    .line 44
    invoke-static {v1, v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-static {v1, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v1, Lcom/discord/simpleast/a/a;->Az:Ljava/util/regex/Pattern;

    const-string v1, "^\\s*(.+)\\n *(=|-){3,} *(?=\\n|$)"

    .line 56
    invoke-static {v1, v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-static {v0, v2}, Lkotlin/jvm/internal/k;->g(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/discord/simpleast/a/a;->AA:Ljava/util/regex/Pattern;

    .line 67
    new-instance v0, Lkotlin/text/Regex;

    const-string v1, "^\\s*(?:(?:(.+)(?: +\\{([\\w ]*)\\}))|(.*))[ \\t]*\\n *([=\\-]){3,}[ \\t]*(?=\\n|$)"

    invoke-direct {v0, v1}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;)V

    .line 1221
    iget-object v0, v0, Lkotlin/text/Regex;->nativePattern:Ljava/util/regex/Pattern;

    .line 67
    sput-object v0, Lcom/discord/simpleast/a/a;->AB:Ljava/util/regex/Pattern;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static eA()Ljava/util/regex/Pattern;
    .locals 1

    .line 44
    sget-object v0, Lcom/discord/simpleast/a/a;->Az:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method public static eB()Ljava/util/regex/Pattern;
    .locals 1

    .line 56
    sget-object v0, Lcom/discord/simpleast/a/a;->AA:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method public static eC()Ljava/util/regex/Pattern;
    .locals 1

    .line 66
    sget-object v0, Lcom/discord/simpleast/a/a;->AB:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method public static ez()Ljava/util/regex/Pattern;
    .locals 1

    .line 34
    sget-object v0, Lcom/discord/simpleast/a/a;->Ay:Ljava/util/regex/Pattern;

    return-object v0
.end method
