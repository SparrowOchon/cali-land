.class public final Lcom/airbnb/lottie/c/c/d;
.super Ljava/lang/Object;
.source "Layer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/airbnb/lottie/c/c/d$b;,
        Lcom/airbnb/lottie/c/c/d$a;
    }
.end annotation


# instance fields
.field final fB:Lcom/airbnb/lottie/d;

.field final fQ:F

.field final hH:Z

.field final iL:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/c/b/g;",
            ">;"
        }
    .end annotation
.end field

.field final jM:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/c/b/b;",
            ">;"
        }
    .end annotation
.end field

.field final lX:Ljava/lang/String;

.field public final lY:J

.field public final lZ:Lcom/airbnb/lottie/c/c/d$a;

.field final ld:Lcom/airbnb/lottie/c/a/l;

.field final ma:Ljava/lang/String;

.field final mb:I

.field final mc:I

.field final md:I

.field final mf:F

.field final mg:I

.field final mh:I

.field final mi:Lcom/airbnb/lottie/c/a/j;

.field final mj:Lcom/airbnb/lottie/c/a/k;

.field final mk:Lcom/airbnb/lottie/c/a/b;

.field final ml:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/g/a<",
            "Ljava/lang/Float;",
            ">;>;"
        }
    .end annotation
.end field

.field final mm:I

.field final parentId:J


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/airbnb/lottie/d;Ljava/lang/String;JLcom/airbnb/lottie/c/c/d$a;JLjava/lang/String;Ljava/util/List;Lcom/airbnb/lottie/c/a/l;IIIFFIILcom/airbnb/lottie/c/a/j;Lcom/airbnb/lottie/c/a/k;Ljava/util/List;ILcom/airbnb/lottie/c/a/b;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/c/b/b;",
            ">;",
            "Lcom/airbnb/lottie/d;",
            "Ljava/lang/String;",
            "J",
            "Lcom/airbnb/lottie/c/c/d$a;",
            "J",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/c/b/g;",
            ">;",
            "Lcom/airbnb/lottie/c/a/l;",
            "IIIFFII",
            "Lcom/airbnb/lottie/c/a/j;",
            "Lcom/airbnb/lottie/c/a/k;",
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/g/a<",
            "Ljava/lang/Float;",
            ">;>;I",
            "Lcom/airbnb/lottie/c/a/b;",
            "Z)V"
        }
    .end annotation

    move-object v0, p0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 66
    iput-object v1, v0, Lcom/airbnb/lottie/c/c/d;->jM:Ljava/util/List;

    move-object v1, p2

    .line 67
    iput-object v1, v0, Lcom/airbnb/lottie/c/c/d;->fB:Lcom/airbnb/lottie/d;

    move-object v1, p3

    .line 68
    iput-object v1, v0, Lcom/airbnb/lottie/c/c/d;->lX:Ljava/lang/String;

    move-wide v1, p4

    .line 69
    iput-wide v1, v0, Lcom/airbnb/lottie/c/c/d;->lY:J

    move-object v1, p6

    .line 70
    iput-object v1, v0, Lcom/airbnb/lottie/c/c/d;->lZ:Lcom/airbnb/lottie/c/c/d$a;

    move-wide v1, p7

    .line 71
    iput-wide v1, v0, Lcom/airbnb/lottie/c/c/d;->parentId:J

    move-object v1, p9

    .line 72
    iput-object v1, v0, Lcom/airbnb/lottie/c/c/d;->ma:Ljava/lang/String;

    move-object v1, p10

    .line 73
    iput-object v1, v0, Lcom/airbnb/lottie/c/c/d;->iL:Ljava/util/List;

    move-object v1, p11

    .line 74
    iput-object v1, v0, Lcom/airbnb/lottie/c/c/d;->ld:Lcom/airbnb/lottie/c/a/l;

    move v1, p12

    .line 75
    iput v1, v0, Lcom/airbnb/lottie/c/c/d;->mb:I

    move/from16 v1, p13

    .line 76
    iput v1, v0, Lcom/airbnb/lottie/c/c/d;->mc:I

    move/from16 v1, p14

    .line 77
    iput v1, v0, Lcom/airbnb/lottie/c/c/d;->md:I

    move/from16 v1, p15

    .line 78
    iput v1, v0, Lcom/airbnb/lottie/c/c/d;->mf:F

    move/from16 v1, p16

    .line 79
    iput v1, v0, Lcom/airbnb/lottie/c/c/d;->fQ:F

    move/from16 v1, p17

    .line 80
    iput v1, v0, Lcom/airbnb/lottie/c/c/d;->mg:I

    move/from16 v1, p18

    .line 81
    iput v1, v0, Lcom/airbnb/lottie/c/c/d;->mh:I

    move-object/from16 v1, p19

    .line 82
    iput-object v1, v0, Lcom/airbnb/lottie/c/c/d;->mi:Lcom/airbnb/lottie/c/a/j;

    move-object/from16 v1, p20

    .line 83
    iput-object v1, v0, Lcom/airbnb/lottie/c/c/d;->mj:Lcom/airbnb/lottie/c/a/k;

    move-object/from16 v1, p21

    .line 84
    iput-object v1, v0, Lcom/airbnb/lottie/c/c/d;->ml:Ljava/util/List;

    move/from16 v1, p22

    .line 85
    iput v1, v0, Lcom/airbnb/lottie/c/c/d;->mm:I

    move-object/from16 v1, p23

    .line 86
    iput-object v1, v0, Lcom/airbnb/lottie/c/c/d;->mk:Lcom/airbnb/lottie/c/a/b;

    move/from16 v1, p24

    .line 87
    iput-boolean v1, v0, Lcom/airbnb/lottie/c/c/d;->hH:Z

    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    .line 175
    invoke-virtual {p0, v0}, Lcom/airbnb/lottie/c/c/d;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .line 183
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 184
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1111
    iget-object v1, p0, Lcom/airbnb/lottie/c/c/d;->lX:Ljava/lang/String;

    .line 184
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    iget-object v2, p0, Lcom/airbnb/lottie/c/c/d;->fB:Lcom/airbnb/lottie/d;

    .line 1139
    iget-wide v3, p0, Lcom/airbnb/lottie/c/c/d;->parentId:J

    .line 185
    invoke-virtual {v2, v3, v4}, Lcom/airbnb/lottie/d;->h(J)Lcom/airbnb/lottie/c/c/d;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v3, "\t\tParents: "

    .line 187
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2111
    iget-object v3, v2, Lcom/airbnb/lottie/c/c/d;->lX:Ljava/lang/String;

    .line 187
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    iget-object v3, p0, Lcom/airbnb/lottie/c/c/d;->fB:Lcom/airbnb/lottie/d;

    .line 2139
    iget-wide v4, v2, Lcom/airbnb/lottie/c/c/d;->parentId:J

    .line 188
    invoke-virtual {v3, v4, v5}, Lcom/airbnb/lottie/d;->h(J)Lcom/airbnb/lottie/c/c/d;

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_0

    const-string v3, "->"

    .line 190
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3111
    iget-object v3, v2, Lcom/airbnb/lottie/c/c/d;->lX:Ljava/lang/String;

    .line 190
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    iget-object v3, p0, Lcom/airbnb/lottie/c/c/d;->fB:Lcom/airbnb/lottie/d;

    .line 3139
    iget-wide v4, v2, Lcom/airbnb/lottie/c/c/d;->parentId:J

    .line 191
    invoke-virtual {v3, v4, v5}, Lcom/airbnb/lottie/d;->h(J)Lcom/airbnb/lottie/c/c/d;

    move-result-object v2

    goto :goto_0

    .line 193
    :cond_0
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4127
    :cond_1
    iget-object v2, p0, Lcom/airbnb/lottie/c/c/d;->iL:Ljava/util/List;

    .line 195
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 196
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\tMasks: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5127
    iget-object v2, p0, Lcom/airbnb/lottie/c/c/d;->iL:Ljava/util/List;

    .line 196
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5159
    :cond_2
    iget v2, p0, Lcom/airbnb/lottie/c/c/d;->mb:I

    if-eqz v2, :cond_3

    .line 6155
    iget v2, p0, Lcom/airbnb/lottie/c/c/d;->mc:I

    if-eqz v2, :cond_3

    .line 199
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\tBackground: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 6159
    iget v5, p0, Lcom/airbnb/lottie/c/c/d;->mb:I

    .line 200
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    .line 7155
    iget v5, p0, Lcom/airbnb/lottie/c/c/d;->mc:I

    .line 200
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    .line 8151
    iget v5, p0, Lcom/airbnb/lottie/c/c/d;->md:I

    .line 200
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const-string v4, "%dx%d %X\n"

    invoke-static {v2, v4, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 199
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    :cond_3
    iget-object v2, p0, Lcom/airbnb/lottie/c/c/d;->jM:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 203
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\tShapes:\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    iget-object v2, p0, Lcom/airbnb/lottie/c/c/d;->jM:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 205
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\t\t"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 208
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
