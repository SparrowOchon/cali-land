.class public final Lcom/airbnb/lottie/c/d;
.super Ljava/lang/Object;
.source "FontCharacter.java"


# instance fields
.field private final fontFamily:Ljava/lang/String;

.field private final jL:Ljava/lang/String;

.field public final jM:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/c/b/n;",
            ">;"
        }
    .end annotation
.end field

.field private final jN:C

.field public final jO:D

.field private final jz:D


# direct methods
.method public constructor <init>(Ljava/util/List;CDDLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/c/b/n;",
            ">;CDD",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/airbnb/lottie/c/d;->jM:Ljava/util/List;

    .line 32
    iput-char p2, p0, Lcom/airbnb/lottie/c/d;->jN:C

    .line 33
    iput-wide p3, p0, Lcom/airbnb/lottie/c/d;->jz:D

    .line 34
    iput-wide p5, p0, Lcom/airbnb/lottie/c/d;->jO:D

    .line 35
    iput-object p7, p0, Lcom/airbnb/lottie/c/d;->jL:Ljava/lang/String;

    .line 36
    iput-object p8, p0, Lcom/airbnb/lottie/c/d;->fontFamily:Ljava/lang/String;

    return-void
.end method

.method public static a(CLjava/lang/String;Ljava/lang/String;)I
    .locals 0

    add-int/lit8 p0, p0, 0x0

    mul-int/lit8 p0, p0, 0x1f

    .line 17
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p1

    add-int/2addr p0, p1

    mul-int/lit8 p0, p0, 0x1f

    .line 18
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result p1

    add-int/2addr p0, p1

    return p0
.end method


# virtual methods
.method public final hashCode()I
    .locals 3

    .line 56
    iget-char v0, p0, Lcom/airbnb/lottie/c/d;->jN:C

    iget-object v1, p0, Lcom/airbnb/lottie/c/d;->fontFamily:Ljava/lang/String;

    iget-object v2, p0, Lcom/airbnb/lottie/c/d;->jL:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/airbnb/lottie/c/d;->a(CLjava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method
