.class public final Lcom/airbnb/lottie/c/b/i;
.super Ljava/lang/Object;
.source "PolystarShape.java"

# interfaces
.implements Lcom/airbnb/lottie/c/b/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/airbnb/lottie/c/b/i$a;
    }
.end annotation


# instance fields
.field public final hH:Z

.field public final ie:Lcom/airbnb/lottie/c/b/i$a;

.field public final kS:Lcom/airbnb/lottie/c/a/b;

.field public final kT:Lcom/airbnb/lottie/c/a/b;

.field public final kU:Lcom/airbnb/lottie/c/a/b;

.field public final kV:Lcom/airbnb/lottie/c/a/b;

.field public final kW:Lcom/airbnb/lottie/c/a/b;

.field public final kc:Lcom/airbnb/lottie/c/a/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/airbnb/lottie/c/a/m<",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation
.end field

.field public final ke:Lcom/airbnb/lottie/c/a/b;

.field public final name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/airbnb/lottie/c/b/i$a;Lcom/airbnb/lottie/c/a/b;Lcom/airbnb/lottie/c/a/m;Lcom/airbnb/lottie/c/a/b;Lcom/airbnb/lottie/c/a/b;Lcom/airbnb/lottie/c/a/b;Lcom/airbnb/lottie/c/a/b;Lcom/airbnb/lottie/c/a/b;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/airbnb/lottie/c/b/i$a;",
            "Lcom/airbnb/lottie/c/a/b;",
            "Lcom/airbnb/lottie/c/a/m<",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            ">;",
            "Lcom/airbnb/lottie/c/a/b;",
            "Lcom/airbnb/lottie/c/a/b;",
            "Lcom/airbnb/lottie/c/a/b;",
            "Lcom/airbnb/lottie/c/a/b;",
            "Lcom/airbnb/lottie/c/a/b;",
            "Z)V"
        }
    .end annotation

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/airbnb/lottie/c/b/i;->name:Ljava/lang/String;

    .line 50
    iput-object p2, p0, Lcom/airbnb/lottie/c/b/i;->ie:Lcom/airbnb/lottie/c/b/i$a;

    .line 51
    iput-object p3, p0, Lcom/airbnb/lottie/c/b/i;->kS:Lcom/airbnb/lottie/c/a/b;

    .line 52
    iput-object p4, p0, Lcom/airbnb/lottie/c/b/i;->kc:Lcom/airbnb/lottie/c/a/m;

    .line 53
    iput-object p5, p0, Lcom/airbnb/lottie/c/b/i;->ke:Lcom/airbnb/lottie/c/a/b;

    .line 54
    iput-object p6, p0, Lcom/airbnb/lottie/c/b/i;->kT:Lcom/airbnb/lottie/c/a/b;

    .line 55
    iput-object p7, p0, Lcom/airbnb/lottie/c/b/i;->kU:Lcom/airbnb/lottie/c/a/b;

    .line 56
    iput-object p8, p0, Lcom/airbnb/lottie/c/b/i;->kV:Lcom/airbnb/lottie/c/a/b;

    .line 57
    iput-object p9, p0, Lcom/airbnb/lottie/c/b/i;->kW:Lcom/airbnb/lottie/c/a/b;

    .line 58
    iput-boolean p10, p0, Lcom/airbnb/lottie/c/b/i;->hH:Z

    return-void
.end method


# virtual methods
.method public final a(Lcom/airbnb/lottie/f;Lcom/airbnb/lottie/c/c/a;)Lcom/airbnb/lottie/a/a/c;
    .locals 1

    .line 102
    new-instance v0, Lcom/airbnb/lottie/a/a/n;

    invoke-direct {v0, p1, p2, p0}, Lcom/airbnb/lottie/a/a/n;-><init>(Lcom/airbnb/lottie/f;Lcom/airbnb/lottie/c/c/a;Lcom/airbnb/lottie/c/b/i;)V

    return-object v0
.end method
