.class final Lcom/crashlytics/android/core/k$16;
.super Ljava/lang/Object;
.source "CrashlyticsController.java"

# interfaces
.implements Lcom/crashlytics/android/core/k$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/crashlytics/android/core/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic rH:I

.field final synthetic rI:I

.field final synthetic rJ:J

.field final synthetic rK:J

.field final synthetic rL:Z

.field final synthetic rM:Ljava/util/Map;

.field final synthetic rN:I

.field final synthetic rt:Lcom/crashlytics/android/core/k;


# direct methods
.method constructor <init>(Lcom/crashlytics/android/core/k;IIJJZLjava/util/Map;I)V
    .locals 0

    .line 1253
    iput-object p1, p0, Lcom/crashlytics/android/core/k$16;->rt:Lcom/crashlytics/android/core/k;

    iput p2, p0, Lcom/crashlytics/android/core/k$16;->rH:I

    iput p3, p0, Lcom/crashlytics/android/core/k$16;->rI:I

    iput-wide p4, p0, Lcom/crashlytics/android/core/k$16;->rJ:J

    iput-wide p6, p0, Lcom/crashlytics/android/core/k$16;->rK:J

    iput-boolean p8, p0, Lcom/crashlytics/android/core/k$16;->rL:Z

    iput-object p9, p0, Lcom/crashlytics/android/core/k$16;->rM:Ljava/util/Map;

    iput p10, p0, Lcom/crashlytics/android/core/k$16;->rN:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/crashlytics/android/core/g;)V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1256
    iget v1, p0, Lcom/crashlytics/android/core/k$16;->rH:I

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iget v3, p0, Lcom/crashlytics/android/core/k$16;->rI:I

    iget-wide v4, p0, Lcom/crashlytics/android/core/k$16;->rJ:J

    iget-wide v6, p0, Lcom/crashlytics/android/core/k$16;->rK:J

    iget-boolean v8, p0, Lcom/crashlytics/android/core/k$16;->rL:Z

    iget-object v9, p0, Lcom/crashlytics/android/core/k$16;->rM:Ljava/util/Map;

    iget v10, p0, Lcom/crashlytics/android/core/k$16;->rN:I

    sget-object v11, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    sget-object v12, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    move-object v0, p1

    invoke-static/range {v0 .. v12}, Lcom/crashlytics/android/core/at;->a(Lcom/crashlytics/android/core/g;ILjava/lang/String;IJJZLjava/util/Map;ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method
