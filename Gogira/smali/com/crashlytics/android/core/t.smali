.class final Lcom/crashlytics/android/core/t;
.super Ljava/lang/Object;
.source "CrashlyticsUncaughtExceptionHandler.java"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/crashlytics/android/core/t$b;,
        Lcom/crashlytics/android/core/t$a;
    }
.end annotation


# instance fields
.field private final sr:Lcom/crashlytics/android/core/t$a;

.field private final ss:Lcom/crashlytics/android/core/t$b;

.field private final st:Z

.field private final su:Ljava/lang/Thread$UncaughtExceptionHandler;

.field final sv:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(Lcom/crashlytics/android/core/t$a;Lcom/crashlytics/android/core/t$b;ZLjava/lang/Thread$UncaughtExceptionHandler;)V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/crashlytics/android/core/t;->sr:Lcom/crashlytics/android/core/t$a;

    .line 32
    iput-object p2, p0, Lcom/crashlytics/android/core/t;->ss:Lcom/crashlytics/android/core/t$b;

    .line 33
    iput-boolean p3, p0, Lcom/crashlytics/android/core/t;->st:Z

    .line 34
    iput-object p4, p0, Lcom/crashlytics/android/core/t;->su:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 35
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/crashlytics/android/core/t;->sv:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method


# virtual methods
.method public final uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 6

    const-string v0, "Crashlytics completed exception processing. Invoking default exception handler."

    const-string v1, "CrashlyticsCore"

    .line 40
    iget-object v2, p0, Lcom/crashlytics/android/core/t;->sv:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    const/4 v2, 0x0

    .line 42
    :try_start_0
    iget-object v3, p0, Lcom/crashlytics/android/core/t;->sr:Lcom/crashlytics/android/core/t$a;

    iget-object v4, p0, Lcom/crashlytics/android/core/t;->ss:Lcom/crashlytics/android/core/t$b;

    iget-boolean v5, p0, Lcom/crashlytics/android/core/t;->st:Z

    invoke-interface {v3, v4, p1, p2, v5}, Lcom/crashlytics/android/core/t$a;->b(Lcom/crashlytics/android/core/t$b;Ljava/lang/Thread;Ljava/lang/Throwable;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    :goto_0
    invoke-static {}, Lio/fabric/sdk/android/c;->Ct()Lio/fabric/sdk/android/l;

    move-result-object v3

    invoke-interface {v3, v1, v0}, Lio/fabric/sdk/android/l;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/crashlytics/android/core/t;->su:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 52
    iget-object p1, p0, Lcom/crashlytics/android/core/t;->sv:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void

    :catchall_0
    move-exception v3

    goto :goto_1

    :catch_0
    move-exception v3

    .line 45
    :try_start_1
    invoke-static {}, Lio/fabric/sdk/android/c;->Ct()Lio/fabric/sdk/android/l;

    move-result-object v4

    const-string v5, "An error occurred in the uncaught exception handler"

    invoke-interface {v4, v1, v5, v3}, Lio/fabric/sdk/android/l;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 48
    :goto_1
    invoke-static {}, Lio/fabric/sdk/android/c;->Ct()Lio/fabric/sdk/android/l;

    move-result-object v4

    invoke-interface {v4, v1, v0}, Lio/fabric/sdk/android/l;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/crashlytics/android/core/t;->su:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 52
    iget-object p1, p0, Lcom/crashlytics/android/core/t;->sv:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 53
    goto :goto_3

    :goto_2
    throw v3

    :goto_3
    goto :goto_2
.end method
