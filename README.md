# Adventures in Cali Land

# Goal

Reverse the 'Gogira' in to peel off the CSS and replace it with something more lightweight and compact.

# Depackaging:

- `apktool d Gogira.apk`

# Notes

- Build using reactive Native
- imagepipelining, gifimage, imagetranscoding, filters and crash analytics seem to have a part of their code that was built natively. There of course is a static webpack code base built for native since Reactive Native.
-

# Used Framework/Libraries

- [okhttp3](https://square.github.io/okhttp/)
- [okio](https://github.com/square/okio)
- [firebase](https://firebase.google.com/)
- [Apache Language Codec](https://commons.apache.org/proper/commons-codec/apidocs/org/apache/commons/codec/language/package-summary.html)

./smali/com/discord/utilities/rest/RestAPI.smali => Interesting link maybe?
